'use strict';

var os = require('os'),
    bower = require('./bower.json'),
    webpack = require('webpack'),

    /*
     * If MICHONNE_BUILD_COMPRESS is set to anything, it will set isCompressed
     * to be `false`.  This should be set to `true` on production.
     */
    isCompressed = (typeof process.env.MICHONNE_BUILD_COMPRESS === 'undefined') ? true : false,

    /*
     * If MICHONNE_BUILD_MANGLE is set to anything, it will set isMangeld to be
     * `false`.  This should be set to `true` on production.
     */
    isMangled = (typeof process.env.MICHONNE_BUILD_MANGLE === 'undefined') ? true : false,

    /*
     * If MICHONNE_BUILD_BEAUTIFY is set to anything, it will set isBeautified
     * to be `true`.  This should be set to `false` on production.
     */
    isBeautified = (typeof process.env.MICHONNE_BUILD_BEAUTIFY === 'undefined') ? false : true,

    /* regex details: http://regex101.com/r/cI5rX3/1 */
    cnnFontsVersion = bower.dependencies['cnn-fonts'].match(/#([\d\.]+)$/),
    cnnIconsVersion = bower.dependencies['cnn-icons'].match(/#([\d\.]+)$/);


module.exports = function (grunt) {
    var requireEnvironmentVariables = false,
        i;

    require('load-grunt-tasks')(grunt);
    require('time-grunt')(grunt);

    grunt.log.oklns('Running with %d cores available to run tasks concurrently.', os.cpus().length);
    grunt.log.oklns('MICHONNE_BUILD_COMPRESS is set to: %j. (default is true)', isCompressed);
    grunt.log.oklns('MICHONNE_BUILD_MANGLE is set to: %j. (default is true)', isMangled);
    grunt.log.oklns('MICHONNE_BUILD_BEAUTIFY is set to: %j. (default is false)', isBeautified);

    /*
     * If a task is called that requries the ENVIRONMENT or EDITION environment
     * variables then set `requireEnvironmentVariables` to `true`.
     *
     * The only tasks that require this are:
     *     test
     *     test-all
     */
    for (i = 0; i < grunt.cli.tasks.length; i++) {
        if (grunt.cli.tasks[i].match(/test/)) {
            requireEnvironmentVariables = true;
            break;
        }
    }

    if (requireEnvironmentVariables && (!process.env.EDITION || !process.env.ENVIRONMENT)) {
        grunt.fail.fatal('The EDITION and/or ENVIRONMENT environment variables are not set.  `source` a `set-env/*.conf` file to set them.', 1);
    }

    grunt.initConfig({
        webpack: {
            componentbundler: {
                entry: {
                    adzones: './tmp/bootstrap/adzones.js',
                    animations: './tmp/bootstrap/animations.js',
                    maps: './tmp/bootstrap/maps.js',
                    subhub: './tmp/bootstrap/subhub.js',
                    usabilla: './tmp/bootstrap/usabilla.js',
                    video: './tmp/bootstrap/video.js',
                    weather: './tmp/bootstrap/weather.js',
                    loaded: './tmp/bootstrap/loaded.js',
                    videoLoader: './tmp/bootstrap/videoLoader.js',
                    videx: './tmp/bootstrap/videx.js',
                    facebook: './tmp/bootstrap/facebook.js',
                    listexpandable: './tmp/bootstrap/list-expandable.js',
                    locator: './tmp/bootstrap/locator-map.js',
                    metadata: './tmp/bootstrap/metadata-header.js',
                    byline: './tmp/bootstrap/byline-images.js',
                    breakoutmedia: './tmp/bootstrap/breakout-media.js',
                    tvschedule: './tmp/bootstrap/tv-schedule-header.js',
                    financial_libs: './tmp/bootstrap/financial-libs.js',
                    one_tap: './tmp/bootstrap/one-tap.js',
                    grid_resize: './tmp/bootstrap/grid-resize.js',
                    bootstrapper: './tmp/bootstrap/bootstrapper.js',
                    mailchimp: './tmp/bootstrap/mailchimp-subscriptions.js',
                    fave: './tmp/bootstrap/fave.js'
                },
                output: {
                    path: 'public/bundles/',
                    filename: '[name].[chunkhash:20].bundle.js'
                },
                plugins: [new webpack.optimize.UglifyJsPlugin({compress: {warnings: false}})],
                module: {
                    loaders: [
                        {test: /\.css/, loader: 'style!raw!'},
                        {test: /\.html/, loader: 'raw-loader'},
                        {
                            test: /iphone-inline-video.browser.js/,
                            loader: 'string-replace',
                            query: {
                                search: 'var makeVideoPlayableInline',
                                replace: 'window.CNN.makeVideoPlayableInline'
                            }
                        }
                    ],
                    noParse: [/cvp.js/]
                },
                storeStatsTo: 'webpackData'
            },
            headerBundler: {
                entry: {
                    header: './tmp/bootstrap/header.js'
                },
                output: {
                    path: 'public/bundles/',
                    filename: 'cnn-[name].[chunkhash:20]-first-bundle.js'
                },
                plugins: [new webpack.optimize.UglifyJsPlugin({compress: {warnings: false}})],
                module: {
                    loaders: [
                        {
                            /* jquery is placed on the global namespace as a side effect to these files being loaded because of the expose rule below */
                            test: /(page-events|fastdom-extend|current-size|scroll-top).js/,
                            loader: 'imports?jQuery=jquery'
                        },
                        {
                            test: /fastdom.js/,
                            loader: 'imports?this=>window'
                        },
                        {
                            /* injector namespace is being replaced with CNN namespace */
                            test: /injector.lite.js/,
                            loader: 'string-replace',
                            query: {search:'window.FAI', replace:'CNN', flags: 'g'}
                        },
                        {
                            test: /eq.js/,
                            loader: 'expose?eqjs'
                        },
                        {
                            /* jquery is placed on the global namespace if the file is renamed from jquery.js this rule won't fire */
                            test: /jquery.js/,
                            loader: 'expose?jQuery'
                        },
                        {
                            /* mobile is placed on the global namespace if the file is renamed from mobile-detect.js */
                            test: /mobile-detect.js/,
                            loader: 'expose?MobileDetect'
                        },
                        {
                            /* mobile is placed on the global namespace if the file is renamed from mobile-detect.js this rule won't fire */
                            test: /mobile-detect.js/,
                            loader: 'expose?mobileDetect'
                        }
                    ]
                },
                resolve: {
                    alias: {
                        jquery: '../../../bower_components/jquery/dist/jquery.js'
                    }
                },
                storeStatsTo: 'headerData'
            }
        },
        project: {
            'package': require('./package.json'),
            cnnFontsVersion: cnnFontsVersion[1],
            cnnIconsVersion: cnnIconsVersion[1],
            config: {}
        },

        bowercopy: {
            fonts: {
                files: {
                    'public/fonts/cnn/<%= project.cnnFontsVersion %>': ['cnn-fonts/dist/fonts/*'],
                    'public/fonts/icons/<%= project.cnnIconsVersion %>': ['cnn-icons/dist/fonts/*']
                }
            }
        },

        dust: {
            server: {
                files: {
                    'views/index.js': [
                        'views/**/*.dust',
                        '!views/xml/**'
                    ]
                },
                options: {
                    wrapper: 'commonjs',
                    runtime: false,
                    wrapperOptions: {
                        returning: 'dust',
                        deps: {
                            dust: 'dustjs-linkedin',
                            dustHelpers: 'dustjs-helpers',
                            customHelpers: '../lib/custom-helpers'
                        }
                    }
                }
            },
            xmlclient: {
                files: {'views/xmlindex.js': 'views/xml/**/*.dust'},
                options: {
                    wrapper: 'commonjs',
                    runtime: false,
                    wrapperOptions: {
                        returning: 'dust',
                        deps: {
                            dust: 'dustjs-linkedin',
                            dustHelpers: 'dustjs-helpers'
                        }
                    },
                    optimizers: {
                        format: function (ctx, node) { return node; }
                    }
                }
            },
            client: {
                files: {
                    'tmp/dust/src/js/dust/dust-client.js': [
                        'views/cards/card.dust',
                        'views/cards/tool/mycnn-alerts.dust',
                        'views/cards/tool/mycnn-account-details.dust',
                        'views/cards/tool/mycnn-newsletters.dust',
                        'views/cards/tool/mycnn-topics.dust',
                        'views/cards/tool/mycnn-social-settings.dust',
                        'views/cards/tool/mycnn-stream.dust',
                        'views/cards/tool/weather-airport-delays.dust',
                        'views/cards/tool/weather-conditions-forecast.dust',
                        'views/cards/tool/weather-current.dust',
                        'views/cards/tool/weather-impending.dust',
                        'views/cards/tool/weather-header.dust',
                        'views/cards/tool/weather-footer.dust',
                        'views/cards/tool/weather-make-default-button.dust',
                        'views/cards/tool/weather-stored-locations-single.dust',
                        'views/cards/tool/weather-general-html.dust',
                        'views/cards/tool/markets-quote-item.dust',
                        'views/cards/tool/markets-quote-header.dust',
                        'views/cards/tool/markets-typeahead-suggestion.dust',
                        'views/cards/tool/mycnn-manage-emails.dust',
                        'views/cards/tool/mycnn-update-avatar.dust',
                        'views/cards/tool/mailchimp-subscribe.dust',
                        'views/cards/tool/mailchimp-subscribe-form.dust',
                        'views/cards/tool/partner-hotels-item.dust',
                        'views/elements/image.dust',
                        'views/global/img-tag.dust',
                        'views/pages/common/video-end-slate-carousel.dust',
                        'views/pages/common/navigation-siblings.dust',
                        'views/pages/common/byline-images.dust'
                    ]
                },
                options: {
                    wrapper: false,
                    runtime: false
                }
            }
        },

        imagemin: {
            dist: {
                files: [
                    {
                        expand: true,
                        cwd: 'src/assets',
                        src: '**/*.{png,jpg,jpeg,gif,svg}',
                        dest: 'public/<%= project.package.version %>/assets'
                    }
                ]
            }
        },

        eslint: {
            bootstrap: {
                files: {
                    src: [
                        /* include */
                        'bootstrap/*.js'

                        /* exclude */
                    ]
                },
                options: {
                    configFile: '.eslint-bootstrap.json'
                }
            },
            client: {
                files: {
                    src: [
                        /* include */
                        'src/js/**/*.js',

                        /* exclude */
                        '!src/js/vendor/**'
                    ]
                },
                options: {
                    configFile: '.eslint-client.json'
                }
            },
            node: {
                files: {
                    /*
                     * You can generate this list with the following shell command:
                     * $ find . -name '*.js' | grep -v '\(node_modules\|bower_components\|public\|tmp\)' | perl -ne 'if(/(^.*\/)/){print $1."\n"}' | sort -u
                     */
                    src: [
                        /* include */
                        '*.js',
                        'cfg/**/*.js',
                        'controllers/**/*.js',
                        'lib/**/*.js',
                        'routes/**/*.js',

                        /* exclude */
                        '!bootstrap/**',
                        '!bower_components/**',
                        '!node_modules/**',
                        '!public/**',
                        '!src/**',
                        '!test/**',
                        '!tmp/**'
                    ]
                },
                options: {
                    configFile: '.eslint-node.json'
                }
            },
            tests: {
                files: {
                    src: [
                        /* include */
                        'test/**/*.js'

                        /* exclude */
                    ]
                },
                options: {
                    configFile: '.eslint-tests.json'
                }
            }
        },

        jsonlint: {
            files: {
                src: [
                    /* include */
                    '*.json',
                    '**/*.json',

                    /* exclude */
                    '!bower_components/**',
                    '!node_modules/**',
                    '!tmp/**',
                    '!vagrant/**'
                ]
            }
        },

        mochaTest: {
            test: {
                options: {
                    mocha: require('mocha'),
                    reporter: 'spec',
                    require: 'test/mocha-setup.js'
                },
                src: ['test/mocha/*.js', 'test/mocks/*.js']
            },
            coverage: {
                options: {
                    mocha: require('mocha'),
                    reporter: 'html-cov',
                    quiet: true,
                    captureFile: 'code-coverage.html'
                },
                src: ['test/mocha/*.js', 'test/mocks/*.js']
            }
        },

        postcss: {
            options: {
                map: true,
                processors: [
                    require('autoprefixer')({
                        browsers: [
                            'last 4 versions',
                            'ios >= 5.1',
                            'and_chr > 41',
                            'last 4 android versions',
                            'ie_mob >= 11'
                        ]
                    }),
                    require('cssnano')({
                        discardDuplicates: false,  // This speeds up cssnano by over 400%
                        safe: true,
                        sourcemap: true,
                        zindex: false
                    })
                ]
            },
            dist: {
                src: 'tmp/css/**/*.css'
            }
        },

        sass: {
            dist: {
                options: {
                    includePaths: ['src/sass/', 'src/assets/', 'bower_components/'],
                    maxBuffer: 1024 * 5000,
                    outputStyle: 'expanded',
                    sourceMap: true,
                    sourceMapRoot: '/css/<%= project.package.version %>',
                    useRenderSync: true
                },
                files: [{
                    expand: true,
                    cwd: 'src/sass',
                    dest: 'tmp/css',
                    src: ['**/*.scss'],
                    ext: '.css'
                }]
            }
        },

        cleanUp: {
            build: [
                'public/',
                'tmp/',
                'views/css/',
                'views/*index.js'
            ],
            bower: ['bower_components'],
            tmp: ['tmp/'],
            prepareForArtifact: [
                '.bower*',
                '.bundle',
                '.DS_Store',
                '.editorconfig',
                '.eslint-client.json',
                '.eslint-node.json',
                '.git',
                '.gitignore',
                '.jscsrc',
                '.jsdoc-conf.json',
                '.log.io',
                '.sass-cache',
                '.ssh',
                'bower*',
                'docs',
                'dump.rdb',
                'Gemfile*',
                'Gruntfile.js',
                'node_modules/blanket',
                'node_modules/bower',
                'node_modules/chai*',
                'node_modules/grunt*',
                'node_modules/jscs',
                'node_modules/jsdoc',
                'node_modules/load-grunt-tasks',
                'node_modules/lsd-cli',
                'node_modules/mocha',
                'node_modules/nock',
                'node_modules/node-inspector',
                'node_modules/selenium*',
                'node_modules/supertest',
                'node_modules/test-util',
                'node_modules/time-grunt',
                'README.md',
                'resources',
                'saucelabs.conf',
                'set-env-*.conf',
                'test',
                'tmp',
                'vagrant',
                'vendor'
            ]
        },

        watch: {
            sass: {
                files: ['**/*.scss'],
                tasks: ['watchSass']
            },
            js: {
                files: [
                    'src/**/*.js',
                    'bootstrap/*.js',
                    'views/**/*.dust'
                ],
                tasks: ['watchJs']
            }
        },

        combinejs: {
            manifest: 'tmp/src/js/manifest.json',
            options: {
                outSourceMap: true,
                sourceRoot: '',
                mangle: isMangled,
                compress: isCompressed,
                warnings: true,
                output: {
                    beautify: isBeautified
                }
            }
        },

        replace: {
            webpack: {
                options: {
                    patterns: [
                        {
                            match: 'WEBPACKASSETS',
                            replacement: '<%= project.webpackAssets %>'
                        }
                    ]
                },
                files: [
                    {
                        expand: true,
                        src: [
                            'tmp/views/*.js'
                        ]
                    }
                ]
            },
            webpackheader: {
                options: {
                    patterns: [
                        {
                            match: 'GLOBALHEADERNAME',
                            replacement: '<%= project.globalHeaderName %>'
                        }
                    ]
                },
                files: [
                    {
                        expand: true,
                        src: [
                            'tmp/views/*.js'
                        ]
                    }
                ]
            },
            all: {
                options: {
                    patterns: [
                        {
                            match: 'BASEVERSION',
                            replacement: '<%= project.package.version %>'
                        }
                    ]
                },
                files: [
                    {
                        expand: true,
                        src: [
                            'src/**/*.html',
                            'src/**/*.js',
                            'src/**/*.json',
                            'views/*index.js'
                        ],
                        dest: 'tmp/'
                    },
                    {
                        expand: true,
                        src: [
                            'bootstrap/*.js'
                        ],
                        dest: 'tmp/'
                    },
                    {
                        cwd: 'tmp/dust/src/js/',
                        expand: true,
                        src: [
                            'dust/*.js'
                        ],
                        dest: 'tmp/src/js/'
                    }
                ]
            }
        },

        scsslint: {
            files: [
                /* include */
                'src/sass/*.scss',
                'src/sass/**/*.scss',

                /* exclude */
                '!src/sass/pattern-library/loud/elements/_owl.scss',
                '!src/sass/**/vendor/*.scss',
                '!src/sass/pages/common/_nativo-social.scss'
            ],
            options: {
                bundleExec: true,
                config: '.scss-lint.yml',
                colorizeOutput: true,
                compact: false,
                maxBuffer: 1024 * 5000  // default is 1024 * 300
            }
        },

        shell: {
            create_dirs: {
                options: {stdout: true},
                command: [
                    'mkdir tmp ',
                    'mkdir public ',
                    'mkdir public/domestic ',
                    'mkdir public/international ',
                    'mkdir views/css '
                ].join('&&')
            },
            do_asset_links: {
                options: {stdout: true},
                command: [
                    'cd public/ ',
                    'rm -f .a .asset ',
                    'ln -s . .a ',
                    'ln -s . .asset'
                ].join('&&')
            },
            verify: {
                options: {stdout: true},
                command: 'build-scripts/verify_manifest.sh tmp/src/js/manifest.json public/<%= project.package.version %>/'
            }
        },

        sprite: {
            chrome: {
                src: 'src/assets/sprite/*.png',
                dest: 'src/assets/sprite-chrome.png',
                destCss: 'src/sass/pattern-library/quiet/utilities/_sprite-chrome.scss',
                imgPath: '/.a/<%= project.package.version %>/assets/sprite-chrome.png'
            },
            financial: {
                src: 'src/assets/sprite-financial/*.png',
                dest: 'src/assets/sprite-financial.png',
                destCss: 'src/sass/pattern-library/quiet/utilities/_sprite-financial.scss',
                imgPath: '/.a/<%= project.package.version %>/assets/sprite-financial.png',
                cssOpts: {
                    functions: false
                }
            }
        },

        copy: {
            main: {
                files: [{
                    expand: true,
                    cwd: 'src/',
                    src: [
                        'static/**/*'
                    ],
                    dest: 'public/<%= project.package.version %>/'
                }]
            },
            root: {
                expand: true,
                cwd: 'src/',
                src: [
                    'crossdomain.xml',
                    'robots.txt',
                    'loaderio-8bd6df9d4b4c29bf1f46db253041221c.txt',
                    'favicon.*'
                ],
                dest: 'public/'
            },
            css: {
                files: [{
                    expand: true,
                    cwd: 'tmp/css/',
                    src: [
                        '**/*.css',
                        '**/*.map'
                    ],
                    dest: 'views/css/'
                }]
            },
            dust: {
                files: [{
                    expand: true,
                    cwd: 'tmp/',
                    src: [
                        'views/*.js'
                    ],
                    dest: './'
                }]
            }
        }
    });

    /*
     * Build Steps for building Michonne.
     *
     * NOTE: When you see `/** /*` below, mentally remove the space.
     *
     * - cleanUp:build - Clean out the build directories
     *   - public/
     *   - tmp/
     *   - views/css/
     *   - views/*index.js
     *
     * - shell:create_dirs - Make the top-level public/ and tmp/ dirs.
     *
     * - Copy assets to public directory
     *   - copy:root - src/crossdomain.xml -> public/
     *   - copy:root - src/favicon.*       -> public/
     *   - bowercopy:fonts - bower_components/cnn-icons/dist/fonts/* -> public/fonts/icons/
     *   - bowercopy:fonts - bower_components/cnn-fonts/fonts/*      -> public/fonts/cnn/
     *   - copy:main - src/static/** /*    -> public/<%= version %>
     *
     * - Build dust files
     *   - dust:server    - views/** /*.dust  -> views/index.js
     *     - TODO: This includes the xml files that are in the next task
     *   - dust:xmlclient - views/xml/*.dust  -> views/xmlindex.js
     *   - dust:client    - specific files -> tmp/src/js/dust/dust-client.js
     *
     * - Compile SASS files
     *   - sass:dist   - src/sass -> tmp/css
     *
     * - Auto-prefix CSS files
     *   - postcss:dist - tmp/css/** /*.css (inline)
     *
     * - Minify CSS files
     *   - postcss:dist - tmp/css/** views/css/*.css (inline)
     *
     * - Copy processed CSS files to views
     *   - copy:css - tmp/css/** /*.css -> views/css/*.css
     *   - copy:css - tmp/css/** /*.map -> views/css/*.map
     *
     * - imagemin - Optimize image assets for production.
     *
     * - Replace @@ strings in files with configuration values
     *   - replace:all - src/** /*.[html|js|json] -> tmp/src/...
     *   - replace:all - views/*index.js -> tmp/views/
     *
     * - Copy Dust-generated javascript files back to views
     *   - copy:dust - tmp/views/*index.js -> views/
     *
     * - Combine javascript files in tmp and output to tmp/dist
     *   - combinejs - Reads the list of files to combine from
     *                 `tmp/src/js/manifest.json`
     *                 and writes them to public/%= version %>/js/
     *
     * - Verify .a and .asset links exist in each public directory
     *   - shell:do_asset_links - Make .a and .asset links in the public directory
     *
     * - Verify files for every environment
     *   - shell:verify - Run verify_manifest.sh shell script
     *
     * -------------------------------------------------------------------------
     *
     * - cleanUp:tmp - Clean the temp directory (/tmp)
     *
     *
     * A good way to check that this is correct is to do a build of all
     * environments and then run this command and examine the results:
     *
     *     $ grep -r breaking_news public/* | grep breaking-news.js
     */
    grunt.registerTask('build', 'Build to create an artifact for a Michonne environment', function () {
        grunt.log.oklns('Building Michonne...');
        grunt.task.run([
            'cleanUp:build',
            'shell:create_dirs',
            'sprite:chrome',
            'sprite:financial',
            'copy:root',
            'bowercopy:fonts',
            'dust:server',
            'dust:xmlclient',
            'dust:client',
            'sass:dist',
            'postcss:dist',
            'copy:css',
            'copy:main',
            'imagemin:dist',
            'replace:all',
            'combinejs',
            'webpackcomponents',
            'mapWebpackerBundles',
            'replace:webpack',
            'webpackheader',
            'setheader',
            'replace:webpackheader',
            'copy:dust',
            'shell:do_asset_links',
            'shell:verify',
            'cleanUp:tmp'
        ]);
    });

    grunt.registerTask('webpackcomponents', 'Runs webpack for components',
        function () {
            if (Promise) {
                grunt.task.run(['webpack:componentbundler']);
            }
        }
    );

    grunt.registerTask('webpackheader', 'Runs webpack for the header',
        function () {
            if (Promise) {
                grunt.task.run(['webpack:headerBundler']);
            }
        }
    );

    grunt.registerTask('setheader', 'Gets and Sets The Header Variable',
        function () {
            var headerAssets = grunt.config.data.headerData.assets,
                i = 0;
            for (i = 0; i < headerAssets.length; i++) {
                if (headerAssets[i].chunkNames[0] === 'header') {
                    grunt.config.set('project.globalHeaderName', headerAssets[i].name);
                    break;
                }
            }
        }
    );

    grunt.registerTask('mapWebpackerBundles', 'Create webpack mapping',
        function () {
            var webpackAssets = JSON.stringify(grunt.config.data.webpackData.assets),
                replaceChar = '\'';
            webpackAssets = webpackAssets.replace(/"/g, replaceChar);
            grunt.config.set('project.webpackAssets', webpackAssets);
        }
    );

    /* Build the JS and dust only */
    grunt.registerTask('build-single-js', 'Build JS only', function () {
        grunt.task.run([
            'dust:server',
            'dust:xmlclient',
            'dust:client',
            'replace:all',
            'combinejs',
            'webpackcomponents',
            'mapWebpackerBundles',
            'replace:webpack',
            'webpackheader',
            'setheader',
            'replace:webpackheader',
            'copy:dust'
        ]);
    });

    /* Build the CSS and JS only */
    grunt.registerTask('build-single-css', 'Build CSS only', function () {
        grunt.task.run([
            'sprite:chrome',
            'sprite:financial',
            'sass:dist',
            'postcss:dist',
            'copy:css',
            'imagemin:dist',
            'replace:all',
            'build-single-js'
        ]);
    });

    grunt.registerTask('default', ['build']);

    grunt.registerTask('test-codecheck', [
        'eslint',
        'jsonlint'
    ]);

    grunt.registerTask('test-unitcheck', [
        'mochaTest:test'
    ]);

    grunt.registerTask('test', [
        'test-codecheck',
        'test-unitcheck'
    ]);

    grunt.registerTask('watchJs', [
        'build-single-js'
    ]);

    grunt.registerTask('watchSass', [
        'build-single-css'
    ]);

    /* Rename clean task so that it is safe, and doesnt perform `prepareForArtifact` */
    grunt.renameTask('clean', 'cleanUp');
    grunt.registerTask('clean', ['cleanUp:build']);
};

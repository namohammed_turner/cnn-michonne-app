'use strict';

var db = {
    '/2013/01/01/foo.html': {title: 'Foo Article', body: 'Foo article example.'},
    '/2013/01/01/bar.html': {title: 'Bar Article', body: 'Bar article example.'},
    '/foo/index.html': {title: 'Foo Section', body: 'Foo section example.'},
    '/index.html': {title: 'HELLO WORLD', body: 'HELLO WORLD example.'}
};

module.exports = {
    getItem: function (uri, callback) {
        var content = db[uri] || {title: 'Hello World', body: 'Hello world example.'};
        callback({}, content);
    }
};

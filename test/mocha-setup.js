'use strict';

/*
 * This file is required by the mocha-test grunt task.  This should contain
 * things that need to be done for every test file
 */

var
    chai = require('chai');
//  sauceAccessKey = process.env.SAUCE_ACCESS_KEY || '',
//  sauceUsername = process.env.SAUCE_USERNAME || '',
//  useSauceLabs,
//  util = require('util');


/* Selenium and SauceLabs */
/* USE_SAUCE_LABS environment variable is a string */
// useSauceLabs = (process.env.USE_SAUCE_LABS === 'TRUE') ? true : false;

// global.capabilities = {
//     browserName: 'chrome',
//     version: useSauceLabs ? 31 : '*',
//     platform: useSauceLabs ? 'Windows 8.1' : null,
//     name: util.format('Michonne: %s', process.env.USER),
//     username: sauceUsername,
//     accessKey: sauceAccessKey
// };

// global.seleniumHost =  useSauceLabs ? 'http://ondemand.saucelabs.com:80/wd/hub' : 'http://localhost:4444/wd/hub';

global._$jscoverage = {};


/* include stack trace on test failure or not */
chai.config.includeStack = false;


/* required globals */
// global.webdriver = require('selenium-webdriver');
global.expect = chai.expect;
global.should = chai.should();
global.assert = chai.assert;


/* include json schema plugin - http://chaijs.com/plugins/chai-json-schema */
chai.use(require('chai-json-schema'));
chai.tv4.banUnknown = true;


/* setup blanket coverage */
require('blanket')({
    'data-cover-only': [
        '/routing/handlers/index.js',
        '/routing/handlers/pal.js'
    ],
    'data-cover-never': ['node_modules', 'test']
});

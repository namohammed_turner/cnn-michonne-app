/* global casper:true */
'use strict';

// integration tests for galleries

var testURL = 'http://localhost.cnn.com:8080/',
    selectors = {
        heroZone: '.zn-left-fluid-right-stack',
        zoneListHier: '.zn-left-fluid-right-stack .cn-list-hierarchical-xs',
        cardOrientation: '.cd--horizontal',
        videoContainer: '.media__video--demand',
        videoPlayer: '.media__video--demand iframe'
    };

(function homepageSuite(test) {
    casper.echo('Testing homepage-tests.js');

    casper.start(testURL, function testHomepageSuite() {
        test.assertExists(selectors.heroZone, 'hero zone found on page.');
        test.assertExists(selectors.zoneListHier, 'hierarchical container in hero zone.');
        test.assertExists(selectors.cardOrientation, 'card has orientation class attached.');
        if (casper.exists(selectors.videoContainer)) {
            casper.waitForSelector(selectors.videoPlayer, function () {
                test.assertExists(selectors.videoPlayer, 'video api has embedded an iframe on the page (within 10s).');
            }, function () {
                test.fail('`' + selectors.videoPlayer + '` failed to load within 10 seconds');
            }, 10000);
        } else {
            casper.echo('`' + selectors.videoContainer + '` does not exist. Instantiation of the player was not tested.', 'WARNING');
        }
    }).run(function runTestHomepageSuite() {
        test.done();
        test.renderResults(true);
        casper.exit();
    });
}(casper.test));

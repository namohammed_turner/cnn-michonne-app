/* global casper:true */
'use strict';

var testHtmlUrl = 'http://localhost.cnn.com:8080/data/ocs/section/index.html:hero/views/zones/body.html',
    testJsonUrl = 'http://localhost.cnn.com:8080/data/ocs/section/index.html:hero.json',
    nl = '\n';

casper.echo('Testing ocs.js');
casper.start();

/*
 *  Test HTML Route
 */

casper.then(function openHTMLUrl() {
    casper.open(testHtmlUrl);
});

casper.then(function testHTMLRoute() {
    this.echo('***** Test HTML Route *****');
    this.test.assertHttpStatus(200, 'HTTP status is 200');
    this.test.assert(this.visible('.zn .cd .cd__headline-text'), 'Card headline visibile');
});

/*
 *  Test JSON Route
 */

casper.then(function openJSONUrl() {
    casper.open(testJsonUrl);
});

casper.then(function testJSONRoute() {
    var jsonContent;

    this.echo(nl + '***** Test JSON Route *****');
    this.test.assertHttpStatus(200, 'HTTP status is 200');

    try {
        jsonContent = JSON.parse(this.getPageContent());
    } catch (err) {
        this.die(err.message);
    }

    this.test.assertType(jsonContent, 'object', 'Result is valid JSON');
    this.test.assert(typeof jsonContent.zoneContents[0].containerContents[0].cardContents.headlineText === 'string', 'Card headline exists');

});

casper.run(function casperRun() {
    this.test.done();
    this.test.renderResults(true);
    this.exit();
});

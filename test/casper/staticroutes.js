/* globals casper:true */
'use strict';
var liveVideoContentURL = 'http://localhost.cnn.com:8080/video/data/3.0/video/cvptve/cvpstream2/index.xml',
    vodVideoContentURL = 'http://localhost.cnn.com:8080/video/data/3.0/video/health/2011/09/27/natpkg-gid-youth.cnn/index.xml',
    badVODVideoContentURL = 'http://localhost.cnn.com:8080/video/data/3.0/video/health/1970/01/01/does-not-exist.cnn/index.xml',
    serverSideRedirectTestURL = 'http://localhost.cnn.com:8080/video/embed',
    nl = '\n';

casper.echo('Testing Static routes staticroutes.js');
casper.start();

/*
 *  Test HTML Route
 */

casper.then(function openLiveRoute() {
    casper.open(liveVideoContentURL);
});

casper.then(function testLiveRoute() {
    this.echo('***** Test Good Live Video XML Route *****');
    this.test.assertHttpStatus(200, 'HTTP status is 200');
    // this.test.
});

/*
 *  Test VOD Route
 */

casper.then(function openGoodVODRoute() {
    casper.open(vodVideoContentURL);
});

casper.then(function testGoodVODRoute() {
    // var jsonContent;
    this.echo(nl + '***** Test Good VOD Route *****');
    this.test.assertHttpStatus(200, 'HTTP status is 200');
});
casper.then(function open301Route() {
    casper.open(serverSideRedirectTestURL);
});
casper.then(function test301Route() {
    // var jsonContent;
    this.echo(nl + '***** Test 301 Route *****');
    this.test.assertHttpStatus(200, 'HTTP status is 200 so server side redirect succeeded');
});


casper.then(function openBADVODRoute() {
    casper.open(badVODVideoContentURL);
});
casper.then(function testBadVODRoute() {
    this.echo(nl + '***** Test Bad VOD Route *****');
    this.test.assertHttpStatus(404, 'HTTP status is 404');
});


casper.run(function casperRun() {
    this.test.done();
    this.test.renderResults(true);
    this.exit();
});

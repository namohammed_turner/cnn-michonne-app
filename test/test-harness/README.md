# CNN Template Test Harness

This directory is used for developers to access the test hanress and test
different elements of the toolkit.

This file is not the most optimized as it's using the [Twitter Bootstrap](bootstrap),
which is included inline, as well as inline JavaScript.

One day, when time permits, this should be cleaned up and transformed into a
more robust, functional test-harness. For now, it works.


## How to use

This file isn't meant to be accessible from an outside URL, so for now you'll
need to open the `index.html` file from your local machine and test it from
there.


### How does it work?

It works by sending query strings to the controller which will then parse those
values and generate a page.

As of know, it's only useful purpose is to test section fronts.


## How to develop/iterate/expand

To make changes to the test harness, you'll need to update the `index.html` file
as well as modify some of the login in `controllers/index.js`.


## Questions?

The original developer of the test harness, John Hayes, is no longer with
Turner. Contact Christian Hain with any questions.


[bootstrap]: http://getbootstrap.com/

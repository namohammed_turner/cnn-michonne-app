/* global capabilities, expect, seleniumHost, webdriver */
'use strict';

var
    driver = new webdriver.Builder().usingServer(seleniumHost).withCapabilities(capabilities).build(),
    url = 'http://localhost.next.cnn.com:8080';


describe('The cnn.com', function () {

    before(function (done) {
        driver.get(url).then(done);
    });


    describe('homepage', function () {
        it('is expected to have a title that contians CNN.com', function (done) {
            driver.getTitle().then(function (title) {
                expect(title).to.contain('CNN.com');
                done();
            });
        });
    });


    after(function (done) {
        driver.quit().then(done);
    });
});

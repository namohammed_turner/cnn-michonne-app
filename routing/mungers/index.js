'use strict';

/**
 * @module mungers
 */

const
    Clone = require('clone'),
    log = global.log,
    Url = require('url'),
    Useful = require('../../lib/useful');

var
    mungers;


/**
 * Function to do a recursive merge of a config object against the default config, but with no array concating.
 *
 * @function mergeConfig
 * @private
 * @param {object} obj - Original object to merge into
 * @param {object} source - Source object to merge from
 * @return {object} - Merged version of original object
 */
function mergeConfig(obj, source) {
    for (let prop in source) {
        if (typeof obj[prop] === 'undefined') {
            obj[prop] = source[prop];
        } else if (typeof obj[prop] !== 'undefined' && typeof obj[prop] !== 'object') {
            if (source[prop] === 'DELETE') {
                delete obj[prop];
            } else {
                obj[prop] = source[prop];
            }
        } else if (typeof obj[prop] === 'object' && !Array.isArray(obj[prop]) && !Array.isArray(source[prop])) {
            obj[prop] = mergeConfig(obj[prop], source[prop]);
        }
    }
    return obj;
}


/**
 * Function returns a configuration object based on the URI
 *
 * @function getConfig
 * @private
 * @param {object} data - The layout data object model
 */
function getConfig(data) {
    let uri = data.uri || '';

    if (uri.length !== 0 && global.config.overrides !== null && typeof global.config.overrides === 'object' &&
        global.config.overrides[uri] !== null && typeof global.config.overrides[uri] === 'object') {

        let defaultConfigClone,
            defaultRuntimeConfig,
            overridesConfigClone;

        try {
            overridesConfigClone = Clone(global.config.overrides[uri]);
            defaultConfigClone = Clone(global.config);
        } catch (e) {
            data.config = global.config || {};
        }

        if (defaultConfigClone && typeof defaultConfigClone.runtime === 'object') {
            defaultRuntimeConfig = defaultConfigClone.runtime;
        }

        if (overridesConfigClone !== null && typeof overridesConfigClone !== 'undefined' &&
            defaultRuntimeConfig !== null && typeof defaultRuntimeConfig !== 'undefined') {

            mergeConfig(defaultRuntimeConfig, overridesConfigClone);
            data.config = defaultConfigClone || global.config || {};
        } else {
            data.config = global.config || {};
        }
    } else {
        data.config = global.config || {};
    }

    // Process Zones config, if set for this page.
    data.zonesConfig = {};
    if (typeof data.pageType === 'string' && data.pageType.length !== 0 && uri.length !== 0 &&
        typeof global.config.zones[data.pageType] === 'object') {

        if (data.pageType === 'video') {
            // Set a static URI for video leaf pages
            uri = 'videos/index.html';
        } else {
            // Replace intl_index.html with index.html
            uri = uri.replace(/^(.*)\/intl_index\.html$/, '$1/index.html');
            if (uri.charAt(0) === '/') {
                // Remove leading slash from URI
                uri = uri.substring(1);
            }
        }

        if (typeof global.config.zones[data.pageType][uri] === 'object') {
            // If we have a zone config then use it
            if (log.do_debug === true) {
                log.debug(`Using zones config for pageType "${data.pageType}" with URI: ${uri}`);
            }
            data.zonesConfig = global.config.zones[data.pageType][uri];
        }
    }
}


/**
 * Function to alter the relative footer links
 * @function fixFooterLinks
 * @private
 * @param {object} navData The navigation data object
 * @returns {object} The navigation data object with altered URLs
 */
function fixFooterLinks(navData) {
    let matchUrl = /^(?:(?:(?:http\:|https\:)?\/\/)|\/)[^\/]+/i,
        matchRelativeUrl = /^\/([^\/].*)*$/;

    function doSubs(subs, doChild) {
        for (let i = 0; i < subs.length; i++) {
            if (subs[i].url && matchUrl.test(subs[i].url)) {
                if (matchRelativeUrl.test(subs[i].url)) {
                    subs[i].url = global.config.host.domain + subs[i].url;
                }
                if (doChild && typeof subs[i].subs === 'object' && Array.isArray(subs[i].subs) && subs[i].subs.length > 0) {
                    let childSubs = subs[i].subs;
                    for (let j = 0; j < childSubs.length; j++) {
                        if (childSubs[j].url && matchRelativeUrl.test(subs[i].url)) {
                            childSubs[j].url = global.config.host.domain + childSubs[j].url;
                        }
                    }
                }
            }
        }
    }

    if (typeof navData === 'object' && navData !== null) {
        if (typeof navData.buckets === 'object' && navData.buckets !== null &&
            typeof navData.buckets.subs === 'object' && Array.isArray(navData.buckets.subs) &&
            navData.buckets.subs.length !== 0) {

            // Do the bucket subs (and child subs)
            doSubs(navData.buckets.subs, true);
        }
        if (typeof navData.widgets === 'object' && navData.widgets !== null &&
            typeof navData.widgets.subs === 'object' && Array.isArray(navData.widgets.subs) &&
            navData.widgets.subs.length !== 0) {

            // Do the widget subs
            doSubs(navData.widgets.subs, false);
        }
        if (typeof navData.legal === 'object' && navData.legal !== null &&
            typeof navData.legal.subs === 'object' && Array.isArray(navData.legal.subs) &&
            navData.legal.subs.length !== 0) {

            // Do the legal subs
            doSubs(navData.legal.subs, false);
        }
    }
    return navData;
}


/**
 * Function to protocolize a files array (in place)
 *
 * @function protoVideoFiles
 * @private
 * @param {array} files - The files array to protocolize
 * @param {string} proto - The protocol to use
 */
function protoVideoFiles(files, proto) {
    let f;

    if (Array.isArray(files)) {
        for (f = 0; f < files.length; f++) {
            if (typeof files[f].fileUri === 'string') {
                files[f].fileUri = Useful.protocolize(files[f].fileUri, proto);
            }
        }
    }
}


/**
 * Function to protocolize an image array (in place)
 *
 * @function protoImages
 * @private
 * @param {array} images - The images array to protocolize
 * @param {string} proto - The protocol to use
 */
function protoImages(images, proto) {
    let i;

    if (Array.isArray(images)) {
        for (i = 0; i < images.length; i++) {
            if (typeof images[i].uri === 'string') {
                images[i].uri = Useful.protocolize(images[i].uri, proto);
            }
        }
    }
}


mungers = {
    /**
     * No-op munger, does nothing
     *
     * @function noop
     * @public
     * @param {object} data - The layout data object model
     * @returns {object} - data, un-munged
     */
    noop: function (data) {
        return data;
    },

    /**
     * Default munger, used for most things.
     *
     * @function default
     * @public
     * @param {object} data - The layout data object model
     * @returns {object} - object containing munged (in place) data object model, same as data
     */
    default: function (data) {
        getConfig(data);
        return data;
    },

    /**
     * Leaf page munger
     *
     * @function leaf
     * @public
     * @param {object} data - The layout data object model
     * @returns {object} - object containing munged (in place) data object model, same as data
     */
    leaf: function (data) {
        getConfig(data);
        return data;
    },

    /**
     * List page munger
     *
     * @function list
     * @public
     * @param {object} data - The layout data object model
     * @returns {object} - object containing munged (in place) data object model, same as data
     */
    list: function (data) {
        getConfig(data);
        return data;
    },

    /**
     * Section page munger
     *
     * @function section
     * @public
     * @param {object} data - The layout data object model
     * @returns {object} - object containing munged (in place) data object model, same as data
     */
    section: function (data) {
        getConfig(data);
        if (data.uri === '/index2.html' || data.uri === '/index4.html') {
            data.metaNoIndex = true;
        }
        return data;
    },

    /**
     * SubSection page munger
     *
     * @function subsection
     * @public
     * @param {object} data - The layout data object model
     * @returns {object} - object containing munged (in place) data object model, same as data
     */
    subsection: function (data) {
        getConfig(data);
        data.subsection = data.subsection || data.section;
        data.superSection = data.superSection || ((data.request && data.request.path) || data.uri).replace(/^\/([\w\-]+)\/.*$/, '$1').toLowerCase();
        return data;
    },

    /**
     * Show page munger
     *
     * @function show
     * @public
     * @param {object} data - The layout data object model
     * @returns {object} - object containing munged (in place) data object model, same as data
     */
    show: function (data) {
        getConfig(data);
        return data;
    },

    /**
     * Video Config response munger
     *
     * @function videoConfig
     * @public
     * @param {object} data - The data object model for converting to XML or returning as JSON
     * @returns {object} - object containing munged (in place) data object model
     */
    videoConfig: function (data) {
        if (typeof data === 'object' && data !== null) {
            let i,
                proto = (data.request && data.request.protocol) || 'http';

            if (Array.isArray(data.groupfiles) && data.groupfiles.length !== 0) {
                for (i = 0; i < data.groupfiles.length; i++) {
                    if (typeof data.groupfiles[i] === 'object') {
                        protoVideoFiles(data.groupfiles[i].files, proto);
                    }
                }
            }
            if (Array.isArray(data.files)) {
                protoVideoFiles(data.files, proto);
            }
            if (Array.isArray(data.images)) {
                protoImages(data.images, proto);
            }
            if (typeof data.closedCaptions === 'object' && data.closedCaptions !== null &&
                Array.isArray(data.closedCaptions.types) && data.closedCaptions.types.length !== 0) {

                for (i = 0; i < data.closedCaptions.types.length; i++) {
                    if (typeof data.closedCaptions.types[i] === 'object' &&
                        typeof data.closedCaptions.types[i].track === 'object' &&
                        typeof data.closedCaptions.types[i].track.url === 'string') {

                        data.closedCaptions.types[i].track.url = Useful.protocolize(data.closedCaptions.types[i].track.url, proto);
                    }
                }
            }
        }
        return data;
    },

    /**
     * OCS request response munger
     *
     * @function ocs
     * @public
     * @param {object} data - The layout data object model
     * @returns {object} - object containing munged data object, may be different than data
     */
    ocs: function (data) {
        getConfig(data);
        return data;
    },

    /**
     * OCS request response munger with subs
     *
     * @function ocs
     * @public
     * @param {object} subs - The substitutions object to use
     * @param {object} data - The layout data object model
     * @returns {object} - object containing munged data object, may be different than data
     */
    ocsSubs: function (subs, data) {
        let i,
            keys = Object.keys(subs);

        getConfig(data);
        for (i = 0; i < keys.length; i++) {
            data[keys[i]] = subs[keys[i]];
        }
        return data;
    },

    /**
     * ncomments request response munger
     *
     * @function ncomments
     * @public
     * @param {object} data - The layout data object model
     * @returns {object} - object containing munged data object, may be different than data
     */
    ncomments: function (data) {
        let articleId = null,
            base64Value = '',
            jsonData,
            result = {},
            siteId = '',
            url = '',
            socialCommenting = false;

        try {
            jsonData = JSON.parse(data);
        } catch (err) {
            jsonData = null;
        }

        if (jsonData !== null && typeof jsonData.livefyre === 'object') {
            siteId = global.config.runtime.social.livefyre.siteId;
            articleId = jsonData.livefyre.id;
            socialCommenting =  jsonData.socialCommenting;
            if (typeof articleId !== 'undefined' && articleId !== null && typeof siteId !== 'undefined' && siteId !== null) {
                base64Value = new Buffer(siteId + ':' + articleId).toString('base64');
                if (typeof base64Value !== 'undefined' && base64Value !== null && base64Value.length > 0) {
                    url = global.config.runtime.social.livefyre.mobileCommentsBaseUrl + base64Value + '.json';
                }
            }
        }

        if (url.length > 0) {
            result = {
                articleId: articleId,
                error: false,
                siteId: siteId,
                status: 'ok',
                statusCode: 200,
                url: url,
                socialCommenting: socialCommenting
            };
        } else {
            result = {
                error: true,
                status: 'error',
                statusCode: 500
            };
        }

        return result;
    },

    /**
     * Profile page munger
     *
     * @function profile
     * @public
     * @param {object} data - The layout data object model
     * @returns {object} - object containing munged (in place) data object model, same as data
     */
    profile: function (data) {
        getConfig(data);
        return data;
    },

    /**
     * Article comments munger
     *
     * @function comments
     * @public
     * @param {object} data - The layout data object model
     * @returns {object} - object containing munged (in place) data object model, same as data
     */
    comments: function (data) {
        getConfig(data);
        data.layout = 'comments';
        return data;
    },

    /**
     * Preview munger
     *
     * @function preview
     * @public
     * @param {object} data - The layout data object model
     * @returns {object} - object containing munged (in place) data object model, same as data
     */
    preview: function (data) {
        getConfig(data);
        data.request.isPreview = true;
        return data;
    },

    /**
     * Sponsored article page munger
     *
     * @function sponsorArticle
     * @public
     * @param {object} data - The layout data object model
     * @returns {object} - object containing munged (in place) data object model, same as data
     */
    sponsorArticle: function (data) {
        getConfig(data);
        data.ads = {};
        data.adType = 'Normal';
        data.analytics = {};
        data.description = 'Sponsor Content Article';
        data.edition = global.edition;
        data.editorialSource = 'Sponsor';
        data.layout = 'sponsor-article';
        data.nav = fixFooterLinks(data.nav);
        data.pageTheme = 'light';
        data.pageType = 'other:sponsor content article';
        data.sectionName = 'sponsor-article';
        data.status = 200;
        data.type = 'page';

        return data;
    },

    /**
     * Sponsored article webview page munger
     *
     * @function sponsorArticleWebview
     * @public
     * @param {object} data - The layout data object model
     * @returns {object} - object containing munged (in place) data object model, same as data
     */
    sponsorArticleWebview: function (data) {
        data = mungers.sponsorArticle(data);
        data.request.isWebview = true;

        return data;
    },

    /**
     * Sponsored interactive page munger
     *
     * @function sponsorInteractive
     * @public
     * @param {object} data - The layout data object model
     * @returns {object} - object containing munged (in place) data object model, same as data
     */
    sponsorInteractive: function (data) {
        getConfig(data);
        data.ads = {};
        data.adType = 'Normal';
        data.analytics = {};
        data.description = 'Sponsor Content Interactive';
        data.edition = global.edition;
        data.editorialSource = 'Sponsor';
        data.layout = 'sponsor-interactive';
        data.nav = fixFooterLinks(data.nav);
        data.pageTheme = 'light';
        data.pageType = 'other:sponsor content interactive';
        data.sectionName = 'sponsor-interactive';
        data.status = 200;
        data.type = 'page';

        return data;
    },

    /**
     * Sponsored summary page munger
     *
     * @function sponsorSummary
     * @public
     * @param {object} data - The layout data object model
     * @returns {object} - object containing munged (in place) data object model, same as data
     */
    sponsorSummary: function (data) {
        getConfig(data);
        data.ads = {};
        data.adType = 'Normal';
        data.analytics = {};
        data.description = 'Sponsor Content Summary';
        data.edition = global.edition;
        data.editorialSource = 'Nativo';
        data.layout = 'sponsor-summary';
        data.nav = fixFooterLinks(data.nav);
        data.pageTheme = 'dark';
        data.pageType = 'other:sponsor content summary';
        data.sectionName = 'sponsor-summary';
        data.status = 200;
        data.type = 'page';

        return data;
    },

    /**
     * Sponsored Go page munger
     *
     * @function sponsorGo
     * @public
     * @param {object} data - The layout data object model
     * @returns {object} - object containing munged (in place) data object model, same as data
     */
    sponsorGo: function (data) {
        getConfig(data);
        data.ads = {};
        data.adType = 'Normal';
        data.analytics = {};
        data.description = 'Sponsor Content Article';
        data.edition = global.edition;
        data.editorialSource = 'Nativo';
        data.pageTheme = 'light';
        data.pageType = 'other:sponsor content article';
        data.status = 200;
        data.type = 'page';

        return data;
    },

    /**
     * search page munger
     *
     * @function search
     * @public
     * @param {object} data - The layout data object model
     * @returns {object} - object containing munged (in place) data object model, same as data
     */
    search: function (data) {
        // data overrides
        getConfig(data);
        data.adType = 'Normal';
        data.description = 'Search';
        data.editorialSource = 'CNN';
        data.layout = 'search';
        data.metaKeywords = 'Search CNN - Videos, Pictures, and News - CNN.com';
        data.pageType = 'other:search results';
        data.seoTitle = 'Search CNN - Videos, Pictures, and News - CNN.com';
        data.title = '';

        return data;
    },

    /**
     * newsletter subscription page munger
     *
     * @function newsletterSub
     * @public
     * @param {object} data - The layout data object model
     * @returns {object} - object containing munged (in place) data object model, same as data
     */
    newsletterSub: function (data) {
        getConfig(data);
        data.ads = {};
        data.adType = 'Normal';
        data.analytics = {};
        data.description = 'CNN News, delivered. Select from our newsletters below and enter your email to subscribe';
        data.metaImage = 'http://i.cdn.cnn.com/cnn/.e1mo/img/3.0/expansion/logo/cnn-badge.png';
        data.edition = global.edition;
        data.layout = 'balanced';
        data.nav = fixFooterLinks(data.nav);
        data.pageTheme = 'light';
        data.pageType = 'subhub-page';
        data.pageLayout = 'email';
        data.sectionName = 'newsletters-hub';
        data.seoTitle = 'CNN Newsletters';
        data.status = 200;
        data.type = 'subhub'

        return data;
    },

    /**
     * Sets the model for the feedback form
     *
     * @function feedbackCnn
     * @param {object} data - The layout data object model (probably empty)
     * @returns {object} - object containing feedback object model data
     */
    feedbackCnn: function (data) {
        // data overrides
        getConfig(data);
        data.adType = 'Normal';
        data.description = 'Feedback';
        data.editorialSource = 'CNN';
        data.layout = 'forms';
        data.formLayout = 'feedback';
        data.headerLabel = 'CNN Feedback';
        data.metaKeywords = 'general, feedback, show, comments, newstips';
        data.pageType = 'feedback';
        data.seoTitle = 'Feedback CNN - Videos, Pictures, and News - CNN.com';

        return data;
    },

    /**
     * Sets the model for the AC360 feedback form
     *
     * @function feedbackAC360
     * @param {object} data - The layout data object model (probably empty)
     * @returns {object} - object containing AC360 feedback object model data
     */
    feedbackAC360: function (data) {
        // data overrides
        getConfig(data);
        data.adType = 'Normal';
        data.canonicalUrl = global.config.host.domain + '/feedback/';
        data.description = 'AC360 Feedback';
        data.editorialSource = 'CNN';
        data.formLayout = 'ac360';
        data.headerLabel = 'Anderson Cooper 360';
        data.layout = 'forms';
        data.metaKeywords = 'ac360, feedback, show, comments';
        data.pageType = 'feedback';
        data.seoTitle = 'AC360 Feedback CNN - Videos, Pictures, and News - CNN.com';

        return data;
    },

    /**
     * Sets the model for the student news form
     *
     * @function feedbackCnn10
     * @param {object} data - The layout data object model (probably empty)
     * @returns {object} - object containing CNN10 feedback object model data
     */
    feedbackCnn10: function (data) {
        // data overrides
        getConfig(data);
        data.adType = 'Normal';
        data.canonicalUrl = global.config.host.domain + '/feedback/';
        data.description = 'CNN10 Feedback';
        data.editorialSource = 'CNN';
        data.layout = 'forms';
        data.formLayout = 'cnn10';
        data.headerLabel = 'CNN10';
        data.metaKeywords = 'student, news, feedback, show, comments';
        data.pageType = 'feedback';
        data.seoTitle = 'CNN10 Feedback CNN - Videos, Pictures, and News - CNN.com';

        return data;
    },

    /**
     * Sets the model for the go feedback form
     *
     * @function feedbackGo
     * @param {object} data - The layout data object model (probably empty)
     * @returns {object} - object containing CNN Go feedback object model data
     */
    feedbackGo: function (data) {
        // data overrides
        getConfig(data);
        data.adType = 'Normal';
        data.canonicalUrl = global.config.host.domain + '/feedback/';
        data.description = 'CNN Go Feedback';
        data.editorialSource = 'CNN';
        data.layout = 'forms';
        data.formLayout = 'go';
        data.headerLabel = 'CNN Go';
        data.metaKeywords = 'cnn, go, feedback, show, comments';
        data.pageType = 'feedback';
        data.seoTitle = 'Go Feedback CNN - Videos, Pictures, and News - CNN.com';

        return data;
    },

    /**
     * Sets the model for the politics app feedback form
     *
     * @function feedbackPoliticsApp
     * @param {object} data - The layout data object model (probably empty)
     * @returns {object} - object containing Politics App feedback object model data
     */
    feedbackPoliticsApp: function (data) {
        // data overrides
        getConfig(data);
        data.adType = 'Normal';
        data.canonicalUrl = global.config.host.domain + '/feedback/';
        data.description = 'Feedback';
        data.editorialSource = 'CNN';
        data.layout = 'forms';
        data.formLayout = 'politicsapp';
        data.headerLabel = 'CNN Politics Feedback';
        data.metaKeywords = 'general, feedback, show, comments, newstips';
        data.pageType = 'feedback';
        data.seoTitle = 'Feedback CNN - Videos, Pictures, and News - CNN.com';

        return data;
    },

    /**
     * Create layout model for the login page
     *
     * @function login
     * @param {object} data - object model, if any
     * @returns {object} data The layout model
     */
    login: function (data) {
        // data overrides
        getConfig(data);
        data.adType = 'Normal';
        data.description = 'Login powered by gigya';
        data.editorialSource = 'CNN';
        data.layout = 'login';
        data.pageType = 'other:feedback results';
        data.pageContents = [{
            layout: 'body',
            name: 'body-text',
            theme: 'light',
            title: 'Log in',
            type: 'zone',
            zoneContents: [{
                containerContents: [{
                    cardContents: {},
                    contentType: 'social-gigya-raas',
                    type: 'tool'
                }],
                id: '',
                layout: 'list-xs',
                title: '',
                type: 'container',
                url: ''
            }]
        }];
        data.pageType = 'article';
        data.sectionDisplayName = 'Login';
        data.seoTitle = 'CNN - Sign In';

        return data;
    },

    /**
     * Create layout model for the mycnn page
     *
     * @function mycnn
     * @param {object} data - object model, if any
     * @returns {object} data The layout model
     */
    mycnn: function (data) {
        // This is a temporary solution that has
        // been loged as a ticket. The mycnn page
        // will be using sectionMunger

        getConfig(data);
        data.layout = 'no-rail';
        data.headerLabel = 'MyCNN';

        data.pageContents = [{
            id: 'mycnn',
            layout: '30-70',
            name: 'avatar-links',
            theme: 'light',
            title: 'avatarlinks',
            type: 'zone',
            zoneContents: [{
                id: 'navigation',
                layout: 'stack-medium-vertical',
                title: '',
                type: 'container',
                url: '',
                containerContents: [{
                    cardContents: {},
                    contentType: 'mycnn-avatar',
                    type: 'tool'
                }, {
                    cardContents: {},
                    contentType: 'mycnn-left-rail',
                    type: 'tool'
                }]
            }, {
                id: 'content',
                layout: 'list-xs',
                title: '',
                type: 'container',
                url: '',
                containerContents: [{
                    type: 'tool',
                    contentType: 'mycnn-body-contents',
                    cardContents: {}
                }]
            }]
        }, {
            id: 'scripts',
            layout: 'body',
            name: 'mycnn-javascript',
            theme: 'light',
            title: 'mycnnjavascript',
            type: 'zone',
            zoneContents: [{
                containerContents: [{
                    type: 'tool',
                    contentType: 'mycnn',
                    cardContents: {}
                }],
                id: '',
                layout: 'list-xs',
                title: '',
                type: 'container',
                url: ''
            }]
        }];

        return data;
    },

    /**
     * Create layout model for error pages
     *
     * @function error
     * @param {object} data - object model, if any
     * @returns {object} data The layout model
     */
    error: function (data) {
        getConfig(data);
        return data;
    },

    //
    // TEST HARNESS FUNCTIONS
    //

    /**
     * Return lorem ipsum text of a given length (up to 2000 characters)
     *
     * @param {integer} charCount - number of characters to include
     * @returns {string} a string of lorem ipsum text
     */
    testHarnessGetLipsum: function (charCount) {
        let retval = '',
            lipsum = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam venenatis fringilla elit vel feugiat. Aliquam nec viverra nibh, eu euismod neque. Suspendisse ultrices auctor felis et luctus. Aenean vitae erat non eros pellentesque congue. Sed pulvinar sollicitudin arcu quis pulvinar. Vivamus hendrerit <a href>consectetur felis</a>, sit amet molestie dui gravida et. Cras cursus dignissim dolor, blandit vulputate nisi viverra sit amet. Sed non lacus sed enim adipiscing euismod et vitae quam. Aenean pretium pulvinar felis in aliquam. Donec urna elit, accumsan sit amet dignissim in, pharetra fermentum dolor. Vivamus eget dolor eros. Praesent velit massa, cursus ut magna vel, viverra vestibulum ligula. Integer scelerisque sollicitudin nunc, iaculis laoreet tortor hendrerit et. Vestibulum eu erat semper mauris pretium eleifend a ac massa. Nullam imperdiet nisl aliquam velit blandit, non ultricies nulla scelerisque. Quisque interdum id odio sit amet dapibus. Maecenas at justo auctor, adipiscing massa ut, tempor libero. Nulla laoreet blandit ultricies. Praesent nec bibendum lorem. Mauris porta neque sed ipsum vestibulum, eget accumsan mauris vulputate. Nam dapibus magna nunc, quis laoreet leo aliquet quis. Duis a mattis libero. Nunc viverra dui et tellus sollicitudin accumsan. Donec pharetra arcu ut imperdiet dictum. Fusce purus diam, pretium non orci vitae, feugiat ultrices enim. In quis pharetra sapien. Nunc eget lectus odio. Vivamus luctus ante non dolor fermentum tristique. Curabitur felis libero, facilisis eu pretium a, porta ac erat. Nullam consectetur nibh id nunc feugiat porttitor. Aenean eros quam, aliquam nec nisl et, auctor luctus erat. Donec laoreet euismod mauris vitae ultricies. Nullam sagittis ullamcorper arcu id commodo. Morbi justo dui, gravida tempor ante vitae, tempus mattis leo. Pellentesque faucibus augue at ornare molestie. Proin suscipit, purus vitae lacinia facilisis, sem elit blandit dui, ut convallis orci velit ac magna. Nullam ultrices ultrices dui, non cras amet.';

        if (charCount > -1) {
            retval = lipsum.substr(0, charCount);
        }

        return retval;
    },

    /**
     * Create a fake card for use in the test harness page.
     *
     * @param {string} pHeadline - the card's headline.
     * @param {boolean} randomDesc - generate random description text
     * @param {object} pCardBanner - formatted banner text object
     * @param {boolean} pCardBranding - brand this card or not
     * @param {string} pLinkKicker - 'full' or 'partial'
     * @param {object} pCardStatus - card status object
     * @param {string} pCta - card cta string
     * @returns {object} Card object
     */
    testHarnessCreateCard: function (pHeadline, randomDesc, pCardBanner, pCardBranding, pLinkKicker, pCardStatus, pCta) {
        let maxDescriptionChars = 1000,      // most chars possible in a random description
            maxParagraphs = 3,               // most paragraphs possible in a random description
            numStaticDescriptionChars = 100, // how many chars to use when non-random
            numRandomDescriptionChars = Math.random() * maxDescriptionChars, // randomly chosen number of chars
            numParagraphs = Math.floor(Math.random() * maxParagraphs + 1),   // randomly chosen number of paragraphs
            descriptionArray = [],
            cardContents = {},
            lipsum = testHarnessGetLipsum(numRandomDescriptionChars),
            staticLipsum = testHarnessGetLipsum(numStaticDescriptionChars);

        // Random description text from lorem ipsum
        if (randomDesc) {
            for (let i = 0; i < numParagraphs; i += 1) {
                descriptionArray[i] = lipsum.substr(numRandomDescriptionChars / numParagraphs * i, numRandomDescriptionChars / numParagraphs);
            }
        } else {
            descriptionArray = [
                staticLipsum,
                staticLipsum,
                staticLipsum
            ];
        }

        // Build cardContents
        cardContents = {
            auxiliaryText: 'Auxiliary Text Goes Right Here',
            bannerText: pCardBanner.cardBannerText,
            contentType: '',
            cta: pCta,
            descriptionText: descriptionArray,
            headlinePostText: 'Post WOOL',
            headlinePreText: 'Pre WOOL',
            headlineText: pHeadline,
            kickerText: (() => {
                if (pLinkKicker === 'full') {
                    return '<a>Kicker Text</a>';
                } else if (pLinkKicker === 'partial') {
                    return 'Kicker with <a>Partial</a> link';
                } else {
                    return 'Kicker Text';
                }
            })(),
            iconType: '',
            iconImageUrl: '',
            maximizedBanner: pCardBanner.cardBannerMaximized,
            media: {
                contentType: 'image',
                type: 'element',
                elementContents: {
                    imageAlt: '16x9 image',
                    imageUrl: global.config.cdn.chrome + '/.element/img/4.0/test-harness-files/card/card-image-16-9.jpg',
                    label: 'Image label',
                    caption: 'I\'m So Meta, Even This <a href="//xkcd.com/917/">Acronym</a>'
                }
            },
            overMediaText: '#OverMediaText',
            targetType: '',
            timestampDisplay: '2013-02-15 4:20am',
            timestampUtc: '',
            type: 'card',
            url: '//' + global.config.hostnames.mainHost
        };

        if (pCardBranding) {
            cardContents.brandingImageUrl = global.config.cdn.chrome + '/.element/img/4.0/test-harness-files/card/card-branding-image.jpg';
            cardContents.brandingTextHead = 'Branding Text Head';
            cardContents.brandingTextSub = 'Branding Text Sub';
        }

        if (pCardStatus) {
            cardContents.statusText = pCardStatus.text;
            cardContents.statusColor = pCardStatus.color;
        }

        return {
            type: 'card',
            contentType: '',
            cardContents: cardContents
        };
    },

    /**
     * Create an empty container for use in the test harness page.
     *
     * @param {string} pName  - the container type, e.g. "list-xs"
     * @param {string} pId    - the container's ID, which is probably useless
     * @param {string} pTitle - the container title
     * @param {boolean} pLink - Whether container titles should link or not.
     * @returns {object} Container object
     */
    testHarnessCreateContainer: function (pName, pId, pTitle, pLink) {
        return {
            id: pId,
            layout: pName,
            title: pTitle,
            type: 'container',
            url: (pLink === true) ? '#' : null,
            containerContents: []
        };
    },

    /**
     * Create an empty zone for use in the test harness page.
     *
     * @param {string} pId - the zone type, e.g. "staggered"
     * @param {string} pTheme - the zone theme, e.g. "dark"
     * @param {object} pTransparent - Values for zone transparency
     * @param {string} pLabel - the zone label
     * @param {string} pBackgroundUrl - the background image url
     * @param {string} pLogoUrl - the zone logo url
     * @returns {object} Zone object
     */
    testHarnessCreateZone: function (pId, pTheme, pTransparent, pLabel, pBackgroundUrl, pLogoUrl) {
        let
            isZoneTransparent = function () {
                if (pTransparent.index < pTransparent.zoneTransparentCount) {
                    return pTransparent.isTransparent;
                } else {
                    return false;
                }
            },
            zoneObj = {
                layout: pId,
                id: pId,
                theme: pTheme,
                transparent: isZoneTransparent(),
                label: pLabel,
                type: 'zone',
                url: '',
                maximizedBanner: true,
                bannerText: pId + ' zone banner',
                background: {},
                logo: {
                    contentType: 'image',
                    type: 'element',
                    elementContents: {
                        imageUrl: pLogoUrl,
                        cuts: {
                            medium: {
                                height: '259',
                                width: '460',
                                type: 'png',
                                uri: pLogoUrl + '#mediumcut'
                            }
                        }
                    }
                },
                zoneContents: []
            };

        if (pBackgroundUrl) {
            zoneObj.background.image = {
                contentType: 'image',
                type: 'element',
                elementContents: {
                    caption: '',
                    imageAlt: '',
                    imageUrl: pBackgroundUrl,
                    label: '',
                    galleryTitle: '',
                    responsiveImage: true,
                    cuts: {
                        small: {
                            height: '124',
                            width: '220',
                            type: 'jpg',
                            uri: pBackgroundUrl + '#smallcut'
                        },
                        medium: {
                            height: '259',
                            width: '460',
                            type: 'jpg',
                            uri: pBackgroundUrl + '#mediumcut'
                        },
                        large: {
                            height: '619',
                            width: '1100',
                            type: 'jpg',
                            uri: pBackgroundUrl + '#largecut'
                        },
                        full16x9: {
                            height: '900',
                            width: '1600',
                            type: 'jpg',
                            uri: pBackgroundUrl + '#fullcut'
                        }
                    }
                }
            };
        }

        return zoneObj;
    },

    testHarnessCreatePage: function (pageLayout, pageBackgroundUrl, pageTheme, bgColorStart, bgColorEnd) {
        // TODO: make this fetch the Page object from the real homepage instead of building it,
        // then override the parts we know about like title, sectionName, etc.
        let
            pageObj = {
                type: 'page',
                byline: '',
                canonicalUrl: '',
                description: 'Test Harness',
                firstPublishDate: '',
                lastPublishDate: '',
                layout: pageLayout,
                location: '',
                metaKeywords: 'CNN - Videos, Pictures, and News - CNN.com',
                pageTheme: pageTheme,
                pageType: 'article',
                sectionName: 'test harness',
                seoTitle: 'CNN - Videos, Pictures, and News - CNN.com',
                editorialSource: '',
                title: 'Page Title',
                pageContents: [[], [], [], [], []],
                ads: {},
                bucketName: 'news',
                sectionDisplayName: 'Test Harness',
                background: {},
                analytics: {
                    author: '',
                    publishDate: '2014-03-13T15:16:07Z',
                    pageBranding: '',
                    branding_content_page: 'default',
                    branding_content_zone: ['default'],
                    branding_content_container: ['default'],
                    branding_content_card: []
                },
                nav: {
                    legal: {
                        label: 'legal',
                        url: '',
                        subs: [{
                            label: 'Terms of service',
                            url: '/terms',
                            name: 'terms-of-service'
                        }, {
                            label: 'Privacy guidelines',
                            url: '/privacy',
                            name: 'privacy-guidelines'
                        }, {
                            label: 'AdChoices',
                            url: '/I-JUST-DONT-KNOW-THE-URL',
                            name: 'adchoices'
                        }, {
                            label: 'Advertise with us',
                            url: '/services/advertise/main.html',
                            name: 'advertise-with-us'
                        }, {
                            label: 'About us',
                            url: '/about',
                            name: 'about-us'
                        }, {
                            label: 'Contact us',
                            url: '/feedback',
                            name: 'contact-us'
                        }, {
                            label: 'Work for us',
                            url: 'http://www.turner.com/careers',
                            name: 'work-for-us'
                        }, {
                            label: 'Help',
                            url: '/help',
                            name: 'help'
                        }, {
                            label: 'Transcripts',
                            url: '/transcripts',
                            name: 'transcripts'
                        }]
                    },
                    widgets: {
                        label: 'widgets',
                        url: '',
                        subs: [{
                            label: 'Videos',
                            url: '/videos',
                            name: 'videos'
                        }, {
                            label: 'Photos',
                            url: '/photos',
                            name: 'photos'
                        }, {
                            label: 'Charts & Maps',
                            url: '/interactives',
                            name: 'charts-maps'
                        }, {
                            label: 'Games',
                            url: '//games.cnn.com',
                            name: 'games'
                        }]
                    },
                    buckets: {
                        label: 'buckets',
                        url: '',
                        subs: [{
                            label: 'News',
                            url: '/news',
                            subs: [{
                                id: '/us/index.html',
                                label: 'U.S.',
                                url: '/us',
                                name: 'us',
                                subs: [{
                                    label: 'Energy & Environment',
                                    url: '//cnn.com/specials/road-to-rio',
                                    name: 'energy-environment'
                                }, {
                                    label: 'Extreme Weather',
                                    url: '//ireport.cnn.com/topics/1303',
                                    name: 'extreme-weather'
                                }]
                            }, {
                                id: '/world/index.html',
                                label: 'World',
                                url: '/world',
                                name: 'world',
                                subs: [{
                                    label: 'Regions (US)',
                                    url: '//cnn.com/trends/#us',
                                    name: 'regions-us'
                                }, {
                                    label: 'War & Conflict',
                                    url: '//cnn.com/specials/war.casualties',
                                    name: 'war-conflict'
                                }, {
                                    label: 'Culture & Society',
                                    url: '//connecttheworld.blogs.cnn.com',
                                    name: 'culture-society'
                                }, {
                                    label: 'Global Politics',
                                    url: '//internationaldesk.blogs.cnn.com',
                                    name: 'global-politics'
                                }, {
                                    label: 'Regions (World)',
                                    url: '//cnn.com/trends/#world',
                                    name: 'regions-world'
                                }, {
                                    label: 'Fareed Zakaria',
                                    url: '//cnn.com/CNN/anchors_reporters/zakaria.fareed.html',
                                    name: 'fareed-zakaria'
                                }, {
                                    label: 'Christiane Amanpour',
                                    url: '//cnn.com/CNN/anchors_reporters/amanpour.christiane.html',
                                    name: 'christiane-amanpour'
                                }]
                            }, {
                                id: '/politics/index.html',
                                label: 'Politics',
                                url: '/politics',
                                name: 'politics',
                                subs: [{
                                    label: 'White House',
                                    url: '//politicalticker.blogs.cnn.com/category/president-obama',
                                    name: 'white-house'
                                }, {
                                    label: 'Congress',
                                    url: '//politicalticker.blogs.cnn.com/category/congress',
                                    name: 'congress'
                                }, {
                                    label: 'National Security',
                                    url: '//security.blogs.cnn.com',
                                    name: 'national-security'
                                }, {
                                    label: 'CNN Polls',
                                    url: '//cnn.com/politics/pollingcenter',
                                    name: 'cnn-polls'
                                }]
                            }, {
                                id: '/tech/index.html',
                                label: 'Tech',
                                url: '/tech',
                                name: 'tech',
                                subs: [{
                                    label: 'Mobile',
                                    url: '//cnn.com/specials/tech/our-mobile-society/index.html',
                                    name: 'mobile'
                                }, {
                                    label: 'Gadgets',
                                    url: '//cnn.com/tech/gaming.gadgets/archive/index.html',
                                    name: 'gadgets'
                                }, {
                                    label: 'Gaming',
                                    url: '//cnn.com/tech/gaming.gadgets/archive/index.html',
                                    name: 'gaming'
                                }, {
                                    label: 'Social Media',
                                    url: '//edition.cnn.com/tech/specials/one-web/index.html',
                                    name: 'social-media'
                                }, {
                                    label: 'Innovation',
                                    url: '//whatsnext.blogs.cnn.com/',
                                    name: 'innovation'
                                }]
                            }, {
                                id: '/health/index.html',
                                label: 'Health',
                                url: '/health',
                                name: 'health',
                                subs: [{
                                    label: 'Diet & Fitness',
                                    url: '//thechart.blogs.cnn.com/category/fitness,weight-loss,obesity,nutrition,healthy-eating,food-safety,exercise',
                                    name: 'diet-fitness'
                                }, {
                                    label: 'Mental Health',
                                    url: '//thechart.blogs.cnn.com/category/mental-health,brain,addiction,adhd,psychology,depression,alzheimers',
                                    name: 'mental-health'
                                }, {
                                    label: 'Alternative Medicine',
                                    url: '//upwave.com/',
                                    name: 'alternative-medicine'
                                }]
                            }, {
                                id: '/showbiz/index.html',
                                label: 'Entertainment',
                                url: '/showbiz',
                                name: 'showbiz',
                                subs: [{
                                    label: 'Movies',
                                    url: '//marquee.blogs.cnn.com/category/movies/summer-movies',
                                    name: 'movies'
                                }, {
                                    label: 'TV',
                                    url: '//marquee.blogs.cnn.com/category/television/tv-recaps',
                                    name: 'tv'
                                }, {
                                    label: 'Music',
                                    url: '//marquee.blogs.cnn.com/category/music',
                                    name: 'music'
                                }, {
                                    label: 'Celebrities',
                                    url: '//marquee.blogs.cnn.com/category/celebrities',
                                    name: 'celebrities'
                                }, {
                                    label: 'Pop Culture',
                                    url: '//hlntv.com/topics/omg',
                                    name: 'pop-culture'
                                }]
                            }, {
                                id: '/living/index.html',
                                label: 'Living',
                                url: '/living',
                                name: 'living',
                                subs: [{
                                    label: 'Parenting',
                                    url: '//cnn.com/specials/living/cnn-parents',
                                    name: 'parenting'
                                }, {
                                    label: 'Relationships',
                                    url: '/living/relationships',
                                    name: 'relationships'
                                }, {
                                    label: 'Style',
                                    url: '//edition.cnn.com/cnni/Programs/icon/',
                                    name: 'style'
                                }, {
                                    label: 'Identity',
                                    url: '//inamerica.blogs.cnn.com/category/documentaries/',
                                    name: 'identity'
                                }, {
                                    label: 'Eatocracy',
                                    url: '//eatocracy.cnn.com',
                                    name: 'eatocracy'
                                }, {
                                    label: 'Belief',
                                    url: '//religion.blogs.cnn.com',
                                    name: 'belief'
                                }, {
                                    label: 'Kelly Wallace',
                                    url: '//cnn.com/specials/living/kelly-wallace/index.html',
                                    name: 'kelly-wallace'
                                }]
                            }, {
                                id: '/travel/index.html',
                                label: 'Travel',
                                url: '/travel',
                                name: 'travel',
                                subs: [{
                                    label: 'Destinations',
                                    url: '//cnn.com/specials/2010/trips/archive/',
                                    name: 'destinations'
                                }, {
                                    label: 'Transportation',
                                    url: '//travel.cnn.com/aviation',
                                    name: 'transportation'
                                }, {
                                    label: 'Seasonal Travel',
                                    url: '//cnn.com/specials/2010/winter.getaways/',
                                    name: 'seasonal-travel'
                                }, {
                                    label: 'Activities',
                                    url: '//travel.cnn.com/activities',
                                    name: 'activities'
                                }, {
                                    label: 'Amenities',
                                    url: '//travel.cnn.com/hotels',
                                    name: 'amenities'
                                }]
                            }, {
                                label: 'Sports',
                                url: '//bleacherreport.com',
                                name: 'sports'
                            }, {
                                label: 'Money',
                                url: '//money.cnn.com',
                                name: 'money'
                            }, {
                                label: 'iReport',
                                url: '//ireport.com',
                                name: 'ireport'
                            }],
                            name: 'news',
                            selected: 'selected'
                        }, {
                            label: 'TV',
                            url: '/tv',
                            subs: [{
                                label: 'Watch CNN',
                                url: '/tv/watch/cnn',
                                name: 'watch-cnn'
                            }, {
                                label: 'Schedule',
                                url: '/tv/schedule/cnn',
                                subs: [{
                                    label: 'CNN',
                                    url: '/tv/schedule/cnn',
                                    name: 'cnn'
                                }, {
                                    label: 'HLN',
                                    url: '//hlntv.com/schedule',
                                    name: 'hln'
                                }, {
                                    label: 'CNNi',
                                    url: '//edition.cnn.com/tv/schedule',
                                    name: 'cnni'
                                }],
                                name: 'schedule'
                            }, {
                                label: 'CNN Films',
                                url: '//cnnpressroom.blogs.cnn.com/category/cnn/cnn-special-programming-documentaries/cnn-films',
                                name: 'cnn-films'
                            }, {
                                label: 'New Day',
                                url: '/shows/new-day',
                                name: 'new-day'
                            }, {
                                label: 'Anthony Bourdain - Parts Unknown',
                                url: '/shows/anthony-bourdain-parts-unknown',
                                name: 'anthony-bourdain---parts-unknown'
                            }, {
                                label: 'TV A-Z',
                                url: '/tv/shows',
                                name: 'tv-a-z'
                            }, {
                                label: 'All Videos',
                                url: '/videos',
                                name: 'all-videos'
                            }],
                            name: 'tv'
                        }, {
                            label: 'Voices',
                            url: '/voices',
                            subs: [{
                                label: 'Contributors',
                                url: '/voices/contributors',
                                name: 'contributors'
                            }],
                            name: 'voices'
                        }, {
                            label: 'This is CNN',
                            url: '/this-is-cnn',
                            subs: [{
                                label: 'CNN Personalities',
                                url: '/profiles',
                                name: 'cnn-personalities'
                            }, {
                                label: 'CNN10',
                                url: '/studentnews',
                                name: 'cnn10'
                            }, {
                                label: 'Impact Your World',
                                url: '/specials/impact-your-world',
                                name: 'impact-your-world'
                            }, {
                                label: 'CNN Heroes',
                                url: '/SPECIALS/cnn.heroes/',
                                name: 'cnn-heroes'
                            }, {
                                label: 'Freedom Project',
                                url: '//thecnnfreedomproject.blogs.cnn.com',
                                name: 'freedom-project'
                            }, {
                                label: 'All Projects',
                                url: '/all-projects',
                                name: 'all-projects'
                            }],
                            name: 'this-is-cnn'
                        }]
                    }
                }
            };

        if (pageBackgroundUrl) {
            pageObj.background = {
                image: {
                    imageUrl: pageBackgroundUrl,
                    cuts: {
                        small: {
                            height: 259,
                            width: 460,
                            type: 'jpg',
                            uri: pageBackgroundUrl + '#smallcut'
                        }
                    }
                }
            };
        }

        if (bgColorStart) {
            pageObj.background.startColor = bgColorStart;
        }
        if (bgColorEnd) {
            pageObj.background.endColor = bgColorEnd;
        }

        return pageObj;
    },

    /**
     * Does a 301 Redirect on legacy profile paths
     * Example URL:
     *    /CNN/anchors_reporters/ripley.will.html -> /profiles/will-ripley
     *
     * @param {object} req Request Object
     * @param {object} res Response Object
     */
    handleRedirectLegacyProfile: function (req, res) {
        try {
            let destPath = '/specials/profiles',
                nameArray,
                origPath = req.path || '';

            origPath = origPath.replace(/^\/(CNN|CNNI)\/anchors_reporters(\/)?/, '').replace(/\.html$/i, '');
            if (origPath.length > 0 && origPath !== 'index' && origPath.search(/[^\w\-\.]/) < 0) {
                nameArray = origPath.toLowerCase().split('.');
                if (nameArray.length > 1) {
                    // Shift the last name to the end
                    nameArray.push(nameArray.shift());
                }
                destPath = '/profiles/' + nameArray.join('-') + '-profile';
            }
            if (log.do_debug === true) {
                log.debug('Redirecting legacy profile path "' + req.path + '" to "' + destPath + '"');
            }
            res.redirect(301, destPath);
        } catch (error) {
            let errorMsg = `Internal Server Error handling request for "${req.path}"`;

            log.error(error);
            res.writeHead(500, errorMsg, {'content-type': 'text/plain'});
            res.end(errorMsg);
        }
    },

    /**
     * Does a 301 Redirect on legacy Video Experience URLs to their michonne equivalent.
     * Example URLS:
     * /video/#/video/us/2013/12/24/dnt-soldier-surprise-proposal.wtsp will redirect to the video landing page
     * and
     * /video/data/2.0/video/bestoftv/2013/12/21/gnr-welcome-to-the-jungle.cnn.html  will redirect to  /videos/bestoftv/2013/12/21/gnr-welcome-to-the-jungle.cnn
     *
     * TODO: /videos/#/video/us/2013/12/24/dnt-soldier-surprise-proposal.wtsp will eventually redirect to landing where client-side script will see the fragment and redirect to the leaf page
     *
     * @param {object} req Request Object
     * @param {object} res Response Object
     */
    handleRedirectLegacyVideoUrl: function (req, res) {
        try {
            let urlParts = Url.parse(req.url, true),
                redUri = urlParts.pathname;

            redUri = redUri.replace(/^(\/video\/data\/\d{1}\.\d{1})/, '');
            redUri = redUri.replace(/^\/video(\/index\.html)?$/, '/videos');
            redUri = redUri.replace(/^\/video\//, '/videos/');
            redUri = redUri.replace(/\/index.xml$/, '');
            redUri = redUri.replace(/\.html$/, '');
            redUri = redUri + urlParts.search;
            res.redirect(301, redUri);
        } catch (error) {
            let errorMsg = `Internal Server Error handling request for "${req.path}"`;

            log.error(error);
            res.writeHead(500, errorMsg, {'content-type': 'text/plain'});
            res.end(errorMsg);
        }
    },

    /**
     * Munger for the template test harness, which sends a huge fake
     * template model to the template.
     *
     * @param {object} options - Query string options for the test harness
     * @property {string|array} zones - list of zones to display
     * @property {string|array} zoneThemes - list of zone themes to display
     * @property {string|array} containers - list of containers to display
     * @property {integer} cardsPerContainer - number of cards to output per container
     * @property {integer} containersPerZone - number of containers to output per zone
     * @property {string} pageLayout - which page layout to use (no-rail, right-rail, etc.)
     * @property {string} pagebg - URL for page background image
     * @property {string} zonebg - URL for zone background images
     * @property {string} zonelogo - URL for zone logo images
     * @property {string} pagebgcolorbegin - hex representation of color for top of page gradient ("#rrggbb")
     * @property {string} pagebgcolorend - hex representation of color for bottom of page gradient ("#rrggbb")
     * @property {boolean} randomizeCardContent - randomize card lorem ipsum
     *
     * @returns {object} Page object
     */
    templateTestHarness: function (options) {
        let
            makeArray = function (arr) {
                let val = [];

                if (arr) {
                    if (Array.isArray(arr)) {
                        val = arr;
                    } else {
                        val = [arr];
                    }
                }

                return val;
            },
            zonesList = makeArray((options && options.zones) || ['left-fluid']),
            zoneThemesList = makeArray((options && options.zoneThemes) || ['light']),
            containersList = makeArray((options && options.containers) || ['list-hierarchical-xs']),
            containerLink = (options && options.linkContainerTitle === 'true') ? true : false,
            cardsPerContainer = (options && options.cardsPerContainer) || 5,
            cardBanner = {
                cardBannerText: (options && options.cardBannerText) ? options.cardBannerText : null,
                cardBannerMaximized: (options && options.cardBannerMaximized) || false
            },
            cardDisplayBranding = (options && options.displayCardBranding === 'true') || false,
            cardLinkKicker = (options && options.linkKickerText) || 'partial',
            cardStatusText = null,
            cardCta = (options && options.cardCta) || '',
            containersPerZone = (options && options.containersPerZone) || 5,
            pageLayout = (options && options.pageLayout) || 'no-rail',
            pageTheme = (options && options.pageTheme) || '',
            zoneTransparent = (options && typeof options.zoneTransparent !== 'undefined') ? true : false,
            zoneTransparentCount = (options && options.zoneTransparentCount) || 1,
            pageBackground = (options && options.pagebg),
            zoneBackground = (options && options.zonebg),
            zoneLogo = (options && options.zonelogo),
            bgColorStart = (options && options.pagebgcolorbegin),
            bgColorEnd = (options && options.pagebgcolorend),
            randomizeCardContent = (options && options.randomizeCardContent) || false,
            toolkit = {
                // arrays of all items to test (zones, themes, containers)
                zones: zonesList,
                zoneThemes: zoneThemesList,
                containers: containersList
            },
            containerCount = 0,
            data = testHarnessCreatePage(pageLayout, pageBackground, pageTheme, bgColorStart, bgColorEnd);

        if (options && options.cardStatusText) {
            cardStatusText = {
                text: options.cardStatusText,
                color: (options.cardStatusColor) || 'breaking'
            };
        }

        // loop over all zone types and make a page object to return
        for (let i = 0, zoneId; i < toolkit.zones.length; i += 1) {
            zoneId = toolkit.zones[i];

            // for each zone, theme it with each theme
            for (let j = 0, zoneThemeName; j < toolkit.zoneThemes.length; j += 1) {
                zoneThemeName = toolkit.zoneThemes[j];

                // for each themed zone, create a temp container for each container type
                for (let k = 0, containerName, tempZone; k < toolkit.containers.length; k += 1) {
                    containerName = toolkit.containers[k];

                    // the zone for this particular zone+container+theme
                    tempZone = testHarnessCreateZone(zoneId, zoneThemeName, {index: k, zoneTransparentCount: zoneTransparentCount, isTransparent: zoneTransparent}, 'Zone [' + zoneId + '] Theme "' + zoneThemeName + '"', zoneBackground, zoneLogo);

                    for (let l = 0, tempContainer; l < containersPerZone; l += 1, containerCount += 1) {
                        // make a container
                        tempContainer = testHarnessCreateContainer(containerName, zoneId + '-' + containerName + '-' + zoneThemeName, 'CONTAINER "' + containerName + '" #' + containerCount, containerLink);

                        // fill this container with lorem ipsum cards
                        for (let m = 0; m < cardsPerContainer; m += 1) {
                            tempContainer.containerContents.push(testHarnessCreateCard(containerName + ' Container #' + containerCount + ' in ' + zoneId + ' Zone, Card ' + m + ' Headline', randomizeCardContent, cardBanner, cardDisplayBranding, cardLinkKicker, cardStatusText, cardCta));
                        }

                        tempZone.zoneContents.push(tempContainer);
                    }

                    // push this themed zone full of copies of this container into the page in each priority spot. the page is huge!
                    data.pageContents[0].push(tempZone);
                    data.pageContents[1].push(tempZone);
                    data.pageContents[2].push(tempZone);
                    /* We don't push anything into the "comments" region in index 3 */
                    data.pageContents[4].push(tempZone);
                }
            }
        }

        getConfig(data);
        return data;
    }

};

module.exports = mungers;


'use strict';

/**
 * @module routing/handlers
 *
 * Top-level context/namespace for all the Route-O-Matic route handlers.
 */

const
    cdnCacheControl = process.env.CDN_CACHE_CONTROL || 'max-age=2592000', // Default cache time is 30 days
    cdnSurrogateControl = process.env.CDN_SURROGATE_CONTROL || 'max-age=2592000, stale-while-revalidate=60, stale-if-error=86400',
    Clone = require('clone'),
    cssDir = 'views/css/',
    fs = require('fs'),
    palHandlers = require('./pal.js'),
    Path = require('path'),
    rootDir = Path.normalize(__dirname + '/../../public/'),
    Url = require('url'),
    Useful = require('../../lib/useful');

var
    legacyContentURL = Url.parse(process.env.LEGACY_CONTENT_SERVICE_URL),
    staticFileURL = Url.parse(process.env.STATIC_FILE_URL);


// Prep the local stuff
legacyContentURL.proto = (legacyContentURL.protocol ? legacyContentURL.protocol.slice(0, -1) : null);
staticFileURL.proto = (staticFileURL.protocol ? staticFileURL.protocol.slice(0, -1) : null);


module.exports = {
    /**
     * Function to handle CSS requests
     *
     * @function handleCss
     * @public
     * @param {object} req - Request object (RouteOMatic)
     * @param {object} route - Route object for matching route rule
     * @param {object} args - Arguments from route matching
     */
    doCss: function (req, route, args) {
        try {
            let matches = args.key.match(/^\/css\/(\d+\.[\w\-\.]+)\/(\w.+)\.css(\.map)?$/);

            req.isXhr = true;  // Short error messages for CSS files
            if (matches !== null && matches[1] === global.config.baseVersion && typeof matches[2] === 'string') {
                let cssName = matches[2] + '.css' + (typeof matches[3] === 'string' ? matches[3] : '');

                if (global.css.hasOwnProperty(cssName)) {
                    // Take care of browser and CDN cache settings.
                    req.type = 'css';
                    req.headers = {
                        'cache-control': cdnCacheControl,
                        'surrogate-control': cdnSurrogateControl,
                        'content-length': Buffer.byteLength(global.css[cssName], 'utf8'),
                        'content-type': 'text/css; charset=UTF-8'
                    };
                    req.send(200, global.css[cssName]);
                    return;
                }
            }

            // Not one of our CSS files, so 404 it.
            if (req.log.do_debug === true) {
                req.log.debug(`Path not found handling CSS request for "${req.path}"`);
            }
            req.error(404);
        } catch (error) {
            req.log.error(`Internal Server Error handling CSS request for "${req.path}"`);
            req.error(500);
        }
    },


    /**
     * Route handler to send a local file.
     *
     * @memberof routing
     * @public
     * @param {object} req - Request object (RouteOMatic)
     * @param {object} route - Route object for matching route rule
     * @param {object} args - Arguments from route matching
     */
    doFile: function (req, route, args) {
        if (req.path.indexOf('..') !== -1) {
            req.error(404);
        } else {
            try {
                req.sendFile(req.path.slice(1), {
                    cacheControl: false, // Use the header settings
                    dotfiles: 'allow',
                    headers: {
                        'cache-control': cdnCacheControl,
                        'surrogate-control': cdnSurrogateControl
                    },
                    lastModified: false,
                    root: rootDir
                });
            } catch (err) {
                req.error(500);
            }
        }
    },


    /**
     * Route handler for static content
     *
     * @memberof routing
     * @public
     * @param {object} req - Request object (RouteOMatic)
     * @param {object} route - Route object for matching route rule
     * @param {object} args - Arguments from route matching
     */
    doStatic: function (req, route, args) {
        if (req.log.do_debug === true) {
            req.log.debug(`doStatic: ${req.href}`);
        }

        req.proxy({
            proxy: {
                auth: staticFileURL.auth,
                hash: staticFileURL.hash,
                hostname: staticFileURL.hostname,
                path: req.path,
                pathMatch: (route.options && route.options.proxy && route.options.proxy.pathMatch) || null,
                pathReplace: (route.options && route.options.proxy && route.options.proxy.pathReplace) || null,
                port: staticFileURL.port,
                proto: staticFileURL.proto,
                query: req.query
            }
        });
    },


    /**
     * Route handler for legacy content
     *
     * @memberof routing
     * @public
     * @param {object} req - Request object (RouteOMatic)
     * @param {object} route - Route object for matching route rule
     * @param {object} args - Arguments from route matching
     */
    doLegacy: function (req, route, args) {
        if (req.log.do_debug === true) {
            req.log.debug(`doLegacy: ${req.href}`);
        }
        req.proxy({
            proxy: {
                auth: legacyContentURL.auth,
                hash: legacyContentURL.hash,
                hostname: legacyContentURL.hostname,
                path: req.path,
                pathMatch: (route.options && route.options.proxy && route.options.proxy.pathMatch) || null,
                pathReplace: (route.options && route.options.proxy && route.options.proxy.pathReplace) || null,
                port: legacyContentURL.port,
                proto: legacyContentURL.proto,
                query: req.query
            }
        });
    },


    /**
     * Handler for general service proxy routes
     *
     * @memberof routing
     * @public
     * @param {object} req - Request object (RouteOMatic)
     * @param {object} route - Route object for matching route rule
     * @param {object} args - Arguments from route matching
     */
    doServiceProxy: function (req, route, args) {
        let proxyOpts = Clone(route.options.proxy);

        if (req.log.do_debug === true) {
            req.log.debug(`doServiceProxy: ${req.href}`);
        }

        if (Array.isArray(proxyOpts.params) && proxyOpts.params.length !== 0) {
            // Trim off the leading key, split the rest for parameters
            let paramsLen = proxyOpts.params.length,
                parts = req.path.slice(args.key.length).split('/'),
                qstr = '';

            for (let i = 0; i < parts.length && i < paramsLen; i++) {
                qstr += (i === 0 ? '' : '&') + proxyOpts.params[i] + '=' + parts[i];
            }
            if (qstr.length !== 0) {
                if (typeof proxyOpts.query === 'string' && proxyOpts.query.length !== 0) {
                    proxyOpts.query += '&' + qstr;
                } else {
                    proxyOpts.query = qstr;
                }
            }
        } else if (typeof req.query === 'string' && req.query.length !== 0) {
            if (typeof proxyOpts.query === 'string' && proxyOpts.query.length !== 0) {
                proxyOpts.query += '&' + req.query;
            } else {
                proxyOpts.query = req.query;
            }
        }

        req.proxy({proxy: proxyOpts});
    },


    /**
     * Route handler for blog content
     *
     * @memberof routing
     * @public
     * @param {object} req - Request object (RouteOMatic)
     * @param {object} route - Route object for matching route rule
     * @param {object} args - Arguments from route matching
     */
    doBlog: function (req, route, args) {
        let blog,
            blogHost = req.hostname.replace(/\.blogs\..+$/, ''),
            nextFunc = palHandlers.doSection;

        if (req.log.do_debug === true) {
            req.log.debug(`doBlog: ${req.href}`);
        }

        if (typeof global.config.blogs[blogHost] !== 'object') {
            // No matching blog config, so send to home page
            req.redirect(302, global.config.host.domain);
            return;
        }

        blog = global.config.blogs[blogHost];
        if (req.path.match(/^\/\d+\/\d+\/\d+\//)) {
            req.path = req.path.replace(/^\/(\/\d+\/\d+\/\d+\/)(.+)$/, '$1' + blog.section + '/$2');
            req.url = req.url.replace(/^\/(\/\d+\/\d+\/\d+\/)(.+)$/, '$1' + blog.section + '/$2');
            req.normalizedPath = req.path.toLowerCase();
            nextFunc = palHandlers.doArticle;
        } else {
            req.path = blog.parent || '/' + blog.section;
            req.url = req.path;
            switch (blog.modelType) {
            case 'article':
                nextFunc = palHandlers.doArticle;
                break;
            case 'list':
                nextFunc = palHandlers.doList;
                break;
            case 'show':
                nextFunc = palHandlers.doShow;
                break;
            case 'special':
                nextFunc = palHandlers.doSpecial;
                break;
            // default:  // Unnecessary, but good to keep commented-out for clarity's sake
            //    nextFunc = palHandlers.doSection;
            }
        }
        nextFunc(req, route, args);
    },

    /**
     * Route handler for google site verification
     *
     * @memberof routing
     * @public
     * @param {object} req - Request object (RouteOMatic)
     * @param {object} route - Route object for matching route rule
     * @param {object} args - Arguments from route matching
     */
    doGoogleVerify: function (req, route, args) {
        let code = args.key.replace(/^\/(google\w+)\..*$/, '$1');

        if (req.log.do_debug === true) {
            req.log.debug(`doGoogleVerify: ${req.href}`);
        }

        if (global.config.runtime.googleSiteVerification[code] === true) {
            req.send(200, `google-site-verification: ${code}.html`);
            return;
        }

        // Did not find it.
        req.isXhr = true;
        req.error(404);
    },

    // Export the "pal" namespace
    pal: palHandlers
};


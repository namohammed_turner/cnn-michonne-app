'use strict';

/**
 * @module routing/handlers/pal
 *
 * Route handlers specific to content from PAL (generally from CMS3 via IBIS)
 */

const
    Clone = require('clone'),
    defaultTemplate = 'views/pages/common/chrome-frame',
    dust = require('../../views/index.js'),
    errorCacheControl = process.env.ERROR_CACHE_CONTROL || 'max-age=5',  // Default cache time is 5 seconds
    Http = require('http'),
    mungers = require('../mungers'),
    noCacheHeaders = {
        'Surrogate-Control': 'max-age=2, stale-while-revalidate=5, stale-if-error=10',
        'Cache-Control': 'no-cache'
    },
    ocsTypesToRender = {
        html: true,
        izl: true,  // Must be set, cannot be extrapolated
        json: false,
        jsonp: false,
        xml: true
    },
    palModels = {
        adsection: {izlSupport: false},
        article:   {izlSupport: false},
        container: {izlSupport: true},
        empty:     {izlSupport: false},
        list:      {izlSupport: true},
        playlist:  {izlSupport: true},
        profile:   {izlSupport: false},
        section:   {izlSupport: true},
        show:      {izlSupport: true},
        special:   {izlSupport: true},
        video:     {izlSupport: true},
    },
    Path = require('path'),
    previewMethods = {
        article: 'getArticle',
        coveragecontainer: 'getArticle',
        default: 'getArticle',
        empty: 'getEmpty',
        gallery: 'getArticle',
        list: 'getList',
        profile: 'getProfile',
        section: 'getSection',
        show: 'getShow',
        special: 'getArticle',
        noorm: 'getNoorm',
        video: 'getArticle',
        'video-collection': 'getArticle'
    },
    previewMungers = {
        default: mungers.preview
    },
    previewTemplates = {
        coveragecontainer: 'views/pages/common/preview-frame',
        default: 'views/pages/common/chrome-frame'
    },
    theSourceHost = global.config.hostnames.theSourceHost.replace(/:\d+$/, ''),
    theSourcePort = parseInt(global.config.hostnames.theSourceHost.replace(/^.+(:(\d+))$/, '$1'), 10) || 80,
    Url = require('url'),
    palUrl = Url.parse(global.palUrl),
    staticFileURL = Url.parse(process.env.STATIC_FILE_URL),
    Useful = require('../../lib/useful'),
    xmlDust = require('../../views/xmlindex.js');


var palHandlers;


// Prep the local stuff
staticFileURL.proto = (staticFileURL.protocol ? staticFileURL.protocol.slice(0, -1) : null);


/**
 * Parse the options.munger setting, if any, and adjust it
 * accordingly.
 *
 * It can be set to a function, which is simply returned.
 * It can be set to the name (string) of a munger which is then
 * looked for in the "mungers" namespace.
 * It can be set to an object, which is used to "munge" the
 * the data.
 * It can be unset or invalid, in which case it is set to the
 * default that is passed-in.
 *
 * @function mungeMunger
 * @private
 * @param {object} options - Request normalizedPath to extract type from
 * @param {function} defMunger - Default munger to use if nothing is set
 * @returns {object} - Munged options value
 */
function mungeMunger(options, defMunger) {
    let o = (options && Clone(options)) || {};

    switch (typeof o.munger) {
    case 'string':
        o.munger = (o.munger.length !== 0 && typeof mungers[o.munger] === 'function') ? mungers[o.munger] : defMunger;
        break;
    case 'object':
        let m = o.munger;
        o.munger = function (a, munger, data) {
            munger(data);
            return Object.assign(data, a);
        }.bind(this, m, defMunger);
        break;
    case 'function':
        break;
    default:
        o.munger = defMunger;
    }
    return o;
}


/**
 * Get OCS request "type" from "normalizedPath" (or key)
 *
 * @function getOcsRequestType
 * @private
 * @param {string} path - Request path to extract type from
 * @returns {string} - Type string: html (default), json, jsonp, or xml
 */
function getOcsRequestType(path) {
    let d = path.lastIndexOf('.'),
        t = (d !== -1 && path.slice(d + 1)) || 'html';

    return (t in ocsTypesToRender) ? t : 'html';
}


/**
 * Get OCS key from request path.
 * Generally means adding /index.html when it isn't there.
 *
 * @function getOcsKey
 * @private
 * @param {string} path - Request path to check
 * @param {string} wid - Workspace ID, if any
 * @returns {string} - String containing the OCS key to use
 */
function getOcsKey(path, wid) {
    let k = path,
        d = k.lastIndexOf('.');

    if (d === -1 || !(k.slice(d + 1) in ocsTypesToRender)) {
        k += (k.charAt(k.length - 1) === '/' ? '' : '/') + 'index.html';
    }
    if (typeof wid === 'string' && wid.length !== 0) {
        k = 'workspace=' + wid + k;
    }
    return k;
}


/**
 * Proxy a request to the Static server
 * Used here primarily for legacy video content.
 *
 * @private
 * @param {object} req - The RomRequest object to use to send this
 */
function doStaticProxy(req) {
    req.proxy({
        proxy: {
            auth: staticFileURL.auth,
            hash: staticFileURL.hash,
            hostname: staticFileURL.hostname,
            path: req.path,
            port: staticFileURL.port,
            proto: staticFileURL.proto,
            query: req.query
        }
    });
}


/**
 * Send the response to the client
 *
 * @private
 * @param {object} req - The RomRequest object to use to send this
 * @param {object} options - Options object
 * @param {Error} error - If things failed, the resulting error object
 * @param {string} result - The data to send
 */
function sendResponse(req, options, error, result) {
    let status,
        type = options && options.type;

    if (error) {
        if (global.metrics !== null) {
            global.metrics.count('template.render.errors');
        }
        status = Number(error.statusCode) || 500;
        req.isXhr = (type === 'izl' || type === 'json' || type === 'jsonp');
        if (req.headers && req.headers['surrogate-control']) {
            delete req.headers['surrogate-control'];
        }
        req.log.error(`Dust returned an error: ${error.status} - ${error.message}`);
        req.error(status);
        return;
    }

    if (options.sKeyHeader) {
        req.headers = req.headers || {};
        req.headers['surrogate-key'] = options.sKeyHeader;
    }

    switch (options.type) {
    case 'izl':
        if (global.metrics !== null) {
            global.metrics.count('template.render.izl');
        }
        req.json(200, {
            izlData: options.izlData,
            html: result
        });
        break;
    case 'json':
        if (global.metrics !== null) {
            global.metrics.count('template.render.json');
        }
        if (typeof result === 'string' || Buffer.isBuffer(result)) {
            // Already JSON encoded
            req.type = 'json';
            req.send(200, result);
        } else {
            req.json(200, result);
        }
        break;
    case 'jsonp':
        req.queryParams.callback = options.jsonpCallback;
        if (typeof result === 'string' || Buffer.isBuffer(result)) {
            // Already JSON encoded... This is ugly.
            try {
                req.jsonp(200, JSON.parse(result), 'callback');
                if (global.metrics !== null) {
                    global.metrics.count('template.render.jsonp');
                }
            } catch(jsonErr) {
                if (global.metrics !== null) {
                    global.metrics.count('template.render.errors');
                }
                req.log.error(`Failed parsing JSON: ${jsonErr.message}`);
                req.error(500);
            }
        } else {
            if (global.metrics !== null) {
                global.metrics.count('template.render.jsonp');
            }
            req.jsonp(200, result, 'callback');
        }
        break;
    case 'xml':
    default:
        req.type = (options.type === 'xml') ? 'xml' : 'html';
        if (global.metrics !== null) {
            global.metrics.count(`template.render.${req.type}`);
        }
        req.send(200, result);
    }
}


/**
 * Process data with Dust and send response
 *
 * @private
 * @param {object} req - RomRequest object to send with
 * @param {object} data - Object model to use process templates with
 * @param {object} [options] - Optional options object
 * @param {function} [options.munger] - Optional munger to munge content
 * @param {string} [options.template] - Optional non-default template to use
 * @param {string} [options.type] - Optional request type (html, json, jsonp, izl)
 */
function processObjectModel(req, data, options) {
    let isSslRequest = (req.proto === 'https'),
        type;

    data = data || {};
    options = options || {};

    // Get type, even if not set
    if (!(type = options.type) || !(type in ocsTypesToRender)) {
        type = getOcsRequestType(req.path);
    }

    // Add request details to object model
    data.request = data.request || {
        domainUrl: (isSslRequest === true ? 'https:' + global.config.host.domain + global.config.host.sslPort : 'http:' + global.config.host.domain + global.config.host.port),
        edition: global.edition,
        environment: global.environment,
        hostname: req.hostname,
        httpVersion: req.serverRequest.httpVersion,
        isPreview: false,
        isSSL: isSslRequest,
        model: options.model || '',
        path: req.path,
        port: req.port,
        protocol: req.proto,
        type: type,
        url: req.url
    };
    if (typeof options.section === 'string') {
        data.request.section = options.section;
        data.request.subsection = options.subsection;
    }

    // Munge the object model
    data = (typeof options.munger === 'function') ? options.munger(data) : mungers.default(data);

    // Deal with surrogate keys
    if (Array.isArray(data.surrogateKeys) && data.surrogateKeys.length !== 0) {
        options.sKeyHeader = data.surrogateKeys.join(' ');
    }

    // Do the IZL stuff
    if (type === 'izl' && typeof data.izlData !== 'undefined') {
        options.izlData = data.izlData;
    } else {
        options.izlData = null;
    }

    try {
        // Render and send
        switch (type) {
        case 'json':
        case 'jsonp':
            // Send JSON response
            delete data.config;
            delete data.request;
            sendResponse(req, options, null, data);
            break;
        case 'xml':
            xmlDust.render(options.template || defaultTemplate, data, sendResponse.bind(this, req, options));
            break;
        default:
            dust.render(options.template || defaultTemplate, data, sendResponse.bind(this, req, options));
        }
    } catch (err) {
        // Note that this exception is usually caught *after* sendResponse is called.
        // May need to deal with that.  The default error handler usually can.
        if (global.metrics !== null) {
            global.metrics.count('template.render.errors');
        }
        req.log.error(`Exception thrown while rendering: ${err.message}`);
        req.error(500);
    }
}


/**
 * Handle the response from PAL
 *
 * Note that this should be bound to provide the req and options objects.
 *
 * @private
 * @param {object} req - RomRequest object this response is for
 * @param {object} options - Options to pass through to processObjectModel
 * @param {Error} error - If request resulted in an error, this is it
 * @param {object|string} content - The content of a successful response
 */
function handlePalResponse(req, options, error, content) {
    // If tracking metrics, finish up
    if (options._palStartTime !== null) {
        if (global.countersOnly === false) {
            global.metrics.histogram('pal.requests.response.time', Useful.getHrDiff(options._palStartTime));
        }
        options._palStartTime = null;
    }

    if (error || !content || !req) {
        let code = (error && error.status) || 500;

        if (!req) {
            if (global.metrics !== null) {
                global.metrics.count('pal.requests.response.failures');
            }
            throw new Error('No request object passed to handlePalResponse!');
        } else if (global.metrics !== null) {
            global.metrics.count(`pal.requests.response.${code}.count`);
        }
        if (error) {
            if (req.log.do_debug === true) {
                req.log.debug(`Error from PAL: ${error.message}`);
            }
        } else {
            req.log.error(`Empty response from PAL!`);
        }
        req.error(code);
        return;
    }

    if (req.log.do_debug === true) {
        req.log.debug('Content has a type of "' + typeof content + '" in handlePalResponse');
    }
    if (typeof content === 'string') {
        try {
            content = JSON.parse(content);
        } catch (parseError) {
            req.log.error(`Problem parsing response from PAL: ${parseError.message}`);
            req.error(500, parseError.message);
            return;
        }
    }
    // Make sure we do not override the status returned...
    if (content.status) {
        content.status = Number(content.status);
    } else {
        // ...unless we didn't get one, which implies success
        content.status = 200;
    }
    if (global.metrics !== null) {
        global.metrics.count(`pal.requests.response.${content.status}.count`);
    }
    if (content.status !== 200) {
        if (global.metrics !== null) {
            global.metrics.count('pal.requests.response.invalids');
        }
        if (req.log.do_debug === true) {
            req.log.debug(`PAL response error, content status ${content.status}`);
        }
        req.error(content.status || 500, 'Error in OCS response.');
        return;
    }

    // Process the results
    processObjectModel(req, content, options);
}


/**
 * Call PAL and potentially fallback to the STATIC server on 404
 *
 * @private
 * @param {object} req - The RomRequest object to use to send this
 * @param {object} options - Options object
 * @param {string} options.template - Template to use
 * @param {string} options.type - Type to use ('xml', 'json', 'html')
 * @param {function} [options.munger] - Optional munger to use
 * @param {boolean} [palOnly] - Stop with PAL if false (default), failover to static if true
 */
function palWithStaticFallback(req, options, palOnly) {
    let palPath = global.palUrl + req.url,
        results = null;

    if (req.log.do_debug === true) {
        req.log.debug(`Checking PAL for ${palPath}`);
    }
    if (!options.template || !options.type) {
        req.log.error('Missing required template or type option!  Aborting.');
        req.error(500);
    }

    // Record PAL request start time
    options._palStartTime = (global.metrics !== null) ? process.hrtime() : null;

    Http.get(palPath, (res) => {
        // If tracking metrics, finish up
        if (options._palStartTime !== null) {
            if (global.countersOnly === false) {
                global.metrics.histogram('pal.requests.response.time', Useful.getHrDiff(options._palStartTime));
            }
            if (palOnly === true || res.statusCode !== 404) {
                global.metrics.count(`pal.requests.response.${res.statusCode}.count`);
            }
            options._palStartTime = null;
        }

        // We only got here if we got an error or we got a 404 response
        if (res.statusCode === 404) {
            if (req.log.do_debug === true) {
                req.log.debug(`PAL returned 404 for request ${palPath}`);
            }
            res.resume();
            if (palOnly === true) {
                // Nothing else to do, send a 404 back to the client
                req.error(404);
            } else {
                // Try the fallback route
                if (palOnly === true || res.statusCode !== 404) {
                    global.metrics.count(`pal.requests.response.fallbacks`);
                }
                doStaticProxy(req);
            }
        } else if (res.statusCode !== 200) {
            // PAL returned an error, so end things here.
            if (req.log.do_warn === true) {
                req.log.warn(`PAL returned error code ${res.statusCode} for request ${palPath}`);
            }
            req.error(res.statusCode);
            res.resume();
        } else {
            // PAL returned data, so get it
            let data = '';

            res.on('data', (chunk) => data += chunk);
            res.on('end', () => {
                try {
                    results = JSON.parse(data);
                } catch (e) {
                    if (req.log.do_warn === true) {
                        req.log.warn(`PAL returned invalid/incomplete data for request ${palPath}`);
                    }
                    req.error(500);
                    return;
                }
                if (!results || results.status && Number(results.status) !== 200) {
                    if (!results || Number(results.status) !== 404) {
                        // PAL really returned an error, so end things here.
                        if (req.log.do_warn === true) {
                            req.log.warn(`PAL returned an error or bad results for request ${palPath}`);
                        }
                        req.error((results && results.status) || 500);
                    } else {
                        if (req.log.do_debug === true) {
                            req.log.debug(`PAL returned 404 for request ${palPath}`);
                        }
                        if (palOnly === true) {
                            // Nothing else to do, send a 404 back to the client
                            req.error(404);
                        } else {
                            // Try the fallback route
                            doStaticProxy(req);
                        }
                    }
                } else {
                    // Valid PAL response, munge the data and run it through Dust
                    let isSslRequest = (req.proto === 'https');

                    results.request = results.request || {
                        domainUrl: (isSslRequest === true ? 'https:' + global.config.host.domain + global.config.host.sslPort : 'http:' + global.config.host.domain + global.config.host.port),
                        edition: global.edition,
                        environment: global.environment,
                        hostname: req.host,
                        httpVersion: req.serverRequest.httpVersion,
                        isPreview: false,
                        isSSL: isSslRequest,
                        path: req.path,
                        port: req.port,
                        protocol: req.proto
                    };
                    if (typeof options.munger === 'function') {
                        results = options.munger(results);
                    }
                    if (options.type === 'xml') {
                        xmlDust.render(options.template, results, sendResponse.bind(this, req, options));
                    } else {
                        dust.render(options.template, results, sendResponse.bind(this, req, options));
                    }
                }
            });
        }
    }).on('error', (err) => {
        // If tracking metrics, finish up
        if (options._palStartTime !== null) {
            if (global.countersOnly === false) {
                global.metrics.histogram('pal.requests.response.time', Useful.getHrDiff(options._palStartTime));
            }
            global.metrics.count(`pal.requests.response.${err.statusCode}.count`);
            options._palStartTime = null;
        }
        if (req.log.do_warn === true) {
            req.log.warn(`PAL returned error code ${err.statusCode} for request ${palPath}`);
        }
    });
}


palHandlers = {
    /**
     * Route handler for OCS requests
     *
     * Pulls the route apart...
     * /data/ocs/{MODEL_TYPE}/{URI}:{ZONE|*}/{TEMPLATE_NAME}.html
     * /data/ocs/{MODEL_TYPE}/{URI}:{ZONE|*}/{TEMPLATE_NAME}.izl
     * /data/ocs/{MODEL_TYPE}/{URI}:{ZONE|*}/{XML_TEMPLATE_NAME}.xml
     * /data/ocs/{MODEL_TYPE}/{URI}:{ZONE|*}/{CALLBACK_FUNCTION}.jsonp
     * /data/ocs/{MODEL_TYPE}/{URI}:{ZONE|*}.json
     * Where:
     *   MODEL_TYPE: article, section, list, etc. (maps to PAL model type or special handler)
     *   URI: index.html or a valid document URI
     *   ZONE: zone id or *
     *   TEMPLATE_NAME: name of a dust view (ie views/cards/card)
     *   XML_TEMPLATE_NAME: name of an XML dust view (ie views/xml/video-content)
     *   CALLBACK_FUNCTION: name of the callback function to use with a JSONP response
     *
     * @memberof routing/pal
     * @public
     * @param {object} req - Request object (RouteOMatic)
     * @param {object} route - Route object for matching route rule
     * @param {object} args - Arguments from route matching
     */
    doOcs: function (req, route, args) {
        let opts = route.options || {},
            params = req.path.match(/^\/data\/ocs\/([\w\-]+)(\/[\w\-\.!~\*\/\+\%]+):([\w\-]+|\*)(\/[\w\-\/]+)?\.(html|izl|json|jsonp|xml)((\/[\w\.]+=\$\$[\dA-Z]+\$\$)*)$/) || [],
            filt = params[3] || '',
            model = params[1] || '',
            subs = params[6] || '',
            tmpl = params[4] || '',
            type = params[5] || '',
            uri = params[2] || '';

        // Short error messages for OCS requests
        req.isXhr = true;

        if (params.length < 6 || (tmpl.length === 0 && type !== 'json') ||
            (type === 'izl' && (filt === '*' || palModels[model].izlSupport !== true)) ||
            (type === 'jsonp' && tmpl.search(/[\-\/]/ !== -1))) {

            // Bad match so 404 it.
            req.error(404);
            return;
        }

        // Process the substitutions
        if (subs) {
            let sObj = {};

            params = subs.substring(1).split('/');
            for (let i = 0; i < params.length; i++) {
                if (typeof params[i] === 'string' && params[i].length !== 0) {
                    let sub = params[i].split('=');

                    sObj[sub[0]] = sub[1];
                }
            }
            opts.munger = mungers.ocsSubs.bind(this, sObj);
        }

        // Set-up the options
        switch (model) {
        case 'adsection':
        case 'playlist':
            opts = {
                model: model,
                munger: opts.munger || mungers.noop,
                type: type
            };
            break;
        default:
            opts = {
                model: 'section',
                munger: opts.munger || mungers.ocs,
                type: type
            };
        }
        if (tmpl.length !== 0) {
            if (type === 'jsonp') {
                opts.jsonpCallback = tmpl;
            } else {
                opts.template = tmpl.slice(1);
            }
        }

        // Record PAL request start time
        opts._palStartTime = (global.metrics !== null) ? process.hrtime() : null;

        // If previewing, use no-cache headers
        if (uri.search(/^\/[\w\-]+\/preview\/.+/) !== -1) {
            req.headers = Clone(noCacheHeaders);
        }

        global.pal.getZone(uri, filt, {
            editionType: global.edition,
            logger: req.log,
            modelType: model,
            previewWorkspace: args.workspace || false
        }, handlePalResponse.bind(this, req, opts));
    },


    /**
     * Route handler for No Model (aka "empty") content.
     * Used for things like local pages or search where CMS/PAL is not
     * really needed, other than for page header/footer navigation stuff.
     *
     * @memberof routing/pal
     * @public
     * @param {object} req - Request object (RouteOMatic)
     * @param {object} route - Route object for matching route rule
     * @param {object} args - Arguments from route matching
     */
    doNoModel: function (req, route, args) {
        let opts = mungeMunger(route.options, mungers.default);

        // Record PAL request start time
        opts._palStartTime = (global.metrics !== null) ? process.hrtime() : null;

        global.pal.getEmpty(getOcsKey(req.normalizedPath), {
            editionType: global.edition,
            logger: req.log,
            previewWorkspace: args.workspace || false
        }, handlePalResponse.bind(this, req, opts));
    },


    /**
     * Route handler for article page content
     *
     * @memberof routing/pal
     * @public
     * @param {object} req - Request object (RouteOMatic)
     * @param {object} route - Route object for matching route rule
     * @param {object} args - Arguments from route matching
     */
    doArticle: function (req, route, args) {
        let opts = mungeMunger(route.options, mungers.leaf);

        if (typeof opts.section === 'string') {
            opts.section = opts.section || 'news';
        } else {
            let params = req.normalizedPath.match(/^\/\d{4}\/\d{2}\/\d{2}\/([\w\-]+)\/[\w\-]+/);

            if (Array.isArray(params)) {
                // If we matched the pattern, treat the first parameter after the date as the section
                opts.section = params[1];
            } else {
                opts.section = 'news';
            }
        }

        // For "TheSource", proxy off to other handler.  Feh.  An artifact of bad section routing.
        if (opts.section === 'the-source') {
            req.proxy({
                proxy: {
                    auth: req.auth,
                    hash: req.hash,
                    headers: {
                        'x-proxied-env': global.environment,
                        'x-proxied-edition': global.edition
                    },
                    hostname: theSourceHost,
                    path: req.path,
                    port: theSourcePort,
                    proto: 'http',
                    query: req.query
                }
            });
            return;
        }

        // Record PAL request start time
        opts._palStartTime = (global.metrics !== null) ? process.hrtime() : null;

        global.pal.getArticle(getOcsKey(req.normalizedPath), {
            editionType: global.edition,
            logger: req.log,
            previewWorkspace: args.workspace || false
        }, handlePalResponse.bind(this, req, opts));
    },


    /**
     * Route handler for generic page content (uses PAL section handler)
     *
     * @memberof routing/pal
     * @public
     * @param {object} req - Request object (RouteOMatic)
     * @param {object} route - Route object for matching route rule
     * @param {object} args - Arguments from route matching
     */
    doGeneric: function (req, route, args) {
        let opts = mungeMunger(route.options, mungers.default);

        opts.munger = args.munger || mungers.default;
        // Record PAL request start time
        opts._palStartTime = (global.metrics !== null) ? process.hrtime() : null;

        global.pal.getSection(getOcsKey(req.normalizedPath), {
            editionType: global.edition,
            logger: req.log,
            previewWorkspace: args.workspace || false
        }, handlePalResponse.bind(this, req, opts));
    },


    /**
     * Route handler for list page content
     *
     * @memberof routing/pal
     * @public
     * @param {object} req - Request object (RouteOMatic)
     * @param {object} route - Route object for matching route rule
     * @param {object} args - Arguments from route matching
     */
    doList: function (req, route, args) {
        let opts = mungeMunger(route.options, mungers.list);

        // Record PAL request start time
        opts._palStartTime = (global.metrics !== null) ? process.hrtime() : null;

        global.pal.getList(getOcsKey(req.normalizedPath), {
            editionType: global.edition,
            logger: req.log,
            previewWorkspace: args.workspace || false
        }, handlePalResponse.bind(this, req, opts));
    },


    /**
     * Route handler for homepage content
     *
     * @memberof routing/pal
     * @public
     * @param {object} req - Request object (RouteOMatic)
     * @param {object} route - Route object for matching route rule
     * @param {object} args - Arguments from route matching
     */
    doHomepage: function (req, route, args) {
        let opts = mungeMunger(route.options, mungers.homepage);

        // Record PAL request start time
        opts._palStartTime = (global.metrics !== null) ? process.hrtime() : null;

        global.pal.getSection(getOcsKey(req.normalizedPath), {
            editionType: global.edition,
            logger: req.log,
            previewWorkspace: args.workspace || false
        }, handlePalResponse.bind(this, req, opts));
    },


    /**
     * Route handler for profile page content
     *
     * @memberof routing/pal
     * @public
     * @param {object} req - Request object (RouteOMatic)
     * @param {object} route - Route object for matching route rule
     * @param {object} args - Arguments from route matching
     */
    doProfile: function (req, route, args) {
        let opts = mungeMunger(route.options, mungers.profile);

        // Record PAL request start time
        opts._palStartTime = (global.metrics !== null) ? process.hrtime() : null;

        global.pal.getProfile(getOcsKey(req.normalizedPath), {
            editionType: global.edition,
            logger: req.log,
            previewWorkspace: args.workspace || false
        }, handlePalResponse.bind(this, req, opts));
    },


    /**
     * Route handler for section page content
     *
     * @memberof routing/pal
     * @public
     * @param {object} req - Request object (RouteOMatic)
     * @param {object} route - Route object for matching route rule
     * @param {object} args - Arguments from route matching
     */
    doSection: function (req, route, args) {
        let opts = mungeMunger(route.options, mungers.section);

        opts.section = opts.section || req.normalizedPath.replace(/^\/([\w\-]+).*$/, '$1');
        // Record PAL request start time
        opts._palStartTime = (global.metrics !== null) ? process.hrtime() : null;

        global.pal.getSection(getOcsKey(req.normalizedPath), {
            editionType: global.edition,
            logger: req.log,
            previewWorkspace: args.workspace || false
        }, handlePalResponse.bind(this, req, opts));
    },


    /**
     * Route handler for sub-section page content
     *
     * @memberof routing/pal
     * @public
     * @param {object} req - Request object (RouteOMatic)
     * @param {object} route - Route object for matching route rule
     * @param {object} args - Arguments from route matching
     */
    doSubSection: function (req, route, args) {
        let opts = mungeMunger(route.options, mungers.subsection);

        if (typeof opts.section !== 'string' || typeof opts.subsection !== 'string') {
            let params = req.normalizedPath.match(/^\/([\w\-]+)\/([\w\-]+).*$/);
            opts.section = params[1];
            opts.subsection = params[2];
        }

        // Record PAL request start time
        opts._palStartTime = (global.metrics !== null) ? process.hrtime() : null;

        global.pal.getSection(getOcsKey(req.normalizedPath), {
            editionType: global.edition,
            logger: req.log,
            previewWorkspace: args.workspace || false
        }, handlePalResponse.bind(this, req, opts));
    },


    /**
     * Route handler for special page content
     *
     * @memberof routing/pal
     * @public
     * @param {object} req - Request object (RouteOMatic)
     * @param {object} route - Route object for matching route rule
     * @param {object} args - Arguments from route matching
     */
    doSpecial: function (req, route, args) {
        let opts = mungeMunger(route.options, mungers.special);

        // Record PAL request start time
        opts._palStartTime = (global.metrics !== null) ? process.hrtime() : null;

        global.pal.getShow(getOcsKey(req.normalizedPath), {
            editionType: global.edition,
            logger: req.log,
            previewWorkspace: args.workspace || false
        }, handlePalResponse.bind(this, req, opts));
    },


    /**
     * Route handler for show page content
     *
     * @memberof routing/pal
     * @public
     * @param {object} req - Request object (RouteOMatic)
     * @param {object} route - Route object for matching route rule
     * @param {object} args - Arguments from route matching
     */
    doShow: function (req, route, args) {
        let opts = mungeMunger(route.options, mungers.show);

        // Record PAL request start time
        opts._palStartTime = (global.metrics !== null) ? process.hrtime() : null;

        global.pal.getShow(getOcsKey(req.normalizedPath), {
            editionType: global.edition,
            logger: req.log,
            previewWorkspace: args.workspace || false
        }, handlePalResponse.bind(this, req, opts));
    },


    /**
     * Route handler for legacy video and video data content
     *
     * @memberof routing/pal
     * @public
     * @param {object} req - Request object (RouteOMatic)
     * @param {object} route - Route object for matching route rule
     * @param {object} args - Arguments from route matching
     */
    doVideo: function (req, route, args) {
        let opts;

        // Redirect legacy video
        if (args[1].search(/^\/2\.0\/video/) !== -1) {
            req.redirect(301, req.path.replace(/^\/video\/data\/2\.0\/video/, '/videos').replace(/\/index.xml$/, '').replace(/\.html$/, '') + (req.query ? '?' + req.query : ''));
            return;
        }

        // Handle video XML content
        if (args[1].search(/^(\/[\w\.\-]+)*\/index\.xml$/) !== -1) {
            opts = mungeMunger(route.options, mungers.videoConfig);
            opts.template = 'views/xml/video-content';
            opts.type = 'xml';
            palWithStaticFallback(req, opts, false);
            return;
        }

        // Handle video JSON content
        if (args[1].search(/^(\/[\w\.\-]+)*\/index\.json$/) !== -1) {
            opts = mungeMunger(route.options, mungers.videoConfig);
            opts.template = 'views/json/video-content';
            opts.type = 'json';
            palWithStaticFallback(req, opts, false);
            return;
        }

        // Handle related videos XML
        if (args[1].search(/^(\/[\w\.\-]+)*\/relateds\.xml$/) !== -1) {
            opts = mungeMunger(route.options, mungers.noop);
            opts.template = 'views/xml/relateds-video-xml';
            opts.type = 'xml';
            palWithStaticFallback(req, opts, true);
            return;
        }

        // Handle related videos JSON (via proxy)
        if (args[1].search(/^(\/[\w\.\-]+)*\/relateds\.json$/) !== -1) {
            req.proxy({
                proxy: {
                    auth: palUrl.auth,
                    hash: palUrl.hash,
                    hostname: palUrl.hostname,
                    path: req.path,
                    port: palUrl.port,
                    proto: palUrl.proto,
                    query: req.query
                }
            });
            return;
        }

        // We don't really match anything, so 404
        req.error(404);
    },


    /**
     * Route handler for video section front and landing page content
     *
     * @memberof routing/pal
     * @public
     * @param {object} req - Request object (RouteOMatic)
     * @param {object} route - Route object for matching route rule
     * @param {object} args - Arguments from route matching
     */
    doVideos: function (req, route, args) {
        if (req.normalizedPath.search(/^\/videos(\/|\/index.html)?$/) !== -1) {
            palHandlers.doSection(req, route, args);
        } else {
            palHandlers.doArticle(req, route, args);
        }
    },


    /**
     * Route handler for ncomments content
     *
     * @memberof routing/pal
     * @public
     * @param {object} req - Request object (RouteOMatic)
     * @param {object} route - Route object for matching route rule
     * @param {object} args - Arguments from route matching
     */
    doNComments: function (req, route, args) {
        route.options = route.options || {};

        route.options.munger = mungers.ncomments;
        req.normalizedPath = req.normalizedPath.replace(/^\/ncomments\//, '/');
        palHandlers.doOcs(req, route, args);
    },


    /**
     * Route handler for preview content
     *
     * @memberof routing/pal
     * @public
     * @param {object} req - Request object (RouteOMatic)
     * @param {object} route - Route object for matching route rule
     * @param {object} args - Arguments from route matching
     */
    doPreview: function (req, route, args) {
        let opts = mungeMunger(route.options, mungers.preview),
            params = req.path.match(/^\/preview\/([\w\-\.]+)\/(.+)$/),
            reqType = req.queryParams.type || '';

        // No cache headers
        req.headers = Clone(noCacheHeaders);

        // If no "type" set, extract it.
        if (reqType.length === 0 && typeof params[2] === 'string') {
            let tpars = params[2].match(/^([0-9A-Za-z\-]+)_/);

            if (typeof tpars[1] === 'string' && previewMethods.hasOwnProperty(tpars[1])) {
                reqType = tpars[1];
            }
        }

        // Must have both params (workspace and docID) and the reqType to be valid.
        if (reqType.length === 0 || typeof params[1] !== 'string' || params[1].length === 0 ||
            typeof params[2] !== 'string' || params[2].length === 0) {

            // Bad preview request
            if (req.log.do_warn === true) {
                req.log.warn(`Ignoring bad preview request ${req.url}`);
            }
            req.error(500);
            return;
        }

        opts.method = previewMethods[reqType] || previewMethods.default;
        opts.munger = previewMungers[reqType] || previewMungers.default;
        opts.template = previewTemplates[reqType] || previewTemplates.default;

        if (global.metrics !== null) {
            global.metrics.count('preview.requests');
            // Record PAL request start time
            opts._palStartTime = process.hrtime();
        } else {
            opts._palStartTime = null;
        }
        global.pal[opts.method](Path.join('/', reqType, 'preview', params[1], params[2]), {
            editionType: global.edition,
            logger: req.log,
            previewWorkspace: params[1]
        }, handlePalResponse.bind(this, req, opts));
    },


    /**
     * Handler for simple proxy routes
     *
     * @memberof routing
     * @public
     * @param {object} req - Request object (RouteOMatic)
     * @param {object} route - Route object for matching route rule
     * @param {object} args - Arguments from route matching
     */
    doSimpleProxy: function (req, route, args) {
        let proxyOpts = Clone(route.options.proxy);

        if (req.log.do_debug === true) {
            req.log.debug(`doSimpleProxy: ${req.href}`);
        }
        if (typeof req.query === 'string' && req.query.length !== 0) {
            if (typeof proxyOpts.query === 'string' && proxyOpts.query.length !== 0) {
                proxyOpts.query += '&' + req.query;
            } else {
                proxyOpts.query = req.query;
            }
        }
        req.proxy({proxy: proxyOpts});
    }
};

module.exports = palHandlers;

# Contributors

## New Development - Atlanta, Georgia
- James Young (Applications Architect) <james.young@turner.com>
- Aditya Srinivasan (Technical Manager) <aditya.srinivasan@turner.com>
- Matt Crutchfield (Senior Systems / Software Developer) <matt.crutchfield@turner.com>
- Vance Price (Senior Systems / Software Developer) <vance.price@turner.com>
- Chris Gonzalez (Senior Interaction Designer) <chris.gonzalez@turner.com>
- John Hayes (Senior Software Developer)
- Michael Imamura (Senior Software Developer) <michael.imamura@turner.com>
- Sean Joseph (Senior Software Developer) <sean.joseph@turner.com>
- A.D. Slaton (Senior Web Developer) <ad.slaton@turner.com>
- James Drenter (Senior Web Developer) <james.drenter@turner.com>
- Joey Garcia (Senior Web Developer) <joey.garcia@turner.com>
- Michael Puckett (Senior Web Developer)
- Subi Babu (Senior Web Developer) <subi.babu@turner.com>
- James Manring (Senior Systems Engineer) <james.manring@turner.com>
- David Lumpkin (Expert Software Developer) <david.lumpkin@turner.com>
- Charles Rawls (Software Application Developer) <charles.rawls@turner.com>
- Chad Bartels (Web Developer)
- Christian Hain (Web Developer) <christian.hain@turner.com>
- Eric Blanks (Web Developer) <eric.blanks@turner.com>
- Jermel Washington (Web Developer) <jermel.washington@turner.com>
- Michael Botelho (Web Developer) <michael.botelho@turner.com>
- Paul Borrego (Web Developer) <paul.borrego@turner.com>
- Sean Grant (Web Developer) <sean.grant@turner.com>
- Sri Batchu (Web Developer) <sri.batchu@turner.com>
- Stephen Slider (Web Developer) <stephen.slider@turner.com>
- Raja Periyasamy (Contractor) <raja.periyasamy@turner.com>


## New Development - New York, New York
- Daniel Ho (Senior Web Developer) <daniel.ho@turner.com>


## Infrastructure & Core Technology - Atlanta, Georgia
- Wilson Wise (Principal Architect) <wilson.wise@turner.com>
- Will Berry (Principal Architect) <will.berry@turner.com>
- Nkomo Butts (Lead Systems / Software Developer) <nkomo.butts@turner.com>


## Operations - Atlanta, Georgia
- Kyle Rogers (Lead Systems / Software Developer) <kyle.rogers@turner.com>
- Brian Duckett (Web Editor / Webmaster) <brian.duckett@turner.com>


## Operations / New Development - London, England
- Rishi Arora (Technical Manager) <rishi.arora@turner.com>
- Kevin Taverner (Webmaster / Developer) <kevin.taverner@turner.com>
- Miguel Villanueva (Senior Software Developer) <miguel.villanueva@turner.com>
- John Morrison (Software Engineer) <john.morrison@turner.com>
- Nav Garcha (Web Developer) <nav.garcha@turner.com>


## ObjectFrontier - Atlanta, Georgia / Chennai, India
- Elango Dhandapani (Tech Lead) <elango.dhandapani@turner.com>
- Hari Jyothsna Nukala (Tech Lead) <hari.j.nukala@turner.com>
- Sivakumar Gopalakrishnan (Tech Lead) <sivakumar.gopalakrishnan@turner.com>
- Boobalakrishnan Ramasamy (Frontend Tech Lead) <boobalakrishnan.ramasamy@turner.com>
- Venkatesan Rangaraj (Senior Developer)<venkatesan.rangaraj@turner.com>
- Arun Kannan (Developer) <arun.kannan@turner.com>
- Dhivya Venkatachalam (Developer) <dhivya.venkatachalam@turner.com>
- Gayathri Subash (Developer) <gayathri.subash@turner.com>
- Gururajan Balakrishnan (Developer) <gururajan.balakrishnan@turner.com>


## Four Kitchens - Austin, Texas
- Chris Ruppel (Frontend Engineer) <chris@fourkitchens.com>
- Matt Grill (Engineer) <matt@fourkitchens.com>

---

To update this file, check names against `git log` and get titles and email from
<http://tim.turner.com>.

```shell
$ git log --format='%aN' | sort | uniq -c | sort -r
```

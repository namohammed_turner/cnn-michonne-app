'use strict';

const
    bodyParser = require('body-parser'),
    cdnBeaconHeader = 'x-cnn-cdn-srv',
    cssDir = './views/css/',
    currentWorkingDir = process.cwd(),
    Compress = require('compression'),
    defaultRetry = 1800,  // 30 minutes
    Domain = require('domain'),
    dust = require('./views/index.js'),
    errorHeaders = {
        'Cache-Control': process.env.ERROR_CACHE_CONTROL || 'max-age=5'  // Default cache time is 5 seconds
    },
    Express = require('express'),
    fs = require('fs'),
    hcStart = Date.now(),
    http = require('http'),
    HttpError = require('./lib/http_error'),
    noCacheHeaders = {
        'Surrogate-Control': 'max-age=2, stale-while-revalidate=5, stale-if-error=10',
        'Cache-Control': 'no-cache'
    },
    packageConfig = require('./package.json'),
    ProxyAddr = require('proxy-addr'),
    RouteOMatic = require('cnn-routeomatic'),
    SigSci = new require('sigsci-module-nodejs'),
    startedAt = process.hrtime(),
    Url = require('url'),
    useGzip = process.env.ORIGIN_GZIP || false,
    Useful = require('./lib/useful');

var
    app = Express(),
    hcHits = 0,
    https,
    httpServer = null,
    httpsServer = null,
    log,
    maxConnects,
    maxSockets,
    offlineCounter = 0,
    offlineTimer,
    responseSent = null,
    romConfig,
    router,
    runtime,
    Runtime,
    sigsci,
    sigscienv,
    sigscimw,
    socketPool = false,
    staticDir,
    tlsOptions = null,
    trinity,
    trustHostTest,
    trustPartnerTest;


//
// Initialize globals and verify env settings
//

// Initialize some required stuff and verify environment settings
if (typeof process.env.EDITION === 'string' && process.env.EDITION.length !== 0 &&
    typeof process.env.ENVIRONMENT === 'string' && process.env.ENVIRONMENT.length !== 0 &&
    typeof process.env.PRODUCT === 'string' && process.env.PRODUCT.length !== 0 &&
    typeof process.env.PORT === 'string' && parseInt(process.env.PORT, 10) !== 0 &&
    typeof process.env.PAL_URL === 'string' && Useful.isValidUrl(process.env.PAL_URL) === true) {

    global.edition = process.env.EDITION.toLowerCase();
    global.environment = process.env.ENVIRONMENT.toLowerCase();
    global.port = parseInt(process.env.PORT, 10);
    global.sslPort = (process.env.PORT && parseInt(process.env.SSL_PORT, 10)) || 0;
    global.palUrl = process.env.PAL_URL;
    global.product = process.env.PRODUCT.toLowerCase();
    global.buildVersion = packageConfig.version;
    global.metricsInterval = (process.env.METRICS_INTERVAL && parseInt(process.env.METRICS_INTERVAL)) || 20000;
    global.countersOnly = (process.env.METRICS_COUNTERS_ONLY === '1' || process.env.METRICS_COUNTERS_ONLY === 'true');
    socketPool = (process.env.ENABLE_SOCKET_POOL === '1' || process.env.ENABLE_SOCKET_POOL === 'true');

    if (global.environment === 'local') {
        global.palEnvironment = (process.env.PAL_ENV && process.env.PAL_ENV.toLowerCase()) || 'local';
    } else {
        global.palEnvironment = global.environment;
    }
} else {
    console.error('Required environment variable EDITION, ENVIRONMENT, PRODUCT, PORT, SSL_PORT, or PAL_URL is not set or invalid.  Unable to start.');
    process.exit(1);
}

// Initialize metrics collection
if ((process.env.ENABLE_METRICS === '1' || process.env.ENABLE_METRICS === 'true') &&
    (process.env.HOSTEDGRAPHITE_APIKEY || (process.env.DATADOG_APIKEY && process.env.DATADOG_APPKEY))) {

    global.metrics = require('cnn-metrics');
    global.metrics.init({
        appName: 'michonne',
        appType: 'fe',
        customer: (process.env.CUSTOMER && process.env.CUSTOMER.toLowerCase()) || 'cnn',
        flushEvery: global.metricsInterval,
        plugins: ['customCounters']
    });
    responseSent = function (getHrDiff, req, res) {
        if (Array.isArray(res.locals._metricsStart) && res.headersSent) {
            let proto = ((req.socket.localPort || req.socket.address().port) === global.sslPort) ? 'https' : 'http';

            if (global.countersOnly === false) {
                global.metrics.histogram(`requests.${proto}.response.${res.statusCode}.time`, getHrDiff(res.locals._metricsStart));
            }
            res.locals._metricsStart = null;
            global.metrics.count(`requests.${proto}.response.${res.statusCode}.count`);
            res.locals._metricsStart = null;
        }
    }.bind(global, Useful.getHrDiff);
} else {
    global.metrics = null;
}

// Initialize the global logger before we do much else...
global.log = require('cnn-logger')(process.env.LOGZIO_TOKEN ? {
    console: {
        logLevel: (process.env.KUBERNETES_PORT ? 'fatal' : (process.env.CONSOLE_LOG_LEVEL || process.env.LOG_LEVEL))
    },
    logzio: {
        tags: [global.product, global.edition + '-edition', global.environment + '-env']
    }
} : {});
log = global.log;

// log message test
log.important('Initializing Logging subsystem...');
log.silly('Running self check: silly');
log.debug('Running self check: debug');
log.verbose('Running self check: verbose');
log.info('Running self check: info');
log.warn('Running self check: warn');
log.error('Running self check: error');
log.fatal('Running self check: fatal');
log.important('Running self check: important');


// Initialize global config
global.config = require('./cfg').getConfiguration(global.edition, global.environment);
global.config.baseVersion = global.buildVersion.replace(/^(\w+\.\w+\.\w+).*$/, '$1');
global.config.edition = global.edition || global.config.edition;
global.config.environment = global.environment;
global.css = {};
global.errorPages = {};
global.srvConfig = {};

// Initialize connection settings
maxConnects = (process.env.MAX_CONNECTIONS && parseInt(process.env.MAX_CONNECTIONS, 10)) || global.config.maxConnections || 1000;
maxSockets = (process.env.MAX_SOCKETS && parseInt(process.env.MAX_SOCKETS, 10)) || global.config.maxSockets || 2000;

// Allow environment variable overrides for LSD server and config paths
global.config.host.data = process.env.LSD_HOST_URL || global.config.host.data;
global.config.dataPaths.config = process.env.LSD_CONFIG_PATH || global.config.dataPaths.config;
global.config.dataPaths.routes = process.env.LSD_ROUTES_PATH || global.config.dataPaths.routes;
global.config.dataPaths.zones = process.env.LSD_INTERNATIONAL_ZONES_PATH || global.config.dataPaths.zones;
global.config.dataPaths.overrides = process.env.LSD_CONFIG_OVERRIDE_PATH || global.config.dataPaths.overrides;
global.config.dataPaths.sectionconfig = process.env.LSD_CONFIG_SECTIONCONFIG_PATH || global.config.dataPaths.sectionconfig;
global.config.dataPaths.homepage = process.env.LSD_HOMEPAGE_PATH || global.config.dataPaths.homepage;
global.config.dataTtl = process.env.LSD_DATA_TTL ? parseInt(process.env.LSD_DATA_TTL, 10) : global.config.dataTtl;
global.config.palTimeout = process.env.PAL_TIMEOUT ? parseInt(process.env.PAL_TIMEOUT, 10) : global.config.palTimeout;
global.config.cdn.proxyContent = global.config.localPalContentProxy[global.palEnvironment] || global.config.cdn.proxyContent;

// Allow overriding the hostnames
if (process.env.ALT_HOST) {
    global.config.hostnames.altHost = process.env.ALT_HOST.toLowerCase();
    if (global.edition === 'domestic') {
        global.config.host.us = '//' + global.config.hostnames.altHost;
    }
}
if (process.env.ASSET_HOST) {
    global.config.hostnames.assetHost = process.env.ASSET_HOST.toLowerCase();
    global.config.cdn.assetHost = '//' + global.config.hostnames.assetHost;
}
if (process.env.MAIN_HOST) {
    global.config.hostnames.mainHost = process.env.MAIN_HOST.toLowerCase();
    if (global.edition === 'domestic') {
        global.config.host.www = '//' + global.config.hostnames.mainHost;
        global.config.host.domain = global.config.host.www;
        global.config.host.domainWww = global.config.host.www;
    } else {
        global.config.host.intl = '//' + global.config.hostnames.mainHost;
        global.config.host.domain = global.config.host.intl;
        global.config.host.domainIntl = global.config.host.intl;
    }
}
if (process.env.PREVIEW_HOST) {
    global.config.hostnames.previewHost = process.env.PREVIEW_HOST.toLowerCase();
    global.config.host.domainPreview = '//' + global.config.hostnames.mainHost;
}
if (process.env.SPONSOR_HOST) {
    global.config.hostnames.sponsorHost = process.env.SPONSOR_HOST.toLowerCase();
    global.config.host.domainPreview = '//' + global.config.hostnames.sponsorHost;
}
if (process.env.TRAVEL_HOST) {
    global.config.hostnames.travelHost = process.env.TRAVEL_HOST.toLowerCase();
}
if (process.env.THESOURCE_HOST) {
    global.config.hostnames.theSourceHost = process.env.THESOURCE_HOST.toLowerCase();
}

// Process the SigSciences environment variable
if (process.env.SIGSCI_AGENT) {
    if (process.env.SIGSCI_AGENT.charAt(0) === '/' && process.env.SIGSCI_AGENT.charAt(1) !== '/') {
        sigscienv = {
            path: process.env.SIGSCI_AGENT
        };
    } else {
        let parts = process.env.SIGSCI_AGENT.match(/^(?:(?:http:|https:)?\/\/)?(\w[\w\-\.]+\w):(\d+)*$/);

        if (parts && parts.length >= 2) {
            sigscienv = {
                host: parts[1],
                port: Number(parts[2] || 9999)
            };
        }
    }
}

// Prep things that share the global logger and/or build time configs
trinity = require('trinity');
Runtime = trinity.runtime;

// Set the number of max client sockets for HTTP
http.globalAgent.pool = socketPool;
http.globalAgent.maxSockets = maxSockets;

// Prep for HTTPS or HTTP/2
if (global.sslPort !== 0) {
    if (global.config.flags.enableHttp2 !== false) {
        https = require('http2');
        // Need to do this for Express 4.x, should be able to remove it with Express 5.x.
        require('express-http2-workaround')({
            express: Express,
            http2: https,
            app: app
        });
    } else {
        https = require('https');
    }

    // Prep for local TLS, if we are doing that kind of thing
    if (global.config.flags.localTLS === true || process.env.LOCAL_TLS === 'true') {
        tlsOptions = {
            key: fs.readFileSync('./resources/local_certs/localhost.next.cnn.com.key'),
            cert: fs.readFileSync('./resources/local_certs/localhost.next.cnn.com.crt')
        };
    }

    if (tlsOptions) {
        // Set the number of max client sockets for HTTPS
        https.globalAgent.pool = socketPool;
        https.globalAgent.maxSockets = maxSockets;
    }
}

// Setup the RouteOMatic environment config
romConfig = {
    env: {
        conds: {
            assetHost: global.config.hostnames.assetHost,
            edition: global.edition,
            environment: global.environment,
            mainHost: global.config.hostnames.mainHost,
            previewHost: global.config.hostnames.previewHost
        },
        subs: {
            altHost: global.config.hostnames.altHost,
            assetHost: global.config.hostnames.assetHost,
            assetHostPath: global.config.cdn.assetPath + global.config.cdn.assetPath + '/' + global.config.baseVersion,
            base: global.config.host.domain,
            cdnBaseChrome: global.config.cdn.baseChrome,
            cdnBaseZip: global.config.cdn.baseZip,
            css: '/css/' + global.config.baseVersion,
            domestic: (global.edition === 'international') ? global.config.host.us : global.config.host.www,
            edition: global.edition,
            env: global.environment,
            international: global.config.host.intl,
            mainHost: global.config.hostnames.mainHost,
            previewHost: global.config.hostnames.previewHost,
            sponsorHost: global.config.hostnames.sponsorHost,
            static: global.config.cdn.assetPath + '/' + global.config.baseVersion,
            theSourceHost: global.config.hostnames.theSourceHost,
            travelHost: global.config.hostnames.travelHost,
            version: global.config.baseVersion
        }
    },
    logger: log,
    onSent: responseSent,
    ports: {},
    requestLogger: log,
    routeHandlers: require('./routing/handlers')
};

romConfig.ports[global.port.toString(10)] = {
    origPort: 80,
    origProto: 'http',
    origProtoVer: '1.1'
};

if (global.sslPort !== 0) {
    romConfig.ports[global.sslPort.toString(10)] = {
        origPort: 443,
        origProto: 'https',
        origProtoVer: (global.config.flags.enableHttp2 !== false) ? '2.0' : '1.1'
    };
}

// Use the trusted proxy settings from the configuration
if (Array.isArray(global.config.trustedAddrs.proxies)) {
    global.config.trustedAddrs.proxies.unshift('loopback', 'linklocal');
}
if (Array.isArray(global.config.trustedAddrs.internal)) {
    global.config.trustedAddrs.internal.unshift('loopback', 'linklocal');
}
trustHostTest = ProxyAddr.compile(global.config.trustedAddrs.internal);
if (Array.isArray(global.config.trustedAddrs.partners)) {
    global.config.trustedAddrs.partners.unshift('loopback', 'linklocal');
    trustPartnerTest = ProxyAddr.compile(global.config.trustedAddrs.partners);
} else {
    // No partners set, so same as internal
    global.config.trustedAddrs.partners = global.config.trustedAddrs.internal;
    trustPartnerTest = trustHostTest;
}

// addProxyContentRoute function
function addProxyContentRoute(routetabs) {
    if (global.config.cdn.proxyContent.length !== 0 && routetabs &&
        routetabs.assets && Array.isArray(routetabs.assets.routes)) {

        let proxyUrl = Url.parse(global.config.cdn.proxyContent);

        routetabs.assets.routes.push({
            do: 'doServiceProxy',
            on: global.config.cdn.content,
            options: {
                proxy: {
                    hostname: proxyUrl.hostname,
                    port: proxyUrl.port,
                    proto: 'http'
                }
            }
        });
        if (log.do_info === true) {
            log.info(`Proxying content at ${global.config.cdn.content} from ${global.config.cdn.proxyContent}`);
        }
    }
}

// Initialize fallbacks for runtime settings
global.config.runtime = trinity.getConfig('michonne', global.environment, global.edition);
global.config.runtime.signalSciences = global.config.runtime.signalSciences || {};
global.config.runtime.signalSciences.log = log.info;
if (sigscienv) {
    if (sigscienv.host) {
        global.config.runtime.signalSciences.host = sigscienv.host;
        global.config.runtime.signalSciences.port = sigscienv.port;
    } else if (sigscienv.path) {
        global.config.runtime.signalSciences.path = sigscienv.path;
    }
}
global.config.overrides = trinity.getConfig('michonne-overrides', global.environment, global.edition);
global.srvConfig.routes = trinity.getConfig('routing', global.environment);
global.config.sectionconfig = trinity.getConfig('sectionconfig', global.environment, global.edition);
global.config.zones = trinity.getConfig('zones', global.environment, global.edition);


// Handle SIGTERM nicely
process.on('SIGTERM', function () {
    function doExit() {
        if (global.metrics !== null) {
            global.metrics.count('app.shutdowns');
            global.metrics.flush();
        }
        process.exit(0);
    }

    if (httpsServer !== null) {
        httpsServer.close(function () {
            httpServer.close(doExit);
        });
    } else if (httpServer) {
        httpServer.close(doExit);
    } else {
        doExit();
    }
});


// Initialize global runtime
runtime = new Runtime(global.config.host.data, global.config.dataTimeout);


// Setup callbacks for runtime settings
function getRuntimeConfigCallback(error, result) {
    if (log.do_debug === true) {
        log.debug(`Checked runtime configuration from ${global.config.host.data}${global.config.dataPaths.config}`);
    }

    if (error) {
        log.error(error);
    } else if (typeof result === 'object' && result !== null) {
        // First check if Signal Sciences settings changed
        result.features = result.features || {};
        if (sigscienv) {
            result.signalSciences = result.signalSciences || {};
            if (sigscienv.host) {
                result.signalSciences.host = sigscienv.host;
                result.signalSciences.port = sigscienv.port;
            } else if (sigscienv.path) {
                result.signalSciences.path = sigscienv.path;
            }
        }
        if (result.features.enableSignalSciences === true && typeof result.signalSciences === 'object' &&
            (global.config.runtime.features.enableSignalSciences !== true ||
            result.signalSciences.maxPostSize !== global.config.runtime.signalSciences.maxPostSize ||
            result.signalSciences.host !== global.config.runtime.signalSciences.host ||
            result.signalSciences.port !== global.config.runtime.signalSciences.port ||
            result.signalSciences.path !== global.config.runtime.signalSciences.path ||
            result.signalSciences.socketTimeout !== global.config.runtime.signalSciences.socketTimeout)) {

            // Signal Sciences settings changed, so reset it
            result.signalSciences.log = log.info;
            sigsci = new SigSci(result.signalSciences);
            sigscimw = sigsci.express();
        }
        global.config.runtime = result;
        if (global.metrics !== null) {
            global.metrics.count('app.config.michonne.updated');
        }
        if (log.do_info === true) {
            log.info(`Updated runtime configuration from ${global.config.host.data}${global.config.dataPaths.config}`);
        }
    }
}

// Setup callback for routes
function getRuntimeRoutesCallback(error, result) {
    if (log.do_debug === true) {
        log.debug(`Checked routes configuration from ${global.config.host.data}${global.config.dataPaths.routes}`);
    }

    if (error) {
        log.error(error);
    } else if (result) {
        addProxyContentRoute(result.routeTables);
        if (router.reconfigure(romConfig, result)) {
            global.srvConfig.routes = result;
            if (global.metrics !== null) {
                global.metrics.count('app.configRouting.updated');
            }
            if (log.do_info === true) {
                log.info(`Updated routes from ${global.config.host.data}${global.config.dataPaths.routes}`);
            }
        } else {
            if (global.metrics !== null) {
                global.metrics.count('app.configRouting.failed');
            }
            if (log.do_warn === true) {
                log.warn(`Failed to update routes from ${global.config.host.data}${global.config.dataPaths.routes}`);
            }
        }
    }
}

// Setup callback for lazy loaded zones
function getZonesConfigCallback(error, result) {
    if (log.do_debug === true) {
        log.debug(`Checked zones configuration from ${global.config.host.data}${global.config.dataPaths.zones}`);
    }

    if (error) {
        log.error(error);
    } else if (result) {
        global.config.zones = result;
        if (global.metrics !== null) {
            global.metrics.count('app.configZones.updated');
        }
        if (log.do_info === true) {
            log.info(`Updated zones configuration from ${global.config.host.data}${global.config.dataPaths.zones}`);
        }
    }
}

function getRuntimeSectionConfigCallback(error, result) {
    if (log.do_debug === true) {
        log.debug(`Checked sectionconfig configuration from ${global.config.host.data}${global.config.dataPaths.sectionconfig}`);
    }

    if (error) {
        log.error(error);
    } else if (result) {
        global.config.sectionconfig = result;
        if (global.metrics !== null) {
            global.metrics.count('app.configSections.updated');
        }
        if (log.do_info === true) {
            log.info(`Updated sectionconfig from ${global.config.host.data}${global.config.dataPaths.sectionconfig}`);
        }
    }
}

// Setup callback for config overrides
function getRuntimeConfigOverridesCallback(error, result) {
    if (log.do_debug === true) {
        log.debug(`Checked overrides configuration from ${global.config.host.data}${global.config.dataPaths.overrides}`);
    }

    if (error) {
        log.error(error);
    } else if (result) {
        global.config.overrides = result;
        if (global.metrics !== null) {
            global.metrics.count('app.configOverrides.updated');
        }
        if (log.do_info === true) {
            log.info(`Updated overrides configuration from ${global.config.host.data}${global.config.dataPaths.overrides}`);
        }
    }
}
// If testing, just use the default runtime settings (from node_modules/trinity dir)
if (global.config.isTest !== true) {
    // Get initial runtime config values
    runtime.get(global.config.dataPaths.config, getRuntimeConfigCallback);
    runtime.get(global.config.dataPaths.sectionconfig, getRuntimeSectionConfigCallback);
    runtime.get(global.config.dataPaths.zones, getZonesConfigCallback);
    runtime.get(global.config.dataPaths.overrides, getRuntimeConfigOverridesCallback);

    // Setup rechecking for runtime config values
    setInterval(runtime.get.bind(runtime, global.config.dataPaths.config, getRuntimeConfigCallback), global.config.dataTtl).unref();
    setInterval(runtime.get.bind(runtime, global.config.dataPaths.zones, getZonesConfigCallback), global.config.dataTtl).unref();
    setInterval(runtime.get.bind(runtime, global.config.dataPaths.sectionconfig, getRuntimeSectionConfigCallback), global.config.dataTtl).unref();
    setInterval(runtime.get.bind(runtime, global.config.dataPaths.overrides, getRuntimeConfigOverridesCallback), global.config.dataTtl).unref();
}


/**
 * Function to process CSS files into memory
 * @private
 */
function processCss() {
    /**
     * Function to parse a CSS file, performing template substitutions, into a string.
     * @private
     * @param {string} file - CSS file name to parse
     * @return {string} Resulting file contents after template substitutions
     */
    function parseCss(file) {
        let result = '',
            subs = [];

        try {
            result = fs.readFileSync(cssDir + file, {encoding: 'utf8', flag: 'r'});
            subs = result.match(/\@\@\w[\w\.]+/g);
            if (Array.isArray(subs) && subs.length > 0) {
                for (let i = 0; i < subs.length; i++) {
                    result = result.replace(subs[i], Useful.parseObjectName(subs[i].substring(2), global.config));
                }
            }
        } catch (err) {
            log.fatal(`Failed to read CSS file "${file}": ${err.message}`);
            if (global.metrics !== null) {
                global.metrics.count('app.fatal.errors');
                global.metrics.flush();
            }
            process.exit(2);
        }
        return result;
    }

    try {
        ['', 'pages/'].forEach((sub) => {
            let cssfiles = fs.readdirSync(cssDir + sub);

            for (let i = 0; i < cssfiles.length; i++) {
                if (cssfiles[i].search(/\.css(\.map)?$/) >= 0) {
                    global.css[sub + cssfiles[i]] = parseCss(sub + cssfiles[i]);
                    if (log.do_debug === true) {
                        log.debug(`Parsed CSS file: ${sub}${cssfiles[i]}`);
                    }
                }
            }
        });
    } catch (err) {
        log.fatal(`Failed to read CSS directory "${cssDir}": ${err.message}`);
        if (global.metrics !== null) {
            global.metrics.count('app.fatal.errors');
            global.metrics.flush();
        }
        process.exit(2);
    }
}


/**
 * Process the error models and templates from the config and pre-render the pages.
 * @private
 * @param {object} parent - The parent object for both the errorModels and errorTemplates objects
 */
function renderErrorPages(parent) {
    let getErrorPage = function (code, error, data) {
        if (error !== null) {
            log.error(error);
            log.fatal('Failed to render %j error page!', code);

            if (global.metrics !== null) {
                global.metrics.count('app.fatal.errors');
                global.metrics.flush();
            }
            process.exit(3);
        }
        global.errorPages[code] = data;
    };

    parent.errorModels = parent.errorModels || {};
    parent.errorTemplates = parent.errorTemplates || {};

    // Verify we have a default error template
    if (typeof parent.errorTemplates.default !== 'string' || parent.errorTemplates.default.length === 0) {
        parent.errorTemplates.default = 'views/pages/errors/default';
    }

    // Verify we have a default error model
    if (typeof parent.errorModels.default !== 'object' || parent.errorModels.default === null) {
        // No default, so use something basic
        parent.errorModels.default = {
            analytics: {},
            byline: '',
            canonicalUrl: '',
            config: global.config,
            copyright: new Date().getFullYear(),
            description: 'Error',
            edition: global.edition,
            editorialSource: '',
            env: global.environment,
            firstPublishDate: '',
            lastPublishDate: '',
            layout: 'error',
            location: '',
            pageType: 'section',
            sectionName: '',
            seoTitle: 'Error',
            title: '',
            type: 'page'
        };
    }

    // For each template, generate the error page
    for (let key in parent.errorTemplates) {
        if (parent.errorTemplates.hasOwnProperty(key)) {
            let model,
                tmplt;

            if (key !== 'default' && (Number(key) < 400 || Number(key) > 599)) {
                log.error('Ignoring invalid template model for error code %j!', key);
                continue;
            }
            tmplt = parent.errorTemplates[key];
            model = (typeof parent.errorModels[key] === 'object' && parent.errorModels[key] !== null) ? parent.errorModels[key] : parent.errorModels.default;
            global.errorPages[key] = '';

            // Pre-render the error page
            dust.render(tmplt, model, getErrorPage.bind(this, key));
        }
    }
}


// Initialize Express
renderErrorPages(global.config);
processCss();
staticDir = `${currentWorkingDir}/public`;
if (log.do_debug === true) {
    log.debug(`Resolved static file directory: ${staticDir}`);
}

app.enable('case sensitive routing');
app.disable('query parser');
app.disable('x-powered-by');
app.disable('etag');

// Use trust proxy with STAGE and PROD
if (global.environment === 'prod' || global.environment === 'stage') {
    app.enable('trust proxy');
}

// Setup Signal Sciences middleware
sigsci = new SigSci(global.config.runtime.signalSciences);
sigscimw = sigsci.express();

// Set-up PAL client
if (typeof global.palUrl !== 'string' || global.palUrl === 'mock') {
    if (log.do_info === true) {
        log.info('No PAL Server value was found, using a mock ORM.');
    }
    global.pal = require('./test/mocks/orm');
} else {
    if (log.do_info === true) {
        log.info(`Instantiating ORM with "${global.palUrl}" as the PAL Server.`);
    }
    global.pal = require('pal-client')(global.palUrl, {
        buildTimeCfg: global.config,
        context: 'content/by-uri',
        chalkboard: false,
        timeout: global.config.palTimeout
    });
    if (typeof global.pal === 'undefined') {
        log.fatal('Failed to load PAL Client!');
        if (global.metrics !== null) {
            global.metrics.count('app.fatal.errors');
            global.metrics.flush();
        }
        process.exit(4);
    }
}


//
// Middleware
//

/**
 * Setup a domain for each request to handle errors and give each request a serial.
 * Do this as the first thing!
 * @private
 * @param {object} req - Request object
 * @param {object} res - Response object
 * @param {function} next - Pass along to the next piece of middleware
 */
function addSerialAndDomain(req, res, next) {
    let requestDomain = Domain.create();

    // Generate serial for this request.
    res.locals.logMeta = {
        serial: Useful.getRequestSerial(req)
    };
    // Start connection timer for metrics
    res.locals._metricsStart = process.hrtime();
    res.set('x-servedByHost', req.socket.localAddress || process.env.SERIAL || 'unknown');

    Domain.active = requestDomain;
    requestDomain.add(req);
    requestDomain.add(res);

    requestDomain.on('error', (error) => {
        global.log.error(`Michonne mishandled a domain request: ${error.message}`);
        global.log.error(error.stack);

        // Setting health check to an error will cause monit to restart the server
        global.healthcheckStatus = 500;

        //  propagate 500 errors to app
        //  res.statusCode will be set in the response
        next(new HttpError(500));
    });

    res.on('end', () => {
        global.log.error(`Michonne disposed of domain for path ${req.path}`, res.locals.logMeta);
        requestDomain.dispose();
    });

    // Don't log healthchecks and internal requests
    if (req.path.charAt(1) !== '_') {
        if (log.do_debug === true) {
            log.debug(`Starting request: ${req.url}`, res.locals.logMeta);
        }
    }
    requestDomain.run(next);
}


/**
 * Log all requests
 * @private
 * @param {object} req - Request object
 * @param {object} res - Response object
 * @param {function} next - Pass along to the next piece of middleware
 */
function logTransaction(req, res, next) {
    if (req.path.charAt(1) !== '_') {
        let port = req.socket.localPort || req.socket.address().port;

        hcHits++;
        if (global.metrics !== null) {
            let proto = (port === global.sslPort) ? 'https' : 'http';

            global.metrics.count(`requests.${proto}.count`);
        }

        if (log.do_info === true) {
            log.info(`Request received from ${req.ip}`, {
                serial: res.locals.logMeta.serial,
                method: req.method,
                port: port,
                protocol: req.protocol,
                host: req.hostname,
                url: req.url
            });
        }
    }
    next();
}


/**
 * Ensure that the route only handles internal requests.
 * This should be the second piece of middleware, right after domain creation!
 * @private
 * @param {object} req - Request object
 * @param {object} res - Response object
 * @param {function} next - Pass along to the next piece of middleware
 */
function internalOnly(req, res, next) {
    let
        protectedPaths = [
            '/_healthcheck',
            '/_stats',
            '/_config_healthcheck'
        ];

    // Make sure request time is set
    if (!req.requestTime) {
        req.requestTime = Date.now();
    }

    // Set x-servedByHost header
    res.set('x-servedByHost', req.socket.localAddress || process.env.SERIAL || 'unknown');

    // These will be useful later
    res.locals.trustedInternal = trustHostTest(req.connection.remoteAddress);
    res.locals.trustedPartner = trustPartnerTest(req.connection.remoteAddress);
    res.locals.trustedUser = trustHostTest(req.ip);
    res.locals.requestedViaCDN = req.get(cdnBeaconHeader) ? true : false;

    if (res.locals.trustedInternal === false) {
        // If we are using a CDN, reject external requests directly to the ELB or origin
        if (global.config.flags.usingCDN === true && res.locals.requestedViaCDN === false) {
            if (log.do_debug === true) {
                log.debug(`Request for "${req.path}" was denied to host/address ${req.ip} because it bypassed the CDN`);
            }
            next(new HttpError(403));  // Reject request with 403 error
            return;
        }
        if (res.locals.trustedUser === false) {
            // Prevent easy external access to the protectedPaths and preview
            if (req.path.length > 4 && req.path.charAt(1) === '_' && protectedPaths.indexOf(req.path) !== -1) {
                if (log.do_warn === true) {
                    log.warn(`Request for "${req.path}" was denied to host/address ${req.ip}`);
                }
                next(new HttpError(403));  // Reject request with 403 error
                return;
            }
            if (((!req.hostname || req.hostname.search(/preview\./) !== -1) ||
                (req.path.length > 9 && req.path.indexOf('/preview/') === 0)) &&
                !req.get('x-fave-env') && !req.get('x-fave-srv')) {

                if (log.do_warn === true) {
                    log.warn(`Request for "${req.path}" was denied to host/address ${req.ip} because preview only works internally`);
                }
                next(new HttpError(403));  // Reject request with 403 error
                return;
            }
        }
    } else if ((res.locals.trustedUser === false || res.locals.requestedViaCDN === true) &&
        ((!req.hostname || req.hostname.search(/preview\./) !== -1) ||
        (req.path.length > 9 && req.path.indexOf('/preview/') === 0)) &&
        !req.get('x-fave-env') && !req.get('x-fave-srv')) {

        if (log.do_warn === true) {
            log.warn(`Request for "${req.path}" was denied to host/address ${req.ip} because preview does not work through the CDN`);
        }
        next(new HttpError(403));  // Reject request with 403 error
        return;
    }
    next();
}


/**
 * Track Signal Sciences RPC response time.
 *
 * @private
 * @param {object} req - Request object
 * @param {object} res - Response object
 * @param {function} next - Pass along to the next piece of middleware
 */
function handleSigSciResponse(req, res, next) {
    global.metrics.histogram('requests.sigSci.response.time', Useful.getHrDiff(res.locals._sigSciStartTime));
    next();
}


/**
 * Call Signal Sciences middleware.  Should be done early, before any routing.
 *
 * @private
 * @param {object} req - Request object
 * @param {object} res - Response object
 * @param {function} next - Pass along to the next piece of middleware
 */
function callSigSci(req, res, next) {
    if (global.config.runtime.features.enableSignalSciences === true) {
        if (global.metrics !== null && global.countersOnly === false) {
            res.locals._sigSciStartTime = process.hrtime();
            sigscimw(req, res, handleSigSciResponse.bind(global, req, res, next));
        } else {
            sigscimw(req, res, next);
        }
    } else {
        next();
    }
}


/**
 * Toggles the health check status, will allow for the healthcheck to fail,
 * even when the application is running well. Useful at times when you want
 * the site to go into offline mode.
 *
 * @private
 * @param {object} req - Request object
 * @param {object} res - Response object
 * @param {function} next - Pass along to the next piece of middleware
 */
function healthcheckToggle(req, res, next) {
    var status = Number(req.body.status) || 503,
        retry = req.body.retry || false;

    if (log.do_warn === true) {
        log.warn(`Current health check status (${global.healthcheckStatus}) is being changed to status (${status})`);
    }

    // Set the status
    global.healthcheckStatus = status;

    // Clear interval if it's running
    if (offlineTimer) {
        clearInterval(offlineTimer);
    }

    if (status === 503 && retry) {
        // Setup interval to modify retry header
        offlineCounter = Number(retry) || defaultRetry;
        offlineTimer = setInterval(function () {
            if (offlineCounter > 0) {
                offlineCounter--;
            } else {
                offlineCounter = defaultRetry;
            }
        }, 1000);
    }

    next();
}


/**
 * Does a quick health check for docker
 * @private
 * @param {object} req - Request object
 * @param {object} res - Response object
 */
function quickHealthcheck(req, res) {
    res.send('OK');
}


/**
 * Does a full stack health check. This call goes through PAL and Ibis.
 * @private
 * @param {object} req - Request object
 * @param {object} res - Response object
 */
function healthcheckMiddleware(req, res) {
    global.pal.getSection('/index.html', {
        editionType: global.edition
    }, function healthCheckPalHandler(error, _result) {
        res.set(noCacheHeaders);  // Don't cache healthchecks
        if (error) {
            // Setting health check to an error will cause monit to restart the server
            global.healthcheckStatus = 500;

            res.status(500).send(error);
        } else {
            res.send('health check');
        }
    });
}


/**
 * Handles the healthcheck request
 * @private
 * @param {object} req - Request object
 * @param {object} res - Response object
 */
function handleHealthcheck(req, res) {
    let date = new Date(),
        now = date.getTime(),
        diff = now - hcStart,
        status = global.healthcheckStatus,
        message = {
            name: packageConfig.name,
            version: global.buildVersion,
            hits: hcHits,
            uptime: diff,
            epochMs: now,
            utcDate: date.toUTCString(),
            status: status,
            siteStatus: status === 200 ? 'online' : 'OFFLINE',
            env: process.env,
            runtimeConfig: global.config.runtime
        };

    if (status === 503 && offlineCounter) {
        res.set('Retry-After', offlineCounter);
    }

    res.set(noCacheHeaders);  // Don't cache healthchecks
    res.status(status).json(message);
}


/**
 * Handles errors
 * @private
 * @param {Error} err - Error object
 * @param {object} req - Request object
 * @param {object} res - Response object
 * @param {object} next - Continuation object
 */
function errorHandler(err, req, res, next) {
    let code,
        msg;

    if (!err || !err.statusCode || err.statusCode <= 0 || err.statusCode >= 600) {
        // Default error
        err = new HttpError((err && err.message), 500);
    }
    code = err.statusCode || 500;
    msg = err.message;

    // Handle error messages semi-intelligently
    if (res.headersSent) {
        if (global.metrics !== null) {
            let proto = ((req.socket.localPort || req.socket.address().port) === global.sslPort) ? 'https' : 'http';
            global.metrics.count(`requests.${proto}.response.errors`);
        }
        if (log.do_warn === true) {
            log.warn(`Error ${code} after response already written.`, res.locals.logMeta);
        }
        // Send to default error handler
        next(err);
        return;
    }

    res.set(errorHeaders);  // Limit caching for error responses
    if (!req.xhr && !res.locals.isXhr && req.hostname !== global.config.hostnames.assetHost) {
        // Send "long" error page
        msg = global.errorPages[code] || global.errorPages.default;
    }
    res.status(code).send(msg);
    if (responseSent !== null) {
        responseSent(req, res);
    }
    if (log.do_info === true) {
        log.info(`Sent error ${code} response.`, res.locals.logMeta);
    }
}


app.use(addSerialAndDomain);
if (useGzip) {
    app.use(Compress());
}
if (Useful.willLog('info')) {
    app.use(logTransaction);
}
app.use(internalOnly);

// Call Signal Sciences thingy
app.use(callSigSci);


function started() {
    global.healthcheckStatus = 200;  // Initialize this to 200
    if (log.do_info === true) {
        log.info(`Michonne started (monit status ${global.healthcheckStatus}).`);
    }
    if (global.metrics !== null) {
        global.metrics.count('app.started');
        if (global.countersOnly === false) {
            global.metrics.histogram('app.startTime', Useful.getHrDiff(startedAt));
        }
    }
}


function startItUp(error, result) {
    if (!error && result) {
        try {
            addProxyContentRoute(result.routeTables);
            router = new RouteOMatic(romConfig, result, app);
            if (router && router.isValid()) {
                global.srvConfig.routes = result;
                if (log.do_debug === true) {
                    log.debug(`Processed initial routes from ${global.config.host.data}${global.config.dataPaths.routes}`);
                }
            } else {
                error = new Error(`Failed to retrieve routes from ${global.config.host.data}${global.config.dataPaths.routes}.`);
            }
        } catch (e) {
            error = e;
        }
    }
    if (error) {
        log.error(error);
        log.fatal(`Fatal error processing routes: ${error.message}\nAborting.`);
        if (global.metrics !== null) {
            global.metrics.count('app.fatal.errors');
            global.metrics.flush();
        }
        process.exit(3);
    }

    // Add default route to 404 unmatched things
    app.all('*', function (req, res, next) {
        next(new HttpError(404));
    });

    // Add error handler, should be the last thing in the list of routes and middleware
    app.use(errorHandler);

    if (log.do_verbose === true) {
        log.verbose(`Using ${maxConnects} max. server connections and ${maxSockets} max. sockets`);
    }
    if (log.do_info === true) {
        log.info(`Michonne initialized:\n    Edition = ${global.edition}\n    Environment = ${global.environment}\n    Using PAL URL: ${global.palUrl}\n    HTTP using port ${global.port}\n` +
            (global.sslPort === 0 ? '' : '    ' + (global.config.flags.enableHttp2 ? 'HTTP/2' : 'HTTPS') +
            ` using port ${global.sslPort}.\n`) + '\nPrepare for excitement...\n');
    }

    // Start the server
    if (global.sslPort !== 0) {
        httpServer = http.createServer(app).listen(global.port);
        if (tlsOptions !== null) {
            // We want to use local certs and serve HTTPS directly
            httpsServer = https.createServer(tlsOptions, app).listen(global.sslPort, started);
        } else {
            // We will be serving HTTPS with an HTTP connection to an ELB, VIP, or CDN that handles the SSL
            httpsServer = http.createServer(app).listen(global.sslPort, started);
        }
        httpsServer.maxConnections = maxConnects;
    } else {
        httpServer = http.createServer(app).listen(global.port, started);
    }
    httpServer.maxConnections = maxConnects;
}

// Initial system routes
app.get(/^\/_(?:dock_hc|edge_healthcheck)$/, quickHealthcheck);
app.get(/^\/_hc$/, healthcheckMiddleware);
app.get(/^\/_healthcheck$/, handleHealthcheck);
app.post(/^\/_config_healthcheck$/, [bodyParser.urlencoded({extended: false}), healthcheckToggle, handleHealthcheck]);

// RouteOMatic
// If testing, just use the default routes (from the node_modules/trinity dir)
if (global.config.isTest === true) {
    if (log.do_debug === true) {
        log.debug('Updated routes from local file (testing)');
    }
    startItUp(null, global.srvConfig.routes);
} else {
    // Normal run, so wait until we get the routes we need before starting
    runtime.get(global.config.dataPaths.routes, startItUp);

    // Setup rechecking for runtime config values
    setInterval(runtime.get.bind(runtime, global.config.dataPaths.routes, getRuntimeRoutesCallback), global.config.dataTtl).unref();
}


//
// Go process, be free.
// Chasing your tail mindlessly,
// like an insane dog.
//
//   -- Event Loop Haiku
//

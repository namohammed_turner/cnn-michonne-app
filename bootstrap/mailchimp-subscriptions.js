/* global CNN */

CNN.INJECTOR.loadFeature('footer').then(
    function () {
        'use strict';
        require('../src/js/mailchimp-subscriptions.js');
        CNN.INJECTOR.scriptComplete('mailchimp');
    }
);

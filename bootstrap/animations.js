/* global CNN */
/* this file is used by webpack to inline video bundles */


/*

The webpack string-replace loader is used to make iphone-inline-video work inside
of webpack.

WHAT IS DONE:

{
    test: /iphone-inline-video.browser.js/,
    loader: 'string-replace',
    query: {
        search: 'var makeVideoPlayableInline',
        replace: 'window.CNN.makeVideoPlayableInline'
    }
}

WHY IT IS DONE:

Because the auther of the iphone inline player library creates a local Variable
and local variables aren't exposed by webpack.

*/

require('../../bower_components/iphone-inline-video/dist/iphone-inline-video.browser.js');
require('../src/js/elements/animations.js');
CNN.INJECTOR.scriptComplete('animations');

/* global CNN */

/*
 * This file is used by webpack to generate a video resource bundle with code that
 * is common to all pages that will have a video player.
 */

CNN.INJECTOR.loadFeature('footer').then(
    function () {
        'use strict';
        if (jQuery('link#wp-video--css').length === 0) {
            jQuery('head').append('<link rel="stylesheet" type="text/css" id="wp-video--css" href="/css/@@BASEVERSION/wp-video.css">');
        }
        require('../src/js/video-player.js');
        require('../src/js/elements/video/video-pinner.js');
        CNN.INJECTOR.scriptComplete('video');
    }
);

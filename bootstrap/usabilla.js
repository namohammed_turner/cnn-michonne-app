/* global CNN */
/* this file is used by webpack to generate usabilla resource bundles */

/* The Footer Must Be Loaded Before Usabilla Is Executed */
CNN.INJECTOR.loadFeature('footer').then(
    function () {
        'use strict';
        require('../src/js/vendor/usabilla.min.js');
    }
);

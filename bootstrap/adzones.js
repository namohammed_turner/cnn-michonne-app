/* global CNN */
/*
This file is used by webpack to create a bundle that manages the layout and
locking of ads in the right rail.

Desktop Ad Template:
Webpack converts this HTML file into a string and injects it into the loadAds module.

The loadAds module replaces all instances {epicAdId} with the ad id.  The ad id is
configured in michonne.

Outbrain Template:
Webpack converts this HTML file into a string and injects it into the loadAds module.

*/



var adTemplate = require('../src/js/elements/ads/adTemplate.html'),
    isMobile = CNN.navigation && CNN.navigation.isMobileDevice,
    outbrainTemplate = require('../src/js/elements/ads/outbrainTemplate.html'),
    adLoader = require('../src/js/elements/ads/loadAds.js'),
    lockZones = require('../src/js/elements/ads/lockZones.js');


/*
    (CNN.rightRailConfig && CNN.rightRailConfig.longArticle && CNN.rightRailConfig.shortArticle && rightRailConfig.longArticle.epic)

    means that the right rail config is configured for short and long articles
*/


if (isMobile === false && (CNN.rightRailConfig && CNN.rightRailConfig.longArticle && CNN.rightRailConfig.shortArticle  && CNN.rightRailConfig.longArticle.epic)) {
    require('../src/js/elements/ads/articleLengthWatch.js');
    adLoader.initializeData();
    adLoader.loadAd(adTemplate);

    if (CNN.Features && CNN.Features.enableOutbrain) {
        adLoader.loadOutbrain(outbrainTemplate);
    }
    adLoader.setupHeightChangeHandler(adTemplate);

    if (CNN.rightRailConfig.longArticle.enableAdLock) {
        lockZones.initializeData();
        lockZones.setupRightRailObserver();
        lockZones.executeLayoutChanges();
    }
}

/* global CNN */

CNN.INJECTOR.loadFeature('footer').then(
    function () {
        'use strict';
        require('../src/js/financial-base.js');
        require('../src/js/financial-currencies.js');
        require('../src/js/financial-markets.js');
    }
);

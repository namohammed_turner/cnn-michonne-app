/* global CNN, CNNVIDEOAPI */

/* This file is used by webpack to generate a videoLoader (CNN Video Loader API) resource bundle */

CNN.INJECTOR.executeFeature('video').then(
    function () {
        'use strict';
        var bootstrapper;
        window.CNNVIDEOAPI = window.CNNVIDEOAPI || {};
        /* CVP Library */
        require('../src/js/vendor/cvp/cvp.js');
        /* Amazon Ad Manager */
        require('../src/js/elements/video/amazon-video-ads/amazon-video-ads-manager.js');


        /* Define callback function that is called by the contents of apimapping.json */
        window.cnn_onAPIMappingLoadComplete = function (data) {
            CNNVIDEOAPI.client = 'expansion';
            CNNVIDEOAPI.apiMapping = data;
            CNNVIDEOAPI.currentVersion = CNNVIDEOAPI.apiMapping.version;
            /* Update the config paths*/
            if (typeof CNNVIDEOAPI.client !== 'undefined' &&
                typeof CNNVIDEOAPI.apiMapping.client !== 'undefined' &&
                typeof CNNVIDEOAPI.apiMapping.client[CNNVIDEOAPI.client] !== 'undefined') {
                if (typeof CNNVIDEOAPI.apiMapping.client[CNNVIDEOAPI.client].config_paths !== 'undefined') {
                    CNNVIDEOAPI.deviceType = typeof CNNVIDEOAPI.deviceType === 'undefined' ? 'desktop' : CNNVIDEOAPI.deviceType;
                    CNNVIDEOAPI.configPaths = CNNVIDEOAPI.apiMapping.client[CNNVIDEOAPI.client].config_paths;
                }
            }
        };
        require('../../bower_components/cnn-video-loader/dist/config/apimapping.json');

        if (!(window.debugMode === undefined || window.debugMode === false || window.debugMode === 'false')) {
            require('../../bower_components/cnn-video-loader/dist/vendor/loggerImpl.js');
        }

        require('../../bower_components/cnn-video-loader/dist/js/CNNVideoBootstrapper.js');
        /* Pass the boolean false as an argument instead of the usual API URL to prevent Video Loader from fetching dependencies */
        bootstrapper = new window.CNNVIDEOAPI.Bootstrapper(false);

        if (!CNNVIDEOAPI.CNNVideoPlayerBase) {
            require('../../bower_components/cnn-video-loader/dist/js/CNNAPIVideoPlayer.js');
        }

        require('../../bower_components/json3/lib/json3.js');

        if (!jQuery.deparam) {
            require('../../bower_components/jquery-bbq/jquery.ba-bbq.js');
        }

        if (!window.VideoPlayer) {
            require('../../bower_components/cnn-video-loader/dist/js/CNNVideoPlayer.js');
        }

        bootstrapper.initialize();

        CNN.INJECTOR.scriptComplete('videoLoader');
    }
);

/*
 * CNN HEADER - All of the libraries that should be synchronously loaded
 * throughout CNN
 *
 * There are three issues that are awkardly resolved because these files are
 * converted to a module as part of bundling:
 *
 * 1) Several files (modules) need access to jquery; This is handled in
 *    the webpack config.
 * 2) The jquery libary must be exposed to the windows names space; This is
 *    handled in the webpack config.
 * 3) The this variable should be window; This is handled in the webpack config.
 *
 * note: jquery only gets exposed to the global namespace as a side effect of
 * one or more of these modules.
 */

require('../src/js/page-events.js');
require('../src/js/throttle-event.js');
require('../../bower_components/passive-events/dist/passive-events.js');
require('../src/js/vendor/modernizr.3.3.1.js');
require('../../bower_components/mobile-detect/mobile-detect.js');
require('../../bower_components/mobile-detect/mobile-detect-modernizr.js');
require('../../bower_components/fastdom/fastdom.js');
require('../src/js/fastdom-extend.js');
require('../src/js/current-size.js');
require('../src/js/pages/common/cnn-ads-freewheel-options.js');
require('../src/js/vendor/amazon/amzn_ads.js');
require('../../bower_components/fa-injector/dist/injector.lite.js');
require('../../bower_components/eq.js/build/eq.js');


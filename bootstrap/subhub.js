/* global CNN */
/* this file is used by webpack to generate a SubHub resource bundles */

/* The Footer Must Be Loaded Before SubHub Is Executed */
CNN.INJECTOR.loadFeature('footer').then(
    function () {
        'use strict';
        require('../src/js/newsletter.js');
        require('../src/js/vendor/gigya/lite-signup.cnn.js');
    }
);

/* global CNN */

CNN.INJECTOR.loadFeature('footer').then(function () {
    'use strict';
    require('../src/js/elements/maps.js');
    CNN.INJECTOR.scriptComplete('maps');
});

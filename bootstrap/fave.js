/* global CNN, FAVE */

/* This file is used by webpack to generate a Future Aligned Video Experience (FAVE) Library resource bundle */

CNN.INJECTOR.loadFeature('footer').then(function () {
    'use strict';
    /* Declare the FAVE namespace and settings */
    require('../src/js/elements/video/components/faveSettings.js');
    /* Add the Omniture component to the FAVE settings */
    require('../src/js/elements/video/components/omniture.js');
    /* Fetch the FAVE Library */
    CNN.INJECTOR.loadFeatureForSource(FAVE.settings.injectorJs.featureName, FAVE.settings.injectorJs.source).then(function () {
        CNN.INJECTOR.scriptComplete('fave');
    });
});

/* global CNN */
/* this file is used by webpack to generate weather resource bundles */

/* The Footer Must Be Loaded Before Weather Is Executed */
CNN.INJECTOR.loadFeature('footer').then(
    function () {
        'use strict';
        if (!window.Cipher) {
            require('../../bower_components/cnn-cipher/dist/cnn.cipher.js');
        }

        require('../src/js/pages/common/cnn-svcs-weather.js');
    }
);

/* global CNN */

/* This file is used by webpack to generate a video experience resource bundle */

require('../src/js/elements/video/client/video-experience-unification.js');
CNN.INJECTOR.scriptComplete('videx');

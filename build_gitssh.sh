#!/bin/sh

our_directory="$(dirname $0)"
chmod 0600 "${our_directory}"/.ssh/id_rsa
exec /usr/bin/ssh -o StrictHostKeyChecking=no -i "${our_directory}/.ssh/id_rsa" "$@"

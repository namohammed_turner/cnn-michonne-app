#!/bin/bash

TOPDIR=$(dirname $0)/..
if [ "${TOPDIR}" != "." -a -n "${TOPDIR}" ]; then
    cd $TOPDIR
fi

NODE_TYPE='node'
NODE_ENGINE=''
if [ -r '.nvmrc' ]; then
    NODE_ENGINE=$(cat '.nvmrc' | tr -d 'A-Za-z~>=^')
fi
if [ -z "${NODE_ENGINE}" ]; then
    NODE_ENGINE=$(jq -r .engines.node package.json | tr -d 'A-Za-z~>=^')
fi
PACKAGE_NAME=$(jq -r .name package.json)
START_COMMAND=$(jq -r .scripts.start package.json)
PACKAGE_MAIN=$(jq -r .main package.json)

if [ -z "${NODE_ENGINE}" -o -z "${PACKAGE_NAME}" -o -z "${START_COMMAND}" ]; then
    echo "Unable to build Dockerfile!"
    exit 1
fi

sed "s/@@nodeType/${NODE_TYPE}/g;\
    s/@@nodeEngine/${NODE_ENGINE}/g;\
    s/@@packageName/${PACKAGE_NAME}/g;\
    s/@@startCommand/${START_COMMAND}/g;\
    s/@@packageMain/${PACKAGE_MAIN}/g" Dockerfile.template > Dockerfile


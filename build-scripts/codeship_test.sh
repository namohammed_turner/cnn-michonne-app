## A copy of this script is in the repository at:
## /build-scripts/codeship_test.sh
## If you make changes directly on Codeship, make sure to check in the changes
## to the repository.  It is actually best to update the one in source control
## and copy/paste the script into the Codeship page.
## Maintained by james.young@turner.com

#### SETUP ####

## Install node engine
nvm install "$(jq -r '.engines.node' package.json)"

## Install npm engine
npm install --global npm@"$(jq -r '.engines.npm' package.json)"

## Version checks
nvm --version
node --version
npm --version

## Install AWS CLI
pip install awscli

## Clean npm cache
npm cache clean
rm -rf ~/clone/node_modules

## Install Grunt
npm install --global grunt-cli

## Configure Node Envrionment
npm config set geoip-lite:update false

## Install Dependencies
test -x ~/clone/build-scripts/expandodeps.sh && ~/clone/build-scripts/expandodeps.sh || true

## Install Trinity
test -x ~/clone/build-scripts/expandodeps.sh && npm install trinity || true

## Install Package
test -x ~/clone/build-scripts/expandodeps.sh || npm install

## Test Source Code
grunt test-codecheck

## Build Package
grunt build


#### Pipeline - Test Commands ####

## Run Tests
source set-env/${BUILD_ENV}.conf
grunt test-unitcheck

## Generate Documentation
npm run generate-docs

## Copy documentation out for uploading to documentation server later
cp -r docs ~/

## Generate Dockerfile
#npm run generate-dockerfile


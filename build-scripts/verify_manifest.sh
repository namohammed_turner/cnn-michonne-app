#!/bin/sh
#
# Verify the contents of the manifest.js were created
#

TOPDIR=$(dirname $0)/..

CSSLIST="global.css pages/static-header-skinny.css pages/page.css pages/static-header.css pages/static-footer.css pages/static-edition-picker.css"

cd $TOPDIR >/dev/null

# Check for manifest file passed-in as a command-line option
MANIFEST=""
if [ $# -lt 1 ]; then
    echo "Missing required manifest file name and public base path."
    exit 1
fi
MANIFEST="${1}"
if [ ! -r "${MANIFEST}" ]; then
    echo "Unable to read manifest file (${MANIFEST})!"
    exit 2
fi

# Check for public path passed-in as a command-line option
CSSPATH=""
if [ $# -lt 2 ]; then
    echo "Missing required public base path."
    exit 1
fi
CSSPATH="${2}css"
JSPATH="${2}js"

echo "Verifying JS files from manifest..."
errorcount=0
for f in $(awk -v jsp=$JSPATH '/^([ \t]*"dest"\:)?[ \t]*"tmp\/dist\// { match($0, /tmp\/dist\/[^"]+/); print jsp substr($0, RSTART+8, RLENGTH-8); }' $MANIFEST) ; do
    if [ -n "$f" ]; then
        if [ ! -s $f ]; then
            echo "Javascript file \"$f\" was not created!"
            errorcount=$((errorcount+1))
        fi
    fi
done

if [ $errorcount -gt 0 ]; then
    echo "ERROR:  Missing $errorcount javascript file(s).  Aborting."
    exit 1
fi

# Now check CSS files, if we have the CSS path
if [ -n "${CSSPATH}" ]; then
    echo "Verifying CSS files in ${CSSPATH}..."
    errorcount=0
    if [ $? -ne 0 ]; then
        echo "ERROR:  CSS directory not found!  Aborting."
        exit 2
    fi

    if [ $errorcount -gt 0 ]; then
        echo "ERROR:  Missing $errorcount CSS file(s).  Aborting."
        exit 2
    fi
fi

# All good
exit 0

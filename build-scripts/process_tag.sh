#!/bin/bash
#
#  Return the artifact tag to use derived from the CI_BRANCH
#

tag=""
case "${CI_BRANCH}" in
    *--sweet) tag=".${CI_BRANCH}" ;;
    *--enable) tag=".${CI_BRANCH}" ;;
    *--terra) tag=".${CI_BRANCH}" ;;
    *--politics) tag=".${CI_BRANCH}" ;;
    *--video) tag=".${CI_BRANCH}" ;;
    *--prod) tag=".${CI_BRANCH}" ;;
    feature/CNN-*) tag=".${CI_BRANCH}--sweet" ;;
    feature/CNNWE-*) tag=".${CI_BRANCH}--enable" ;;
    feature/CNNINTL-*) tag=".${CI_BRANCH}--terra" ;;
    feature/CNNPOL-*) tag=".${CI_BRANCH}--politics" ;;
    feature/CNNVID-*) tag=".${CI_BRANCH}--video" ;;
    feature/*) tag=".${CI_BRANCH}--dev" ;;
    enable) tag=".enable" ;;
    politics) tag=".politics" ;;
    sweet) tag=".sweet" ;;
    terra) tag=".terra" ;;
    video) tag=".video" ;;
    hotfix[-_/]*) tag=".${CI_BRANCH}--prod" ;;
    develop) tag=".prod" ;;
    master) tag="" ;;
    *) tag=".dev" ;;
esac
echo $tag | sed 's/\//-/g'
exit 0


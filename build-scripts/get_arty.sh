#!/bin/sh
#
# Script to build Michonne, make an artifact, and get it in Argo
#

#PATH=/usr/bin:/usr/local/bin:/usr/local/sbin:/usr/sbin:/bin

COMMANDS="awk bundle curl gpg grunt jshint npm openssl ruby s3cmd sed tar"

# Exit function
doExit () {
    ex=0
    if [ "$#" = "0" ]; then
        ec=20
    else
        ec=$1
    fi
    if [ -f package.json.bak ]; then
        rm -rf bin lib/node_modules
        mv -f package.json.bak package.json
    fi
    exit $ec
}

# Verify O.S.
if [ `uname -s` != "Linux" ]; then
    echo "You can only build artifacts on a Linux system!"
    exit 1
fi

echo ""
echo " *** Getting Started ***"
echo ""

# Get build number
if [ "$#" != "0" ]; then
    BUILDNUM=${1}
fi
if [ -z "${BUILDNUM}" ]; then
    echo "Missing build number!"
    echo "usage:  $0 <build_number>"
    exit 2
else
    echo "NOTE:  Using build number \"${BUILDNUM}\""
fi

# Make sure we are in the build directory
TOPDIR=`dirname $0`/..
cd $TOPDIR >/dev/null
if [ $? -ne 0 ]; then
    echo "Failed to enter main build directory!"
    exit 3
fi

# Get details from package
BUILDVER=`awk '/"version"\: "[0-9]+\.[0-9]+\.[0-9]+"/ { gsub(/[\"\,]/,"",$2); print $2 }' package.json`
if [ -z "${BUILDVER}" ]; then
    echo "Failed to find the application version from the package.json file!"
    exit 3
fi
sed -i.bak "s/\"version\": \"${BUILDVER}\"/\"version\": \"${BUILDVER}-${BUILDNUM}\"/" ./package.json
if [ $? -ne 0 ]; then
    echo "Failed to update package.json with build version."
    doExit 3
fi
BUILDVER="${BUILDVER}-${BUILDNUM}"
ARTNAME="cnn-michonne-app-${BUILDVER}.tar.gz"
NODEVER=`awk '/"node"\:/ { gsub(/[\"\,]/,"",$2); print $2 }' package.json`
if [ -z "${NODEVER}" ]; then
    echo "Failed to find the node version from the package.json file!"
    doExit 3
fi
NODEPKG="node-v${NODEVER}-linux-x64.tar.gz"
NODEDIR="tmp/node-v${NODEVER}-linux-x64"

echo ""
echo " *** Verifying Stuff is in PATH for Build ***"
echo ""
for p in ${COMMANDS}; do
    which $p >/dev/null 2>&1
    if [ $? -ne 0 ]; then
        echo "Failed to find \"${p}\"!  Install it before continuing."
        doExit 5
    fi
done

echo ""
echo " *** Installing Stuff for Build ***"
echo ""
npm install
if [ $? -ne 0 ]; then
    echo "Installing stuff failed."
    echo ""
    echo "If it was a ruby gem failure and mentioned trying \"bundle upgrade some_package\","
    echo "than follow those instructions and try again."
    echo ""
    echo "See https://bitbucket.org/vgtf/cnn-michonne-app/overview for other potential build tips."
    doExit 6
fi

echo ""
echo " *** Building Application ***"
echo ""
npm run build
if [ $? -ne 0 ]; then
    echo "Building failed."
    echo ""
    echo "If you don't know what happened or why things failed, try re-running \"npm run build\""
    echo "to see what might have gone wrong."
    echo ""
    echo "See https://bitbucket.org/vgtf/cnn-michonne-app/overview for other potential build tips."
    doExit 7
fi

# Make sure we have a viable tmp directory
if [ ! -d tmp ]; then
    mkdir -m 0777 tmp
    if [ $? -ne 0 ]; then
        echo "Failed to create tmp directory!"
        doExit 7
    fi
fi
# Download nodejs package if we don't aleady have it
if [ ! -f "tmp/${NODEPKG}" ]; then
    echo "Retrieving Node Package: ${NODEPKG}"
    curl -s -o tmp/${NODEPKG} http://nodejs.org/dist/v${NODEVER}/${NODEPKG}
    if [ $? -ne 0 ]; then
        echo "Failed to retrieve nodejs package!"
        doExit 4
    fi
fi
if [ ! -d ${NODEDIR} ]; then
    cd tmp
    tar xzf ${NODEPKG}
    if [ $? -ne 0 ]; then
        echo "Failed to extract nodejs package!"
        cd ..
        doExit 4
    fi
    cd ..
fi

echo ""
echo " *** Building Artifact ***"
echo ""
rm -rf bin lib/node_modules
cp -rp ${NODEDIR}/bin ./bin
cp -rp ${NODEDIR}/lib/node_modules ./lib/node_modules
if [ ! -f bin/node ]; then
    echo "Failed to install required node binary in bin directory!"
    doExit 8
fi
tar czf tmp/${ARTNAME} --exclude=".git*" --exclude=.npm --exclude=tmp --exclude=vagrant --exclude="^.node-gyp" *
if [ $? -ne 0 ]; then
    echo "Failed to build tar file from directory contents!"
    doExit 8
fi
echo "Built artifact ${ARTNAME}"
CHECKSUM=`openssl dgst -sha256 tmp/${ARTNAME} | awk '{ print $2 }' -`
if [ -z "${CHECKSUM}" ]; then
    echo "Failed to find checksum on artifact tar file!"
    doExit 8
fi

echo ""
echo " *** Pushing Artifact to S3 ***"
echo ""
s3cmd -c .s3cfg put tmp/${ARTNAME} s3://artifacts-east/cnn/michonne-app/${ARTNAME}
if [ $? -ne 0 ]; then
    echo "Failed to write artifact to S3 service!"
    doExit 9
fi

echo ""
echo " *** POST Artifact to Argo ***"
echo ""
curl -H "Content-Type: application/json" -sv -X POST http://deployit.lax.dmtio.net:19601/artifacts/cnn-michonne/${BUILDVER} --data-binary "{ \"type\": \"slug\", \"checksum\": \"${CHECKSUM}\", \"url\": \"https://artifacts:negvsnpgf@artifacts.api.56m.vgtf.net/cnn/michonne-app/${ARTNAME}\" }"
if [ $? -ne 0 ]; then
    echo "Failed to POST artifact to Argo!"
    doExit 10
fi

echo ""
echo "Michonne artifact ${BUILDVER} succesfully built and deployed to Argo."
echo ""
doExit 0


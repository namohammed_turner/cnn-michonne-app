#!/bin/bash
#
#  Determine the current branch and output the corresponding environment
#
# NOTE:  If you change this logic, also change the _processBranch function
#        in Gruntfile.js.
#

cenv=""
case "${CI_BRANCH}" in
    *--sweet) cenv="sweet" ;;
    *--enable) cenv="enable" ;;
    *--terra) cenv="terra" ;;
    *--politics) cenv="politics" ;;
    *--video) cenv="video" ;;
    *--prod) cenv="prod" ;;
    feature/*) cenv="dev" ;;
    feature/CNN-*) cenv="sweet" ;;
    feature/CNNWE-*) cenv="enable" ;;
    feature/CNNINTL-*) cenv="terra" ;;
    feature/CNNPOL-*) cenv="politics" ;;
    feature/CNNVID-*) cenv="video" ;;
    feature/*) cenv="dev" ;;
    enable) cenv="enable" ;;
    politics) cenv="politics" ;;
    sweet) cenv="sweet" ;;
    terra) cenv="terra" ;;
    video) cenv="video" ;;
    hotfix[-_/]*) cenv="prod" ;;
    develop) cenv="prod" ;;
    master) cenv="prod" ;;
    *) cenv="dev" ;;
esac
echo $cenv
exit 0


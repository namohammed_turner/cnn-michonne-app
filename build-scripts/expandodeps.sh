#!/bin/bash
#
# Expand-O-Deps script
#
# Pull dependency versions out of config files and make a useful checksum
# out of them, use that to generate a tarball name, look for that tarball
# in the dependency cache (on S3), if it exists then pull it down and
# use it, if not then generate it and push it up.
#
# Created -- J. Drenter / December 2015
#

# Files to include
PACKAGE='package.json'
BOWER='bower.json'
GEM='Gemfile'

# Directories to install in
NODEMODS="node_modules"
BOWERCOMPS="bower_components"
BUNDLE=".bundle"
VENDCACHE="vendor"

# Dependency names to skip
declare -a SKIP=('trinity')

# RSA key location
RSA_KEY=~/.ssh/id_rsa

# Encrypted ENV settings
ENCENV="build-scripts/codeship_deploy-env.enc"

# S3 Bucket
S3BUCKET='cnn-deps-store'

# Install command
INSTALL="npm install"

# Where to find JQ
JQ='jq'

# Where to find AWS
AWS='aws'

# Where to find openssl
OPENSSL='openssl'

#
# ----- SHOULD NOT NEED TO MODIFY ANYTHING BELOW HERE -----
#

OSNAME=$(uname -s)
OSVERS=$(uname -r)
NAME=""
TOPDIR=$(dirname $0)/..
TARDIRS=""

COLLECT="${OSNAME}||${OSVERS}"

#
# Processes each dependency, $1 = name, $2 = version
addDependency () {
    local sn=""
    for sn in "${SKIP[@]}"; do
        if [ "$sn" = "$1" ]; then
            return 1
        fi
    done
    COLLECT="${COLLECT}|$1--$2"
    return 0
}


#
# Get dependencies from a package/bower file, $1 = filename
getJSONDependencies () {
    local contents=""
    local i=0
    declare -a pns
    declare -a pvs

    COLLECT="${COLLECT}||$1"
    contents=$(cat $1)
    if [ -z "$NAME" ]; then
        NAME=$(echo ${contents} | ${JQ} -r '.name')
    fi
    pns=( $(echo ${contents} | ${JQ} -r '.engines + .dependencies + .devDependencies | to_entries | .[] | "\(.key)"') )
    pvs=( $(echo ${contents} | ${JQ} -r '.engines + .dependencies + .devDependencies | to_entries | .[] | "\(.value)"') )
    for (( i=0; i < ${#pns[@]}; i++ )) ; do
        addDependency ${pns[$i]} ${pvs[$i]}
    done
}


#
# Get dependencies from a Gemfile, $1 = filename
getGemDependencies () {
    local i=0
    declare -a gns
    declare -a gvs

    COLLECT="${COLLECT}||$1"
    gns=( $(grep -Eis '^gem[ \t]+' $GEM | awk -F "[\"\' \t,]+" '{print $2}' -) )
    gvs=( $(grep -Eis '^gem[ \t]+' $GEM | awk -F "[\"\' \t,]+" '{print $3}' -) )
    for (( i=0; i < ${#gns[@]}; i++ )) ; do
        addDependency ${gns[$i]} ${gvs[$i]}
    done
}


#
# Process the package file
if [ -n "${PACKAGE}" -a -s "${PACKAGE}" ]; then
    TARDIRS=${NODEMODS}
    getJSONDependencies ${TOPDIR}/$PACKAGE
fi
#
# Process the bower file
if [ -n "${BOWER}" -a -s "${BOWER}" ]; then
    TARDIRS="${TARDIRS} ${BOWERCOMPS}"
    getJSONDependencies ${TOPDIR}/$BOWER
fi
#
# Process the Gem file
if [ -n "${GEM}" -a -s "${GEM}" ]; then
    TARDIRS="${TARDIRS} ${BUNDLE} ${VENDCACHE}"
    getGemDependencies ${TOPDIR}/$GEM
fi

if [ -z "$NAME" ]; then
    echo "Unable to determine program name!" > /dev/stderr
    exit 2
fi

MD5="md5sum -"
which md5sum > /dev/null 2>&1
if [ $? -ne 0 ]; then
    which md5 > /dev/null 2>&1
    if [ $? -ne 0 ]; then
        echo "No md5sum or md5 executable found!  Aborting." > /dev/stderr
        exit 1
    fi
    MD5="md5 -r"
fi

CHKSUM=$(echo $COLLECT | $MD5 | awk '{print $1}' -)
if [ -z "${CHKSUM}" ]; then
    echo "Failed to determine MD5 checksum!" > /dev/stderr
    exit 3
fi

TARFILE="deps-${OSNAME}-${CHKSUM}.tgz"
echo "Looking for ${NAME}/${TARFILE}..."

#
# Decrypt and source the environment settings for AWS
$OPENSSL rsautl -decrypt -inkey $RSA_KEY -in $ENCENV -out ./envfile
source ./envfile
rm -f $ENCENV
if [ -z "${AWS_ACCESS_KEY_ID}" -o -z "${AWS_SECRET_ACCESS_KEY}" ]; then
    echo "AWS access environment variables are NOT set!" > /dev/stderr
    exit 3
fi

#
# Pull down the tarball, if it exists
$AWS s3 cp s3://${S3BUCKET}/${NAME}/$TARFILE ./$TARFILE --quiet
if [ $? -eq 0 ]; then
    # We got it, so use it
    echo "Extracting ${NAME}/${TARFILE}"
    tar xzf $TARFILE
else
    # Not found, so build it and push it up
    $INSTALL
    echo "Building ${NAME}/${TARFILE}"
    tar czf $TARFILE $TARDIRS
    if [ $? -eq 0 ]; then
        # Successfully built tar file, push it up
        echo "Pushing ${NAME}/${TARFILE} up to S3"
        $AWS s3 cp ./$TARFILE s3://${S3BUCKET}/${NAME}/$TARFILE --quiet
    fi
fi

#
# Remove the tarball, done with it
rm -f $TARFILE

exit 0

<div id="nav__plain-header" class="nav--plain-header">
    {> "views/pages/common/breaking-news" /}
    <div id="nav" class="nav {+selected-index-class /}">
        <div class="nav__color-strip"></div>
        <div class="nav__container">
            <a href="{@navUrlRenderer absolute=$var-use-absolute-urls host=request.domainUrl /}" id="logo" class="nav__logo"></a>

            <div class="nav-section">
                {#nav.buckets.subs}
                    {@eq key="{.selected}" value="selected"}
                        <div class="nav-section__name js-nav-section-name" data-analytics-header="main-menu_{.name}">
                            {?.url}
                                <a href="{@navUrlRenderer url=.url absolute=$var-use-absolute-urls host=request.domainUrl /}">
                            {/.url}
                            {.label}
                            {?.url}
                                </a>
                            {/.url}
                        <span class="nav-section__expand-icon">+</span></div>
                    {/eq}
                {/nav.buckets.subs}

                {#nav.buckets.subs}
                    {@eq key="{.selected}" value="selected"}
                        {?.subSection}
                            <div id="nav-section-submenu" class="nav-section__submenu" data-analytics-header="main-menu_{.name}">
                                {#.subs}
                                    {@eq key="{.selected}" value="selected"}
                                        <a class="nav-section__submenu-item nav-section__submenu-active" href="{#. $var-url=.url}{+nav-render-url /}{/.}">{.label}</a>
                                    {:else}
                                        <a class="nav-section__submenu-item" href="{#. $var-url=.url}{+nav-render-url /}{/.}">{.label}</a>
                                    {/eq}
                                {/.subs}
                            </div>

                            {! PAGE TITLE !}
                            <div id="js-nav-section-article-title" class="js-nav-section-article-title nav-section__article-title"></div>
                        {/.subSection}
                    {/eq}
                {/nav.buckets.subs}
            </div>

            {! TOP LINKS !}
            <div class="nav-menu-links">
                {#nav.buckets.subs}
                    {?.enableSectionLink}
                        <a class="nav-menu-links__link" href="{@navUrlRenderer url=.url absolute=$var-use-absolute-urls host=request.domainUrl /}" data-analytics-header="main-menu_{.name}">
                            {.label}
                        </a>
                    {/.enableSectionLink}
                {/nav.buckets.subs}
            </div>

            {! LIVE TV !}
            <a href="{config.runtime.pageBadge.link|s|u}" data-header="live" class="js-nav__live-tv nav__live-tv" id="nav-mobileTV">
                {config.runtime.pageBadge.copy|s} <i class="nav__live-tv-icon"></i>
            </a>

            {! SEARCH BAR !}
            <form id="search-form" class="search__form" method="get" action="{#. $var-url="/search/"}{+nav-render-url /}{/.}" name="headerSearch">
                <div class="search__form-fields">
                    {?config.runtime.features.enableSearchBySection}
                        {?vertical}
                            <input type="hidden" name="category" value="{vertical}">
                            {#. $var-section-name=vertical}{+search-input-render /}{/.}
                        {:else}
                            {@ne key=sectionName value="homepage"}
                                {@ne key=sectionName value="intl_homepage"}
                                    <input type="hidden" name="category" value="{@toLowerCase}{headerLabel}{/toLowerCase}">
                                {/ne}
                            {/ne}
                            {+search-input-render /}
                        {/vertical}
                    {:else}
                        {+search-input-render /}
                    {/config.runtime.features.enableSearchBySection}

                    <button id="submit-button" class="search__submit-button">Search &raquo;</button>
                </div>
                <div id="search-button" class="search__button" type="button" role="button"></div>
            </form>

            {! EDITION SELECTOR !}
            {> "views/pages/common/navigation-edition-picker" /}

            {! MENU SELECTOR !}
            <div id="menu" class="nav-menu js-navigation-hamburger"><div class="nav-menu__hamburger"></div></div>

        </div>
    </div>

    {> "views/pages/common/navigation-expanded-menu" /}
</div>



{<nav-editions-radio-button-options}
    <form class="edition-picker__radio-buttons" data-location="header" data-analytics="edition-picker">
        <p class="edition-picker__radio-button-label" >Set edition preference:</p>

        <label class="edition-picker__radio-button">
            {?request.isSSL}
                <input type="radio" class="js-edition-option edition_domestic" name="nav-edition" data-type="www" data-value="https:{config.host.us}{config.host.sslPort}"/><span for="edition_domestic">U.S.</span>
            {:else}
                <input type="radio" class="js-edition-option edition_domestic" name="nav-edition" data-type="www" data-value="http:{config.host.us}{config.host.port}"/><span for="edition_domestic">U.S.</span>
            {/request.isSSL}
        </label>
        <label class="edition-picker__radio-button">
            {?request.isSSL}
                <input type="radio" class="js-edition-option edition_international" name="nav-edition" data-type="edition" data-value="https:{config.host.intl}{config.host.sslPort}"/><span for="edition_international">International</span>
            {:else}
                <input type="radio" class="js-edition-option edition_international" name="nav-edition" data-type="edition" data-value="http:{config.host.intl}{config.host.port}"/><span for="edition_international">International</span>
            {/request.isSSL}
        </label>

        <button class="js-edition-confirm edition-picker__confirm-button">Confirm</button>
    </form>
{/nav-editions-radio-button-options}

{<search-input-render}
    <input class="search__input-field" type="text" placeholder="Search CNN
        {@ne key=sectionName value="homepage"}
            {@ne key=sectionName value="intl_homepage"}
                {?$var-section-name}
                    {~s}{$var-section-name}
                {/$var-section-name}
            {/ne}
        {/ne}
    ..." id="search-input-field" name="q">
{/search-input-render}

{!
    Renders a given URL. If the $var-use-absolute-urls flag is passed
    into the header partial then host.domain is added to the beginning
    of relative URLs.
!}
{<nav-render-url}
    {?$var-use-absolute-urls}
        {@relativeUrl url=$var-url}
            {request.domainUrl|s}{$var-url}
        {:else}
            {$var-url}
        {/relativeUrl}
    {:else}
        {$var-url}
    {/$var-use-absolute-urls}
{/nav-render-url}

{!
    ATTENTION!!!

    The files in this file might need to be included in the exact order you
    see below. Please be sure you know what you`re doing before rearranging.

!}


{!
    Script to handle a certain pattern of DDoS attacks and stop the page from doing anything.
!}
<script>
    if (
        {?config.runtime.ddosFiltering.minViewportHeight}
            (typeof window.innerHeight === 'number' && window.innerHeight < {config.runtime.ddosFiltering.minViewportHeight} && window.innerHeight >= 0)
        {:else}
            false
        {/config.runtime.ddosFiltering.minViewportHeight}
        ||
        {?config.runtime.ddosFiltering.referralFilter}
            (typeof document.location.href === 'string' && document.location.href.search(/{config.runtime.ddosFiltering.referralFilter|s}/i) !== -1)
        {:else}
            false
        {/config.runtime.ddosFiltering.referralFilter}
    ) {
        if (typeof window.stop === 'function') {
            window.stop();
        } else {
            document.execCommand('Stop');
        }
    }
</script>

{!
    Script to handle '?' and '#' redirects on the video landing page.
    This script has been inlined and added as far up as possible so that
    if it does run then nothing else is being initiated.
!}
{@eq key="{pageType}" value="section"}
    {@eq key="{sectionName}" value="videos"}
        <script>
            (function redirectLegacyVideoUrls(win) {
                var loc = (win && win.location) ? win.location : {};

                if (loc.search && loc.search.search(/^\?\/video\/.+$/) >= 0) {
                    loc.replace('/videos/' + loc.search.replace(/^\?\/video\/(.+)$/, '$1'));
                } else if (loc.hash && loc.hash.search(/^\#\/video\/.+$/) >= 0) {
                    loc.replace('/videos/' + loc.hash.replace(/^\#\/video\/(.+)$/, '$1'));
                }
            }(window));
        </script>
    {/eq}
{/eq}

{! The namespace and initial runtime config setup script !}
{>"views/pages/common/scripts-config" /}

{! The header scripts that need to be loaded first (jQuery, page-events, fastdom, etc. !}
<script src="{config.cdn.assetHost}{config.cdn.assetPath}/bundles/@@GLOBALHEADERNAME"></script>

{! Header scripts that need to be loaded after jQuery and compatibility stuff - advertising libs, CVP, Modernizr, etc. !}
{?config.runtime.features.enableDisplayAds}
    <script src="{config.cdn.assetHost}{config.cdn.assetPath}/{config.baseVersion}/js/cnn-header-second.min.js"></script>
{:else}
    <script src="{config.cdn.assetHost}{config.cdn.assetPath}/{config.baseVersion}/js/cnn-header-second-noads.min.js"></script>
{/config.runtime.features.enableDisplayAds}

{! Font fallback rule for browsers that don`t support fontface - this is a Dust template because its in a IE conditional comment !}
{>"views/pages/common/fontface-fallback-js" /}

{?config.runtime.features.enableOptimizely}
    {?config.runtime.optimizely.url}
        {> "views/pages/common/perf-mark" $var-mark="optimizelyStart" /}
        <script src="{config.runtime.optimizely.url}"></script>
        {> "views/pages/common/perf-mark" $var-mark="optimizelyEnd" /}
    {/config.runtime.optimizely.url}
{/config.runtime.features.enableOptimizely}

{!
    Controls NewRelic Browser for tracking client side performance
!}
{?config.runtime.features.enableNewRelicBrowser}
    {?config.runtime.newrelic.src}
        <script src="{config.runtime.newrelic.src}"></script>
    {/config.runtime.newrelic.src}
{/config.runtime.features.enableNewRelicBrowser}

{! Add performance mark for end of the head block !}
{?config.runtime.features.enableTiming}
    {> "views/pages/common/perf-mark" $var-mark="headEnd" /}
{/config.runtime.features.enableTiming}


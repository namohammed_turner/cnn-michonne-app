{!
    @file: preview-frame.dust
    @description: preview frame creates the basic shell for the preview page.
!}
<!DOCTYPE html>
<html class="no-js">

<head>
    {! Here are the meta tags & styles that are global to all CNN.com pages !}
    {> "views/pages/common/meta" /}
    {> "views/pages/common/styles" /}

    {! There should be VERY FEW, IF ANY, scripts in the header. If it must be, they live in this partial !}
    {> "views/pages/common/scripts-header" /}
</head>

<body class="pg pg-preview pg-{sectionName} {edition}{?pageTheme} t-{pageTheme}{/pageTheme}" data-eq-pts="xsmall: {config.runtime.containerBreakpoints.xsmall}">
    <div class="pg-{layout}
                {~s}pg-wrapper{~s}
                {?background.image}
                    {~s}pg__background__image
                {/background.image}
                ">
        {! The actual page template is here !}
        {> "views/pages/{layout}" /}
    </div>

    {! Here are the footer and scripts that are global to all CNN.com pages !}
    <script>
        var CNN = CNN || {};

        CNN.contentModel = {
            {?layout}       layout:      {layout|js|s}     {/layout}
            {?sectionName}, sectionName: {sectionName|js|s}{/sectionName}
            {?pageType}   , pageType:    {pageType|js|s}   {/pageType}
            {?env}        , env:         {env|js|s}        {/env}
            {?type}       , type:        {type|js|s}       {/type}
            {?analytics}  , analytics:   {analytics|js|s}  {/analytics}
            {?edition}    , edition:     {edition|js|s}    {/edition}
            {?title}      , title:       {title|js|s}      {/title}
        };
    </script>
    {> "views/pages/common/scripts-footer" /}
</body>
</html>

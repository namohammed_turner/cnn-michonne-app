
(function ($) {
    'use strict';
    $(function () {
        $('.iframe-container').fitFrame({
            mode: 'resize'
        });
    });
}(jQuery));

/* global CNN, AdFuel */

/**
 * Functionality to handle the companion ads feature.
 * This includes initial layout and swapping of the
 * epic ads for freewheel.
 *
 * @function
 */
(function supportCompanionAds(ns, $j, doc) {
    'use strict';

    if (!ns.Features || ns.Features.enableEpicAds !== true) {
        return;
    }

    ns.companion = ns.companion || {};

    ns.companion = $j.extend({}, ns.companion, {
        /**
         * This is the reference to the registry URL for the page.
         *
         * @member {string} registry
         */
        registry: (ns.contentModel && ns.contentModel.registryURL) || '',

        /**
         * A global cache of known epic ids in the registry. Each key is
         * an epic slot id and each value is always true.
         *
         * @member {object} knownEpicIds
         */
        knownEpicIds: {},

        /**
         * This is a reference to the companion configuration coming
         * directly from Pal and added to the page via the contentModel
         * object.
         *
         * @member {object} config
         */
        config: (ns.contentModel && ns.contentModel.companion) || {},

        /**
         * Tracks the current layout that is set on the page. This keeps
         * the code from applying the same layout twice in a row.
         *
         * @member {string} currentLayout
         */
        currentLayout: '',

        /**
         * Updates the ads on the page based on the layout configuration
         * from PAL and the layout given.
         *
         * @param {string} layout - The requested layout to set the ads
         * to as defined in the configuration.
         */
        updateCompanionLayout: function updateCompanionLayout(layout) {
            var layoutToUse = layout,
                addedHTML = '',
                command,
                i,
                layoutConfig,
                target;

            if (layout !== this.currentLayout && ns.companionAdState && this.config.layoutStates && this.config.layoutStates[ns.companionAdState]) {
                if (!this.config.layoutStates[ns.companionAdState][layoutToUse]) {
                    layoutToUse = 'default';
                }

                layoutConfig = this.config.layoutStates[ns.companionAdState][layoutToUse];
                if (layoutConfig && $j.isArray(layoutConfig)) {
                    for (i = layoutConfig.length - 1; i >= 0; i -= 1) {
                        command = layoutConfig[i];
                        target = this.getTargetDOM(command);
                        if (target) {
                            target.innerHTML = this.getReplacementHTML(command);
                            addedHTML += target.innerHTML;
                        }
                    }
                    this.refreshAnyEpicIds(addedHTML);
                    this.currentLayout = layout;
                }
            }
        },

        /**
         * Checks if there are any epic ids in the HTML. If so then it
         * refreshes each one.
         *
         * @param {string} html - The HTML snippet to check for epic
         * ids.
         */
        refreshAnyEpicIds: function refreshAnyEpicIds(html) {
            var self = this,
                $html = $j(html).filter('[id^="ad_"]'),
                id;

            if ($html && $html.length !== 0) {
                $html.each(function refreshTheIds() {
                    id = $j(this).attr('id');
                    self.refreshEpicSlot(id);
                });
            }
        },

        /**
         * Safely returns the ad library for use in this singleton. If
         * not found then a "mock" version is return so that no
         * function fail.
         *
         * @returns {object} The ad library to use.
         */
        getAdlib: function returnAdlib() {
            var hasInterface = AdFuel && AdFuel.processNewRegistry && AdFuel.refreshAd && AdFuel.registry;

            if (!hasInterface) {
                console.warn('companion.js: Could not find AdFuel! Using a mock object instead.');
            }

            return hasInterface ? AdFuel : {
                registry: [],
                processNewRegistry: function () {},
                refreshAd: function () {}
            };
        },

        /**
         * Pulls epic ids from the registry on the page.
         *
         * @returns {object} An object where the keys represent epic ids
         * that the registry knows about on the page. The values are
         * always true for each key.
         */
        getEpicIdsFromRegistry: function getEpicIdsFromRegistry() {
            var adlib = this.getAdlib(),
                i,
                j,
                currentSlot;

            this.knownEpicIds = {};
            for (i = adlib.registry.length - 1; i >= 0; i -= 1) {
                if ($j.isArray(adlib.registry[i])) {
                    for (j = adlib.registry[i].length - 1; j >= 0; j -= 1) {
                        currentSlot = adlib.registry[i][j] || {};
                        if (currentSlot.present && currentSlot.present === true && currentSlot.rktr_slot_id) {
                            this.knownEpicIds[currentSlot.rktr_slot_id] = currentSlot.present;
                        }
                    }
                }
            }

            return this.knownEpicIds;
        },

        /**
         * Determines if the epic Id is known in the registry. This
         * first checks the cached registry values and if it is not
         * found there then it will go and regenerate the cache of known
         * epic values in the registry.
         *
         * @param {string} id - The epic Id to check.
         *
         * @returns {boolean} True if the epic slot id is known in the
         * registry.
         */
        hasEpicId: function hasEpicId(id) {
            var ids = this.knownEpicIds;

            if (!ids[id]) {
                ids = this.getEpicIdsFromRegistry();
            }

            return (ids.hasOwnProperty(id) && typeof ids[id] === 'boolean') ? ids[id] : false;
        },

        /**
         * Refreshes the registry on the page which will add the
         * registry to the page again and cause the code to rerender the
         * ads. This is done through the ad library's function.
         */
        refreshEpicRegistry: function refreshEpicRegistry() {
            var adlib;

            if (this.registry !== '') {
                adlib = this.getAdlib();
                adlib.processNewRegistry(this.registry);
            }
        },

        /**
         * Refreshes an epic slot on the page.
         *
         * @param {string} id - The epic ID to refresh.
         */
        refreshEpicSlot: function figureOutHowToUpdateTheSlot(id) {
            var adlib = this.getAdlib();

            if (!this.hasEpicId(id)) {
                this.refreshEpicRegistry();
            }
            adlib.refreshAd(id);
        },

        /**
         * Uses the multi ads library to pull the correct target dom
         * element from the page to fill.
         *
         * @param {object} command - The command that is requesting the
         * target DOM element.
         * @returns {Element} - Target DOM element if found, otherwise null.
         */
        getTargetDOM: function doesThisPageHaveTheTarget(command) {
            var multiAds = ns.MultiAds || {},
                hasMultiAds = (ns.MultiAds && ns.MultiAds.adGroups && ns.MultiAds.domAds) ? true : false,
                multiAdsGroup = (hasMultiAds && command && command.target) ? multiAds.adGroups[command.target] : {},
                targetDom = null;

            if (typeof multiAdsGroup !== 'undefined' && typeof multiAdsGroup.active === 'number' && multiAdsGroup.active !== -1) {
                targetDom = ns.MultiAds.domAds[multiAdsGroup.active];
            }

            return targetDom;
        },

        /**
         * Pulls ad HTML from textarea elements on the page.
         *
         * @param {object} command - The object that directs the
         * function as to where to pull the ad HTML from the textarea.
         *
         * @param {string} command.replace - The name of the textarea
         * element to pull the ad HTML. This is found as a data-ad-name
         * attribute on the textarea.
         *
         * @returns {string} The ad HTML associated with the command.
         */
        getReplacementHTML: function findTheTextareaOnThePage(command) {
            var html = '',
                dom;

            if (command && command.replace && command.replace !== 'empty') {
                dom = doc.querySelectorAll('textarea[data-ad-name="' + command.replace + '"]');

                if (dom && dom.length && dom[0] && dom[0].value) {
                    html = dom[0].value;
                }
            }

            return html;
        },

        /**
         * Sets the initial companion layout for the page based on the
         * initial layoutType set by the the configuration coming from
         * Pal.
         */
        initCompanionLayout: function setThePageUpForCompanionAds() {
            var config = this.config || {};

            if (config.enabled &&
                typeof config.layoutType === 'string' &&
                typeof this.updateCompanionLayout === 'function') {
                this.updateCompanionLayout(config.layoutType);
            }
        }
    });
}(CNN, jQuery, document));


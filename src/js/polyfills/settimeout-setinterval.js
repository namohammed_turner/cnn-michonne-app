var __nativeST__,
    __nativeSI__;

if (document.all && !window.setTimeout.isPolyfill) {
    __nativeST__ = window.setTimeout;
    window.setTimeout = function (vCallback, nDelay /* , argumentToPass1, argumentToPass2, etc. */) {
        'use strict';

        var aArgs = Array.prototype.slice.call(arguments, 2);

        return __nativeST__(vCallback instanceof Function ? function () {
            vCallback.apply(null, aArgs);
        } : vCallback, nDelay);
    };
    window.setTimeout.isPolyfill = true;
}

if (document.all && !window.setInterval.isPolyfill) {
    __nativeSI__ = window.setInterval;
    window.setInterval = function (vCallback, nDelay /* , argumentToPass1, argumentToPass2, etc. */) {
        'use strict';

        var aArgs = Array.prototype.slice.call(arguments, 2);

        return __nativeSI__(vCallback instanceof Function ? function () {
            vCallback.apply(null, aArgs);
        } : vCallback, nDelay);
    };
    window.setInterval.isPolyfill = true;
}

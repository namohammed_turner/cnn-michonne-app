
/* Simple (incomplete) polyfill for top-level console */
(function (win) {
    'use strict';

    var c,
        empty = function () {},
        groups = [];

    win.console = win.console || {};
    c = win.console;
    if (!c.log) {
        c.log = empty;
    }
    if (!c.debug) {
        c.debug = c.log;
    }
    if (!c.error) {
        c.error = c.log;
    }
    if (!c.info) {
        c.info = c.log;
    }
    if (!c.warn) {
        c.warn = c.log;
    }
    if (!c.trace) {
        c.trace = empty;
    }
    if (!c.group) {
        c.group = function (label) {
            if (typeof label !== 'string') {
                label = Number(groups.length + 1).toString();
            }
            groups.push(label);
            c.log('---- START GROUP: ' + label);
        };
        c.groupCollapsed = c.group;
        c.groupEnd = function () {
            c.log('---- END GROUP: ' + (groups.length > 0 ? groups.pop() : '???'));
        };
    }
})(window);


/**
 * Polyfill used to see if an event listener with options will be properly
 * handled by the browser.
 *
 * @example
 * addEventListenerWithOptions(document.querySelector('.elem'), 'touchstart', function(event) {}, {capture: true, passive: true});
 *
 * @see
 * https://github.com/WICG/EventListenerOptions/blob/gh-pages/explainer.md#user-content-feature-detection
 */
(function () {
    'use strict';

    var opts = null;

    window.CNN = window.CNN || {};
    window.CNN.eventListenerWithOptions = window.CNN.passiveEventListeners || {
        browserSupportsCaptureOption: false,
        addListener: function (target, type, handler, options) {
            var optionsOrCapture = options;

            if (!window.CNN.eventListenerWithOptions.browserSupportsCaptureOption) {
                optionsOrCapture = options.capture || false;
            }

            target.addEventListener(type, handler, optionsOrCapture);
        }
    };

    try {
        // Check to see if the browser supports Event Listener options.
        opts = Object.defineProperty({}, 'capture', {
            get: function () {
                window.CNN.eventListenerWithOptions.browserSupportsCaptureOption = true;
            }
        });

        window.addEventListener('test', null, opts);
    } catch (e) {}
}());


/* Polyfill for Date.now courtesy of Mozilla */
if (!Date.now) {
    Date.now = function now() {
        'use strict';

        return new Date().getTime();
    };
}

/* global MSIB, CNN */

/* Define the functions needed for the tab in here */
(function ($) {
    'use strict';

    var displayName = '',
        DEFAULT_AVATAR_SIZE = 180,
        uploadSuccessMessage = 'It may take a few minutes for your updated avatar to appear.',
        uploadFailMessage = 'Upload failed!',
        updateFailMessage = 'Avatar update failed!',
        noImageSelectedMessage = 'Please choose an image to update your avatar.';

    CNN.mycnn.updateAvatar = {
        show: function () {
            CNN.mycnn.updateAvatar.displayAvatar();
        },

        /**
         * Displays Avatar
         */
        displayAvatar: function () {
            var
                output = {
                    avatarURL: '',
                    isSocialAvatar: 'false',
                    avatarUploadServiceUrl: ''
                };

            if (typeof CNN.msib.cfg !== 'undefined' && CNN.msib.cfg !== null) {
                /* build avatar upload service url */
                output.avatarUploadServiceUrl = 'http://' + CNN.msib.cfg.avatar.host + '/services/profile/avatar';
            }
            if (typeof CNN.mycnn.user !== 'undefined' && CNN.mycnn.user !== null) {
                displayName = CNN.mycnn.user.displayName;
            }
            output.avatarURL = CNN.mycnn.user.getAvatarUrl();
            if (output.avatarURL.indexOf('http://' + CNN.msib.cfg.avatar.host) === -1) {
                output.isSocialAvatar = 'true';
            }
            window.dust.render('views/cards/tool/mycnn-update-avatar', output, function (err, out) {
                if (err) {
                    if (window.console && window.console.error) {
                        console.error(err);
                    }
                } else {
                    CNN.mycnn.updateBody(out);
                    CNN.mycnn.updateAvatar.registerHandlers();
                }
            });
        },

        /**
         * add handler for save,cancel button
        */
        registerHandlers: function () {
            $(document.getElementById('mycnn-update-avatar-save')).click(function () {
                CNN.mycnn.updateAvatar.doUpload();
            });
            $(document.getElementById('mycnn-update-avatar-cancel')).click(function () {
                CNN.mycnn.details.show();
            });
            $('.mycnn-upload-status').hide();
        },

        /* get the iframe inside content value.for the legacy browser support
           used both contentWindow,contentDocument.
           Link:http://stackoverflow.com/questions/17197084/
           difference-between-contentdocument-and-contentwindow-javascript-iframe-frame-acc
        */
        findContentDoc: function (iframe) {
            var cdoc = iframe;
            if (cdoc.contentWindow) {
                cdoc = cdoc.contentWindow;
            }
            if (cdoc.contentDocument) {
                cdoc = cdoc.contentDocument;
            }
            if (cdoc.document) {
                cdoc = cdoc.document;
            }
            return cdoc;
        },

        /* reaches into the iframe and trigger the form to submit
        * the selected file to avatar upload service
        */
        doUpload: function () {
            var contentDoc = CNN.mycnn.updateAvatar.findContentDoc(document.getElementById('uploadFrame')),
                targetForm = contentDoc.getElementById('uploadForm'),
                uploadedContent = $(contentDoc.getElementById('uploadFile')).val(),
                $uploadStatus = $('.mycnn-upload-status'),
                userAvatarUrl = CNN.mycnn.updateAvatar.getAvatarUrlWithCacheBuster(),
                opts = {
                    tid: MSIB.util.getTid(),
                    authid: MSIB.util.getAuthid(),
                    authpass: MSIB.util.getAuthpass(),
                    'attributes[\'avatarUrl\']': userAvatarUrl
                };

            if (uploadedContent === '') {
                $uploadStatus.text(noImageSelectedMessage).show();
                return;
            }
            targetForm.submit();

            MSIB.msapi.editUserInfo(opts, function (xhr, _textStatus, _msg) {
                if (CNN.msib.isSuccessStatus(xhr)) {
                    $uploadStatus.text(uploadSuccessMessage).show();
                } else {
                    $uploadStatus.text(updateFailMessage).show();
                }
            });
        },

        /**
        * reload all user image with buster value
        */
        reloadAvatar: function () {
            var imageSrcURL = CNN.mycnn.updateAvatar.getAvatarUrlWithCacheBuster();
            $('.mycnn-avatar-update__image').attr('src', imageSrcURL);
            $('.mycnn-upload-status').hide();
            $('.mycnn__avatar-image').find('img').attr('src', imageSrcURL);
            $(document.getElementById('mycnn-update-avatar-cancel')).attr('disabled', true);
        },

        /**
         * upload status fail then trigger this method
         */
        uploadFail: function () {
            $('.mycnn-upload-status').show();
            $('.mycnn-upload-status').text(uploadFailMessage);
            $(document.getElementById('mycnn-update-avatar-cancel')).attr('disabled', true);
        },

        /**
         * upload status callback method
         * @param {boolean} success - true on success
         */
        avatarUploadStatus: function (success) {
            /* possible arguments: success, code, message */
            if (success) {
                CNN.mycnn.updateAvatar.reloadAvatar();
            } else {
                CNN.mycnn.updateAvatar.uploadFail();
            }
        },

        /**
         * geting the image url with buster values
         * @returns {string} - image URL string with cache-busting query parameter
         */
        getAvatarUrlWithCacheBuster: function () {
            var imageSrcURL = CNN.msib.avatarForScreenName(DEFAULT_AVATAR_SIZE, displayName);

            return imageSrcURL + '?' + (new Date().getTime());
        },

        /**
         * add handler for iframe change event
         */
        uploadFrameLoad: function () {
            var targetFrame = 'uploadFrame';

            $('input', window.frames[targetFrame].document).change(function () {
                $(document.getElementById('mycnn-update-avatar-cancel')).attr('disabled', false);
            });
        }
    };

    /* upload iframe response handling callback method binding */
    window.avatarUploadStatus = CNN.mycnn.updateAvatar.avatarUploadStatus;
}(jQuery));


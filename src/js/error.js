/* global CSIManager, CNN */

(function ($) {
    'use strict';

    var _search = {},
        params,
        idx;

    $(document).onZonesAndDomReady(function getCnnContentColumn() {
        var callObj, dates,
            textParam = ($.urlParam('text') ? $.urlParam('text') : '');

        _search = {
            input: [$('input#searchInputTop')],
            term: textParam,
            url: CNN.Search.queryUrl,
            args: {
                page:   1,
                npp:    9,
                start:  1,
                text:   textParam,
                type:   'all',
                bucket: true,
                sort:   'relevance'
            },
            filtered: false,
            filteredBy: [],
            firstLoad: true,
            scrollTop: 300,
            emptyQuery: false
        };

        try {
            _search.query = (typeof _search.term !== 'undefined') ? $.trim(decodeURIComponent(_search.term.replace(/\+/g, ' '))) : '';
        } catch (err) {
            /* Catch malformed data */
            _search.query = '';
        }
        if (_search.query === '') {
            /* Extract search terms from current URL */
            idx = window.location.href.indexOf('?');
            params = (idx >= 0) ? window.location.href.slice(idx) : '';
            try {
                _search.query = $.trim(decodeURIComponent(window.location.pathname + params).replace(/\/index\.(html|js|php|htm)/, '').replace(/\W+/g, ' '));
                _search.args.text = encodeURIComponent(_search.query);
            } catch (err) {
                /* Catch malformed data */
                _search.query = '';
                _search.args.text = '';
            }
        }

        function getLast7Dates() {
            var dateArray = [], currentDate = new Date(), i;
            currentDate.setHours(12);  /* Set time to noon on current date to avoid DST issues */
            for (i = 6; i >= 0; i--) {
                dateArray[i] = new Date(currentDate);
                currentDate.setDate(currentDate.getTime() - 86400);  /* Subtract a day */
            }
            return dateArray;
        }

        function setSearchParams() {
            var _args = '', _sep = '';
            $.each(_search.args, function assembleArgs(k, v) {
                if (k !== 'dredate') {
                    _args += _sep + k + '=' + v;
                    _sep = '&';
                }
            });
            return _args;
        }

        function handleSearchResults(o) {
            var didYouMean, didYouMeanMsg, sResults, html;

            $('#cnnSearchLoader').hide('fast');
            $('#cnnContentColumn').show('fast');

            _search.results = {
                count: o.metaResults.mixed,
                content: o.results[0],
                start: o.criteria[0].startAt,
                term: o.criteria[0].queries[0],
                end: o.criteria[0].maxResults,
                spotlights: o.spotlights,
                buckets: o.buckets,
                dym: o.didYouMean
            };

            didYouMean = ($.trim(_search.results.dym.prompt) !== '' && _search.results.count < 1) ? true : false;

            if (_search.firstLoad) {
                _search.firstLoad = false;
            }

            if (didYouMean) {
                didYouMeanMsg = 'Your search for <strong>' + decodeURIComponent(_search.term.replace(/\+/g, ' ')) + '</strong> did not match any documents';
                sResults = _search.results.dym.correctedResults;
                $('#cnnSearchRefine').hide();
                $('#cnnSearchPagination').hide();
                $('#cnnSearchTips').show();
                $('#cnnSearchResults').removeClass('cnnSearchResults').addClass('cnnSearchSuggestSpelling');

                if ($('div#cnnSearchSuggest').length < 1) {
                    html = '<div id="cnnSearchSuggest">';
                    html += 'Did you mean: <a href="/search/?text=' + _search.results.dym.prompt + '">' + _search.results.dym.prompt + '</a>?';
                    if (sResults.length > 0) {
                        html += '<span>Top ' + sResults.length + ' results shown</span>';
                    }
                    html += '</div>';
                    if (sResults.length > 0) {
                        html += '<h6>Example of found results</h6>';
                    }
                    $(html).insertAfter('div#cnnSearchSummary');
                }

                displaySuggestedResults();
            } else {
                /* display the content */
                displayResults();
            }
            return (didYouMean) ? didYouMeanMsg : _search.results.count + ' Results';
        } /* end: handle_search_results */

        /* results config */
        callObj = {
            url: _search.url,
            args: '',
            domId: 'cnnSearchSummaryResults',
            funcObj: handleSearchResults,
            renderFullTable: '',
            breakCache: '',
            overrideID: ''
        };

        /* set the search parameters, fetch the results */
        if (typeof _search.query !== 'undefined' && _search.query.length > 0) {
            $('#cnnSearchLoader').show('fast');

            /* fetch the results */
            callObj.args = setSearchParams();
            CSIManager.getInstance().callObject(callObj);
        } else {
            $(_search.input[0]).focus();

            _search.emptyQuery = true;
            _search.args.text = _search.query = ' ';
            _search.dateSort = 'Last 7 days';
            _search.filtered = true;
            _search.filteredBy.push('dredate');

            dates = getLast7Dates();
            if (dates.start !== '') {
                _search.args.startDate = dates.start;
            }

            if (dates.end !== '') {
                _search.args.endDate = dates.end;
            }

            callObj.args = setSearchParams();
            CSIManager.getInstance().callObject(callObj);

            $('#cnnSearchLoader').show('fast');
        }

        function displayResults() {
            var _content = '<ul class="cn cn-stack cn-stack--large-horizontal cn--idx-0">',
                emptyThumbnail = 'data:image/gif;base64,R0lGODlhEAAJAJEAAAAAAP///////wAAACH5BAEAAAIALAAAAAAQAAkAAAIKlI+py+0Po5yUFQA7';

            jQuery('.search-container').attr('data-result-count', _search.results.content.length);

            $.each(_search.results.content, function setSearchResultsContent(i) {
                var rslt = _search.results.content[i];
                rslt.thumbnail = rslt.thumbnail === '' ? emptyThumbnail : rslt.thumbnail;

                _content += '<li class="cn__column">';
                _content += '<article class="cd cd--card cd--small cd--idx-0 cd--vertical">';
                _content +=     '<div class="cd__wrapper" data-analytics="_cn-results_stack-large-horizontal_">';
                _content +=         '<a href="' + rslt.url + '">';

                _content +=             '<div class="thumbnail">';
                _content +=                 '<img class="" src="' + rslt.thumbnail + '">';

                if (rslt.collection === 'Videos') {
                    _content +=             '<i aria-hidden="true" class="media__icon icon-media-video"></i>';
                }
                _content +=     '</div>';

                _content += '<div class="cd__headline">' + rslt.title + '</div>';

                _content += '</article></a></li>';
            });
            _content += '</ul>';

            $('div#textResultsContainer').html(_content);
        }

        function displaySuggestedResults() {
            var _content = '<ul>';

            $('<li><a href="/search/?text=' + encodeURIComponent(_search.term) +
                '" id="cnnSearchRefineClearAll">Clear Any Search Filters</a></li>').insertBefore('div#cnnSearchTips ul li:first');

            if (typeof _search.results !== 'object') {
                _search.results = {
                    count: 0,
                    dym: {
                        correctedResults: []
                    },
                    term: ''
                };
            }
            $.each(_search.results.dym.correctedResults, function displaySearchResults(i) {
                var rslt = _search.results.dym.correctedResults[i],
                    type = rslt.mediaType,
                    date = rslt.mediaDateUts.replace(/\//g, '.'),
                    imgUrl,
                    assetUrl;

                _content += '<li>';
                if (type === 'gallery') {
                    assetUrl = CNN.Host.assetPath + '/assets/photo-bnw.svg';
                    imgUrl = (rslt.metadata.media.thumbnail.url) ? rslt.metadata.media.thumbnail.url : assetUrl;
                    _content += '<div>';
                    _content +=     '<a href="' + rslt.url + '">';
                    _content +=         '<span class="image">';
                    _content +=             '<img title="' + rslt.title + '" alt="' + rslt.title + '" src="' + imgUrl + '"/>';
                    _content +=         '</span>';
                    _content +=     '</a>';
                    _content += '</div>';
                }

                _content += '<div>';
                _content +=     '<a href="' + rslt.url + '">';
                _content +=         '<span class="title">' + rslt.title  + '</span>';
                _content +=         '<span class="date"> (' + date + ')</span>';
                _content +=     '</a>';
                _content += '</div>';
                _content += '<div>';
                _content +=     '<a href="' + rslt.url + '">';
                _content +=         '<span class="desc">' + rslt.metadata.media.excerpt + '</span>';
                _content +=     '</a>';
                _content += '</div>';

                /* END: article */
                _content += '</li>';

            });
            _content += '</ul>';
            $('div#textResultsContainer').html(_content);
        }
    });
}(jQuery));

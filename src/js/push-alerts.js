/* global CNN */

/**
 * Registration for CNN breaking news alerts via push notifications on supported platforms.
 */
CNN.PushAlerts = CNN.PushAlerts || {};
CNN.PushAlertsConfig = CNN.PushAlertsConfig || {};
CNN.PushAlertsConfig.safari = CNN.PushAlertsConfig.safari || {};

/**
 * Logic for Safari push notifications, available on OS X v10.9 and later.
 *
 * @see [Notification Programming Guide for Websites]{@link http://apple.co/1dGoVUp}
 */
CNN.PushAlerts.Safari = {

    webServiceURL: CNN.PushAlertsConfig.safari.webServiceURL || '',
    websitePushID: CNN.PushAlertsConfig.safari.websitePushID || '',

    /**
     * Prompts user for permission to send push notifications, if we have not already asked.
     * @param {object} permissionData - permission object structure described in Table 2-2
     * @param {string} permissionData.permission - the permission level set by the user: default, granted, or denied.
     * @param {string} [permissionData.deviceToken] - the unique identifier for the user on the device. Only present if permission is granted.
     *
     * Safari tracks the permission for each site, so no cookie or local storage is needed.
     * @see [Requesting Permission]{@link http://apple.co/1GJk6kg}
     * @see [Table 2-2]{@link http://apple.co/1JKrl1y}
     *
     * It logs activity to console because it can be difficult for non-developers to see what's happening.
     * Developers can see calls from SafariNotificationAgent to CNN Alerts Hub via Charles proxy server
     * with SSL proxying enabled for alertshub.cnn.com or qa.alerts.cnn.com.
     *
     * Note 1. After the user responds to the permission prompt (clicks Don't Allow or Allow), requestPermission completes,
     * and Safari should call the callback (checkRemotePermission), but it doesn't work reliably (tested in Safari 8.0.3 on OS X 10.10.2).
     * The behavior may be related to timing or other activity on the page.  However, this is not a blocker because Safari still makes the
     * registration call to Alerts Hub correctly, and we only use the script callback to show in the console what happened.  In any event,
     * the permission is shown on the next page load when checkPermission is called directly from init.
     */
    checkRemotePermission: function (permissionData) {
        'use strict';
        if (CNN.Utils.existsObject(permissionData)) {
            if (permissionData.permission === 'default') {
                /* This is a new web service URL and its validity is unknown. */
                console.info('Requesting user permission for Safari push alerts.');
                try {
                    window.safari.pushNotification.requestPermission(
                        CNN.PushAlerts.Safari.webServiceURL,
                        CNN.PushAlerts.Safari.websitePushID,
                        {}, /* Data that you choose to send to your server to help you identify the user. */
                        CNN.PushAlerts.Safari.checkRemotePermission /* The callback function; see Note 1 above. */
                    );
                    /* Safari requests the push package and, if valid, prompts the user then calls the callback with denied or granted.
                     * If not valid, it does not prompt, stays in the default state, and reports the error to webServiceURL/version/log.
                     */
                } catch (ex) {
                    /* Safari throws "Push notification prompting has been disabled." if the user has turned
                     * off Preferences > Notifications > Allow websites to ask for permission to send push notifications.
                     */
                    console.info('Safari push alerts permission request failed:', ex);
                }
            } else if (permissionData.permission === 'denied') {
                console.info('User said no to Safari push alerts.');
            } else if (permissionData.permission === 'granted') {
                console.info('User said yes to Safari push alerts.  deviceToken=' + permissionData.deviceToken);
            }
        }
    },

    /**
     * Prompts for permission when the page is loaded, if push notifications are enabled and supported.
     * @see CNN-21209
     */
    init: function () {
        'use strict';
        var origin,
            permissionData;
        if (CNN.Utils.existsObject(CNN.Features) && CNN.Features.enableSafariPushAlerts) {
            /* Ensure that the user can receive Safari Push Notifications. */
            if ('safari' in window && 'pushNotification' in window.safari) {
                /* Ask only when the user is accessing the page through a supported origin. */
                origin = window.location.origin;
                if (origin === CNN.Host.www || origin === CNN.Host.us || origin === CNN.Host.intl) {
                    permissionData = window.safari.pushNotification.permission(CNN.PushAlerts.Safari.websitePushID);
                    CNN.PushAlerts.Safari.checkRemotePermission(permissionData);
                } else {
                    /* Don't ask if the user has reached this page through an alternate origin like a preview server
                     * or a vanity (or deprecated) CNAME.  It will fail since the origin won't be in the push package.
                     */
                    console.info('Safari push alerts are not available on ' + origin);
                }
            }
        }
    }
};

CNN.PushAlerts.init = function initPushAlerts() {
    'use strict';
    CNN.PushAlerts.Safari.init();
    /* ...and potentially other platforms in the future. */
};

(function ($) {
    'use strict';

    /* Page render is more important than prompting for push alerts permission, so we defer when possible
     * until after the zones are loaded.
     */
    if (typeof CNN.contentModel !== 'undefined' && CNN.contentModel !== null && CNN.contentModel.lazyload) {
        $(document).onZonesAndDomReady(CNN.PushAlerts.init);
    } else {
        $(document).ready(CNN.PushAlerts.init);
    }
})(jQuery);


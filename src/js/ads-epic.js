/* global CNN */

/**
 * This sets up the functionality to refresh epic ad tags at specific
 * breakpoint values. This does not move the epic tags but lets adfuel
 * bring in the correct ad tag for the breakpoint region.
 *
 * @param {object} ns - The namespace where the new functionality will
 * live.
 *
 * @param {object} $j - jQuery library.
 *
 * @param {object} win - The global window object.
 *
 * @param {object} doc - The global document object.
 */
(function refreshAllAds(ns, $j, win, doc) {
    'use strict';

    if (!ns.Features || ns.Features.enableEpicAds !== true) {
        return;
    }

    ns.RefreshAds = ns.RefreshAds || {
        /**
         * Keeps track of the last know viewport width of the page.
         *
         * @member {number} lastKnownViewportWidth
         */
        lastKnownViewportWidth: 0,
        /**
         * A constant for the first breakpoint threshold that tiggers a refresh of all ads.
         *
         * @member {number} breakpoint1
         */
        breakpoint1: 480,
        /**
         * A constant for the second breakpoint threshold that tiggers a refresh of all ads.
         *
         * @member {number} breakpoint2
         *
         * @todo - Confirm with Ads that this shouldn't be 768 instead of 782 (_epic-ad.scss uses 768px as it's breakpoint)
         */
        breakpoint2: 782,
        /**
         * A constant for the third breakpoint threshold that tiggers a refresh of all ads.
         *
         * @member {number} breakpoint3
         */
        breakpoint3: 1024,
        /**
         * Checks if the current viewport triggers a refresh of all ads.
         * Then resets the lastKnownViewportWidth to the current
         * viewport's width.
         */
        refreshAllAds: function RefreshAllAds() {
            var viewportSize = this.getViewPortSize();

            if (
                (
                    (viewportSize.width >= this.breakpoint1  && this.lastKnownViewportWidth < this.breakpoint1) ||
                    (viewportSize.width <= this.breakpoint1  && this.lastKnownViewportWidth > this.breakpoint1)
                ) ||
                (
                    (viewportSize.width >= this.breakpoint2  && this.lastKnownViewportWidth < this.breakpoint2) ||
                    (viewportSize.width <= this.breakpoint2  && this.lastKnownViewportWidth > this.breakpoint2)
                ) ||
                (
                    (viewportSize.width >= this.breakpoint3  && this.lastKnownViewportWidth < this.breakpoint3) ||
                    (viewportSize.width <= this.breakpoint3  && this.lastKnownViewportWidth > this.breakpoint3)
                )
            ) {
                this.amptResizeAllAds();
            }

            this.lastKnownViewportWidth = viewportSize.width;
        },
        /**
         * Returns an object that holds the current viewport's width and
         * height in pixels.
         *
         * @returns {object} with the properties of width and height.
         */
        getViewPortSize: function GetViewportSize() {
            var myHeight = 0;

            if (typeof win.innerWidth === 'number') {
                /* Non-IE */
                myHeight = win.innerHeight;
            } else if (doc.documentElement && (doc.documentElement.clientWidth || doc.documentElement.clientHeight)) {
                /* IE 6+ in 'standards compliant mode' */
                myHeight = doc.documentElement.clientHeight;
            } else if (doc.body && (doc.body.clientWidth || doc.body.clientHeight)) {
                /* Lower IE compatible */
                myHeight = doc.body.clientHeight;
            }

            return {
                width: ns.CurrentSize && typeof ns.CurrentSize.getClientWidth === 'function' ? ns.CurrentSize.getClientWidth() : 0,
                height: myHeight
            };
        },
        /**
         * Relies on the MultiAd's ad library to call the refreshAllAds.
         */
        amptResizeAllAds: function amptResizeAllAds() {
            var library = (ns.MultiAds && ns.MultiAds.getAdlib) ? ns.MultiAds.getAdlib() : {};

            if (library.refreshAllAds) {
                library.refreshAllAds();
            }
        }
    };

    /*
     * bind resize event. This event also fires when orientation changes
     */
    $j(win).on('resize', function notifyWhenResizeOccurs() {
        ns.RefreshAds.refreshAllAds();
    });
}(CNN, jQuery, window, document));

/**
 * This sets up the multi ads namespace and kicks off the functionality.
 * The multi ads feature will pull all the elements that have the
 * attribute of data-ad-id from the DOM and cache it for later. Creates
 * a placeholder object for each data-ad-id attribute as a property that
 * maps to index values into the cached DOM elements. Each property in
 * the placeholder object is an object with two properties: active and
 * inactive. Active is the index in the cached DOM elements of the
 * element with a display that is no none (e.g., block, inline, etc.).
 * Inactive are all elements with the CSS display value of none. On
 * intialization cycle through the placeholder object checking each
 * active property to ensure it is still active. If not active then look
 * for an active element in the inactive property. If no active ads are
 * found for the placeholder then do nothing. If a new active ad is
 * found then update the active property and remove from the inactive.
 * Move the previous active into the inactives. Delete the ad from the
 * placeholder in the DOM and add it do the new place in the DOM. Then
 * use the trigger The refresh ads function from the ad library to
 * update the ads on the page.
 *
 *
 * @param {object} ns - The namespace to attach this new feature.
 *
 * @param {object} $j - The jQuery library.
 *
 * @param {object} win - The HTML Window object.
 *
 * @param {object} doc - The HTML Document object.
 *
 * @param {object} adlib - The ad library class reference to be used to
 * trigger an ad refresh.
 *
 * @param {string|undefined} registry - The registry URL for the ads.
 *
 */
(function setupMultiAds(ns, $j, win, doc, adlib, registry) {
    'use strict';

    if (!ns.Features || ns.Features.enableEpicAds !== true) {
        return;
    }

    ns.MultiAds = ns.MultiAds || {
        /**
         * Holds the NodeList set of DOM references generated by the querySelectorAll.
         *
         * @member {array} domAds
         */
        domAds: [],

        /*
         * Track ads added through update function
         *
         * @member {array} updatedAds
         */
        updatedAds: [],

        /*
         * Track viewport based ad position
         *
         * @member {string} updatePosition
         */
        updatePosition: 'desktop',

        /**
         * An object where each property is an data-ad-id on the page. Each property
         * is an object with the properties of active and inactive. Active is the index
         * of the domAd placeholder div that has a content css value of active. Inactive
         * is an array of index values of the domAd placeholder divs that has a content
         * css value of inactive.
         *
         * @member {object} adGroups
         */
        adGroups: {},

        /**
         * Holds the property keys of the adGroups object.
         *
         * @member {array} adGroupIds
         */
        adGroupIds: [],

        /**
         * Safely returns the ad library for use in this singleton. If
         * not found then a "mock" version is return so that no
         * function fail.
         *
         * @returns {object} The ad library to use.
         */
        getAdlib: function returnAdlib() {
            var lib = win[adlib],
                hasInterface = lib && lib.processNewRegistry && lib.refreshAd && lib.requestAndRenderAds && lib.refreshAllAds;

            if (!hasInterface) {
                console.warn('cnn-epic-multi-ads::getAdlib: Could not find %s! Using a mock object instead.', adlib);
            }

            return hasInterface ? lib : {
                processNewRegistry: function () {},
                refreshAd: function () {},
                requestAndRenderAds: function () {},
                refreshAllAds: function () {}
            };
        },

        /**
         * Checks if the element is empty.
         *
         * @param {object} node - The element to check.
         *
         * @returns {boolean} True if the element is empty otherwise it
         * is false.
         */
        isEmptyNode: function CheckIfDOMHasAnyElements(node) {
            var current = null,
                isEmpty = true;

            if (node.hasChildNodes()) {
                current = node.firstChild;
                while (current.nextSibling !== null && current.nodeType !== Node.ELEMENT_NODE) {
                    current = current.nextSibling;
                }
                isEmpty = (current !== null && current.nodeType !== Node.ELEMENT_NODE);
            }
            current = null;

            return isEmpty;
        },

        /**
         * Tests if the DOM element is considered active or not.
         * If it has a display of none then it is inactive otherwise it
         * is considered active.
         *
         * @param {object} placeholder - The DOM element to check.
         *
         * @returns {boolean} True if the placeholder is active,
         * otherwise false.
         */
        isActive: function isThisAdPlaceholderActive(placeholder) {
            var currentDisplay;

            if (typeof win.getComputedStyle === 'function') {
                currentDisplay = win.getComputedStyle(placeholder).getPropertyValue('display');
            } else {
                /* This is the IE8 solution for getComputedStyle */
                currentDisplay = placeholder.currentStyle.display;
            }

            return currentDisplay !== 'none';
        },

        /**
         * Adds an epic ad tag to the placeholder tag on the page.
         *
         * @param {object} ad - The DOM element where the epic tag
         * will be added.
         *
         * @param {string} id - The epic ID to use to create the epic
         * tag.
         */
        addAdTagToPage: function addAdTagToPage(ad, id) {
            var self = this,
                newAdTag,
                newClass,
                refresh;

            if (self.isEmptyNode(ad)) {
                refresh = ad.getAttribute('data-ad-refresh');

                newAdTag = doc.createElement('div');

                newClass = 'ad-' + id + (refresh ? ' ad-refresh-' + refresh : '');
                newAdTag.setAttribute('id', id);
                newAdTag.setAttribute('class', newClass);

                ad.appendChild(newAdTag);
            }
        },

        /**
         * Removes all child nodes from a placeholder tag on the page.
         *
         * @param {object} ad - The DOM node whose children will be
         * removed.
         */
        removeAdTagFromPage: function removeAdTagFromPage(ad) {
            var self = this;

            if (!self.isEmptyNode(ad)) {
                while (ad.firstChild) {
                    ad.removeChild(ad.firstChild);
                }
            }
        },

        /**
         * Called once all the work in setting up the ad positions is complete.
         *
         * @param {boolean} update - Set to true if we are updating existing ad slots
         * @param {array} adSlots - Array of ad slots to update, if updating
         */
        allDone: function allDone(update, adSlots) {
            var library = ns.MultiAds.getAdlib();

            $j('body').addClass('multi-ads--complete');

            if (typeof registry === 'string' && registry !== '') {
                update = update || false;
                if (update !== true) {
                    library.queueRegistry(registry);
                } else {
                    library.queueRegistry(registry, {slots: (adSlots || [])});
                }
            }
        },

        /**
         * This is the quickest work around to fix the companion ads issue found with AVNGRS-2391.
         * This is not a good long term solution and should be remove as soon as we have a better
         * solution for suppressing placeholder ads when needed.
         *
         * @returns {object} - Set of placeholder companion ad ids.
         */
        getPlaceholderOnlyIds: function thisIsAHackThatNeedsToBeReplaced() {
            var hasCompanionConfig = ns.contentModel && ns.contentModel.companion && ns.contentModel.companion.enabled,
                ids = hasCompanionConfig ? ns.contentModel.companion.ids || {} : {};

            return ids;
        },

        /**
         * Handler to process the cases when the Ad rendering is completed successfully and
         * when it is not
         */
        handleAdStatus: function handleSingletonAdRenderStatus() {
            /*
             * TODO: Need to account for the case where the singleton Ad is not
             * rendered successfully - will be done as part of a differnt subtask
             */
        },

        /**
         * Loads the singleton ad file to display the ad into a singleton location
         * and returns the ajax promise-like result that can be used to do something
         * with the result
         *
         * Example:
         *     loadSingleton(file).success(function loadedSingleton(ad) {
         *        .. do something awesome with the singleton ...
         *      });
         *
         * @param {string} singletonFile - the singleton ad file that has to be loaded
         * @returns {jQuery.fn} - the ajax request
         */
        loadSingleton: function loadSingleton(singletonFile) {
            return $j.ajax({
                url: singletonFile,
                type: 'GET',
                dataType: 'script',
                cache: true,
                error: function (_jqXHR, _textStatus, _errorThrown) {
                    console.error('Could not load singleton script: ' + singletonFile);
                }
            });
        },

        /**
         * Sets the correct original or cloned singleton after the owl carousel
         * has been initialized
         *
         * @param {object} _e - The owl event passed into the event handler
         */
        setExactSingleton: function setExactSingleton(_e) {
            var self = this,
                doubletonInfoSelector = '.js-doubleton-info',
                owlItemSelector = '.owl-item',
                doubletonClass = 'ad-doubleton',
                clonedOwlClass = 'cloned',
                isClonedOwlItem,
                isDoubletonAd,
                singletonId,
                singletonFile,
                $adDoubleton,
                $doubletonInfoEl,
                doubletonSelector = '.ad-doubleton',
                doubletonElements;

            doubletonElements = $j(doubletonSelector);

            if (doubletonElements.length) {
                $j.each(doubletonElements, function () {
                    $adDoubleton = $j(this);
                    $doubletonInfoEl = $adDoubleton.parent().siblings(doubletonInfoSelector);
                    isClonedOwlItem = $adDoubleton.closest(owlItemSelector).hasClass(clonedOwlClass);
                    isDoubletonAd = $adDoubleton.hasClass(doubletonClass);

                    if (isClonedOwlItem) {
                        singletonId = $doubletonInfoEl.data('cloneid');
                        singletonFile = $doubletonInfoEl.data('clonefile');
                    } else {
                        singletonId = $doubletonInfoEl.data('origid');
                        singletonFile = $doubletonInfoEl.data('origfile');
                    }

                    $adDoubleton.attr('id', singletonId);
                    if (isDoubletonAd) {
                        $adDoubleton.removeClass(doubletonClass);
                        $adDoubleton.addClass('ad-' + singletonId);
                    }
                    self.loadSingleton(singletonFile).success(self.handleAdStatus);
                });
            }
        },

        /**
         * Find the carousel within the sunrise/collection player, listen on the
         * owl initializaton event and call the handler to set singletons
         */
        getDoubletons: function findAndParseDoubletonLocations() {
            var carouselContainerSelector = '.js-owl-carousel',
                $carouselContainer,
                doubletonSelector = '.ad-doubleton',
                allDoubletons;

            allDoubletons = $j(doubletonSelector);
            if (allDoubletons.length) {
                $carouselContainer = allDoubletons.eq(0).closest(carouselContainerSelector);
                $carouselContainer.on(ns.Carousel.events.initialized, $j.proxy(this.setExactSingleton, this));
            }
        },

        setupAdSpace: function setupAdSpace(ad, i) {
            var self = this,
                placeholderOnlyIds = self.getPlaceholderOnlyIds(),
                id,
                isActive;

            id = ad.getAttribute('data-ad-id');
            isActive = self.isActive(ad);

            if (!self.adGroups.hasOwnProperty(id)) {
                self.adGroupIds.push(id);
                self.adGroups[id] = {
                    active: -1,
                    inactive: []
                };
            }

            /* Only fill the first active placeholder found. */
            if (isActive && self.adGroups[id].active === -1) {
                self.adGroups[id].active = i;
                if (placeholderOnlyIds[id] !== 'suppress') {
                    self.addAdTagToPage(ad, id);
                }
            }

            if (!isActive) {
                self.adGroups[id].inactive.push(i);
                self.removeAdTagFromPage(ad);
            }
        },

        update: function CheckForAdditionalAdsOnPage(selectors) {
            var self = this,
                i,
                existingAdCount = self.domAds.length,
                querySelectors = [],
                moreAds = [],
                adSlots = [],
                id,
                position;

            selectors = selectors || [];

            if (selectors.length === 0) {
                return;
            }

            for (i = 0; i < selectors.length; i++) {
                querySelectors.push(selectors[i] + ' [data-ad-id]');
            }

            moreAds = [].slice.call(doc.querySelectorAll(querySelectors.join(', ')));

            if (moreAds.length > 0) {
                for (i = moreAds.length - 1; i >= 0; i -= 1) {
                    id = moreAds[i].getAttribute('data-ad-id');
                    position = moreAds[i].getAttribute('data-ad-position');

                    if (position !== self.updatePosition || self.updatedAds.indexOf(id) >= 0) {
                        self.removeAdTagFromPage(moreAds[i]);
                        continue;
                    }

                    self.setupAdSpace(moreAds[i], existingAdCount + i);

                    if (adSlots.indexOf(id) === -1) {
                        adSlots.push(id);
                        self.updatedAds.push(id);
                    }

                    self.domAds.push(moreAds[i]);
                }
            } else {
                return;
            }

            self.getDoubletons();

            if (Array.isArray(adSlots) && adSlots.length > 0) {
                self.allDone(true, adSlots);
            }
        },

        /**
         * This is the initialization for the multi-ads script. Creates
         * the tracking object for each multiple ad placement and
         * determines where ads should be placed.
         */
        init: function CheckForMultiAdsOnPage() {
            var self = this,
                i,
                viewportSize = ns.RefreshAds.getViewPortSize();

            /* convert nodelist to array */
            self.domAds = [].slice.call(doc.querySelectorAll('[data-ad-id]'));

            /* ensure that the correct ad positions (based on viewport and updateAds fallback) are removed */
            if (viewportSize.width < ns.RefreshAds.breakpoint2) {
                self.updatePosition = 'mobile';
            } else if (viewportSize.width >= ns.RefreshAds.breakpoint2 && viewportSize.width < ns.RefreshAds.breakpoint3) {
                self.updatePosition = 'tablet';
            }

            /*
             * Create initial adGroups object and fill any active
             * placeholders with epic tags. All inactive placeholders
             * are cleared of content.
             */
            for (i = self.domAds.length - 1; i >= 0; i -= 1) {
                self.setupAdSpace(self.domAds[i], i);
            }

            self.getDoubletons();
            self.allDone();
        }
    };

    $j(doc).onZonesAndDomReady(function startYourEngines() {
        var viewport;

        console.timeStamp('ads-epic::startYourEngines: before init call');

        viewport = ns.RefreshAds.getViewPortSize();
        ns.RefreshAds.lastKnownViewportWidth = viewport.width;

        console.timeStamp('ads-epic::startYourEngines: done with function');
    });
}(CNN, jQuery, window, document, 'AdFuel', (CNN && CNN.contentModel && CNN.contentModel) ? CNN.contentModel.registryURL : ''));

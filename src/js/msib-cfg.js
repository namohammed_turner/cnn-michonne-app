/* global CNN */

/* Sets the global MSIB configuration for this site. */

(function () {
    'use strict';

    if (typeof CNN.msib === 'object' && CNN.msib !== null &&
        typeof CNN.msib.localCfg === 'object' && CNN.msib.localCfg !== null) {

        return;  /* Already initialized */
    }

    CNN.SocialConfig = CNN.SocialConfig || {};
    CNN.SocialConfig.avatar = CNN.SocialConfig.avatar || {};
    CNN.SocialConfig.gigya = CNN.SocialConfig.gigya || {};
    CNN.SocialConfig.livefyre = CNN.SocialConfig.livefyre || {};
    CNN.SocialConfig.msib = CNN.SocialConfig.msib || {};

    CNN.msib = CNN.msib || {};
    CNN.msib.localCfg = CNN.msib.localCfg || {};

    CNN.msib.localCfg.avatar = CNN.msib.localCfg.avatar || {};
    CNN.msib.localCfg.avatar.host = CNN.SocialConfig.avatar.host;

    CNN.msib.localCfg.gigya = CNN.msib.localCfg.gigya || {};
    CNN.msib.localCfg.gigya.appId = CNN.SocialConfig.gigya.appId;
    /* This doesn't change between environments. */
    CNN.msib.localCfg.gigya.shareButtons = 'Email,Facebook,Twitter,Share';

    CNN.msib.localCfg.livefyre = CNN.msib.localCfg.livefyre || {};
    CNN.msib.localCfg.livefyre.network = CNN.SocialConfig.livefyre.network;
    CNN.msib.localCfg.livefyre.siteId = CNN.SocialConfig.livefyre.siteId;
    CNN.msib.localCfg.livefyre.srcDomain = CNN.SocialConfig.livefyre.srcDomain;

    CNN.msib.localCfg.msib = CNN.msib.localCfg.msib || {};
    CNN.msib.localCfg.msib.baseUrl = CNN.SocialConfig.msib.baseUrl;
    /* This doesn't change between environments. */
    CNN.msib.localCfg.msib.loginUrl = '/login.html';
}());


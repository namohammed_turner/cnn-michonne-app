/* global CNN */

/**
 * Informs the injector that the footer has completed loadeding asynchronously.
 *
 * This library should be the last library in the footer.  This calls a
 * footer specific function in the injector so any functionality that is
 * dependent on any libraries in the footer can execute.
 */

(function (win, doc, j, cnn) {
    'use strict';

    cnn = cnn || {};
    cnn.Features = cnn.Features || {};

    /**
     * Do performance marks/measures for onZonesAndDomReady.
     * @private
     */
    function perfOnZonesAndDomReady() {
        win.performance.mark('zonesAndDomReady');
        win.performance.measure('timeToInteractive', 'responseEnd', 'domInteractive');
        win.performance.measure('timeToDomReady', 'responseEnd', 'domReady');
        win.performance.measure('timeToZonesAndDomReady', 'responseEnd', 'zonesAndDomReady');
    }

    /**
     * Do performance marks/measures for onZonesReady.
     * @private
     */
    function perfOnZonesReady() {
        win.performance.mark('zonesReady');
        win.performance.measure('timeToZonesComplete', 'responseEnd', 'zonesReady');
    }

    try {
        /* If using WebPack, this is WebPack injector marker */
        if (typeof cnn.INJECTOR === 'object') {
            cnn.INJECTOR.scriptComplete('footer');
        }

        if (cnn.Features.enableTiming === true) {
            /* Set the performance mark that the footer is done */
            win.performance.mark('footerEnd');
            /* Do the performance measures */
            win.performance.measure('headTime', 'responseEnd', 'headEnd');
            win.performance.measure('domTime', 'headEnd', 'footerStart');
            win.performance.measure('footerTime', 'footerStart', 'footerEnd');
            if (cnn.Features.enableOptimizely === true && typeof cnn.OptimizelyConfig.url === 'string' &&
                cnn.OptimizelyConfig.url.length > 0) {

                win.performance.measure('optimizelyTime', 'optimizelyStart', 'optimizelyEnd');
            }
            if (typeof j.fn.onZonesAndDomReady === 'function') {
                j(doc).ready(function () {
                    win.performance.mark('domReady');
                });
                j(doc).onZonesRendered(perfOnZonesReady);
                j(doc).onZonesAndDomReady(perfOnZonesAndDomReady);
            } else {
                j(doc).ready(function () {
                    win.performance.mark('domReady');
                    perfOnZonesReady();
                    perfOnZonesAndDomReady();
                });
            }
        }
    } catch (e) {
        console.log(e);
    }
})(window, document, jQuery, CNN);


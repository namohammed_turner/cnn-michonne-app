/* global CNN */

(function ($, ns) {
    'use strict';

    $(document).onZonesAndDomReady(function () {

        $(document).on('focus click', '#subEmail, .email-input', onEmailContainerFocus);
        $(document).on('blur', '#subEmail, .email-input', onEmailContainerBlur);
        $('.pg-subhub-page .addletter').on('click', onAddNewsletter);
        $(document).on('keypress', '#subEmail', onKeypress);
        $('.pg-subhub-page .selectletter').on('click keypress', onSubmitSubscriptions);

        if ($('html').hasClass('ios')) { // iOS Fixes for email input focus issues
            window.sh_currScrollPosition = 0;

            $(document).on('scroll touchmove', onScrollIOSFix).trigger('scroll');
            $(document).on('touchstart focusin', '.email-input input, .errorDisplay', onEmailInputFocusIOSFix);
            $(document).on('click touchstart', '#mobile-backdrop', onBackdropClick);
        }

        $('.email-input').val('');

        function onEmailContainerFocus(e) {
            var $input = $('.email-input input'),
                isIOS = $('html').hasClass('ios'),
                isSubmitTarget = $(e.target).hasClass('selectletter'),
                $emailcontainer = $('.email-container'),
                isMobile = jQuery('html.mobile').length !== 0 ? true : false;

            $('.email-input').removeClass('errorDisplay');

            if (!isIOS && isMobile && screen.width > screen.height) {
                $emailcontainer.css({
                    position: 'absolute',
                    top: '0',
                    'z-index': '28'
                });
            }

            if (!$input.is(':focus') && !isSubmitTarget && !isIOS) {
                $input.focus();
                e.stopPropagation();
            }
        }

        function onEmailContainerBlur() {
            var isIOS = $('html').hasClass('ios'),
                $emailcontainer = $('.email-container'),
                isMobile = jQuery('html.mobile').length !== 0 ? true : false;
            if (!isIOS && isMobile && screen.width > screen.height) {
                $emailcontainer.css({
                    position: 'fixed',
                    bottom: '0',
                    top: '',
                    'z-index': '5'
                });
            }
        }

        function onKeypress(e) {
            var keyCode = e.keyCode;

            if (keyCode === 13) {
                onSubmitSubscriptions();
            }
        }

        function onAddNewsletter(e) {
            var $this = $(e.target);
            if ($this.hasClass('selectedLetter')) {
                $this.removeClass('selectedLetter').text($this.attr('title'));
            } else {
                $this.addClass('selectedLetter').text(CNN.newsLetterConfig.selectedButtonText);
                $('.email-input').removeClass('errorDisplay');
            }
        }

        function onSubmitSubscriptions() {
            var mailChimpNewsletterIDs = [],
                gigyaNewsletters = {},
                subEmail = $('#subEmail').val(),
                elementId,
                elementIsGigya;


            $('.pg-subhub-page .selectedLetter').each(function () {
                elementId = $(this).attr('id');
                elementIsGigya = $(this).data('isgigya');

                if (elementIsGigya === true) {
                    gigyaNewsletters[elementId] = true;
                } else {
                    mailChimpNewsletterIDs.push(elementId);
                }
            });

            if (mailChimpNewsletterIDs.length <= 0 && Object.keys(gigyaNewsletters).length <= 0) {
                showError('*Please select at least one newsletter.');
                return;
            }
            if (!validateEmail(subEmail)) {
                showError('*Please enter valid email');
                return;
            }

            doSubmitSubHubSubscriptions(subEmail, mailChimpNewsletterIDs, gigyaNewsletters);

        }

        function submitMailChimpSubs(userEmail, mailChimpNewsletterIDs) {
            var dataString,
                formData = {id: mailChimpNewsletterIDs, email: userEmail},
                payload = mergeTracking(formData, {
                    edition: ns.contentModel.edition || 'domestic',
                    pageType: ns.contentModel.pageType || '',
                    sectionName: ns.contentModel.sectionName || ''
                });

            dataString = JSON.stringify(payload);

            return $.ajax({
                type: 'POST',
                url: ns.newsLetterConfig.mailchimpUrl,
                data: dataString,
                contentType: 'application/json; charset=UTF-8',
                beforeSend: function () {
                    $('.progressbar-control').animate({width: '70%'}, 1000);
                }
            });
        }

        /**
         * Submit newsletter to gigya using lite registration process.
         * @param {string} userEmail - users email address.
         * @param {object} gigyaNewsletters - newsletters user is signing up too.
         * @returns {Promise} - promise with result
         *
         * notes: error codes:
         *  0 ='success'
         *  400006 =Validation error - invalid email address.
         *  400021 (formerly 400009) = Schema doesn't have the newsletter. Add using this tool: https://tools.gigya-cs.com/schema/
         *  500026, 504001, or 504002 = Network connectivity issue
         *  500001 or 500028 = Gigya server issues
         */
        function submitGigyaSubs(userEmail, gigyaNewsletters) {
            var gigyaPromise = $.Deferred();

            window.liteSignup({
                email: userEmail,
                newsletters: gigyaNewsletters,
                callback: function (resp, errorMessage) {
                    if (resp.errorCode === 0 || resp.errorCode === 40006) {
                        gigyaPromise.resolve(true);
                    } else {
                        gigyaPromise.reject({errorCode: resp.errorCode, errorMessage: errorMessage});
                    }
                }
            });

            return gigyaPromise;
        }

        function doSubmitSubHubSubscriptions(userEmail, mailChimpNewsletterIDs, gigyaNewsletters) {
            var mailChimpPromise = $.Deferred(),
                gigyaPromise = $.Deferred();

            window.oi_sda_email = userEmail; /* opt intelligence email for offer sign-up on thank you page */
            trackUserSubscribeClick();

            if (mailChimpNewsletterIDs.length) {
                mailChimpPromise = submitMailChimpSubs(userEmail, mailChimpNewsletterIDs);
            } else {
                mailChimpPromise.resolve();
            }

            if (Object.keys(gigyaNewsletters).length) {
                gigyaPromise = submitGigyaSubs(userEmail, gigyaNewsletters);
            } else {
                gigyaPromise.resolve();
            }

            $.when(mailChimpPromise, gigyaPromise)
                .done(function (_mc_response, _g_response) {
                    showSuccess();
                }).fail(function (_error) {
                    showError('Problem in email subscription, please try again.');
                });
        }

        function validateEmail(subEmail) {
            var check = /^([\w\.\'\+\-]+)\@([\w\-]{1,63}\.)+[\w\-]{2,63}$/;

            return check.test(subEmail);
        }

        function showError(msg) {
            $('.email-input').addClass('errorDisplay').find('.errorSpan').text(msg);
        }

        function trackUserSubscribeClick(_e) {
            if (typeof window.trackMetrics !== 'function') { return; }

            try {
                window.trackMetrics({
                    type: 'user-interaction',
                    data: {
                        interaction: 'cnn:emailnewsletter:subscribe' // if subscribe button is clicked
                    }
                });
            } catch (e) {
                /* Ignored. */
            }
        }

        function showSuccess() {
            $('.selectletter').addClass('submitted').text(ns.newsLetterConfig.submittedButtonText);
            $('.progressbar-control').animate({width: '100%'});

            $(document.body).addClass('complete');
            window.scroll(0, 0);

            if (typeof window.oi_show_offers === 'function') {
                window.oi_show_offers();
            }
        }

        function onScrollIOSFix(_e) {
            window.sh_currScrollPosition = $(document).scrollTop();
        }

        function onEmailInputFocusIOSFix(e) { // ios fixes for email input focus issues
            ensureWindowCurrentScrollTop();
            if ($(e.target).hasClass('errorSpan')) {
                $('.email-input').removeClass('errorDisplay');
            }
            $('.signupDisplay').addClass('modal-email');
            $('.email-container').css({top: window.sh_currScrollPosition});
        }

        function onBackdropClick(_e) {
            ensureWindowCurrentScrollTop();
            $('.signupDisplay').removeClass('modal-email');
            $('.email-container').css({top: ''});
            document.activeElement.blur();
        }

        function ensureWindowCurrentScrollTop() {
            setTimeout(function () {
                document.body.scrollTop = window.sh_currScrollPosition;
            }, 0);
        }

        /**
         * merges source data into newsletter merge fields
         * for audience development tracking
         *
         * @param {object} target - original object containing the merge_fields property
         * @param {object} source - source object to merge from
         * @return {object} - Returns a modified target if source has a value.
         */
        function mergeTracking(target, source) {
            var prop,
                src = '';

            target.merge_fields = target.merge_fields || {};

            if (target && typeof target === 'object') {
                if (!source) {
                    return target;
                }

                for (prop in source) {
                    if (source.hasOwnProperty(prop) && !Array.isArray(source[prop])) {
                        src += source[prop] + ':';
                    }
                }

                src = src.substr(0, src.length - 1);
                target.merge_fields.SOURCE = src;
            }
            return target;
        }
    });

}(jQuery, CNN));

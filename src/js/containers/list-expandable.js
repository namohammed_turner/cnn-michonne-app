/* global CNN */

/**
 * Function used for expandable containers on mobile
 */
(function ($j) {
    'use strict';

    var CONST_COLLAPSED = 'cn--collapsed',
        CONST_SMALL_HORIZONTAL = 'cn-list-small-horizontal',
        hideOnLoad = true;

    function _bindEvents() {
        $j('.cn--expandable .js-cn__button').on('click', function expandableContainerButtonClick() {
            _toggleContainer($j(this).closest('.cn'));
        });
    }

    function _hideOnLoad() {
        if (hideOnLoad) {
            $j('.cn--expandable').addClass(CONST_COLLAPSED);
        }
    }

    function _toggleContainer($container) {
        if ($container.hasClass(CONST_COLLAPSED)) {
            _expandContainer($container);

            /* Reload responsive images because 1x1 is loaded intially */
            if ($container.hasClass(CONST_SMALL_HORIZONTAL)) {
                CNN.ResponsiveImages.processChildrenImages($container[0]);
            }
        } else {
            _collapseContainer($container);
        }
    }

    function _collapseContainer($container) {
        $container.addClass(CONST_COLLAPSED);
    }

    function _expandContainer($container) {
        $container.removeClass(CONST_COLLAPSED);
    }

    /* Initialize */
    _bindEvents();
    _hideOnLoad();
})(jQuery);


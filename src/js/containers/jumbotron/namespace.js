/* global CNN */

CNN.Jumbotron = CNN.Jumbotron || {};
CNN.Jumbotron.Events = CNN.Jumbotron.Events || {};
CNN.JumbotronConfig = CNN.JumbotronConfig || {};

CNN.Jumbotron.ObjectBase = function () {
    'use strict';
};

/**
 * The Base Class that Jumbotron objects can extend
 */
CNN.Jumbotron.ObjectBase.prototype = {
    /**
     * toString override that's meant to output human readable
     * output of current variables and methods for the current
     * class. This is helpful for debugging.
     *
     * @returns {string} - Human readable representation of this jumbotron
     */
    toString: function () {
        'use strict';

        var retString = '[',
            functions = [],
            properties = [],
            property;

        for (property in this) {
            if (this.hasOwnProperty && this.hasOwnProperty(property)) {
                switch (typeof this[property]) {
                case 'function':
                    functions.push(property);
                    break;
                case 'undefined':
                    break;
                default:
                    properties.push(property + ':' + this[property]);
                    break;
                }
            }
        }
        retString += '\n\tproperties:[' + properties.toString() + '],';
        retString += '\n\tfunctions:[' + functions.toString() + ']';
        retString += '\n]';
        return retString;
    }
};


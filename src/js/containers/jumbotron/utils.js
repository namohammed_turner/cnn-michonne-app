/* global CNN */

/**
 * Utility methods for Jumbtotron.
 */
CNN.Jumbotron.Utils = {
    detailsSelector: CNN.Jumbotron.Constants.CONST_CARD_DETAILS_SELECTOR,
    thumbnailsSelector: CNN.Jumbotron.Constants.CONST_SMALL_CAROUSEL_SELECTOR,
    detailsCardSelector: '.js-jumbotron-card-wrapper',

    /**
     * Simple utility for Class inheritance.
     *
     * @param {object} base - base object to inherit from
     * @param {object} sub - sub-ordinate object
     */
    extend: function (base, sub) {
        'use strict';

        var origProto = sub.prototype,
            key;

        sub.prototype = Object.create(base.prototype);

        /*
         * No calls are being made to .hasOwnProperty within this loop to ensure it
         * works on IE8
         */
        for (key in origProto) {
            if (origProto.hasOwnProperty(key)) {
                sub.prototype[key] = origProto[key];
            }
        }
        /* Remember the constructor property was set wrong, let's fix it */
        sub.prototype.constructor = sub;
        /*
         * In ECMAScript5+ (all modern browsers), you can make the constructor property
         * non-enumerable if you define it like this instead
         */
        try {
            Object.defineProperty(sub.prototype, 'constructor', {
                enumerable: false,
                value: sub
            });
        } catch (error) {
            /* IE <= 8 will throw an error here because MS can't follow any kind of recommended standards. */
        }
    },

    /**
     * Generates the elements that will make up the small carousel under the jumbotron when not in
     * "mobile" mode.
     *
     * @param {object} jumbotronContainerElement - The jumbotron container to check.
     * @returns {array} - Array of new jQuery elements for the jumbotron carousel.
     */
    generateSmallCarouselContent: function (jumbotronContainerElement) {
        'use strict';

        var newElements = [],
            detailsContainer = jQuery(jumbotronContainerElement).find(this.detailsSelector),
            thumbnailContainer = jQuery(jumbotronContainerElement).find(this.thumbnailsSelector),
            additionalClass = '',
            numDetailCards = 0;

        thumbnailContainer.empty();
        if (typeof detailsContainer !== 'undefined') {
            numDetailCards = jQuery(detailsContainer).find(this.detailsCardSelector).length;
            jQuery(detailsContainer).find(this.detailsCardSelector).each(function (index, elem) {
                var $elem = jQuery(elem),
                    bannerText = $elem.find('.banner-text').text(),
                    headline = $elem.find('.cd__content .cd__headline').text(),
                    imgSrc = $elem.find('.media img').attr('data-src-mini'),
                    imgTag = jQuery('<img>'),
                    newElement;

                if (index === 0) {
                    additionalClass = 'cd--first-item';
                } else if (index === (numDetailCards - 1)) {
                    additionalClass = 'cd--last-item';
                } else {
                    additionalClass = '';
                }
                newElement = jQuery(
                    '<div class="cn__column carousel__content__item js-jumbotron-card-wrapper">' +
                    '<div class="cd ' + additionalClass + '" data-index="' + index + '">' +
                    '<div class="media"></div><div class="cd__content "></div>' +
                    '</div></div>'
                );

                if (typeof imgSrc === 'undefined' || imgSrc === '') {
                    imgSrc = $elem.find('.media img').attr('src');
                }
                imgTag.attr('src', imgSrc);

                newElement.find('.media').append(imgTag);
                newElement.find('.cd__content').append(bannerText ? bannerText : headline);
                newElements.push(newElement);
            });
        }
        return newElements;
    }
};


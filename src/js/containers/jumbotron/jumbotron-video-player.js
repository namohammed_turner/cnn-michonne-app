/* global CNN */

CNN.VideoConfig = CNN.VideoConfig || {};

/**
 * Class for the Video Player for the jumbotron.
 *
 * @param {object} videoPlayerElementWrapper - Element enclosing the video player element
 * @param {object} overrides - video config overrides object
 */
CNN.Jumbotron.VideoPlayer = function (videoPlayerElementWrapper, overrides) {
    'use strict';

    this.videoPlayerElementWrapper = videoPlayerElementWrapper;
    this.constants = CNN.Jumbotron.Constants;
    this.isVideoPaused = false;
    this.isVideoPlaying = false;
    this.videoConfigOverrides = {};
    this.autoPaused = false;
    this.currentVideoInst = {};
    this.rawElement = jQuery(videoPlayerElementWrapper).find(this.constants.CONST_CONTAINER_VIDEO_PLAYER_SELECTOR);
    this.videoPlayerElement = {};
    this.detailCardContent = {};
    jQuery.extend(this, this.rawElement);

    /**
     * This will be the method that will be called if this VideoPlayer object
     * was created before the video loader was initialized.
     */
    this.onVideoAPILoadComplete = function () {
        window.cnnVideoManager.renderVideoContainer(this.videoPlayerConfiguration);
    };

    /**
     * Initialize the Video Player.
     * @param {object} el - the DOM element to convert to a VideoPlayer
     * @param {object} overrides - Video Player overrides
     */
    this.initialize = function (el, overrides) {
        this.videoPlayerElement = jQuery(el);
        if (overrides) {
            this.videoConfigOverrides = overrides;
        }
        this.createVideoPlayerConfig();
    };

    /**
     * Returns the Video Loader handler for this video player
     * @returns {object} - The video loader handler object, or a blank one
     */
    this.getVideoManager = function () {
        try {
            return window.cnnVideoManager.getPlayerByContainer(this.videoPlayerElement.attr('id'));
        } catch (error) {
            return {
                play: function () {},
                resume: function () {},
                pause: function () {},
                stop: function () {}
            };
        }
    };

    /**
     * Create video player config. This is needed for the loader
     * @returns {object} - The Video Configuration
     */
    this.createVideoPlayerConfig = function () {
        var videoObj,
            callbacks,
            self = this;

        if (!this.videoPlayerConfiguration) {
            videoObj = {
                markupId: this.videoPlayerElement.attr('id'),
                section: CNN.Edition,
                profile: 'expansion',
                network: CNN.VideoConfig.network || 'cnn',
                context: (this.videoConfigOverrides && this.videoConfigOverrides.context) ? this.videoConfigOverrides.context : 'general',
                width: '100%',
                height: '100%',
                frameWidth: '100%',
                frameHeight: '100%',
                thumb: '',
                analytics: 'jsmdJumbotron',
                autostart: true
            };
            callbacks = {
                onAdPlay: function () {
                    self.autoPaused = false;
                    self.isVideoPlaying = true;
                    jQuery(self).trigger(self.constants.CONST_EVENT_CVP_EVENT,
                        new CNN.Jumbotron.Events.VideoActionEvent('onAdPlay'));
                },
                onAdPlayhead: function () {
                    self.isVideoPlaying = true;
                    jQuery(self).trigger(self.constants.CONST_EVENT_CVP_EVENT,
                        new CNN.Jumbotron.Events.VideoActionEvent('onAdPlayhead'));
                },
                onContentPlay: function (containerId) {
                    var evt = new CNN.Jumbotron.Events.VideoActionEvent('onContentPlay', containerId);

                    jQuery(self).trigger(self.constants.CONST_EVENT_CVP_EVENT, evt);
                    self.autoPaused = false;
                    self.isVideoPlaying = true;
                    self.isVideoPaused = false;
                },
                onContentPlayhead: function (containerId, playerId, _contentId, _playhead, _duration, _currentPlayTime) {
                    self.isVideoPlaying = true;
                    jQuery(self).trigger(self.constants.CONST_EVENT_CVP_EVENT,
                        new CNN.Jumbotron.Events.VideoActionEvent('onContentPlayhead', containerId, playerId));
                },
                onContentMetadata: function (containerId, playerId, metadata, contentId, duration, width, height) {
                    var evt;

                    CNN.VideoSourceUtils.updateSource(containerId, metadata);
                    evt = new CNN.Jumbotron.Events.VideoMetaDataActionEvent(containerId, playerId, metadata, contentId, duration, width, height);
                    jQuery(self).trigger(self.constants.CONST_EVENT_CVP_EVENT, evt);
                },
                onContentComplete: function (containerId, playerId, _contentId) {
                    jQuery(self).trigger(self.constants.CONST_EVENT_CVP_EVENT,
                        new CNN.Jumbotron.Events.VideoActionEvent('onContentComplete', containerId, playerId));
                },
                onContentPause: function (containerId, playerId, contentId, paused) {
                    self.isVideoPaused = paused;
                    self.isVideoPlaying = !paused;
                    jQuery(self).trigger(self.constants.CONST_EVENT_CVP_EVENT,
                        new CNN.Jumbotron.Events.VideoActionEvent('onContentPause', containerId, playerId));
                }
            };

            this.videoPlayerConfiguration = {
                videoConfig: videoObj,
                eventsCallback: callbacks,
                overridesConfig: {}
            };
        }
        return this.videoPlayerConfiguration;
    };

    /**
     * Stop Video Handler
     */
    this.onStopVideo = function () {
        jQuery(this.videoPlayerElement).hide();

        if (this.detailCardContent.removeClass) {
            this.detailCardContent.removeClass('hidden');
        }

        try {
            this.getVideoManager().stop();
        } catch (error) {
            /*
             * Errors could occur if video function gets called prior to
             * the video loader initialization
             */
        }
    };

    /**
     * Resume video handler
     */
    this.onResumeVideo = function () {
        try {
            this.getVideoManager().resume();
        } catch (error) {
            /*
             * Errors could occur if video function gets called prior to
             * the video loader initialization
             */
        }

        this.autoPaused = false;
    };

    /**
     * Pause Video handler
     * @param {boolean} [forced] - Force pausing video if true
     */
    this.onPauseVideo = function (forced) {
        try {
            this.getVideoManager().pause();
        } catch (error) {
            /*
             * Errors could occur if video function gets called prior to
             * the video loader initialization
             */
        }
        this.autoPaused = forced || false;
    };

    /**
     * Play video handler
     * @param {object} detailCard - The Detail Card that requested a video to be played in
     * @param {object} config - the Video config - this is the video id, uri, leaf url to play
     */
    this.onPlayVideoInCard = function (detailCard, config) {
        var cardMediaOverlayVideoPlayer;

        this.currentVideoInst = config;
        cardMediaOverlayVideoPlayer = jQuery(detailCard).find(this.constants.CONST_CARD_DETAILS_MEDIA_OVERLAY_VIDEO_CONTAINER_SELECTOR);
        cardMediaOverlayVideoPlayer.prepend(this.videoPlayerElementWrapper);
        this.detailCardContent = jQuery('.cd__content, .el-action-bar, .cd__timestamp', detailCard);
        this.videoPlayerElement.show();
        cardMediaOverlayVideoPlayer.show();
        this.detailCardContent.addClass('hidden');
        try {
            if (config.videoId === this.videoPlayerConfiguration.videoConfig.videoId) {
                this.getVideoManager().resume();
            } else {
                try {
                    if (window.cnnVideoManager.getPlayerByContainer(this.videoPlayerElement.attr('id'))) {
                        this.getVideoManager().play(this.currentVideoInst.videoId);
                    } else {
                        this.videoPlayerConfiguration.videoConfig.video = this.currentVideoInst.videoId;
                        window.cnnVideoManager.renderVideoContainer(this.videoPlayerConfiguration);
                    }
                } catch (error) {
                    this.videoPlayerConfiguration.videoConfig.video = this.currentVideoInst.videoId;
                    window.cnnVideoManager.renderVideoContainer(this.videoPlayerConfiguration);
                }
            }
        } catch (error) {
            /*
             * Errors could only occur if video function gets called prior to
             * the video loader initialization. Fail gracefully.
             * The loader itself will take care of reinitializing the video
             * play in these cases.
             */
        }
    };

    this.initialize(jQuery(videoPlayerElementWrapper).find(this.constants.CONST_CONTAINER_VIDEO_PLAYER_SELECTOR), overrides);

};

/**
 * Public methods
 */
CNN.Jumbotron.VideoPlayer.prototype = {
    /**
     * Sets video config overrides for a specific video
     * @param {object} overrides - Video player config overrides object
     */
    setVideoConfigOverrides: function (overrides) {
        'use strict';

        this.videoConfigOverrides = overrides;
    },

    /**
     * Returns true if the current video is playing, false otherwise
     * @returns {boolean} - true if video is playing, false otherwise
     */
    isPlaying: function () {
        'use strict';

        return this.isVideoPlaying;
    },

    /**
     * Returns true if the video is autopaused, false otherwise
     * @returns {boolean} - true if video is auto paused, false otherwise
     */
    isAutoPaused: function () {
        'use strict';

        return this.autoPaused;
    },

    /**
     * Retuns true if video is paused, false otherwise
     * @returns {boolean} - true if video is paused, false otherwise
     */
    isPaused: function () {
        'use strict';

        return this.isVideoPaused;
    },

    /**
     * Play a video in a given detail card
     * @param {object} detailCard - The card to play the video in
     * @param {object} videoConfig - The video to play
     */
    playVideoInCard: function (detailCard, videoConfig) {
        'use strict';

        this.onPlayVideoInCard(detailCard, videoConfig);
    },

    /**
     * Pause the current video, if it's playing
     * @param {boolean} [forced] - true if it's an auto pause (Eg pause caused by scrolling off the screen)
     */
    pauseVideo: function (forced) {
        'use strict';

        this.onPauseVideo(forced);
    },

    /**
     * Resume play of a paused video
     */
    resumeVideo: function () {
        'use strict';

        this.onResumeVideo();
    },

    /**
     * Stop play of a video and clean up the video instance
     */
    stopVideo: function () {
        'use strict';

        this.onStopVideo();
    }
};


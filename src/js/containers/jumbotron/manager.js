/* global CNN */

/**
 * This Object iterates over all the jumbotron elements on the page and
 * initializes them.
 * @param {object} elements - the DOM elements to convert to jumbotron instances
 */
CNN.Jumbotron.Manager = function (elements) {
    'use strict';

    this.jumbotronRawElements = elements;
    this.jumbotrons = [];

    this.constants = CNN.Jumbotron.Constants;
    this.mode = this.constants.CONST_DESKTOP_MODE;
    this.windowEventsAdded = false;
    this.resizeEventAdded = false;

    /**
     * Add all event listeners
     */
    this.addEventListeners = function () {
        /*
         * Add the scroll event for the window
         */
        if (!this.windowEventsAdded) {
            this.windowEventsAdded = true;
            jQuery(window).scroll(jQuery.proxy(this.onWindowScroll, this));

        }

        /**
         * Adds the window resize listener
         */
        if (!this.resizeEventAdded) {
            jQuery(window).resize(jQuery.proxy(this.onWindowResize, this));
            this.resizeEventAdded = true;
        }
    };

    /**
     * Callback function for window resize. This is set on a timer
     * so that it doesn't cause unnecessary calls to updateLayout until the user
     * has stopped resizing the window.
     */
    this.onWindowResize = function () {
        if (this.resizeTO) {
            clearTimeout(this.resizeTO);
        }

        /*
         * call onWindowResizeComplete only after 1.5 seconds to make sure that the user
         * isn't still resizing the window
         */
        this.resizeTO = setTimeout(jQuery.proxy(this.onWindowResizeComplete, this), (1000 * 0.5));

    };

    /**
     * Called when window resize event is complete
     */
    this.onWindowResizeComplete = function () {
        jQuery(this.jumbotrons).each(function (index, elem) {
            if (elem.onWindowResize) {
                elem.onWindowResize();
            }
        });
    };

    /**
     * Called when window scroll event is complete
     */
    this.onWindowScrollComplete = function () {
        jQuery(this.jumbotrons).each(function (index, elem) {
            if (jQuery(elem.getJumbotronElement()).visible(true)) {
                if (elem.resumeAutoScroll) {
                    elem.resumeAutoScroll(false);
                }
            }else {
                if (elem.pauseAll) {
                    elem.pauseAll();
                }
            }
        });
    };

    /**
     * Window scroll event handler
     */
    this.onWindowScroll = function () {
        if (this.scrollWaitTimeout) {
            clearTimeout(this.scrollWaitTimeout);
        }

        /*
         * Call the update on each container right away to get it to initialize
         * responsive images/layout, if needed.
         */
        jQuery(this.jumbotrons).each(function (index, elem) {
            if (jQuery(elem.getJumbotronElement()).visible(true)) {
                if (elem.update) {
                    elem.update(false);
                }
            }
        });

        /*
         * Wait a predetermined amount of time before firing off the
         * onWindowScrollComplete handler to make sure the user isn't still
         * scrolling
         */
        this.scrollWaitTimeout = setTimeout(jQuery.proxy(this.onWindowScrollComplete, this), CNN.JumbotronConfig.scrollPausesJumbotronInterval || 5000);
    };

    /**
     * Iterate over all the DOM elements that should be converted to
     * Jumbotron instances and initialize them.
     */
    this.initializeRawContainers = function () {
        var el,
            i;
        for (i = 0; i < this.jumbotronRawElements.length; i++) {
            el = this.jumbotronRawElements[i];
            this.initializeContainer(el);
        }

    };

    /**
     * Create a Jumbtotron Object for the given DOM element
     * @param {object} rawEl - DOM element to create jumbotrom object for
     */
    this.initializeContainer = function (rawEl) {
        var jumbotronInstance;

        if (typeof rawEl !== 'undefined') {
            jumbotronInstance = new CNN.Jumbotron.Container(rawEl, {clientMode: this.mode});
            this.jumbotrons.push(jumbotronInstance);
        }
    };

    this.initializeRawContainers();
    this.addEventListeners();
};

/**
 * Public accessible methods for Manager
 */
CNN.Jumbotron.Manager.prototype = {
    /**
     * Returns the Jumbotron Instances on the page
     * @returns {array} - Array of jumbotron instances
     */
    getJumbotrons: function () {
        'use strict';

        return this.jumbotrons;
    },

    /**
     * Returns the device mode
     * @returns {string} - The device mode
     */
    getDeviceMode: function () {
        'use strict';

        return this.mode;
    }
};

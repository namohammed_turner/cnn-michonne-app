/* global CNN */

CNN.Jumbotron = CNN.Jumbotron || {};

/**
 * The Bootstrapper is responsible for checking the DOM and seeing if there
 * are any jumbotrons on the page. If there are, it loads the jumbotron dependencies.
 * This way, these dependencies are only loaded if necessary.
 */
CNN.Jumbotron.Bootstrapper = CNN.Jumbotron.Bootstrapper || {
    carouselSelector: '.js-owl-carousel',
    jumbotronContainerSelector: '.js-jumbotron-container',
    CONST_JUMBOTRON_LOAD_COMPLETE_EVENT: 'jumbotron-initialization-complete',
    jumbotronElements: [],

    /*
     * Initializes the bootstrapper
     */
    initialize: function () {
        'use strict';
        if (this.shouldLoadJumbotronDependencies()) {
            this.loadJumbotronDependencies();
        }
    },

    /*
     * Checks to see if jumbotron dependencies should be loaded.
     *
     * @return true if there are jumbotron(s) on the page, false otherwise
     */
    shouldLoadJumbotronDependencies: function () {
        'use strict';
        this.jumbotronElements = jQuery(this.jumbotronContainerSelector);
        return (typeof this.jumbotronElements !== 'undefined' && typeof this.jumbotronElements.length !== 'undefined' && this.jumbotronElements.length > 0);
    },

    /**
     * Loads the jumbotron dependencies.
     */
    loadJumbotronDependencies: function () {
        'use strict';
        this.loadScript(CNN.Host.assetPath + '/js/jumbotron.min.js', jQuery.proxy(this.onJumbotronDependencyLoadComplete, this));
    },

    /**
     *  Loads a script file
     *
     * @param {string} url - The url to the script to load
     * @param {object} callback - The callback function to call when the script has completed load
     */
    loadScript: function (url, callback) {
        'use strict';
        var script = document.createElement('script');

        if (script.readyState) {  /* IE */
            script.onreadystatechange = function () {
                if (script.readyState === 'loaded' ||
                        script.readyState === 'complete') {
                    script.onreadystatechange = null;
                    callback();
                }
            };
        } else {  /* Others */
            script.onload = function () {
                callback();
            };
        }
        script.src = url;
        document.getElementsByTagName('head')[0].appendChild(script);
    },

    /**
     * Called when the jumbotron dependency scripts have completed loading.
     */
    onJumbotronDependencyLoadComplete: function () {
        'use strict';
        if (typeof CNN.Jumbotron.Manager !== 'undefined') {
            /*
             * Initialize the Manager
             */
            if (!window.cnnJumbotronManager) {
                window.cnnJumbotronManager = new CNN.Jumbotron.Manager(this.jumbotronElements);
            }

            jQuery(document).trigger(CNN.Jumbotron.Bootstrapper.CONST_JUMBOTRON_LOAD_COMPLETE_EVENT);
        }
    },

    /**
     * Called when the jumbotron dependency scripts have failed to load.
     */
    onJumbotronDependencyLoadFailure: function () {
        'use strict';
    }
};

jQuery(document).onZonesAndDomReady(function () {
    'use strict';

    CNN.Jumbotron.Bootstrapper.initialize();
});


/* global Modernizr, CNN */

/**
 * Class for the Carousel(s) for the jumbotron.
 * There are 2 modes for the carousel for the Jumbotron, mobile vs non-mobile mode
 * In mobile mode, the Details Cards will be the carousel and in
 * non-mobile mode, a carousel will show up underneath the Details card(s)
 */
CNN.Jumbotron.Carousel = function () {
    'use strict';

    var isTouch = Modernizr.touchevents;

    /**
     * Cleanup resources/carousel objects.
     */
    this.cleanup = function () {
        try {
            if (this.owlContainer && this.owlContainer.data) {
                this.owlContainer.trigger(CNN.Carousel.events.destroy);
            }

            if (this.rawElement) {
                this.removeEventListeners(this.rawElement);
            }

        } catch (error) {
            /*
             * if the owl carousel had been removed by internal processes,
             * then an error would occur, but that means all resources would've been
             * purged anyway
             */
        }
    };

    /**
     * Returns the Owl Carousel instance
     * @returns {object} - The carousel instance object, or a blank one.
     */
    this.getOwl = function () {
        if (this.owlContainer && this.owlContainer.data) {
            return this.owlContainer.data('owl.carousel');
        }
        return {
            goTo: function () {},
            destroy: function () {}
        };
    };

    /**
     * Add Event Listeners for OWL element
     * @param {object} element - the jQuery object for the carousel
     */
    this.addEventListeners = function (element) {
        /* Add OWL-specific event listeners */
        element.on(CNN.Carousel.events.initialize, this.onInitialize);
        element.on(CNN.Carousel.events.initialized, this.onInitialized);
        element.on(CNN.Carousel.events.initialized, this.setNumberOfCarouselItems);
        element.on(CNN.Carousel.events.initialized, this.carouselAdjustNavigation);
        element.on(CNN.Carousel.events.navigationInitialized, this.carouselAdjustNavigation);
        element.on(CNN.Carousel.events.resized, this.carouselAdjustNavigation);
        element.on(CNN.Carousel.events.resized, this.setNumberOfCarouselItems);
        element.on(CNN.Carousel.events.changed, this.onCarouselChange);
        element.on(CNN.Carousel.events.changed, this.carouselAdjustNavigation);
    };

    /**
     * Remove Event Listeners for OWL element
     * @param {object} element - the jQuery object for the carousel
     */
    this.removeEventListeners = function (element) {
        /* Remove OWL-specific event listeners */
        element.off(CNN.Carousel.events.initialize, this.onInitialize);
        element.off(CNN.Carousel.events.initialized, this.onInitialized);
        element.off(CNN.Carousel.events.initialized, this.setNumberOfCarouselItems);
        element.off(CNN.Carousel.events.initialized, this.carouselAdjustNavigation);
        element.off(CNN.Carousel.events.navigationInitialized, this.carouselAdjustNavigation);
        element.off(CNN.Carousel.events.resized, this.carouselAdjustNavigation);
        element.off(CNN.Carousel.events.resized, this.setNumberOfCarouselItems);
        element.off(CNN.Carousel.events.changed, this.onCarouselChange);
        element.off(CNN.Carousel.events.changed, this.carouselAdjustNavigation);

        /* Remove custom event listeners */
        jQuery(element).off(CNN.Jumbotron.Constants.CONST_OWL_EVENT_TYPES.move,
            CNN.Jumbotron.Constants.CONST_OWL_WRAPPER_SELECTOR, this.onDragStart);
        jQuery(element).off(CNN.Jumbotron.Constants.CONST_OWL_EVENT_TYPES.start,
            CNN.Jumbotron.Constants.CONST_OWL_CONTROLS_SELECTOR);
    };


    /**
     * Suppresses the drag event if the user was interacting with the
     * video object (only an issue with the flash video player).
     * This is so that interactions on the video player such as
     * clicking on pause, play or sliding on the seekbar or the
     * volume control doesn't initiate a drag of the carousel
     *
     * @param {object} e - the event
     */
    this.onDragStart = function (e) {
        /*
         * Stop the event from bubbling if the target is
         * an object or embed tag. This indicates it's a Flash
         * Video Object.
         */
        if (jQuery(e.target).is('object, embed, video')) {
            e.stopPropagation();
            e.stopImmediatePropagation();
        }
        return ;
    };

    /**
     * Dispatch User Interaction events
     * @param {string} action - The user interaction that occured (eg Drag Events)
     */
    this.onUserInteraction = function (action) {
        var event = new CNN.Jumbotron.Events.CarouselActionEvent(action);

        this.rawElement.trigger(event.eventType, event);
    };

    /**
     * Dispatch an event for when a user clicks in an item in the carousel.
     * @param {object} e - jQuery Event passed as event handler callback parameter
     */
    this.onItemClicked = function (e) {
        var event = new CNN.Jumbotron.Events.CarouselActionEvent(CNN.Jumbotron.Constants.CONST_EVENT_CAROUSEL_ACTION_TYPE_CLICK),
            index = parseInt(jQuery(e.target).parents('.owl-item').find('.cd').attr('data-index'), 10);

        /*
         * Trigger an event up to the container
         */
        this.rawElement.trigger(event.eventType, event);

        /*
         * Trigger an event to move the filmstrip
         */
        this.rawElement.trigger(CNN.Carousel.events.to, [index]);
    };

    /**
     * Dispatch an event for turning off autoscroll
     */
    this.stopAutoScrollHandler = function () {
        var event = new CNN.Jumbotron.Events.CarouselActionEvent(CNN.Jumbotron.Constants.CONST_EVENT_JUMBOTRON_STOP_AUTOSCROLL);

        this.rawElement.trigger(event.eventType, event);
    };

    /**
     * Set the item index to mark as active in the carousel
     * @param {number} itemIndexValue - The index of the item in the carousel
     */
    this.setSelectedIndex = function (itemIndexValue) {
        /*
         * Set the selected item as active
         */
        if (!this.rawElement) { return; }

        this.rawElement.find('.cd').removeClass('cd--active');
        this.rawElement.find('.cd[data-index="' + itemIndexValue + '"]').addClass('cd--active');
    };

    /**
     * Dispatches the Carousel Item selected event and sets the active item for the
     * carousel .
     *
     * @param {number} itemIndexValue - The index of the item in the carousel
     */
    this.onItemSelected = function (itemIndexValue) {
        var event;

        /* Set the active element in the carousel */
        this.setSelectedIndex(itemIndexValue);

        /* Dispatch item selected event to any object that's listening */
        event = new CNN.Jumbotron.Events.CarouselItemSelectedEvent(itemIndexValue);
        this.rawElement.trigger(event.eventType, event);
    };

    /**
     * Initializes the carousel.
     * @param {object} elementToConvert - The Dom element that will be the carousel
     * @param {object} config - additional configuration for the carousel
     */
    this.initializeCarousel = function (elementToConvert, config) {
        var self = this;

        this.cleanup();
        this.config = config;
        this.rawElement = elementToConvert;
        this.isThumbnailCarousel = elementToConvert.hasClass(CNN.Jumbotron.Constants.CONST_SMALL_CAROUSEL_CLASS_NAME);

        /*
         * Here are the event handlers for the carousel
         */

        /**
         * Fires before full initialization of the carousel.
         * @param {object} _e - The OWL Event
         */
        this.onInitialize = function (_e) {
            jQuery(elementToConvert).off('click', CNN.Jumbotron.Constants.CONST_OWL_ITEM_SELECTOR);
            /*
             * If the carousel is in non-mobile mode, then dispatch
             * Item selected events when a user clicks on an item in the
             * carousel.
             */
            if (self.isThumbnailCarousel) {
                jQuery(elementToConvert).on('click', CNN.Jumbotron.Constants.CONST_OWL_ITEM_SELECTOR, function (e) {
                    var num;

                    e.preventDefault();
                    num = parseInt(jQuery(this).find('.cd').attr('data-index'), 10);
                    self.onItemClicked(e);
                    self.onItemSelected(num);
                });
            }
        };

        /**
         * Fired off after initialization of the Carousel is complete
         * @param {object} e - OWL Event passed as event handler callback parameter
         */
        this.onInitialized = function (e) {
            var owl = e.relatedTarget,
                $firstImg = owl._items[0].find('img');

            /*
             * Post initialization work here. This will be for setting styles on cards, etc.
             */
            jQuery(elementToConvert).off(CNN.Jumbotron.Constants.CONST_OWL_EVENT_TYPES.start,
                CNN.Jumbotron.Constants.CONST_OWL_CONTROLS_SELECTOR);
            jQuery(elementToConvert).on(CNN.Jumbotron.Constants.CONST_OWL_EVENT_TYPES.start,
                CNN.Jumbotron.Constants.CONST_OWL_CONTROLS_SELECTOR, function (_e) {
                    self.stopAutoScrollHandler();
                });

            /* Add drag overrides for embed, video and object tags */
            jQuery(elementToConvert).off(CNN.Jumbotron.Constants.CONST_OWL_EVENT_TYPES.move,
                CNN.Jumbotron.Constants.CONST_OWL_WRAPPER_SELECTOR, self.onDragStart);
            if (!elementToConvert.hasClass(CNN.Jumbotron.Constants.CONST_SMALL_CAROUSEL_CLASS_NAME)) {
                jQuery(this).on(CNN.Jumbotron.Constants.CONST_OWL_EVENT_TYPES.move,
                    CNN.Jumbotron.Constants.CONST_OWL_WRAPPER_SELECTOR, self.onDragStart);
            }

            /* Mark the first non-cloned item as active */
            self.onItemSelected(0);

            /* Mark the jumbotron as initialized */
            jQuery(self.rawElement.context).addClass('jumbotron-initialized');

            if ($firstImg.height() < 5) {
                $firstImg.on('load', function (_e) {
                    owl.$element.trigger(CNN.Carousel.events.refresh);
                });
            }
        };

        /**
         * Make the next/prev buttons the height of the image
         * @param {object} _e - OWL Event passed as event handler callback parameter
         */
        this.carouselAdjustNavigation = function (_e) {
            var instance = this,
                $carousel = jQuery(this),
                $media = $carousel.find('.owl-item').find('.media'),
                /* $cardWrapper,
                $status, */
                height;

            /* These values are never used...
            if (e.property && e.property.name === 'position') {
                $cardWrapper = $carousel.find('.owl-item .cd__wrapper').eq(e.property.value);
                $status = $carousel.find('.owl-item').eq(e.property.value).find('.cd__status');

            } else {
                $cardWrapper = $carousel.find('.owl-item.active .cd__wrapper');
                $status = $carousel.find('.owl-item.active .cd__status');
            }
            */

            if ($media.length === 0) { return; }

            height = ($media.width() * 9 / 16);

            if (height < 5) {
                $carousel.find('.owl-item').find('.media img').load(function (e) {
                    self.carouselAdjustNavigation.call(instance, e);
                });
            }

            $carousel.find('.owl-prev, .owl-next').css({
                height: height + 'px'
            });
        };

        /**
         * Calculate number of items that will fit in a given carousel, so that we can
         * limit the amount of horizontal movement
         * @param {object} e OWL Event passed as event handler callback parameter
         */
        this.setNumberOfCarouselItems = function (e) {
            var owl = e.relatedTarget,
                numItems;

            if (owl._widths[0] === 0) {
                return;
            } else {
                numItems = owl._width / owl._widths[0];
            }

            if (owl.settings.items > 1) {
                owl.settings.items = Math.ceil(numItems);
                owl.trigger(CNN.Carousel.events.refresh);
            }
        };

        /**
         * Fired off after a change in the carousel
         * This handler will only respond to change in 'position'
         * @param {object} e - OWL Event passed as event handler callback parameter
         */
        this.onCarouselChange = function (e) {
            var index;

            if (e.property.name === 'position') {
                /* find the current card at position 1, and then find it's .cd data-index attribute value */
                index = parseInt(jQuery(this).find('.owl-item').eq(e.property.value).find('.cd').attr('data-index'), 10);

                self.onItemSelected(index);
            }
        };

        /**
         * Fired off when the owl carousel starts drag.
         * @param {object} _e - Event passed as event handler callback parameter
         */
        this.onDragStart = function (_e) {
            self.onUserInteraction(CNN.Jumbotron.Constants.CONST_EVENT_CAROUSEL_ACTION_TYPE_DRAG);
        };


        /**
         * Fired off when the owl carousel is resized.
         * @param {object} _e - Event passed as event handler callback parameter
         */
        this.onResized = function (_e) {
            if (this.rawElement) {
                this.rawElement.data('owl.carousel').onResize();
            }
        };

        /*
         * Add Event Listeners for the DOM Element with our OWL
         */
        this.addEventListeners(elementToConvert);

        /*
         * Initialize the OWL
         */
        this.owlContainer = elementToConvert.owlCarousel({
            dots: false,
            mouseDrag: true,
            touchDrag: true,
            nav: true,
            navText: ['', ''],
            autoWidth: !isTouch,
            autoHeight: isTouch,
            responsive: false,
            scrollPerPage: false,
            slideBy: 1,
            items: isTouch ? 1 : 2,
            itemsDesktop: [1100, 3],
            itemsDesktopSmall: [900, 3],
            itemsTablet: [600, 2],
            itemsMobile: false,
            loop: true
        });
    };
};

/**
 * Carousel methods
 */
CNN.Jumbotron.Carousel.prototype = {
    /**
     * Destroy the carousel and clean up resources
     */
    destroy: function () {
        'use strict';

        this.cleanup();
    },

    /**
     * Initialize the Carousel
     * @param {object} element - The DOM element to convert to a carousel
     * @param {object} config - Additional config for the carousel.
     */
    initialize: function (element, config) {
        'use strict';

        this.initializeCarousel(element, config);
    },

    /**
     * Navigate to a specific index in the carousel. If the
     * passed in index is greater than the number of elements in the carousel,
     * then the selected index will be 0
     * @param {number} index - the index to navigate to
     */
    gotoIndex: function (index) {
        'use strict';

        this.owlContainer.trigger(CNN.Carousel.events.to, [index]);
    },

    /**
     * Navigate to the next item in the carousel
     * @param {number} _fromItemIndex - The current index to navigate from
     */
    gotoNextItem: function (_fromItemIndex) {
        'use strict';

        /* Navigate to the next item */
        this.owlContainer.trigger(CNN.Carousel.events.next);
    }
};


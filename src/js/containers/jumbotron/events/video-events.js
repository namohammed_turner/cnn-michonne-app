/* global CNN */


/**
 * Base Event dispatched from the Jumbotron Video Player
 *
 * @param {string} eventType - The event type
 * @param {string} containerId - The container DOM id of the video player
 * @param {string} playerId - The CVP id of the video player
 */
CNN.Jumbotron.Events.VideoActionEvent = function (eventType, containerId, playerId) {
    'use strict';

    this.eventType = eventType;
    this.containerId = containerId;
    this.playerId = playerId;
};

/**
 * The VideoActionEvent methods
 */
CNN.Jumbotron.Events.VideoActionEvent.prototype = {
    /**
     * Returns the event type for the event
     * @returns {string} - The event type
     */
    getEventType: function () {
        'use strict';

        return this.eventType;
    },

    /**
     * Returns the player id (CVP id)
     * @returns {string} - The player id
     */
    getPlayerId: function () {
        'use strict';

        return this.playerId;
    },

    /**
     * Returns the container id for the video player
     * @returns {string} - The container id
     */
    getContainerId: function () {
        'use strict';

        return this.containerId;
    }
};

/**
 * The event fired when metadata for a currently playing video in the
 * jumbotron is retrieved
 * @param {string} containerId - The container DOM id of the video player
 * @param {string} playerId - The CVP id of the video player
 * @param {object} metadata - The video metadata
 * @param {string} contentId - The video id of the playing video
 * @param {string} duration - the video play length
 * @param {number} width - the width of the video encode
 * @param {number} height - the height of the video encode
 */
CNN.Jumbotron.Events.VideoMetaDataActionEvent = function (containerId, playerId, metadata, contentId, duration, width, height) {
    'use strict';

    CNN.Jumbotron.Events.VideoActionEvent.call(this, 'onContentMetadata', containerId, playerId);
    this.metadata = metadata;
    this.contentId = contentId;
    this.duration = duration;
    this.width = width;
    this.height = height;
};

/**
 * VideoMetaDataActionEvent methods
 */
CNN.Jumbotron.Events.VideoMetaDataActionEvent.prototype = {
    /**
     * Returns the metadata of the currently playing video in the jumbotron
     * @returns {object} - The metadata object
     */
    getMetadata: function () {
        'use strict';

        return this.metadata;
    }
};

CNN.Jumbotron.Utils.extend(CNN.Jumbotron.Events.VideoActionEvent, CNN.Jumbotron.Events.VideoMetaDataActionEvent);


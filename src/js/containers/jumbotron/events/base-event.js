/* global CNN */

/**
 * Base Event from which all other Jumbotron Events will extend
 * @param {string} eventType the Event Type
 * @param {object} [jumbotron] The Jumbotron instance
 */
CNN.Jumbotron.Events.EventBase = function (eventType, jumbotron) {
    'use strict';
    this.eventType = eventType;
    this.jumbotron = jumbotron;
};

/**
 * EventBase Methods
 */
CNN.Jumbotron.Events.EventBase.prototype = {
    /**
     * Return the event type
     * @returns {string} The event type that was dispatched
     */
    getEventType: function () {
        'use strict';
        return this.eventType;
    },

    /**
     * Returns the Jumtotron instance, if available
     * @returns {object} The jumbotron instance or undefined
     */
    getJumbotronInstance: function () {
        'use strict';
        return this.jumbotron;
    }
};

CNN.Jumbotron.Utils.extend(CNN.Jumbotron.ObjectBase, CNN.Jumbotron.Events.EventBase);


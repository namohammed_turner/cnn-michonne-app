/* global CNN */

/**
 * Base Event dispatched from the  Deteils Card. (The card above the carousel in
 * full screen mode) or the cards in the carousel in mobile view
 *
 * @param {string} actionType - The action type (Eg card-detail-video-play-event)
 * @param {object} card - the Card element that dispatched the event
 * @param {object} [videoConfig] - The video config to play. Contains Video Id, uri and leaf url
 * @param {object} [jumbotron] The Jumbotron instance
*/
CNN.Jumbotron.Events.CardDetailActionEvent = function (actionType, card, videoConfig, jumbotron) {
    'use strict';

    CNN.Jumbotron.Events.EventBase.call(this, CNN.Jumbotron.Constants.CONST_EVENT_CARD_ACTION_EVENT, jumbotron);
    this.actionType = actionType;
    this.card = card;
    this.videoConfig = videoConfig;
};
/**
 * CardDetailActionEvent Methods
 */
CNN.Jumbotron.Events.CardDetailActionEvent.prototype = {
    /**
     * Returns the action type for the event
     * @returns {string} - The action type string
     */
    getActionType: function () {
        'use strict';

        return this.actionType;
    },

    /**
     * Returns the card element that dispatched the event
     * @returns {object} - The card element
     */
    getCard: function () {
        'use strict';

        return this.card;
    },

    /**
     * Returns the video configuration, if applicable.
     * @returns {object} - The video config object
     */
    getVideoConfig: function () {
        'use strict';

        return this.videoConfig;
    }
};

CNN.Jumbotron.Utils.extend(CNN.Jumbotron.Events.EventBase, CNN.Jumbotron.Events.CardDetailActionEvent);

/**
 * A Video Play request event
 * @param {object} card - the Card element that dispatched the event
 * @param {object} videoConfig - The video config to play. Contains Video Id, uri and leaf url
 * @param {object} [jumbotron] The Jumbotron instance
 */
CNN.Jumbotron.Events.CardDetailVideoPlayRequestEvent = function (card, videoConfig, jumbotron) {
    'use strict';

    CNN.Jumbotron.Events.CardDetailActionEvent.call(this,
        CNN.Jumbotron.Constants.CONST_EVENT_CARD_ACTION_TYPE_VIDEO_PLAY_REQUEST,
        card,
        videoConfig,
        jumbotron
    );
};

/**
 * The CardDetailVideoPlayRequestEvent methods
 */
CNN.Jumbotron.Events.CardDetailVideoPlayRequestEvent.prototype = {
    /**
     * Returns the item index in the carousel where the video card resides
     * @returns {number} - The item index
     */
    getItemIndex: function () {
        'use strict';

        return this.itemIndex;
    }
};

CNN.Jumbotron.Utils.extend(CNN.Jumbotron.Events.CardDetailActionEvent, CNN.Jumbotron.Events.CardDetailVideoPlayRequestEvent);


/* global CNN */


/**
 * Base Event dispatched from the Jumbotron Carousel
 *
 * @param {string} actionType - The action type
 * @param {object} [jumbotron] The Jumbotron instance
*/
CNN.Jumbotron.Events.CarouselActionEvent = function (actionType, jumbotron) {
    'use strict';

    CNN.Jumbotron.Events.EventBase.call(this, CNN.Jumbotron.Constants.CONST_EVENT_CAROUSEL_ACTION_EVENT, jumbotron);
    this.actionType = actionType;
};

/**
 * The CarouselActionEvent methods
 */
CNN.Jumbotron.Events.CarouselActionEvent.prototype = {
    /**
     * Returns the action type for the event
     * @returns {string} - The action type string
     */
    getActionType: function () {
        'use strict';
        return this.actionType;
    }
};

CNN.Jumbotron.Utils.extend(CNN.Jumbotron.Events.EventBase, CNN.Jumbotron.Events.CarouselActionEvent);

/**
 * The event fired when an item in the carousel gets selected
 * @param {number} itemIndex - The index of the item that was selected
 * @param {object} [jumbotron] - The jumbotron instance
 */
CNN.Jumbotron.Events.CarouselItemSelectedEvent = function (itemIndex, jumbotron) {
    'use strict';

    CNN.Jumbotron.Events.CarouselActionEvent.call(this, CNN.Jumbotron.Constants.CONST_EVENT_CAROUSEL_ACTION_TYPE_ITEM_SELECTED, jumbotron);
    this.itemIndex = itemIndex;
};

/**
 * CarouselItemSelectedEvent methods
 */
CNN.Jumbotron.Events.CarouselItemSelectedEvent.prototype = {
    /**
     * Returns the index of the item in the carousel
     * @returns {number} - The item index
     */
    getItemIndex: function () {
        'use strict';

        return this.itemIndex;
    }
};

CNN.Jumbotron.Utils.extend(CNN.Jumbotron.Events.CarouselActionEvent, CNN.Jumbotron.Events.CarouselItemSelectedEvent);

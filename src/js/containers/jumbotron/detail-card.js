/* global CNN */

/**
 * Class for the Detail Card(s) for the jumbotron.
 *
 * @param {Element} detailCardElement - The DOM element of the detail card to use.
 */
CNN.Jumbotron.DetailCard = function (detailCardElement) {
    'use strict';

    this.rawElement = detailCardElement;
    this.videoConfig = {};
    this.constants = CNN.Jumbotron.Constants;
    jQuery.extend(this, this.rawElement);

    /**
     * Add Event listeners for the Detail Card
     */
    this.addEventListeners = function () {
        if (this.shareBar) {
            this.shareBar.off('click');
            this.shareBar.on('click', jQuery.proxy(this.onItemClicked, this));
        }
        if (this.btn_playVideo) {
            this.btn_playVideo.off('click');
            this.btn_playVideo.on('click', jQuery.proxy(this.onShowVideoClicked, this));
        }
    };

    /**
     * Hide the play button
     */
    this.hidePlayButton = function () {
        if (this.btn_playVideo) {
            jQuery(this.btn_playVideo).hide();
        }
    };

    /**
     * Show the play button
     */
    this.showPlayButton = function () {
        if (this.btn_playVideo) {
            jQuery(this.btn_playVideo).show();
        }
    };

    /**
     * Resets this card's layout if/when needed
     */
    this.reset = function () {
        this.showPlayButton();
        /* Hide the Source element until next play */
        this.cardMediaOverlayVideoPlayer = jQuery(this.rawElement).find(this.constants.CONST_CARD_DETAILS_MEDIA_OVERLAY_VIDEO_CONTAINER_SELECTOR);
        this.cardMediaOverlayVideoPlayer.hide();
    };

    /**
     * Clean up/remove all event listeners added by this object
     */
    this.removeEventListeners = function () {
        if (this.shareBar) {
            this.shareBar.off('click');
        }
        if (this.btn_playVideo) {
            this.btn_playVideo.off('click');
        }
    };

    /**
     * Cleanup resources/carousel objects.
     */
    this.cleanup = function () {
        this.removeEventListeners();
    };

    /**
     * Checks if the detail card is within a carousel or not.
     *
     * @returns {boolean} - true if the Detail Card is within a carousel, false otherwise
     */
    this.isCarousel = function () {
        return (jQuery(this.rawElement).has('.owl-item').length > 0);
    };

    /**
     * Dispatches events when an item is selected.
     */
    this.onItemClicked = function () {
        var event = new CNN.Jumbotron.Events.CarouselActionEvent(this.constants.CONST_EVENT_CAROUSEL_ACTION_TYPE_CLICK);

        jQuery(this.rawElement).trigger(event.getEventType(), event);
    };

    /**
     * Event Handler for when a video play in page start button was clicked
     *
     * @param {object} evt - Event object that triggered this handler.
     */
    this.onShowVideoClicked = function (evt) {
        var thisEvent;

        evt.stopPropagation();
        /* Dispatch an event letting the parent container know that a video play was requested */
        thisEvent = new CNN.Jumbotron.Events.CardDetailVideoPlayRequestEvent(this.rawElement, this.videoConfig);
        jQuery(this.rawElement).trigger(thisEvent.getEventType(), thisEvent);
    };

    /**
     * Initialize the Sharebar on the card detail.
     */
    this.initializeShareBar = function () {
        this.shareBar = jQuery(this.rawElement).find(this.constants.CONST_CARD_DETAILS_MEDIA_SHAREBAR_SELECTOR);
    };

    /**
     * Initialize any video start button that may be on the page if this is a video card
     *
     */
    this.initializeVideoButton = function () {
        this.btn_playVideo = jQuery(this.rawElement).find(this.constants.CONST_CARD_DETAILS_MEDIA_PLAY_VIDEO_BUTTON_SELECTOR);

    };

    /**
     * Initialize this Detail Card
     */
    this.initializeCard = function () {
        var vidId,
            vidCMSUrl,
            vidLeafUrl;

        vidId = jQuery(this.rawElement).find(this.constants.CONST_CARD_DETAILS_INPUT_VIDEO_ID_SELECTOR).val();
        vidCMSUrl = jQuery(this.rawElement).find(this.constants.CONST_CARD_DETAILS_INPUT_VIDEO_CMS_URI_SELECTOR).val();
        vidLeafUrl = jQuery(this.rawElement).find(this.constants.CONST_CARD_DETAILS_INPUT_VIDEO_LEAF_URL_SELECTOR).val();
        this.videoConfig = {
            videoId: vidId,
            videoCmsUri: vidCMSUrl,
            videoUrl: vidLeafUrl
        };
        this.cleanup();
        this.initializeVideoButton();
        this.initializeShareBar();
        this.addEventListeners();
    };

    this.initializeCard();
};

/**
 * Detail card public methods
 */
CNN.Jumbotron.DetailCard.prototype = {
    /**
     * Cleanup of Detail card
     */
    destroy: function () {
        'use strict';

        this.cleanup();
    }
};


/**
 * The Container (div) that wraps the Detail Cards
 * @param {object} detailCardContainerElement - The DOM element that wraps the detail cards
 */
CNN.Jumbotron.DetailCardContainer = function (detailCardContainerElement) {
    'use strict';

    this.rawElement = detailCardContainerElement;
    this.detailCards = [];
    jQuery.extend(this, this.rawElement);

    /**
     * Calls the reset method on all cards in this container.
     */
    this.resetCards = function () {
        var numCards = 0,
            cardImpl;

        try {
            for (numCards = 0; numCards < this.detailCards.length; numCards++) {
                cardImpl = this.detailCards[numCards];
                if (cardImpl.reset) {
                    cardImpl.reset();
                }
            }
        } catch (error) {
            /* fail gracefully. */
        }
    };

    /**
     * Initialize the container.
     */
    this.initialize = function () {
        var rawArray = [],
            rawCard,
            numElements;

        rawArray = jQuery(this.rawElement).find('.js-jumbotron-card-wrapper');
        /*
         * Iterate over all card detail elements in this container and initialize
         * them.
         */
        if (rawArray) {
            for (numElements = 0; numElements < rawArray.length; numElements++) {
                rawCard = rawArray[numElements];
                this.detailCards.push(new CNN.Jumbotron.DetailCard(rawCard));

            }
        }
    };

    this.initialize();
};


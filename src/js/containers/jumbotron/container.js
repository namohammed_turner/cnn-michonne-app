/* global Modernizr, CNN, eqjs */

/**
 * This is the Class for the Base Jumbotron container. This is the markup (div) that houses the cards and carousels
 * @param {object} jumbotronElement - The DOM element that is the Jumbotron
 * @param {object} config - jumbotron configuration
 */
CNN.Jumbotron.Container = function (jumbotronElement, config) {
    'use strict';

    var enableAutoScroll = Modernizr.touchevents ? false : CNN.JumbotronConfig.autoScrollEnabled;

    this.config = config;
    this.constants = CNN.Jumbotron.Constants;
    this.autoScroll = enableAutoScroll;
    this.autoScrollInterval = CNN.JumbotronConfig.autoScrollInterval || (1000 * 8);
    this.carouselSelector = this.constants.CONST_OWL_CAROUSEL_SELECTOR;
    this.detailsSelector = CNN.Jumbotron.Constants.CONST_CARD_DETAILS_SELECTOR;
    this.thumbnailsSelector = CNN.Jumbotron.Constants.CONST_SMALL_CAROUSEL_SELECTOR;
    this.jumbotronElementRaw = jumbotronElement;
    this.smallContainerContents = [];
    this.currentIndex = 0;
    this.hasInitializedResponsiveOverrides = false;
    this.containerSizeConstraints = CNN.JumbotronConfig.responsiveContainerConstraints || null;
    /* Events that will be dispatched from the Detail Card(s) */
    this.cardDetailActionEvents = [
        this.constants.CONST_EVENT_CARD_ACTION_EVENT,
        this.constants.CONST_EVENT_CARD_ACTION_TYPE_VIDEO_PLAY_REQUEST
    ];
    /* Events that will be dispatched from the Carousel */
    this.carouselActionEvents = [
        this.constants.CONST_EVENT_CAROUSEL_ACTION_TYPE_CLICK,
        this.constants.CONST_EVENT_CAROUSEL_ACTION_EVENT,
        this.constants.CONST_EVENT_CAROUSEL_ACTION_TYPE_MOVE_NEXT,
        this.constants.CONST_EVENT_CAROUSEL_ACTION_TYPE_MOVE_PREV,
        this.constants.CONST_EVENT_CAROUSEL_ACTION_TYPE_ITEM_SELECTED,
        this.constants.CONST_EVENT_CAROUSEL_ACTION_TYPE_CAROUSEL_INIT_START
    ];

    /**
     * Start autoscroll
     * @param {boolean} force - true if autoscroll should be started even if it had
     * previously been turned off
     */
    this.initiateAutoScroll = function (force) {
        force = force || false;
        if (force && enableAutoScroll) {
            this.autoScroll = true;
        }
        if (this.autoScroll && jQuery(this.jumbotronElementRaw).visible(true)) {
            this.doAutoScroll();
        }
    };

    /**
     * Autoscroll functionality.
     */
    this.doAutoScroll = function () {
        var self = this;
        /* If autoscroll is enabled, kick off autoscroll */
        if (this.autoScroll) {
            this.autoScrollTimeout = setTimeout(function () {
                if (self.autoScroll) {
                    clearTimeout(self.autoScrollTimeout);
                    self.doAutoScroll();
                    self.owlContainer.gotoNextItem(self.currentIndex);
                }
            }, this.autoScrollInterval);
        }
    };

    /**
     * Stop autoscroll functionality
     */
    this.onStopAutoScroll = function () {
        this.autoScroll = false;
        clearTimeout(this.autoScrollTimeout);
    };

    /**
     * Remove event listeners
     */
    this.removeEventListeners = function () {
        var numActionEventsIndex = 0;
        if (this.carouselActionEvents) {
            for (numActionEventsIndex = 0; numActionEventsIndex < this.carouselActionEvents.length; numActionEventsIndex++) {
                this.thumbnailsContainer.off(this.carouselActionEvents[numActionEventsIndex]);
                this.detailsContainer.off(this.carouselActionEvents[numActionEventsIndex]);
            }
        }
        if (this.cardDetailActionEvents) {
            for (numActionEventsIndex = 0; numActionEventsIndex < this.cardDetailActionEvents.length; numActionEventsIndex++) {
                this.detailsContainer.off(this.cardDetailActionEvents[numActionEventsIndex]);
            }
        }
    };

    /**
     * Adds jumbotron event listeners
     */
    this.addEventListeners = function () {
        var numActionEventsIndex = 0;

        for (numActionEventsIndex = 0; numActionEventsIndex < this.carouselActionEvents.length; numActionEventsIndex++) {
            this.thumbnailsContainer.on(this.carouselActionEvents[numActionEventsIndex], jQuery.proxy(this.onCarouselActionEvent, this));
            this.detailsContainer.on(this.carouselActionEvents[numActionEventsIndex], jQuery.proxy(this.onCarouselActionEvent, this));
        }

        for (numActionEventsIndex = 0; numActionEventsIndex < this.cardDetailActionEvents.length; numActionEventsIndex++) {
            this.detailsContainer.on(this.cardDetailActionEvents[numActionEventsIndex], jQuery.proxy(this.onCardActionEvent, this));
        }
    };

    /**
     * Event Handler for Card Detail events
     * @param {object} event - The event that was dispatched
     * @param {object} eventInst - The CardDetailActionEveng instance
     */
    this.onCardActionEvent = function (event, eventInst) {
        var card;
        event.stopPropagation();
        event.stopImmediatePropagation();
        if (eventInst instanceof CNN.Jumbotron.Events.CardDetailActionEvent) {
            if (eventInst.getActionType() === this.constants.CONST_EVENT_CARD_ACTION_TYPE_VIDEO_PLAY_REQUEST) {
                this.onStopAutoScroll();
                card = eventInst.getCard();
                this.videoPlayerContainer.playVideoInCard(card, eventInst.getVideoConfig());
            }
        }
    };

    /**
     * Event handler for Carousel Events
     * @param {object} event - The event that was dispatched
     * @param {object} eventInst - The CarouselActionEvent instance
     */
    this.onCarouselActionEvent = function (event, eventInst) {
        if (!jQuery(this.jumbotronElementRaw).has(event.target)) {
            return;
        }
        if (eventInst instanceof CNN.Jumbotron.Events.CarouselActionEvent) {
            switch (eventInst.getActionType()) {
            case this.constants.CONST_EVENT_JUMBOTRON_ITEM_SELECTED:
            case this.constants.CONST_EVENT_CAROUSEL_ACTION_TYPE_ITEM_SELECTED:
                this.handleCarouselItemSelected(eventInst.getItemIndex());
                event.stopPropagation();
                jQuery(this).trigger(this.constants.CONST_EVENT_JUMBOTRON_ITEM_SELECTED);
                break;
            case this.constants.CONST_EVENT_CAROUSEL_ACTION_TYPE_DRAG:
                this.handleCarouselDrag();
                this.onStopAutoScroll();
                break;
            case this.constants.CONST_EVENT_JUMBOTRON_STOP_AUTOSCROLL:
                event.stopPropagation();
                this.onStopAutoScroll();
                break;
            case this.constants.CONST_EVENT_CAROUSEL_ACTION_TYPE_CLICK:
                this.onStopAutoScroll();
                break;
            }
        }
    };

    /**
     * Carousel Drag behavior implementation.
     */
    this.handleCarouselDrag = function () {
        /*
         * Only pause the video during drag operation if we're in mobile mode
         * since the video player will be part of the carousel itself and we should pause
         * the video in this instance.
         *
         */
        if (this.currentCarouselElement && this.currentCarouselElement !== this.thumbnailsContainer) {
            if (this.videoPlayerContainer && this.videoPlayerContainer.isPlaying()) {
                this.videoPlayerContainer.pauseVideo();
            }
        }
    };

    /**
     * Handles the behavior for when a carousel item is selected
     * @param {number} itemIndex - The item index that was selected.
     */
    this.handleCarouselItemSelected = function (itemIndex) {
        var useAnimations = (this.config && this.config.clientMode && this.config.clientMode === this.constants.CONST_DESKTOP_MODE) ? true : false,
            detailCards;

        if (this.currentIndex !== itemIndex) {
            this.videoPlayerContainer.stopVideo();
        }

        this.currentIndex = itemIndex;
        jQuery(this.jumbotronElementRaw).attr('data-animation', (useAnimations ? this.dataAnimationAttributeValue : 'none'));

        /*
         * Reset detail cards, regardless of whether it's mobile or non-mobile mode
         */
        if (this.detailsContainer.resetCards) {
            this.detailsContainer.resetCards();
        }

        /*
         * If we're currently in non-mobile mode, set the active class on the detail
         * card elements.
         */
        if (this.currentCarouselElement !== this.detailsContainer) {
            if (this.detailsContainer instanceof jQuery === false) {
                this.detailsContainer = jQuery(this.detailsContainer);
            }
            detailCards = this.detailsContainer.find('.js-jumbotron-card-wrapper');
            detailCards.removeClass('active-card').addClass('inactive-card');
            detailCards.eq(itemIndex).removeClass('inactive-card').addClass('active-card');
        }
    };

    /**
     * Pause autoscroll (if it's active) and pause any video that's currently playing.
     * This is usually called when the jumbotron gets scrolled out of the active viewport
     * (eg. its not visible on the page)
     */
    this.pauseAll = function () {
        this.pauseAutoScroll();
        /* Only pause the video if it's currently playing */
        if (this.videoPlayerContainer && this.videoPlayerContainer.isPlaying()) {
            this.videoPlayerContainer.pauseVideo(true);
        }
    };

    /**
     * Pause Autoscroll.
     */
    this.pauseAutoScroll = function () {
        clearTimeout(this.autoScrollTimeout);
    };

    /**
     * Called by the Manager when the jumbotron comes into view in the browser.
     * The Manager will call this right away on scroll events, with no timeout
     * to see if scrolling has completed.
     * This is so that any intialization on items such as responsive images
     * within the Jumbotron don't have to wait to run
     */
    this.update = function () {
        this.handleResponsiveImages();
    };

    /**
     * Resume autoscroll. This is usually only called by the window scroll event
     * when the jumbtotron comes back into the viewport.
     * @param {boolean} force - true if autoscroll should be turned on even it
     *                          was previously turned off
     */
    this.resumeAutoScroll = function (force) {
        force = force || false;
        if (force && enableAutoScroll) {
            this.autoScroll = true;
        }
        if (this.autoScroll) {
            this.doAutoScroll();
        }
        if (this.videoPlayerContainer && this.videoPlayerContainer.isAutoPaused()) {
            this.videoPlayerContainer.resumeVideo();
        }

    };

    /**
     * Callback function for window resize. This is set on a timer
     * so that it doesn't cause unnecessary calls to updateLayout until the user
     * has stopped resizing the window.
     */
    this.onWindowResize = function () {
        if (this.owlContainer) {
            this.owlContainer.onResized();
        }
    };

    /**
     * Initialize the Jumbotron container
     */
    this.initialize = function () {
        var videoConfigOverrides = {};

        this.dataAnimationAttributeValue = jQuery(this.jumbotronElementRaw).attr('data-animation') || 'yoyoyo';

        if (CNN.Features && CNN.Features.enableShareButtons) {
            jQuery(this.jumbotronElementRaw).addClass('with-share-buttons');
        }

        this.thumbnailsContainer = jQuery(this.jumbotronElementRaw).find(this.thumbnailsSelector);
        try {
            videoConfigOverrides.context = jQuery(this.jumbotronElementRaw).find('#input_video_context').val();
        } catch (error) {
            videoConfigOverrides.context = 'general';
        }
        this.videoPlayerContainer = new CNN.Jumbotron.VideoPlayer(jQuery(this.jumbotronElementRaw)
            .find(CNN.Jumbotron.Constants.CONST_CONTAINER_VIDEO_PLAYER_WRAPPER_SELECTOR), videoConfigOverrides);
        this.detailsContainer = new CNN.Jumbotron.DetailCardContainer(jQuery(this.jumbotronElementRaw).find(this.detailsSelector));
        this.removeEventListeners();
        this.addEventListeners();
        this.handleResponsiveImages();
        this.initializeOwlContainer();
        this.initiateAutoScroll();
    };

    /**
     * Initializes the jumbotron's children's responsive images to set them to
     * the correct size.
     *
     */
    this.handleResponsiveImages  = function () {
        var initialJumbotronEQState;

        /* We only want to do this once and only if the jumbotron is visible
         * on the screen
         */
        if (!this.hasInitializedResponsiveOverrides && jQuery(this.jumbotronElementRaw).visible(true)) {
            if (eqjs && this.detailsContainer) {
                /*
                 * Force eqjs to refresh the settings for the detail cards
                 * Without this, the hidden cards do not get set to their
                 * correct sizes. This will only make calls to eqjs and
                 * ResponsiveImages for this specific container, not for the
                 * whole page. This is specifically targeted for just this
                 * Jumbotron instance.
                 */
                try {
                    initialJumbotronEQState = jQuery(this.jumbotronElementRaw).attr(this.constants.CONST_RESPONSIVE_EQ_STATE_ATTRIBUTE);

                    if (typeof initialJumbotronEQState !== 'undefined') {
                        this.detailsContainer.find(CNN.Jumbotron.Constants.CONST_RESPONSIVE_EQ_STATE_SELECTOR).each(function (index, element) {
                            jQuery(element).attr(CNN.Jumbotron.Constants.CONST_RESPONSIVE_EQ_STATE_ATTRIBUTE, initialJumbotronEQState);
                            /*
                             * force an eqjs.query and then have
                             * ResponsiveImages do a targeted updated on just
                             * the items in the the detail card.
                             */
                            eqjs.query(element, function (elements) {
                                if (window.CNN && window.CNN.ResponsiveImages) {
                                    window.CNN.ResponsiveImages.process(elements);
                                }
                            });

                        });

                    }
                    this.hasInitializedResponsiveOverrides = true;
                } catch (error) {
                    /*
                     * An error can occur in ReponsiveImages or eqjs and we
                     * don't want the jumbotron to stop working just because of
                     * a responsive image error.
                     */
                }
            }
        }
    };

    /**
     * Clean up of resources/etc
     */
    this.destroy = function () {
        if (this.owlContainer) {
            this.owlContainer.destroy();
        }
    };

    /**
     * Initializes the actual OWL carousel for the Jumbotron. This will either
     * end up being the thumbs under the Card or the Card(s) themselves.
     */
    this.initializeOwlContainer = function () {
        var elementToConvert;

        /*
         * If we are on a touch device, generate just one large carousel
         * If we're on a non-touch device, generate a thumbs carousel
         */
        if (typeof this.owlContainer !== 'undefined') {
            this.owlContainer.destroy();
        } else {
            this.owlContainer = new CNN.Jumbotron.Carousel();
        }

        if (Modernizr.touchevents) {
            elementToConvert = this.detailsContainer;
            this.thumbnailsContainer.empty();
        } else {
            elementToConvert = this.thumbnailsContainer;
            if (typeof this.smallContainerContents === 'undefined' || this.smallContainerContents.length === 0) {
                this.smallContainerContents = CNN.Jumbotron.Utils.generateSmallCarouselContent(jQuery(this.jumbotronElementRaw));
            }
            elementToConvert.empty();
            elementToConvert.append(this.smallContainerContents);
        }

        this.owlContainer.initialize(elementToConvert, this.config);
        this.currentCarouselElement = elementToConvert;
    };

    this.initialize(jumbotronElement, config);
};

/**
 * Public methods for Container
 */
CNN.Jumbotron.Container.prototype = {
    /**
     * Start autoscroll
     * @param {boolean} force - true if autoscroll should be started even if it
     * had previously been turned off
     */
    startAutoscroll: function (force) {
        'use strict';
        this.initiateAutoScroll(force);
    },

    /**
     * Stop Autoscroll
     */
    stopAutoscroll: function () {
        'use strict';
        this.onStopAutoScroll();
    },

    /**
     * returns the DOM element that is the jumbotron
     * @returns {Element} - The DOM element for the jumbotron
     */
    getJumbotronElement: function () {
        'use strict';
        return this.jumbotronElementRaw;
    },

    /**
     * Cleanup jumbotron resources and destroy the owl carousel
     */
    destroy: function () {
        'use strict';
        this.destroy();
    }

};

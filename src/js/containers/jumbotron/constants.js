/* global CNN */

/**
 * This is a constants file for Jumbtotron objects.
 */
CNN.Jumbotron.Constants = {
    CONST_CONTAINER_VIDEO_PLAYER_WRAPPER_SELECTOR: '.js-jumbotron-video-player-wrapper',
    CONST_CONTAINER_VIDEO_PLAYER_SELECTOR: '.js-jumbotron-video-player',
    CONST_OWL_CAROUSEL_SELECTOR: '.js-owl-carousel',
    /* Card Detail constants */
    CONST_CARD_DETAILS_STANDARD: 'cn-jumbotron-card-details',
    CONST_CARD_DETAILS_MOBILE: 'cn-jumbotron-card-details-mobile',
    CONST_CARD_DETAILS_INPUT_VIDEO_ID_SELECTOR: '#input_video_id',
    CONST_CARD_DETAILS_INPUT_VIDEO_CMS_URI_SELECTOR: '#input_video_cms_uri',
    CONST_CARD_DETAILS_INPUT_VIDEO_LEAF_URL_SELECTOR: '#input_video_leaf_url',
    CONST_CARD_DETAILS_SELECTOR: '.js-jumbotron-card-details',
    CONST_CARD_DETAILS_MEDIA_SHAREBAR_SELECTOR: '.m-share',
    CONST_CARD_DETAILS_MEDIA_PLAY_VIDEO_BUTTON_SELECTOR: '.js-el__video__play-button',
    CONST_CARD_DETAILS_MEDIA_OVERLAY_SELECTOR: '.js-jumbotron-card-media-overlay',
    CONST_CARD_DETAILS_MEDIA_OVERLAY_VIDEO_CONTAINER_SELECTOR: '.js-video-player-container',
    /* Thumbnail carousel constants */
    CONST_SMALL_CAROUSEL_CLASS_NAME: 'jumbotron-small-carousel',
    CONST_SMALL_CAROUSEL_SELECTOR: '.jumbotron-small-carousel',
    /* Event constants */
    CONST_EVENT_CARD_ACTION_TYPE_VIDEO_PLAY_REQUEST: 'card-detail-video-play-event',
    CONST_EVENT_CARD_ACTION_EVENT: 'card-action-event',
    CONST_EVENT_CAROUSEL_ACTION_EVENT: 'carousel-action-event',
    CONST_EVENT_CAROUSEL_ACTION_TYPE_DRAG: 'carousel-drag',
    CONST_EVENT_CAROUSEL_ACTION_TYPE_MOVE_NEXT: 'carousel-move-next',
    CONST_EVENT_CAROUSEL_ACTION_TYPE_MOVE_PREV: 'carousel-move-prev',
    CONST_EVENT_CAROUSEL_ACTION_TYPE_CLICK: 'carousel-item-clicked',
    CONST_EVENT_CAROUSEL_ACTION_TYPE_ITEM_SELECTED: 'carousel-item-selected',
    CONST_EVENT_CAROUSEL_ACTION_TYPE_CAROUSEL_INIT_START: 'carousel-init-start',
    CONST_EVENT_JUMBOTRON_ITEM_SELECTED: 'jumbotron-item-selected',
    CONST_EVENT_JUMBOTRON_STOP_AUTOSCROLL: 'jumbotron-stop-autoscroll',
    CONST_EVENT_JUMBOTRON_MODE_CHANGE: 'jumbotron-mode-change',

    /* Responsive contants */
    CONST_RESPONSIVE_EQ_STATE_ATTRIBUTE: 'data-eq-state',
    CONST_RESPONSIVE_EQ_STATE_SELECTOR: '.cd--large[data-eq-state], img[data-eq-state]',
    /* Owl constants */
    CONST_OWL_CONTROLS_SELECTOR: '.owl-nav',
    CONST_OWL_ITEM_SELECTOR: '.owl-item',
    CONST_OWL_WRAPPER_SELECTOR: '.owl-stage',
    CONST_OWL_EVENT_TYPES: {
        start: 'touchstart.owl mousedown.owl',
        move: 'touchmove.owl mousemove.owl',
        end: 'touchend.owl touchcancel.owl mouseup.owl'
    },
    /* Video Event Contants */
    CONST_EVENT_CVP_EVENT: 'CVPEvent'
};

/* global CNN */

/**
 * Used to set a User's Edition preferences
 *
 * Javascript Module Pattern
 * @see http://www.adequatelygood.com/JavaScript-Module-Pattern-In-Depth.html
 *
 * @name CNN.EditionPickerPrefs
 * @namespace
 * @memberOf CNN
 */
CNN.EditionPickerPrefs = CNN.EditionPickerPrefs || (function ($) {
    'use strict';

    var CONST_TTL_FOREVER = 854400,
        CONST_EDITION_COOKIE = CNN.EditionCookie;

    /**
     * Entry point for module. Initializes listeners but will
     * not set any temporary cookies
     *
     * @private
     */
    function _init() {
        _bindListeners();
    }

    /**
     * Setup required event handlers.
     *
     * @private
     */
    function _bindListeners() {
        $('.js-edition-confirm').on('click', confirmEdition);
    }

    /**
     * Sets a cookie containing the users edition preference.
     *
     * @param {string} edition - The selected edition choice
     * @param {number} ttl - The time to live value for the cookie
     * @private
     */
    function setEditionCookie(edition, ttl) {
        CNN.Utils.setCNNCookie(CONST_EDITION_COOKIE, edition, ttl);
    }

    /**
    * Calling the set cookie and change edition for selected edition.
    *
    * @param {string} choice - The selected edition choice
    * @param {object} targetChoice - selected choice object.
    */
    function selectEdition(choice, targetChoice) {
        setEditionCookie(choice, CONST_TTL_FOREVER);
        CNN.PlainNavigation.toggleEdition(false);
        CNN.PlainNavigation.changeEdition(false, targetChoice);
    }

    /**
    * Get the selected edition object for Nav Header.
    *
    * @param {object} event - jQuery event
    */
    function confirmEdition(event) {
        var editionChoice;

        event.preventDefault();
        editionChoice = $(event.target.form).children().filter('label').children().filter('input').filter(':checked');
        selectEdition(editionChoice.data('type'), editionChoice);
    }

    _init();

    return {
        init: _init
    };

})(jQuery);


/* global CNN */

/**
 * JavaScript to initialize Mobile TVE process/player
 *
*/
jQuery(document).onZonesAndDomReady(function () {
    'use strict';

    CNN.TVEMobile = {
        /**
         * Function to detect specific mobile/tablet platform and to render a
         * download app link appropiate to that platform
         *
         */
        isMobile: function () {
            var config = {
                    uaMatchString: 'iPad|iPhone|Android|Windows Phone|IEMobile|Kindle|Fire|Silk',
                    ipad: {
                        label: 'Download CNN iPad App',
                        link: 'https://itunes.apple.com/us/app/cnn-app-for-ipad/id407824176?mt=8'
                    },
                    iphone: {
                        label: 'Download CNN iPhone App',
                        link: 'https://itunes.apple.com/app/cnn-app-for-iphone/id331786748?mt=8'
                    },
                    android: {
                        label: 'Download CNN Android App',
                        link: 'https://play.google.com/store/apps/details?id=com.cnn.mobile.android.phone'
                    },
                    nook: {
                        label: 'Download CNN Nook App',
                        link: 'http://www.barnesandnoble.com/w/cnn-app-cnn-interactive-group-inc/1113900818?ean=2940043934499'
                    },
                    kindle: {
                        label: 'Download CNN Kindle App',
                        link: 'http://www.amazon.com/gp/product/B0093QQCGI'
                    }
                },
                buttonWrapper = jQuery(document.getElementById('js-tve-player__download__button__wrapper')),
                buttonContainer = jQuery('<div class="el__tve-player__download__button"></div>'),
                button = jQuery('<a href="#"></a>'),
                map = {},
                re = new RegExp('\\b(' + config.uaMatchString + ')\\b', 'ig'),
                match = re.exec(window.navigator.userAgent),
                device = String(((match instanceof Array) ? match[0] : match)).toLowerCase();

            if (typeof device !== 'undefined') {
                buttonContainer.append(button);
                buttonWrapper.append(buttonContainer);
                map = {
                    ipad: function () {
                        button.text(config.ipad.label).prop('href', config.ipad.link);
                    },
                    iphone: function () {
                        button.text(config.iphone.label).prop('href', config.iphone.link);
                    },
                    android: function () {
                        /* check if nook or android */
                        if (window.navigator.userAgent.toLowerCase().search('nook') > -1) {
                            button.text(config.nook.label).prop('href', config.nook.link);
                        } else {
                            button.text(config.android.label).prop('href', config.android.link);
                        }
                    },
                    windows_phone: function () {
                        button.replaceWith('<span>Coming Soon on Windows</span>');
                    },
                    kindle: function () {
                        button.text(config.kindle.label).prop('href', config.kindle.link);
                    }
                };
                /* iemobile function is similar to windows_phone */
                map.iemobile = map.windows_phone;
                /* Fire and Silk functions are similar to Kindle */
                map.fire = map.silk = map.kindle;
                /* executes function appropiate to the detected device.
                 * For any un-matched devices, it will not render the button
                 */
                map[device]();
            }
            jQuery(document.getElementById('js-tve-player-desktop')).hide();
            jQuery(document.getElementById('js-tve-player-mobile')).show();
        }
    };
});


/* global Modernizr, navigator */

/* code to sniff out iPod/iPhone/iPad UA to use modernizr to remove hover state */
Modernizr.addTest('ios', function () {
    'use strict';
    return !!navigator.userAgent.match(/(iPod|iPhone|iPad)/i);
});

Modernizr.addTest('android', function () {
    'use strict';
    return !!navigator.userAgent.match(/android/i);
});

Modernizr.addTest('iemobile', function () {
    'use strict';
    return !!navigator.userAgent.match(/iemobile/i);
});

/* find all unsupported IE versions */
Modernizr.addTest('ieunsupported', function () {
    'use strict';
    return (navigator.userAgent.search(/MSIE ([2-9]|10)\./i) >= 0) ? true : false;
});

/* find IE 11 for displaying video unsupported */
Modernizr.addTest('ie11unsupported', function () {
    'use strict';
    return (navigator.userAgent.search(/Trident\/7\.0;(.*)rv:11\./i) >= 0) ? true : false;
});

Modernizr.addTest('ie', function () {
    'use strict';
    return (navigator.userAgent.search(/\s+(MSIE|rv:11|Edge\/\d+\.)/) >= 0);
});

Modernizr.addTest('edge', function () {
    'use strict';
    return (navigator.userAgent.search(/\s+(Edge\/\d+\.)/) >= 0);
});

/* global AdFuel */

var CNN = window.CNN || {};
CNN.Features = CNN.Features || {};
CNN.PageParams = {};

/**
 * page-events script
 *
 * It is important that this is the first script included on *every page*
 * immediately after jQuery in the HTML head.
 *
 * Adds the allZonesRenderComplete and allZonesAndDomReady events as well
 * as the associated onZonesRendered and onZonesAndDomReady jQuery extensions
 * for setting "no-miss" handlers on them.
 *
 * Also sets up the page, event, and ad timers.
 */

(function initEvents(document) {
    'use strict';

    var $doc = jQuery(document),
        getParams,
        legacyEvent = null,
        now,
        processTimers;

    jQuery.noConflict();

    /* Setup for page visibility check */
    CNN.pageVis = {
        hiddenEvent: '',
        hiddenKey: '',
        isDocumentHidden: function () {
            return (this.hiddenKey.length > 0 && document[this.hiddenKey] === true) ? true : false;
        },
        isDocumentVisible: function () {
            return (this.hiddenKey.length > 0 && document[this.hiddenKey] === false) ? true : false;
        }
    };

    /* Set-up to work with the Visibility API */
    if (typeof document.hidden !== 'undefined') {
        CNN.pageVis.hiddenKey = 'hidden';
        CNN.pageVis.hiddenEvent = 'visibilitychange';
    } else if (typeof document.mozHidden !== 'undefined') {
        CNN.pageVis.hiddenKey = 'mozHidden';
        CNN.pageVis.hiddenEvent = 'mozvisibilitychange';
    } else if (typeof document.msHidden !== 'undefined') {
        CNN.pageVis.hiddenKey = 'msHidden';
        CNN.pageVis.hiddenEvent = 'msvisibilitychange';
    } else if (typeof document.webkitHidden !== 'undefined') {
        CNN.pageVis.hiddenKey = 'webkitHidden';
        CNN.pageVis.hiddenEvent = 'webkitvisibilitychange';
    } else {
        /* The Visibility API is not available, use blur and focus events */
        if (document.createEvent) {
            if (!legacyEvent) {
                legacyEvent = document.createEvent('HTMLEvents');
                legacyEvent.initEvent('visibilitychange', true, true);
            }
        }
        CNN.pageVis.hiddenKey = 'hidden';
        CNN.pageVis.hiddenEvent = 'visibilitychange';
        document.hidden = false;
        $doc.on('focus', function _handleOnFocus() {
            if (document.hidden) {
                document.hidden = false;
                document.dispatchEvent(legacyEvent);
            }
        });
        $doc.on('blur', function _handleOnBlur() {
            if (!document.hidden) {
                document.hidden = true;
                document.dispatchEvent(legacyEvent);
            }
        });
    }

    /* Add jQuery onAnalyticsMetadataReady, onZonesAndDomReady, and onZonesRendered event jQuery extensions */
    CNN._analyticsMetadataEvent = false;
    CNN._corePlayerReady = false;
    CNN._onCarouselLoaded = false;
    CNN._videoContentStarted = false;
    CNN._zonesRenderedEvent = false;
    CNN._zonesAndDomReadyEvent = false;
    CNN._zoneRenderedEvent = false;
    CNN._articleLengthChange = false;
    jQuery.fn.extend({
        /* Event for analytics metadata ready */
        onAnalyticsMetadataReady: function (handler) {
            if (typeof handler === 'function') {
                if (CNN._analyticsMetadataEvent === true) {
                    handler(jQuery);
                } else {
                    jQuery(this).on('cnn-analytics-metadata-added', {handler: handler}, function _analyticsMetaDataReadyEventHandler(event) {
                        event = event || {};
                        if (typeof event.data === 'object' && event.data !== null && typeof event.data.handler === 'function') {
                            event.data.handler(jQuery);
                        }
                    });
                }
            }
        },
        /* Event for visibility change */
        onVisibilityChange: function (handler) {
            if (typeof handler === 'function') {
                jQuery(this).on(CNN.pageVis.hiddenEvent, {handler: handler}, function _visibilityChangeEventHandler(event) {
                    event = event || {};
                    if (typeof event.data === 'object' && event.data !== null && typeof event.data.handler === 'function') {
                        event.data.handler(jQuery);
                    }
                });
            }
        },
        /* Event for all zones rendered and DOM ready */
        onZonesAndDomReady: function (handler) {
            if (typeof handler === 'function') {
                if (CNN._zonesAndDomReadyEvent === true) {
                    handler(jQuery);
                } else {
                    jQuery(this).on('allZonesAndDomReady', {handler: handler}, function _allZonesAndDomReadyEventHandler(event) {
                        event = event || {};
                        if (typeof event.data === 'object' && event.data !== null && typeof event.data.handler === 'function') {
                            event.data.handler(jQuery);
                        }
                    });
                }
            }
        },
        /* Event for all zones rendered */
        onZonesRendered: function (handler) {
            if (typeof handler === 'function') {
                if (CNN._zonesRenderedEvent === true) {
                    handler(jQuery);
                } else {
                    jQuery(this).on('allZonesRenderComplete', {handler: handler}, function _allZonesRenderCompleteEventHandler(event) {
                        event = event || {};
                        if (typeof event.data === 'object' && event.data !== null && typeof event.data.handler === 'function') {
                            event.data.handler(jQuery);
                        }
                    });
                }
            }
        },
        /* Event for a single zone rendered */
        onZoneRendered: function (handler) {
            if (typeof handler === 'function') {
                if (CNN._zoneRenderedEvent === true) {
                    handler(jQuery);
                } else {
                    jQuery(this).on('zoneRenderComplete', {handler: handler}, function _zoneRenderCompleteEventHandler(event) {
                        event = event || {};
                        if (typeof event.data === 'object' && event.data !== null && typeof event.data.handler === 'function') {
                            event.data.handler(jQuery);
                        }
                    });
                }
            }
        },
        /* Add event handler for Article Length Change Event */
        onArticleLengthChange: function (handler) {
            if (typeof handler === 'function') {
                if (CNN._articleLengthChange === true) {
                    handler(jQuery);
                } else {
                    jQuery(this).on('articleLengthChange', {handler: handler}, function _articleLengthChangeEventHandler(event) {
                        event = event || {};
                        if (typeof event.data === 'object' && event.data !== null && typeof event.data.handler === 'function') {
                            event.data.handler(jQuery);
                        }
                    });
                }
            }
        },
        /* Trigger the onArticleLengthChange event */
        triggerArticleLengthChange: function () {
            CNN._articleLengthChange = true;
            $doc.trigger('articleLengthChange');
        },
        /* Add event handler for CVP ready event */
        onCorePlayerReady: function (handler) {
            if (typeof handler === 'function') {
                if (CNN._corePlayerReady === true) {
                    handler(jQuery);
                } else {
                    jQuery(this).on('corePlayerReady', {handler: handler}, function _corePlayerReadyEventHandler(event) {
                        event = event || {};
                        if (typeof event.data === 'object' && event.data !== null && typeof event.data.handler === 'function') {
                            event.data.handler(jQuery);
                        }
                    });
                }
            }
        },
        /* Trigger the onCorePlayerReady event once a core video player is ready */
        triggerCorePlayerReady: function () {
            CNN._corePlayerReady = true;
            $doc.trigger('corePlayerReady');
        },
        /* Add event handler for CVP ad start event */
        onVideoContentStarted: function (handler) {
            if (typeof handler === 'function') {
                if (CNN._onVideoContentStarted === true) {
                    handler(jQuery);
                } else {
                    jQuery(this).on('videoContentStarted', {handler: handler}, function _videoContentStartedEventHandler(event) {
                        event = event || {};
                        if (typeof event.data === 'object' && event.data !== null && typeof event.data.handler === 'function') {
                            event.data.handler(jQuery);
                        }
                    });
                }
            }
        },
        /* Trigger the onVideoContentStarted event once a the ad starts playing */
        triggerVideoContentStarted: function () {
            CNN._videoContentStarted = true;
            $doc.trigger('videoContentStarted');
        },
        /* Add event handler for CarouselLoaded event */
        onCarouselLoaded: function (handler) {
            if (typeof handler === 'function') {
                if (CNN._onCarouselLoaded === true) {
                    handler(jQuery);
                } else {
                    jQuery(this).on('carouselLoaded', {handler: handler}, function _carouselLoadedEventHandler(event) {
                        event = event || {};
                        if (typeof event.data === 'object' && event.data !== null && typeof event.data.handler === 'function') {
                            event.data.handler(jQuery);
                        }
                    });
                }
            }
        },
        /* Trigger the onCarouselLoaded event once a the ad starts playing */
        triggerCarouselLoaded: function () {
            CNN._onCarouselLoaded = true;
            $doc.trigger('carouselLoaded');
        },
        /* Trigger the cnn-analytics-metadata-added event */
        triggerAnalyticsMetadataReady: function () {
            CNN._analyticsMetadataEvent = true;
            jQuery(this).trigger('cnn-analytics-metadata-added');
        },
        /* Trigger the allZonesAndDomReady event once both domReady and allZonesRenderComplete are triggered */
        triggerZonesRendered: function () {
            var doc = jQuery(document);
            CNN._zonesRenderedEvent = true;
            doc.trigger('allZonesRenderComplete');
            doc.ready(function _onZonesRenderedAndDomReadyEventHandler() {
                CNN._zonesAndDomReadyEvent = true;
                jQuery(document).trigger('allZonesAndDomReady');
            });
        }
    });

    /* If lazyLoad is off, throw an allZonesRenderComplete event on domReady */
    if (CNN.LazyLoad !== true) {
        $doc.ready(function () {
            jQuery(document).triggerZonesRendered();
        });
    }

    /* Pull apart the param string and make it generally available for other scripts. */
    if (window.location.search) {
        getParams = function () {
            var ePos,
                i,
                params = window.location.search.substr(1).split('&');

            for (i = 0; i < params.length; i++) {
                ePos = params[i].indexOf('=');
                try {
                    if (ePos > 0) {
                        CNN.PageParams[params[i].substr(0, ePos)] = decodeURIComponent(params[i].substr(ePos + 1).replace(/\+/g, ' '));
                    } else if (ePos < 0) {
                        CNN.PageParams[params[i]] = '';
                    }
                } catch (e) {}
            }
        };
        getParams();
    }

    /* Add the super mega happy awesome power timer, now with more flavor! */
    if (CNN.Features.enableRefreshTimers !== false &&
        ((typeof CNN.pageTimer === 'object' && typeof CNN.pageTimer.interval === 'number' && CNN.pageTimer.interval > 0) ||
        (typeof CNN.adTimers === 'object' && CNN.adTimers !== null) ||
        (typeof CNN.eventTimers === 'object' && CNN.eventTimers !== null))) {

        /* Initialize the super mega happy awesome power timer settings */
        now = new Date().getTime();
        CNN.goActiveUntil = (typeof CNN.goActiveUntil === 'number') ? CNN.goActiveUntil : 0;

        /**
         * CNN.refreshTimer contains all the functions and variables needed for the refresh timers.
         */
        CNN.refreshTimer = {
            adList: [],
            forceReset: 0,
            hiddenResetDelay: 0,
            hiddenResetThreshold: 0,
            hiddenTime: 0,
            intervals: {},
            intervalsCount: 0,
            intervalsId: null,
            intervalsVis: {},
            intervalsVisCount: 0,
            intervalsVisId: null,
            startTime: now,
            visibleTime: 0,
            visibleTimeAccrued: 0
        };

        /* Pull in the page refresh timer, if there is one */
        if (CNN.pageTimer && CNN.pageTimer.interval) {
            CNN.pageTimer.isPage = true;
            if (CNN.pageTimer.isVisible === true) {
                CNN.refreshTimer.intervalsVis[CNN.pageTimer.interval] = [CNN.pageTimer];
                CNN.refreshTimer.intervalsVisCount++;
                if (typeof CNN.pageTimer.resetThreshold === 'number' && CNN.pageTimer.resetThreshold > 0) {
                    /* The hiddenResetThreshold and hiddenResetDelay are the same as resetThreshold and resetDelay, but converted to milliseconds */
                    CNN.refreshTimer.hiddenResetThreshold = CNN.pageTimer.resetThreshold * 60000;
                    CNN.refreshTimer.hiddenResetDelay = (typeof CNN.pageTimer.resetDelay === 'number' ? CNN.pageTimer.resetDelay : 0) * 60000;
                }
            } else {
                CNN.refreshTimer.intervals[CNN.pageTimer.interval] = [CNN.pageTimer];
                CNN.refreshTimer.intervalsCount++;
            }
        }

        /**
         * Process the timers into the object
         * @private
         * @param {object} timers - The adTimers or eventTimers object
         * @param {string} timerType - Either 'ad' or 'event'
         */
        processTimers = function (timers, timerType) {
            var self = CNN.refreshTimer,
                k;

            for (k in timers) {
                if (timers.hasOwnProperty(k) && timers[k].interval) {
                    if (timerType === 'ad') {
                        timers[k].className = '.ad-refresh-' + k;
                    } else if (typeof timers[k].eventName !== 'string' || timers[k].eventName.length <= 0) {
                        continue;
                    }
                    if (timers[k].isVisible === true) {
                        if (typeof self.intervalsVis[timers[k].interval] !== 'undefined' &&
                            jQuery.isArray(self.intervalsVis[timers[k].interval]) === true) {

                            self.intervalsVis[timers[k].interval].push(timers[k]);
                        } else {
                            self.intervalsVis[timers[k].interval] = [timers[k]];
                            self.intervalsVisCount++;
                        }
                    } else {
                        if (typeof self.intervals[timers[k].interval] !== 'undefined' &&
                            jQuery.isArray(self.intervals[timers[k].interval]) === true) {

                            self.intervals[timers[k].interval].push(timers[k]);
                        } else {
                            self.intervals[timers[k].interval] = [timers[k]];
                            self.intervalsCount++;
                        }
                    }
                }
            }
        };

        /* Add the event timers, if any */
        if (typeof CNN.eventTimers === 'object' && CNN.eventTimers !== null) {
            processTimers(CNN.eventTimers, 'event');
        }

        /* Add the ad refresh timers, if any */
        if (typeof CNN.adTimers === 'object' && CNN.adTimers !== null) {
            processTimers(CNN.adTimers, 'ad');
        }

        /**
         * Called for each timer at its interval
         * @private
         * @param {object} timer - A timer object being triggered
         */
        CNN.refreshTimer.triggerTimer = function (timer) {
            var a,
                adDivs,
                adId;

            if (typeof timer.eventName === 'string' && timer.eventName.length > 0) {
                /* If we are throwing an event, do it. */
                jQuery(document).trigger(timer.eventName);
            }
            if (timer.isPage === true) {
                /* Redirect to the same page (with the refresh=1 param) */
                if (window.location.href.search(/[\&\?]refresh\=/) >= 0) {
                    window.location.reload(true);
                } else {
                    a = window.location.href.indexOf('#');
                    if (window.location.href.indexOf('?') >= 0) {
                        if (a >= 0) {
                            window.location.replace(window.location.href.substr(0, a) + '&refresh=1' + window.location.href.substr(a));
                        } else {
                            window.location.replace(window.location.href + '&refresh=1');
                        }
                    } else {
                        if (a >= 0) {
                            window.location.replace(window.location.href.substr(0, a) + '?refresh=1' + window.location.href.substr(a));
                        } else {
                            window.location.replace(window.location.href + '?refresh=1');
                        }
                    }
                }
                return;  /* No point in processing ads now */
            }
            if (typeof AdFuel === 'object' && typeof AdFuel.refreshAds === 'function' &&
                typeof timer.className === 'string' && timer.className.length > 0) {

                /* Build list of Ads to refresh */
                adDivs = jQuery(timer.className);
                if (adDivs !== null && adDivs.length > 0) {
                    for (a = 0; a < adDivs.length; a++) {
                        adId = adDivs.eq(a).attr('id');
                        if (typeof adId === 'string' && adId.length > 0) {
                            CNN.refreshTimer.adList.push(adId);
                        }
                    }
                }
            }
        };

        /**
         * Called each minute to check "plain" interval timers
         * @private
         */
        CNN.refreshTimer.intervalAction = function () {
            var self = CNN.refreshTimer,  /* Don't want use "this" in timer functions */
                now = new Date().getTime(),
                i,
                /* Current minute is (now in ms - start in ms + 10000 ms to account for drift) / 60000 ms to get minutes and then rounded down */
                interval = Math.floor(((now - self.startTime) + 10000) / 60000),
                ival,
                j;

            if (interval > 0) {
                self.adList = [];
                for (i in self.intervals) {
                    if (self.intervals.hasOwnProperty(i) && (ival = parseInt(i, 10)) > 0 && (interval % ival) === 0 &&
                        typeof self.intervals[i] !== 'undefined' && jQuery.isArray(self.intervals[i])) {

                        for (j = 0; j < self.intervals[i].length; j++) {
                            self.triggerTimer(self.intervals[i][j]);
                        }
                    }
                }
                if (self.adList.length > 0) {
                    try {
                        AdFuel.refreshAds(self.adList);
                    } catch (e) {
                        console.log('Failed to refresh ads (' + self.adList.join(',') + '): ', e);
                    }
                }
            }
        };

        /**
         * Called each "visible" minute to check visible interval timers
         * @private
         */
        CNN.refreshTimer.intervalVisAction = function () {
            var self = CNN.refreshTimer,  /* Don't want use "this" in timer functions */
                now = new Date().getTime(),
                diff,
                i,
                interval,
                ival,
                j;

            if (self.visibleTime > 0) {
                diff = now - self.visibleTime;
                self.visibleTimeAccrued += diff;
                if (now >= CNN.goActiveUntil && self.forceReset > 0) {
                    self.forceReset -= diff;
                    if (self.forceReset <= 0) {
                        /* We've exceeded the reset threshold so reset the page */
                        self.triggerTimer(CNN.pageTimer);
                    }
                }
                self.visibleTime = now;
            }
            if (CNN.pageVis.isDocumentHidden()) {
                self.handleVisibilityChange();
            } else {
                /* Our current minute is (milliseconds accrued + 10000 ms to account for drift) / 60000 ms (aka 1 minute) then rounded down */
                interval = Math.floor((self.visibleTimeAccrued + 10000) / 60000);
                /* Restart the timer, adjusting for drift error by subtracting the accrued time (in ms) from the desired time (one more minute). */
                self.intervalsVisId = setTimeout(self.intervalVisAction, (((interval + 1) * 60000) - self.visibleTimeAccrued));

                if (interval > 0) {
                    self.adList = [];
                    for (i in self.intervalsVis) {
                        if (self.intervalsVis.hasOwnProperty(i) && (ival = parseInt(i, 10)) > 0 && (interval % ival) === 0 &&
                            typeof self.intervalsVis[i] !== 'undefined' && jQuery.isArray(self.intervalsVis[i])) {

                            for (j = 0; j < self.intervalsVis[i].length; j++) {
                                if (!self.intervalsVis[i][j].isPage || now >= CNN.goActiveUntil) {
                                    self.triggerTimer(self.intervalsVis[i][j]);
                                }
                            }
                        }
                    }
                    if (self.adList.length > 0) {
                        try {
                            AdFuel.refreshAds(self.adList);
                        } catch (e) {
                            console.log('Failed to refresh ads (' + self.adList.join(',') + '): ', e);
                        }
                    }
                }
            }
        };

        /**
         * Called whenever the visibility status of the page changes
         * @private
         */
        CNN.refreshTimer.handleVisibilityChange = function () {
            var self = CNN.refreshTimer,
                now = new Date().getTime(),
                hdiff,
                diff;

            if (CNN.pageVis.isDocumentHidden()) {
                /* The page is hidden */
                if (self.intervalsVisId !== null) {
                    /* Stop the running timer */
                    clearTimeout(self.intervalsVisId);
                    self.intervalsVisId = null;
                    self.hiddenTime = now;
                    if (self.visibleTime > 0) {
                        self.visibleTimeAccrued += (now - self.visibleTime);
                        self.visibleTime = 0;
                    }
                }
            } else {
                /* The page is visible */
                if (self.intervalsVisId === null) {
                    /* diff is the time in milliseconds until the next 1 minute interval */
                    diff = 60000 - (self.visibleTimeAccrued % 60000);
                    self.visibleTime = now;
                    /* Did we exceed page reset idle time and no live stream running? */
                    if (self.hiddenResetThreshold > 0 && self.hiddenTime > 0 && CNN.goActiveUntil <= now) {
                        hdiff = now - self.hiddenTime;
                        if (self.forceReset > 0 || (self.hiddenResetThreshold < hdiff &&
                            self.hiddenResetDelay < (((CNN.pageTimer.interval - 1) * 60000) - self.visibleTimeAccrued))) {

                            if (self.forceReset > 0) {
                                /* We have exceeded the reset threshold and we are more than
                                 * a minute more than the reset delay from a page reset, so
                                 * move things along.
                                 */
                                self.forceReset -= hdiff;
                            } else {
                                /* Start the reset clock... */
                                self.forceReset = self.hiddenResetDelay;
                            }
                            if (self.forceReset <= 0) {
                                /* We've hit the reset delay so reset the page immediately */
                                self.triggerTimer(CNN.pageTimer);
                            }
                        }
                    }
                    self.hiddenTime = 0;
                    /* Start the timer back up */
                    self.intervalsVisId = setTimeout(self.intervalVisAction, diff);
                }
            }
        };

        if (CNN.refreshTimer.intervalsCount > 0) {
            /* Start the super mega happy awesome power timer for normal (1 minute) intervals */
            CNN.refreshTimer.intervalsId = setInterval(CNN.refreshTimer.intervalAction, 60000);
        }
        if (CNN.refreshTimer.intervalsVisCount > 0) {
            /* Start the super mega happy awesome power timer for visible page intervals */
            $doc.onVisibilityChange(CNN.refreshTimer.handleVisibilityChange);

            if (CNN.pageVis.isDocumentHidden()) {
                CNN.refreshTimer.hiddenTime = now;
            } else {
                /* If we are currently visible, set the timer for 1 minute */
                CNN.refreshTimer.visibleTime = now;
                CNN.refreshTimer.intervalsVisId = setTimeout(CNN.refreshTimer.intervalVisAction, 60000);
            }
        }
    }
}(document));


var CNN = window.CNN || {};

/**
 * Some common utility functions usable site-wide
 *
 * Javascript Module Pattern
 * @see http://www.adequatelygood.com/JavaScript-Module-Pattern-In-Depth.html
 *
 * @name CNN.Utils
 * @namespace
 * @memberOf CNN
 */
CNN.Utils = CNN.Utils || (function ($, window, document, undefined) {
    'use strict';

    var CONST_HOUR = (1000 * 60) * 60, /* 1 hour in milliseconds: 60 x 60 x 1000 ms */
        CONST_PAST = 'Thu, 01 Jan 1970 00:00:00 UTC';

    CNN.hasLocalStorage = (function _checkLocalStorage() {
        var x = '__test_it';
        try {
            window.localStorage.setItem(x, x);
            window.localStorage.removeItem(x);
            window.sessionStorage.setItem(x, x);
            window.sessionStorage.removeItem(x);
            return true;
        } catch (e) {
            return false;
        }
    })();

    /**
     * Returns a GMT string of the ttl given
     *
     * @private
     * @param  {number} ttl cookie expirty in hours
     * @return {string} a GMT time string
     */
    function _calculateCookieExpiry(ttl) {
        var expires = null;

        if ((typeof ttl === 'string') && Date.parse(ttl)) { /* already a Date string */
            expires = ttl;
        } else if (typeof ttl === 'number') { /* calculate Date from number of ttl */
            expires = (new Date(Date.now() + (ttl * CONST_HOUR))).toGMTString();
        }

        return expires;
    }

    /**
     * Checks for the existance of an object.
     *
     * @public
     * @param  {...*} _obj object or list of objects to test
     * @return {boolean} true if obj is not null or undefined
     */
    function exists(_obj) {
        var result = true,
            i,
            existsTest = function (obj) {
                return obj !== undefined && obj !== null;
            };

        /* check the individual arguments */
        for (i = 0; i < arguments.length; i++) {
            if (!existsTest(arguments[i])) {
                result = false;
                break;
            }
        }

        return result;
    }

    /**
     * Checks for the existances of a non-null object.
     *
     * @public
     * @param {...*} _obj object or list of objects to test
     * @return {boolean} true if obj is an object and not null
     */
    function existsObject(_obj) {
        var result = true,
            i,
            existsTest = function (obj) {
                return typeof obj === 'object' && obj !== null;
            };

        /* check the individual arguments */
        for (i = 0; i < arguments.length; i++) {
            if (!existsTest(arguments[i])) {
                result = false;
                break;
            }
        }

        return result;
    }

    /**
     * Checks for the existence of a non-empty string.
     *
     * @public
     * @param {...*} _obj object or list of objects to test
     * @returns {boolean} true if obj is an object and not null
     */
    function existsString(_obj) {
        var result = true,
            i,
            existsTest = function (obj) {
                return typeof obj === 'string' && obj.length > 0;
            };

        /* check the individual arguments */
        for (i = 0; i < arguments.length; i++) {
            if (!existsTest(arguments[i])) {
                result = false;
                break;
            }
        }

        return result;
    }

    /**
     * Return a hash of all the cookies on this domain
     *
     * @public
     * @return {object} a hash of all cookies and their values
     */
    function getCookies() {
        var hash = {},
            cookies,
            namevaluePairs,
            i = 0;

        if (document.cookie) {
            cookies = document.cookie.split('; ');
            for (; i < cookies.length; i++) {
                namevaluePairs = cookies[i].split('=');
                hash[namevaluePairs[0]] = decodeURIComponent(namevaluePairs[1]) || null;
            }
        }

        return hash;
    }

    /**
     * Check whether the cookie name exists
     *
     * @public
     * @param  {string} name the cookie name
     * @return {boolean} true if the cookie exists
     */
    function cookieExists(name) {
        return getCookie(name) !== null;
    }

    /**
     * Returns the value of the given cookie name
     *
     * @public
     * @param  {string} name the cookie name
     * @return {string} value of the cookie
     */
    function getCookie(name) {
        var re = new RegExp('(^|;)\\s*' + name + '\\s*=\\s*([^;]+)'),
            matches = document.cookie.match(re);

        return matches ? matches.pop() : null;
    }

    /**
     * Sets a cookie with the given params
     *
     * @public
     * @param {string} name name of your cookie
     * @param {string} value value of your cookie
     * @param {string|number} expiry date string expiry of cookie or int hours
     * @param {string} path the path of your cookie
     * @param {string} domain the domain of your cookie
     * @param {boolean} secure (optional) true if this is a cookie on a secure domain
     */
    function setCookie(name, value, expiry, path, domain, secure) {
        var cookie = '';

        if (exists(name) && exists(value)) {
            expiry = _calculateCookieExpiry(expiry);
            path = exists(path) ? path : '/';
            domain = exists(domain) ? domain : ''; /* set to current domain */
            secure = exists(secure) ? secure : false;

            cookie += name + '=' + encodeURIComponent(value);

            /* If no expiry, it will be a session cookie */
            if (exists(expiry)) {
                cookie += ';expires=' + expiry;
            }

            /* Add additional parameters specified. */
            if (path) {
                cookie += ';path=' + path;
            }

            if (domain) {
                cookie += ';domain=' + domain;
            }

            if (secure) {
                cookie += '; secure';
            }

            /* Set the cookie */
            document.cookie = cookie;
        }
    }

    /**
     * Sets a cookie that will live across all cnn.com sub-domains
     *
     * @public
     * @param {string} name   name of your cookie
     * @param {string} value  value of your cookie
     * @param {string|number} expiry date string expiry of cookie or int hours
     * @param {boolean} secure (optional) true if this is a cookie on a secure domain
     */
    function setCNNCookie(name, value, expiry, secure) {
        setCookie(name, value, expiry, '/', '.cnn.com', secure);
    }

    /**
     * Deletes the cookie from the given path and domain
     *
     * @public
     * @param {string} name   name of your cookie
     * @param {string} path   the path of your cookie
     * @param {string} domain the domain of your cookie
     */
    function deleteCookie(name, path, domain) {
        var cookie = '';

        if (exists(name)) {

            /* set defaults */
            path = exists(path) ? path : '/';
            domain = exists(domain) ? domain : ''; // set to current domain

            cookie += name + '=;expires=' + CONST_PAST;

            /* Add additional parameters specified. */
            if (path) {
                cookie += ';path=' + path;
            }

            if (domain) {
                cookie += ';domain=' + domain;
            }

            /* Set the cookie */
            document.cookie = cookie;
        }
    }

    /**
     * Delete the cookie from all cnn.com sub-domains
     *
     * @public
     * @param {string} name the name of your cookie
     */
    function deleteCNNCookie(name) {
        deleteCookie(name, '/', '.cnn.com');
    }

    /**
     * Store a client value for a period of time.  Will use Web Local Storage
     * if possible, otherwise it will use a cookie.
     *
     * @public
     * @param {string} name - name of the cookie/key
     * @param {string} value - value to store
     * @param {string|number} expiry - date string expiry of cookie or int hours
     */
    function storeLocalValue(name, value, expiry) {
        var expires;

        if (typeof name === 'string' && name.length > 0 && typeof value === 'string') {
            if (CNN.hasLocalStorage === false) {
                /* We don't have local storage, so use a cookie */
                setCookie(name, value, (expiry === 'session' ? null : expiry), '/', '.cnn.com', false);
            } else {
                try {
                    /* We have local storage, so set the value */
                    if (expiry === 'session') {
                        /* Use session storage */
                        window.sessionStorage.setItem(name, value);
                    } else {
                        /* Use local storage */
                        window.localStorage.setItem(name, value);
                        /* Set the expiration, if present */
                        if (typeof expiry === 'number') {
                            expires = new Date(Date.now() + (expiry * CONST_HOUR));
                            window.localStorage.setItem(name + '__expires', expires.getTime().toString(10));
                        } else if (typeof expiry === 'string' && expiry !== 'session') {
                            expires = new Date(expiry);
                            if (expires.getTime() > Date.now()) {
                                window.localStorage.setItem(name + '__expires', expires.getTime().toString(10));
                            }
                        }
                    }
                } catch (e) {}
            }
        }
    }

    /**
     * Do we have a client value
     *
     * @public
     * @param {string} name - name of the cookie/key to check
     * @return {boolean} - true if it exists, false if not
     */
    function hasLocalValue(name) {
        if (typeof name === 'string' && name.length > 0) {
            if (CNN.hasLocalStorage === false) {
                return cookieExists(name);
            } else {
                try {
                    if (exists(window.sessionStorage.getItem(name)) ||
                        exists(window.localStorage.getItem(name + '__expires'))) {

                        return true;
                    }
                } catch (e) {}
            }
        }
        return false;
    }

    /**
     * Remove a client value
     *
     * @public
     * @param {string} name - name of the cookie/key to delete
     */
    function deleteLocalValue(name) {
        if (typeof name === 'string' && name.length > 0) {
            if (CNN.hasLocalStorage === false) {
                deleteCookie(name);
            } else {
                try {
                    window.localStorage.removeItem(name);
                    window.localStorage.removeItem(name + '__expires');
                } catch (e) {}
                try {
                    window.sessionStorage.removeItem(name);
                } catch (e) {}
            }
        }
    }

    /**
     * Get a client value
     *
     * @public
     * @param {string} name - name of the cookie/key to get
     * @return {mixed} - the value string, if set, or null of not set
     */
    function getLocalValue(name) {
        var expires,
            value = null;

        if (typeof name === 'string' && name.length > 0) {
            try {
                if (CNN.hasLocalStorage === false) {
                    value = getCookie(name);
                } else if ((expires = window.localStorage.getItem(name + '__expires')) !== null) {
                    /* We have an expiration value, so check it */
                    if (parseInt(expires, 10) > Date.now()) {
                        /* Still good */
                        value = window.localStorage.getItem(name);
                    } else {
                        /* Expired, so delete it */
                        deleteLocalValue(name);
                    }
                } else {
                    /* No expiration value, so return what we have */
                    value = window.sessionStorage.getItem(name);
                    if (!exists(value)) {
                        value = window.localStorage.getItem(name);
                    }
                }
            } catch (e) {}
        }
        return value;
    }

    /*
     * base64 encoding function limited to ascii characters,
     * not utf-8 characters. This function utilizes the
     * browser's atob and btoa native functions as a priority
     * and fallsback to a function that will be IE compatiable.
     *
     * @public
     * @param {string} string to be encoded
     * @return {string} encoded version of the string
     * @see http://en.wikibooks.org/wiki/Algorithm_Implementation/Miscellaneous/Base64
     */
    function base64Encode(origString) {
        var encodedString,
            base64chars,
            s = origString,
            r,
            p,
            c,
            n;

        if (typeof window.btoa === 'function') {
            try {
                encodedString = window.btoa(s);
            } catch (err) {
                encodedString = window.btoa(origString.replace(/[^\w\s\!\@\#\$\%\^\&\*\(\)\-\=\+\.\,\\\;\:\'\"\?]/, '-'));
            }
        } else {
            base64chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/'.split('');

            /* the result/encoded string, the padding string, and the pad count */
            r = '';
            p = '';
            c = s.length % 3;

            /* add a right zero pad to make this string a multiple of 3 characters */
            if (c > 0) {
                for (; c < 3; c++) {
                    p += '=';
                    s += '\0';
                }
            }

            /* increment over the length of the string, three characters at a time */
            for (c = 0; c < s.length; c += 3) {

                /* we add newlines after every 76 output characters, according to the MIME specs */
                if (c > 0 && (c / 3 * 4) % 76 === 0) {
                    r += '\r\n';
                }

                /* these three 8-bit (ASCII) characters become one 24-bit number */
                n = (s.charCodeAt(c) << 16) + (s.charCodeAt(c + 1) << 8) + s.charCodeAt(c + 2);

                /* this 24-bit number gets separated into four 6-bit numbers */
                n = [(n >>> 18) & 63, (n >>> 12) & 63, (n >>> 6) & 63, n & 63];

                /* those four 6-bit numbers are used as indices into the base64 character list */
                r += base64chars[n[0]] + base64chars[n[1]] + base64chars[n[2]] + base64chars[n[3]];
            }
            /* add the actual padding string, after removing the zero pad */
            encodedString = r.substring(0, r.length - p.length) + p;
        }

        return encodedString;
    }

    /*
     * base64 decoding function limited to ascii characters,
     * not utf-8 characters. This function utilizes the
     * browser's atob and btoa native functions as a priority
     * and fallsback to a function that will be IE compatiable.
     *
     * @public
     * @param {string} string to be decoded
     * @return {string} decoded version of the encoded string
     * @see http://en.wikibooks.org/wiki/Algorithm_Implementation/Miscellaneous/Base64
     */

    function base64Decode(encodedString) {
        var decodedString,
            base64chars,
            base64inv,
            s = encodedString,
            r,
            p,
            c,
            n,
            i;

        if (typeof window.atob === 'function') {
            try {
                decodedString = window.atob(s);
            } catch (err) {
                console.log(err.message, '\nString to decode: ' + s);
            }
        } else {
            base64chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/'.split('');
            base64inv = {};

            for (i = 0; i < base64chars.length; i++) {
                base64inv[base64chars[i]] = i;
            }

            /*
             * remove/ignore any characters not in the base64 characters list
             * or the pad character -- particularly newlines
             */
            s = s.replace(new RegExp('[^' + base64chars.join('') + '=]', 'g'), '');

            /* replace any incoming padding with a zero pad (the 'A' character is zero) */
            p = (s.charAt(s.length - 1) === '=' ? (s.charAt(s.length - 2) === '=' ? 'AA' : 'A') : '');
            r = '';
            s = s.substr(0, s.length - p.length) + p;

            /* increment over the length of this encoded string, four characters at a time */
            for (c = 0; c < s.length; c += 4) {

                /*
                 * each of these four characters represents a 6-bit index in the base64 characters list
                 * which, when concatenated, will give the 24-bit number for the original 3 characters
                 */
                n = (base64inv[s.charAt(c)] << 18) + (base64inv[s.charAt(c + 1)] << 12) +
                (base64inv[s.charAt(c + 2)] << 6) + base64inv[s.charAt(c + 3)];

                /* split the 24-bit number into the original three 8-bit (ASCII) characters */
                r += String.fromCharCode((n >>> 16) & 255, (n >>> 8) & 255, n & 255);
            }
            /* remove any zero pad that was added to make this a multiple of 24 bits */
            decodedString = r.substring(0, r.length - p.length);
        }

        return decodedString;
    }

    /**
     * strip the hash from a given id
     * @param  {String} id
     * @return {String}
     */

    function stripHash(id) {
        return id.replace('#', '');
    }

    /**
     * has Modernizr detected a phone?
     * @return {Boolean}
     */

    function isPhone() {
        return window.Modernizr.phone;
    }

    return {
        exists: exists,
        existsObject: existsObject,
        existsString: existsString,
        getCookies: getCookies,
        getCookie: getCookie,
        cookieExists: cookieExists,
        setCookie: setCookie,
        deleteCookie: deleteCookie,
        setCNNCookie: setCNNCookie,
        deleteCNNCookie: deleteCNNCookie,
        storeLocalValue: storeLocalValue,
        hasLocalValue: hasLocalValue,
        deleteLocalValue: deleteLocalValue,
        getLocalValue: getLocalValue,
        b64Encode: base64Encode,
        b64Decode: base64Decode,
        stripHash: stripHash,
        isPhone: isPhone
    };
})(jQuery, window, document);

/* global CNN, Bloodhound, dust */

CNN.MarketsConfig = CNN.MarketsConfig || {};
CNN.Money = CNN.Money || {};

CNN.Money.markets = function () {
    'use strict';

    var $ = jQuery,
        lookupPath = CNN.MarketsConfig.lookupService,
        $userQuoteForm;

    /**
     * Takes a raw HTML API response and strips out/replaces
     * unrequired and incorrect values to produce a valid JSON object.
     *
     * @param {string} response - Raw HTML response from API
     * @return {object} Parsed JSON data
     * @private
     */
    function _parseResponse(response) {
        return $.parseJSON(($('#jsCode', response).val() || '{}').replace('WSOD = ', '').replace('data', '"data"'));
    }

    /**
     * Setup the typeahead functionality for quote lookup.
     *
     * @private
     */
    function _initTypeahead() {
        var $quoteInput = $userQuoteForm.find('.js-quote-input'),
            lookupEngine = new Bloodhound({
                limit: 5,
                queryTokenizer: Bloodhound.tokenizers.whitespace,
                datumTokenizer: function (datum) {
                    return Bloodhound.tokenizers.whitespace(datum.name);
                },
                remote: {
                    url: lookupPath + '/cnn.com/%QUERY',
                    filter: function (response) {
                        var parseResponse = _parseResponse(response),
                            datums = [];

                        if (typeof parseResponse.data !== 'undefined') {
                            $.each(parseResponse.data, function (index, quotes) {
                                $.each(quotes, function (index, quote) {
                                    datums.push({
                                        name: quote.n,
                                        symbol: quote.s
                                    });
                                });
                            });
                        }

                        return datums;
                    },
                    ajax: {
                        dataType: 'html'
                    }
                }
            });

        lookupEngine.initialize();

        $quoteInput.typeahead({
            hint: false
        }, {
            name: 'user-quote',
            displayKey: 'symbol',
            source: lookupEngine.ttAdapter(),
            templates: {
                suggestion: function (context) {
                    var output;

                    dust.render('views/cards/tool/markets-typeahead-suggestion', context, function (err, out) {
                        output = out;
                    });

                    return output;
                }
            }
        });

        /* Fix z-index issue with subsequent columns appear above dropdown element */
        $userQuoteForm.closest('.column').addClass('column--top');
    }

    /**
     * Opens up the select user quote on the
     * CNN Money website within a new page.
     *
     * @return {function} The event handler
     * @private
     */
    function _showQuoteHandler() {
        var $quoteInput = $userQuoteForm.find('.js-quote-input');

        return function () {
            var quoteSymbol = $quoteInput.val();

            if (quoteSymbol) {
                /* Open new page with select quote on money site */
                window.open('http://money.cnn.com/quote/quote.html?symb=' + quoteSymbol, '_blank').focus();
            }

            return false;
        };
    }

    /**
     * Setup card functionality and plugins.
     *
     * @private
     * @param {object} $el - jQuery card DOM element object
     */
    function _setupCard($el) {
        $userQuoteForm = $el.find('.js-market-quote');

        CNN.Financial.restoreState($el);

        _bindEvents();
        if (typeof Bloodhound !== 'undefined') {
            _initTypeahead();
        }
    }

    /**
     * Setup required event handlers.
     *
     * @private
     */
    function _bindEvents() {
        var processFormHandle = _showQuoteHandler();

        $userQuoteForm.on('submit', processFormHandle)
            .on('click', '.js-quote-button', processFormHandle);
    }

    /* If this card is on the page initialize this module. */
    if (typeof CNN.Financial.checkLoaded === 'function') {
        $.when(CNN.Financial.checkLoaded('markets')).done(_setupCard);
        $.when(CNN.Financial.checkLoaded('marketsBasic')).done(_setupCard);
    }

};


if (typeof CNN.contentModel === 'object' && !CNN.contentModel.lazyLoad) {
    jQuery(document).onZonesAndDomReady(function () {
        'use strict';
        /*
        * HTML that this function is used on is pulled in by ajax at ssiPath
        * /.element/ssi/auto/4.0/sect/MAIN/markets_wsod_expansion.html and
        * /.element/ssi/auto/4.0/sect/MAIN/hp_markets_wsod_expansion.html
        */
        CNN.Money.markets();
    });
}

if (CNN.Utils.existsObject(CNN.Money)) {
    if (typeof CNN.Money.markets === 'function' && (CNN.Money.cardLoaded.markets === true) && (CNN.DemandLoading.thirdParty.money.markets !== true)) {
        CNN.Money.markets();
        CNN.DemandLoading.thirdParty.money.markets = true;
    }
}

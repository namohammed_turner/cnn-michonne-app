
(function ($) {
    'use strict';

    /*
     * to use: jQuery(selector).throttleEvent(eventname, callback function, delay in milliseconds, [context])
     */
    $.fn.throttleEvent = function throttleEvent(eventName, callback, delayMilliSecs, context) {
        var self = this,
            firstTime = true;

        if (typeof callback === 'function') {
            self.each(function () {
                var delayTimeout = null;

                $(this).on(eventName, context, function throttleEventHandler(e) {
                    /* by binding callback to this, we are executing the callback function with the same scope as our eventlistener */
                    var cb = callback.bind(this);

                    if (firstTime) {
                        cb(e);
                        firstTime = false;
                    }
                    window.clearTimeout(delayTimeout);
                    delayTimeout = window.setTimeout(cb, delayMilliSecs, e);
                });
            });
        }
    };
}(jQuery));


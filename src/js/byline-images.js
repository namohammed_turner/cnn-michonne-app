/* global CNN, fastdom, dust */

/**
 * @module BylineImages
 *
 * @description
 * Adds photos to bylines on articles whose viewport is larger then
 * small (mobile).
 *
 * @public
 *
 * @param {object} ns
 * The name space where the module lives.
 *
 * @param {object} runtimeConfig
 * The runtime configuration for this feature.
 *
 * @param {object} $j
 * The jQuery library.
 *
 * @param {object} fastdom
 * The fastdom library used for DOM manipulation.
 *
 * @param {object} dust
 * The template render engine for creating the HTML from the model and
 * template.
 *
 * @param {object} win
 * The global window object of the browser.
 *
 * @param {object} doc
 * The global document object of the browser.
 */
(function createBylineImagesFeature(ns, runtimeConfig, $j, fastdom, dust, win, doc) {
    'use strict';

    ns.BylineImages = (function createTheClass() {
        var isWaiting,
            threshold,
            template,
            model,
            getHTML;

        /**
         * A flag that tracks if the viewport width has passed the
         * threshold.
         *
         * @member {boolean} isWaiting
         *
         * @private
         */
        isWaiting = true;

        /**
         * The width in pixels that the viewport must be greater then
         * before the byline photos show up.
         *
         * @member {number} threshold
         *
         * @private
         */
        threshold = runtimeConfig.threshold;

        /**
         * The dust client template that is used to render the model.
         * Client side templates are added into the build via the
         * Gruntfile.
         *
         * @member {string} template
         *
         * @private
         */
        template = 'views/pages/common/byline-images';

        /**
         * The template model used for byline images.
         *
         * @member {object[]} model
         *
         * @private
         */
        model = [];

        /**
         * Promise that resolves to the rendered HTML from the dust
         * output. If an error occurs while rendering then the promise
         * rejects.
         *
         * @member {object} getHTML
         *
         * @private
         */
        getHTML = $j.Deferred();

        /**
         * Returns the jquery object that searches the page for all
         * elements with the class of js-byline-images.
         *
         * @method getPlaceholder
         *
         * @private
         *
         * @returns {object}
         * A promise that resolves with the jquery object that holds all
         * the elements with a class of js-byline-images.
         */
        function getPlaceholder() {
            var promiseMe = $j.Deferred();

            fastdom.measure(function theDOM() {
                promiseMe.resolve($j('.js-byline-images'));
            });

            return promiseMe.promise();
        }

        /**
         * Prepends the rendered HTML to all the placeholders found on
         * the page.
         *
         * @method placeOnThePagePlease
         *
         * @private
         */
        function placeOnThePagePlease() {
            $j.when(getHTML, getPlaceholder()).done(function letsShowTheUser(html, $byline) {
                fastdom.mutate(function toThePage() {
                    $byline.before(html);
                });
            });
        }

        /**
         * The dust render callback. Passes the value along to getHTML.
         *
         * @method doneRendering
         *
         * @private
         *
         * @param {null|string} err
         * If an error occurs while rendering then the error string is
         * set here.
         *
         * @param {string} out
         * The result of the dust rendering. This holds the HTML that
         * can be placed on the page.
         */
        function doneRendering(err, out) {
            if (err) {
                getHTML.reject(err);
            } else {
                getHTML.resolve(out);
            }
        }

        /**
         * Uses dust to render the the template and model. Rendering is
         * only done if getHTML promise has not yet resolved. Uses
         * fastdom to give the browser a chance to do something before
         * rendering begins.
         *
         * @method render
         *
         * @private
         */
        function render() {
            if (getHTML.state() === 'pending') {
                fastdom.mutate(function letDustDoItsThing() {
                    dust.render(template, model, doneRendering);
                });
            }
        }

        /**
         * Sets up all the little things needed before the actual
         * rendering starts.
         *
         * @method getReadyToRender
         *
         * @private
         */
        function getReadyToRender() {
            /* Don't need these anymore! */
            $j(win).off('resize.bylineimages');
            $j(doc).off('bylineimages.start');

            /* We have something to render so lets get notified once the page is ready. */
            $j(doc).onZonesAndDomReady(placeOnThePagePlease);

            /* now render!! */
            render();
        }

        /**
         * Resize listener to detect when the viewport's width is
         * greater then the threshold. This will trigger the custom
         * event bylineimages.start once the threshold has been reached.
         *
         * @method canIStartNow
         *
         * @private
         */
        function canIStartNow() {
            if (isWaiting && ns.CurrentSize.getClientWidth() > threshold) {
                isWaiting = false;
                $j(doc).trigger('bylineimages.start');
            }
        }

        /**
         * Kicks off the class. Checks for a template model and sets up
         * listener events. Finally checks to see if the viewport is
         * initially greater then the threshold.
         *
         * @method readySetGo
         *
         * @public
         */
        function readySetGo() {
            var hasModel = typeof ns.contentModel === 'object' &&
                           typeof ns.contentModel.bylineImages === 'object' &&
                           ns.contentModel.bylineImages.length > 0;

            if (hasModel && typeof threshold === 'number') {
                /* Set the model */
                model = {
                    bylineImages: ns.contentModel.bylineImages
                };

                /* Set up the events. */
                $j(doc).on('bylineimages.start', getReadyToRender);
                $j(win).throttleEvent('resize.bylineimages', canIStartNow, 263);

                /* Let's try and do something */
                canIStartNow();
            }
        }

        return {
            readySetGo: readySetGo
        };
    }());

    ns.BylineImages.readySetGo();
}(CNN, CNN.BylineImages || {}, jQuery, fastdom, dust, window, document));

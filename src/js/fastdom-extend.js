/* global fastdom */

var fastdomExtend;

if (typeof fastdom === 'object' && typeof fastdom.read !== 'function') {
    /**
     * Extend fastdom with legacy methods
     *
     * @name legacyFastdom
     * @namespace
     * @memberOf Fastdom
     */
    fastdomExtend = (function ($, win, fd) {
        'use strict';

        /**
         * Fastdom defer function using counter.
         *
         * @param {number} frames - Number of frames to defer
         * @param {function} callback - Callback to defer calling
         */
        function defer(frames, callback) {
            if (frames <= 0) {
                callback();
            } else {
                frames--;
                requestAnimationFrame($.proxy(defer, win, frames, callback));
            }
        }

        /**
         * Fastdom read function
         *
         * @param {function} doRead - Function to read from the DOM
         * @param {object} [context] - Context object, if any
         * @returns {object} - task object
         */
        function read(doRead, context) {
            return fd.measure(doRead, context);
        }

        /**
         * Fastdom write function
         *
         * @param {function} doWrite - Function to write to the DOM
         * @param {object} [context] - Context object, if any
         * @returns {object} - task object
         */
        function write(doWrite, context) {
            return fd.mutate(doWrite, context);
        }

        return {
            defer: defer,
            read: read,
            sync: function () {},
            write: write
        };
    })(jQuery, window, fastdom);

    window.fastdom = fastdom.extend(fastdomExtend);
}

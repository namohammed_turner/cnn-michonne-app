/* global CNN, fastdom */

CNN.Features = CNN.Features || {};
CNN.Feeds = CNN.Feeds || {};

/**
 * Requests the 'Breaking News' banner and displays it to the user.
 * A user can click to close the banner.
 * We continuously poll the server for new breaking news
 */
jQuery(document).onZonesAndDomReady(function () {
    'use strict';

    var banner = jQuery('.breaking-news'),
        closeBtn = jQuery('.breaking-news__close-btn'),
        defaultTtl = (CNN.Feeds.breakingNews && CNN.Feeds.breakingNews.ttl) || (1000 * 60) * 3,
        bannerBg = '#cc0000',
        pollId;

    /**
     * Shows or hides breaking news banner
     * @param {object} data - response.data ( ie { id: 123, content: ''...})
     */
    function renderBreakingNews(data) {
        var hasContent = (typeof data === 'object' && data !== null && typeof data.content === 'string' && data.content !== ''),
            sameNews = data && (data.id === banner.data('breaking-news-id')),
            $bnArticleLink,
            $articleLink,
            searchPos,
            tmpLink,
            tmpLinkSearch,
            bannerObj,
            textColor = '#fefefe',
            bannerBgHover = '#dd0000',
            bannerBgSelected = '#990000';

        /* only show breaking news if it differs */
        if (hasContent && !sameNews) {
            /* In a case where user closed breaking news, on dom reloads check if data.id
             * is equal to any cookie saved data.id
             */
            if (checkCloseBreakingNews() === data.id.toString()) {
                hideBreakingNews();
            } else {
                fastdom.mutate(function _showBreakingNews() {
                    jQuery('.breaking-news__title-text').html((typeof data.title === 'string' && data.title.length > 0) ? data.title : 'Breaking News');
                    jQuery('.breaking-news__msg').html(data.content);
                    $bnArticleLink = jQuery('.breaking-news__msg a');
                    /* remove listener */
                    bannerObj = jQuery('#breaking-news');
                    removeBannerListeners(bannerObj);
                    if ($bnArticleLink.length > 0) {
                        jQuery.each($bnArticleLink, function () {
                            $articleLink = jQuery(this);
                            tmpLink = $articleLink.attr('href');
                            searchPos = tmpLink.indexOf('?');
                            tmpLinkSearch = (searchPos < 0) ? '' : tmpLink.substring(searchPos);
                            if (tmpLinkSearch.length <= 1) {
                                $articleLink.attr('href', $articleLink.attr('href') + '?adkey=bn');
                            } else if (tmpLinkSearch.search(/[\&\?]adkey=/) >= 0)  {
                                /* Edge Case: If for some weird reason there is already an adkey, just change the value to 'bn' */
                                $articleLink.attr('href', $articleLink.attr('href') + tmpLinkSearch.replace(/([\&\?]adkey=)[^\&]*/, '$1bn'));
                            } else {
                                $articleLink.attr('href', $articleLink.attr('href') + '&adkey=bn');
                            }
                        });
                        /* Check for single link in breaking news banner */
                        if ($bnArticleLink.length === 1) {
                            bannerObj.css('cursor', 'pointer');
                            bannerObj.find('a').css('color', textColor);
                            bannerObj.hover(function () {
                                jQuery(this).css('background-color', bannerBgHover);
                            }, function () {
                                jQuery(this).css('background-color', bannerBg);
                            });
                            bannerObj.on('mousedown', function (event) {
                                event.preventDefault();
                                jQuery(this).css('background-color', bannerBgSelected);
                            });
                            bannerObj.on('click', function (event) {
                                var target = event.target;
                                event.preventDefault();
                                if (target && !jQuery(target).hasClass('breaking-news__close-btn')) {
                                    window.open($bnArticleLink.attr('href'), '_self');
                                }
                            });
                        }
                    }
                    bannerObj.on('click', breakingNewsClickEvent);
                    /* we've got banner content so add class to show banner */
                    jQuery('body').addClass('breaking-news--showing');
                    banner.data('breaking-news-id', data.id);
                    /* do some magic to allow for fixed navigation with variable height */
                    CNN.navigation.headerTracking.start();
                });
            }
        } else if (!hasContent) {
            hideBreakingNews();
        } /* nothing to change for sameNews */
    }

    function hideBreakingNews() {
        fastdom.measure(function () {
            if (jQuery('body').hasClass('breaking-news--showing')) {
                fastdom.mutate(function () {
                    /* remove class and hide banner */
                    jQuery('body').removeClass('breaking-news--showing');
                    window.CNN.navigation.headerTracking.adjustNav();
                    /* stop header height tracking */
                    CNN.navigation.headerTracking.stop();
                });
            }
        });
    }

    function closeBreakingNews() {
        /* save the breaking news id, as a session cookie, if the user closes breaking news*/
        CNN.Utils.setCookie('closeBreakingNews', banner.data('breaking-news-id'));
        hideBreakingNews();
    }

    function pollBreakingNews() {
        clearTimeout(pollId);
        pollId = setTimeout(loadBreakingNews, defaultTtl);
    }

    function removeBannerListeners(banObj) {
        banObj.css({'background-color': bannerBg, cursor: 'default'});
        banObj.off('click mousedown mouseenter mouseleave');
    }

    /**
     * Check for closeBreakingNews cookie and return the breaking news id
     *
     * @returns {string} breaking news id or empty string
     */
    function checkCloseBreakingNews() {
        return CNN.Utils.getCookie('closeBreakingNews');
    }

    /**
     * Requests, renders, and creates an endless poll for breaking news.
     */
    function loadBreakingNews() {
        if (CNN.Features.enableBreakingNews === true && CNN.Feeds.breakingNews && CNN.Feeds.breakingNews.url) {
            jQuery.ajax({
                url: CNN.Feeds.breakingNews.url,
                dataType: 'jsonp',
                cache: true,
                jsonpCallback: 'CNNBreakingNewsCallback',
                error: function (jqXHR, textStatus, errorThrown) {
                    if (typeof console !== 'undefined') {
                        console.log(errorThrown);
                    }
                },
                success: function (data) {
                    /* data, textStatus, jqXHR */
                    if (typeof data !== 'undefined' && data !== null) {
                        renderBreakingNews(data);
                    }
                },
                complete: pollBreakingNews
            });
        }
    }

    function initBreakingNews() {
        closeBtn.click(closeBreakingNews);
        loadBreakingNews();
    }

    if (banner.length > 0) {
        initBreakingNews();
    }

    /**
     * Analytics: Trigger the Analytics "Breaking News click" event.
     */
    function breakingNewsClickEvent() {
        if (typeof window.trackMetrics === 'function') {
            try {
                window.trackMetrics({
                    type: 'breaking-news',
                    data: {
                        item: 'breaking news'
                    }
                });
            } catch (e) {
                /* Ignored. */
            }
        }
    }

});

/* global CNN */

var MSIB = window.MSIB || {};

/* Define the functions needed for the tab in here */
(function ($) {
    'use strict';

    var alertNewsletterName = 'textbreakingnews',
        isSubscribed = false,
        displayFeedback,
        successStatus,
        subscribe = 'subscribe',
        unsubscribe = 'unsubscribe',
        errorMessage;

    CNN.mycnn.alerts = {

        show: function () {
            displayFeedback = false;
            CNN.mycnn.alerts.getAlertNewsletter();
        },

        /**
         * Displays alerts html
         */
        displayAlerts: function () {

            var output = {isSubscribed: isSubscribed};

            window.dust.render('views/cards/tool/mycnn-alerts', output, function (err, out) {
                if (err) {
                    if (window.console && window.console.error) {
                        console.error(err);
                    }
                } else {
                    CNN.mycnn.updateBody(out);

                    /* check emailid exists */
                    CNN.mycnn.user.reload(function (user) {
                        if (user.loggedIn) {
                            if (typeof CNN.mycnn.user === 'object' && typeof CNN.mycnn.user.msib === 'object' && CNN.mycnn.user.msib.preferredEmail === null) {
                                errorMessage = 'An email address is required to sign up for Email Alerts. ' +
                                    '<a href="javascript:void(0);" id="mycnn-alerts-acclink">' +
                                    'Please add an email address to your CNN profile here.</a>';
                                $(document.getElementById('mycnn-alerts-message')).html(errorMessage);
                                $(document.getElementById('mycnn-alerts-sign-up-button')).attr('disabled', true);
                                $(document.getElementById('mycnn-alerts-edit-email')).hide();
                                $(document.getElementById('mycnn-alerts-acclink')).click(function () {
                                    CNN.mycnn.details.show();
                                });
                            }
                        }
                    });

                    if (displayFeedback) {
                        if (successStatus) {
                            $(document.getElementById('mycnn-alerts-message')).html('CNN Breaking News email alerts successfully saved!');
                        } else {
                            $(document.getElementById('mycnn-alerts-message')).html('Error while processing request.');
                        }
                    }
                    CNN.mycnn.alerts.registerHandlers();
                }
            });
        },

        /**
         * Gets the logged in users subscribed news letters
         */

        getAlertNewsletter: function () {
            var params;

            if (typeof MSIB.util !== 'undefined' && typeof MSIB.util.getTid !== 'undefined' &&
                typeof MSIB.util.getAuthid !== 'undefined') {
                params = {
                    tid: MSIB.util.getTid(),
                    authid: MSIB.util.getAuthid(),
                    name: alertNewsletterName
                };
                MSIB.msapi.news.get(params, CNN.mycnn.alerts.handleSubscriptionsCallback);
            }
        },

        /**
         * Callback that MSIB will call once getSubscriptions has finished
         * @param {object} result -  MSIB object
         */

        handleSubscriptionsCallback: function (result) {

            if (result !== null && result !== 'undefined' && result.responseJSON.newsletters) {

                if (result.responseJSON.newsletters.length > 0) {
                    /* only called msib for one newsletter. */

                    if (result.responseJSON.newsletters[0].status === 'active') {
                        isSubscribed = true;
                    } else {
                        isSubscribed = false;
                    }
                } else {
                    isSubscribed = false;
                }
            }
            CNN.mycnn.alerts.displayAlerts();
        },

        /**
         * Register button clicks
         */

        registerHandlers: function () {
            $(document.getElementById('mycnn-alerts-sign-up-button')).click(function () {
                CNN.mycnn.alerts.processRequest(subscribe);
            });

            $(document.getElementById('mycnn-alerts-unsubscribe-button')).click(function () {
                CNN.mycnn.alerts.processRequest(unsubscribe);
            });

            $(document.getElementById('mycnn-alerts-edit-email')).click(function () {
                CNN.mycnn.details.show();
            });
        },

        /**
         * Sign up user for breaking news
         * @param {string} action - subscribe or unsubscribe
         */
        processRequest: function (action) {
            var params;

            if (typeof MSIB.util !== 'undefined' && typeof MSIB.settings !== 'undefined' &&
                typeof MSIB.settings.appID !== 'undefined' && typeof MSIB.util.getTid !== 'undefined' &&
                typeof MSIB.util.getAuthid !== 'undefined' && typeof MSIB.util.getAuthpass !== 'undefined') {
                params = {
                    appid: MSIB.settings.appID,
                    tid: MSIB.util.getTid(),
                    authid: MSIB.util.getAuthid(),
                    authpass: MSIB.util.getAuthpass(),
                    name: 'textbreakingnews'
                };
                if (action === subscribe) {
                    MSIB.msapi.news.subscribe(params, CNN.mycnn.alerts.handleCallback);
                } else {
                    MSIB.msapi.news.unsubscribe(params, CNN.mycnn.alerts.handleCallback);
                }
            }
        },

        /**
         * handles callback of subscribing to BN newsletter. Sets success if no error; otherwise false
         * @param  {object} response - msib response object
         */

        handleCallback: function (response) {
            displayFeedback = true;
            if (response && response.responseJSON.success) {
                successStatus = true;
            } else {
                successStatus = false;
            }

            /* check msib response and display */
            CNN.mycnn.alerts.getAlertNewsletter();
        }
    };

}(jQuery));

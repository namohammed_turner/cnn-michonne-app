/* global CNN */

/**
 * Creates an object that tracks the client's browser size and scroll position
 * and keeps a cached version for available use. This cuts down on calcuations to
 * the height and width function that are not needed. This should be fast enough
 * for most needs of the browser height and width.
 */
(function createCurrentSize(ns, win, $j) {
    'use strict';

    ns.CurrentSize = $j.extend({}, {
        /**
         * Keeps a cache of the last known client height of the page. If
         * not set then -1 will be returned.
         *
         * @member {number} lastKnownHeight
         */
        lastKnownHeight: -1,

        /**
         * Keeps a cache of the last known client width of the page. If
         * not set then -1 will be returned.
         *
         * @member {number} lastKnownWidth
         */
        lastKnownWidth: -1,

        /**
         * The last known client ScrollTop of the page. If not set then -1 will be returned.
         *
         * @member {number} lastKnownScrollTop
         */
        lastKnownScrollTop: -1,

        /**
         * Returns a cached value of the page's height.
         *
         * @returns {number} The client's height or -1 one if it has not been set yet.
         */
        getClientHeight: function getHeight() {
            if (this.lastKnownHeight === -1) {
                console.warn('getClientHeight: Requesting the height before this object was initialized!');
            }

            return this.lastKnownHeight;
        },

        /**
         * Returns a cached value of the page's width.
         *
         * @returns {number} The client's width or -1 one if it has not been set yet.
         */
        getClientWidth: function getWidth() {
            if (this.lastKnownWidth === -1) {
                console.warn('getClientWidth: Requesting the width before this object was initialized!');
            }

            return this.lastKnownWidth;
        },

        /**
         * Returns a cached value of the page's ScrollTop.
         *
         * @returns {number} The client's ScrollTop or -1 one if it has not
         * been set yet.
         */
        getClientScrollTop: function () {
            if (this.lastKnownScrollTop === -1) {
                console.warn('getClientScrollTop: Requesting the scrollTop before this object was initialized!');
            }
            return this.lastKnownScrollTop;
        },

        /**
         * Updates the cached value for the client's window height and
         * width and scrollTop  with the current values using jQuery's
         * measurements.
         */
        trackClientSize: function trackClientSize() {
            var st = $j(win).scrollTop();

            this.lastKnownScrollTop = st > 0 ? st : 0;
            this.lastKnownHeight = $j(win).height();
            this.lastKnownWidth = $j(win).width();
        },

        /**
         * Updates the cached value for the client's window height and
         * width and scrollTop  with the current values using jQuery's
         * measurements.
         * Stores those value in the browser session storage.
         */
        trackAndStoreClientSize: function trackClientSize() {
            var st = $j(win).scrollTop();

            win.sessionStorage.cnnST = (this.lastKnownScrollTop = st > 0 ? st : 0);
            win.sessionStorage.cnnWd = (this.lastKnownWidth = $j(win).width());
            /* win.sessionStorage.cnnHt = (this.lastKnownHeight = $j(win).height()); */
        },

        /**
         * Updates the cached value for the client's window ScrollTop with
         * a read to the live ScrollTop using jQuery's ScrollTop measurement.
         */
        trackClientScrollTop: function trackClientScrollTop() {
            var st = $j(win).scrollTop();

            this.lastKnownScrollTop = st > 0 ? st : 0;
        },

        /**
         * Updates the cached value for the client's window ScrollTop with
         * a read to the live ScrollTop using jQuery's ScrollTop measurement.
         * Stores that value in the browser session storage.
         */
        trackAndStoreClientScrollTop: function trackClientScrollTop() {
            var st = $j(win).scrollTop();

            win.sessionStorage.cnnST = (this.lastKnownScrollTop = st > 0 ? st : 0);
        },

        /**
         * Kicks off the first height read and the events needed to track
         * the client's size and scroll position.
         */
        readySetGo: function readySetGo() {
            var hasSessionStorage = false,
                x = '__testing';

            try {
                win.sessionStorage.setItem(x, x);
                win.sessionStorage.removeItem(x);
                hasSessionStorage = true;
            } catch (e) {}

            this.lastKnownHeight = $j(win).height();
            this.lastKnownWidth = $j(win).width();
            if (hasSessionStorage === false) {
                this.lastKnownScrollTop = $j(win).scrollTop() || 0;
                $j(win).on('scroll', $j.proxy(this.trackClientScrollTop, this));
                $j(win).on('resize', $j.proxy(this.trackClientSize, this));
            } else {
                if (win.sessionStorage.cnnLP === win.location.pathname &&
                    win.sessionStorage.cnnWd === this.lastKnownWidth &&
                    win.sessionStorage.cnnLT > (Date.now() - 3600000)) {

                    this.lastKnownScrollTop = win.sessionStorage.cnnScrollTop || 0;
                } else {
                    this.lastKnownScrollTop = $j(win).scrollTop() || 0;
                    win.sessionStorage.cnnST = this.lastKnownScrollTop;
                    win.sessionStorage.cnnLP = win.location.pathname;
                }
                win.sessionStorage.cnnLT = Date.now();
                $j(win).on('scroll', $j.proxy(this.trackAndStoreClientScrollTop, this));
                $j(win).on('resize', $j.proxy(this.trackAndStoreClientSize, this));
            }
        }
    }, ns.CurrentSize);

    ns.CurrentSize.readySetGo();

})(CNN, window, jQuery);


/* eslint no-unused-vars: "off" */
/* global CNN */

/**
 * These functions are called by nativo script to render the facebook,twitter
 * feeds and ads related to the sponsor content in the sponsor article page.
 */

var nativo = {},
    $staggeredZone,
    $staggeredColumn,
    $nativeContainer,
    layoutSize,
    $staggeredNativo,
    $authorDetails,
    $twitterImage,
    $nativeEmbedVideo,
    $bigPicture,
    isGalleryRendered = false,
    $nativeHubCarousel,
    isEpicRendered = false;

/**
 * This will be called by Nativo after the card is injected in zones.
 * This method is handling only for staggered zone.
 */

function nativoComplete() {
    'use strict';

    jQuery('.zn.zn-staggered').each(function () {
        $staggeredZone = jQuery(this);
        layoutSize = ($staggeredZone.attr('data-eq-state') || '').replace(/^(\S+\s+)*/, '');
        $staggeredZone.find('.zn__containers .column').each(function () {
            $staggeredColumn = jQuery(this);
            if ($staggeredColumn.attr('class') !== undefined && $staggeredColumn.attr('class').indexOf('ntv') > -1) {
                $nativeContainer = $staggeredColumn.find('.cn').addClass('nativeSponsor').clone();
                $staggeredColumn.remove();
                $staggeredNativo = $staggeredZone.find('.column');
                /* Nativo containers should always be placed as second container in staggered zone */
                if (layoutSize === 'xsmall') {
                    $staggeredNativo = $staggeredNativo.eq(0).find('.cn').eq(1);
                } else {
                    $staggeredNativo = $staggeredNativo.eq(1).find('.cn').eq(0);
                }
                $staggeredNativo.before($nativeContainer);
            }
        });
    });

    /* Need to add classname once the native zone is loaded to handle for lazy loading on scroll */
    jQuery('.pg-wrapper').find('section.nativeSponsor').addClass('zn-loaded');

    /* This is to create Nativo container layout as carousel for Mobile view */
    jQuery(window).load(function () {
        $nativeHubCarousel = jQuery(document.getElementById('ntv-videoZone')).find('.js-owl-carousel');

        if ($nativeHubCarousel.length > 0 && !isGalleryRendered) {
            CNN.Carousel.restore($nativeHubCarousel.parent());
            isGalleryRendered = true;
        }
    });
}


/**
 * This is called by nativo to pass any key value pair values associated with the ad campaign.
 *
 * @param {object} params - facebook and twitter ids are passed.
 */
function onNativoRenderAd(params) {
    'use strict';

    nativo.fbFeed = params.facebook_feed;
    nativo.twFeed = params.twitter_feed;
    nativo.twitterImage = params.twitter_image;
    nativo.authorName = params.author_name;
    nativo.epicAdName = params.native_adv;
    $nativeEmbedVideo = jQuery(document.getElementById('prx_full_ad_video'));
    if ($nativeEmbedVideo.length > 0) {
        $nativeEmbedVideo.parent().addClass('ntv_video_p');
    }
    $bigPicture = jQuery(document.getElementById('bigPicture'));
    if ($bigPicture.length > 0 && !isGalleryRendered) {
        nativo.renderGallery();
        isGalleryRendered = true;
    }

    jQuery(window).load(function nativoPageLoad() {
        nativo.renderAuthor();
        nativo.renderFacebook();
        nativo.renderTwitter();
        nativo.renderEpic();
    });
}

function changeOwlCarousalWidth() {
    'use strict';
    var $owlStage = jQuery('.js-pg-sponsor .owl-stage-outer .owl-stage'),
        $owlItemLength = $owlStage.find('.owl-item').length;
    $owlStage.css('width', $owlItemLength * 70);
}

(function () {
    'use strict';

    nativo = {
        fbFeed: '',
        twFeed: '',
        social_hub: {
            icons: {
                twitter: '',
                facebook: '',
                linkedin: '',
                google: '',
                pinterest: '',
                instagram: ''
            },
            fb_like: '',
            tw_follow: ''
        }
    };



    /* This function renders the facebook feed on the left rail of sponsor article page */
    nativo.renderAuthor = function () {
        if (typeof nativo.authorName === 'string' && nativo.authorName.length > 0) {
            $authorDetails = jQuery('.js-sponsor-article-author');
            $authorDetails.find('.js-author_name').text(nativo.authorName + ',');
            $authorDetails.show();
        }
    };

    nativo.renderEpic = function () {
        var edition = CNN.contentModel.edition === 'international' ? 'cnni' : 'cnn';

        if (typeof nativo.epicAdName === 'string' && nativo.epicAdName.length > 0 && !isEpicRendered) {
            window.AdFuel.processNewRegistry(CNN.epicAdRegistry + '/' + edition + '_native_' + nativo.epicAdName + '.js');
            jQuery('.js-epicAd_Render').show();
            isEpicRendered = true;
        }
    };

    nativo.renderFacebook = function () {
        var fbFeed,
            divElem;

        if (typeof nativo.fbFeed === 'string' && nativo.fbFeed.length > 0) {
            fbFeed = encodeURIComponent(nativo.fbFeed);
            divElem = document.getElementById('fb-likebox');
            divElem.innerHTML = '<iframe src="//www.facebook.com/plugins/likebox.php?href=' + fbFeed +
                '&amp;width=300&amp;height=590&amp;show_faces=true&amp;colorscheme=light&amp;' +
                'stream=true&amp;show_border=true&amp;header=true&amp;appId=70673860847" scrolling="no" ' +
                'frameborder="0" class="sponsor-content-fb" allowTransparency="true"></iframe>';
        }
    };


    /* This function renders the twitter feed on the left rail of sponsor article page */
    nativo.renderTwitter = function () {
        var js,
            fjs;

        if (typeof nativo.twFeed === 'string' && nativo.twFeed.length > 0) {
            jQuery(document.getElementById('tw-feedbox')).html(nativo.twFeed);
            jQuery('.js-right-rail-tw').show();
        }

        /* call this no matter what for twitter button in nav */
        fjs = document.getElementsByTagName('script')[0];

        if (!document.getElementById('twitter-wjs')) {
            js = document.createElement('script');
            js.id = 'twitter-wjs';
            js.src = '//platform.twitter.com/widgets.js';
            fjs.parentNode.insertBefore (js, fjs);
        }
        if (typeof nativo.twitterImage === 'string' && nativo.twitterImage.length > 0) {
            $twitterImage = jQuery('.js-twitterImage');
            $twitterImage.find('img').attr('src', nativo.twitterImage);
            $twitterImage.show();
        }
    };

    nativo.renderGallery = function () {
        setTimeout (function () {
            var $pgSponsor,
                $bigPicture,
                $bigPictureImg,
                $link,
                firstImageSrc,
                imageCaption,
                sponsorGalleryTitle,
                $owlStage,
                $owlItemLength,
                photoCount = 1;

            /* OWL custom javascript */
            jQuery('.js-pg-sponsor .owl-carousel').owlCarousel({
                slideSpeed: 300,
                pagination: true,
                paginationSpeed: 400,
                items: 9,
                nav: true,
                navText: ['', ''],
                dots: false,
                lazyLoad: true,
                loop: false,
                autoWidth: true,
                autoHeight: false
            });

            $pgSponsor = jQuery('.js-pg-sponsor');
            $bigPicture = $pgSponsor.find('.cnngo-bigPicture');
            $bigPictureImg = $bigPicture.find('img');
            $link = $pgSponsor.find('.owl-carousel').find('.link');
            firstImageSrc = $link.first().attr('data-img-full');
            imageCaption = $link.first().attr('data-value');
            sponsorGalleryTitle = $bigPicture.attr('data-title');
            $owlStage = $pgSponsor.find('.owl-stage-outer .owl-stage');
            $owlItemLength = $owlStage.find('.owl-item').length;

            if ($bigPicture.find('img').length === 0) {
                $bigPicture.append('<img/>');
            }
            $bigPictureImg.attr('src', firstImageSrc);
            $bigPicture.append($pgSponsor.find('.js-galleryCaption'));
            $bigPicture.append($pgSponsor.find('.js-owl-nav'));
            if ($owlItemLength >= 1) {
                $bigPicture.find('.js-active-count').html(photoCount);
                $bigPicture.find('.js-total-count').html($owlItemLength);
                $bigPicture.find('.js-gallery-title').html(sponsorGalleryTitle);
                $bigPicture.find('.el__storyelement__gray').html(' ' + $owlItemLength + ' Photos');
                $owlStage.find('.owl-item').first().addClass('synced');
                $bigPicture.find('.image__caption').html(imageCaption);
            }
            $link.on('click', function (_event) {
                var $this = jQuery(this);
                $bigPictureImg.attr('src', $this.data('imgFull'));
                $this.parent().parent().find('.owl-item').removeClass('synced');
                $this.parent().addClass('synced');
                $bigPicture.find('.js-active-count').html($this.closest('.owl-item').index() + 1);
                $bigPicture.find('.image__caption').html($this.attr('data-value'));
            });
            $bigPicture.find('.js-el__gallery-caption').click(function () {
                toggleNativoCaptions(this, 'slow');
            });
            $bigPicture.find('.js-owl-prev').click(function () {
                navigateImage('prev');
            });
            $bigPicture.find('.js-owl-next').click(function () {
                navigateImage('next');
            });
            changeOwlCarousalWidth();

        }, 100 * 8);
    };

    function toggleNativoCaptions(el, speed) {
        var $this = jQuery(el).closest(document.getElementById('bigPicture')),
            $sibs = $this.find('.media__caption'),
            $gallery = $this.find('.el__gallery-caption');

        CNN.CurrentSize = CNN.CurrentSize || {};
        CNN.CurrentSize.getClientWidth = CNN.CurrentSize.getClientWidth || jQuery.noop;

        if (CNN.CurrentSize.getClientWidth() > CNN.minWidthForExpandElements) {
            if ($gallery.hasClass('el__gallery-caption--closed')) {
                $gallery.removeClass('el__gallery-caption--closed');
            } else {
                $gallery.addClass('el__gallery-caption--closed');
            }
        } else {
            $gallery.toggleClass('el__gallery-caption--closed');
        }
        $sibs.toggle(speed);
        $gallery.text($gallery.hasClass('el__gallery-caption--closed') ? 'Show Caption' : 'Hide Caption');
    }

    function navigateImage(navigation) {
        var $syncedImage = jQuery('.js-pg-sponsor .owl-stage-outer .owl-stage .owl-item.synced'),
            $nextImage = $syncedImage.next(),
            $prevImage = $syncedImage.prev();

        if (navigation === 'prev') {
            if (typeof $prevImage !== 'undefined' && $prevImage.length > 0) {
                loadImage($prevImage);
            }
        } else {
            if (typeof $nextImage !== 'undefined' && $nextImage.length > 0) {
                loadImage($nextImage);
            }
        }
    }

    function loadImage($image) {
        var $pgSponsor = jQuery('.js-pg-sponsor'),
            $bigPicture = $pgSponsor.find('.cnngo-bigPicture'),
            $bigPictureImg = $bigPicture.find('img'),
            imageUrl = $image.find('.link').attr('data-img-full');

        $bigPictureImg.attr('src', imageUrl);
        $image.parent().find('.owl-item').removeClass('synced');
        $image.addClass('synced');
        $bigPicture.find('.js-active-count').html($image.closest('.owl-item').index() + 1);
        $bigPicture.find('.image__caption').html($image.find('.link').attr('data-value'));
    }

    jQuery(window).resize (function () {
        setTimeout (function () {
            changeOwlCarousalWidth();
        }, 100 * 3);
    });
})();


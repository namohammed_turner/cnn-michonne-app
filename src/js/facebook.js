/* global CNN */

CNN.Facebook = CNN.Facebook || {
    initHandlers: [],
    isFBinit: false,
    addToFBInitHandlers: function (cb) {
        'use strict';

        if (CNN.Facebook.isFBinit) {
            cb();
        } else {
            CNN.Facebook.initHandlers.push(cb);
        }
    }
};

window.fbAsyncInit = function () {
    'use strict';

    CNN.Facebook.isFBinit = true;
    CNN.Facebook.initHandlers.forEach(function (cb) {
        cb();
    });
    CNN.Facebook.initHandlers = [];
};

jQuery(document).onZonesAndDomReady(function () {
    'use strict';

    var fbelements = jQuery('.fbelement'),
        width = (fbelements.length > 0) ? fbelements.eq(0).width() : 0;

    jQuery('.fb-post').attr('data-width', width);
    (function (d, id) {
        var js,
            fjs = d.getElementsByTagName('script')[0];

        if (!d.getElementById(id)) {
            js = d.createElement('script');
            js.id = id;
            js.src = '//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.2';
            fjs.parentNode.insertBefore(js, fjs);
        }
    }(document, 'facebook-jssdk'));
});


/* global CNN, MSIB, gigya */

(function ($) {
    'use strict';

    var /*
         * Request a 180x180px avatar (if possible) to match the largest
         * avatar size set by our social media providers (namely, Facebook).
         */
        DEFAULT_AVATAR_SIZE = 180,
        params = {},
        /* CnnUser from msib-init. */
        user,
        gigyaUser,
        gigyaAvatarUrl,
        /* Gigya login timestamp from the user's clock.  See isPrincipalExpired(). */
        gigyaLoginTs,
        redirectUrl,
        redirectId,
        /* Functions to execute when the user has successfully logged in. */
        loginActions = [];

    /**
     * Parses a URL with optional query string into a mapping of parameters.
     *
     * This only handles single string values for simplicity.  Multiple
     * parameters with the same name will only map to a one of the values;
     * which of the values is chosen is left undefined.
     *
     * @param {string} url - The full URL.
     * @return {object} The parsed parameters.
     */
    function parseQueryParams(url) {
        var params = {},
            div = url.indexOf('?'),
            qs;

        if (div >= 0) {
            qs = url.substr(div);
            qs.replace(new RegExp('([^?=&]+)(=([^&]*))?', 'g'),
                function ($0, $1, $2, $3) {
                    params[$1] = decodeURIComponent($3);
                });
        }

        return params;
    }

    /**
     * Check if a specific error is amongst the list of errors in the
     * response from MSIB.
     * @param {object} msg - The response payload from MSIB.
     * @param {string} field - The "field" field to match.  May be null to
     *                         match only the error field.
     * @param {string} errorCode - The "error" field to match.
     * @return {boolean} true if a matching error was found,
     *                   false otherwise.
     */
    function hasMsibError(msg, field, errorCode) {
        var errors,
            i;

        if (msg && msg.errors && msg.errors.length > 0) {
            errors = msg.errors;

            for (i = errors.length - 1; i >= 0; i -= 1) {
                if ((!field || errors[i].field === field) &&
                        errors[i].error === errorCode) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Attempts to determine if the Gigya principal expired.
     *
     * When we get an "invalid.principal" error from MSIB, there's two
     * possibilities: The server is misconfigured, or the principal timed-out.
     * MSIB only gives us 5 minutes to do our normal post-login actions before
     * we need to either finalize the login or log in again.
     *
     * This function attempts to disambiguate the two cases so we can
     * properly message the user.
     *
     * @return {boolean} true if the principal (probably) expired,
     *                   false if the server is (probably) misconfigured.
     */
    function isPrincipalExpired() {
        var timeout = 3 * 60 * 1000,  /* 3 minutes */
            diff;

        diff = new Date() - gigyaLoginTs;

        return diff >= timeout;
    }

    /**
     * Clear the error banner.
     */
    function clearError() {
        showError('');
        $('.social-error').hide();
    }

    /**
     * Display a plain text error message.
     * @param {string} s - The message to display.
     */
    function showError(s) {
        var errorText = 'Please correct the following errors:';
        $('.social-error').html('<span class="social-error__message">' + errorText + '</span><ul><li class="social-error__error-item">' + s + '</li>');
        $('.social-error').show();
    }

    /**
     * Display a plain text error messages.
     * @param {object} errors - The messages to display.
     */
    function showErrors(errors) {
        var errorContainer = $('.social-error'),
            listElem = $('<ul>'),
            i,
            errorMsg;

        errors = errors || [];
        for (i = 0; i < errors.length; i++) {
            errorMsg = (errors[i].message).replace('loginId', 'Email')
                .replace('password OR principalType', 'Password')
                .replace('invalidCredentials', 'Invalid credentials');
            listElem.append($('<li class="social-error__error-item">').text(errorMsg));
        }

        errorContainer.empty();
        errorContainer.append('<span class="social-error__message">Please correct the following errors:</span>');
        listElem.appendTo(errorContainer);
        errorContainer.show();
    }

    /**
     * Display the error(s) from an MSIB Platform API call.
     * @param {object} msg - The response object from the API call.
     *                       This is the third param passed to the callback.
     */
    function showMsibStatusError(msg) {
        var errors;
        if (msg && msg.errors) {
            errors = msg.errors;
        } else {
            errors = [{message: JSON.stringify(msg)}];
        }
        showErrors(errors);
    }

    /**
     * Sanitize a potential redirect URL.
     * @param {string} url - The URL to check.
     * @param {string} def - The default URL.
     * @return {string} The sanitized URL, or the default if the URL is invalid.
     */
    function sanitizeRedirectUrl(url, def) {
        var urlRx = /^https?:\/\/(([-a-z0-9]+\.)*cnn\.com)(:[0-9]+)?\/?/,
            matches;

        if (!url || typeof url !== 'string') {
            return def;
        }

        url = url.replace(/^\s+|\s+$/g, '');
        if (url === '') {
            return def;
        }

        matches = url.match(urlRx);
        if (!matches) {
            return def;
        }

        return url;
    }

    /**
     * Redirect back to the site.
     */
    function redirectBack() {
        window.location.href = redirectUrl;
    }

    /**
     * Update the UI to reflect the current login state.
     * @param {boolean} skipReload - true to just update the panels without
     *                               reloading the data from the server.
     * @param {boolean} registering - true to set the new account register success message
     */
    function updatePanels(skipReload, registering) {
        var loginPanel = $(document.getElementById('msib-login-panel')),
            infoPanel = $(document.getElementById('msib-info-panel'));

        function fillPanels() {
            if (user.loggedIn) {
                if (!user.hasAuthority('USERNAME') &&
                        !user.hasAuthority('GIGYA')) {
                    showScreenNamePanel();
                } else {
                    if ($(document.getElementById('msib-display-username'))) {
                        $(document.getElementById('msib-display-username')).text(user.displayName);
                    }
                    if ($(document.getElementById('msib-display-msg'))) {
                        if (registering) {
                            $(document.getElementById('msib-display-msg')).text('Thank you for registering!');
                        } else {
                            $(document.getElementById('msib-display-msg')).text('Welcome Back!');
                        }
                    }
                    infoPanel.show();

                    postLogin(function () {
                        redirectId = window.setTimeout(redirectBack, 2000);
                    });
                }

            } else {
                /*
                 * This happens when the user isn't logged-in or there was
                 * a problem retrieving the user's info.
                 * Retrieving the user's info may be a transient problem,
                 * so just act like the user isn't logged in for now.
                 */
                loginPanel.show();
            }
        }

        /* Hide all panels while we look up the user's info. */
        $('.msib-panel').hide();

        if (skipReload) {
            fillPanels();
        } else {
            user.reload(fillPanels);
        }
    }

    /**
     * Switch to the panel that prompts the user to accept the current TOS.
     * @param {function} acceptFn - The event handler for the "Accept" button.
     */
    function showTosPanel(acceptFn) {
        /*
         * Note: Since we're not actually logged in at this point, we
         * silently re-use the login form.  Clicking on the "Accept" form
         * is the same as logging in, except we set the acceptedLegalDocs
         * parameter.
         */
        $(document.getElementById('msib-tos-learn-more')).hide();
        $(document.getElementById('msib-tos-learn-more-link')).show();
        $(document.getElementById('msib-login-panel')).hide();
        $(document.getElementById('msib-link-panel')).hide();
        $(document.getElementById('msib-tos-panel')).show();

        $(document.getElementById('msib-tos-accept')).off('click').on('click', acceptFn);
    }

    function showScreenNamePanel() {
        $(document.getElementById('msib-screen-name-panel')).show();
        $(document.getElementById('msib-screen-name-txt')).val('');
    }

    /**
     * Persist CNN-specific user attributes which are generated at login.
     *
     * These attributes may come from either CNN-specific business logic
     * or from the social login provider.  This allows the information to be
     * restored and synchronized between user sessions when the MSIB SDK
     * performs login normalization.
     *
     * Currently, only the user's avatar is persisted.
     *
     * @param {object} attrs - Attributes to set, all are optional.
     * @param {string} [attrs.avatarUrl] - The full URL to the user's avatar.
     * @param {string} [attrs.displayName] - The user's display name.
     * @param {string} callback - Called when complete, with standard MSIB SDK
     *                            callback args.
     */
    function persistUserAttributes(attrs, callback) {
        var
            opts = {
                tid: MSIB.util.getTid(),
                authid: MSIB.util.getAuthid(),
                authpass: MSIB.util.getAuthpass()
            };

        if (attrs.avatarUrl) {
            opts['attributes[\'avatarUrl\']'] = attrs.avatarUrl;
        }
        if (attrs.displayName) {
            opts.displayName = attrs.displayName;
        }

        MSIB.msapi.editUserInfo(opts, callback);
    }

    /**
     * User has been logged-in to MSIB (and maybe Gigya) and the user profile
     * has been updated to reflect this.  Now, connect the login to Livefyre
     * by setting the Livefyre user token in a cookie.
     * @param {boolean} registering - true to set the new account register success message
     */
    function finalizeLogin(registering) {
        /* Generate Livefyre user token and stash in a cookie. */
        MSIB.msapi.livefyre.getToken({
            tid: MSIB.util.getTid(),
            authid: MSIB.util.getAuthid()
        }, function (xhr, textStatus, msg) {
            if (CNN.msib.isSuccessStatus(xhr)) {
                updatePanels(false, registering);
            } else {
                showMsibStatusError(msg);
            }
        });
    }

    /**
     * Perform queued actions for after the user has been logged in.
     * @param {function} callback - The final callback (no params).
     */
    function postLogin(callback) {
        (function next() {
            var action;

            if (loginActions.length === 0) {
                callback();
            } else {
                action = loginActions.shift();
                action(next);
            }
        }());
    }

    /**
     * Update the user profile now that the user has been logged-in to
     * MSIB (and maybe Gigya).
     *
     * @param {boolean} registering - true to set the new account register success message
     */
    function onMsibLoggedIn(registering) {
        var persistCallback;

        /* Clear the login form since we're sure we don't need it anymore. */
        $(document.getElementById('msib-login-frm'))[0].reset();

        persistCallback = function (xhr, textStatus, msg) {
            if (CNN.msib.isSuccessStatus(xhr)) {
                finalizeLogin(registering);
            } else {
                showMsibStatusError(msg);
            }
        };

        /*
         * If the user logged-in using Gigya, use the profile data from
         * the social login provider.  Otherwise, generate the profile data
         * from the MSIB profile.
         */
        if (gigyaUser) {
            if (typeof registering === 'boolean' && !registering) {
                if (typeof gigyaAvatarUrl !== 'undefined' && gigyaAvatarUrl.length > 0 && gigyaAvatarUrl !== gigyaUser.photoURL) {
                    persistUserAttributes({
                        avatarUrl: gigyaUser.photoURL || CNN.msib.cfg.avatar.defaultUrl,
                        displayName: gigyaUser.nickname || 'CNN User'
                    }, persistCallback);
                } else {
                    finalizeLogin(registering);
                }
            } else {
                persistUserAttributes({
                    avatarUrl: gigyaUser.photoURL || CNN.msib.cfg.avatar.defaultUrl,
                    displayName: gigyaUser.nickname || 'CNN User'
                }, persistCallback);
            }
        } else {
            user.reload(function () {
                var username = user.getAuthority('USERNAME'),
                    prevAvatarUrl = user.msib.attributes.avatarUrl,
                    avatarUrl = CNN.msib.avatarForScreenName(
                        DEFAULT_AVATAR_SIZE, username);

                /* For MSIB login, verify whether same user logged in before using social account; if so persist user info. */
                if (typeof registering === 'boolean' && !registering) {
                    if (typeof prevAvatarUrl !== 'undefined' && typeof avatarUrl !== 'undefined') {
                        if (prevAvatarUrl !== avatarUrl) {
                            /*
                             * NOTE If social account is not linked with MSIB then prevAvatarUrl does not return
                             * actual avatar url of the social account
                             */
                            persistUserAttributes({
                                avatarUrl: avatarUrl,
                                displayName: username || 'CNN User'
                            }, persistCallback);
                        } else {
                            finalizeLogin(registering);
                        }
                    } else {
                        finalizeLogin(registering);
                    }
                } else {
                    /* Signup always requires user data persistence */
                    persistUserAttributes({
                        avatarUrl: avatarUrl,
                        displayName: username || 'CNN User'
                    }, persistCallback);
                }
            });
        }
    }

    /**
     * Update the UI to indicate that the user has successfully logged out.
     */
    function onMsibLoggedOut() {
        $('.msib-panel').hide();
        $(document.getElementById('msib-logout-panel')).show();

        redirectId = window.setTimeout(redirectBack, 2000);
    }

    /**
     * Fired when Gigya successfully logs the user in via social login.
     * @param {object} gigyaResponse - See http://developers.gigya.com/020_Client_API/010_Socialize/socialize.login#response
     */
    function onGigyaLoggedIn(gigyaResponse) {
        clearError();

        gigyaUser = gigyaResponse.user || {};
        gigyaLoginTs = new Date();

        /* Logged in to Gigya, now log in to MSIB. */
        MSIB.msapi.login({
            /* HACK See msibLogin(). */
            acceptedLegalDocuments: 'domestic_version',

            principalType: 'GIGYA',
            loginId: MSIB.social.gigya.makeGigyaPrincipal(gigyaUser)
        }, function (xhr, textStatus, msg) {
            if (CNN.msib.isSuccessStatus(xhr)) {
                if (gigyaUser.isSiteUID) {
                    MSIB.msapi.user.get({
                        tid: xhr.responseJSON.tid,
                        authid: xhr.responseJSON.authid
                    }, function (result) {
                        if (typeof result === 'object' && result.status === 200) {
                            gigyaAvatarUrl = result.responseJSON.attributes.avatarUrl || '';
                            onMsibLoggedIn();
                        }
                    });
                } else {
                    onMsibLoggedIn();
                }
            } else if (msg.errors && msg.errors.length > 0 &&
                msg.errors[0].error === 'notRegistered') {
                /* Not linked yet; ask the user to link the account. */
                $(document.getElementById('msib-login-panel')).hide();
                $(document.getElementById('msib-link-panel')).show();
            } else {
                /* Other kind of error. */
                gigyaUser = undefined;
                gigyaLoginTs = undefined;
                showMsibStatusError(msg);
            }
        });
    }

    /**
     * Handle when an operation failed because the Gigya principal expired.
     *
     * This performs a full logout to clean up the session then returns the
     * user to the login stage, with an appropriate error message.
     */
    function onPrincipalExpired() {
        clearError();

        /*
         * Clear any bits of the session so that we're not returning to
         * a half-initialized state.
         */
        user.logout(function () {
            gigyaUser = undefined;
            gigyaLoginTs = undefined;
            showError('Your session has timed out; please log in again.');
            onMsibLoggedOut();
        });
    }

    /**
     * Finalize the registration by linking the MSIB user back to Gigya.
     */
    function gigyaRegisterLink() {
        MSIB.msapi.social.gigya.generateSignature({
            tid: MSIB.util.getTid(),
            authid: MSIB.util.getAuthid()
        }, function (xhr, textStatus, msg) {
            if (CNN.msib.isSuccessStatus(xhr)) {
                gigya.services.socialize.notifyRegistration({
                    siteUID: MSIB.util.getTid(),
                    callback: onMsibLoggedIn,
                    UIDTimestamp: msg.timestamp,
                    UIDSig: msg.signature
                });
            } else {
                showMsibStatusError(msg);
            }
        });
    }

    /**
     * Ordinary MSIB login (email and password).
     * @param {boolean} acceptedTos - Indicate that the user has accepted
     *                                the latest ToS.
     * @returns {boolean} - always false
     */
    function msibLogin(acceptedTos) {
        var params = {
                loginId: $(document.getElementById('msib-login-id')).val(),
                password: $(document.getElementById('msib-password')).val()
            },
            rememberMe = $(document.getElementById('msib-remember')).prop('checked');

        if (acceptedTos) {
            params.acceptedLegalDocuments = 'domestic_version';
        }

        clearError();

        MSIB.settings.tidCookieTTL =
            rememberMe ? CNN.msib.cfg.msib.cookieTtl : 'SESSION';

        MSIB.msapi.login(params, function (xhr, textStatus, msg) {
            if (CNN.msib.isSuccessStatus(xhr)) {
                onMsibLoggedIn();
            } else if (hasMsibError(msg, null, 'missing.legalDocument')) {
                /* Prompt the user to accept the TOS. */
                showTosPanel(function () {
                    return msibLogin(true);
                });
            } else {
                showMsibStatusError(xhr.responseJSON);
            }
        });

        return false;
    }

    /**
     * Log out from all systems (Gigya, MSIB, etc.).
     * @returns {boolean} - always false
     */
    function msibLogout() {
        /* Cancel any pending redirect. */
        window.clearTimeout(redirectId);
        redirectId = undefined;

        clearError();

        MSIB.social.gigya.logout(function () {
            MSIB.util.fullLogout(function () {
                /*
                 * Ignore any logout errors.  The MSIB SDK will make sure
                 * that the MSIB cookies are deleted at least, putting us
                 * in the "logged-out" state.
                 */

                gigyaUser = undefined;
                gigyaLoginTs = undefined;
                onMsibLoggedOut();
            });
        });

        return false;
    }

    /**
     * Send a code to allow a user to reset their password.
     * @returns {boolean} - always false
     */
    function msibForgot() {
        var request;

        clearError();

        /* There's no msib_sdk function for this, so we'll do it directly. */
        request = MSIB.ajax({
            url: '/msapi/password.request',
            data: {
                type: 'EMAIL',
                principal: $('.msib-forgot-login-id').val()
            }
        });
        request.done(function (data, textStatus, jqXHR) {
            if (CNN.msib.isSuccessStatus(jqXHR) ||
                    hasMsibError(data, 'principal', 'invalid')) {
                $('.msib-forgot-panel').hide();
                $('.msib-forgot-success-panel').show();
            } else {
                showMsibStatusError(data);
            }
        });
        request.fail(function (jqXHR, textStatus, errorThrown) {
            showMsibStatusError(errorThrown);
        });

        return false;
    }

    /**
     * Handle when the user chose a new password from the password select
     * panel.
     * @returns {boolean} - always false
     */
    function msibPassword() {
        var password = $(document.getElementById('msib-password-password')).val(),
            confirmPassword = $(document.getElementById('msib-password-confirm')).val(),
            formPanel = $(document.getElementById('msib-password-panel')),
            successPanel = $(document.getElementById('msib-password-success-panel')),
            error = '';

        clearError();

        if (password === '') {
            error = 'Password is required.';
        } else if (password !== confirmPassword) {
            error = 'Passwords must match. Please try again.';
        }

        if (error !== '') {
            showError(error);
            return false;
        }

        MSIB.msapi.user.resetPassword({
            userId: params.userId,
            code: params.code,
            password: password,
            confirmPassword: confirmPassword
        }, function (xhr, textStatus, msg) {
            if (CNN.msib.isSuccessStatus(xhr)) {
                formPanel.hide();
                successPanel.show();

            } else if (hasMsibError(msg, null, 'passwordreset.codemismatch') ||
                    hasMsibError(msg, null, 'passwordreset.codeexpired')) {
                showError('Your password reset code has expired.');
                formPanel.hide();
                $(document.getElementById('msib-forgot-panel')).show();

            } else if (hasMsibError(msg, 'password', 'invalidLength')) {
                showError('Password must be between 6-15 characters and is case sensitive. Please enter a new password.');

            } else {
                showMsibStatusError(msg);
            }
        });

        return false;
    }

    /**
     * Switch to the panel that lets the user select a new password after
     * receiving and email with the password reset code.
     */
    function showPasswordPanel() {
        $('.msib-panel').hide();

        if (!params.userId || !params.code) {
            showError('Please check your email for a link to change your password.');
        } else {
            $(document.getElementById('msib-password-panel')).show();
        }
    }

    /**
     * Ordinary MSIB registration (email and password).
     * @returns {boolean} - always false
     */
    function msibRegister() {
        var email = $.trim($(document.getElementById('msib-register-email')).val()),
            password = $.trim($(document.getElementById('msib-register-password')).val()),
            passwordConfirm = $.trim($(document.getElementById('msib-register-confirm')).val()),
            screenName = $.trim($(document.getElementById('msib-register-display-name')).val()),
            firstName = $.trim($(document.getElementById('msib-register-first-name')).val()),
            lastName = $.trim($(document.getElementById('msib-register-last-name')).val()),
            optInNewsletterChecked = $(document.getElementById('msib-register-newsletter-subscribe')).is(':checked'),
            errors = [],
            REGISTERING = true;

        clearError();

        /* Mandatory fields empty validation*/
        if (screenName === '') {
            errors.push({message: 'Screen name required.'});
        }

        if (email === '' || !CNN.social.isValidEmail(email)) {
            errors.push({message: 'Please provide a valid email address.'});
        }

        if (firstName === '') {
            errors.push({message: 'First Name is a required field.'});
        }

        if (lastName === '') {
            errors.push({message: 'Last Name is a required field.'});
        }

        if (password === '') {
            errors.push({message: 'Password is required.'});
        } else if (password !== passwordConfirm) {
            errors.push({message: 'Passwords must match. Please try again.'});
        }

        if (errors.length > 0) {
            showErrors(errors);
            return false;
        }

        MSIB.msapi.register({
            /* Create a new email authority, with primary email to match. */
            emailAddress: email,
            emailAuthority: email,

            /* Create a new username authority, with displayName to match. */
            username: screenName,
            displayName: screenName,

            acceptedLegalDocuments: 'domestic_version',
            password: password,
            firstName: $(document.getElementById('msib-register-first-name')).val(),
            lastName: $(document.getElementById('msib-register-last-name')).val(),
            'address.zip': $(document.getElementById('msib-register-postcode')).val()
        }, function (xhr, textStatus, msg) {
            if (CNN.msib.isSuccessStatus(xhr)) {
                onMsibLoggedIn(REGISTERING);
                if (optInNewsletterChecked) {
                    subscribeOptInNewsletter();
                }
            } else if (hasMsibError(msg, null, 'emailInvalidFormat')) {
                showError('Please provide a valid email address.');
            } else if (hasMsibError(msg, 'emailAddress', 'duplicate')) {
                showError('The email address is already in use.');
            } else if (hasMsibError(msg, 'password', 'invalidLength')) {
                showError('Password must be between 6-15 characters and is case sensitive. Please enter a new password.');
            } else {
                showMsibStatusError(msg);
            }
        });

        return false;
    }

    /**
     * Link the current Gigya user to an MSIB account.
     * @param {string} tid - The CNNid of the MSIB account.
     * @param {string} authid - The authid of the MSIB account.
     * @param {string} authpass - The authpass of the MSIB account.
     * @returns {boolean} - always false
     */
    function msibRegisterLink(tid, authid, authpass) {
        clearError();

        if (typeof gigyaUser === 'undefined') {
            msibLogout();
            return false;
        }

        MSIB.msapi.authority.add({
            principalType: 'GIGYA',
            principal: MSIB.social.gigya.makeGigyaPrincipal(gigyaUser),
            tid: tid,
            authid: authid,
            authpass: authpass
        }, function (xhr, textStatus, msg) {
            if (CNN.msib.isSuccessStatus(xhr)) {
                gigyaRegisterLink();
            } else if (hasMsibError(msg, 'principal', 'invalid')) {
                if (isPrincipalExpired(gigyaUser, xhr, msg)) {
                    onPrincipalExpired();
                } else {
                    showError('There was an error; please try again.');
                }
            } else {
                showMsibStatusError(msg);
            }
        });

        return false;
    }

    /**
     * Link the current Gigya user to a new MSIB account.
     * @returns {boolean} - always false
     */
    function msibLinkNew() {
        clearError();

        if (typeof gigyaUser === 'undefined') {
            msibLogout();
            return false;
        }

        MSIB.msapi.register({
            principalType: 'GIGYA',
            principal: MSIB.social.gigya.makeGigyaPrincipal(gigyaUser),
            acceptedLegalDocuments: 'domestic_version'
        }, function (xhr, textStatus, msg) {
            if (CNN.msib.isSuccessStatus(xhr)) {
                gigyaRegisterLink();
            } else {
                showMsibStatusError(msg);
            }
        });

        return false;
    }

    /**
     * Link the social user to an existing MSIB account.
     * @param {boolean} acceptedTos - Indicate that the user has accepted
     *                                the latest ToS.
     * @returns {boolean} - always false
     */
    function msibLinkLogin(acceptedTos) {
        var
            params = {
                loginId: $(document.getElementById('msib-link-login-id')).val(),
                password: $(document.getElementById('msib-link-password')).val()
            };

        if (acceptedTos) {
            params.acceptedLegalDocuments = 'domestic_version';
        }

        clearError();

        MSIB.msapi.login(params, function (xhr, textStatus, msg) {
            if (CNN.msib.isSuccessStatus(xhr)) {
                msibRegisterLink(msg.tid, msg.authid, msg.authpass);
            } else if (hasMsibError(msg, null, 'missing.legalDocument')) {
                /* Prompt the user to accept the TOS. */
                showTosPanel(function () {
                    return msibLinkLogin(true);
                });
            } else {
                showMsibStatusError(msg);
            }
        });

        return false;
    }

    /**
     * Handle when the user chose a screen name on the screen name panel.
     * @returns {boolean} - always false
     */
    function msibChooseScreenName() {
        var screenName = $(document.getElementById('msib-screen-name-txt')).val(),
            avatarUrl;

        clearError();

        if (!screenName) {
            showError('Screen name is required.');
        } else if (!screenName.match(/^[A-Za-z0-9]{3,15}$/)) {
            showError('Screen name must be: Letters and numbers, 3-15 characters.');
        } else {
            /* Try to add the username authority via user.edit. */
            avatarUrl = CNN.msib.avatarForScreenName(
                DEFAULT_AVATAR_SIZE, screenName);
            MSIB.msapi.user.edit({
                username: screenName,

                /*
                 * Username changed, so update the avatar and display name
                 * here so we don't have to call persistUserAttributes()
                 * separately.
                 */
                'attributes[\'avatarUrl\']': avatarUrl,
                displayName: screenName,

                tid: MSIB.util.getTid(),
                authid: MSIB.util.getAuthid(),
                authpass: MSIB.util.getAuthpass()
            }, function (xhr, textStatus, msg) {
                if (CNN.msib.isSuccessStatus(xhr)) {
                    finalizeLogin();
                } else if (hasMsibError(msg, 'username', 'duplicate')) {
                    showError('Screen name "' + screenName + '" ' +
                        'has already been taken.  Please choose another.');
                } else {
                    showMsibStatusError(msg);
                }
            });
        }

        return false;
    }

    /**
     * Perform post-MSIB-initialization steps.
     * @param {CnnUser} cnnUser - The composite user, from msib-init.
     */
    function postInit(cnnUser) {
        user = cnnUser;
        params = parseQueryParams(document.location.href);

        CNN.msib.loadGigya(postGigyaInit);
    }

    /**
     * Perform post-Gigya-initialization steps.
     * @param {boolean} success - Whether the Gigya init was successful.
     * @returns {boolean} - always false
     */
    function postGigyaInit(success) {
        if (!(success && CNN.msib.isLoaded)) {
            /* MSIB or Gigya failed to load. */
            $(document.getElementById('msib-initializing-panel')).hide();
            $(document.getElementById('msib-init-error-panel')).show();
            return false;
        }

        /*
         * We must set a global handler for onLogin instead of in showLoginUI.
         * If showLoginUI's authFlow falls back to "redirect" mode, then
         * its onLogin event will never fire.  The global onLogin event will.
         */
        gigya.socialize.addEventHandlers({
            onLogin: onGigyaLoggedIn
        });

        gigya.socialize.showLoginUI({
            containerID: 'gigya-widget',
            context: {},
            enabledProviders: window.__gigyaConf.enabledProviders,
            showTermsLink: false,
            height: 80,
            width: 400,
            buttonsStyle: 'fullLogoColored',
            lastLoginIndication: 'none',
            onLoad: function () {
                /*
                *  This function gets called after gigya draws it's madness (5 tables!)
                *  Here I am cherrypicking the actual useful buttons and links and appending
                *  them flat-like to the #gigya-widget div
                */

                var gigyaWidget = $(document.getElementById('gigya-widget')),
                    buttons = $('#gigya-widget div[onclick]').addClass('login-button'),
                    poweredBy = $('#gigya-widget_bottomLink span[onclick]').addClass('poweredby');

                gigyaWidget.removeAttr('style');
                gigyaWidget.empty();
                gigyaWidget.append(buttons).append(poweredBy);
            }
        });

        /* Sanitize the redirect URL and update the redirect links. */
        redirectUrl = sanitizeRedirectUrl(params.r, '/');
        $(document.getElementById('msib-redirect-link')).prop('href', redirectUrl);

        /* Select the initial action to perform. */
        switch (params.a) {
        case 'follow':
            /*
             * Don't trigger the action if the user is already logged-in to
             * prevent CSRF.
             */
            if (!user.loggedIn && params.topic && CNN.follow && CNN.follow.follow) {
                loginActions.push(function (callback) {
                    CNN.follow.follow(params.topic, function () {
                        callback();
                    });
                });
            }
            updatePanels(true);
            break;
        case 'logout':
            msibLogout();
            break;
        case 'password':
            showPasswordPanel();
            break;
        default:
            updatePanels(true);
        }

        /* Wire up the forms. */
        $(document.getElementById('msib-login')).click(function () {
            return msibLogin(false);
        });

        $('.msib-forgot-link').click(function () {
            clearError();
            $('.msib-login-form').hide();
            $('.msib-forgot-panel').show();
            return false;
        });

        $(document.getElementById('msib-register-link')).click(function () {
            clearError();
            $(document.getElementById('msib-login-form')).hide();
            $(document.getElementById('msib-register-panel')).show();
            return false;
        });
        $(document.getElementById('msib-login-toggle')).click(function () {
            clearError();
            $(document.getElementById('msib-register-panel')).hide();
            $(document.getElementById('msib-login-form')).show();
            return false;
        });

        $('.js-msib-forgot-link').click(msibForgot);

        $('.msib-forgot-cancel').click(function () {
            clearError();
            $('.msib-forgot-panel').hide();
            $('.msib-login-form').show();
            return false;
        });

        $(document.getElementById('msib-tos-learn-more-link')).click(function () {
            $(document.getElementById('msib-tos-learn-more')).show();
            $(document.getElementById('msib-tos-learn-more-link')).hide();
            return false;
        });
        $(document.getElementById('msib-link-learn-more-link')).click(function () {
            $(document.getElementById('msib-link-learn-more')).show();
            $(document.getElementById('msib-link-learn-more-link')).hide();
            return false;
        });
        $(document.getElementById('msib-register-learn-more-link')).click(function () {
            $(document.getElementById('msib-register-learn-more')).show();
            $(document.getElementById('msib-register-learn-more-link')).hide();
            return false;
        });
        $(document.getElementById('msib-screen-name-continue')).click(msibChooseScreenName);

        $(document.getElementById('msib-register')).click(msibRegister);
        $(document.getElementById('msib-logout')).click(msibLogout);
        $(document.getElementById('msib-link-login')).click(function () {
            return msibLinkLogin(false);
        });
        $(document.getElementById('msib-link-new')).click(msibLinkNew);

        $(document.getElementById('msib-select-password')).click(msibPassword);
        $(document.getElementById('msib-password-login-link')).prop('href', CNN.msib.cfg.msib.loginUrl);

        $('.js-msib-login-return-link').click(function () {
            $('.msib-forgot-success-panel').hide();
            $('.msib-login-form').show();
            return false;
        });
    }

    function subscribeOptInNewsletter() {
        var params = {},
            optInNewsletterId = 'member_services'; /* 'Updates from CNN' newsletter */
        if (MSIB !== undefined) {
            params.appid = MSIB.settings.appID;
            params.tid = MSIB.util.getTid();
            params.authid = MSIB.util.getAuthid();
            params.authpass = MSIB.util.getAuthpass();
            params.name = optInNewsletterId;
            MSIB.msapi.news.subscribe(params, function (xhr, textStatus, msg) {
                if (!CNN.msib.isSuccessStatus(xhr)) {
                    showMsibStatusError(msg);
                }
            });
        }
    }

    $(function () {
        /* visible the error panel by default */
        $(document.getElementById('msib-initializing-panel')).show();
        $(document).onZonesAndDomReady(function loginDOMReady() {
            if (CNN.Utils.existsObject(CNN.msib) && typeof CNN.msib.onInitialized === 'function') {
                CNN.msib.onInitialized(postInit);
            }
        });
    });

}(jQuery));

/* global CNN */

(function (NS) {
    'use strict';

    if (typeof NS.StaggeredZone === 'function') { return; }

    function findIndexOfMin(arr) {
        var min = 0,
            i;

        /* mobile first is 1 column, faster to not cache .length */
        for (i = 1; i < arr.length; i++) {
            if (arr[i] < (arr[min])) {
                min = i;
            }
        }

        return min;
    }

    function getActualHeight($el) {
        var height,
            $imgs = jQuery('img[data-aspect-ratio]', $el),
            i,
            len,
            width = $el.width();

        /* this can be cached per breakpoint */
        for (i = 0, len = $imgs.length; i < len; i++) {
            $imgs.css('height', width / $imgs.eq(i).data('aspect-ratio') + 'px');
        }

        height = $el.outerHeight(true);
        $imgs.removeAttr('height');

        return height;
    }

    /*
     * Returns the number of columns to render based on $zone's width.
     * @param {object} $zone    - The zone to check, passed in as a jQuery object
     * @param {object} options  - The options object used to construct StaggeredZone.
     * @returns {number} count The number of columns to render.
     */
    function getColumnCountFromZoneWidth($zone, options) {
        var count = 1,
            i,
            zoneWidth = $zone.width();

        for (i = 0; i < options.breakpoints.length; i++) {

            if (zoneWidth <= options.breakpoints[i].query.maxWidth && zoneWidth >= options.breakpoints[i].query.minWidth) {
                count = options.breakpoints[i].count;
                break;
            }
        }

        return count;
    }

    /**
     * Render the width of the column.
     * @param {number} count - The number of columns to render.
     * @param {jQuery} $cols - The matched jQuery columns.
     * @param {object} options - The zone options
     * @param {jQuery} $zone - The jQuery zone DOM element
     */
    function renderColWidth(count, $cols, options, $zone) {
        $cols.each(function (i) {
            var colWidth;

            /* Check to see if the colWidths is defined and defined properly. */
            if (typeof options.breakpoints[count - 1].colWidths !== 'undefined' &&
                options.breakpoints[count - 1].colWidths.length === count) {

                /* Use custom defined widths. */
                colWidth = options.breakpoints[count - 1].colWidths[i];
            } else {

                /* Set widths equal to each other. */
                colWidth = 100 / count + '%';
            }

            jQuery(this).css('width', colWidth);
        });

        if (typeof $zone !== 'undefined' && $zone instanceof jQuery) {
            $zone.trigger(NS.StaggeredZone.columnResizeEvent);
        }
    }

    /*
     * Returns a function to be used as event handler for resize event.
     * @param {object} $zones - jQuery collection of zone elements, each of which is a Staggered zone
     * @param {object} options - The options object used to construct StaggeredZone.
     * @return {function} staggeredHandler Event handler which reflows all Staggered zones on the page.
     */
    function createStaggerHandler($zones, options) {
        return function staggerHandler() {
            var $cols,
                $zoneElements,
                colHeights = [],
                shortestCol = 0,
                i,
                j,
                zonesLen,
                zoneElementsLen,
                count,
                $currentCount,
                $zone,
                $displayElement;

            for (i = 0, zonesLen = $zones.length; i < zonesLen; i++) {
                colHeights = [0];
                $zone = $zones.eq(i);
                $cols = jQuery(options.colSelector, $zone);

                /* cache original order */
                $zoneElements = $zone.data('staggered-zone');
                if ($zoneElements === undefined) {
                    if (options.elSelector === undefined || options.elSelector === null) {
                        $zoneElements = $cols.children();
                    } else {
                        $zoneElements = jQuery(options.elSelector, $zone);
                    }
                    $zone.data('staggered-zone', $zoneElements);
                }

                count = getColumnCountFromZoneWidth($zone, options);

                /* save the current column width on every resize event.*/
                $currentCount = $zone.data('data-col-count');

                if ($currentCount === undefined) {
                    /* the first time the page is loaded or after a page refresh $currentCount will not be set*/
                    $zone.data('data-col-count', count);

                    /* When pages loads up initially empty any desktop or tablet ads depending on current view width(2 cols or 3 cols)*/
                    if (count === 2) {
                        jQuery($zone).find('.zn-staggered__col .ad--companion.ad--desktop .js-adtag').next('span').empty().removeClass('js-adcontent');
                    }
                } else {
                    /* If currentCount is equal to the new count then we haven't reached a breakpoint */
                    if ($currentCount === count) {
                        /* Do nothing for this zone, if zone width hasn't crosssed a new breakpoint */
                        continue;
                    } else {
                        /* if going down from desktop to tablet mode remove the desktop ads */
                        if ((count === 2) && ($currentCount === 3)) {
                            /* Remove the desktop Ad of the page when going to tablet mode */
                            jQuery($zone).find('.zn-staggered__col .ad--companion.ad--desktop .js-adtag').next('span').empty().removeClass('js-adcontent');
                            jQuery($zone).find('.zn-staggered__col .ad--de.ad--desktop .js-adtag').next('span').empty().removeClass('js-adcontent');
                            jQuery($zone).find('.zn-staggered__col .cn-shingle .js-adtag').next('span').empty();
                        }

                        /* If going up from tablet to desktop mode */
                        if ((count === 3) && ($currentCount === 2)) {
                            /* adding js-content class for desktop */
                            jQuery($zone).find('.zn-staggered__col .ad--de.ad--desktop .js-adtag').next('span').empty().addClass('js-adcontent');
                            jQuery($zone).find('.zn-staggered__col .ad--companion.ad--desktop .js-adtag').next('span').empty().addClass('js-adcontent');
                            jQuery($zone).find('.zn-staggered__col .cn-shingle .js-adtag').next('span').empty();
                        }

                        /* Update column count */
                        $zone.data('data-col-count', count);
                    }
                }

                /* Set the columns to equal widths */
                renderColWidth(count, $cols, options, $zone);

                for (j = 0; j < count; j++) {
                    colHeights[j] = 0;
                }

                for (j = 0, zoneElementsLen = $zoneElements.length; j < zoneElementsLen; j++) {
                    shortestCol = findIndexOfMin(colHeights);
                    $displayElement = jQuery($zoneElements[j]).detach();
                    $cols.eq(shortestCol).append($displayElement);
                    colHeights[shortestCol] = colHeights[shortestCol] + getActualHeight($zoneElements.eq(j));
                }

                if (typeof NS.renderAds === 'function') {
                    NS.renderAds(jQuery($zone).find('.js-adtag'), '.js-adcontent',  {staggeredZone:true});
                }
            }
        };
    }

    /**
     * Function to position cards in a staggered zone.
     *
     * @description
     * Stagger cards to create a masonry-like appearnce on the
     * front end.
     *
     * By defining breakpoints and the number of columns to be rendered
     * accordingly, this function will move containers into the user-defined
     * number of columns.
     *
     * By default, the columns will be set to equal width, but the colWidths
     * option will allow for an aray of custom widths. The lenght of this array
     * must be equal to the count integer.
     *
     * @param {string} zones - jQuery formatted string selector for the zones.
     * @param {string} options - Custom options for the instance.
     *
     * @example
     * CNN.StaggeredZone('.zone',
     *     {
     *         breakpoints:
     *             [
     *                 {count: 1, query: { maxWidth: 679,  minWidth: 0 } },
     *                 {count: 3, query: { maxWidth: Number.POSITIVE_INFINITY, minWidth: 680 }, colWidths: ['40%', '30%', '30%'] }
     *             ],
     *         colSelector: '.col',
     *         elselector: '.el' // [optional - defaults to children of `colSelector`]
     *     }
     *
     */
    NS.StaggeredZone = function (zones, options) {
        var $zones = jQuery(zones),
            staggerHandler = createStaggerHandler($zones, options),
            delayTimeout = null;

        if ($zones === undefined || options === undefined) { return; }

        NS.StaggeredZone.columnResizeEvent = NS.StaggeredZone.columnResizeEvent || jQuery.Event('resize.staggeredZoneWidth');

        /* A function to delay execution of callback until callback hasn't fired for over 100ms */
        function delay(callback) {
            window.clearTimeout(delayTimeout);
            delayTimeout = window.setTimeout(callback, 100);
        }

        jQuery(window).resize(function () { delay(staggerHandler); });

        staggerHandler();
    };

    jQuery(document).onZonesAndDomReady(function () {
        NS.StaggeredZone('.zn-staggered .l-container .zn__containers', {
            breakpoints: [
                {count: 1, query: {maxWidth: 639, minWidth: 0}},
                {count: 2, query: {maxWidth: 959, minWidth: 640}},
                {count: 3, query: {maxWidth: Number.POSITIVE_INFINITY, minWidth: 960}}
            ],
            colSelector: '.zn-staggered__col'
        });
    });

}(CNN));

/* global CNN */

/* Sets the global MSIB configuration for this site. */

(function () {
    'use strict';

    if (typeof CNN.accounts === 'object' && CNN.accounts !== null &&
        typeof CNN.accounts.localCfg === 'object' && CNN.accounts.localCfg !== null) {

        return;  /* Already initialized */
    }

    CNN.SocialConfig = CNN.SocialConfig || {};
    CNN.SocialConfig.avatar = CNN.SocialConfig.avatar || {};
    CNN.SocialConfig.gigya = CNN.SocialConfig.gigya || {};
    CNN.SocialConfig.livefyre = CNN.SocialConfig.livefyre || {};
    CNN.SocialConfig.msib = CNN.SocialConfig.msib || {};

    CNN.accounts = CNN.accounts || {};
    CNN.accounts.localCfg = CNN.accounts.localCfg || {};

    CNN.accounts.localCfg.avatar = CNN.accounts.localCfg.avatar || {};
    CNN.accounts.localCfg.avatar.host = CNN.SocialConfig.avatar.host;

    CNN.accounts.localCfg.gigya = CNN.accounts.localCfg.gigya || {};
    CNN.accounts.localCfg.gigya.appId = CNN.SocialConfig.gigya.appId;
    /* This doesn't change between environments. */
    CNN.accounts.localCfg.gigya.shareButtons = 'Facebook,Twitter,Share';

    CNN.accounts.localCfg.livefyre = CNN.accounts.localCfg.livefyre || {};
    CNN.accounts.localCfg.livefyre.network = CNN.SocialConfig.livefyre.network;
    CNN.accounts.localCfg.livefyre.networkName = CNN.SocialConfig.livefyre.networkName;
    CNN.accounts.localCfg.livefyre.siteId = CNN.SocialConfig.livefyre.siteId;
    CNN.accounts.localCfg.livefyre.srcDomain = CNN.SocialConfig.livefyre.srcDomain;
    CNN.accounts.localCfg.livefyre.tokenEndpoint = CNN.SocialConfig.livefyre.tokenEndpoint;
    CNN.accounts.localCfg.livefyre.pingEndpoint = CNN.SocialConfig.livefyre.pingEndpoint;

    CNN.accounts.localCfg.accounts = CNN.accounts.localCfg.accounts || {};
    /* This doesn't change between environments. */
    CNN.accounts.localCfg.accounts.loginUrl = '/login.html';
}());

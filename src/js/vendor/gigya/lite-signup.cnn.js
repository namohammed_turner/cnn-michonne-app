(function (win, doc) {
    'use strict';

    function setupLiteReg() {

        if(!win.gigya || win.gigya._staticPasswordEnhancement) {
            return;
        }
        win.gigya._staticPasswordEnhancement = true;


        // Configuration:

        // Append to all new users as data.Turner_Brand.
        var TURNER_BRAND = 'CNN';


        // Enhanced registration function that will fallback to login if user exists.
        var gigya_register = gigya.accounts.register;
        win.liteSignup = function liteSignup(params) {
            var key;

            params = gigya.utils.object.merge([gigya.thisScript.globalConf, arguments]);

            // Auto-generate regToken if necessary.
            // This allows the method to be called without first calling initRegistration.
            // Additionally this is called when the reg token expires.
            if(!params.regToken) {
                gigya.accounts.initRegistration({
                    callback: function(res) {
                        params.regToken = res.regToken;
                        liteSignup(params);
                    }
                });
                return;
            }

            // Abort lite signup if password was passed.
            if(params.password) {
                return gigya_register.call(this, params);
            }

            // Assign static password.
            params.password = 'auto-generated-password';

            // Initialize data object because it may not have been passed.
            if(!params.data) {
                params.data = {};
            }

            // Add Turner_Brand to new registrations.
            if(!params.data.Turner_Brand) {
                params.data.Turner_Brand = TURNER_BRAND;
            }

            // Ensure registration is finalized.
            params.finalizeRegistration = true;

            // All lite signup forms have copy that says you are agreeing to terms.
            params.data.terms = true;

            // In string to support both string and boolean data types.
            params.data.lite = 'true';

            // One account may have multiple CIDs.
            params.data.cid = [params.cid];

            // Prefix email with __lite__ to prevent conflict w/ "normal" users.
            params.email = '__lite__' + params.email;

            // Allow shortcut to set newsletters based on boolean.
            if(params.newsletters) {
                if(!params.data.newsletters) {
                    params.data.newsletters = {};
                }
                var now = (new Date()).toISOString();
                var n;
                for(key in params.newsletters) {
                    n = params.newsletters[key];
                    if(n === true || n === false) {
                        params.data.newsletters[key] = {
                            newsletter_sts: n ? 1 : 4,
                            create_dte: now,
                            last_update_dte: now
                        };
                    }
                }
            }

            var cb = params.callback;
            params.callback = function(response) {
                // Deal with registration token expiration.
                if(response && response.errorDetails === 'regToken has been revoked') {
                    delete params.regTokens;
                    liteSignup(params);
                    return;
                }

                // If registration fails because email already exists:
                if(response && response.validationErrors && response.validationErrors[0].message === 'email already exists') {
                    gigya.accounts.login({
                        loginID: params.email,
                        password: params.password,
                        callback: function(res) {
                            // If the login failed, return the original failure from registration.
                            if(res.errorCode !== 0) {
                                return cb(response);
                            }

                            // Data to set after login.
                            var data = {
                                newsletters: params.data.newsletters
                            };

                            // Add cid to list if not present.
                            if(params.cid) {
                                if(!res.data || !res.data.cid) {
                                    // No CID attached to account previously.
                                    data.cid = [params.cid];
                                } else  {
                                    if(typeof res.data.cid === 'string') {
                                        // String CID attached previously. Convert to array.
                                        data.cid = [res.data.cid, params.cid];
                                    } else if(Object.prototype.toString.call(res.data.cid) === '[object Array]') {
                                        // Check if in array. CID cannot contain comma.
                                        if((res.data.cid.join(',') + ',').indexOf(params.cid + ',') === -1) {
                                            res.data.cid.push(params.cid);
                                            data.cid = res.data.cid;
                                        }
                                    }
                                }
                            }

                            // Preserve create date from newsletters that were already created.
                            if(res.data && res.data.newsletters) {
                                for(key in res.data.newsletters) {
                                    if(res.data.newsletters[key].create_dte && data.newsletters[key]) {
                                        data.newsletters[key].create_dte = res.data.newsletters[key].create_dte;
                                    }
                                }
                            }

                            // Append data to account.
                            if(data.cid || data.newsletters) {
                                gigya.accounts.setAccountInfo({
                                    data: data
                                });
                            }

                            // Return success response if login succeeded.
                            // TODO: This returns old version of the newsletters and cid data.
                            //       No one has a use case for needing it up to date currently, so no need for the extra API call.
                            return cb(res);
                        }
                    });
                    return;
                }

                if(cb) {
                    cb(response);
                }
            };

            // Pass through to original function.
            gigya_register.call(this, params);
        };


        // Helpers:

        if (!Date.prototype.toISOString) {
            function pad(number) {
                if (number < 10) {
                    return '0' + number;
                }
                return number;
            }
            Date.prototype.toISOString = function() {
                return this.getUTCFullYear() +
                    '-' + pad(this.getUTCMonth() + 1) +
                    '-' + pad(this.getUTCDate()) +
                    'T' + pad(this.getUTCHours()) +
                    ':' + pad(this.getUTCMinutes()) +
                    ':' + pad(this.getUTCSeconds()) +
                    '.' + (this.getUTCMilliseconds() / 1000).toFixed(3).slice(2, 5) +
                    'Z';
            };
        }
    }

    /**
     * Allows loading of this library after gigya is loaded on the page
     * added by MMorrison, if this file is every updated this added paradigm should likely stay in-place
     *
     */
    jQuery(doc).onZonesAndDomReady(function () {
        if (CNN.Utils.existsObject(CNN.accounts) && typeof CNN.accounts.onInitialized === 'function') {
            CNN.accounts.onInitialized(function () {
                CNN.accounts.loadGigya(setupLiteReg);
            });
        }
    });

})(window, document);

(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
/* aspen/aspen.js */

// Load dependent modules
var createAspenInstance = require('./core');


// Create a singleton -- all CVP players share the same Aspen instance.
var Aspen = window.Aspen = createAspenInstance();

// Expose option to create a separate Aspen instance for AuthLib.
Aspen.createInstance = createAspenInstance;

Aspen.getSessionToken = function () {
	return 'deprecated';
};


// Export public APIs
module.exports = Aspen;

},{"./core":5}],2:[function(require,module,exports){
/* aspen/aspencomm.js */

// Load dependent modules
var logger = require('./log');
var Request = require('../cvp/request');
var Utils = require('../cvp/utils');
var Convert = require('../cvp/convert');


var MAX_QUEUE_LENGTH = 500;
var MAX_RETRIES = 3;
var inited = false;
var queue = [];

var requeue = function (arrayOrArgs) {
	// re-insert messages post-failure
	// but first, make sure the queue isn't too large
	if (queue.length > MAX_QUEUE_LENGTH) {
		queue.splice(0, MAX_QUEUE_LENGTH - queue.length);
	}
	if (Utils.isArray(arrayOrArgs) && arrayOrArgs.length) {
		return queue.unshift.apply(queue, arrayOrArgs);
	}
	return queue.push(arrayOrArgs);
};

var makeRequest = function (options, callback, errback) {
	logger.log("AspenComm::makeRequest", options);

	var success = function (json) {
		logger.log("AspenComm::makeRequest", "request success", json);

		var obj = Convert.jsonTextToJsObject(json);

		callback(obj);
	};

	var failure = function (response) {
		logger.debug("AspenComm::makeRequest", "request failure", response);

		if (options._retries > MAX_RETRIES) {
			errback(options.url);
		} else {
			options._retries += 1;
			requeue(arguments);
		}
	};

	options._retries = '_retries' in options ? options._retries + 1 : 0;

	var req = new Request(options);

	req.then(success, failure);

	return req;
};

var queueRequest = function () {
	queue.push(arguments);
};

var dequeue = function () {
	// take a chunk of the queue to process
	var arrayOfArgs = queue.splice(0, queue.length);
	var args = null;

	while ((args = arrayOfArgs.shift()) != null) {
		makeRequest.apply(null, args).otherwise(Utils.bind(requeue, null, args));
	}
};

var request = function (config, succ, fail) {
	if (inited) {
		makeRequest(config, succ, fail);
	} else {
		queueRequest(config, succ, fail);
	}
};

var init = function () {
	logger.log("AspenComm::init");

	inited = true;
	init = function () { throw new Error("already init'ed - ignored"); };
	dequeue();
};


// Export public APIs
exports.init = init;
exports.request = request;

},{"../cvp/convert":14,"../cvp/request":36,"../cvp/utils":50,"./log":6}],3:[function(require,module,exports){
/* aspen/batchservice.js */

// Load dependent modules
var Class = require('../vendor/Class');
var Utils = require('../cvp/utils');
var Shared = require('../cvp/shared');
var logger = require('./log');
var TransportManager = require('./transportmanager');


var BATCH_MODE_TOTAL = "total";
var BATCH_MODE_TIMER = "timer";
var BATCH_MODE_NONE = "none";

var allUrlEncodedSpaces = /%20/g;

/**
 * Handles collecting messages into an array
 * to send to Aspen servers.
 */
var BatchService = Class.extend({

	init : function (aspenInstance)
	{
		this._enabled = false;
		this._queue = [];
		this._postThrottled = true;
		this._aspen = aspenInstance || window.Aspen;
		this._timer = null;
		this._postTimerEvent = Utils.bind(this._postTimerEvent, this);
		this._firstPostTimerEvent = Utils.bind(this._firstPostTimerEvent, this);
		this.dispose = Utils.bind(this.dispose, this);

		Shared.addBeforeUnLoadEvent(this.dispose);
	},

	dispose : function ()
	{
		this.disableTimer();
		this.post();
	},

	start : function () {
		var batchApi = this._aspen.getConfig("batchApi");

		if (!batchApi) {
			return;
		}

		this._enabled = true;

		if (this._aspen.getConfig("batchMode") === BATCH_MODE_TIMER)
		{
			var batchModeAmount = Utils.toInt(this._aspen.getConfig("batchModeAmount"));
			var random = Math.ceil(Math.random() * batchModeAmount);
			if (random === batchModeAmount)
			{
				random -= 1;
				if (random <= 0)
				{
					random = 0.5;
				}
			}
			logger.log("BatchService::start", "random time: " + random);
			setTimeout(this._firstPostTimerEvent, random * 1000);
		}
	},

	add : function (message)
	{
		if (!this._aspen.getConfig("enabled"))
		{
			logger.debug("BatchService::add", "Aspen is disabled via aspenanalytics config. Message not added.");
			return;
		}

		logger.debug("BatchService::add", "Adding message to batch queue.");

		// TODO max queue size?
		this._queue.push(message);

		if (!this._enabled) {
			logger.debug("BatchService::add", "Batch service not enabled. Queuing message.");
			return;
		}

		var batchMode = this._aspen.getConfig("batchMode");
		var batchModeAmount = this._aspen.getConfig("batchModeAmount");

		if (batchMode === BATCH_MODE_TIMER && this._timer === null)
		{
			logger.debug("BatchService::add", "Starting timer on message-add.");
			this._timer = setTimeout(this._postTimerEvent, batchModeAmount * 1000);
		}
		else if (batchMode === BATCH_MODE_TOTAL)
		{
			if (this._queue.length >= batchModeAmount)
			{
				this.post();
			}
		}
		else if (batchMode === BATCH_MODE_NONE)
		{
			this.post();
		}
		else if (batchMode !== BATCH_MODE_TIMER)
		{
			this.post();
		}

		// Flush the batch based on prioritized errors
		if (!Utils.empty(this._queue))
		{
			if (message.eventType === "error")
			{
				this.flushByError(message.eventData);
			}
			else if (message.eventType !== "error" && !Utils.undef(this._aspen.getConfig("highPriorityEvents")))
			{
				this.flushByEvent(message.eventType);
			}
		}
	},

	post : function ()
	{
		if (this._postThrottled)
		{
			logger.debug("BatchService::post", "Batch posting is throttled. Batch not posted.");
			return;
		}

		if (!this._enabled)
		{
			logger.debug("BatchService::post", "Batch posting is not enabled.");
			return;
		}

		if (Utils.empty(this._queue))
		{
			logger.debug("BatchService::post", "Nothing to post.");
			return;
		}

		var data = {
			sessionId : this._sessionId,
			events :    this._queue.slice(0)
		};

		// Send batch string
		logger.debug("BatchService::post", "send batch string");

		if (this._aspen.getConfig("batchContentEncoding") === "gzip")
		{
			var jsonString = "?aspenJson=" + JSON.stringify(data);
			data = encodeURIComponent(jsonString).replace(allUrlEncodedSpaces, "+");
		}

		TransportManager.createRequest(this._aspen.getConfig("batchApi"), data, this._postSuccess, this._postFailure, this);

		// Clear the batch
		this._queue.length = 0;
	},

	_postTimerEvent : function ()
	{
		if (Utils.empty(this._queue))
		{
			logger.log("BatchService::_postTimerEvent", "no messages to post");
			this._timer = null;
		}
		else
		{
			logger.log("BatchService::_postTimerEvent", "queue.length: " + this._queue.length);
			this.post();
			this._timer = setTimeout(this._postTimerEvent, this._aspen.getConfig("batchModeAmount") * 1000);
		}
	},

	_firstPostTimerEvent : function ()
	{
		if (Utils.empty(this._queue))
		{
			logger.log("BatchService::_firstPostTimerEvent", "no messages to post");
		}
		else
		{
			logger.log("BatchService::_firstPostTimerEvent", "queue.length: " + this._queue.length);
			this.post();
		}
	},

	_postSuccess : function (data)
	{
		logger.log("BatchService::_postSuccess", data);
	},

	_postFailure : function (url)
	{
		logger.log("BatchService::_postFailure", url);
	},

	flushByError : function (message)
	{
		var highPriorityErrors = this._aspen.getConfig("highPriorityErrors");

		if (Utils.isString(message.errorCode) && Utils.isString(highPriorityErrors))
		{
			var errorCode = message.errorCode;
			var errorCodeType = errorCode.substring(0, 2);
			var errorCodeSub = errorCode.substring(2, 4);
			var errorCodeLevel = errorCode.substring(4, 6);
			var errorCodeNumber = errorCode.substring(6);

			var priorityArray = highPriorityErrors.split(/[, ]+/);
			var typeStr = "";
			var subtypeStr = "";
			var levelStr = "";
			var codeNumberStr = "";

			var types = [];
			var subtypes = [];
			var levels = [];
			var codeNumbers = [];

			for (var i = 0, endi = priorityArray.length; i < endi; ++i)
			{
				switch (i)
				{
					case 0:
						typeStr = priorityArray[i];
						types = typeStr.split("+");
						break;
					case 1:
						subtypeStr = priorityArray[i];
						subtypes = subtypeStr.split("+");
						break;
					case 2:
						levelStr = priorityArray[i];
						levels = levelStr.split("+");
						break;
					case 3:
						codeNumberStr = priorityArray[i];
						codeNumbers = codeNumberStr.split("+");
						break;
				}
			}

			for (i = 0, endi = types.length; i < endi; ++i)
			{
				var type = types[i];
				if ((errorCodeType === type) || (type === "*"))
				{
					for (var j = 0, endj = subtypes.length; j < endj; ++j)
					{
						var subtype = subtypes[j];
						if ((errorCodeSub === subtype) || (subtype === "*"))
						{
							for (var k = 0, endk = levels.length; k < endk; ++k)
							{
								var level = levels[k];
								if ((errorCodeLevel === level) || (level === "*"))
								{
									for (var l = 0, endl = codeNumbers.length; l < endl; ++l)
									{
										var codeNumber = codeNumbers[l];
										if ((errorCodeNumber === codeNumber) || (codeNumber === "*"))
										{
											logger.debug("BatchService::flushByError", "found match, flushing batch");
											this.post();
										}
									}
								}
							}
						}
					}
				}
			}
		}
	},

	flushByEvent : function (messageType)
	{
		var highPriorityEvents = this._aspen.getConfig("highPriorityEvents");

		if (Utils.isString(highPriorityEvents))
		{
			var priorityArray = highPriorityEvents.split(/[, ]+/);

			for (var i = 0, endi = priorityArray.length; i < endi; ++i)
			{
				var type = priorityArray[i];
				if (type === messageType)
				{
					logger.debug("BatchService::flushByEvent", "found match, flushing batch");
					this.post();
				}
			}
		}
	},

	setSessionId : function (id)
	{
		this._sessionId = id;
	},

	setPostThrottled : function (throttle)
	{
		this._postThrottled = throttle;
	},

	disableTimer : function ()
	{
		if (this._aspen.getConfig("batchMode") === BATCH_MODE_TIMER)
		{
			clearTimeout(this._timer);
		}
	}
});


// Export public APIs
module.exports = BatchService;

},{"../cvp/shared":37,"../cvp/utils":50,"../vendor/Class":93,"./log":6,"./transportmanager":7}],4:[function(require,module,exports){
/* aspen/configfile.js */

// Load dependent modules
var Dependency = require('../cvp/util/dependency');
var logger = require('./log');
var Utils = require('../cvp/utils');
var Request = require('../cvp/request');


var jsonpCallbackRegex = /\b(\S+)\(([\s\S]+)\)/;

function unwrapJsonp(success, failure) {
	return function handleJsonp(jsonpString) {
		if (!jsonpString) {
			return failure('Unsuccessful attempt to obtain JSONP');
		}

		var match = jsonpCallbackRegex.exec(jsonpString);

		if (!match) {
			return failure('Failed to determine JSONP callback name in response');
		}

		var callbackName = match[1];

		try {
			var jsonpFn = new Function(callbackName, jsonpString);
			// function jsonpFn(callbackName) {
			// 	callbackName && callbackName(json);
			// }

			try {
				jsonpFn(function (json) {
					success(json);
				});
			}
			catch (callbackException) {
				failure(callbackException);
			}
		}
		catch (fnCreationException) {
			failure(fnCreationException);
		}
	};
}


var ConfigFileDependency = Dependency.extend({
	init : function (options) {
		var basePath = options.basePath || '.';

		logger.log("Using config base path: " + basePath);

		var timeBucket = 1000 * 60 * 15;
		var cacheBustValue = (Math.round(Utils.now() / timeBucket)).toString(36);
		var analyticsConfigUrl = basePath + "/aspenanalytics.json?_=" + cacheBustValue;

		logger.log("config url: ", analyticsConfigUrl);

		this._super(analyticsConfigUrl, false);
	},

	load : function () {
		var success = Utils.bind(this._loadConfigSuccess, this);
		var failure = Utils.bind(this._loadConfigFailure, this);

		Request.get({
			url : this._assetUrl
		}).then(unwrapJsonp(success, failure), failure);
	},

	_loadConfigSuccess : function (data) {
		logger.log("Config loaded successfully");
		this._success(data, this._assetUrl);
	},

	_loadConfigFailure : function (url) {
		logger.log("Config failed to load", url);
		this._failure(url);
	},

	getDesc : function () {
		return "Aspen ConfigDependency: " + this._assetUrl;
	}
});


// Export public APIs
module.exports = ConfigFileDependency;

},{"../cvp/request":36,"../cvp/util/dependency":44,"../cvp/utils":50,"./log":6}],5:[function(require,module,exports){
/* aspen/core.js */

// Load dependent modules
var App = require('../core/app');
var Utils = require('../cvp/utils');
var Event = require('../cvp/customevent');
var Events = require('../cvp/events');
var Browser = require('../cvp/browser');
var logger = require('./log');
var BatchService = require('./batchservice');
var ConfigFileDependency = require('./configfile');
var TransportManager = require('./transportmanager');


var CVP_ANALYTICS_BASE_URL = App.CDN_ORIGIN + "/xslo/cvp/config/";
var ASPEN_ANALYTICS_BASE_URL = App.CDN_ORIGIN + "/xslo/aspen/config/";

var LoadState = {
	WAITING : "loadStateWaiting",
	LOADING : "loadStateLoading",
	READY :   "loadStateReady",
	FAILED :  "loadStateFailed"
};

// lowercased list of reserved keywords disallowed from Aspen messages
var reservedKeywords = [
	'timestamp',
	'aspenconfigurl',
	'applicationname',
	'session_id',
	'sessionid',
	'hbase_timestamp',
	'type'
];

/**
 * Constructor for a new Aspen object.
 * @param {Object} options
 *   - aspenAppSignature: {Object} key/values to override default aspenAppSignature
 * @returns {Object} Aspen instance.
 */
var createAspenInstance = function (options) {

	var aspenAppSignature = {
		applicationName : "cvp",
		applicationVersion : App.VERSION,
		context : "default",
		deviceModel : "desktop",
		deviceType : "desktop",
		deviceVersion : "desktop",
		platformName : "javascript",
		platformVersion : Browser.version ? String(Browser.version) : undefined
	};

	if (typeof options === 'object') {
		if ('aspenAppSignature' in options) {
			aspenAppSignature = Utils.extend({}, aspenAppSignature, options.aspenAppSignature);
		}
		if ('useSharedSessionToken' in options) {
			logger.warn('"useSharedSessionToken" support is deprecated and is no longer applicable!');
		}
	}

	return ({
		initialized : false,
		ready : false,
		config : {
			throttled : true,
			timeDifference : 0,
			progressReportInterval : 37,  // seconds
			adProgressReportInterval : 17  // seconds
		},
		batchService : null,
		sessionObject : {},
		serviceUrlRoot : "",
		_retryTimer : null,
		_retries : 0,
		_retriesMade : 0,
		_retryTime : 0,
		_savedSession : {},
		_messageQueue : [],
		_loadState : LoadState.WAITING,
		eInitSuccess : new Event('AspenInitSuccess'),
		eInitFailure : new Event('AspenInitFailure'),

		// methods
		/**
		 * Aspen.init(options)
		 *
		 * @param {Object} options
		 *   - configBaseUrl: (optional) path of directory containing aspenanalytics.(xml|json) [default: ANALYTICS_BASE_URL/{site}]
		 *   - site: CVP-approved site value [default: CVP.init({site: value}) or 'cvp']
		 *   - context: which context in aspenanalytics.(xml|json) to use
		 */
		init : function (options, initSuccessCallback)
		{
			var self = this;

			if (this.initialized) {
				logger.warn("Aspen is already set up!");
				return;
			}

			// bind methods called out of scope
			this._helloRetry = Utils.bind( this._helloRetry, this );

			// initialize member vars
			this.initialized = true;

			this._options = Utils.extend({}, options);

			// Providing defaults if param is missing
			this._options.site = this._options.site || (App._cvpsetup && App._cvpsetup.site) || 'cvp';

			// verify the site
			if (Utils.empty(this._options.site))
			{
				logger.error("Unable to start Aspen, no site provided");
				return;
			}

			// Augment aspenAppSignature
			if ('appName' in this._options) {
				aspenAppSignature.applicationName = this._options.appName;

				if ('appVersion' in this._options) {
					aspenAppSignature.applicationVersion = this._options.appVersion;
				}
			}

			// Initialize the batch service - allow it to potentially queue
			// messages while Aspen is initializing
			this.batchService = new BatchService(this);

			// Loading the config file
			this._loadState = LoadState.LOADING;

			if (Utils.isFunction(initSuccessCallback)) {
				this.eInitSuccess.addListener(initSuccessCallback);
			}

			// Wait until onReady to load any dependencies via JSONP, to
			// prevent IE from complaining in the op-aborted style.
			Events.onReady(function () {
				// Load all required dependencies before making the hello
				var basePath = self._options.configBaseUrl
					|| ('appName' in self._options
						? ASPEN_ANALYTICS_BASE_URL + self._options.appName + '/' + self._options.site
						: CVP_ANALYTICS_BASE_URL + self._options.site
				);
				var dependency = self._dependency = new ConfigFileDependency({
					basePath : !self._options.dt && basePath
				});

				dependency.eSuccess.addListener( self._onConfigLoaded, self );
				dependency.eFailure.addListener( self._onConfigLoadFailure, self );
				dependency.load();
			});
		},

		_freeDependency : function ()
		{
			if (!this._dependency) return;

			this._dependency.eSuccess.removeListener(this._onConfigLoaded, this);
			this._dependency.eFailure.removeListener(this._onConfigLoadFailure, this);
			this._dependency = null;
		},

		dispose : function ()
		{
			this._freeDependency();
			clearTimeout(this._retryTimer);
			this.batchService.dispose();
			this.batchService = null;
		},

		// handle results from config download
		_setupConfig : function (data, url) {

			// parse the config
			// (Merge to new object so as to not mutate existing objects.)
			var config = Utils.extend(
				{},
				data['default'],
				('context' in this._options ? data[this._options.context] : null)
			);

			config.enabled              = config.enabled === 'true' && !Utils.empty(config.servicesUrl);
			config.batchMode            = config.batchMode || 'total';
			config.batchModeAmount      = config.batchModeAmount && Number(config.batchModeAmount) || 10;  // 0 is falsey and invalid
			config.helloRetryCount      = !isNaN(config.helloRetryCount = Number(config.helloRetryCount)) ? config.helloRetryCount : 5;  // 0 is falsey, but valid
			config.helloRetryTime       = config.helloRetryTime && Number(config.helloRetryTime) || 10000;  // 0 is invalid
			config.batchContentEncoding = config.batchContentEncoding || 'json';
			config.messageExcludes      = Utils.isString(config.messageExcludes) && config.messageExcludes.split(/[, ]+/) || [];
			config.transportFormats     = config.transportFormats && config.transportFormats.split(/[, ]+/) || TransportManager.getDefaultFormats();
			config.propertyName         = config.propertyName || this._options.site;
			config.aspenUserIdEnabled   = config.aspenUserIdEnabled === 'true';
			config.aspenUserIdHash      = config.aspenUserIdHash || '392aspenUID392';

			Utils.print(config, "Aspen config");

			this.config = config;
			this.configSourceUrl = url;

			// Spawn immediate message with the contents of the config.
			this.send("aspenConfigDetails", this.config);
		},

		getConfig : function (config) {
			if (config in this.config) {
				return this.config[config];
			}
		},

		// more setup after config-load
		_onConfigLoaded : function (data, url) {
			this._freeDependency();
			this._setupConfig(data, url);
			this._loadState = LoadState.READY;
			logger.log("_onConfigLoaded - config successfully loaded");

			// make the hello call
			if (this.config.enabled)
			{
				logger.log("analytics enabled via config");

				// Avoid mixed content browser errors:
				// If page is served via 'https', ensure Aspen service URL is 'https'.
				var servicesUrl = this.config.servicesUrl;
				var pageProtocol = window.location.protocol;
				var secureProtocol = 'https:';
				var insecureProtocol = 'http:';
				var pageServedSecurely = pageProtocol === secureProtocol;
				var servicesUrlIsInsecure = servicesUrl.indexOf(insecureProtocol) === 0;

				if (pageServedSecurely && servicesUrlIsInsecure) {
					servicesUrl.replace(insecureProtocol, pageProtocol);
				}

				TransportManager.init({
					transportFormats : this.config.transportFormats,
					servicesUrl : servicesUrl
				});

				this.serviceUrlRoot = servicesUrl;
				this._hello();
			}
			else
			{
				logger.log("analytics disabled via config");
				this._initFailure();
			}
		},

		_onConfigLoadFailure : function (url) {
			this._freeDependency();
			this._loadState = LoadState.FAILED;
			logger.log("_onConfigLoadFailure - failed to load " + url);
		},

		_hello : function ()
		{
			logger.log("hello");

			// (Merge to new object so as to not mutate existing objects.)
			var data = Utils.extend(
				{},
				aspenAppSignature,
				{
					aspenConfigUrl : this.configSourceUrl,
					propertyName : this.config.propertyName,
					pageUrl : document.URL,
					context : this._options.context,
					protocol : "1.0"
				}
			);

			if (this._options.dt) {
				data.debug = "true";
			}

			if (this.config.aspenUserIdEnabled) {
				data.aspenUUIDToken = this.config.aspenUserIdHash;
			}

			TransportManager.bootstrapTM();
			TransportManager.createRequest(this.config.helloApi, data, this._helloSuccess, this._helloFailure, this);

			this._retryTimer = setTimeout(this._helloRetry, this.config.helloRetryTime);
		},

		/**
		 * @param {Object} config - parsed JSON response from hello call
		 *   - "throttled": <boolean value either "yes" or "no">,
		 *   - "sessionId": <128-bit server-generated session ID, not included in response when throttled == "yes">,
		 *   - "timestamp": <server-side timestamp, specified in milliseconds since the epoch>
		 */
		_helloSuccess : function (config)
		{
			logger.log("_helloSuccess");

			clearTimeout(this._retryTimer);

			this.config.throttled = Utils.isString(config.throttled) ? config.throttled !== 'no' : this.config.throttled;
			// this.config.sessionId = config.sessionId || this.sessionId;  // config.sessionId cannot be 0, '', null
			this.config.sessionId = config.sessionId;
			this.config.timeDifference = config.timestamp ? config.timestamp - Utils.now() : this.config.timeDifference;
			this.config.progressReportInterval = config.progressReportInterval || this.config.progressReportInterval;  // config.progressReportInterval cannot be 0, '', null
			this.config.adProgressReportInterval = config.adProgressReportInterval || this.config.adProgressReportInterval;  // config.adProgressReportInterval cannot be 0, '', null

			logger.debug("throttled: " + config.throttled + " => " + this.config.throttled);
			logger.debug("sessionId: " + config.sessionId);
			logger.debug("timeDifference: " + this.config.timeDifference);
			logger.debug("progressReportInterval: " + this.config.progressReportInterval);
			logger.debug("adProgressReportInterval: " + this.config.adProgressReportInterval);

			// configure batch service
			this.batchService.setSessionId(this.config.sessionId);

			this.batchService.setPostThrottled(this.config.throttled);

			// start the batch service
			this.batchService.start();

			this._initSuccess();
		},

		_helloFailure : function (url)
		{
			logger.log("_helloFailure url: " + url);


			if (TransportManager.fallback())
			{
				this._hello();
			}
		},

		_helloRetry : function ()
		{
			logger.log("helloRetry");
			if (this._retries === this.config.helloRetryCount)
			{
				logger.log("retries exhasted");
				clearTimeout(this._retryTimer);
				this._retries = 0;

				if (TransportManager.fallback())
				{
					this._hello();
				}
				else
				{
					logger.debug("retries exhasted, fallbacks exhausted, aspen is disabled");
					this.config.throttled = true;
					this.batchService.setPostThrottled(true);
					this.batchService.disableTimer();
				}
			}
			else
			{
				this._retries += 1;
				this._retryTime = this._retries * this.config.helloRetryTime;
				this._savedSession.retries = this._retries;
				this._savedSession.retryTime = this._retryTime;
				this._hello();
			}
		},

		isMessageExcluded : function (messageType)
		{
			return Utils.indexOf(this.config.messageExcludes, messageType) !== -1;
		},

		send : function (p_type, p_message)
		{
			if (this.disabled) {
				logger.debug("send - Aspen is disabled, message was not sent");
				return;
			}

			var type = p_type;
			var message = p_message;

			if (Utils.isString(p_message)) {
				type = p_message;
				message = p_type;
				logger.debug("Signature change: Aspen.send(type, message)", type, message);
			}

			if (Utils.isNull(message)) {
				message = {};
			}
			else if (!Utils.isSimpleObject(message)) {
				logger.warn("send - message param must be a simple object");
				return;
			}

			// Remove reserved keywords.
			// Also, warn of duplicate keys of differing case.
			// Combining to save loops.
			var seen = {};
			var key;
			var lowerKey;
			for (key in message) {
				if (!Utils.hasOwn(message, key)) continue;
				lowerKey = key.toLowerCase();
				if (reservedKeywords.indexOf(lowerKey) !== -1) {
					delete message[key];
				}
				else if (lowerKey in seen) {
					logger.warn("JS is case-sensitive, but Aspen is not: the property '" + key + "' is duplicated!");
				}
				else {
					seen[lowerKey] = 1;
				}
			}

			// preserve timestamp of message-creation, if present
			message.timestamp = (message.timestamp || Utils.now()) + this.config.timeDifference;

			// Queue up any messages that come in before the hello call has returned
			// so the throttling and messageExcluded checks will evaluate properly
			if (!this.ready) {
				logger.debug("send - Aspen's not ready, queuing message");
				this._messageQueue.push({
					message : message,
					type : type
				});
				return;
			}

			if (this.config.throttled)
			{
				logger.debug("Add message to batch is throttled! Message not added.");
				return;
			}

			if (this.isMessageExcluded(type))
			{
				logger.debug("message excluded by config, not added to batch", type);
				return;
			}

			this._send(type, message);
		},

		_send : function (type, message)
		{

			switch (type) {
				case "appConfig":
					// Slip in the app signature to the appConfig message.
					// (Merge to new object so as to not mutate existing objects.)
					message = Utils.extend({}, aspenAppSignature, message);

					// Pass along that URL we grabbed earlier.
					message.aspenConfigUrl = this.configSourceUrl;
				break;
			}

			if ('applicationName' in aspenAppSignature && !('applicationName' in message)) {
				message.applicationName = aspenAppSignature.applicationName;
			}

			var batchObj = {};
			batchObj.eventType = type;
			batchObj.eventData = message;

			logger.log("add to batch json message: " + JSON.stringify(batchObj));

			this.batchService.add(batchObj);
		},

		_initSuccess : function ()
		{
			logger.log("Successful init");
			this.ready = true;

			// Send any messages that have been queued up
			for (var i = 0, len = this._messageQueue.length; i < len; ++i) {
				this.send(this._messageQueue[i].type, this._messageQueue[i].message);
			}
			this._messageQueue.length = 0;

			// once Aspen is initialized, get the system info
			this.send("systemInfo", {
				screenWidth : screen.width,
				screenHeight : screen.height,
				browserName : navigator.appName,
				userAgent : navigator.userAgent,
				platform : navigator.platform
			});

			this.eInitSuccess.dispatch();
		},

		_initFailure : function ()
		{
			logger.log("Failed init");
			this.disabled = true;

			this.eInitFailure.dispatch();
		}
	});
};


// Export public APIs
module.exports = createAspenInstance;

},{"../core/app":8,"../cvp/browser":12,"../cvp/customevent":16,"../cvp/events":26,"../cvp/utils":50,"./batchservice":3,"./configfile":4,"./log":6,"./transportmanager":7}],6:[function(require,module,exports){
/* aspen/log.js */

// Load dependent modules
var Log = require('../cvp/log');


var logger = Log.getLogger('Aspen');


// Export public APIs
module.exports = logger;

},{"../cvp/log":28}],7:[function(require,module,exports){
/* aspen/transportmanager.js */

// Load dependent modules
var AspenComm = require('./aspencomm');
var Utils = require('../cvp/utils');
var Request = require('../cvp/request');


var logger = require('./log');

/**
 * Abstracts the selection of the transport format.
 */
var TransportManager = {

	Format : {
		JSON :  "json",
		JSONP : "jsonp"
	},

	getDefaultFormats : function () {
		return [ TransportManager.Format.JSONP ];
	},

	init : function (config)
	{
		this._config = config;
		this._transportFormat = this._config.transportFormats.shift();  // TODO: support index?
		this._bootstrapped = false;
	},

	bootstrapTM : function ()
	{
		logger.log("TransportManager::bootstrapTM", "_bootstrapped:", this._bootstrapped);
		if (this._transportFormat === TransportManager.Format.JSON)
		{
			if (!this._bootstrapped)
			{
				var servicesUrl = this._config.servicesUrl;
				AspenComm.init(servicesUrl);

				this._bootstrapped = true;
				logger.log("TransportManager::bootstrapTM", "transportFormat:", this._transportFormat);
			}
		}
	},

	createRequest : function (request, data, success, failure, scope)
	{
		logger.log("TransportManager::createRequest");

		var url = this._config.servicesUrl + request + "." + this._transportFormat;
		var callback = Utils.bind(success, scope);
		var errback = Utils.bind(failure, scope);

		switch (this._transportFormat) {
			case TransportManager.Format.JSON:
				AspenComm.request(
					{
						url : url,
						method : 'POST',
						data : {
							aspenJson : data
						}
					},
					callback,
					errback
				);
			break;
			case TransportManager.Format.JSONP:
				Request.getJSONP({
					jsonpCallback : 'parseCVPServicesInitialization',
					url : url,
					data : {
						aspenJson : data
					},
					success : callback,
					failure : errback
				});
			break;
		}
	},

	fallback : function ()
	{
		logger.debug("TransportManager::fallback", "current transport format failed", this._transportFormat);

		this._transportFormat = this._config.transportFormats.shift();

		if (this._transportFormat)
		{
			logger.debug("TransportManager::fallback", "trying next transport format", this._transportFormat);
			return true;
		}

		return false;
	}

};


// Export public APIs
module.exports = TransportManager;

},{"../cvp/request":36,"../cvp/utils":50,"./aspencomm":2,"./log":6}],8:[function(require,module,exports){
/* core/app.js */

var debugRegex = /\bdmtdebug=(\w+)/;
var debugMatch = debugRegex.exec(window.location.href);

if (!debugMatch && window !== window.top) {
	try {
		debugMatch = debugRegex.exec(window.top.location.href);
	} catch (e) {
		// attempting to access data across domains could throw an error
	}
}

var isDebug = !!debugMatch;
var cdnOrigin = window.location.protocol + "//cvp1.cdn.turner.com";

var APPCONFIG = {

	VERSION : "2.8.10.0",
	BUILD_DATE : "2016-08-31T11:09:09-0400",
	FLASH_VERSION : "10.1.0.0",
	HTML5 : 'html5',
	FLASH : 'flash',

	CDN_ORIGIN : cdnOrigin,
	DEBUG : isDebug,
	DEBUG_LEVEL : isDebug ? debugMatch[1] : null,

	MAPPING_PATH : cdnOrigin + "/xslo/cvp/config/{site}/",
	MAPPING_FILE : "mapping.xml",

	OMNITURE_JS_URL : "",
	AD_MANAGER_URL : cdnOrigin + "/xslo/cvp/ads/freewheel/js/0/AdManager.js",
	AD_DFP_URL : "//www.google.com/uds/api/ima/1.7/9fd415db3bb953707e811e4827fe957b/default.IN.js",
	EXPRESS_INSTALL_URL : cdnOrigin + "/xslo/cvp/assets/flash/expressInstall.swf",
	FLASHXHR_SWF_URL : cdnOrigin + "/xslo/cvp/plugins/cvp/xhr/1.0/cvp_flashXhr.swf",
	LOG4JAVASCRIPT_URL : cdnOrigin + "/xslo/cvp/js/logging/log4javascript.js",
	EASYXDM_JS_URL : cdnOrigin + "/xslo/cvp/easyxdm/js/easyXDM.ugly.js",
	// EASYXDM_SWF_URL : cdnOrigin + "/xslo/cvp/easyxdm/swf/easyxdm.swf",
	BLANK_MP4_URL : cdnOrigin + "/xslo/cvp/assets/video/blank.mp4",
	BLANK_IMG_URL : cdnOrigin + "/xslo/cvp/assets/video/blank.png",
	EXPIRED_MP4_URL : cdnOrigin + "/xslo/cvp/assets/video/video_not_available.mp4",
	EXPIRED_IMG_URL : cdnOrigin + "/xslo/cvp/assets/video/video_not_available.png",

	ERROR_CMS_PARSE : "CVP0201",
	ERROR_CMS_IO : "CVP0202",
	ERROR_CMS_LOADER : "CVP0206",
	ERROR_CONTENT_EXPIRED : "CVP2052"

};

module.exports = APPCONFIG;

},{}],9:[function(require,module,exports){
/* cvp.js */

// Load dependent modules
var CVP = require('./cvp/main');

window.CVP = CVP;

},{"./cvp/main":29}],10:[function(require,module,exports){
/* cvp/ajax.js */

// Load dependent modules
var when = require('../vendor/when');
var Utils = require('./utils');
var Log = require('./log');


var log = Log.getLogger();

// Ajax, simple impl
var Ajax = {
	get : function(obj) {
		obj.method = "GET";
		return this._request(obj);
	},

	getXml : function(obj) {
		obj.type = "xml";
		return this.get(obj);
	},

	getJSON : function(obj) {
		var deferred = when.defer();
		var success = obj.success;

		obj.type = "json";
		obj.success = function (text) {
			try {
				var json = JSON.parse(text);
				deferred.resolve(json);
			} catch (e) {
				deferred.reject(e);
			}
		};

		this.get(obj);

		when(deferred.promise, success, obj.error);

		return deferred.promise;
	},

	getJSONP : function(obj) {
		return this.JSONP._request(obj);
	},

	post : function(obj) {
		obj.method = "POST";
		return this._request(obj);
	},

	_request : function(obj)
	{
		var xhr = new XMLHttpRequest();
		var deferred = when.defer();

		xhr.onreadystatechange = function()
		{
			if (xhr.readyState === 4)
			{
				if ((200 <= xhr.status && xhr.status < 300) || xhr.status === 304)
				{
					deferred.resolve(xhr.responseText);
				}
				else
				{
					deferred.reject(xhr);
				}
				xhr.onreadystatechange = Function.prototype;
				xhr.abort();
				xhr = null;
			}
		};

		when(deferred.promise, obj.success, obj.error);

		// Note - if true is not set here, mobile safari will throw INVALID_STATE_ERR
		xhr.open(obj.method, obj.url, true);
		xhr.send(obj.method === "POST" ? obj.data : null);

		// Return a Promise for later use.
		return deferred.promise;
	}
};

/**
 * Module to send a JSONP request
 */
Ajax.JSONP = {
	requestTimeout : 5000,
	maxRetries : 0,
	_externalRoutes : {},
	requestMap : {},
	_counter : 0,

	_request : function(obj) {
		obj = Utils.extend({
			timeout : this.requestTimeout,
			maxRetries : this.maxRetries
		}, obj);

		obj.tries = 0;
		obj.deferred = when.defer();

		log.debug("JSONP - creating request with timeout ", obj.timeout, " and maxRetries ", obj.maxRetries);

		this._sendRequest(obj);

		when(obj.deferred.promise, obj.success, obj.error);

		// Return a Promise for later use.
		return obj.deferred.promise;
	},

	_sendRequest : function(obj) {
		var self = this;

		var requestId = this._createCallback(obj);
		var script = this._createScript(requestId, obj.url);
		if (!script) {
			var errorMsg = "JSONP - unable to create script for request " + obj.url;
			log.error(errorMsg);
			obj.deferred.reject(obj.url);
			throw new Error(errorMsg);
		}

		if (obj.timeout) {
			setTimeout(function() {
				self._onError(requestId, obj);
			}, obj.timeout);
		}
	},

	/**
	 * The request was successfully received
	 * @param {String} id - The request id
	 * @param {Object} obj - The obj passed to the request
	 * @param {String} data - The JSON response string
	 */
	_onSuccess : function(id, obj, data) {
		if (!this.requestMap[id]) {
			return;
		}

		this._destroyRequest(id, obj);

		if (Utils.isString(data)) {
			try {
				data = JSON.parse(data);
			} catch (e) {
				log.error("JSON failed to parse", e);
			}
		}

		log.info("JSONP", "success", obj.url);
		obj.deferred.resolve(data);
	},

	/**
	 * The request was not received successfully
	 * @param {String} id - The request id
	 * @param {Object} obj - The obj passed to the request
	 */
	_onError : function(id, obj) {
		if (!this.requestMap[id]) {
			return;
		}

		this._destroyRequest(id, obj);

		// Retry the request if appropriate
		++obj.tries;
		if (obj.maxRetries && obj.tries < obj.maxRetries) {
			log.warn("Request failed - resending");
			this._sendRequest(obj);
			return;
		}

		log.error("JSONP", "error", obj.url);
		obj.deferred.reject(obj.url);
	},

	/**
	 * Destroy all artifacts associated with the request
	 * @param {String} id - The request id
	 * @param {Object} obj - The obj passed to the request
	 */
	_destroyRequest : function(id, obj) {
		this._deregisterExternalCallback(obj.jsonpCallback, id);
		delete this.requestMap[id];
		this._counter--;

		var script = document.getElementById(this._getScriptName(id));
		if (script) {
			script.parentNode.removeChild(script);
			script = null;
		}
	},

	/**
	 * Create the script tag to be used for the request
	 * @param {String} id - The request id
	 * @param {String} url - The script URL to load
	 * @returns {Element} The <script> element added to page.
	 */
	_createScript : function(id, url) {
		var tagName = "script";
		var first = document.getElementsByTagName(tagName)[0];
		var script = document.createElement(tagName);

		script.type = "text/javascript";

		if (id)
			script.id = this._getScriptName(id);

		script.src = url;
		script.async = true;

		first.parentNode.insertBefore(script, first);

		return script;
	},

	_getScriptName : function(id) {
		return "cvp_jsonp_" + id;
	},

	/**
	 * Create the global callback hook for the request
	 * @param {Object} obj - The request configuration.
	 * @returns {String} The request ID.
	 */
	_createCallback : function(obj) {
		var self = this;

		if (Utils.empty(obj.jsonpCallback)) {
			return null;
		}

		++this._counter;
		var id = "request_" + this._counter + "_" + Utils.now();
		this.requestMap[id] = function(data) {
			self._onSuccess(id, obj, data);
		};

		// Keep the external and internal callbacks separate for now, for flexibility
		var cb = function(data) {
			log.debug("JSONP", "Internal CB received for request:", id);
			if (self.requestMap[id]) {
				self.requestMap[id](data);
			} else {
				log.debug("Request response came in after allotted time: ", id);
			}
		};
		this._registerExternalCallback(obj.jsonpCallback, id, cb);

		return id;
	},

	_registerExternalCallback : function(name, id, cb) {
		if (!window[name])
			this._createExternalCallback(name, this._routeExternalCallback(name));

		if (!this._externalRoutes[name])
			this._externalRoutes[name] = [];

		this._externalRoutes[name].push({ id : id, cb : cb });

	},

	_routeExternalCallback : function(name) {
		var self = this;
		return function(data) {
			log.info("JSONP", "routing:", name);
			if (!self._externalRoutes) {
				log.error("JSONP", "routes haven't been initialized");
				return;
			}
			if (!self._externalRoutes[name]) {
				log.error("JSONP", "routing hasn't been initialized for:", name);
				return;
			}
			if (!self._externalRoutes[name].length) {
				log.error("JSONP", "no waiting requests for route:", name);
				return;
			}

			var request = self._externalRoutes[name].shift();
			log.info("JSONP", "request retrieved - id: ", request.id, "cb:", typeof request.cb);
			if (Utils.isFunc(request.cb))
				request.cb(data);
		};
	},

	_createExternalCallback : function(name, cb) {
		window[name] = cb;
	},

	_deregisterExternalCallback : function(name, id) {
		for (var i = 0, len = this._externalRoutes[name].length; i < len; i++) {
			var request = this._externalRoutes[name][i];
			if (request.id === id) {
				log.info("JSONP", "removing request from route:", name, "with id:", id);
				this._externalRoutes[name].splice(i, 1);
				return;
			}
		}
	}
};


// Export public APIs
module.exports = Ajax;

},{"../vendor/when":96,"./log":28,"./utils":50}],11:[function(require,module,exports){
/* cvp/api.js */

// Load dependent modules
var Utils = require('./utils');
var CVP = require('./static');
var Shared = require('./shared');
var swfobject = require('../vendor/swfobject');
var Events = require('./events');
var Cdn = require('./cdn');


// TODO move the API implementations to their respective player impls
var CVPObjectPrototype = {

	getDOMPlayer : (
		navigator.appName.indexOf("Microsoft") !== -1
		? function getDOMPlayer() { return window[this.getId()]; }
		: function getDOMPlayer() { return document[this.getId()]; }
	),

	sendMessage : function(name, data) {
		this.getDOMPlayer().sendMessage(name, data);
		return this;
	},

	getPlayer : function(id) {
		if (this._playerType === CVP.FLASH) {
			this.getDOMPlayer().getPlayerJS(id);
			return this;
		}

		return this.getDOMPlayer();
	},

	setDefaultPlayer : function(id) {
		this.getDOMPlayer().setDefaultPlayer(id);
		return this;
	},

	// _embedCallback : function (e) {
	// 	if (!e.success) {
	// 		var id = this.getId();
	// 		CVP.onCallback(id, ["onPlayerLoadError", id]);
	// 	}
	// },

	/**
	 * @deprecated Use embed() instead.
	 * @returns {CVP} The CVP instance.
	 */
	embedSWF : function () {
		return this.embed.apply(this, arguments);
	},

	/**
	 * Global function for actionscript to call.
	 * @param {String} containerElementId The container element's id.
	 * @returns {CVP} The CVP instance.
	 */
	embed : function(containerElementId) {

		if (this._playerType === CVP.FLASH)
		{
			var id = this.getId();
			var flashvars = this.getFlashVars();
			flashvars.domId = CVP.sanitizeId(id);

			var embed = this.getEmbed();
			var container = Cdn.replace(embed.containerSwf, flashvars);
			var params = embed.options;
			var express = embed.expressInstallSwf;
			var version = Shared.validateFlashVersion(embed.flashVersion);

			var attributes = {
				id : id,
				name : id
			};

			if (!swfobject.hasFlashPlayerVersion("1.0.0"))
				CVP.onCallback(id, ["onNoFlashDetected"]);

			var flash_var_name = "overwrite"; // reference to FlashVarsEntryPoint.FLASH_VAR_NAME, but unable to reference it at this point
			var overwrite = flashvars[flash_var_name];

			// FlashVars are received as strings in Flash. The overwrite object must be converted to a string
			// to retain the information. Then it can be re-parsed on the Flash side.
			if (!Utils.isEmpty(overwrite))
				flashvars[flash_var_name] = JSON.stringify(overwrite);

			// swfobject.embedSWF(container, containerElementId, this.getWidth(), this.getHeight(), version, express, flashvars, params, attributes, Utils.bind(this._embedCallback, this));
			swfobject.embedSWF(container, containerElementId, this.getWidth(), this.getHeight(), version, express, flashvars, params, attributes);
		}
		else
		{
			var player = this.getPlayer();
			if (Utils.isFunc(player.embed))
			{
				// Swfobject internally waits until onReady before embedding
				// For all other cases, ensure the DOM is ready before proceeding
				Events.onReady(function() {
					player.embed(containerElementId);
				});
			}
		}

		return this;
	},

	/**
	 * @deprecated Use remove() instead.
	 * @returns {CVP} The CVP instance.
	 */
	removeSWF : function () {
		return this.remove.apply(this, arguments);
	},

	/**
	 * Removes the swfobject from CVP.
	 * @returns {CVP} The CVP instance.
	 */
	remove : function() {
		try {
			this.stop();
		} catch (e) {
			// fail silently
		}

		if (this._playerType === CVP.FLASH)
		{
			swfobject.removeSWF(this.getId());
		}
		else
		{
			var player = this.getPlayer();
			if (Utils.isFunc(player.remove))
			{
				player.remove();
			}
		}

		return this;
	},

	/**
	 * Destroys CVP instance.  Cannot be reembeded.  ID can be reused.
	 */
	destroy : function ()
	{
		var id = this.getId();

		if (!CVP.findInstance(id))
			return;

		this.remove();

		if (this._playerType === CVP.FLASH)
		{
			// Is there something else to be done to destroy the Flash player?
		}
		else
		{
			var player = this.getPlayer();
			if (Utils.isFunc(player.dispose))
			{
				try {
					player.dispose();
				} catch (e) {
					// fail silently
				}
			}
		}

		// Unregister instance and callback handlers for both HTML5 and FLASH.
		CVP.unregisterInstance(id);
	},

	/**
	 * Play a specified video by id. This call will reset the video queue and will
	 * start playing the video immediately.
	 * @param {String} id The id of the content being played.
	 * @param {Object} options More parameters for playing content.
	 * @returns {CVP} The CVP instance.
	 */
	play : function (id, options) {
		this.getDOMPlayer().playContent(id, options || {});
		return this;
	},

	playFromObject : function (object, options) {
		this.getDOMPlayer().playFromObject(object, options || {});
		return this;
	},

	/**
	 * report a event message to analytics server such as Aspen.
	 * @param {String} eventName The name of the event message.
	 * @param {Object} data An object whose properties contains the details of the event message.
	 * @returns {CVP} The CVP instance.
	 */
	reportAnalytics : function (eventName, data) {
		this.getDOMPlayer().reportAnalytics(eventName, data);
		return this;
	},

	/**
	 * Replay the last viewed video.
	 * @returns {CVP} The CVP instance.
	 */
	replay : function () {
		this.getDOMPlayer().replayContent();
		return this;
	},

	playNextInQueue : function () {
		this.getDOMPlayer().playNextInQueue();
		return this;
	},

	/**
	 * Paused the video playback. Asynchronous call, a callback will be made when
	 * the call completes. See callbacks.
	 * @returns {CVP} The CVP instance.
	 */
	pause : function () {
		this.getDOMPlayer().pause();
		return this;
	},

	/**
	 * Resumes video playback. Asynchronous call, a callback will be made when the
	 * call completes. See callbacks.
	 * @returns {CVP} The CVP instance.
	 */
	resume : function () {
		this.getDOMPlayer().resume();
		return this;
	},

	stop : function () {
		this.getDOMPlayer().stopContent();
		return this;
	},

	/**
	 * Add a video to the play queue. The video will be added to the end of the queue.
	 * If the queue is empty and there is no video playing the video will start playing
	 * immediately.
	 * @param {String} id - The id of the content.
	 * @param {Object} options - More parameters for playing content.
	 * @param {Number} index - [Optional]
	 * @returns {CVP} The CVP instance.
	 */
	queue : function (id, options, index) {
		if (typeof index === 'undefined')
			index = -1;
		this.getDOMPlayer().queue(id, options || {}, index);
		return this;
	},

	queueFromObject : function (object, options, index) {
		if (typeof index === 'undefined')
			index = -1;
		this.getDOMPlayer().queueFromObject(object, options || {}, index);
		return this;
	},

	dequeue : function (id) {
		this.getDOMPlayer().dequeue(id);
		return this;
	},

	/**
	 * Empties the content queue.
	 * @returns {CVP} The CVP instance.
	 */
	emptyQueue : function () {
		this.getDOMPlayer().emptyQueue();
		return this;
	},

	setQueueAutoplay : function(autoplay) {
		this.getDOMPlayer().setQueueAutoplay(autoplay);
		return this;
	},

	getQueue : function () {
		return this.getDOMPlayer().getQueue();
	},

	seek : function (time) {
		this.getDOMPlayer().seek(time);
		return this;
	},

	mute : function () {
		this.getDOMPlayer().mute();
		return this;
	},

	unmute : function () {
		this.getDOMPlayer().unmute();
		return this;
	},

	setVolume : function (volume) {
		this.getDOMPlayer().setVolume(volume);
		return this;
	},

	getVolume : function () {
		return this.getDOMPlayer().getVolume();
	},

	isMuted : function () {
		return this.getDOMPlayer().isMuted();
	},

	getContentEntry : function (id) {
		return this.getDOMPlayer().getContentEntry(id);
	},

	goFullscreen : function () {
		this.getDOMPlayer().goFullscreen();
		return this;
	},

	resize : function (width, height, duration) {
		this.getDOMPlayer().resize(width, height, duration);
		return this;
	},

	setMaxBitrate : function (bitrate) {
		this.getDOMPlayer().setMaxBitrate(bitrate);
		return this;
	},

	switchBitrateId : function (id) {
		this.getDOMPlayer().switchBitrateId(id);
		return this;
	},

	setAutoBitrateSwitch : function (auto) {
		this.getDOMPlayer().setAutoBitrateSwitch(auto);
		return this;
	},

	getAvailableBitrates : function (playMode) {
		return this.getDOMPlayer().getAvailableBitrates(playMode);
	},

	getBitrateId : function () {
		return this.getDOMPlayer().getBitrateId();
	},

	setGroupOrder : function (order) {
		this.getDOMPlayer().setGroupOrder(order);
		return this;
	},

	getShareOptions : function () {
		return this.getDOMPlayer().getShareOptions();
	},

	setClosedCaptions : function (on, trackId) {
		this.getDOMPlayer().setClosedCaptions(on, trackId);
		return this;
	},

	setAdSection : function (section) {
		this.getDOMPlayer().setAdSection(section);
		return this;
	},

	setAdKeyValue : function (key, value) {
		this.getDOMPlayer().setAdKeyValue(key, value);
		return this;
	},

	getAdKeyValues : function () {
		return this.getDOMPlayer().getAdKeyValues();
	},

	clearAdKeyValues : function () {
		this.getDOMPlayer().clearAdKeyValues();
		return this;
	},

	setAdExchangeValue : function (key, value) {
		this.getDOMPlayer().setAdExchangeValue(key, value);
		return this;
	},

	setAdVisibility : function (adsObject) {
		this.getDOMPlayer().setAdVisibility(adsObject);
		return this;
	},

	switchAdContext : function (context) {
		this.getDOMPlayer().switchAdContext(context);
		return this;
	},

	switchTrackingContext : function (context) {
		this.getDOMPlayer().switchTrackingContext(context);
		return this;
	},

	setTrackingInterval : function (interval) {
		this.getDOMPlayer().setTrackingInterval(interval);
		return this;
	},

	setDataSrc : function (src) {
		this.getDOMPlayer().setDataSrc(src);
		return this;
	},

	setFileKey : function (key) {
		this.getDOMPlayer().setFileKey(key);
		return this;
	},

	sendUIMessage : function (message, data) {
		this.getDOMPlayer().sendUIMessage(message, data);
		return this;
	},

	sendUserReportedIssue : function (data) {
		this.getDOMPlayer().sendUserReportedIssue(data);
		return this;
	}
};


// Export public APIs
module.exports = CVPObjectPrototype;

},{"../vendor/swfobject":95,"./cdn":13,"./events":26,"./shared":37,"./static":38,"./utils":50}],12:[function(require,module,exports){
/* cvp/browser.js */

/**
 * A function to detect the current browser.
 * @private
 * @returns An object with browser info.
 * @type Object
 */

// ## CVP.Browser
//
//   - *members*:
//     - name (Safari, Chrome, Firefox, IE, Opera | others?)
//     - version:
//       - raw (what was dumped in)
//       - major
//       - minor
//       - revision
//       - toString() (string)
//     - engine
//       - name (Gecko, AppleWebKit, Presto, Trident)
//       - version (same as browser version setup [above])
//     - os
//       - name (OSX, iOS, Android | others?)
//       - version (same as browser version setup [above])
//     - device
//       - name (iPad, iPod, iPhone, Kindle | others?)
//       - type (mobile, tablet | desktop? headset?)
//     - supports
//       - video
//       - mp4
//       - m3u8
//
//   - *methods*:
//     - isBrowser(name, [version])
//     - isEngine(name, [version])
//     - isOS(name, [version])
//     - isDevice(name)
//     - isPlatform(name)

var Version = require('./version');


var ua = window.navigator.userAgent;

/* eslint-disable no-console */
var log = (
	"object" === typeof console && "function" === typeof console.log
	? function () { console.log.apply(console, arguments); }
	: function () {}
);
/* eslint-enable no-console */

var browser = {
	IOS : "iOS",
	OSX : "OSX",
	ANDROID : "Android",
	SAFARI : "Safari",
	SILK : "Silk",
	CHROME : "Chrome",
	FIREFOX : "Firefox",
	OPERA : "Opera",
	IE : "Internet Explorer",
	IPAD : "iPad",
	IPOD : "iPod",
	IPHONE : "iPhone",
	KINDLE : "Kindle",
	DESKTOP : "desktop",
	MOBILE : "mobile",
	TABLET : "tablet"
};

// Determine basic `video` element support.
var video = document.createElement('video');
var successPattern = /^(maybe|probably)$/;

var canPlayType = function(mediaType) {
	try {
		return successPattern.test(video.canPlayType(mediaType));
	} catch (e) {
		return false;
	}
};

browser.supports = {};
browser.supports.video = 'function' === typeof video.canPlayType;
// TODO For desktop purposes, this will need to be revisited
// For accurate codec reporting, IE9 can require the audio codec to be passed in as well
// https://github.com/Modernizr/Modernizr/commit/b51f17bb717b
browser.supports.mp4 = browser.supports.video && canPlayType('video/mp4');
browser.supports.m3u8 = browser.supports.video && ( canPlayType('application/x-mpegURL') || canPlayType('application/vnd.apple.mpegURL') );

// Detect stuff from `navigator.userAgent` string:
var tells = {
	// browsers,
	Safari : / Safari\/\d+(?:\.\d+){0,2}/,
	Version : / Version\/(\d+(?:\.\d+){0,2})/,
	Opera : /^Opera\/\d+(?:\.\d+){0,2}/,
	Silk : / Silk\/(\d+(?:\.\d+){0,2})/,
	InternetExplorer : /; MSIE (\d+(?:\D\d+){0,2})/,
	InternetExplorer11 : /; Trident\/[^)]*; rv:(\d+(?:\D\d+){0,2})/,
	Firefox : / Firefox\/(\d+(?:\D\d+){0,2})/,
	Chrome : / Chrome\/(\d+(?:\D\d+){0,2})/,

	// rendering engines,
	RenderingEngine : / (AppleWebKit|Trident|Gecko|Presto)\/(\d+(?:\D\d+){0,2})/,

	// devices,
	AppleDevice : / \((iP(?:[ao]d|hone));/,

	// operating systems,
	Android : /; Android(?: (\d+(?:\D\d+){0,2}))?[;)]/,
	iOS : / OS (\d+(?:\D\d+){0,2}) like(?: Mac)? OS X/,
	OSX : / OS X (\d+(?:\D\d+){0,2})/,

	// mobile platforms.
	MobileSafari : / Mobile(?:\/\S+)? Safari\//,
	MobileFirefox : /; Mobile; .+ Firefox\/| Fennec\//,
	TabletFirefox : /; Tablet; .+ Firefox\//,
	IEMobile : /; IEMobile\/(\d+(?:\.\d+){0,2})/,
	OperaMobile : /; Opera (Mini|Mobi)\//,
	OperaTablet : /; Opera Tablet\//,
	SilkAccelerated : / Silk-Accelerated=(true|false)/
};

function readTells(userAgent) {
	var results = {};
	for (var tell in tells) {
		results[tell] = tells[tell].exec(userAgent);
	}
	return results;
}

(function (has) {

	// browser - non-Safari-like (unique strings in `userAgent`)

	//   - Firefox
	if (has.Firefox) {
		browser.name = browser.FIREFOX;
		browser.version = new Version(has.Firefox[1]);
	}
	//   - Internet Explorer
	else if (has.InternetExplorer) {
		browser.name = browser.IE;
		browser.version = new Version(has.InternetExplorer[1]);
	}
	else if (has.InternetExplorer11) {
		browser.name = browser.IE;
		browser.version = new Version(has.InternetExplorer11[1]);
	}
	//   - Opera
	else if (has.Opera && has.Version) {
		browser.name = browser.OPERA;
		browser.version = new Version(has.Version[1]);
	}

	// browser - Safari-like (similar WebKit `userAgent`s)

	//   - Silk
	else if (has.Silk) {
		browser.name = browser.SILK;
		browser.version = new Version(has.Silk[1]);
	}
	//   - Chrome
	else if (has.Chrome) {
		browser.name = browser.CHROME;
		browser.version = new Version(has.Chrome[1]);
	}
	//   - Safari
	else if (has.Safari && has.Version) {
		browser.name = browser.SAFARI;
		browser.version = new Version(has.Version[1]);
	}

	// rendering engine
	browser.engine = {};
	if (has.RenderingEngine) {
		browser.engine.name = has.RenderingEngine[1];
		browser.engine.version = new Version(has.RenderingEngine[2]);
	}

	// operating system
	browser.os = {};

	//   - Android
	if (has.Android) {
		browser.os.name = browser.ANDROID;
		if ('undefined' !== typeof has.Android[1]) {
			browser.os.version = new Version(has.Android[1]);
		}
	}
	//   - iOS
	else if (has.iOS) {
		browser.os.name = browser.IOS;
		browser.os.version = new Version(has.iOS[1]);
	}
	//   - Mac OS X
	else if (has.OSX) {
		browser.os.name = browser.OSX;
		browser.os.version = new Version(has.OSX[1]);
	}

	// device/platform
	browser.device = {};

	// device.name

	//   - Apple devices
	if (has.AppleDevice) {
		browser.device.name = has.AppleDevice[1];
	}
	//   - Amazon devices
	else if (has.Silk) {
		browser.device.name = browser.KINDLE;
	}

	// device.type
	//   - "desktop" is assumed to be the default
	browser.device.type = browser.DESKTOP;
	//   - "mobile" overwrites "desktop" if a mobile browser found
	if (has.MobileSafari || has.MobileFirefox || has.IEMobile || has.OperaMobile) {
		browser.device.type = browser.MOBILE;
	}
	//   - "tablet" overwrites "mobile" as it's more specific
	if (has.TabletFirefox || has.OperaTablet || (has.Android && browser.device.type !== browser.MOBILE)) {
		browser.device.type = browser.TABLET;
	}

	// `is(property, name, [version])`
	// checks a "property" (browser, OS, etc.) against a value ("name")
	// with an option to check against a version number,
	// e.g. is('browser', 'Safari', '>5.1').
	//
	// TODO: Figure out what to do when we don't get a version number
	// from the userAgent.  Throw an error?  Return undefined instead
	// of true/false?
	var is = function (property, name, version) {
		var result;
		var pattern = /^(!?~|[!=><]=?)?(\d.*)$/;
		var checkVersion = function (version, string) {
			var match = pattern.exec(string);
			var fn = version.is;
			var response;
			if (!match) {
				throw new Error("checkVersion: The pattern -- " + string + " -- was not considered valid.");
			}
			switch (match[1]) {
			case '~':
				fn = version.is;
				break;
			case '!~':
				fn = version.isnt;
				break;
			case '=':
			case '==':
				fn = version.eq;
				break;
			case '!':
			case '!=':
				fn = version.ne;
				break;
			case '>':
				fn = version.gt;
				break;
			case '>=':
				fn = version.gte;
				break;
			case '<':
				fn = version.lt;
				break;
			case '<=':
				fn = version.lte;
				break;
			}
			response = fn.call(version, match[2]);
			return response;
		};

		switch (property) {
		case 'browser':
			result = name === browser.name;
			if (version) {
				result = result && checkVersion(browser.version, version);
			}
			break;
		case 'engine':
			result = name === browser.engine.name;
			if (version) {
				result = result && checkVersion(browser.engine.version, version);
			}
			break;
		case 'os':
			result = name === browser.os.name;
			if (version) {
				result = result && checkVersion(browser.os.version, version);
			}
			break;
		case 'device':
			result = name === browser.device.name;
			break;
		case 'platform':
			result = name === browser.device.type;
			break;
		default:
			log('Browser.is() did not understand property "' + property + '"');
		}
		return result;
	};


	// curry (partial application)
	function curry(fn, scope) {
		var context = scope || window;
		var args = [].slice.call(arguments, 2);
		return function() {
			return fn.apply(context, args.concat([].slice.call(arguments)));
		};
	}

	// Setting up pre-filled functions for ease-of-use.
	browser.isBrowser  = curry(is, this, 'browser');
	browser.isEngine   = curry(is, this, 'engine');
	browser.isOS       = curry(is, this, 'os');
	browser.isDevice   = curry(is, this, 'device');
	browser.isPlatform = curry(is, this, 'platform');


	/* BROWSER DETECTION OVERRIDES */

	// Disallowing m3u8 on Android < 4.
	if (browser.isOS(browser.ANDROID, '<4')) {
		browser.supports.m3u8 = false;
	}

	// Identify Kindle Fire as mobile simply because it plays video fullscreen.
	if (browser.device.name === browser.KINDLE) {
		browser.device.type = browser.MOBILE;
	}

	// Identify iPad as tablet.
	if (browser.device.name === browser.IPAD) {
		browser.device.type = browser.TABLET;
	}

}(readTells(ua)));


module.exports = browser;

},{"./version":51}],13:[function(require,module,exports){
/* cvp/cdn.js */

/**
 * Basic string-replacement of urls based on available flashvars
 *
 * This is temporary, and is a stop-gap pending full url-replacement support
 * across JS / Flash / HTML5
 */

// The CDN url, broken down into it's parts:
// protocol, cdnSubDomain, cdnHost
var DEFAULT_CDN_SUBDOMAIN 	= "cvp1";
var SECURE_CDN_SUBDOMAIN 	= "s";
var LEGACY_CDN_SUBDOMAIN 	= "z";
var DEFAULT_CDN_HOST 		= "${cdnSubdomain}.cdn.turner.com";

// Template for the cdn url used for string replacement
var CDN_TEMPLATE 			= "${protocol}${cdnHost}";

function calculateCdnParts(params) {
	var secure = null,
		protocol = null,
		cdnSubdomain = null,
		cdnHost = null;

	// Calculate the CDN url parts
	secure = params.hasOwnProperty("secure");
	protocol = (secure
		? "https"
		: "http") + "://";
	cdnSubdomain = params.hasOwnProperty("cdnSubdomain")
		? params.cdnSubdomain
		: DEFAULT_CDN_SUBDOMAIN;
	cdnHost = DEFAULT_CDN_HOST;
	
	// For now, the cvp hostname doesn't support SSL. Use "s." if secure
	if (secure)
		cdnSubdomain = SECURE_CDN_SUBDOMAIN;

	return {
		secure : secure,
		protocol : protocol,
		subdomain : cdnSubdomain,
		host : cdnHost
	};
}

function calculateCdnUrl(template, parts) {
	var evaluatedHost = (parts.host).replace(/\$\{cdnSubdomain\}/, parts.subdomain);
	var url = template
					.replace(/\$\{protocol\}/, parts.protocol)
					.replace(/\$\{cdnHost\}/, evaluatedHost);
	
	return url;
}

function replace(url, flashvars) {
	var parts = calculateCdnParts(flashvars);
	var cdn = calculateCdnUrl(CDN_TEMPLATE, parts);

	return url.replace(/\$\{cdn\}/, cdn);
}

// Export public APIs
module.exports = {
	replace : replace
};
},{}],14:[function(require,module,exports){
/* cvp/convert.js */

// Load dependent modules
var Utils = require('./utils');
var JsonConverter = require('./util/jsonconverter');
var XML = require('./util/xml');
var Log = require('./log');


var logger = Log.getLogger();

// Source:  JSON2.js <http://www.JSON.org/js.html>
var allBackslashPairsRegex = /\\(?:["\\\/bfnrt]|u[0-9a-fA-F]{4})/g;
var allSimpleValueTokensRegex = /"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g;
var allOpenBracketsRegex = /(?:^|:|,)(?:\s*\[)+/g;
var onlyValidLeftoverCharsRegex = /^[\],:{}\s]*$/;
var isValidJSON = function (text) {
	return (
		onlyValidLeftoverCharsRegex
			.test(
				text
					.replace(allBackslashPairsRegex, '@')
					.replace(allSimpleValueTokensRegex, ']')
					.replace(allOpenBracketsRegex, '')
			)
	);
};

function jsonTextToJsObject(jsontext) {
	logger.log('Convert::jsonTextToJsObject');

	if (Utils.isNull(jsontext)) {
		return null;
	}

	jsontext = Utils.trim(jsontext);

	if (jsontext === '') {
		return null;
	}

	var jsobject = null;

	try {
		jsobject = JSON.parse(jsontext);
	} catch (e) {
		logger.warn('Provided text was not valid JSON.  Attempting to eval()...');

		if (isValidJSON(jsontext)) {
			try {
				/* eslint-disable no-eval */
				jsobject = eval('(' + jsontext + ')');
				/* eslint-enable no-eval */
			} catch (e2) {
				logger.error('Unable to eval text into JS object.');
			}
		}
	}

	return jsobject;
}

function xmlTextToXmlDoc(xmltext) {
	logger.log('Convert::xmlTextToXmlDoc');

	var xmldoc;

	if (!xmltext)
		return null;

	xmldoc = XML.stringToDoc(xmltext);

	if (!xmldoc) {
		logger.warn('Provided text was not valid XML.  Attempting to clean and parse again...');
		xmltext = XML.cleanString(xmltext);
		xmldoc = XML.stringToDoc(xmltext);
	}

	return xmldoc;
}

function xmlDocToXmlText(xmldoc) {
	logger.log('Convert::xmlDocToXmlText');

	return XML.docToString(xmldoc);
}

function xmlDocToJsonText(xmldoc) {
	logger.log('Convert::xmlDocToJsonText');

	return JsonConverter.xmlToJson(xmldoc);
}

function xmlDocToJsObject(xmldoc) {
	logger.log('Convert::xmlDocToJsObject');

	return JsonConverter.xmlToObject(xmldoc);
}

function xmlTextToJsonText(xmltext) {
	logger.log('Convert::xmlTextToJsonText');

	return xmlDocToJsonText(xmlTextToXmlDoc(xmltext));
}

function xmlTextToJsObject(xmltext) {
	logger.log('Convert::xmlTextToJsObject');

	return jsonTextToJsObject(xmlTextToJsonText(xmltext));
}


// Export public APIs
module.exports = {
	jsonTextToJsObject : jsonTextToJsObject,
	xmlTextToXmlDoc : xmlTextToXmlDoc,
	xmlDocToXmlText : xmlDocToXmlText,
	xmlDocToJsonText : xmlDocToJsonText,
	xmlDocToJsObject : xmlDocToJsObject,
	xmlTextToJsonText : xmlTextToJsonText,
	xmlTextToJsObject : xmlTextToJsObject
};

},{"./log":28,"./util/jsonconverter":46,"./util/xml":49,"./utils":50}],15:[function(require,module,exports){
/* cvp/core.js */

// Load dependent modules
var CVP = require('./static');
var CVPPrototype = require('./api');
var Shared = require('./shared');
var Utils = require('./utils');
var App = require('../core/app');
var Browser = require('./browser');
var swfobject = require('../vendor/swfobject');
var HTML5Player = require('./players/html5');
var NullPlayer = require('./players/null');
var Log = require('./log');
var Log4JS = require('./util/log4js');
var Performance = require('./performance');


/**
 * Constructs a new CVP object.
 * Here is where the player is created and its behaviors are defined.
 * @class
 * @param {Object} options - The various parameters that define a player.
 * @return {CVP} The CVP instance.
 */
var CVPObject = function (options) {
	var self = this;

	this.options = Utils.extend({
		id : 'cvp_player',
		width : '320',
		height : '240',
		flashVars : { },
		initialize : function () {}
	}, options);

	var log = Log.getLogger(this.options.id);

	this.options.embed = Utils.extend({
		containerSwf : '',
		expressInstallSwf : App.EXPRESS_INSTALL_URL,
		flashVersion : App.FLASH_VERSION
	}, this.options.embed);

	this.options.embed.options = Utils.extend({
		quality : 'high',
		bgcolor : '#000000',
		allowFullScreen : 'true',
		allowScriptAccess : 'always'
	}, this.options.embed.options);

	if (!this.options.embed.containerSwf) {
		log.error('Invalid containerSwf...exiting');
		throw new Error("Invalid containerSwf");
	}

	this.options.initialize();

	var id = this.options.id;
	var width = this.options.width;
	var height = this.options.height;
	var flashVars = this.options.flashVars;
	var embed = this.options.embed;

	// Stringify flashVars for consistency! (Flash was getting strings.)
	Utils.each(flashVars, function (key, value, collection) {
		if (!Utils.isObject(value)) {
			flashVars[key] = String(value);
		}
	});

	this._playerType = this.options.playerType;
	var validTypes = [App.FLASH, App.HTML5];

	// if playerType option is undefined/null/invalid, auto-swivel
	if (!this._playerType
		|| Utils.indexOf(validTypes, this._playerType) === -1)
	{
		this._playerType = App.FLASH;

		if (Browser.supports.video
			&& !swfobject.hasFlashPlayerVersion("1.0.0"))
		{
			this._playerType = App.HTML5;
		}
	}

	var player = null;

	if (this._playerType === App.HTML5)
	{
		if (flashVars.site
			&& flashVars.profile)
		{
			log.info("instantiating the HTML5 player");

			player = new HTML5Player(this.options);
		}
		else
		{
			log.error("invalid HTML5 params...instantiating null player");

			this._playerType = false;

			player = new NullPlayer(this.options);
		}

		this.getDOMPlayer = function ()
		{
			return player;
		};
	}
	else
	{
		log.info("instantiating the Flash player");

		Log4JS.install();

	}

	if (CVP.findInstance(id) || Utils.byId(id)) {
		log.error(id + ' is already in use...exiting');
		throw new Error(id + ' is already in use');
	}

	this.getId = function () { return id; };
	this.getWidth = function () { return width; };
	this.getHeight = function () { return height; };
	this.getFlashVars = function () { return flashVars; };
	this.getEmbed = function () { return embed; };
	this.getPlayerType = function () { return this._playerType; };

	if (!Shared.createCallbackHandler(id)) {
		log.error('callback handler for id "' + id + '" could not be created...exiting');
		throw new Error('callback handler for id "' + id + '" could not be created...exiting');
	}

	this.callbacks = {};
	delete this.options.initialize;
	var p, pfn;
	for (p in this.options) {
		if (Utils.hasOwn(this.options, p)) {
			pfn = this.options[p];
			if (Utils.isFunc(pfn)) {
				this.callbacks[p] = pfn;
			}
		}
	}

	// A map of possible messages to be invoked via sendMessage
	var onSendMessage = {
		handleCallBack : function (funcName /*, args... */) {
			var ret, fn;

			if (arguments.length) {

				var args = Utils.slice(arguments, 1);
				// log.debug("handleCallBack", funcName, args);

				fn = self[funcName];

				if (Utils.isFunc(fn)) {
					try {
						// log.debug("Found internal CB");
						ret = fn.apply(self, args);
					} catch(internalCBException) {
						log.warn("exception on internal CB " + funcName, internalCBException.message);
					}
				}

				fn = self.callbacks[funcName];

				if (Utils.isFunc(fn)) {
					try {
						// log.debug("Found user CB");
						ret = fn.apply(self, args);
					} catch(userCBException) {
						log.error("exception on user CB " + funcName, userCBException.message);
					}
				}
			}

			return ret;
		},

		onPageOrientationChange : function (orientation) {
			if (!self.isCVPReady())
				return;

			if (orientation === self.getPageOrientation())
				return;

			// Send the state change to the player via the sendMessage API
			this._sendPageStateChange({ orientation : orientation });
			this.handleCallBack("onPageOrientationChange", id, orientation);
		},

		onPageVisibilityChange : function (visible) {
			if (!self.isCVPReady())
				return;

			if (visible === self.isPageVisible())
				return;

			this.handleCallBack("onPageVisibilityChange", id, visible);
			this._fireCVPVisibilityChange();
		},

		onViewportVisibilityChange : function (visible) {
			if (!self.isCVPReady())
				return;

			if (visible === self.isViewportVisible())
				return;

			this.handleCallBack("onViewportVisibilityChange", id, visible);
			this._fireCVPVisibilityChange();
		},

		_fireCVPVisibilityChange : function () {
			this.handleCallBack("onCVPVisibilityChange", id, self.isCVPVisible());
		},

		_sendPageStateChange : function (data) {
			this._invokeAPI("sendMessage", "pageStateChange", data);
		},

		_invokeAPI : function (apiName) {
			var args = Utils.slice(arguments, 1);
			if (apiName in self) {
				try {
					self[apiName].apply(self, args);
				} catch(e) {
					log.warn("[_invokeAPI] Error calling: " + apiName);
				}
			}
		}

	};

	// Allow messages to be sent to execute internal routines.
	// This allows CVP to control each instance without having
	// to worry about collision
	var sendMessage = function (name) {
		if (Utils.hasOwn(onSendMessage, name)) {
			try {
				return onSendMessage[name].apply(onSendMessage, Utils.slice(arguments, 1));
			} catch(messageException) {
				log.warn("exception on message " + name, messageException.message);
			}
		}
	};

	// Performance functionality
	var embedStartTime = -1;
	var readyTime = -1;
	var loadMetrics = null;
	var loadMetricsReport = null;

	this.embed = CVP.Utils.before(this, this.embed, function() {
		embedStartTime = CVP.Utils.now();
		Performance.mark(id, "embedRequest");
	});

	var reportPerfMetrics = function() {
		if (!isCVPReady) {
			log.warn("[perfMetrics] Called before CVPReady!");
			return;
		}

		var data = Performance.getCVPLoadMetrics(id, embedStartTime, readyTime);

		// Add some additional useful info to report - visibility, version, etc..
		data.cvp = Utils.extend(data.cvp, {
			cvpjsVersion : App.VERSION,
			isPageVisible : isPageVisible,
			isViewportVisible : isViewportVisible
		});

		loadMetrics = data;

		// Send the data to Aspen
		self.reportAnalytics("playerLoadMetrics", loadMetrics);

		loadMetricsReport = JSON.stringify(data, null, 4);
		log.info(loadMetricsReport);
	};

	// Convenience functions

	var isCVPReady = false;
	var contentId = flashVars.contentId || '';
	var context = flashVars.context || '';
	var playerInstance = context;

	var contentWidth = 0;
	var contentHeight = 0;

	var duration = 0;
	var playhead = 0;
	var absolutePlayhead = -1;

	var buffering = false;
	var paused = false;
	var fullscreen = false;
	var uniqueVideoId = '';

	// TODO get these initial values appropriately
	var pageOrientation = window.orientation || "unknown";
	var isPageVisible = true;
	var isViewportVisible = true;

	this.isCVPReady = function () { return isCVPReady; };
	this.getDOMPlayerInstance = function () { return playerInstance; };
	this.getContentId = function () { return contentId; };
	this.getContext = function () { return context; };

	this.getContentWidth = function () { return contentWidth; };
	this.getContentHeight = function () { return contentHeight; };

	this.getDuration = function () { return duration; };
	this.getPlayhead = function () { return playhead; };
	this.getAbsolutePlayhead = function () { return absolutePlayhead; };

	this.isBuffering = function () { return buffering; };
	this.isPaused = function () { return paused; };
	this.isFullscreen = function () { return fullscreen; };

	this.getUniqueVideoId = function () { return uniqueVideoId; };

	this.getPageOrientation = function () { return pageOrientation; };
	this.isPageVisible = function () { return isPageVisible; };
	this.isViewportVisible = function () { return isViewportVisible; };
	this.isCVPVisible = function () { return isPageVisible && isViewportVisible; };

	// Dev convenience functions
	this._getLoadMetrics = function() { return loadMetrics; };
	this._getLoadMetricsReport = function() { return loadMetricsReport; };

	this.onCVPReady = function () {
		readyTime = CVP.Utils.now();
		isCVPReady = true;

		Performance.mark(id, "cvpReady");
		reportPerfMetrics();
	};

	this.onPlayerReady = function () {
		playerInstance = this.getDOMPlayerInstance();
	};

	this.onPageVisibilityChange = function (pPlayerId, pVisible) {
		isPageVisible = pVisible;
	};

	this.onViewportVisibilityChange = function (pPlayerId, pVisible) {
		isViewportVisible = pVisible;
	};

	this.onContentMetadata = function (pPlayerId, pContentId, pDuration, pWidth, pHeight) {
		contentId = pContentId;
		playhead = 0;
		duration = pDuration;
		contentWidth = pWidth;
		contentHeight = pHeight;
	};

	this.onContentBegin = function (pPlayerId, pContentId) {
		uniqueVideoId = Utils.uuid();
		this.reportAnalytics('uniqueVideoId', { uniqueVideoId: uniqueVideoId });
		paused = true;
		contentId = pContentId;
	};

	this.onAdPlay = function (/*pPlayerId, pContentId*/) {
		paused = false;
	};

	this.onContentPlay = function (/*pPlayerId, pContentId*/) {
		paused = false;
	};

	this.onContentBuffering = function (pPlayerId, pContentId, pBuffering) {
		buffering = pBuffering;
	};

	this.onContentPlayhead = function (pPlayerId, pContentId, pPlayhead, pTotalDuration, pCurrentTime) {
		playhead = pPlayhead;
		absolutePlayhead = pCurrentTime || -1;
	};

	this.onContentPause = function (pPlayerId, pContentId, pPaused) {
		paused = pPaused;
	};

	this.onContentResize = function (pPlayerId, pWidth, pHeight, pFullscreen) {
		fullscreen = pFullscreen;
	};

	CVP.registerInstance(id, this, sendMessage);

	return this;
};

CVPObject.prototype = CVPPrototype;


// Export public APIs
module.exports = CVPObject;

},{"../core/app":8,"../vendor/swfobject":95,"./api":11,"./browser":12,"./log":28,"./performance":30,"./players/html5":31,"./players/null":32,"./shared":37,"./static":38,"./util/log4js":47,"./utils":50}],16:[function(require,module,exports){
/* cvp/customevent.js */

// Load dependent modules
var Class = require('../vendor/Class');
var Utils = require('./utils');
var Log = require('./log');


var logger = Log.getLogger();

var CustomEvent = Class.extend({
	init : function(type) {
		this._type = type;
		this._listeners = [];
	},

	addListener : function(fn, ctxt) {
		if (!Utils.isFunc(fn))
		{
			logger.warn("CustomEvent.addListener: The provided param was not a function.");
			return;
		}

		this._listeners.push({ fn : fn, ctxt : ctxt });
	},

	removeListener : function(fn, ctxt) {
		var i = 0,
			fns = this._listeners,
			o;

		while (i < fns.length)
		{
			o = fns[i];
			if (o.fn === fn && o.ctxt === ctxt)
			{
				fns.splice(i, 1);
			}
			else {
				++i;
			}
		}
	},

	dispatch : function() {
		var i = 0,
			fns = this._listeners.slice(),
			len = fns.length,
			o;

		for (; i < len; ++i)
		{
			o = fns[i];
			o.fn.apply(o.ctxt, arguments);
		}
	}
});


// Export public APIs
module.exports = CustomEvent;

},{"../vendor/Class":93,"./log":28,"./utils":50}],17:[function(require,module,exports){
/* cvp/detectors/basestrategy.js */

// Load dependent modules
var Class = require('../../vendor/Class');


var BaseStrategy = Class.extend({

	getType : function () {
		throw new Error("'getType' must be implemented by child class.");
	},

	canUse : function () {
		throw new Error("'canUse' must be implemented by child class.");
	},

	isComposable : function () {
		return true;
	},

	start : function ()
	{
		throw new Error("'start' must be implemented by child class.");
	},

	stop : function ()
	{
		throw new Error("'stop' must be implemented by child class.");
	}

});


// Export public APIs
module.exports = BaseStrategy;

},{"../../vendor/Class":93}],18:[function(require,module,exports){
/* cvp/detectors/pagefocusdetector.js */

// Load dependent modules
var Class = require('../../vendor/Class');
var Utils = require('../utils');
var Log = require('../log');
var PageVisibilityStrategy = require('./pagevisibilitystrategy');
var PageFocusStrategy = require('./pagefocusstrategy');
var PageFocusInStrategy = require('./pagefocusinstrategy');


var _logger = Log.getLogger();

/**
 * A PageFocusDetector class that detects whether the page becomes hidden or visible due to
 * user switching tab, or minimizing/restore. For older versions of browser, switching process
 * will be counted as out focus which is correct but not necessarily hidden.
 * @class Contains variables and methods facilitating the detection, including start/stop
 * @extends CVP.Utils
 */
var PageFocusDetector = Class.extend({

	/**
	 * Initializes the PageFocusDetector.
	 * @param {Object} options
	 *   - callback: The callback to be notified.
	 *   - strategies: The strategy to use.
	 *   - mode: [default: AGGRESSIVE]
	 * @memberOf CVP.Utils.PageFocusDetector
	 */
	init : function (options)
	{
		options = Utils.extend({
			callback : null,
			strategies : null,
			mode : PageFocusDetector.AGGRESSIVE
		}, options);

		this._mode = options.mode;
		this._callback = options.callback;

		this._visible = null;
		this._onChangeBind = Utils.bind(this._onChange, this);

		this._availableStrategies = options.strategies || [
			new PageVisibilityStrategy({ callback : this._onChangeBind }),
			new PageFocusStrategy({ callback : this._onChangeBind }),
			new PageFocusInStrategy({ callback : this._onChangeBind })
		];
		this._strategies = [];

		for (var i = 0; i < this._availableStrategies.length; ++i)
		{
			if (this._availableStrategies[i].canUse()) {
				// If we've already selected one or more strategies, only choose additional strategies
				// if they're marked as composable
				if (this._strategies.length > 0
					&& !this._availableStrategies[i].isComposable()) {
					continue;
				}

				this._strategies.push(this._availableStrategies[i]);
				_logger.log("[PageFocusDetector] Using detection strategy: " + this._availableStrategies[i].getType());

				if (this._mode !== PageFocusDetector.AGGRESSIVE) {
					_logger.log("[PageFocusDetector] Conservative mode - sticking with one strategy");
					break;
				}
			}
		}

		if (!this._strategies.length) {
			_logger.log("[PageFocusDetector] Warning - no valid detection strategies found.");
		}
	},

	_onChange : function (visible)
	{
		if (this._visible != null
			&& this._visible === visible) {
			return;
		}

		this._visible = visible;
		if (this._callback)
			this._callback(visible);
	},

	/**
	 * start detecting by registering proper listeners based on browser and browser version this page is running.
	 * @memberOf CVP.Utils.PageFocusDetector
	 */
	start : function ()
	{
		Utils.each(this._strategies, function (strategy) {
			strategy.start();
		});
	},

	/**
	 * start detecting by removing listeners registered.
	 * @memberOf CVP.Utils.PageFocusDetector
	 */
	stop : function ()
	{
		Utils.each(this._strategies, function (strategy) {
			strategy.stop();
		});
	}

});

PageFocusDetector.CONSERVATIVE = "conservative";
PageFocusDetector.AGGRESSIVE = "aggressive";


// Export public APIs
module.exports = PageFocusDetector;

},{"../../vendor/Class":93,"../log":28,"../utils":50,"./pagefocusinstrategy":19,"./pagefocusstrategy":20,"./pagevisibilitystrategy":22}],19:[function(require,module,exports){
/* cvp/detectors/pagefocusinstrategy.js */

// Load dependent modules
var BaseStrategy = require('./basestrategy');
var Utils = require('../utils');
var Browser = require('../browser');
var Events = require('../events');


var PageFocusInStrategy = BaseStrategy.extend({

	init : function (options)
	{
		options = Utils.extend({
			callback : null
		}, options);

		this._delayFireBlur = false;
		this._timerId = null;
		this._callback = options.callback;

		this._onFocusFiredBind = Utils.bind(this._onFocusFired, this);
		this._onBlurFiredBind = Utils.bind(this._onBlurFired, this);
		this._fireBlurBind = Utils.bind(this._fireBlur, this);
	},

	getType : function ()
	{
		return "PageFocusInStrategy";
	},

	canUse : function ()
	{
		return (Browser.isBrowser(Browser.IE) === true);
	},

	_onFocusFired : function ()
	{
		if (this._delayFireBlur)
		{
			if (this._timerId)
				window.clearTimeout(this._timerId);

			this._delayFireBlur = false;
			return;
		}

		this._onChange(true);
	},

	_onBlurFired : function ()
	{
		this._delayFireBlur = true;
		this._timerId = window.setTimeout(this._fireBlurBind, 500);
	},

	_fireBlur : function ()
	{
		if (this._delayFireBlur && this._callback)
			this._onChange(false);

		this._delayFireBlur = false;
	},

	_onChange : function (visible)
	{
		if (this._callback)
			this._callback(visible);
	},

	start : function ()
	{
		Events.addListener(document, "focusin", this._onFocusFiredBind);
		Events.addListener(document, "focusout", this._onBlurFiredBind);
	},

	stop : function ()
	{
		Events.removeListener(document, "focusin", this._onFocusFiredBind);
		Events.removeListener(document, "focusout", this._onBlurFiredBind);
	}

});


// Export public APIs
module.exports = PageFocusInStrategy;

},{"../browser":12,"../events":26,"../utils":50,"./basestrategy":17}],20:[function(require,module,exports){
/* cvp/detectors/pagefocusstrategy.js */

// Load dependent modules
var BaseStrategy = require('./basestrategy');
var Utils = require('../utils');
var Browser = require('../browser');
var Events = require('../events');


var PageFocusStrategy = BaseStrategy.extend({

	init : function (options)
	{
		options = Utils.extend({
			callback : null
		}, options);

		this._delayFireBlur = false;
		this._timerId = null;
		this._callback = options.callback;

		this._onFocusFiredBind = Utils.bind(this._onFocusFired, this);
		this._onBlurFiredBind = Utils.bind(this._onBlurFired, this);
		this._fireBlurBind = Utils.bind(this._fireBlur, this);
	},

	getType : function ()
	{
		return "PageFocusStrategy";
	},

	canUse : function ()
	{
		return (Browser.isBrowser(Browser.IE) === false);
	},

	_onFocusFired : function ()
	{
		if (this._delayFireBlur)
		{
			if (this._timerId)
				window.clearTimeout(this._timerId);

			this._delayFireBlur = false;
			return;
		}

		this._onChange(true);
	},

	_onBlurFired : function ()
	{
		this._delayFireBlur = true;
		this._timerId = window.setTimeout(this._fireBlurBind, 500);
	},

	_fireBlur : function ()
	{
		if (this._delayFireBlur && this._callback)
			this._onChange(false);

		this._delayFireBlur = false;
	},

	_onChange : function (visible)
	{
		if (this._callback)
			this._callback(visible);
	},

	start : function ()
	{
		Events.addListener(window, "focus", this._onFocusFiredBind);
		Events.addListener(window, "blur", this._onBlurFiredBind);
	},

	stop : function ()
	{
		Events.removeListener(window, "focus", this._onFocusFiredBind);
		Events.removeListener(window, "blur", this._onBlurFiredBind);
	}

});


// Export public APIs
module.exports = PageFocusStrategy;

},{"../browser":12,"../events":26,"../utils":50,"./basestrategy":17}],21:[function(require,module,exports){
/* cvp/detectors/pageorientationdetector.js */

// Load dependent modules
var Class = require('../../vendor/Class');
var Utils = require('../utils');
var Events = require('../events');


/**
 * Monitor for page orientation changes
 */
var PageOrientationDetector = Class.extend({
	init : function (options)
	{
		options = Utils.extend({
			callback : function () {}
		}, options);

		this._callback = options.callback;
		// We could debounce this, but given the typical orientation usage, probably more useful not to
		this.handleOrientationBind = Utils.bind(this._handleScroll, this);
	},

	_handleScroll : function ()
	{
		var orientation;
		switch (window.orientation)
		{
			case -90:
			case 90:
				orientation = "landscape";
				break;

			default:
				orientation = "portrait";
				break;
		}

		if (this._callback) {
			this._callback(orientation);
		}
	},

	start : function ()
	{
		Events.addListener(window, "orientationchange", this.handleOrientationBind);
	},

	stop : function ()
	{
		Events.removeListener(window, "orientationchange", this.handleOrientationBind);
	}
});


// Export public APIs
module.exports = PageOrientationDetector;

},{"../../vendor/Class":93,"../events":26,"../utils":50}],22:[function(require,module,exports){
/* cvp/detectors/pagevisibilitystrategy.js */

// Load dependent modules
var BaseStrategy = require('./basestrategy');
var Utils = require('../utils');
var Events = require('../events');


var PageVisibilityStrategy = BaseStrategy.extend({

	init : function (options)
	{
		options = Utils.extend({
			callback : null
		}, options);

		this._callback = options.callback;
		this._eventName = null;
		this._propName = null;

		var available = [
			{ event : "msvisibilitychange", prop : "msHidden" },
			{ event : "webkitvisibilitychange", prop : "webkitHidden" },
			{ event : "mozvisibilitychange", prop : "mozHidden" },
			{ event : "visibilitychange", prop : "hidden" }
		];

		for (var i = 0; i < available.length; ++i) {
			if (available[i].prop in document) {
				this._propName = available[i].prop;
				this._eventName = available[i].event;
			}
		}

		this._onChangeBind = Utils.bind(this._onChange, this);
	},

	getType : function ()
	{
		return "PageVisibilityStrategy";
	},

	canUse : function ()
	{
		return (this._propName != null);
	},

	_onChange : function ()
	{
		var isHidden = this._getHiddenValue();

		if (this._callback)
			this._callback(!isHidden);
	},

	_getHiddenValue : function ()
	{
		return document[this._propName];
	},

	start : function ()
	{
		Events.addListener(document, this._eventName, this._onChangeBind);
	},

	stop : function ()
	{
		Events.removeListener(document, this._eventName, this._onChangeBind);
	}

});


// Export public APIs
module.exports = PageVisibilityStrategy;

},{"../events":26,"../utils":50,"./basestrategy":17}],23:[function(require,module,exports){
/* cvp/detectors/scrolldetector.js */

// Load dependent modules
var Class = require('../../vendor/Class');
var Utils = require('../utils');
var Events = require('../events');
var Log = require('../log');


var _logger = Log.getLogger();

/**
 * A ScrollDetector class that detects when the window has been scrolled or resized.
 * On trigger, a callback will be invoked with the current scroll and window position values.
 *
 * @extends CVP.Class
 */
var ScrollDetector = Class.extend({

	/**
	 * Initializes the ScrollDetector.
	 * @param {Object} options
	 *   - callback: function to fire after delay
	 *   - delay: milliseconds between firing [default: 500]
	 * @memberOf CVP.Utils.ScrollDetector
	 */
	init : function (options)
	{
		options = Utils.extend({
			callback : function () {},
			delay : 500
		}, options);

		this._callback = options.callback;
		this._interval = options.interval;

		_logger.log("[ScrollDetector] debouncing with a delay of: " + options.delay);
		this.handleScrollBind = Utils.debounce(Utils.bind(this._handleScroll, this), options.delay);
	},

	/**
	 * handle user scroll and resize event. testing to finger out whether target element is out of viewport or into the viewport
	 * reporting accordingly.
	 * @memberOf CVP.Utils.ViewportDetector
	 */
	_handleScroll : function ()
	{
		var curScrollTop = window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop;
		var curScrollLeft = window.pageXOffset || document.documentElement.scrollLeft || document.body.scrollLeft;
		var windowW = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
		var windowH = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;

		if (this._callback) {
			this._callback({
				scrollTop : curScrollTop,
				scrollLeft : curScrollLeft,
				windowWidth : windowW,
				windowHeight : windowH
			});
		}
	},

	/**
	 * start detecting by registering listener for "scroll" and "resize".
	 * @memberOf CVP.Utils.ViewportDetector
	 */
	start : function ()
	{
		Events.addListener(window, "scroll", this.handleScrollBind);
		Events.addListener(window, "resize", this.handleScrollBind);
	},

	/**
	 * stop detecting by removing listener for "scroll" and "resize".
	 * @memberOf CVP.Utils.ViewportDetector
	 */
	stop : function ()
	{
		Events.removeListener(window, "scroll", this.handleScrollBind);
		Events.removeListener(window, "resize", this.handleScrollBind);
	}

});


// Export public APIs
module.exports = ScrollDetector;

},{"../../vendor/Class":93,"../events":26,"../log":28,"../utils":50}],24:[function(require,module,exports){
/* cvp/detectors/viewportdetector.js */

// Load dependent modules
var Class = require('../../vendor/Class');
var Utils = require('../utils');
var ScrollDetector = require('./scrolldetector');


/**
 * A ViewportDetector class that detects whether a particular element has been scrolled or resized
 * out of or into viewport.
 * @class Contains variables and methods facilitating the detection, including start/stop
 * @extends CVP.Utils
 */
var ViewportDetector = Class.extend({

	/**
	 * Initializes the ViewportDetector.
	 * @param {Object} options
	 *   - {String|Object} elementId - The target element detecting of, can be either the element id or the element object.
	 *   - {Function} callback - The callback to be notified.
	 * @memberOf CVP.Utils.ViewportDetector
	 */
	init : function (options)
	{
		options = Utils.extend({
			delay : 500
		}, this._validateTargetOptions(options));

		this._targets = [];
		this.addTarget(options);

		this._scrollDetector = new ScrollDetector({
			callback : Utils.bind(this._onScroll, this),
			delay : options.delay
		});
	},

	/**
	 * Validate the options for adding a target
	 * @param {Object} options
	 *   - {String|Object} elementId - The target element detecting of, can be either the element id or the element object.
	 *   - {Function} callback - The callback to be notified.
	 * @returns {Object} Validated options config.
	 */
	_validateTargetOptions : function (options) {
		return Utils.extend({
			elementId : null,
			callback : function () {}
		}, options);
	},

	/**
	 * Add a target to check against the scrolled / resized state
	 * @param {Object} options
	 *   - {String|Object} elementId - The target element detecting of, can be either the element id or the element object.
	 *   - {Function} callback - The callback to be notified.
	 */
	addTarget : function (options) {
		options = this._validateTargetOptions(options);

		if (options.elementId == null)
			return;

		var elementId = options.elementId;
		var callback = options.callback;

		this._targets.push({
			elementId : elementId,
			callback : callback,
			clipped : false
		});
	},

	/**
	 * handle user scroll and resize event. testing to finger out whether target element is out of viewport or into the viewport
	 * reporting accordingly.
	 * @param {Object} scrollObj - ScrollDetector
	 * @memberOf CVP.Utils.ViewportDetector
	 */
	_onScroll : function (scrollObj)
	{
		var curScrollTop = scrollObj.scrollTop;
		var curScrollLeft = scrollObj.scrollLeft;
		var windowW = scrollObj.windowWidth;
		var windowH = scrollObj.windowHeight;

		function getOffset(elem) {
			var doc, docElem,
				box = { top : 0, left : 0 };

			doc = elem && elem.ownerDocument;
			if (!doc)
				return null;

			docElem = doc.documentElement;

			if (typeof elem.getBoundingClientRect !== "undefined") {
				box = elem.getBoundingClientRect();
			}

			return {
				top : (box.top + curScrollTop - (docElem.clientTop || 0)),
				left : (box.left + curScrollLeft - (docElem.clientLeft || 0)),
				width : elem.offsetWidth,
				height : elem.offsetHeight
			};
		}

		function handleClipped(currentlyClipped, lastClipped, callback)
		{
			if (currentlyClipped != lastClipped
				&& callback)
				callback(!currentlyClipped);

			return currentlyClipped;
		}

		// For each target, determine if it's status has changed
		Utils.each(this._targets, function (target /*, i, obj*/) {
			var element = target.elementId;
			if (Utils.isString(element))
				element = Utils.byId(element);

			if (!element)
				return;

			var offset = getOffset(element);
			if (!offset)
				return;

			if (curScrollTop >= (offset.top + offset.height)
				|| curScrollLeft >= (offset.left + offset.width)
				|| offset.top >= curScrollTop + windowH
				|| offset.left >= curScrollLeft + windowW)
			{
				target.clipped = handleClipped(true, target.clipped, target.callback);
			}
			else if ((offset.top > curScrollTop)
				&& ((offset.top + offset.height) < (curScrollTop + windowH)))
			{
				target.clipped = handleClipped(false, target.clipped, target.callback);
			}
		});

		// function log(target, offset) {
		// 	_logger.log("target - " + target.elementId);
		// 	_logger.log("	", curScrollTop, curScrollLeft, windowH, windowW);
		// 	_logger.log("	", offset.top, offset.left, offset.height, offset.width);
		// }
	},

	/**
	 * start detecting by registering listener for "scroll" and "resize".
	 * @memberOf CVP.Utils.ViewportDetector
	 */
	start : function ()
	{
		this._scrollDetector.start();
	},

	/**
	 * stop detecting by removing listener for "scroll" and "resize".
	 * @memberOf CVP.Utils.ViewportDetector
	 */
	stop : function ()
	{
		this._scrollDetector.stop();
	}

});


// Export public APIs
module.exports = ViewportDetector;

},{"../../vendor/Class":93,"../utils":50,"./scrolldetector":23}],25:[function(require,module,exports){
/* html5/errors.js */

var ErrorType = {
	ASPEN_REPORTING : 'AS',
	CONTENT_ERROR : 'CT',
	AD_ERROR : 'AD',
	PLAYER_ERROR : 'PL',
	CONTAINER_ERROR : 'CR',
	EXTERNAL_ERROR : 'EX'
};

var PlayerErrorSubType = {
	PLAYER_CONFIG_ERROR : 'PC',
	PLAYER_INIT_ERROR : 'PI',
	VIDEO_ASSET_ERROR : 'VA',
	NOT_AVAILABLE_ : 'NA',
	CONTENT_TOKEN_SERVICE_ERROR : 'CA',
	TOKEN_SERVICE_ERROR : 'TS',
	CLOSED_CAPTION_ERROR : 'CC'
};

var AdErrorSubType = {
	AD_LOAD_ERROR : 'LD',
	AD_INIT_ERROR : 'IN',
	AD_RUNTIME_ERROR : 'RT',
	FW_NOTIFICATION : 'FN'
};

var ErrorLevel = {
	HIGH : 'hi',
	MEDIUM : 'md',
	LOW : 'lo'
};

var ErrorMessage = {
	AD_TYPE_NOT_TRAFFICKED : 'ad type not trafficked',
	NO_AD_PLAYED_AT_AD_SPOT : 'slot finished but no ad to play in slot',
	AD_SKIPPED_DUE_TO_SENSITIVITY : 'ad skipped due to ad sensitivity setting',
	AD_SERVER_NOT_CONFIGURED : 'no ad server is configured',
	PLAY_CALL_2ND_PARAM_DISABLED_AD : 'ad was disabled via optional second param on play call',
	AD_SKIPPED_BY_JAVASCRIPT : 'skipped as directed by the javascript callback',
	AD_RULE_FAILED : 'skipped because did not pass ad rules',
	AD_PLAYBACK_ERROR : 'unknown ad playback error',
	AD_EXISTS_BUT_FAILED : 'ad exists but failed to play in slot',
	AD_INTERRUPTED : 'slot was interrupted'
};

function determineAdErrorCode(message) {
	var errorCode = '' + ErrorType.AD_ERROR + AdErrorSubType.AD_RUNTIME_ERROR + ErrorLevel.LOW;

	switch (message) {
		case ErrorMessage.AD_TYPE_NOT_TRAFFICKED:
			return '' + ErrorType.AD_ERROR + AdErrorSubType.AD_RUNTIME_ERROR + ErrorLevel.HIGH + '4000';
		case ErrorMessage.NO_AD_PLAYED_AT_AD_SPOT:
			return '' + ErrorType.AD_ERROR + AdErrorSubType.AD_RUNTIME_ERROR + ErrorLevel.HIGH + '4001';
		case ErrorMessage.AD_RULE_FAILED:
			return errorCode + '4006';
		case ErrorMessage.AD_SKIPPED_DUE_TO_SENSITIVITY:
			return errorCode + '4005';
		case ErrorMessage.AD_SKIPPED_BY_JAVASCRIPT:
			return errorCode + '4004';
		case ErrorMessage.PLAY_CALL_2ND_PARAM_DISABLED_AD:
			return errorCode + '4003';
		case ErrorMessage.AD_SERVER_NOT_CONFIGURED:
			return '' + ErrorType.AD_ERROR + AdErrorSubType.AD_RUNTIME_ERROR + ErrorLevel.HIGH + '4002';
		case ErrorMessage.AD_EXISTS_BUT_FAILED:
			return '' + ErrorType.AD_ERROR + AdErrorSubType.AD_RUNTIME_ERROR + ErrorLevel.HIGH + '4007';
		default:
			return '' + ErrorType.AD_ERROR + AdErrorSubType.AD_RUNTIME_ERROR + ErrorLevel.HIGH + '4009';
	}
}


// Export public APIs
module.exports = {
	ErrorType : ErrorType,
	PlayerErrorSubType : PlayerErrorSubType,
	AdErrorSubType : AdErrorSubType,
	ErrorLevel : ErrorLevel,
	ErrorMessage : ErrorMessage,
	determineAdErrorCode : determineAdErrorCode
};

},{}],26:[function(require,module,exports){
/* cvp/events.js */

// Load dependent modules
var swfobject = require('../vendor/swfobject');
var Utils = require('./utils');
var CustomEvent = require('./customevent');


/**
 * Will fire an event as soon as the DOM is ready
 * @param {Function} handler The function to execute on ready.
 */
function onReady(handler)
{
	swfobject.addDomLoadEvent(handler);
}


// Modified version of http://therealcrisp.xs4all.nl/upload/addEvent_dean.html
// written by Dean Edwards, 2005
// with input from Tino Zijdel - crisp@xs4all.nl
// http://dean.edwards.name/weblog/2005/10/add-event/
var _guid = 1;
function addListener(element, type, handler)
{
	if (element.addEventListener)
		element.addEventListener(type, handler, false);
	else
	{
		if (!handler.$$guid) handler.$$guid = _guid++;
		if (!element._cvpStoredEvents) element._cvpStoredEvents = {};
		var handlers = element._cvpStoredEvents[type];
		if (!handlers)
		{
			handlers = element._cvpStoredEvents[type] = {};
			if (element['on' + type]) handlers[0] = element['on' + type];
			element['on' + type] = _handleEvent;
		}

		handlers[handler.$$guid] = handler;
	}
}

function removeListener(element, type, handler)
{
	if (element.removeEventListener)
		element.removeEventListener(type, handler, false);
	else if (element._cvpStoredEvents && element._cvpStoredEvents[type] && handler.$$guid)
		delete element._cvpStoredEvents[type][handler.$$guid];
}

function _handleEvent(event)
{
	event = event || _fixEvent(window.event);
	var returnValue = true;
	var handlers = this._cvpStoredEvents[event.type];
	var i;

	for (i in handlers)
	{
		if (Utils.hasOwn(handlers, i))
		{
			this.$$handler = handlers[i];
			if (this.$$handler(event) === false) returnValue = false;
		}
	}

	if (this.$$handler) this.$$handler = null;

	return returnValue;
}

function _fixEvent(event)
{
	event.preventDefault = _fixEvent._preventDefault;
	event.stopPropagation = _fixEvent._stopPropagation;
	return event;
}

_fixEvent._preventDefault = function ()
{
	this.returnValue = false;
};

_fixEvent._stopPropagation = function ()
{
	this.cancelBubble = true;
};


// Export public APIs
exports.onReady = onReady;
exports.addListener = addListener;
exports.removeListener = removeListener;
exports.CustomEvent = CustomEvent;

},{"../vendor/swfobject":95,"./customevent":16,"./utils":50}],27:[function(require,module,exports){
/* cvp/globals.js */

/* eslint no-eval: 0 */

/**
 * Global function for actionscript to call.
 * Also used by string replacement.
 *
 * @private
 * @param theObj {Object} The component to search.
 * @param thePath {String} A dot-notation "path" object reference.
 * @returns {Any} The value at theObj.thePath, or undefined.
 */

function cvpSearchTheClient(_object, _path) {
  var undef;
  if (_object === undef) return undef;
  var root = _object;
  if (root === null) root = window;
  try {
    return eval('root.' + _path);
  } catch (e) {
    return undef;
  }
}

window.cvpSearchTheClient = cvpSearchTheClient;

exports.cvpSearchTheClient = cvpSearchTheClient;

},{}],28:[function(require,module,exports){
/* cvp/log.js */

/* eslint no-console:0 */

// Load dependent modules
var App = require('../core/app');


var OBJECT = 'object';
var FUNCTION = 'function';
var STRING = 'string';
var NUMBER = 'number';

var __slice = Array.prototype.slice;
var fnProto = Function.prototype;
var fnBind = fnProto.bind;

var consoleIsObject = typeof console === OBJECT;
var functionHasBind = typeof fnBind === FUNCTION;

var log = (function () {
	if (consoleIsObject) {
		var consoleLogType = typeof console.log;
		if (consoleLogType === FUNCTION || consoleLogType === OBJECT && functionHasBind) {
			return fnBind.call(console.log, console);
		}
	}

	return fnProto;
}());

var now = (
	typeof Date.now === FUNCTION
	? Date.now
	: function () {
		return (new Date()).getTime();
	}
);

var start = now();

// Log message prefix: [{appId}|{windowId}|{CVP instance id}]
var appId = 'CVP';
var windowId = window.self === window.top ? '_top' : window.name || '';

// The firebug console API seen in most browsers.
var consoleMethods = ['error', 'warn', 'info', 'debug', 'log'];

// The log4javascript API
// -- adding a couple things not typically seen in browser console.
var log4jsMethods = [].concat('fatal', consoleMethods, 'trace');

// Setting up logger levels
// -- bookending the various levels with silent and verbose options.
var levelNames = [].concat('none', log4jsMethods, 'all');

// Dynamically creating list of logger levels.
var levels = {};
for (var method = null, i = 0, endi = levelNames.length; i < endi; ++i) {
	method = levelNames[i];
	levels[method.toUpperCase()] = i;
}

// Default to logging warnings and errors only.
var defaultLevel = levels.WARN;

// RegExp to test against "dmtdebug={something}".
var silent = /NO|NONE|OFF|SILENT|QUIET/;

var getLevelNum = function (level) {
	level = level.toUpperCase();
	return levels[level] || (silent.test(level) ? levels.NONE : levels.ALL);
};

var determineLevel = function (level) {
	switch (typeof level) {
		case STRING: return getLevelNum(level);
		case NUMBER: return level;
		default: return defaultLevel;
	}
};

// If the `dmtdebug` flag is present in the query string or hash,
// use the value following the equal sign as the default log level.
if (App.DEBUG_LEVEL) {
	defaultLevel = determineLevel(App.DEBUG_LEVEL);
}

// CVP.Log API
var api = {};

function buildLogMethod(fn) {
	if (consoleIsObject) {
		var methodType = typeof console[fn];
		api[fn] = (
			methodType === FUNCTION || methodType === OBJECT && functionHasBind
			? fnBind.call(console[fn], console)
			: log
		);
	}
}

// Populating pass-thru methods and setting up adapters in API.
for (var j = 0, endj = log4jsMethods.length; j < endj; ++j) (buildLogMethod)(log4jsMethods[j]);


// ## Logger
// An instance of a logger should be created for each CVP instance.

function Logger(id) {
	var _this = this;
	this.id = id || '';
	this.setLevel(defaultLevel);
	this._logger = {};
	for (var key in api) (function (name, fn) {

		// Set up native console logger.
		_this._logger[name] = function () {
			var args, level;
			level = name.toUpperCase();
			if (_this.getLevel() >= levels[level]) {
				args = __slice.call(arguments);
				args.unshift("[" + appId + "|" + windowId + "|" + _this.id + "|" + (now() - start) + "]", level);
				return fn.apply(_this, args);
			}
		};

		// Set up aliases -- Logger.log, Logger.warn, etc. --
		// which can be replaced by log4javascript
		_this[name] = function () {
			return _this._logger[name].apply(_this._logger, arguments);
		};

	})(key, api[key]);
}

Logger.prototype.setLevel = function (level) {
	this.loggerLevel = determineLevel(level);
	return this.loggerLevel;
};

Logger.prototype.getLevel = function () {
	return this.loggerLevel;
};


// Factory-like functionality
var registeredLoggers = [];

var getLogger = function (instanceId) {
	if (instanceId == null) {
		instanceId = '';
	}

	registeredLoggers[instanceId] = registeredLoggers[instanceId] || new Logger(instanceId);

	return registeredLoggers[instanceId];
};


// Return the external (public) API.
module.exports = {
	Level : levels,
	getDefaultLevel : function () {
		return defaultLevel;
	},
	setDefaultLevel : function (level) {
		defaultLevel = determineLevel(level);
		return defaultLevel;
	},
	getLogger : getLogger
};

},{"../core/app":8}],29:[function(require,module,exports){
/* cvp/main.js */

// Load dependent modules
var App = require('../core/app');
var CVP = require('./static');
var CVPObject = require('./core');
var Shared = require('./shared');
var Utils = require('./utils');


CVP.Class = require('../vendor/Class');
CVP.swfobject = require('../vendor/swfobject');

CVP.Utils                     = Utils;
CVP.Utils.CommandQueue        = require('./util/commandqueue');
CVP.Utils.Asset               = require('./util/asset');
CVP.Utils.Dependency          = require('./util/dependency');
CVP.Utils.DependencyManager   = require('./util/dependencymanager');
CVP.Utils.JsonConverter       = require('./util/jsonconverter');
CVP.Utils.XML                 = require('./util/xml');
CVP.Utils.Timing              = require('./util/timing');
CVP.Utils.ViewportDetector    = require('./detectors/viewportdetector');
CVP.Utils.PageFocusDetector   = require('./detectors/pagefocusdetector');

CVP.Ajax = require('./ajax');

CVP.Events = require('./events');

CVP.JSON = JSON;

CVP.Log = require('./log');

CVP.Version = require('./version');
CVP.Browser = require('./browser');

CVP.Request = require('./request');

CVP.Store = require('./store');

CVP.PluginManager = require('./pluginmanager');
CVP.Plugins = require('./plugins');

CVP.Performance = require('./performance');


/**
 * CVP Factory
 */

CVP.init = function(setup) {
	var key, val;

	if (!Utils.isSimpleObject(setup)) {
		throw new Error("CVP.init() requires one param: a simple object");
	}

	for (key in setup) {
		if (Utils.hasOwn(setup, key)) {
			val = setup[key];
			if (Utils.isSimpleObject(val)) {
				App._cvpsetup[key] = Utils.extend(App._cvpsetup[key], val);
			}
			// else
			// if (Utils.isFunc(val)) {
			// 	_cvpsetup[key] = val();
			// }
			else {
				App._cvpsetup[key] = val;
			}
		}
	}
};

CVP.createPlayer = function(options) {
	return new CVPObject(options);
};

// CVP.createAuthManager = function(options) {
//
// };

CVP.getSessionToken = function () {
	return 'deprecated';
};


var CVPconstructor = function (options) {
	return CVP.createPlayer(options);
};


Shared.addBeforeUnLoadEvent(CVP.cleanup);


// Export public APIs
module.exports = Utils.extend(CVPconstructor, CVP);

},{"../core/app":8,"../vendor/Class":93,"../vendor/swfobject":95,"./ajax":10,"./browser":12,"./core":15,"./detectors/pagefocusdetector":18,"./detectors/viewportdetector":24,"./events":26,"./log":28,"./performance":30,"./pluginmanager":33,"./plugins":34,"./request":36,"./shared":37,"./static":38,"./store":39,"./util/asset":40,"./util/commandqueue":41,"./util/dependency":44,"./util/dependencymanager":45,"./util/jsonconverter":46,"./util/timing":48,"./util/xml":49,"./utils":50,"./version":51}],30:[function(require,module,exports){
/* cvp/performance.js */

/**
 * A simple wrapper around window.performance, with support
 * for calculating load metrics for a given CVP instance
 */

var Utils = require('./utils');


var hasPerf = false,
	hasTiming = false,
	hasEntries = false,
	hasMark = false,
	hasMeasure = false,
	hasNow = false;

if ('performance' in window) {
	hasPerf = true;

	if ('timing' in window.performance)
		hasTiming = true;

	if ('getEntries' in window.performance)
		hasEntries = true;

	if ('mark' in window.performance)
		hasMark = true;

	if ('measure' in window.performance)
		hasMeasure = true;

	if ('now' in window.performance)
		hasNow = true;
}

var wp = hasPerf
	? window.performance
	: null;

var Performance = {

	has : function(type) {
		switch(type) {
			case "perf":
				return hasPerf;

			case "timing":
				return hasTiming;

			case "entries":
				return hasEntries;

			case "mark":
				return hasMark;

			case "measure":
				return hasMeasure;

			case "now":
				return hasNow;

			default:
				return false;
		}
	},

	getTiming : function() {
		if (!hasTiming)
			return {};

		return wp.timing;
	},

	getEntries : function() {
		if (!hasEntries)
			return [];

		return wp.getEntries();
	},

	getEntriesByType : function(type) {
		if (!hasEntries)
			return [];

		return wp.getEntriesByType(type);
	},

	getEntriesByName : function(name) {
		if (!hasEntries)
			return [];

		return wp.getEntriesByName(name);
	},

	mark : function(ns, name) {
		if (!hasMark)
			return null;

		var markName = ns + "::" + name;
		wp.mark(markName);
		return markName;
	},

	measure : function(ns, name, mark1, mark2) {
		if (!hasMeasure)
			return null;

		var measureName = ns + "::" + name;
		wp.measure(measureName, mark1, mark2);
		return measureName;
	},

	getMarks : function(ns) {
		if (!hasMark)
			return [];

		return this.getEntriesByType("mark");
	},

	getMeasurements : function(ns) {
		if (!hasMeasure)
			return [];

		return this.getEntriesByType("measure");
	},

	/**
	 * Calculate the load metrics for a given CVP instance
	 *
	 * Note that we don't have the information to tie network to a particular
	 * CVP instance, so for now, this will act on all CVP-related traffic.
	 * This won't be fully accurate when multiple instances are on a page.
	 *
	 * Regarding embed and ready times, we'll eventually move to retrieving them
	 * via marks instead of passing explicitly, pending evaluating initial results
	 */
	getCVPLoadMetrics : function(cvpId, embedRequestTime, cvpReadyTime) {

		// Data to calculate and report
		// Values that are initially null are dependent on perf being available,
		// and will be populated if possible
		var data = {
			cvp : {
				embedRequestTime : embedRequestTime,
				cvpReadyTime : cvpReadyTime,
				navStartToEmbedRequest : null,
				embedRequestToReady : (cvpReadyTime - embedRequestTime),

				flash : {
					embedRequestToContainerRequest : null,
					containerRequestToCoreComplete : null,
					coreCompleteToReady : null
				}
			},
			timing : null,
			totalEntries : null,
			cvpEntries : null
		};

		if (hasTiming) {
			data.timing = this.getTiming();
		}

		if (hasEntries) {
			var entries = this.getEntriesByType("resource");

			data.cvp.navStartToEmbedRequest = (data.cvp.embedRequestTime - data.timing.navigationStart);

			var map = function(coll, func) {
				return Utils.map(coll, func);
			};

			var filter = function(coll, func) {
				return Utils.filter(coll, func);
			};

			var createFilterEntryByName = function(regex) {
				return function(entry) {
					return regex.test(entry.name);
				}
			}

			var createCVPResourceRegex = function() {
				var cdn = "cdn\\.turner\\.com\\/xslo\\/cvp";

				// All resources that may be places other
				// than our CDN
				var exts = "\\.(xml|json)"
				var resources = [
					"cvp\\_(\\w)+\\_container\\.swf",
					"mapping{ext}",
					"appConfig{ext}",
					"bundle\\_[\\d\\.]{ext}",
					"container{ext}",
					"config{ext}",
					"ad_policy{ext}",
					"tracking_policy{ext}",
					"ui_policy{ext}",
					"restriction_policy{ext}"
				];

				var strReplace = function(str) {
					return str.replace(/\{ext\}/g, exts);
				};

				var regexStr = [cdn].concat(map(resources, strReplace)).join("|");
				return new RegExp(regexStr, "i");
			};

			var toCVP = createFilterEntryByName(createCVPResourceRegex());

			var shortEntry = function(entry, i, arr) {
				return {
					name : entry.name,
					startTime : entry.startTime,
					duration : entry.duration,
					requestIndex : i
				};
			};

			var toCVPEntries = function(coll) {
				coll = filter(coll, toCVP);
				return map(coll, shortEntry);
			};

			var byStartTime = function(a, b) {
				if (a.startTime < b.startTime)
    				return -1;
  				if (a.startTime > b.startTime)
    				return 1;
  				return 0;
			}

			var getStartTime = function(regex, entries) {
				entries = filter(entries, createFilterEntryByName(regex))

				return (entries.length
					? entries[0].startTime
					: -1);
			}

			var getEndTime = function(regex, entries) {
				entries = filter(entries, createFilterEntryByName(regex))

				return (entries.length
					? entries[0].startTime + entries[0].duration
					: -1);
			};

			// Send the total # of entries
			data.totalEntries = entries.length;

			// Filter to just the CVP-relevant entries
			data.cvpEntries = toCVPEntries(entries).sort(byStartTime);

			// Time from embed request to container request
			var containerStartTime = getStartTime(/cvp\_(\w)+\_container\.swf/, data.cvpEntries);
			if (containerStartTime !== -1) {
				data.cvp.flash.embedRequestToContainerRequest = (containerStartTime - data.cvp.navStartToEmbedRequest);
			}

			// Time from container request to core binaries complete
			var coreCompleteTime = getEndTime(/CVP\_[\d\.\_]+(\_RC\d)?\.swf/, data.cvpEntries);
			if (containerStartTime !== -1
				&& coreCompleteTime !== -1) {
				data.cvp.flash.containerRequestToCoreComplete = (coreCompleteTime - data.cvp.flash.embedRequestToContainerRequest);
			}

			// Time from core complete to cvp ready
			if (containerStartTime !== -1
				&& coreCompleteTime !== -1) {
				var effectiveReady = data.cvp.embedRequestToReady - data.cvp.flash.embedRequestToContainerRequest;
				data.cvp.flash.coreCompleteToReady = (effectiveReady - coreCompleteTime);
			}
		}

		return data;
	}
};

// Export public APIs
module.exports = Performance;


},{"./utils":50}],31:[function(require,module,exports){
/* cvp/players/html5.js */

// Load dependent modules
var CVP = require('../static');
var NullPlayer = require('./null');
var CommandQueue = require('../util/commandqueue');
var Utils = require('../utils');
var Log = require('../log');
var HTML5PlayerAPI = require('../../html5/api');


var bind = Utils.bind;
var log = Log.getLogger();

var createHTML5Instance = function (Player) {
	this._instance = new Player(this._options);
	this._instance.ePlayerLoaded.addListener(this._onPlayerLoaded, this);
	this._instance.ePlayerLoadError.addListener(this._onPlayerLoadError, this);
	this._instance.ePlayerReady.addListener(this._onPlayerReady, this);
	this._instance.ePlayerReady.addListener(this._onCVPReady, this);

	this._instance.ePlayerReady.addListener(bind(this._onCallBack, this, 'onPlayerReady'));
	this._instance.ePlayerReady.addListener(bind(this._onCallBack, this, 'onCVPReady'));
	this._instance.eContentBegin.addListener(bind(this._onCallBack, this, 'onContentBegin'));
	this._instance.eContentPlay.addListener(bind(this._onCallBack, this, 'onContentPlay'));
	this._instance.eContentPause.addListener(bind(this._onCallBack, this, 'onContentPause'));
	this._instance.eContentEnd.addListener(bind(this._onCallBack, this, 'onContentEnd'));
	this._instance.eContentComplete.addListener(bind(this._onCallBack, this, 'onContentComplete'));
	this._instance.eContentEntryLoad.addListener(bind(this._onCallBack, this, 'onContentEntryLoad'));
	this._instance.eContentEntryLoadError.addListener(bind(this._onCallBack, this, 'onContentEntryLoadError'));
	this._instance.eContentMetadata.addListener(bind(this._onCallBack, this, 'onContentMetadata'));
	this._instance.eContentPlayhead.addListener(bind(this._onCallBack, this, 'onContentPlayhead'));
	this._instance.eContentBuffering.addListener(bind(this._onCallBack, this, 'onContentBuffering'));
	this._instance.eContentResize.addListener(bind(this._onCallBack, this, 'onContentResize'));
	this._instance.eContentQueue.addListener(bind(this._onCallBack, this, 'onContentQueue'));
	this._instance.eContentQueueAutoplay.addListener(bind(this._onCallBack, this, 'onContentQueueAutoplay'));
	this._instance.eContentVolume.addListener(bind(this._onCallBack, this, 'onContentVolume'));
	this._instance.eContentError.addListener(bind(this._onCallBack, this, 'onContentError'));

	this._instance.eAdPlayhead.addListener(bind(this._onCallBack, this, 'onAdPlayhead'));
	this._instance.eAdPlay.addListener(bind(this._onCallBack, this, 'onAdPlay'));
	this._instance.eAdEnd.addListener(bind(this._onCallBack, this, 'onAdEnd'));
	this._instance.eAdError.addListener(bind(this._onCallBack, this, 'onAdError'));
	this._instance.eAdSensitive.addListener(bind(this._onCallBack, this, 'onAdSensitive'));

	this._instance.eTrackingAdStart.addListener(bind(this._onCallBack, this, 'onTrackingAdStart'));
	this._instance.eTrackingAdComplete.addListener(bind(this._onCallBack, this, 'onTrackingAdComplete'));
	this._instance.eTrackingAdProgress.addListener(bind(this._onCallBack, this, 'onTrackingAdProgress'));
	this._instance.eTrackingAdCountdown.addListener(bind(this._onCallBack, this, 'onTrackingAdCountdown'));
	this._instance.eTrackingAdClick.addListener(bind(this._onCallBack, this, 'onTrackingAdClick'));
	this._instance.eTrackingContentPlay.addListener(bind(this._onCallBack, this, 'onTrackingContentPlay'));
	this._instance.eTrackingContentBegin.addListener(bind(this._onCallBack, this, 'onTrackingContentBegin'));
	this._instance.eTrackingContentProgress.addListener(bind(this._onCallBack, this, 'onTrackingContentProgress'));
	this._instance.eTrackingContentComplete.addListener(bind(this._onCallBack, this, 'onTrackingContentComplete'));
	this._instance.eTrackingContentReplay.addListener(bind(this._onCallBack, this, 'onTrackingContentReplay'));
	this._instance.eTrackingContentSeek.addListener(bind(this._onCallBack, this, 'onTrackingContentSeek'));
	this._instance.eTrackingContentSeekEnd.addListener(bind(this._onCallBack, this, 'onTrackingContentSeekEnd'));
	this._instance.eTrackingFullscreen.addListener(bind(this._onCallBack, this, 'onTrackingFullscreen'));
	this._instance.eTrackingMuted.addListener(bind(this._onCallBack, this, 'onTrackingMuted'));
	this._instance.eTrackingPaused.addListener(bind(this._onCallBack, this, 'onTrackingPaused'));
};

var HTML5Player = NullPlayer.extend({
	init : function (options)
	{
		this._super();

		this._options = options;

		this._loadQ = new CommandQueue();
		this._loaded = false;

		// Prior attempt to load HTML5 player on-demand prevents VideoPrimer from working on Android.
		// require(['html5/api'], bind(createHTML5Instance, this));
		createHTML5Instance.call(this, HTML5PlayerAPI);
	},

	_onPlayerLoaded : function ()
	{
		log.debug("_onPlayerLoaded");
		this._loaded = true;
		this._loadQ.execute();
	},

	/**
	 * Fatal error thrown during the bootstrapping process
	 * Bubbling up to page level
	 */
	_onPlayerLoadError : function ()
	{
		log.debug("_onPlayerLoadError");
		this._onCallBack('onPlayerLoadError');
	},

	_onPlayerReady : function ()
	{
		log.debug("_onPlayerReady");
	},

	_onCVPReady : function ()
	{
		log.debug("_onCVPReady");
	},

	embed : function (containerElement)
	{
		if (!this._loaded)
		{
			log.info("queuing embed");
			this._loadQ.push(this.embed, this, arguments);
			return;
		}

		log.info("executing embed");
		this._instance.render(containerElement);
	},

	remove : function ()
	{
		this._instance.remove();
	},

	dispose : function ()
	{
		this._instance.dispose();
		this._instance = null;
	},

	playContent : function (contentId, options)
	{
		this._instance.play(contentId, options);
	},

	playFromObject : function (object, options)
	{
		this._instance.playFromObject(object, options);
	},

	replayContent : function () {
		this._instance.replay();
	},

	playNextInQueue : function () {
		this._instance.playNextInQueue();
	},

	pause : function ()
	{
		this._instance.pause();
	},

	resume : function ()
	{
		this._instance.resume();
	},

	stopContent : function ()
	{
		this._instance.stop();
	},

	queue : function (contentId, options, index)
	{
		this._instance.queue(contentId, options, index);
	},

	queueFromObject : function (object, options, index)
	{
		this._instance.queueFromObject(object, options, index);
	},

	dequeue : function (contentId)
	{
		this._instance.dequeue(contentId);
	},

	emptyQueue : function ()
	{
		this._instance.emptyQueue();
	},

	setQueueAutoplay : function (autoplay) {
		this._instance.setQueueAutoplay(autoplay);
	},

	getQueue : function ()
	{
		return this._instance.getQueue();
	},

	seek : function (seconds)
	{
		this._instance.seek(seconds);
	},

	resize : function (width, height)
	{
		this._instance.resize(width, height);
	},

	goFullscreen : function ()
	{
		this._instance.goFullscreen();
	},

	mute : function ()
	{
		this._instance.mute();
	},

	unmute : function ()
	{
		this._instance.unmute();
	},

	setVolume : function (v)
	{
		this._instance.setVolume(v);
	},

	getVolume : function ()
	{
		return this._instance.getVolume();
	},

	isMuted : function ()
	{
		return this._instance.isMuted();
	},

	getContentEntry : function (id)
	{
		return this._instance.getContentEntry(id);
	},

	setAdSection : function (ssid)
	{
		this._instance.setAdSection(ssid);
	},

	setAdKeyValue : function (key, value)
	{
		this._instance.setAdKeyValue(key, value);
	},

	getAdKeyValues : function ()
	{
		return this._instance.getAdKeyValues();
	},

	clearAdKeyValues : function ()
	{
		this._instance.clearAdKeyValues();
	},

	// TODO Implement
	//
	// CVP Docs:
	//
	//     Provides a means for telling FreeWheel which companion ads
	//     on the page are visible. This method will not hide ads;
	//     the page will still be responsible for hiding ads if desired.
	//     This is simply for telling FreeWheel which companion ads are
	//     visible so it doesn't deliver an ad that won't be seen.
	//
	//     Parameters:
	//     ads:Object (required) This is an object containing key/value pairs of companion slot name (key) and visibility boolean (value).
	//
	//     Example for CNN:
	//
	//     In cinema mode, the page should call: setAdVisibility({'medium_rectangle':false, 'mini_header': true});
	//
	//     In browse mode, the page should call: setAdVisibility({'medium_rectangle':true, 'mini_header': false});
	//
	//     Where "medium_rectangle" is the name of CNN's 300x250 ad slot and "mini_header" is the name of CNN's 234x60 ad slot.
	//
	// FreeWheel's API:
	//
	//     tv.freewheel.SDK.Slot.prototype.setVisible = function (	visible	)
	//     Set the visibility of a nontemporal slot.  By default the slot is visible.
	//
	// setAdVisibility : function (adsObject)
	// {
	// 	this._instance.setAdVisibility(adsObject);
	// },

	// TODO Implement
	// switchAdContext : function (context)
	// {
	// 	this._instance.switchAdContext(context);
	// },

	setTrackingInterval : function (interval)
	{
		this._instance.setTrackingInterval(interval);
	},

	setDataSrc : function (src)
	{
		this._instance.setDataSrc(src);
	},

	// TODO Implement
	// cannot until file selection is solved
	// or should this be used to select <file>s?
	// setFileKey : function (key) {
	// 	this._instance.setFileKey(key);
	// },

	reportAnalytics : function (eventName, data)
	{
		this._instance.reportAnalytics(eventName, data);
	},

	_onCallBack : function ()
	{
		CVP.onCallback(this._options.id, arguments);
	},

	instance : function ()
	{
		return this._instance;
	}
});


// Export public APIs
module.exports = HTML5Player;

},{"../../html5/api":58,"../log":28,"../static":38,"../util/commandqueue":41,"../utils":50,"./null":32}],32:[function(require,module,exports){
/* cvp/players/null.js */

// Load dependent modules
var Class = require('../../vendor/Class');


var NullPlayer = Class.extend({
	init : function()
	{
		var functions = "sendMessage setDefaultPlayer play playFromObject replay playNextInQueue pause resume stop queue queueFromObject dequeue emptyQueue setQueueAutoplay getQueue seek mute unmute setVolume getVolume isMuted getContentEntry goFullscreen resize setMaxBitrate switchBitrateId setAutoBitrateSwitch getAvailableBitrates getBitrateId setGroupOrder getShareOptions setClosedCaptions setAdSection setAdKeyValue getAdKeyValues clearAdKeyValues setAdExchangeValue setAdVisibility switchAdContext switchTrackingContext setTrackingInterval setDataSrc setFileKey sendUIMessage sendUserReportedIssue".split(" ");
		var noop = function () {};

		for (var fn, f = 0, endf = functions.length; f < endf; ++f)
		{
			fn = functions[f];
			if (!this[fn])
				this[fn] = noop;
		}
	}
});


// Export public APIs
module.exports = NullPlayer;

},{"../../vendor/Class":93}],33:[function(require,module,exports){
/* cvp/pluginmanager.js */

// Load dependent modules
var CVP = require('./static');
var Utils = require('./utils');
var Log = require('./log');


/**
 * Handle the loading, instantiation, and routing of
 * CVP plugins
 */

var extend = Utils.extend,
	slice = Utils.slice,
	isFunc = Utils.isFunc,
	now = Utils.now;

var logger = Log.getLogger("PluginManager");

/**
 * Valid plugin types
 */
var pluginTypes = {
	LMDB : "lmdb"
};

/**
 * Register loaded plugins
 */
var pluginRegistry = {};

var registerPlugin = function (type, constructFunc) {
	type = (type || "").toUpperCase();
	if (!pluginTypes[type]) {
		logger.warn("[registerPlugin] Invalid plugin type. Unable to register: " + type);
		return;
	}

	pluginRegistry[type] = constructFunc;
};

/**
 * Initialize factory functions
 */
var PluginFactory = {};

PluginFactory.create = function (type, options) {
	type = (type || "").toUpperCase();
	if (!pluginRegistry[type]) {
		logger.warn("[create] Invalid plugin type. Unable to create: " + type);
		return null;
	}

	return new pluginRegistry[type](options);
};

PluginFactory.createLMDB = function (options) {
	return PluginFactory.create(pluginTypes.LMDB, options);
};

/**
 * Initialize host / plugin comm and routing
 * This will faciliate two way communication between
 * the host and plugin instance
 */
var pluginHostInstances = {};

var registerPluginHost = function (id, pluginAdapter) {
	pluginHostInstances[id] = pluginAdapter;
};

// An adapter to wrap the plugin and forward API requests
// to the plugin, and plugin callbacks to the host
var PluginHostAdapter = function (id, getHost, plugin) {
	var self = this;
	this.id = id;

	if (!getHost || !plugin) {
		logger.error("[PluginHostAdapter] Invalid host or plugin. Aborting");
		return null;
	}

	var delegateToPlugin = function (method) {
		return function () {
			return plugin[method].apply(plugin, arguments);
		};
	};

	for (var prop in plugin) {
		if (isFunc(plugin[prop])) {
			this[prop] = delegateToPlugin(prop);
		}
	}

	var forwardEventToHost = function (eventName) {
		var host = getHost();

		return function () {
			host._sendToPluginHost(self.id, eventName, slice(arguments));
		};
	};

	for (var eventName in plugin.getEvents()) {
		plugin.on(eventName, forwardEventToHost(eventName));
	}
};

PluginHostAdapter.create = function (domId, playerId, pluginType, pluginOptions) {
	var id = [
		domId,
		playerId,
		pluginType,
		now()
	].join("_");

	var host = function () {
		var instanceObj = CVP.findInstance(domId);
		return instanceObj.instance.getDOMPlayer();
	};

	var plugin = PluginFactory.create(pluginType, pluginOptions);

	return new PluginHostAdapter(id, host, plugin);
};

// Create a global hook to allow host/plugin comm
var onPluginHostRequest = function (cvpDomId, args) {
	var pluginId = args.shift(),
		method = args.shift(),
		pluginAdapter = null;

	logger.debug("[onPluginHostAPIRequest] request received: " + pluginId + ", " + method);
	// If there's no plugin ID passed, assume this is a create request
	if (!pluginId) {
		if (method !== "create") {
			logger.error("[onPluginHostAPIRequest] Invalid create method passed");
			return null;
		}

		// TODO create plugin instance
		// 	this must be unique across [CVP.instance][player]
		pluginAdapter = PluginHostAdapter[method].apply(PluginHostAdapter, [cvpDomId].concat(args));
		registerPluginHost(pluginAdapter.id, pluginAdapter);
		return pluginAdapter.id;
	}

	// Else, forward the request to the plugin
	pluginAdapter = pluginHostInstances[pluginId];
	try {
		return pluginAdapter[method].apply(pluginAdapter, args);
	} catch(e) {
		logger.warn("[onPluginHostAPIRequest] Unable to call method: " + method + " on plugin: " + pluginId);
	}
};


var PluginManager = {
	register : registerPlugin
};

// Add in the plugin types so they can be referenced as PluginManager.LMDB
PluginManager = extend(PluginManager, pluginTypes);

// Add in the factory methods
PluginManager = extend(PluginManager, PluginFactory);

// Add the hook to route communication between host / plugin
PluginManager.onPluginHostRequest = onPluginHostRequest;


// Export public APIs
module.exports = PluginManager;

},{"./log":28,"./static":38,"./utils":50}],34:[function(require,module,exports){
/* cvp/plugins.js */

// Load dependent modules
var PluginManager = require('./pluginmanager');
var LMDB = require('./plugins/lmdb');


/**
 * Plugins
 */
var plugins = {};

// Register the plugin so it can be instantiated
PluginManager.register(PluginManager.LMDB, LMDB);

// Create a convenience instance for the page to use
plugins.LMDB = PluginManager.createLMDB({ lazy : true });


// Export public APIs
module.exports = plugins;

},{"./pluginmanager":33,"./plugins/lmdb":35}],35:[function(require,module,exports){
/* cvp/plugins/lmdb.js */

// Load dependent modules
var Class = require('../../vendor/Class');
var CustomEvent = require('../customevent');
var Utils = require('../utils');
var Request = require('../request');
var Timing = require('../util/timing');
var Convert = require('../convert');
var Log = require('../log');


var logger = Log.getLogger("LMDB");

/**
 * Utility functions
 */

// Create a timer for LMDB requests on an interval
var createLMDBTimer = function(cb, ms) {
	return Timing.periodically(cb, ms);
};

// Determine if the payload is XML
var isXML = function(data) {
	return (data
		&& data.indexOf("<") === 0
		&& data.lastIndexOf(">") === (data.length - 1));
};

/**
 * LMDB Plugin
 */
var LMDB = Class.extend({
	init : function(options) {
		var self = this;

		this.setOptions(options);

		// A map of public events
		this.events = {
			dataReceived : new CustomEvent("dataReceived"),
			dataReceivedError : new CustomEvent("dataReceivedError")
		};

		this._onDataReceived = function(url, data) {
			logger.debug("[LMDB.onDataReceived] url: " + url);
			if (isXML(data)
				&& self.options.convertXML) {
				logger.debug("[LMDB.onDataReceived] Detected XML - converting");
				data = Convert.xmlTextToJsonText(data);
			}
			self.events.dataReceived.dispatch(url, Convert.jsonTextToJsObject(data));
		};

		this._onDataReceivedError = function(url, error) {
			logger.debug("[LMDB.onDataReceivedError] url: " + url);
			self.events.dataReceivedError.dispatch(url, error);
		};
	},

	/**
	 * Plugin API
	 */

	setOptions : function(options) {
		var defaultOptions = {
			convertXML : true
		};

		this.options = Utils.extend((this.options || defaultOptions), (options || {}));

		logger.debug("[LMDB.setOptions] url: " + this.options.url + ", interval: " + this.options.interval);
		if (this.options.interval
			&& this.options.url) {

			// Only poll with an interval greater than 0
			if (Utils.toInt(this.options.interval) <= 0)
				return;

			logger.debug("[LMDB.setOptions] creating timer with interval: " + this.options.interval);
			if (this.periodical)
				this.periodical.stop();
			this.periodical = createLMDBTimer(Utils.bind(this.request, this, this.options.url), this.options.interval);
		}
	},

	getEvents : function() {
		return this.events;
	},

	request : function(url) {
		logger.info("[LMDB.request] url: " + url);
		// this._transport.request(url);

		this._transport = Request.get({
			url : url,
			lazy : this.options.lazy,
			success : Utils.bind(this._onDataReceived, this, url),
			failure : Utils.bind(this._onDataReceivedError, this, url)
		});
	},

	on : function(type, cb) {
		if (!Utils.hasOwn(this.events, type))
			return;

		this.events[type].addListener(Utils.bind(cb, this));
	},

	start : function(immediate) {
		logger.debug("[LMDB.start]");

		if (Utils.undef(immediate))
			immediate = true;

		if (!this.periodical)
			return;

		this.periodical.start();

		if (immediate)
			this.request(this.options.url);
	},

	stop : function() {
		if (this.periodical)
			this.periodical.stop();
	},

	getScheduleItems : function(data) {
		var items = [];
		if (data && data.hasOwnProperty("LiveFeed")) {
			if (data.LiveFeed.hasOwnProperty("tveLiveSched")) {
				if (data.LiveFeed.tveLiveSched.hasOwnProperty("SchedItem")) {
					return items.concat(data.LiveFeed.tveLiveSched.SchedItem);
				}
			}

		}
		return items;
	},

	getScheduleItem : function(data, filter) {
		var items = this.getScheduleItems(data);

		var first = Utils.undef(filter);
		var isFunc = Utils.isFunc(filter);

		for (var i = 0; i < items.length; ++i) {
			if (first)
				return items[i];

			if (isFunc) {
				if (filter(items[i]))
					return items[i];
			} else {
				// Coerce, as the types can vary
				if (items[i].AiringID == filter)
					return items[i];
			}
		}
		return null;
	}
});


// Export public APIs
module.exports = LMDB;

},{"../../vendor/Class":93,"../convert":14,"../customevent":16,"../log":28,"../request":36,"../util/timing":48,"../utils":50}],36:[function(require,module,exports){
/* cvp/request.js */

// # Request
//
// All-purpose request mechanism that returns a promise.
//
// Usage:
//
//     var promise = Request('url')
//
//     var promise = Request('url', callback)
//
//     var promise = Request({
//       url: 'string'
//       method: 'GET' or 'POST'
//       type: 'json' or 'xml'
//       success: callback function
//       transports: ['xdm']
//     })
//
//     var promise = Request.get()
//     var promise = Request.getXML()
//     var promise = Request.getJSON()
//     var promise = Request.getJSONP()
//     var promise = Request.post()

// Load dependent modules
var Utils = require('./utils');
var Log = require('./log');
var easyXDM = require('../vendor/easyXDM');
var Convert = require('./convert');
var when = require('../vendor/when');


// Logger
var logger = Log.getLogger();

// repeating strings
var GET = 'GET';
var POST = 'POST';
var withCredentials = 'withCredentials';


// ## Transport Protocols

// `handleXHRReady` can be re-used for Ajax and CORS.
var handleXHRReady = function (xhr, opts, rsv) {
	return function () {
		if (xhr && xhr.readyState === 4) {
			try {
				if ((200 <= xhr.status && xhr.status < 300) || xhr.status === 304) {
					rsv.resolve(xhr.responseText);
				}
				else {
					rsv.reject(xhr.status + " " + xhr.statusText);
				}
			}
			catch (e) {
				logger.error("Exception with XHR: ", e);
				rsv.reject(e.message);
			}
			finally {
				xhr.onreadystatechange = Function.prototype;
				xhr.abort();
				xhr = null;
			}
		}
	};
};

// `setHeaders` can be re-used for Ajax and CORS.
var setHeaders = function (xhr, headers) {
	for (var key in headers) {
		if (!Utils.hasOwn(headers, key)) continue;
		xhr.setRequestHeader(key, headers[key]);
	}
};

var processData = function (data) {
	if (Utils.isNull(data))
		return data;

	if (Utils.isObject(data))
		return Utils.encodeParams(data);

	return window.encodeURIComponent(data).replace(/%20/g, '+');
};

var stringifyNestedObjects = function (data) {
	if (Utils.isNull(data))
		return data;

	if (Utils.isObject(data)) {
		var obj = {};

		Utils.each(data, function (key, datum) {
			obj[key] = Utils.isSimpleObject(datum) ? JSON.stringify(datum) : datum;
		});

		return obj;
	}

	return data;
};


// ### AjaxTransport

var AjaxTransport = {

	accepts : function (opts) {
		return !Utils.isCrossDomain(opts.url);
	},

	request : function (opts) {
		logger.log("Request::AjaxTransport.request", opts.url);

		if (opts.method === GET && opts.data) {
			opts.url = Utils.augmentQueryString(opts.url, opts.data);
			opts.data = null;
		}

		var deferred = when.defer();

		var xhr = new window.XMLHttpRequest();
		xhr.onreadystatechange = handleXHRReady(xhr, opts, deferred.resolver);
		xhr.open(opts.method, opts.url, true);
		setHeaders(xhr, opts.headers);

		var data = processData(opts.data);

		xhr.send(opts.method === POST ? data : null);

		return deferred.promise;
	}

};


// ### JSONPTransport

var JSONPTransport = {

	_uniqId : 0,
	_paramRE : /([^?&]+)=\?(?=&|$)/,

	createCallbackName : function (reqId) {
		return 'cvp_jsonp_' + reqId + '_' + Utils.now();
	},

	determineCallbackParam : function (url) {
		var match = this._paramRE.exec(url);
		if (match) {
			return match[1];
		}
	},

	accepts : function (opts) {
		return opts.type === 'jsonp' || opts.jsonpCallback;
	},

	request : function (opts) {
		logger.log("Request::JSONPTransport.request", opts.url);

		var reqId = ++this._uniqId;
		opts.data = opts.data || {};
		opts.timeout = opts.timeout || 5000;

		var deferred = when.defer();

		var lastValue;
		var callback = function (value) {
			lastValue = value;
			deferred.resolve(value);
		};

		var tag = 'script';
		var first = document.getElementsByTagName(tag)[0];
		var script = document.createElement(tag);
		var readyRE = /^(complete|loaded)$/;
		var done = false;

		var callbackParam = opts.jsonpParam || this.determineCallbackParam(opts.url);
		var callbackName = opts.jsonpCallback || this.createCallbackName(reqId);
		var overwritten = window[callbackName];
		window[callbackName] = callback;

		if (callbackParam) {  // not required for static JSONP; only useful for dynamically-served JSONP
			opts.data[callbackParam] = callbackName;
		}

		if (opts.cache === false) {
			opts.data._ = Utils.now();
		}

		opts.url = Utils.augmentQueryString(opts.url, opts.data);

		var onLoadSuccess = function () {
			if (!done && (!('readyState' in script) || readyRE.test(script.readyState.toString()))) {
				done = true;
				logger.info('installed ' + this.src);
			}
		};

		var onLoadFailure = function () {
			var msg = 'failed to install ' + this.src;
			done = true;
			logger.error(msg);
			deferred.reject(msg);
		};

		var abortLoad = function () {
			if (done) {
				return;
			}

			var msg = 'timeout while attempting to load ' + opts.url;
			done = true;
			logger.warn(msg);
			deferred.reject(msg);
		};

		var timer = (
			typeof opts.timeout === 'number'
			? window.setTimeout(abortLoad, opts.timeout)
			: null
		);

		var cleanup = function () {
			window[callbackName] = overwritten;
			window.clearTimeout(timer);
			script.onload = script.onreadystatechange = script.onerror = null;
			script.parentElement.removeChild(script);
			if (lastValue && Utils.isFunction(overwritten)) {
				overwritten(lastValue);
			}
			first = script = overwritten = lastValue = undefined;
			if (window[callbackName] === undefined) {
				delete window[callbackName];
			}
		};

		deferred.promise.ensure(cleanup);

		script.onload = script.onreadystatechange = onLoadSuccess;
		script.onerror = onLoadFailure;
		script.id = opts.id || this.createCallbackName(reqId);
		script.src = opts.url;

		first.parentNode.insertBefore(script, first);

		return deferred.promise;
	}

};


// ### CORSTransport

var CORSTransport = {

	accepts : function (/* opts */) {
		return (window.XMLHttpRequest && withCredentials in new window.XMLHttpRequest()) || !Utils.isUndefined(window.XDomainRequest);
	},

	request : function (opts) {
		logger.log("Request::CORSTransport.request", opts.url);

		var xhr = new window.XMLHttpRequest();

		var deferred = when.defer();

		if (opts.method === GET && opts.data) {
			opts.url = Utils.augmentQueryString(opts.url, opts.data);
			opts.data = null;
		}

		xhr.onreadystatechange = handleXHRReady(xhr, opts, deferred.resolver);

		if (withCredentials in xhr) {
			if (Utils.isBoolean(opts.withCredentials)) {
				xhr.withCredentials = opts.withCredentials;
			}

			xhr.open(opts.method, opts.url);

			if (opts.method === POST) {
				setHeaders(xhr, {
					"Content-Type" : "application/x-www-form-urlencoded"
				});
			}
		}
		else if (!Utils.isUndefined(window.XDomainRequest)) {
			xhr = new window.XDomainRequest();
			xhr.open(opts.method, opts.url);
		}
		else {
			xhr = null;
			throw new Error("Did not find CORS-compatible interface.");
		}

		var data = processData(opts.data);

		xhr.send(opts.method === POST ? data : null);

		return deferred.promise;
	}

};


// ### Cross-Domain iFrame Registry
//
// An array of objects that map a string or regexp (tested against
// the origin of the request url) to a path on the remote server.

var xdmRegistry = [];

// `registerXDM` adds { match, path } to `xdmRegistry` list.
//
// Usage:
//
//     Request.registerXDM(/(http?:\/\/[iz]|https:\/\/s)\.cdn\.turner\.com/, '/path/to/iframe.xml');
//
//     Request.registerXDM({
//       'http://i.cdn.turner.com': '/path/to/iframe.xml',
//       'http://z.cdn.turner.com': '/path/to/iframe.xml',
//       'https://s.cdn.turner.com': '/path/to/iframe.xml'
//     });

var registerXDM = function (listOrMapOrArg) {
	logger.log("Request::registerXDM");

	if (Utils.isArray(listOrMapOrArg)) {  // assume list of compatible objects
		logger.debug("Request::registerXDM - merging provided array with xdmRegistry");
		xdmRegistry.push.apply(xdmRegistry, listOrMapOrArg);
	}
	else if (Utils.isSimpleObject(listOrMapOrArg)) {
		logger.debug("Request::registerXDM - adding each property to xdmRegistry");
		for (var key in listOrMapOrArg) {
			if (!Utils.hasOwn(listOrMapOrArg, key)) continue;
			xdmRegistry.push({
				match : key,
				path : listOrMapOrArg[key]
			});
		}
	}
	else if (arguments.length === 2) {
		logger.debug("Request::registerXDM - adding xdmRegistry for '" + arguments[0] + "' with path '" + arguments[1] + "'");
		xdmRegistry.push({
			match : arguments[0],
			path : arguments[1]
		});
	}
	else {
		throw new Error("Invalid parameters to `registerXDM` -- use either registerXDM(list/map) or registerXDM(string/regex, path-to-iframe)");
	}
};

var unregisterXDM = function (pattern) {
	logger.log("Request::unregisterXDM");
	var match = pattern.toString();  // convert regexp to string

	for (var item, i = xdmRegistry.length - 1; i >= 0; i--) {
		item = xdmRegistry[i];
		if (match === item.match.toString()) {  // can't compare regexps
			return xdmRegistry.splice(i, 1);
		}
	}
};

var testPattern = function (string, pattern) {
	logger.log("Request::testPattern");
	var method = (
		Object.prototype.toString.call(pattern) === "[object RegExp]"
		? String.prototype.search
		: String.prototype.indexOf
	);
	return -1 !== method.call(string, pattern);
};

var lookupIframe = function (origin) {
	logger.log("Request::lookupIframe");
	for (var item, i = xdmRegistry.length - 1; i >= 0; i--) {
		item = xdmRegistry[i];
		if (testPattern(origin, item.match)) {
			logger.debug("Request::lookupIframe - origin '" + origin + "' matched '" + item.match + "'");
			return origin + item.path;
		}
		logger.debug("Request::lookupIframe - origin '" + origin + "' did not match '" + item.match + "'");
	}
};


// ### XDMTransport
//
// uses easyXDM for cross-domain messaging

var XDMTransport = {

	getIframe : function (url) {
		var urlParsed = Utils.parseUrl(url);
		var origin = urlParsed.origin;

		return lookupIframe(origin);
	},

	hasIframe : function (url) {
		return !!this.getIframe(url);
	},

	accepts : function (opts) {
		return Utils.isCrossDomain(opts.url) && this.hasIframe(opts.url);
	},

	request : function (opts) {
		logger.log("Request::XDMTransport.request", opts.url);

		var xdmIframe = this.getIframe(opts.url);

		if (!xdmIframe) {
			var urlParsed = Utils.parseUrl(opts.url);
			var origin = urlParsed.origin;
			throw new Error("Did not find a registered XDM iframe for origin '" + origin + "'.");
		}

		logger.log("Found a registered XDM iframe at '" + xdmIframe + "'.");

		var deferred = when.defer();

		// easyXDM API:
		//
		// request(object config, function successFn, function failureFn)
		//
		// config properties:
		//
		//   - url {string} - The url to request
		//   - method {string} - GET or POST.
		//   - headers {object} - A map of headers to apply - the defaults are "Content-Type": "application/x-www-form-urlencoded" and "X-Requested-With": "XMLHttpRequest". Set headers are added to the default, null values removed.
		//   - timeout {number} - the number of milliseconds before a timeout occurs. Default 10000 (10 seconds)
		//   - data {object} - a map of the data to pass
		//
		// successFn parameter properties:
		//
		//   - data {string} - the responseText
		//   - status {number} - The status of the request
		//   - headers {object} - a map of the returned headers
		//
		// failureFn parameter properties:
		//
		//   - data {string} - the responseText if available, or null
		//   - status {number} - The status of the request
		//   - message {string} - A friendly message explaining the error
		//

		var rpc = new easyXDM.Rpc(
			{
				lazy : true,
				remote : xdmIframe
			},
			{
				remote : {
					request : {}
				}
			}
		);

		var config = {
			url : opts.url,
			method : opts.method,
			headers : opts.header || undefined,
			timeout : opts.timeout || undefined,
			data : stringifyNestedObjects(opts.data)
		};

		var successFn = function (response) {
			logger.log("XDMTransport request success: ", response);

			try {
				deferred.resolve(response.data);
			} catch (e) {
				deferred.reject(Utils.extend(response, {
					responseText : response.data
				}));
			}
		};

		var failureFn = function (response) {
			logger.log("XDMTransport request failure: ", response);

			if ('data' in response) {
				response = Utils.extend({
					responseText : response.data.data,
					statusText : response.message
				}, response.data);
			}

			deferred.reject(response);
		};

		rpc.request(config, successFn, failureFn);

		return deferred.promise;
	}

};


// ## Transport Mechanism Registry

var transportRegistry = [
	{ name : 'jsonp', impl : JSONPTransport, exclusive : true },
	{ name : 'ajax', impl : AjaxTransport, exclusive : true },
	{ name : 'cors', impl : CORSTransport },
	{ name : 'xdm', impl : XDMTransport }
];

// `determineType` attempts to figure out the `type` for the Request
// based on the file extension.

var fileExtensionRegExp = /\.(js(?:onp?)?|(?:ht|x)ml)$/i;
var determineType = function (opts) {
	if (opts.type) {
		return opts.type.toLowerCase();
	}

	var urlParts = Utils.parseUrl(opts.url);
	var match = fileExtensionRegExp.exec(urlParts.pathname);
	return match ? match[1].toLowerCase() : '';
};

// `determineTransports` loops through the list of available/requested
// mechanisms and collects references to the mechanisms that accept the
// provided options.

var determineTransports = function (opts) {
	var transports = [];
	var keys = [].concat(opts.transports || opts.transport || []);
	var any = Utils.isEmpty(keys);

	for (var transport, i = 0, endi = transportRegistry.length; i < endi; ++i) {
		transport = transportRegistry[i];
		if ((any || Utils.indexOf(keys, transport.name) !== -1) && transport.impl.accepts(opts)) {
			transports.push(transport.impl);
			if (transport.exclusive) {
				break;
			}
		}
	}
	return transports;
};


// `createRequest` is the gateway to Request mechanisms.

var createRequest = function (opts) {
	if (!Utils.isSimpleObject(opts)) {
		throw new Error('Request.createRequest requires an object parameter.');
	}

	var deferred = when.defer();

	// sanitize options
	opts.method = (opts.method || GET).toUpperCase();
	opts.type = determineType(opts);
	opts.data = opts.data || null;
	opts.headers = opts.headers || {};
	opts.success = opts.callback || opts.success;
	opts.failure = opts.error || opts.failure;

	/*
	jsonpParam
	jsonpCallback
	*/

	// add callbacks if provided
	deferred.promise.then(opts.success, opts.failure);

	var transports = determineTransports(opts);

	var lastResult = null;
	var success = function (data) {
		deferred.resolve(data);
	};
	var failure = function (error) {
		lastResult = error;
		keepTrying();
	};
	var keepTrying = function () {
		if (transports.length === 0) {
			deferred.reject(lastResult);
		}
		else {
			var transport = transports.shift();
			try {
				var attempt = transport.request(opts);
				attempt.then(success, failure);
			} catch (e) {
				failure(e);
			}
		}
	};

	keepTrying();

	return deferred.promise;
};


// ## Request
// The public API

function Request() {
	return createRequest.apply(this, arguments);
}

Request.registerXDM = registerXDM;
Request.unregisterXDM = unregisterXDM;

// convenience methods

Request.get = function (opts) {
	opts.method = GET;
	return createRequest(opts);
};

Request.getXML = function (opts) {
	opts.type = 'xml';
	return createRequest(opts).then(Convert.xmlTextToXmlDoc);
};

Request.getJSON = function (opts) {
	opts.type = 'json';
	return createRequest(opts).then(Convert.jsonTextToJsObject);
};

Request.getJSONP = function (opts) {
	opts.type = 'jsonp';
	return createRequest(opts);
};

Request.post = function (opts) {
	opts.method = POST;
	return createRequest(opts);
};


// ## Register XDM iframes
// Set up default iframes for XDMTransport.

// Turner CDN
Request.registerXDM(/^(http:\/\/([iz]|cvp\d+)|https:\/\/(s|cvp\d+))\.cdn\.turner\.com(:\d+)?$/, '/xslo/xslo/test/xdm_iframe');

// Aspen
Request.registerXDM(/^https?:\/\/aspen\.turner\.com(:\d+)?$/, '/static/xdm_iframe.html');
Request.registerXDM(/^https?:\/\/refwebapp\.aspen\.vgtf\.net(:\d+)?$/, '/static/xdm_iframe.html');


// Return public module.
module.exports = Request;

},{"../vendor/easyXDM":94,"../vendor/when":96,"./convert":14,"./log":28,"./utils":50}],37:[function(require,module,exports){
/* cvp/shared.js */

// Load dependent modules
var CVP = require('./static');
var Version = require('./version');
var App = require('../core/app');
var Utils = require('./utils');
var Log = require('./log');


var log = Log.getLogger();


/**
 * Add the global callback handler for a particular instance of CVP.
 * @private
 * @param {String} id The callback id.
 * @returns {Boolean} True or false based on the success of the creation of
 * the callback.
 */
function createCallbackHandler(id) {
	var funcName = CVP.createCallbackHandlerName(id);

	if (Utils.isFunc(window[funcName]))
		return false;

	window[funcName] = function() {
		if (arguments.length
			&& arguments[0] === "_pluginRequest") {
			return CVP.PluginManager.onPluginHostRequest(id, Utils.slice(arguments, 1));
		}

		return CVP.onCallback(id, arguments);
	};

	return true;
}

/**
 * Validate the embed flash version against our min version.
 * @private
 * @param {String} embedVersion The embed version's name.
 * @returns {String} The correct flash version name.
 */
function validateFlashVersion(embedVersion)
{
	// short-circuit: don't bother comparing if it's the default!
	if (embedVersion === App.FLASH_VERSION) return embedVersion;

	try {
		var configVersion = new Version(embedVersion);
		if (configVersion.gt(App.FLASH_VERSION)) {
			return configVersion.toString();
		}
	} catch (e) {
		log.warn("Failure to compare Flash versions", App.FLASH_VERSION, embedVersion);
	}

	return App.FLASH_VERSION;
}

/**
 * Adds before unloading.
 * @private
 * @param {Function} func The function to add.
 */
function addBeforeUnLoadEvent(func)
{
	var oldfunc = window.onbeforeunload;
	if (typeof window.onbeforeunload !== 'function')
	{
		window.onbeforeunload = func;
	}
	else
	{
		window.onbeforeunload = function()
		{
			if (oldfunc)
			{
				oldfunc();
			}
			func();
		};
	}
}


// Export public APIs
exports.createCallbackHandler = createCallbackHandler;
exports.validateFlashVersion = validateFlashVersion;
exports.addBeforeUnLoadEvent = addBeforeUnLoadEvent;

},{"../core/app":8,"./log":28,"./static":38,"./utils":50,"./version":51}],38:[function(require,module,exports){
/* cvp/static.js */

// Load dependent modules
var App = require('../core/app');
var PageOrientationDetector = require('./detectors/pageorientationdetector');
var PageFocusDetector = require('./detectors/pagefocusdetector');
var ViewportDetector = require('./detectors/viewportdetector');
var Utils = require('./utils');
var Log = require('./log');


var logger = Log.getLogger();
var CVP = {};
var GlobalInstanceOps = {
	initialized : false,

	init : function () {
		var self = this;

		if (this.initialized)
			return;

		this._pageOrientationDetector = new PageOrientationDetector({
			callback : function (orientation) {
				self._sendToAllInstances("onPageOrientationChange", orientation);
			}
		});

		this._pageFocusDetector = new PageFocusDetector({
			callback : function (visible) {
				self._sendToAllInstances("onPageVisibilityChange", visible);
			}
		});

		this._viewportDetector = new ViewportDetector();

		this._pageOrientationDetector.start();
		this._pageFocusDetector.start();
		this._viewportDetector.start();

		this.initialized = true;
	},

	registerInstance : function (id) {
		var instance = CVP.findInstance(id);
		if (instance) {
			this._addViewportTarget(id);
		}
	},

	unregisterInstance : function (id) {

	},

	_addViewportTarget : function (id) {
		var self = this;

		this._viewportDetector.addTarget({
			elementId : id,
			callback : function (visible) {
				self._sendToInstance(id, "onViewportVisibilityChange", visible);
			}
		});
	},

	_sendToAllInstances : function () {
		var args = Utils.slice(arguments);
		Utils.each(CVP.instances, function (id, instanceObj) {
			instanceObj.send.apply(instanceObj, args);
		});
	},

	_sendToInstance : function (id) {
		var args = Utils.slice(arguments, 1);

		var instanceObj = CVP.instances[id];
		if (instanceObj)
			instanceObj.send.apply(instanceObj, args);
	}

};


// Static vars/functions

CVP.HTML5 = App.HTML5;
CVP.FLASH = App.FLASH;

/**
 * The current CVP version.
 * @private
 */
CVP.version = App.VERSION;
CVP.buildDate = App.BUILD_DATE;

// Error constants
/**
 * An error constant for video not found.
 * @private
 */
CVP.VIDEO_NOT_FOUND_ERROR = "video not found";
/**
 * An error constant for xml not found.
 * @private
 */
CVP.VIDEO_XML_NOT_FOUND_ERROR = "cms error";

/**
 * HTML5 video media errors
 */
CVP.MEDIA_ERR_UNKNOWN = "Unknown error.";
CVP.MEDIA_ERR_ABORTED = "The fetching process for the media resource was aborted by the user agent at the user's request.";
CVP.MEDIA_ERR_NETWORK = "A network error of some description caused the user agent to stop fetching the media resource, after the resource was established to be usable.";
CVP.MEDIA_ERR_DECODE = "An error of some description occurred while decoding the media resource, after the resource was established to be usable.";
CVP.MEDIA_ERR_SRC_NOT_SUPPORTED = "The media resource indicated by the src attribute was not suitable.";

/**
 * Instances object.
 * @private
 */
CVP.instances = {};

// Disallow characters that interfere with ExternalInterface.
// Source: http://help.adobe.com/en_US/FlashPlatform/reference/actionscript/3/flash/external/ExternalInterface.html
var charsThatInterfereWithExternalInterface = /[-+*\\\/]/g;
var lookupTable = {};
var reverseLookupTable = {};

/**
 * Returns a Flash-friendly CVP ID for use in callback naming.
 * @param {String} id - The instance's ID.
 * @returns {String} Sanitized ID.
 */
CVP.sanitizeId = function (id) {
	if (id in lookupTable)
		return lookupTable[id];

	if (!charsThatInterfereWithExternalInterface.test(id))
		return id;

	var newId = id.replace(charsThatInterfereWithExternalInterface, '_');

	lookupTable[id] = newId;
	reverseLookupTable[newId] = id;

	logger.warn("The provided ID '" + id + "' contains characters that interfere with Flash ExternalInterface; sanitizing to '" + newId + "'.");

	return newId;
};

/**
 * Required to map a sanitized ID back to the DOM ID.
 * @param {String} id - The callback ID.
 * @returns {String} The original DOM ID.
 */
CVP.unsanitizeId = function (id) {
	if (id in reverseLookupTable)
		return reverseLookupTable[id];

	return id;
};

/**
 * Ensure that invalid characters do not interfere with Flash ExternalInterface.
 * @param {String} id - The callback ID.
 * @returns {String} The callback function name with sanitized ID.
 */
CVP.createCallbackHandlerName = function (id) {
	return CVP.sanitizeId(id) + '_callback_handler';
};

/**
 * Sets the specified CVP instance.
 * @private
 * @param {String} id The instance's id.
 * @param {Object} instance The instance object to set.
 * @param {Function} send The message handler.
 */
CVP.registerInstance = function (id, instance, send) {
	CVP.instances[id] = {
		instance : instance,
		send : send
	};

	if (!GlobalInstanceOps.initialized)
		GlobalInstanceOps.init();

	GlobalInstanceOps.registerInstance(id);
};

/**
 * Un-registers the specified CVP instance.
 * @private
 * @param {String} id The instance's id.
 */
CVP.unregisterInstance = function (id) {
	if (id in CVP.instances) {
		var callbackName = CVP.createCallbackHandlerName(id);

		if (callbackName in window)
			window[callbackName] = null;

		CVP.instances[id] = null;
		delete CVP.instances[id];
	}
};

/**
 * Returns the specified CVP instance.
 * @private
 * @param {String} id The instance's id.
 * @returns {CVP} The CVP instance object.
 */
CVP.findInstance = function (id) {
	var instanceObj = CVP.instances[id];
	return (instanceObj && instanceObj.instance) || instanceObj;
};

/**
 * Handles a registered callback.
 * @private
 * @param {String} id The callback's id.
 * @param {Arguments} args Optional parameters for the callback function.
 * @returns {*} Response from calling `send`.
 */
CVP.onCallback = function (id, args) {
	var instanceObj = CVP.instances[id];
	if (!instanceObj) {
		logger.error("onCallback - unable to find instance " + id);
		throw new Error("onCallback - unable to find instance " + id);
	}
	return instanceObj.send.apply(instanceObj.instance, ["handleCallBack"].concat(Utils.slice(args)));
};

/**
 * Sets all instances of CVP to null.
 * @private
 */
CVP.cleanup = function () {
	Utils.each(CVP.instances, function (id, obj) {
		try {
			obj.instance.destroy();
		} catch (e) {
			// fail silently
		}
	});
};


// Export public APIs
module.exports = CVP;

},{"../core/app":8,"./detectors/pagefocusdetector":18,"./detectors/pageorientationdetector":21,"./detectors/viewportdetector":24,"./log":28,"./utils":50}],39:[function(require,module,exports){
/* cvp/store.js */

// Load dependent modules
var Utils = require('./utils');


/**
 * General purpose key/value store
 */
var store = {};


// Export public APIs
module.exports = {
	get : function(key, del) {
		var val = store[key];
		if (del)
			delete store[key];
		return val;
	},

	keys : function () {
		var keys = [];
		for (var key in store) {
			if (Utils.hasOwn(store, key)) {
				keys.push(key);
			}
		}
		return keys;
	},

	set : function(key, val) {
		store[key] = val;
	}
};

},{"./utils":50}],40:[function(require,module,exports){
/* cvp/util/asset.js */

// Load dependent modules
var Class = require('../../vendor/Class');
var Utils = require('../utils');
var CustomEvent = require('../customevent');
var Log = require('../log');
var log = Log.getLogger();


/**
 * A base class representing an asset to be used by CVP.
 * @class Contains variables and methods pertaining to the management of an
 * assets.
 */
var Asset = Class.extend({

	/**
	 * Initializes the asset.
	 * @param {String} url The asset's source url.
	 * @param {String} type The asset's type.
	 */
	init : function(url, type)
	{
		this._firedSuccess = false;
		this._url = url;
		this._type = Utils.empty(type) ? this._determineType() : type;
		this.id = "cvp_asset_" + Utils.getRandomInt(1, 1000);

		this.eSuccess = new CustomEvent();
		this.eFailure = new CustomEvent();
	},

	/**
	 * Determine's the type of the asset.
	 * @private
	 * @returns {String} The extension of the file.
	 */
	_determineType : function()
	{
		return (Utils.empty(this._url)) ? "" : this._url.substring(this._url.lastIndexOf(".") + 1);
	},

	/**
	 * Loads the asset.
	 */
	load : function()
	{
		log.info("Asset", "loading type", this._type);
		switch (this._type)
		{
			case "js":
				this._loadJs();
				break;
			case "css":
				this._loadCss();
				break;
			default:
				this._failure();
		}
	},

	/**
	 * This function is called when the asset loads successfullly.
	 * @private
	 */
	_success : function()
	{
		log.info("Asset", "successfully loaded asset", this._url);
		this.eSuccess.dispatch();
		this._firedSuccess = true;
	},

	/**
	 * This function is called when the asset fails to load.
	 * @private
	 */
	_failure : function()
	{
		log.error("Asset", "failed to load asset", this._url);
		this.eFailure.dispatch();
	},

	/**
	 * Loads a javascript asset.
	 * @private
	 */
	_loadJs : function()
	{
		var head = document.getElementsByTagName("head")[0];
		if (!head)
		{
			this._failure();
			return;
		}

		var script = document.createElement('script');
		script.id = this.id;
		script.type = 'text/javascript';

		var successCB = Utils.bind(function() {
			if (this._firedSuccess)
				return;

			this._success();
			script.onload = script.onreadystatechange = null;
			head.removeChild(script);
		}, this);

		script.onload = successCB;
		script.onerror = Utils.bind(this._failure, this);
		script.onreadystatechange = function ()
		{
			if (this.readyState === 'loaded' || this.readyState === 'complete')
			{
				successCB();
			}
		};

		script.src = this._url;
		head.appendChild(script);

	},

	/**
	 * Loads a css asset.
	 * @private
	 */
	_loadCss : function()
	{
		var node,
			head = document.getElementsByTagName("head")[0];

		if (!head)
		{
			this._failure();
			return;
		}

		node = document.createElement('link');
		node.type = 'text/css';
		node.rel = 'stylesheet';
		node.href = this._url;
		node.media = 'screen';
		head.appendChild(node);

		this._success();
	}
});


// Export public APIs
module.exports = Asset;

},{"../../vendor/Class":93,"../customevent":16,"../log":28,"../utils":50}],41:[function(require,module,exports){
/* cvp/util/commandqueue.js */

// Load dependent modules
var Class = require('../../vendor/Class');
var Utils = require('../utils');


var CommandQueue = Class.extend({

	/**
	 * Initializes the queue.
	 * @function
	 */
	init : function()
	{
		this._queue = [];
	},

	clear : function ()
	{
		this._queue.length = 0;
	},

	/**
	 * Pushes a new command into the queue.
	 * @param {Function} fn The function to put in the queue.
	 * @param {Object} scope The scope of the command.
	 * @param {Object} [args] Additional, optional parameters.
	 * @returns {Number} Index where pushed fn/scope added.
	 */
	push : function(fn, scope, args)
	{
		var index = this._queue.length;
		this._queue.push({ fn : fn, scope : scope, args : args });
		return index;
	},

	/**
	 * Removes a command from the queue according to the given index.
	 * @param {int} index The index of the queue to remove the command.
	 */
	remove : function(index)
	{
		index = typeof index !== 'number' ? this._queue.length - 1 : index;
		this._queue.splice(index, 1);
	},

	/**
	 * Run the commands in the queue.
	 */
	execute : function()
	{
		for (var cmd, i = 0, len = this._queue.length; i < len; ++i)
		{
			cmd = this._queue[i];
			cmd.fn.apply(cmd.scope, cmd.args);
		}
	},

	/**
	 * Run the commands in the queue asynchronously
	 * @memberOf CVP.Utils.CommandQueue
	 * @param {Number} [delay=10] - Milliseconds of delay.
	 */
	executeAsync : function(delay)
	{
		delay = Utils.undef(delay) ? 10 : delay;
		var cmd, len = this._queue.length;

		if (len) {
			cmd = this._queue.shift();
			cmd.fn.apply(cmd.scope, cmd.args);
			setTimeout(Utils.bind(this.executeAsync, this, delay), delay);
		}
	}

});


// Export public APIs
module.exports = CommandQueue;

},{"../../vendor/Class":93,"../utils":50}],42:[function(require,module,exports){
/* cvp/util/conditionaltask.js */

// Load dependent modules
var Utils = require('../utils');
var Log = require('../log');


var log = Log.getLogger();

var ConditionalTask = function(condition, success, interval) {
	this._interval = Utils.isNull(interval) ? 10 : interval;
	this._maxTries = 500;

	this._condition = condition;
	this._success = success;

	this._tries = 0;

	this.conditionWrapper = Utils.bind(function() {
		var ret = this._condition();
		if (ret)
		{
			window.clearInterval(this._timer);
			this._success();
		}
		else if (this._tries > this._maxTries)
		{
			log.warn("condition never met!");
			window.clearInterval(this._timer);
		}
		++this._tries;
	}, this);

	this.start = function() {
		this._timer = window.setInterval(this.conditionWrapper, this._interval);
	};
};


// Export public APIs
module.exports = ConditionalTask;

},{"../log":28,"../utils":50}],43:[function(require,module,exports){
/* cvp/util/configutils.js */

// Load dependent modules
var Utils = require('../utils');
var Globals = require('../globals');
var XML = require('./xml');


var cvpSearchTheClient = Globals.cvpSearchTheClient;

function stringReplace(str, entry, uriEncode) {
	var patterns = this.getReplacementPatterns(str),
		i = 0,
		endi = 0,
		pattern,
		replaceStr = "";

	if (!Utils.empty(patterns)) {
		for (endi = patterns.length; i < endi; ++i)
		{
			pattern = patterns[i];
			replaceStr = "";

			replaceStr = this.getReplacementText(pattern, entry);

      // Separate path for ad-kvp extension handling [CVP-2893]
      // If the return value is not a string, return it directly
      if (!Utils.isString(replaceStr)) {
            return replaceStr;
      }

			// Convert chars like '&' and '?' to their hex representations so
			// these chars don't conflict with other &'s and ?'s in the URL.
			// (Also converts space to %20, and so on).
			if (Utils.empty(replaceStr))
				replaceStr = "";
			else if (uriEncode)
				replaceStr = encodeURI(replaceStr);

			str = str.replace(pattern, replaceStr);
		}
	}

	return str;
}

function getReplacementPatterns(str) {
	return (str && str.match(/[$][{][^}]*[}]/g));
}

function getReplacementText(pattern, entry) {
	var replaceStr = "",
		nodeName = "",
		array = [],
		objName = "";

	switch (pattern)
	{
		case "${page.domain}":
			replaceStr = document.domain;
			break;

		case "${page.url}":
			replaceStr = location.href;
			break;

		case "${videoId}":
		case "${video.id}":
			if (entry)
				replaceStr = entry.getId();
			break;

		default:
			// strip off ${}, store result in tmp variable.
			nodeName = pattern.substr(2, pattern.length - 3);

			// get the object name (ex: 'video', 'container')
			array = nodeName.split('.');
			objName = array[0];

			if (array.length > 1)
			{
				nodeName = nodeName.substr(objName.length + 1);

				switch (objName)
				{
					case "video":
						if (entry)
							replaceStr = XML.getNodeValue(entry._xmlEntry, nodeName);
						break;
					case "javascript":
						replaceStr = cvpSearchTheClient(null, nodeName);
						break;
					default:
						replaceStr = pattern;
				}
			}
			else {
				replaceStr = pattern;
			}
	}

	return replaceStr;
}


// Export public APIs
exports.stringReplace = stringReplace;
exports.getReplacementPatterns = getReplacementPatterns;
exports.getReplacementText = getReplacementText;

},{"../globals":27,"../utils":50,"./xml":49}],44:[function(require,module,exports){
/* cvp/util/dependency.js */

// Load dependent modules
var Class = require('../../vendor/Class');
var Event = require('../customevent');
var Asset = require('./asset');
var Log = require('../log');


var log = Log.getLogger();

var Dependency = Class.extend({
	init : function(assetUrl, required)
	{
		this._assetUrl = assetUrl;
		this.required = required;

		this._manager = null;

		// Events
		this.eSuccess = new Event();
		this.eFailure = new Event();
	},

	load : function()
	{
		log.info("Dependency", "loading asset", this._assetUrl);
		this._assetLoader = new Asset(this._assetUrl);
		this._assetLoader.eSuccess.addListener(this._success, this);
		this._assetLoader.eFailure.addListener(this._failure, this);
		this._assetLoader.load();
	},

	_success : function()
	{
		log.info("Dependency", "successfully loaded dependency", this._assetUrl);
		this.eSuccess.dispatch.apply(this.eSuccess, arguments);
		this._complete();
	},

	_failure : function()
	{
		log.error("Dependency", "failed to load dependency", this._assetUrl);
		this.eFailure.dispatch.apply(this.eFailure, arguments);
		this._complete();
	},

	_complete : function ()
	{
		if (this._assetLoader) {
			this._assetLoader.eSuccess.removeListener(this._success, this);
			this._assetLoader.eFailure.removeListener(this._failure, this);
			this._assetLoader = null;
		}
	},

	setManager : function(manager)
	{
		this._manager = manager;
	},

	getDesc : function()
	{
		return this._assetUrl;
	}
});


// Export public APIs
module.exports = Dependency;

},{"../../vendor/Class":93,"../customevent":16,"../log":28,"./asset":40}],45:[function(require,module,exports){
/* cvp/util/dependencymanager.js */

// Load dependent modules
var Class = require('../../vendor/Class');
var Event = require('../customevent');


var DependencyManager = Class.extend({

	init : function()
	{
		this._dependencies = [];
		this._currentDependency = null;

		// Events
		this.eSuccess = new Event();				// All required dependencies loaded successfully
		this.eFailure = new Event();				// One or more required dependencies failed to load
		this.eDependencySuccess = new Event();		// A dependency loaded successfully
		this.eDependencyFailure = new Event();		// A dependency failed to load
	},

	addDependency : function(dependency)
	{
		dependency.setManager(this);
		this._dependencies.push(dependency);
	},

	load : function()
	{
		this._currentDependency = this._dependencies.shift();
		this._currentDependency.eSuccess.addListener(this._onDependencySuccess, this);
		this._currentDependency.eFailure.addListener(this._onDependencyFailure, this);

		var self = this;
		window.setTimeout(function() { self._currentDependency.load(); }, 10);
	},

	_onDependencySuccess : function(result)
	{
		this.eDependencySuccess.dispatch(this._currentDependency, result);
		this._next();
	},

	_onDependencyFailure : function(error)
	{
		this.eDependencyFailure.dispatch(this._currentDependency, error);

		// Fatal error
		if (this._currentDependency.required)
		{
			this._onFailure(this._currentDependency.getDesc());
			return;
		}

		this._next();
	},

	_next : function()
	{
		this._currentDependency.eSuccess.removeListener(this._onDependencySuccess, this);
		this._currentDependency.eFailure.removeListener(this._onDependencyFailure, this);
		this._currentDependency = null;

		if (this._dependencies.length)
			this.load();
		else
			this._onSuccess();
	},

	_onSuccess : function()
	{
		this.eSuccess.dispatch();
	},

	_onFailure : function(desc)
	{
		this.eFailure.dispatch(desc);
	}

});


// Export public APIs
module.exports = DependencyManager;

},{"../../vendor/Class":93,"../customevent":16}],46:[function(require,module,exports){
/* cvp/util/jsonconverter.js */

// Load dependent modules
var XML = require('./xml');
var Utils = require('../utils');
var Log = require('../log');


var log = Log.getLogger('JSONConverter');
var whitespaceRE = /[\t\n\r\xA0]+/g;
var namespaceRE = /^xmlns($|:)/;

function ifAttributeIsNamespaced(attribute)
{
	return namespaceRE.test(attribute.nodeName);
}

function toNodeName(node)
{
	return node.nodeName;
}

function hasDuplicateNodeNames(arrayOfNodes)
{
	var arrayOfNames = Utils.map(arrayOfNodes, toNodeName);
	var uniqueNames = Utils.uniq(arrayOfNames);
	return arrayOfNames.length !== uniqueNames.length;
}

function ifAttributeIsNotNamespace(attribute)
{
	return !ifAttributeIsNamespaced(attribute);
}

function getAttributes(node)
{
	return Utils.filter(Utils.slice(node.attributes), ifAttributeIsNotNamespace);
}

function addAttributes(attributes, target)
{
	target = target || {};

	if (!attributes.length)
	{
		return target;
	}

	Utils.each(attributes, function (attr) {
		target[attr.nodeName] = attr.nodeValue;
	});

	return target;
}

function getTextValue(node)
{
	if (!node.hasChildNodes())
	{
		return null;
	}

	var text = node.textContent || node.innerText || '';
	return Utils.trim(text).replace(whitespaceRE, ' ');
}

function convertNode(node, target)
{
	target = Utils.isSimpleObject(target) ? target : {};

	var nodeName = node.nodeName;
	var nodeType = node.nodeType;
	var childElements;
	var result = {};

	switch (nodeType)
	{
		case Node.DOCUMENT_NODE:

			childElements = XML.getChildElements(node);

			target = childElements.length ? convertNode(childElements[0]) : null;

		break;

		case Node.ELEMENT_NODE:

			// If it has child elements with similar names, then treat as an array.
			// Only process the child elements.  Ignore all other data.

			// If it has child elements of differing names, then treat as an object.
			// Process attributes, then elements.  Ignore other nodes (text, cdata).

			// If it has attributes, then treat as an object.
			// Process attributes.
			// If getTextValue is not null, then add a 'text' property with value.

			// If it has no child elements and no attributes,
			// then, if all else fails, target[nodeName] = getTextValue.

			childElements = XML.getChildElements(node);
			var attributes = getAttributes(node);

			if (childElements.length)
			{
				if (hasDuplicateNodeNames(childElements))
				{
					// result = array (list of each processed child element)
					result = Utils.map(childElements, function (child /*, index, list*/) {
						return convertNode(child);
					});
				}
				else
				{
					// result = object (just add children responses to object)
					if (attributes.length)
					{
						addAttributes(attributes, result);
					}

					Utils.each(childElements, function (child /*, index, list*/) {
						return convertNode(child, result);
					});
				}
			}
			else
			{
				var text = getTextValue(node);

				if (attributes.length)
				{
					addAttributes(attributes, result);

					if (text !== null)
					{
						result.text = text;
					}
				}
				else
				{
					// in the absence of text(), provide empty string instead of null
					result = text || '';
				}
			}

			target[nodeName] = result;

		break;

		default:

			log.debug('Ignoring type ' + nodeType + ' node with name "' + nodeName + '"');
	}

	return target;
}

function convertVideoNode(xml) {

	if (!xml || !xml.nodeType || !xml.nodeName || xml.nodeType !== Node.ELEMENT_NODE || xml.nodeName !== 'video') {
		log.debug('Not a "video" node; passing through.');
		return convertNode(xml);
	}

	// Selectively processing video childNodes differently after CNN started adding duplicate nodes.
	// At least the resulting object should be an object, and your duplicates will overwrite each other,
	// instead of returning an array because we didn't want to lose your data.
	log.debug('Processing "video" children...');

	var result = {};
	var childElements = XML.getChildElements(xml);
	var attributes = getAttributes(xml);

	if (childElements.length)
	{
		// result = object (just add children responses to object)
		if (attributes.length)
		{
			addAttributes(attributes, result);
		}

		Utils.each(childElements, function (node /*, index, list*/) {
			return convertNode(node, result);
		});
	}
	else
	{
		var text = getTextValue(xml);

		if (attributes.length)
		{
			addAttributes(attributes, result);

			if (text !== null)
			{
				result.text = text;
			}
		}
		else
		{
			// in the absence of text(), provide empty string instead of null
			result = text || '';
		}
	}

	return ({
		video : result
	});

}

function xmlToObject(xml)
{
	// join adjacent text nodes, remove empty text nodes
	try {
		xml.normalize();
	} catch (e) {
		// IE8 fails
		if (xml.nodeType === 9) {
			try {
				var children = XML.getChildElements(xml);
				if (children.length) {
					children[0].normalize();
				}
			} catch (e2) {
				/* ignore */
			}
		}
	}

	var obj = convertVideoNode(xml);

	if (obj)
	{
		for (var key in obj)
		{
			// return first item in obj (root node)
			return obj[key];
		}
	}

	return null;
}

function xmlToJson(xml)
{
	return JSON.stringify(xmlToObject(xml), null, '\t');
}

// Export public APIs
exports.encodeXmlObject = xmlToJson;
exports.xmlToJson = xmlToJson;
exports.xmlToObject = xmlToObject;

},{"../log":28,"../utils":50,"./xml":49}],47:[function(require,module,exports){
/* cvp/util/log4js.js */

// Load dependent modules
var App = require('../../core/app');
var Log = require('../log');


var log = Log.getLogger();

var debugFlag = App.DEBUG;
var installingLog4JS = false;
var installedLog4JS = false;

function install() {
	if (debugFlag && !installedLog4JS && !installingLog4JS && !(window.log4javascript)) {
		log.info("installing log4javascript");
		installingLog4JS = true;
		(function (doc, tag, prop) {
			var el = doc.createElement(tag),
				s1 = doc.getElementsByTagName(tag)[0],
				readyRE = /^(complete|loaded)$/,
				done = false;
			el.onload = el.onreadystatechange = function () {
				if (!done && !((prop in el) && readyRE.test(el[prop]))) {
					el.onload = el.onreadystatechange = null;
					done = true;
					log.info("installed log4javascript");
					installedLog4JS = true;
				}
			};
			el.src = App.LOG4JAVASCRIPT_URL;
			s1.parentNode.insertBefore(el, s1);
		}(document, "script", "readyState"));
	}
}

function isInstalled() {
	return installedLog4JS;
}


// Export public APIs
exports.install = install;
exports.isInstalled = isInstalled;

},{"../../core/app":8,"../log":28}],48:[function(require,module,exports){
/* cvp/util/timing.js */

/**
 * Timing utilities.
 */

function eventually(cb, ms) {
	var timeout;

	function stop() {
		if (!timeout) return;
		window.clearTimeout(timeout);
		timeout = null;
	}

	function start() {
		stop();
		timeout = window.setTimeout(cb, ms);
	}

	if (!isFinite(ms) || typeof cb !== 'function')
		throw new Error('usage: eventually(cb, ms)');

	return ({
		start : start,
		stop : stop
	});
}

function periodically(cb, ms) {
	var timeout,
		count = 0,
		tick = function() {
			cb(++count, ms);
		},
		stop = function() {
			window.clearInterval(timeout);
		},
		start = function() {
			if (timeout)
				stop();
			timeout = window.setInterval(tick, ms || 1000);
		};

	return {
		start : start,
		stop : stop
	};
}


// Export public APIs
module.exports = {
	eventually : eventually,
	periodically : periodically
};

},{}],49:[function(require,module,exports){
/* cvp/util/xml.js */

// Load dependent modules
var Utils = require('../utils.js');


var DOMPARSER = 'DOMParser';
var XMLSERIALIZER = 'XMLSerializer';
var ACTIVEXOBJECT = 'ActiveXObject';

var stringToDoc = (function () {
	var parser;
	var isInvalid;
	if (DOMPARSER in window) {
		parser = new window[DOMPARSER]();
		isInvalid = function (doc) {
			var errors = doc.getElementsByTagName('parsererror');
			return errors && errors.length !== 0;
			// var error = errors && errors.length !== 0 && errors[0];
			// if (error) {
			// 	throw new Error('Failed to parse XML string to document!'
			// 		+ '\n' + (error.textContent || error.innerText)
			// 	);
			// }
		};

		return function stringToDoc(xmlString) {
			var doc;

			if (!xmlString)
				return null;

			doc = parser.parseFromString(Utils.trim(xmlString), 'text/xml');

			if (isInvalid(doc))
				return null;

			return doc;
		};
	}
	if (ACTIVEXOBJECT in window) {
		isInvalid = function (doc) {
			var error = doc.parseError;
			return error && error.errorCode !== 0;
			// if (error && error.errorCode !== 0) {
			// 	throw new Error('Failed to parse XML string to document!'
			// 		+ '\nCode: ' + error.errorCode
			// 		+ '\nReason: ' + error.reason
			// 		+ '\nLine: ' + error.line
			// 	);
			// }
		};

		return function stringToDoc(xmlString) {
			var doc;

			if (!xmlString)
				return null;

			doc = new window[ACTIVEXOBJECT]('Microsoft.XMLDOM');
			doc.async = 'false';
			doc.loadXML(xmlString);

			if (isInvalid(doc))
				return null;

			return doc;
		};
	}
}());

var docToString = (function () {
	if (XMLSERIALIZER in window) {
		var serializer = new window[XMLSERIALIZER]();

		return function docToString(doc) {
			return serializer.serializeToString(doc);
		};
	}
	if (ACTIVEXOBJECT in window) {
		return function docToString(doc) {
			return doc.xml;
		};
	}
}());


var cleanString = (function () {

	var allWhitespaceRE = />\s+</g;
	var allTabsRE = /\t+/g;
	var allNewlinesRE = /[\r\n]+/g;
	var allCommentsRE = /<!--(.*?)-->/g;
	var allNonEntityAmpersandsRE = /&(?!amp;)/g;
	var allNamespacedAttrsRE = /xmlns(?::[^\s=]+)?=['"][^'"]*['"]/g;

	return function cleanString(xmlString) {
		// remove whitespace
		xmlString = xmlString.replace(allWhitespaceRE, "><");

		// remove tabs
		xmlString = xmlString.replace(allTabsRE, " ");

		// remove new lines
		xmlString = xmlString.replace(allNewlinesRE, " ");

		// remove comments
		xmlString = xmlString.replace(allCommentsRE, "");

		// entity encode & into &amp; without turning &amp; into &amp;amp;
		xmlString = xmlString.replace(allNonEntityAmpersandsRE, "&amp;");

		// remove all namespaced attributes
		xmlString = xmlString.replace(allNamespacedAttrsRE, "");

		return xmlString;
	};

}());


function ifNodeIsElement(node)
{
	return node.nodeType === 1;
}

function getChildElements(node)
{
	// node.childNodes includes every node.
	// node.children is supposed to be only element nodes,
	// except IE < 9 incorrectly includes comment nodes, too.
	var childElements = Utils.slice(node.children || node.childNodes || []);

	if (!childElements.length)
		return childElements;

	return Utils.filter(childElements, ifNodeIsElement);
}


function getNodeValue(doc, nodeName)
{
	if (!doc)
		return null;

	if (!nodeName)
	{
		if (!Utils.isNull(doc.firstChild))
			return doc.firstChild.nodeValue;

		return null;
	}

	var nodes = doc.getElementsByTagName(nodeName);

	// If there's more than one, return the first
	if (nodes.length > 0 && nodes[0].firstChild)
		return nodes[0].firstChild.nodeValue;
}


function getAttribute(node, attrName)
{
	if (node && node.attributes && attrName)
	{
		var attr = node.attributes.getNamedItem(attrName);

		if (attr)
			return attr.nodeValue;

		return null;
	}
}


function getParamValue(doc, paramName)
{
	if (!doc || !paramName)
		return null;

	var nodes = doc.getElementsByTagName('param');

	for (var node, i = 0, endi = nodes.length; i < endi; ++i)
	{
		node = nodes[i];

		if (getAttribute(node, 'name') === paramName)
			return getAttribute(node, 'value');
	}
}


function assignAttributes(obj, attributes)
{
	for (var attr, i = 0, endi = (attributes && attributes.length) || 0; i < endi; ++i)
	{
		attr = attributes[i];
		obj[attr.nodeName] = attr.nodeValue;
	}
}


var mapXmlCharToEntity = {
	'<': '&lt;',
	'>': '&gt;',
	'&': '&amp;',
	'"': '&quot;',
	'\'': '&apos;'
};

var mapXmlEntityToChar = {};

Utils.each(mapXmlCharToEntity, function (key, value) {
	mapXmlEntityToChar[value] = key;
});

var allBadXmlChars = new RegExp('[' + Utils.keys(mapXmlCharToEntity).join('') + ']', 'g');
var allXmlEntities = new RegExp('(' + Utils.keys(mapXmlEntityToChar).join('|') + ')', 'g');

function xmlCharToEntityReplacer(c) {
	return mapXmlCharToEntity[c];
}

function xmlEntityToCharReplacer(c) {
	return mapXmlEntityToChar[c];
}

function encodeXml(string) {
	return string.replace(allBadXmlChars, xmlCharToEntityReplacer);
}

function decodeXml(string) {
	return string.replace(allXmlEntities, xmlEntityToCharReplacer);
}


// Export public APIs
exports.stringToDoc = stringToDoc;
exports.docToString = docToString;
exports.cleanString = cleanString;
exports.getChildElements = getChildElements;
exports.getNodeValue = getNodeValue;
exports.getParamValue = getParamValue;
exports.getAttribute = getAttribute;
exports.assignAttributes = assignAttributes;
exports.encodeXml = encodeXml;
exports.decodeXml = decodeXml;

},{"../utils.js":50}],50:[function(require,module,exports){
/* cvp/utils.js */

'use strict';

// Load dependent modules
var App = require('../core/app');
var when = require('../vendor/when');
var Log = require('./log');


var logger = Log.getLogger();


var getStyle = (function () {

	if ('getComputedStyle' in window) {
		return function (el, prop) {
			return window.getComputedStyle(el, null).getPropertyValue(prop);
		};
	}

	var regex = /(-([a-z]))/g;
	function replacer(match, group1, group2) {
		return group2.toUpperCase();
	}

	return function (el, prop) {
		if (!('currentStyle' in el)) return null;

		if (prop === 'float') prop = 'styleFloat';

		prop = prop.replace(regex, replacer);

		return el.currentStyle[prop] || null;
	};

}());


/**
 * Returns the current time.
 * @returns {Number} The current time in milliseconds.
 */
var now = (typeof Date.now === "function" ? Date.now : function () { return (new Date()).getTime(); });

/**
 * @param {String} key - Name of cookie to retrieve.
 * @returns {String|Null} The value of the cookie, or null if not found.
 */
function getCookie(key)
{
	if (!key)
		return document.cookie;

	var id = encodeURIComponent(key) + '=';
	var cs = document.cookie.split('; ');

	for (var cn, i = 0, endi = cs.length; i < endi; ++i) {
		cn = cs[i];
		if (cn.indexOf(id) === 0)
			return decodeURIComponent(cn.substring(id.length, cn.length));
	}

	return null;
}

/**
 * @param {String} key - Name for the cookie.
 * @param {String} value - Value for the cookie.
 * @param {String|Number} timeExpire - Expiry of the cookie.
 */
function setCookie(key, value, timeExpire)
{
	if (!key)
		return;

	var expires = null;

	if (timeExpire) {
		if (isNaN(Number(timeExpire))) {
			expires = timeExpire;
		}
		else {
			expires = new Date(timeExpire).toUTCString();
		}
	}

	document.cookie = encodeURIComponent(key) + "=" + encodeURIComponent(value) + ( expires ? "; expires=" + timeExpire : "" ) + "; path=/";
}

/**
 * @param {String} key - the key string
 */
function deleteCookie(key)
{
	if (!key)
		return;

	setCookie(key, "", -1);
}

/**
 * Retrieves the object on the page by the passed in id.
 * @param {String} id The element's unique identifier.
 * @returns {Object} The desired element.
 */
function byId(id) {
	return document.getElementById(id);
}

/**
 * Performs the slice function on the passed array.
 * @param {Array|Arguments|*} arr The array to slice.
 * @param {Number} [index=0] The array's index to start the slice.
 * @returns {Array} The resulting array after it has been sliced at the
 * passed index.
 */
function slice(arr, index) {
	return Array.prototype.slice.call(arr, index || 0);
}

/**
 * Convenience method to hasOwnProperty (oft-used in for-in loops)
 * @returns {boolean}
 *   - true if the property is defined in the object;
 *   - false if the property is inherited from prototype chain
 */
var hasOwn = (
	typeof Object.prototype.hasOwnProperty === "function"
	? function(object, property) {
			return Object.prototype.hasOwnProperty.call(object, property);
		}
	: function (object, property) {
			return object[property] !== Object.prototype[property];
		}
);

/**
 * bind.
 * @param {Function} func - Function to bind.
 * @param {Object} scope - Context to bind func to.
 * @param {Object} [...args] - Optional arguments to bind to func parameters.
 * @returns {Function} The bound function.
 */
function bind(func, scope /*, args... */) {
	var args = slice(arguments, 2);
	return function() {
		var a = args.concat(slice(arguments));
		return func.apply(scope, a);
	};
}

/**
 * Tests to determine if the obj parameter is undefined.
 * @param {Object} obj - The object to test.
 * @returns {Boolean} The result: true or false.
 */
function undef(obj) {
	return typeof obj === "undefined";
}

/**
 * Tests to determine if the obj parameter is null.
 * @param {Object} obj - The object to test.
 * @returns {Boolean} The result: true or false.
 */
function isNull(obj) {
	return undef(obj) || obj === null;
}

/**
 * Tests to determine if the parameter's type is 'function'.
 * @param {Object} f The object to test.
 * @returns {Boolean} The result: true or false.
 */
function isFunction(f) {
	return typeof f === "function";
}

/**
 * Tests to determine if the parameter's type is 'object'.
 * @param {Object} obj The object to test.
 * @returns {Boolean} The result: true or false.
 */
function isObject(obj) {
	return typeof obj === "object";
}

// Test if the parameter is a simple object, e.g. { keys: values }.
// ...because Arrays have typeof "object"...
//     console.assert( typeof [] === "object" )
// ...but Object.prototype.toString tells its class...
//     console.assert( ({}).toString.call([]) === "[object Array]" )
// ...and this also rules out functions or anything with a constructor...
//     console.assert( typeof Math === "object" )
//     console.assert( ({}).toString.call(Math) === "[object Math]" )
function isSimpleObject(obj) {
	return Object.prototype.toString.call(obj) === "[object Object]";
}

/**
 * Tests to determine if the parameter's type is 'string'.
 * @param {Object} obj The object to test.
 * @returns {Boolean} The result: true or false.
 */
function isString(obj) {
	return typeof obj === "string";
}

/**
 * Tests to determine if the parameter's type is 'boolean'.
 * @param {Object} obj The object to test.
 * @returns {Boolean} The result: true or false.
 */
function isBoolean(obj) {
	return typeof obj === "boolean";
}

/**
 * Determine if the parameter is an array.
 * @param {Object} obj The object to test.
 * @returns {Boolean} The result: true or false.
 */
var isArray = (
	typeof Array.isArray === "function"
	? Array.isArray
	: function (object) {
		return Object.prototype.toString.call(object) === "[object Array]";
	}
);

/**
 * Determine if the param is null, or an empty String, Array or Object.
 * @param {Object} obj The object to test.
 * @returns {Boolean} The result: true or false.
 */
function isEmpty(obj) {
	if (isNull(obj)) {
		return true;
	}
	if (isString(obj) || isArray(obj)) {
		return obj.length === 0;
	}
	for (var key in obj) {
		if (hasOwn(obj, key)) {
			return false;
		}
	}
	return true;
}

/**
 * Determine the index of the value in an array/string.
 * @param {Object} obj The object to test.
 * @param {Object} val The target to search for.
 * @param {Number} i (optional) The index to start searching from (negative indexes are only allowed on arrays).
 * @returns The index of the value found or -1 if not found.
 * @type Number.
 */
var indexOf = (function () {
	var nativeArrayIndexOf = Array.prototype.indexOf;
	var arrayIndexOf = (
		isFunction(nativeArrayIndexOf)
		? nativeArrayIndexOf
		: function (val, i) {
			if (this) {
				var len = this.length;
				i = i ? i < 0 ? Math.max( 0, len + i ) : i : 0;
				for ( ; i < len; ++i ) {
					if ( i in this && this[i] === val ) {
						return i;
					}
				}
			}
			return -1;
		}
	);
	return function (obj, val, i) {
		if (isString(obj)) return obj.indexOf(val, i);  // String.indexOf since 1.0
		if (isArray(obj)) return arrayIndexOf.call(obj, val, i);  // Array.indexOf not in IE<9
	};
}());

/**
 * Tests to determine if the passed string flag is active.
 * The possible true values are "yes", "true", and "on".
 * All other values will return false.
 * @param {String} str The string flag.
 * @param {Boolean} bDefault The flag's default value.
 * @returns {Boolean} The result: true or false.
 */
function isFlagActive(str, bDefault)
{
	if (isBoolean(str))
		return str;

	if (isEmpty(str))
		return bDefault;

	switch (str.toLowerCase())
	{
		case "yes":
		case "true":
		case "on":
			return true;
		default:
			return false;
	}
}

/**
 * extend.
 * @param {Object} target - object to merge into (use {} if you don't want to modify target!)
 * @param {Object} source - properties of which are merged/copied into target
 * @param {Object} [...sources] - optional additional objects
 * @returns {Object} The target object.
 */
function extend(target, source /*, sources...*/) {
	var sources = arguments.length > 2 ? slice(arguments, 1) : source ? [source] : [];
	var i, endi, src, p;
	target = target || {};
	for (i = 0, endi = sources.length; i < endi; ++i) {
		src = sources[i];
		if (!src || !isSimpleObject(src) || target === src) { continue; }
		for (p in src) {
			target[p] = src[p];
		}
	}
	return target;
}

/**
 * merge.  A recursive extend, of sorts.
 * @param {Object} target - object to merge into (use {} if you don't want to modify target!)
 * @param {Object} source - properties of which are merged/copied into target
 * @param {Object} [...sources] - optional additional objects
 * @returns {Object} The target object, modified.
 */
function merge(target, source /*, sources...*/) {
	var sources = arguments.length > 2 ? slice(arguments, 1) : source ? [source] : [];
	var i, endi, src, p, v;
	target = isSimpleObject(target) ? target : {};
	for (i = 0, endi = sources.length; i < endi; ++i) {
		src = sources[i];
		if (!src || !isSimpleObject(src) || target === src) { continue; }
		for (p in src) {
			v = src[p];
			if (isSimpleObject(v) && v !== target) {
				target[p] = merge(target[p], v);
			}
			else {
				target[p] = v;
			}
		}
	}
	return target;
}

/**
 * Iterate over a collection.
 *
 * Callback signatures are:
 *
 * - Arrays: function(item, index, collection)
 * - Objects: function(key, value, collection)
 *
 * Returning false from the callback will break the loop.
 *
 * @param {Object} obj The collection to iterate over
 * @param {Function} callback The callback to invoke
 * @param {Object} [context=eachCollectionItem] - The context in which the callback is executed.
 */
function each(obj, callback, context) {
	var value, i, length;

	if (isArray(obj)) {
		for (i = 0, length = obj.length; i < length; ++i) {
			value = callback.call(context || obj[i], obj[i], i, obj);
			if (value === false) {
				break;
			}
		}
	}
	else {
		for (i in obj) {
			if (hasOwn(obj, i)) {
				value = callback.call(context || obj[i], i, obj[i], obj);
				if (value === false) {
					break;
				}
			}
		}
	}
}

// return array of keys from an object
function keys(obj) {
	var results = [];
	for (var key in obj) if (hasOwn(obj, key)) results.push(key);
	return results;
}

// map values of array to a new array
function map(array, iterator, context) {
	var results = [];
	if (array == null) return results;
	each(array, function (value, index, list) {
		results.push(iterator.call(context, value, index, list));
	});
	return results;
}

// remove duplicate values from array
function uniq(array) {
	var results = [];
	each(array, function (value /*, index, list*/) {
		if (indexOf(results, value) === -1) results.push(value);
	});
	return results;
}

function identity(value) {
	return value;
}

// if every value in array is truthy, return true; else return false
function every(array, iterator, context) {
	iterator = iterator || identity;
	var result = true;
	if (array == null) return result;
	each(array, function(value, index, list) {
		if (!(result = result && iterator.call(context, value, index, list))) return false;
	});
	return !!result;
}

// if some value in array is truthy, return true; else return false
function some(array, iterator, context) {
	iterator = iterator || identity;
	var result = false;
	if (array == null) return result;
	each(array, function(value, index, list) {
		if (result || (result = iterator.call(context, value, index, list))) return false;
	});
	return !!result;
}

// returns all values that meet iterator's condition
// if no iterator provided, removes falsey values from array
function filter(array, iterator, context) {
	iterator = iterator || identity;
	var results = [];
	if (array == null) return results;
	each(array, function (value, index, list) {
		if (iterator.call(context, value, index, list)) results.push(value);
	});
	return results;
}

// opposite of `filter`, rejects all values from array that meet iterator's condition
// if no iterator provided, removes truthy values from array
function reject(array, iterator, context) {
	iterator = iterator || identity;
	return filter(array, function (value, index, list) {
		return !iterator.call(context, value, index, list);
	}, context);
}

// find first item in array that meets iterator's condition
// if no iterator provided, returns first truthy value of array
function find(array, iterator, context) {
	iterator = iterator || identity;
	var result;
	some(array, function (value, index, list) {
		if (iterator.call(context, value, index, list)) {
			result = value;
			return true;
		}
	});
	return result;
}

// returns index of first item in array that meets iterator's condition
// if no iterator provided, returns index of first truthy value of array
function findIndex(array, iterator, context) {
	iterator = iterator || identity;
	var result = -1;
	some(array, function (value, index, list) {
		if (iterator.call(context, value, index, list)) {
			result = index;
			return true;
		}
	});
	return result;
}

/**
 * Throttle a function, so it will only be invoked once
 * for a given interval.
 *
 * The execution of the leading and trailing invocations
 * can be toggled via options:
 * {
 *		leading : false,
 *		trailing : false
 * }

 * @param {Function} func The function to throttle
 * @param {Number} wait The throttling interval
 * @param {Object} options The throttling options
 * @returns The desired element.
 */
// Borrowed from Underscore.js
function throttle(func, wait, options) {
	var context, args, result;
	var timeout = null;
	var previous = 0;
	options = options || {};
	wait = wait || 250;

	var later = function() {
		previous = options.leading === false ? 0 : now();
		timeout = null;
		result = func.apply(context, args);
	};

	return function() {
		var present = now();
		if (!previous && options.leading === false)
			previous = present;

		var remaining = wait - (present - previous);
		context = this;
		args = arguments;
		if (remaining <= 0) {
			window.clearTimeout(timeout);
			timeout = null;
			previous = present;
			result = func.apply(context, args);
		} else if (!timeout && options.trailing !== false) {
			timeout = window.setTimeout(later, remaining);
		}
		return result;
	};
}

/**
 * Debounce a function
 *
 * @param {Function} func The function to debounce
 * @param {Number} wait The debounce interval
 * @param {Boolean} [immediate=false] Whether the function should trigger on the leading edge, instead of the trailing.
 * @returns The debounced function.
 */
// Borrowed from Underscore.js
function debounce(func, wait, immediate) {
	var timeout, args, context, timestamp, result;
	wait = wait || 250;

	return function() {
		context = this;
		args = arguments;
		timestamp = now();

		var later = function() {
			var last = (now()) - timestamp;
			if (last < wait) {
				timeout = window.setTimeout(later, wait - last);
			} else {
				timeout = null;

				if (!immediate)
					result = func.apply(context, args);
			}
		};

		var callNow = immediate && !timeout;
		if (!timeout) {
			timeout = window.setTimeout(later, wait);
		}
		if (callNow)
			result = func.apply(context, args);
		return result;
	};
}

/**
 * template.
 * @param {Object} tmpl - String with placeholders.
 * @param {Object} ...args - Values to replace placeholders.
 * @returns {String} The interpolated template.
 */
function template(tmpl /*, args... */)
{
	var matches = tmpl.match(/\{[^{}]*\}/g),
		i = 0,
		len = 0,
		args = slice(arguments, 1),
		argc = args.length;

	if (!matches)
	{
		logger.info("No {placeholders} present in template: ", tmpl);
	}
	else
	{
		for (i = 0, len = matches.length; i < len && i < argc; ++i)
		{
			tmpl = tmpl.replace(matches[i], args[i]);
		}
	}

	return tmpl;
}

/**
 * Parses the passed object into a string inserting the passed
 * delimiter string.
 * @param {Object} obj The object to parse into a string.
 * @param {String} delimiter The string added to the result as a delimiter.
 * @returns {String} A string of the parsed object.
 */
function joinKeys(obj, delimiter)
{
	var arr = [],
		p;

	if (isNull(delimiter))
		delimiter = ",";

	for (p in obj) {
		if (hasOwn(obj, p)) {
			arr.push( p + "=" + obj[p] );
		}
	}

	return arr.join(delimiter);
}

/**
 * Returns whether a given url is an absolute url
 *
 * @param {String} url - the url to test.
 * @return {Boolean} whether the url is absolute.
 */
function isAbsolute(url) {
	return 0 === url.indexOf('//') || -1 !== url.indexOf('://');
}

/**
 * Returns whether a given url is a relative path
 *
 * @param {String} url - the url to test.
 * @return {Boolean} whether the url is relative.
 */
function isRelative(url) {
	return !isAbsolute(url);
}

// RegExp used by parseUrl.
var urlRegex = /^((([^:\/?#]+):)?(?:\/\/(([^\/?#]*)(?::(\d+))?))?)(([^?#]*\/)([^\/?#]*)?)(\?([^#]*))?(#(.*))?$/;

/**
 * Parse a URL into an object/hash.
 *
 * @param {String} url - URL to interpret into pieces.
 * @return {Object} hash of URL parts
 *
 * Given the URL "http://host.com:8080/dir/file?query#anchor"
 *   - href [0] = same as input
 *   - origin [1] = "http://host.com:8080" (protocol + '//' + host)
 *   - protocol [2] = "http:"
 *   - scheme [3] = "http"
 *   - host [4] = "host.com:8080" (hostname + ':' + port)
 *   - hostname [5] = "host.com"
 *   - port [6] = "8080"
 *   - pathname [7] = "/path/file"
 *   - dir [8] = "/path/"
 *   - file [9] = "file"
 *   - search [10] = "?search"
 *   - query [11] = "query"
 *   - hash [12] = "#anchor"
 *   - anchor [13] = "anchor"
 */
function parseUrl(url) {
	// "normalize" URL: turn relative paths into full URLs relative to page URL
	// (I reserve "canonical" for when full URL + explicit ports are needed.)
	var a = document.createElement('a');
	a.href = url;
	var href = a.href;
	a = null;

	var match = urlRegex.exec(href);

	/* eslint-disable key-spacing, no-multi-spaces */
	if (match) {
		return {
			href     : match[0]  || '',
			origin   : match[1]  || '',
			protocol : match[2]  || '',
			scheme   : match[3]  || '',
			host     : match[4]  || '',
			hostname : match[5]  || '',
			port     : match[6]  || '',
			pathname : match[7]  || '',
			dir      : match[8]  || '',
			file     : match[9]  || '',
			search   : match[10] || '',
			query    : match[11] || '',
			hash     : match[12] || '',
			anchor   : match[13] || ''
		};
	}
	/* eslint-enable key-spacing, no-multi-spaces */
}

// Regexp used by parseQueryString
var queryStringRegex = /[?&]?([^&=]+)(?:=([^&=]+))?/g;

/**
 * Parse a query string into an object/hash.
 *
 * @param {String} queryString - query string
 * @return {Object} hash of query string params
 */
function parseQueryString(queryString) {
	var queryObject = {};

	if (isNull(queryString))
		return queryObject;

	queryString.replace(queryStringRegex, function ($0, $1, $2 /*, offset, string*/) {
		queryObject[$1] = $2 ? window.decodeURIComponent($2) : '';
		return $0;
	});

	return queryObject;
}

/**
 * Returns a map of query params
 *
 * @param {String} [str=location.href] - The optional query string source; otherwise, defaults to current page URL.
 * @returns {Object} A map of query params
 */
function getQueryParams(str) {
	var params = {};
	var url = str || window.location.href;
	var urlParts = parseUrl(url);
	if (urlParts.query) {
		params = parseQueryString(urlParts.query);
	}
	return params;
}

/**
 * Returns a query param value
 *
 * @param {String} key - The query param to retrieve
 * @returns {String} The query param value
 */
function getQueryParam(key) {
	return getQueryParams()[key];
}

function encodeParams(hash) {
	var queryArray = [];
	var encode = window.encodeURIComponent;
	var key;
	var value;

	for (key in hash) {
		if (!hasOwn(hash, key)) continue;
		value = hash[key];
		queryArray.push( encode(key) + (value !== '' ? '=' + encode(isObject(value) ? JSON.stringify(value) : value) : '') );
	}

	return queryArray.join('&').replace( /%20/g, '+' );
}

/**
 * Convert object/hash into a query string (naively).
 *
 * @param {Object} hash - key/value hash
 * @return {String} query string
 */
function toQueryString(hash) {
	var params = encodeParams(hash);

	return params ? '?' + params : '';
}

/**
 * Append query hash values onto an existing URL.
 *
 * @param {String} url - existing URL
 * @param {Object} data - query params as key/value hash
 * @return {String} full URL
 */
function augmentQueryString(url, data) {
	var urlParts = parseUrl(url);
	var queryParts = parseQueryString(urlParts.query);

	queryParts = extend(queryParts, data);

	var queryString = toQueryString(queryParts);

	return urlParts.origin + urlParts.pathname + queryString + urlParts.hash;
}

/**
 * Compares window's origin to provided URL's origin.
 *
 * @requires parseUrl
 * @param {String} url - URL to test
 * @return {Boolean} whether the URL is cross-domain
 */
function isCrossDomain(url) {
	var defaultPorts = {
		'http:'  : '80',
		'https:' : '443'
	};
	url = parseUrl(url);
	return url.hostname.toLowerCase() !== location.hostname.toLowerCase()
		|| (url.port || defaultPorts[url.protocol]) !== (location.port || defaultPorts[location.protocol])
		|| url.protocol.toLowerCase() !== location.protocol.toLowerCase();
}

/**
 * Prints the logs.
 */
function log() {
	if (window.console
		&& window.console.log
		&& App.DEBUG) {
		window.console.log(slice(arguments).join(" | "));
	}
}

/**
 * Prints the logs.
 * @param {Object} obj - Object to log.
 * @param {String} str - Label for object being logged.
 */
function print(obj, str) {
	var o;

	if (str !== "nested")
		logger.info("Print all values for ", str);

	for (o in obj)
	{
		if (hasOwn(obj, o))
		{
			if (!o)
				continue;

			if (isObject(obj[o]))
			{
				logger.info("Printing nested object value", o);
				print(obj[o], "nested");
			}
			else
			{
				logger.info(str === "nested" ? "\t" : "", "key:", o, "value:", obj[o]);
			}
		}
	}

	if (str !== "nested")
	{
		logger.info("End Print all values for ", str);
	}
}


// Using `when` to wrap a Promise around setTimeout.
// Usage: delay(500).then(doSomething)
// Overkill? Maybe.
function delay(ms) {
	var deferred = when.defer();
	setTimeout(deferred.resolve, ms);
	return deferred.promise;
}

// Using `when` to create a Promise that will reject on timeout.
// Usage: timeout(Ajax.get(url), 5000)
// This creates a new Promise that can be resolved by the Promise
// of the first param, but can also be rejected after time passes.
// Remember: A Promise/Deferred can only be resolved once!
function timeout(promise, ms) {
	var deferred = when.defer();

	promise.then(deferred.resolve, deferred.reject);

	setTimeout(function () {
		deferred.reject( new Error("timed out") );
	}, ms);

	return deferred.promise;
}


/**
 * Returns a random integer from a range (min, max).
 * If only a single parameter is provided, it's assumed max and min is 0.
 *
 * @param {Number} min - Lowest possible value in range.
 * @param {Number} max - Highest possible value in range.
 * @returns {Number} random number for the range (2 params) or 0 to param (1 param)
 */
function getRandomInt(min, max)
{
	return (
		arguments.length === 1
		? Math.floor(Math.random() * min)
		: Math.floor(Math.random() * (max - min + 1)) + min
	);
}

function toNum(str) {
	return +(str);
}

function toInt(str) {
	return parseInt(str, 10);
}

function toFixed(value) {
	return Number(value).toFixed(3);
}

/**
 * Returns random string.
 * @param {Number} length - Number of characters.
 * @returns {String} random string of length length.
 */
function randomString(length)
{
	var str = '';
	var i;
	var chars = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz'.split('');

	if (!length)
	{
		length = getRandomInt(chars.length);
	}

	for (i = 0; i < length; i++)
	{
		str += chars[getRandomInt(chars.length)];
	}
	return str;
}


var regexpMetaCharRegex = /[.*+?^${}()|[\]\\]/g;

function escapeRegExp(string) {
	return String(string).replace(regexpMetaCharRegex, '\\$&');
}


/**
 * Utility to replace text at the end of a string.
 *
 * @param {String} str - String to modify.
 * @param {String} search - Substring to search for at end of `str`.
 * @param {String} replace - Value which replaces `search`.
 * @returns {String} Modified string.
 */
function replaceExtension(str, search, replace) {
	if (isEmpty(str))
		return str;

	var escapedSearch = escapeRegExp(search);
	return str.replace(new RegExp(escapedSearch + "$", "i"), replace);
}


var trim = (function () {
	// find all whitespace and nonbreakingspace (CMS1 anyone?) at front & rear of string
	var reAllSpaceAtEnds = /^[\s\xA0]+|[\s\xA0]+$/g;
	var nativeTrim = String.prototype.trim;

	return (
		isFunction(nativeTrim)
		? function (str) {
			return isNull(str) ? "" : nativeTrim.call(str);
		}
		: function (str) {
			return isNull(str) ? "" : String(str).replace(reAllSpaceAtEnds, "");
		}
	);
}());

/**
 * Simple AOP functionality
 */
function before(scope, func, action) {
	return function() {
		action.apply(scope, arguments);
        var result = func.apply(scope, arguments);
        return result;
    };
}

function after(scope, func, action) {
	return function() {
		var args = slice(arguments);
        var result = func.apply(scope, args);
        action.apply(scope, [args, result]);
        return result;
    };
}

function around(scope, func, action) {
	return function() {
        return action.apply(scope, [func, slice(arguments)]);
    };
}


var allUuidPatternChars = /[xy]/g;

function intoUuidChar(c) {
	var r = Math.random()*16|0;
	var v = c === 'x' ? r : (r&0x3|0x8);
	return v.toString(16);
}

function uuid() {
	return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(allUuidPatternChars, intoUuidChar);
}


// Export public APIs
exports.trim = trim;
exports.getStyle = getStyle;
exports.now = now;
exports.getCookie = getCookie;
exports.setCookie = setCookie;
exports.deleteCookie = deleteCookie;
exports.undef = undef;
exports.isUndefined = undef;
exports.nil = isNull;
exports.isNull = isNull;
exports.empty = isEmpty;
exports.isEmpty = isEmpty;
exports.isFunc = isFunction;
exports.isFunction = isFunction;
exports.isObject = isObject;
exports.isSimpleObject = isSimpleObject;
exports.isString = isString;
exports.isBoolean = isBoolean;
exports.isArray = isArray;
exports.indexOf = indexOf;
exports.isFlagActive = isFlagActive;
exports.extend = extend;
exports.merge = merge;
exports.each = each;
exports.keys = keys;
exports.map = map;
exports.uniq = uniq;
exports.identity = identity;
exports.every = every;
exports.some = some;
exports.filter = filter;
exports.reject = reject;
exports.find = find;
exports.findIndex = findIndex;
exports.throttle = throttle;
exports.debounce = debounce;
exports.byId = byId;
exports.slice = slice;
exports.hasOwn = hasOwn;
exports.bind = bind;
exports.template = template;
exports.joinKeys = joinKeys;
exports.isAbsolute = isAbsolute;
exports.isRelative = isRelative;
exports.isRelativePath = isRelative;
exports.parseUrl = parseUrl;
exports.parseQueryString = parseQueryString;
exports.getQueryParams = getQueryParams;
exports.getQueryParam = getQueryParam;
exports.encodeParams = encodeParams;
exports.toQueryString = toQueryString;
exports.augmentQueryString = augmentQueryString;
exports.isCrossDomain = isCrossDomain;
exports.log = log;
exports.print = print;
exports.delay = delay;
exports.timeout = timeout;
exports.getRandomInt = getRandomInt;
exports.toNum = toNum;
exports.toInt = toInt;
exports.toFixed = toFixed;
exports.randomString = randomString;
exports.escapeRegExp = escapeRegExp;
exports.replaceExtension = replaceExtension;
exports.before = before;
exports.after = after;
exports.around = around;
exports.uuid = uuid;

},{"../core/app":8,"../vendor/when":96,"./log":28}],51:[function(require,module,exports){
/* cvp/version.js */

// ## Version
//
// The constructor is hoping for a string that looks like a version string,
// up to three numbers separated by a single non-digit character,
// e.g., "1.2.3", "something 1.2-30", "\_1\_2\_345\_", but some notes:
//
//   - it ignores alpha and beta releases: "1.2b34" is treated as "1.2.0"
//   - something like "sdf1-sdf2-sdf3" will be interpreted simply as "1.0.0"
//   - something like "1.4something5" will simply be read as "1.4.0"
//   - something like "1.2.3.4.5" will simply be read as "1.2.3" (first three)

var regex = /(\d+)(?:(\D)(\d+))?(?:(\D)(\d+))?/;
var greekRE = /^[abd]$/;  // test for alpha, beta, dev

// The constructor expects a string that looks like a version number.
// `NaN` is the default for any missing values (matching earlier logic).
function Version(string) {
	if (typeof string !== 'string') { throw new TypeError('Version() only accepts a string'); }

	this.raw = string;
	this.major = NaN;
	this.minor = NaN;
	this.revision = NaN;

	var matches = regex.exec(string);

	if (!matches) {
		throw new SyntaxError('Version string "' + this.raw + '" failed to parse as a valid version number');
	}

	var major = matches[1];
	var minor = matches[3];
	var revision = matches[5];

	this.major = major;

	if (minor && !greekRE.test(matches[2])) {
		this.minor = minor;
		if (revision && !greekRE.test(matches[4])) {
			this.revision = revision;
		}
	}
}

// `toString()` returns a string representation of the interpreted version.
Version.prototype.toString = function () {
	return this.toArray().join('.');  // shallow array
};

// `toArray()` can return either a shallow [1] or full [1, 0, 0] array.
Version.prototype.toArray = function (full) {
	var pieces = [this.major, this.minor, this.revision];
	var array = [];

	for (var num, i = 0, endi = pieces.length; i < endi; ++i) {
		num = +(pieces[i]);
		if (!full && isNaN(num)) { break; }
		array.push(num);
	}

	return array;
};

// `compare(version, [shallow])` returns:
// 1 if this > version; -1 if this < version; 0 if this is same as version.
//
// But "same as" is subjective:  Does "1.2.3" equal "1.2"?
//
// The `shallow` option is used for the `is()` method -- a relaxed equals.
// `"1.2.3".is("1.2")` is `true`; whereas, `"1.2".is("1.2.3")` is `false`.
Version.prototype.compare = function (version, shallow) {
	if (!(version instanceof Version)) {
		return this.compare(new Version(version), shallow);
	}

	var i = 0,
		endi = 0,
		cmpArray,
		thisArray = this.toArray(true),
		a, b;

	cmpArray = shallow ? version.toArray() : version.toArray(true);
	for (endi = cmpArray.length; i < endi; ++i) {
		a = +(thisArray[i]) || 0;
		b = +(cmpArray[i]) || 0;
		if (a > b) { return 1; }
		else if (a < b) { return -1; }
	}
	return 0;
};

// Some helper methods to make comparisons.
Version.prototype.is = function (version) { return this.compare(version, true) === 0; };
Version.prototype.eq = function (version) { return this.compare(version) === 0; };
Version.prototype.gt = function (version) { return this.compare(version) > 0; };
Version.prototype.lt = function (version) { return this.compare(version) < 0; };

// The inverse of the above methods.
Version.prototype.isnt = function (version) { return !this.is(version); };
Version.prototype.ne = function (version) { return !this.eq(version); };
Version.prototype.lte = function (version) { return !this.gt(version); };
Version.prototype.gte = function (version) { return !this.lt(version); };

module.exports = Version;

},{}],52:[function(require,module,exports){
/* html5/ads/adserverproxy.js */

// Load dependent modules
var Class = require('../../vendor/Class');
var Event = require('../../cvp/customevent');
var Log = require('../../cvp/log');
var AdTypes = require('./adtypes');
var VideoStates = require('./videostates');
var FreeWheelImpl = require('./freewheelimpl');
var DFPImpl = require('./dfpimpl');
var Errors = require('../../cvp/errors');


var Messages = Errors.ErrorMessage;
var log = Log.getLogger('AdServerProxy');

var AdServerProxy = Class.extend({

	init : function(adPolicy)
	{
		this._adPolicy = adPolicy;

		this.eAdPlay = new Event();
		this.eAdPlayhead = new Event();
		this.eAdEnd = new Event();
		this.eAdError = new Event();
		this.eAdClick = new Event();
		this.eAdReady = new Event();
		this.eAdStopped = new Event();

		this._adServerType = this._adPolicy.ads.attr.type.toLowerCase();
		this._adServer = this._createImpl();

		if (!this._adServer) {
			log.warn('Unable to create ad server impl.');
			this._onAdError(Messages.AD_SERVER_NOT_CONFIGURED);
			return;
		}

		this._listen(true);
	},

	_listen : function (bool) {
		if (this._isListening === bool) return;
		this._isListening = bool;
		var method = bool ? 'addListener' : 'removeListener';

		this._adServer.eAdPlay[method](this._onAdPlay, this);
		this._adServer.eAdPlayhead[method](this._onAdPlayhead, this);
		this._adServer.eAdEnd[method](this._onAdEnd, this);
		this._adServer.eAdError[method](this._onAdError, this);
		this._adServer.eAdClick[method](this._onAdClick, this);
		this._adServer.eAdReady[method](this._onAdReady, this);
		this._adServer.eAdStopped[method](this._onAdStopped, this);
	},

	_createImpl : function() {
		var type = this._adServerType;
		switch (type) {
			case 'freewheel': return new FreeWheelImpl(this._adPolicy);
			case 'dfp': return new DFPImpl(this._adPolicy);
			default: return null;
		}
	},

	setAdSection : function(adSection)
	{
		if (this._adServer) {
			this._adServer.setAdSection(adSection);
		}
	},

	getAdSection : function()
	{
		if (this._adServer) {
			return this._adServer.getAdSection();
		}
	},

	setAdKeyValue : function (key, value)
	{
		if (this._adServer) {
			this._adServer.setAdKeyValue(key, value);
		}
	},

	getAdKeyValue : function (key)
	{
		if (this._adServer) {
			return this._adServer.getAdKeyValue(key);
		}
	},

	getAdKeyValues : function ()
	{
		if (this._adServer) {
			return this._adServer.getAdKeyValues();
		}
	},

	clearAdKeyValues : function ()
	{
		if (this._adServer) {
			this._adServer.clearAdKeyValues();
		}
	},

	setVideoState : function (state)
	{
		if (this._adServer) {
			this._adServer.setVideoState(state);
		}
	},

	setVideoStatePlaying : function () {
		this.setVideoState(VideoStates.PLAYING);
	},

	setVideoStatePaused : function () {
		this.setVideoState(VideoStates.PAUSED);
	},

	setVideoStateStopped : function () {
		this.setVideoState(VideoStates.STOPPED);
	},

	setVideoStateCompleted : function () {
		this.setVideoState(VideoStates.COMPLETED);
	},

	getVideoState : function ()
	{
		if (this._adServer) {
			return this._adServer.getVideoState();
		}
	},

	setVideoDisplayBase : function(videoDisplayBase)
	{
		if (this._adServer) {
			this._adServer.setVideoDisplayBase(videoDisplayBase);
		}
	},

	isEnabled : function() {
		return this._adServer ? this._adServer.enabled : false;
	},

	playPreroll : function() {
		if (this._adServer) {
			this._adServer.playPreroll();
		}
	},

	getMidrolls : function () {
		if (this._adServer) {
			return this._adServer.getMidrolls();
		}
	},

	playMidroll : function(index) {
		if (this._adServer) {
			this._adServer.playMidroll(index);
		}
	},

	playPostroll : function() {
		if (this._adServer) {
			this._adServer.playPostroll();
		}
	},

	loadAds : function(catalogEntry)
	{
		if (this._adServer) {
			this._adServer.loadAds(catalogEntry);
		}
	},

	stop : function ()
	{
		if (this._adServer) {
			this._adServer.stop();
		}
	},

	dispose : function ()
	{
		if (this._adServer) {
			this._listen(false);
			this._adServer.dispose();
			this._adServer = null;
		}
	},

	getCurrentAdId : function () {
		if (this._adServer) {
			return this._adServer.getCurrentAdId();
		}
		return '';
	},

	getCurrentAdDuration : function () {
		if (this._adServer) {
			return this._adServer.getCurrentAdDuration();
		}
		return 0;
	},

	getCurrentAdType : function () {
		if (this._adServer) {
			return this._adServer.getCurrentAdType();
		}
	},

	getAdAnalyticsData : function ()
	{
		if (this._adServer) {
			return this._adServer.getAdAnalyticsData();
		}
	},

	getAdAnalyticsDataForCurrentSlot : function ()
	{
		if (this._adServer) {
			return this._adServer.getAdAnalyticsDataForCurrentSlot();
		}
	},

	_onAdPlay : function (data) {
		log.debug('_onAdPlay', data.adType);
		this.eAdPlay.dispatch(data);
	},

	_onAdPlayhead : function (playhead, duration) {
		this.eAdPlayhead.dispatch(playhead, duration);
	},

	_onAdEnd : function (data) {
		log.debug('_onAdEnd', data.adType);
		this.eAdEnd.dispatch(data);
	},

	_onAdError : function (data) {
		log.debug('_onAdError', data.adType);
		this.eAdError.dispatch(data);
	},

	_onAdClick : function (data) {
		log.debug('_onAdClick', data.adType, data.url);
		this.eAdClick.dispatch(data.url);
	},

	_onAdReady : function () {
		log.log('_onAdReady');
		this.eAdReady.dispatch();
	},

	_onAdStopped : function () {
		log.log('_onAdStopped');
		this.eAdStopped.dispatch();
	}

});

AdServerProxy.PREROLL = AdTypes.PREROLL;
AdServerProxy.MIDROLL = AdTypes.MIDROLL;
AdServerProxy.POSTROLL = AdTypes.POSTROLL;


// Export public APIs
module.exports = AdServerProxy;

},{"../../cvp/customevent":16,"../../cvp/errors":25,"../../cvp/log":28,"../../vendor/Class":93,"./adtypes":53,"./dfpimpl":55,"./freewheelimpl":56,"./videostates":57}],53:[function(require,module,exports){
/* html5/ads/adtypes.js */

module.exports = {
	PREROLL : "preroll",
	MIDROLL : "midroll",
	POSTROLL : "postroll"
};

},{}],54:[function(require,module,exports){
/* html5/ads/baseimpl.js */

// Load dependent modules
var Class = require('../../vendor/Class');
var Utils = require('../../cvp/utils');
var Event = require('../../cvp/customevent');
var Log = require('../../cvp/log');


var log = Log.getLogger();

var AdImplBase = Class.extend({

	init : function(adPolicy)
	{
		this._adPolicy = adPolicy;
		this._keyValues = {};

		this._adServerInfo = this._adPolicy.ads;
		this._adSection = this.getConfigProperty("adSection");

		this.currentAdType = '';
		this.currentAdId = '';
		this.currentAdDuration = 0;

		this.eAdPlayhead = new Event();
		this.eAdPlay = new Event();
		this.eAdEnd = new Event();
		this.eAdError = new Event();
		this.eAdClick = new Event();
		this.eAdReady = new Event();
		this.eAdStopped = new Event();
	},

	stop : function ()
	{
		// define in impl
	},

	dispose : function ()
	{
		// define in impl
	},

	setAdSection : function(adSection)
	{
		this._adSection = adSection;
	},

	getAdSection : function()
	{
		return this._adSection;
	},

	setAdKeyValue : function (key, value)
	{
		if (key) {
			if (value === null) {
				delete this._keyValues[key];
			}
			else {
				this._keyValues[key] = value;
			}
		}
	},

	getAdKeyValue : function (key)
	{
		return this._keyValues[key] || undefined;
	},

	getAdKeyValues : function ()
	{
		return this._keyValues;
	},

	clearAdKeyValues : function ()
	{
		this._keyValues = {};
	},

	setVideoState : function (state)
	{
		this._videoState = state;
	},

	getVideoState : function ()
	{
		return this._videoState;
	},

	getAdAnalyticsData : function ()
	{
	},

	getAdAnalyticsDataForCurrentSlot : function ()
	{
	},

	getMidrolls : function ()
	{
		return {};
	},

	getConfigProperty : function (key)
	{
		var value = null;
		var required = false;
		switch (key)
		{
			case 'adApi':
				value = this._adServerInfo.apiUrl;
				required = true;
				break;
			case 'adServerRootUrl':
				value = this._adServerInfo.adServerRootUrl;
				required = true;
				break;
			case 'adNetworkId':
				value = this._adServerInfo.adNetworkId;
				required = true;
				break;
			case 'adVideoNetworkId':
				value = this._adServerInfo.adVideoNetworkId;
				required = true;
				break;
			case 'adVideoAssetId':
				value = this._adServerInfo.adVideoAssetId;
				required = true;
				break;
			case 'adSection':
				value = this._adServerInfo.adSection;
				required = true;
				break;
			case 'adPlayerProfile':
				value = this._adServerInfo.adPlayerProfile;
				required = true;
				break;
			case 'renderersUrl':
				value = this._adServerInfo.renderersUrl;
				required = false;
				break;
			case 'sensitiveFallbackId':
				value = this._adServerInfo.sensitiveFallbackId;
				required = false;
				break;
			case 'adLiveContentDuration':
				value = this._adServerInfo.liveDuration;
				required = false;
				break;
      case 'keyValues':
        value = this._adServerInfo.keyValues;
        required = false;
        break;
		}

		if (Utils.empty(value))
		{
			if (required)
				log.error("the following required ad server config value is missing:", key);
			else
				log.warn("the following optional ad server config value is missing:", key);
		}

		return value;
	},

	getCurrentAdType : function () {
		return this.currentAdType;
	},

	getCurrentAdId : function () {
		return this.currentAdId;
	},

	getCurrentAdDuration : function () {
		return this.currentAdDuration;
	},

	_emitAdPlay : function (opts) {
		var data = Utils.extend({
			ads : [],
			token : null,
			mode : 'standard',
			id : this.getCurrentAdId(),
			duration : this.getCurrentAdDuration(),
			adType : this.getCurrentAdType(),
			adAnalyticsData : this.getAdAnalyticsDataForCurrentSlot()
		}, opts);

		this.eAdPlay.dispatch(data);
	},

	_emitAdEnd : function (opts) {
		var data = Utils.extend({
			token : null,
			mode : 'standard',
			id : this.getCurrentAdId(),
			adType : this.getCurrentAdType()
		}, opts);

		this.eAdEnd.dispatch(data);
	},

	_emitAdError : function (opts) {
		var data = Utils.extend({
			id : this.getCurrentAdId(),
			adType : this.getCurrentAdType(),
			errors : []
		}, opts);

		this.eAdError.dispatch(data);
	},

	_emitAdClick : function (opts) {
		var data = Utils.extend({
			id : this.getCurrentAdId(),
			adType : this.getCurrentAdType(),
			url : ''
		}, opts);

		this.eAdClick.dispatch(data);
	}

});


// Export public APIs
module.exports = AdImplBase;

},{"../../cvp/customevent":16,"../../cvp/log":28,"../../cvp/utils":50,"../../vendor/Class":93}],55:[function(require,module,exports){
/* html5/ads/dfpimpl.js */

// Load dependent modules
var AdImplBase = require('./baseimpl');
var Utils = require('../../cvp/utils');
var ConfigUtils = require('../../cvp/util/configutils');
var Errors = require('../../cvp/errors');
var Log = require('../../cvp/log');
var AdTypes = require('./adtypes');


var Messages = Errors.ErrorMessage;
var log = Log.getLogger("DFPImpl");

var DFPImpl = AdImplBase.extend({

	init : function()
	{
		log.debug("Instantiating");

		this._super.apply(this, arguments);

		this.enabled = false;

		this._loader = new window.google.ima.AdsLoader();
		this._loader.addEventListener(
			window.google.ima.AdsLoadedEvent.Type.ADS_LOADED,
			Utils.bind(this._onAdsLoaded, this));

		this._loader.addEventListener(
			window.google.ima.AdErrorEvent.Type.AD_ERROR,
			Utils.bind(this._onAdLoadError, this));
	},

	setVideoDisplayBase : function(videoDisplayBase)
	{
		log.debug("setVideoDisplayBase", videoDisplayBase);
		if (!Utils.undef(window.google) && !Utils.undef(window.google.ima.AdsLoader))
		{
			this.displayBase = videoDisplayBase;
			this.enabled = true;
			log.debug("setVideoDisplayBase", "videoDisplayBase", "enabling");
		}
	},

	loadAds : function(catalogEntry)
	{
		this._catalogEntry = catalogEntry;
		this.playPreroll();
	},

	playPreroll : function() {
		var preroll = this._catalogEntry.getAdUrl("preroll");
		var url = this._constructAdUrl(preroll, this._catalogEntry);

		this.currentAdType = DFPImpl.PREROLL;
		this.loadAds(url);
	},

	playPostroll : function() {
		var postroll = this._catalogEntry.getAdUrl("postroll");
		var url = this._constructAdUrl(postroll, this._catalogEntry);

		this.currentAdType = DFPImpl.POSTROLL;
		this.loadAds(url);
	},

	_convertAdTypeFromImpl : function (implAdType) {
		switch (implAdType) {
			case DFPImpl.PREROLL: return AdTypes.PREROLL;
			case DFPImpl.POSTROLL: return AdTypes.POSTROLL;
		}
	},

	getCurrentAdType : function () {
		return this._convertAdTypeFromImpl(this.currentAdType);
	},

	_loadAdUrl : function (url) {
		log.debug("_loadAdUrl - adTag", url);

		var adsRequest = {
			adTagUrl : url,
			adType : "video"
		};

		this._loader.requestAds(adsRequest);
	},

	_constructAdUrl : function(entryAdTag, catalogEntry) {
		var adTag = "";
		var baseUrl = this.getConfigProperty("adServerRootUrl");

		if (Utils.empty(entryAdTag))
			adTag = baseUrl;
		else if (Utils.isRelative(entryAdTag))
			adTag = this._addToTag(baseUrl, entryAdTag);
		else
			adTag = entryAdTag;

		var keyValues = this.getAdKeyValues();
		for (var key in keyValues) {
			if (Utils.hasOwn(keyValues, key)) {
				var newPair = key + "=" + keyValues[key];
				adTag = this._addToTag(adTag, newPair);
			}
		}

		adTag = ConfigUtils.stringReplace(adTag, catalogEntry);

		return adTag;
	},

	_addToTag : function(adTag, pair) {
		if (!adTag) {
			return pair;
		}

		var delimiter = ";";
		if (adTag.charAt(adTag.length - 1) === ";")
			delimiter = "";

		return adTag + delimiter + pair;
	},

	_onAdsLoaded : function(event) {
		log.debug("_onAdsLoaded");

		var adsManager = event.getAdsManager();
		adsManager.addEventListener(
			window.google.ima.AdErrorEvent.Type.AD_ERROR,
			Utils.bind(this._onAdError, this)
		);
		adsManager.addEventListener(
			window.google.ima.AdEvent.Type.CONTENT_PAUSE_REQUESTED,
			Utils.bind(this._onPauseRequested, this)
		);
		adsManager.addEventListener(
			window.google.ima.AdEvent.Type.CONTENT_RESUME_REQUESTED,
			Utils.bind(this._onResumeRequested, this)
		);

		try {
			var playSettings = {
				restoreContentState : false
			};

			var element = document.getElementById(this.displayBase).getElementsByTagName("video")[0];

			adsManager.play(element, playSettings);

			this._emitAdPlay();
		} catch (adError) {
			log.error("ad play error", adError.toString());

			this._emitAdError({
				errorMessage : Messages.AD_EXISTS_BUT_FAILED,
				errors : [adError.toString()]
			});
		}
	},

	_onAdLoadError : function() {
		log.debug("_onAdLoadError");

		this._emitAdError({
			errorMessage : Messages.AD_EXISTS_BUT_FAILED,
			errors : event.errors
		});
	},

	_onPauseRequested : function() {
		log.debug("_onPauseRequested");
	},

	_onResumeRequested : function() {
		log.debug("_onResumeRequested");

		this._emitAdEnd();
	},

	_onAdError : function(event) {
		log.debug("_onAdError", event.toString());

		this._emitAdError({
			errorMessage : Messages.AD_EXISTS_BUT_FAILED,
			errors : [event.toString()]
		});
	}

});

DFPImpl.PREROLL = "dfpPreroll";
DFPImpl.POSTROLL = "dfpPostroll";


// Export public APIs
module.exports = DFPImpl;

},{"../../cvp/errors":25,"../../cvp/log":28,"../../cvp/util/configutils":43,"../../cvp/utils":50,"./adtypes":53,"./baseimpl":54}],56:[function(require,module,exports){
/* html5/ads/freewheelimpl.js */

// Load dependent modules
var AdImplBase = require('./baseimpl');
var Utils = require('../../cvp/utils');
var ConfigUtils = require('../../cvp/util/configutils');
var Timing = require('../../cvp/util/timing');
var Errors = require('../../cvp/errors');
var Log = require('../../cvp/log');
var AdTypes = require('./adtypes');
var VideoStates = require('./videostates');
var AdExt = require('../extensions/ad-kvps');


var Messages = Errors.ErrorMessage;
var log = Log.getLogger("FreeWheelImpl");

var State = {
	READY : 0,
	CREATING_NEW_CONTEXT : 1,
	STOPPING_ADS : 2
};

var FreeWheelImpl = AdImplBase.extend({

	init : function()
	{
		log.log("Instantiating");

		this._super.apply(this, arguments);

		this._state = State.READY;
		this._freewheel = null;
		this.enabled = false;

		if (Utils.isObject(window.tv) && Utils.isObject(window.tv.freewheel) && Utils.isObject(window.tv.freewheel.SDK))
		{
			if (!FreeWheelImpl.initialized)
			{
				FreeWheelImpl.SDK = window.tv.freewheel.SDK;

				FreeWheelImpl.PREROLL = FreeWheelImpl.SDK.TIME_POSITION_CLASS_PREROLL;
				FreeWheelImpl.MIDROLL = FreeWheelImpl.SDK.TIME_POSITION_CLASS_MIDROLL;
				FreeWheelImpl.POSTROLL = FreeWheelImpl.SDK.TIME_POSITION_CLASS_POSTROLL;

				FreeWheelImpl.VIDEO_PLAYING = FreeWheelImpl.SDK.VIDEO_STATE_PLAYING;
				FreeWheelImpl.VIDEO_PAUSED = FreeWheelImpl.SDK.VIDEO_STATE_PAUSED;
				FreeWheelImpl.VIDEO_STOPPED = FreeWheelImpl.SDK.VIDEO_STATE_STOPPED;
				FreeWheelImpl.VIDEO_COMPLETED = FreeWheelImpl.SDK.VIDEO_STATE_COMPLETED;

				FreeWheelImpl.initialized = true;
			}

			var networkId = this.getConfigProperty("adNetworkId");
			var serverUrl = this.getConfigProperty("adServerRootUrl") + "/ad/g/1";

			this._freewheel = new FreeWheelImpl.SDK.AdManager();
			this._freewheel.setNetwork(networkId);
			this._freewheel.setServer(serverUrl);
		}

    this._keyValueConfigEvaluator = new AdExt.AdKeyValueEvaluator(this.getConfigProperty('keyValues'));

		this._isListening = false;
		this._adProgress = 0;
		this._adTimer = null;
		this._adTimerDelay = 1000;
		this._ads = {};
		this._ads[FreeWheelImpl.PREROLL] = [];
		this._ads[FreeWheelImpl.MIDROLL] = [];
		this._ads[FreeWheelImpl.POSTROLL] = [];

		// bind event listeners
		this._onRequestComplete = Utils.bind(this._onRequestComplete, this);
		this._onSlotStarted = Utils.bind(this._onSlotStarted, this);
		this._onSlotEnded = Utils.bind(this._onSlotEnded, this);
		this._onPauseRequested = Utils.bind(this._onPauseRequested, this);
		this._onResumeRequested = Utils.bind(this._onResumeRequested, this);
		this._onAdStart = Utils.bind(this._onAdStart, this);
		this._onAdEnd = Utils.bind(this._onAdEnd, this);
		this._onAdClick = Utils.bind(this._onAdClick, this);

		// CVP-2583  Digital Ads requested we identify our platform.
		this.setAdKeyValue("_fw_cvp_player", "html5");
	},

	_createAdIncrementor : function (fwslot) {
		return Utils.bind(function (slot) {
			var playhead = slot.getPlayheadTime();

			// Ignore duplicate events or events in the past.
			// if (playhead <= this._adProgress) {
			// 	return;
			// }

			// Ignore duplicate events.
			if (playhead === this._adProgress) {
				return;
			}

			// Keep quiet if we've gone longer than we were supposed to.
			if (playhead > this.currentAdDuration) {
				return;
			}

			this._adProgress = playhead;
			this.onAdPlayhead(this._adProgress, this.currentAdDuration);
		}, this, fwslot);
	},

	_updateAdTimer : function (slot) {
		this._disposeAdTimer();
		this._adTimer = Timing.periodically(this._createAdIncrementor(slot), this._adTimerDelay);
		this._adTimer.start();
	},

	_disposeAdTimer : function () {
		if (this._adTimer !== null) {
			this._adTimer.stop();
			this._adTimer = null;
		}
	},

	setVideoDisplayBase : function(videoDisplayBase)
	{
		if (FreeWheelImpl.initialized)
		{
			this.displayBase = videoDisplayBase;
			this.enabled = true;
		}
	},

	_purgeLoadedAds : function ()
	{
		this._ads[FreeWheelImpl.PREROLL].length = 0;
		this._ads[FreeWheelImpl.MIDROLL].length = 0;
		this._ads[FreeWheelImpl.POSTROLL].length = 0;
	},

	_fireAdStopped : function () {
		log.log("_fireAdStopped");

		this.eAdStopped.dispatch();
	},

	_handleAdReset : function () {
		switch (this._state) {
			case State.CREATING_NEW_CONTEXT:
				this._createNewContext();
			break;
			case State.STOPPING_ADS:
				this._fireAdStopped();
			break;
			default:
				log.debug("_handleAdReset", "unknown or unexpected state '" + this._state + "'!");
		}

		this._state = State.READY;
	},

	_disposeCurrentContext : function () {
		log.log("_disposeCurrentContext");

		this._listen(false);

		this._fwContext.dispose();
		this._fwContext = null;

		this._handleAdReset();
	},

	_reset : function ()
	{
		log.log("_reset");

		// Kill ad progress observer, in case we're called mid-ad-play.
		this._disposeAdTimer();

		// Reset any previously loaded ads.
		this._purgeLoadedAds();

		// Reset expected ad type; if we're resetting, the first ad should be preroll.
		this.currentAdType = FreeWheelImpl.PREROLL;

		if (this._fwContext) {
			// If we're currently playing an ad, we must wait until slot ends before calling Context.dispose()!
			if (this._currentSlot) {
				this._currentSlot.stop();
			}
			else {
				this._disposeCurrentContext();
			}
		}
		else {
			this._handleAdReset();
		}
	},

	_createNewContext : function ()
	{
		log.log("_createNewContext");

		var catalogEntry = this._catalogEntry;

		if (catalogEntry.isAdSensitive()) {
			log.log("catalogEntry specified 'isAdSensitive'; skipping ad context creation.");
			this._adError(Messages.AD_SKIPPED_DUE_TO_SENSITIVITY);
			return;
		}

		var networkId = this.getConfigProperty("adNetworkId");
		var duration = catalogEntry.getTrt();
		var adId = (
			Utils.empty(this._adServerInfo.adVideoAssetId)
			? catalogEntry.getId()
			: ConfigUtils.stringReplace(this._adServerInfo.adVideoAssetId, catalogEntry, false)
		);

		// create new context
		this._fwContext = this._freewheel.newContext();
		this._fwContext.setProfile(networkId + ":turner_html5");
		this._fwContext.setVideoAsset(adId, duration);
		this._fwContext.setSiteSection(this._adSection);
		this._fwContext.registerVideoDisplayBase(this.displayBase);  // pass container element ID

		// gather ad key/values
		var configValues = this._keyValueConfigEvaluator.evaluate(catalogEntry);
		var userKeyValues = this.getAdKeyValues();

		var keyValues = Utils.extend({}, configValues, userKeyValues);
		var theValue;

		// set ad key/values
		for (var key in keyValues) {
			if (Utils.hasOwn(keyValues, key)) {
				theValue = keyValues[key];
				if (Utils.isNull(theValue)) {
					log.debug("Ignoring key '" + key + "' because value was null (ad blocker?)");
				}
				else {
					log.log("Setting key/value pair: " + key + " | " + theValue);
					this._fwContext.addKeyValue(key, String(theValue));
				}
			}
		}

		// attach event listeners
		this._listen(true);

		// `submitRequest(timeoutSeconds)` [default: 5]
		this._fwContext.submitRequest();  // will trigger onRequestComplete
	},

	_listen : function (bool)
	{
		var ctxt = this._fwContext;

		if (!ctxt || this._isListening === bool) return;

		this._isListening = bool;

		var fn = bool ? ctxt.addEventListener : ctxt.removeEventListener;

		fn.call(ctxt, FreeWheelImpl.SDK.EVENT_REQUEST_COMPLETE, this._onRequestComplete);
		fn.call(ctxt, FreeWheelImpl.SDK.EVENT_SLOT_STARTED, this._onSlotStarted);
		fn.call(ctxt, FreeWheelImpl.SDK.EVENT_SLOT_ENDED, this._onSlotEnded);
		fn.call(ctxt, FreeWheelImpl.SDK.EVENT_CONTENT_VIDEO_PAUSE_REQUESTED, this._onContentPauseRequested);
		fn.call(ctxt, FreeWheelImpl.SDK.EVENT_CONTENT_VIDEO_RESUME_REQUESTED, this._onContentResumeRequested);
		fn.call(ctxt, FreeWheelImpl.SDK.EVENT_AD_IMPRESSION, this._onAdStart);
		fn.call(ctxt, FreeWheelImpl.SDK.EVENT_AD_IMPRESSION_END, this._onAdEnd);
		fn.call(ctxt, FreeWheelImpl.SDK.EVENT_AD_CLICK, this._onAdClick);
		fn.call(ctxt, FreeWheelImpl.SDK.EVENT_ERROR, this._onAdError);
	},

	loadAds : function(catalogEntry)
	{
		log.log("loadAds");

		this._catalogEntry = catalogEntry;

		this._state = State.CREATING_NEW_CONTEXT;
		this._reset();
	},

	stop : function ()
	{
		log.log("stop");

		this._state = State.STOPPING_ADS;
		this._reset();
	},

	dispose : function ()
	{
		log.log("dispose");

		this.stop();

		this._freewheel = null;
	},

	// `_onRequestComplete(event)`
	//
	// event:
	//   - `type`: "onRequestComplete"
	//   - `target`: this._fwContext
	//   - `success`: true if successful
	//   - `response`: (undocumented)  use `target.getTemporalSlots()`
	_onRequestComplete : function (event) {
		log.log("_onRequestComplete", event);

		if (!(event && event.success)) {
			log.debug("_onRequestComplete", "No ad event or ad request was unsuccessful.");
		}
		else {
			var fwTemporalSlots = event.target.getTemporalSlots();
			for ( var i = 0, endi = fwTemporalSlots.length; i < endi; ++i ) {
				var slot = fwTemporalSlots[i];
				var adType = slot.getTimePositionClass();
				switch (adType) {
					case FreeWheelImpl.PREROLL:
					case FreeWheelImpl.MIDROLL:
					case FreeWheelImpl.POSTROLL:
						this._ads[adType].push(slot);
					break;
				}
			}
		}

		this._createMidrollsData();

		this.eAdReady.dispatch();

		this.playPreroll();
	},

	_createMidrollsData : function () {
		var midrollMap = {};
		var slots = this._ads[FreeWheelImpl.MIDROLL];

		Utils.each(slots, function (slot, index /*, list*/) {
			var time = slot.getTimePosition();
			if (time >= 0)
				midrollMap[time] = index;
		});

		this._midrolls = midrollMap;
	},

	getMidrolls : function () {
		return Utils.extend({}, this._midrolls);
	},

	_onSlotStarted : function (event) {
		log.log("_onSlotStarted", event.type, event.target, event.slot);
	},

	_playSlot : function (slot) {
		this._currentSlot = slot;
		this._currentSlot.play();
	},

	_continuePlayingSlots : function (adType) {
		var slots = this._ads[adType];
		if (slots && slots.length) {
			slots.shift();
			if (slots.length) {
				this._playSlot(slots[0]);
				return true;
			}
		}
		return false;
	},

	_onSlotEnded : function (event) {
		log.log("_onSlotEnded", event.type, event.target, event.slot);

		this._currentSlot = null;

		if (this._state !== State.READY) {
			// Either in process of creating new context or stopping ads.
			log.log("_onSlotEnded", "short-circuit because state is " + this._state);
			this._disposeCurrentContext();
			return;
		}

		var adType = event.slot.getTimePositionClass();

		switch (adType) {
			case FreeWheelImpl.PREROLL:
				if (!this._continuePlayingSlots(adType)) {
					this.onPrerollCompleted();
				}
			break;
			case FreeWheelImpl.MIDROLL:
				// this._ads[adType].shift();
				this.onMidrollCompleted();
			break;
			case FreeWheelImpl.POSTROLL:
				if (!this._continuePlayingSlots(adType)) {
					this.onPostrollCompleted();
				}
			break;
		}
	},

	_onContentPauseRequested : function (event) {
		log.log("_onContentPauseRequested", event.type, event.target, event.success);
	},

	_onContentResumeRequested : function (event) {
		log.log("_onContentResumeRequested", event.type, event.target, event.slot);
	},

	_onAdStart : function (event) {
		log.log("_onAdStart", event.type, event.target, event.adInstance);
	},

	_onAdEnd : function (event) {
		log.log("_onAdEnd", event.type, event.target, event.adInstance);
	},

	_onAdClick : function (event) {
		log.log("_onAdClick", event);

		var ad = event.adInstance;

		if (!ad) {
			return;
		}

		var callbackUrls = ad.getEventCallbackUrls(FreeWheelImpl.SDK.EVENT_AD_CLICK, FreeWheelImpl.SDK.EVENT_TYPE_CLICK);
		log.log("_onAdClick", 'callbackUrls:', callbackUrls);

		if (callbackUrls && callbackUrls.length) {
			this._emitAdClick({
				url : callbackUrls[0]
			});
		}
	},

	_onAdError : function (event) {
		log.debug("_onAdError", event);

		// {
		// 	"id": "",
		// 	"adType": "preroll",
		// 	"errors": []
		// }

		if (!Utils.isObject(event)) {
			log.warn("_onAdError did not receive an object; cannot report an error");
			return;
		}

		this._emitAdError({
			errorMessage : Messages.AD_EXISTS_BUT_FAILED,
			errors : event.errors
		});
	},

	playPreroll : function()
	{
		log.log("playPreroll");
		this.currentAdType = FreeWheelImpl.PREROLL;
		this._adPlay(FreeWheelImpl.PREROLL);
	},

	onPrerollCompleted : function()
	{
		log.log("onPrerollCompleted");
		this._adEnd();
	},

	playMidroll : function (index) {
		log.log("playMidroll");
		this.currentAdType = FreeWheelImpl.MIDROLL;
		this._adPlay(FreeWheelImpl.MIDROLL, index);
	},

	onMidrollCompleted : function () {
		log.log("onMidrollCompleted");
		this._adEnd();
	},

	playPostroll : function()
	{
		log.log("playPostroll");
		this.currentAdType = FreeWheelImpl.POSTROLL;
		this._adPlay(FreeWheelImpl.POSTROLL);
	},

	onPostrollCompleted : function()
	{
		log.log("onPostrollCompleted");
		this._adEnd();
	},

	_adPlay : function(adType, index)
	{
		index = index || 0;

		log.log("_adPlay", adType);

		var entry = this._catalogEntry;

		if (entry.isAdSensitive()) {
			log.debug("catalogEntry specified 'isAdSensitive'; skipping ad play.");
			this._adError(Messages.AD_SKIPPED_DUE_TO_SENSITIVITY);
			return;
		}

		var slots = this._ads[adType];

		if (!slots.length) {
			log.debug("no slots provided for the ad type: " + adType);
			this._adError(Messages.AD_TYPE_NOT_TRAFFICKED);
			return;
		}

		if (index < 0 || slots.length <= index) {
			log.debug("no slot at index '" + index + "' provided for the ad type: " + adType);
			this._adError(Messages.NO_AD_PLAYED_AT_AD_SPOT);
			return;
		}

		var slot = slots[index];

		if (!slot || slot.getAdCount() === 0 || slot.getTotalDuration() === 0) {
			log.debug("no ad provided for the slot for ad type: " + adType);
			this._adError(Messages.NO_AD_PLAYED_AT_AD_SPOT);
			return;
		}

		this._currentSlot = slot;
		this.currentAdId = slot.getCustomId();
		this.currentAdDuration = Number(slot.getTotalDuration());
		this._adProgress = 0;

		this._updateAdTimer(slot);

		this._emitAdPlay();

		this._playSlot(slot);
	},

	_adEnd : function()
	{
		log.log("_adEnd");

		this._emitAdEnd();

		this._cleanup();
	},

	_adError : function (msg)
	{
		log.debug("_adError", msg);

		this._emitAdError({
			errorMessage : msg
		});

		this._cleanup();
	},

	_cleanup : function ()
	{
		this._disposeAdTimer();
		this.currentAdId = '';
	},

	onAdPlayhead : function(playhead, duration)
	{
		this.eAdPlayhead.dispatch(playhead, duration);
	},

	_convertAdTypeFromImpl : function (implAdType) {
		switch (implAdType) {
			case FreeWheelImpl.PREROLL: return AdTypes.PREROLL;
			case FreeWheelImpl.MIDROLL: return AdTypes.MIDROLL;
			case FreeWheelImpl.POSTROLL: return AdTypes.POSTROLL;
		}
	},

	getCurrentAdType : function () {
		return this._convertAdTypeFromImpl(this.currentAdType);
	},

	_convertVideoStateToImpl : function (state)
	{
		switch (state) {
			case VideoStates.PLAYING: return FreeWheelImpl.VIDEO_PLAYING;
			case VideoStates.PAUSED: return FreeWheelImpl.VIDEO_PAUSED;
			case VideoStates.STOPPED: return FreeWheelImpl.VIDEO_STOPPED;
			case VideoStates.COMPLETED: return FreeWheelImpl.VIDEO_COMPLETED;
		}
	},

	setVideoState : function (state)
	{
		this._super(state);

		if (this._fwContext) {
			var fwState = this._convertVideoStateToImpl(state);
			if (fwState) {
				this._fwContext.setVideoState(fwState);
			}
		}
	},

	getAdAnalyticsData : function ()
	{
		var slotsInfo = {};

		if (!this._fwContext) return slotsInfo;

		var slots = this._fwContext.getTemporalSlots();

		Utils.each(slots, function (slot, index /*, list*/) {
			var slotKey = 'temporal' + '_' + index;
			slotsInfo[slotKey] = this._getDataForSlot(slot);
		}, this);

		return slotsInfo;
	},

	_getDataForSlot : function (slot)
	{
		return {
			slotId : slot.getCustomId(),
			adType : this._convertAdTypeFromImpl(slot.getTimePositionClass()),
			position : slot.getTimePosition(),
			category : slot.getType(),
			duration : slot.getTotalDuration(),
			adCount : slot.getAdCount(),
			adInstances : this._getAdAnalyticsForSlot(slot)
		};
	},

	getAdAnalyticsDataForCurrentSlot : function ()
	{
		if (this._currentSlot)
			return ({
				adInstances : this._getAdAnalyticsForSlot(this._currentSlot)
			});
	},

	_getAdAnalyticsForSlot : function (slot)
	{
		var adInstances = slot.getAdInstances();

		return Utils.map(adInstances, this._getDataForAdInstance, this);
	},

	_getDataForAdInstance : function (ad)
	{
		var adInfo = {
			adId : ad.getAdId()
		};

		var callbackUrls = ad.getEventCallbackUrls(FreeWheelImpl.SDK.EVENT_AD_CLICK, FreeWheelImpl.SDK.EVENT_TYPE_CLICK);

		adInfo.clickUrl = callbackUrls.length ? callbackUrls[0] : '';

		var rendition = ad.getActiveCreativeRendition();

		if (rendition && Utils.isFunc(rendition.getPrimaryCreativeRenditionAsset)) {
			var asset = rendition.getPrimaryCreativeRenditionAsset();

			if (asset) {
				adInfo.assetUrl = asset.getUrl();
				adInfo.contentType = asset.getContentType();
			}
		}

		return adInfo;
	}

});

FreeWheelImpl.PREROLL = null;
FreeWheelImpl.MIDROLL = null;
FreeWheelImpl.POSTROLL = null;


// Export public APIs
module.exports = FreeWheelImpl;

},{"../../cvp/errors":25,"../../cvp/log":28,"../../cvp/util/configutils":43,"../../cvp/util/timing":48,"../../cvp/utils":50,"../extensions/ad-kvps":70,"./adtypes":53,"./baseimpl":54,"./videostates":57}],57:[function(require,module,exports){
/* html5/ads/videostates.js */

module.exports = {
	PLAYING : "PLAYING",
	PAUSED : "PAUSED",
	STOPPED : "STOPPED",
	COMPLETED : "COMPLETED"
};

},{}],58:[function(require,module,exports){
/* html5/api.js */

// Load dependent modules
var Class = require('../vendor/Class');
var Event = require('../cvp/customevent');
var CVPManager = require('./manager');
var Utils = require('../cvp/utils');


// Externally available API
/**
 * A CVP wrapper for html5.
 * @name HTML5Player
 * @class A CVP wrapper for html5.
 * Here is where the player is created and its behaviors are defined.
 */
var HTML5Player = Class.extend({

	/**
	 * Initializes the player by registering its event
	 * listeners.
	 * @param {Object} options Additional optional parameters.
	 * @memberOf HTML5Player
	 */
	init : function(options)
	{
		this._manager = new CVPManager();

		this.ePlayerLoaded = new Event();
		this.ePlayerLoadError = new Event();
		this.ePlayerReady = new Event();
		this.eContentMetadata = new Event();
		this.eContentBegin = new Event();
		this.eContentPlay = new Event();
		this.eContentPause = new Event();
		this.eContentEnd = new Event();
		this.eContentComplete = new Event();
		this.eContentPlayhead = new Event();
		this.eContentBuffering = new Event();
		this.eContentQueue = new Event();
		this.eContentQueueAutoplay = new Event();
		this.eContentVolume = new Event();
		this.eContentError = new Event();
		this.eContentResize = new Event();
		this.eContentEntryLoad = new Event();
		this.eContentEntryLoadError = new Event();

		this.eAdPlayhead = new Event();
		this.eAdPlay = new Event();
		this.eAdEnd = new Event();
		this.eAdError = new Event();
		this.eAdSensitive = new Event();

		this.eTrackingAdStart = new Event();
		this.eTrackingAdComplete = new Event();
		this.eTrackingAdProgress = new Event();
		this.eTrackingAdCountdown = new Event();
		this.eTrackingAdClick = new Event();
		this.eTrackingContentPlay = new Event();
		this.eTrackingContentBegin = new Event();
		this.eTrackingContentProgress = new Event();
		this.eTrackingContentComplete = new Event();
		this.eTrackingContentReplay = new Event();
		this.eTrackingContentSeek = new Event();
		this.eTrackingContentSeekEnd = new Event();
		this.eTrackingFullscreen = new Event();
		this.eTrackingMuted = new Event();
		this.eTrackingPaused = new Event();

		this._isListening = false;
		this.listen(true);

		this._manager.load(options);
	},

	listen : function (bool)
	{
		if (this._isListening === bool) return;

		this._isListening = bool;

		var method = bool ? 'addListener' : 'removeListener';

		this._manager.ePlayerLoaded[method](this._onPlayerLoaded, this);
		this._manager.ePlayerLoadError[method](this._onPlayerLoadError, this);
		this._manager.ePlayerRendered[method](this._onPlayerRendered, this);
		this._manager.eContentMetadata[method](this._onContentMetadata, this);
		this._manager.eContentBegin[method](this._onContentBegin, this);
		this._manager.eContentPlay[method](this._onContentPlay, this);
		this._manager.eContentPause[method](this._onContentPause, this);
		this._manager.eContentEnd[method](this._onContentEnd, this);
		this._manager.eContentComplete[method](this._onContentComplete, this);
		this._manager.eContentPlayhead[method](this._onContentPlayhead, this);
		this._manager.eContentBuffering[method](this._onContentBuffering, this);
		this._manager.eContentQueue[method](this._onContentQueue, this);
		this._manager.eContentQueueAutoplay[method](this._onContentQueueAutoplay, this);
		this._manager.eContentVolume[method](this._onContentVolume, this);
		this._manager.eContentError[method](this._onContentError, this);
		this._manager.eContentResize[method](this._onContentResize, this);
		this._manager.eContentEntryLoad[method](this._onContentEntryLoad, this);
		this._manager.eContentEntryLoadError[method](this._onContentEntryLoadError, this);

		this._manager.eAdPlayhead[method](this._onAdPlayhead, this);
		this._manager.eAdPlay[method](this._onAdPlay, this);
		this._manager.eAdEnd[method](this._onAdEnd, this);
		this._manager.eAdError[method](this._onAdError, this);
		this._manager.eAdSensitive[method](this._onAdSensitive, this);

		this._manager.eTrackingAdStart[method](this._onTrackingAdStart, this);
		this._manager.eTrackingAdComplete[method](this._onTrackingAdComplete, this);
		this._manager.eTrackingAdProgress[method](this._onTrackingAdProgress, this);
		this._manager.eTrackingAdCountdown[method](this._onTrackingAdCountdown, this);
		this._manager.eTrackingAdClick[method](this._onTrackingAdClick, this);
		this._manager.eTrackingContentPlay[method](this._onTrackingContentPlay, this);
		this._manager.eTrackingContentBegin[method](this._onTrackingContentBegin, this);
		this._manager.eTrackingContentProgress[method](this._onTrackingContentProgress, this);
		this._manager.eTrackingContentComplete[method](this._onTrackingContentComplete, this);
		this._manager.eTrackingContentReplay[method](this._onTrackingContentReplay, this);
		this._manager.eTrackingContentSeek[method](this._onTrackingContentSeek, this);
		this._manager.eTrackingContentSeekEnd[method](this._onTrackingContentSeekEnd, this);
		this._manager.eTrackingFullscreen[method](this._onTrackingFullscreen, this);
		this._manager.eTrackingMuted[method](this._onTrackingMuted, this);
		this._manager.eTrackingPaused[method](this._onTrackingPaused, this);
	},

	/**
	 * Renders a player element to the page.
	 * @param {Object} containerElement The object to render.
	 * @memberOf HTML5Player
	 */
	render : function(containerElement)
	{
		this._manager.render(containerElement);
	},

	/**
	 * Removes a player element from the page.
	 * @memberOf HTML5Player
	 */
	remove : function()
	{
		this._manager.remove();
	},

	/**
	 * Disposes of the player, so it cannot be re-embeded.
	 */
	dispose : function ()
	{
		this.listen(false);
		this._manager.dispose();
		this._manager = null;
	},

	/**
	 * Get the default player
	 * Currently, this will always be the first parsed context element
	 * In the future, we (& Apple) may allow multiple players
	 * @returns {String} DOM ID of player
	 */
	getDefaultPlayer : function() {
		return this._manager.getCurrentPlayer();
	},

	/**
	 * Plays the content based on it's id.
	 * @param {String} id The content's unique string identifier.
	 * @param {Object} options Additional optional parameters.
	 * @memberOf HTML5Player
	 */
	play : function(id, options)
	{
		this._manager.playContentWithId(id, options);
	},

	playFromObject : function (object, options)
	{
		this._manager.playFromObject(object, options);
	},

	replay : function () {
		this._manager.replay();
	},

	playNextInQueue : function () {
		this._manager.playNextInQueue();
	},

	/**
	 * Pauses the current content.
	 * @memberOf HTML5Player
	 */
	pause : function()
	{
		this._manager.pause();
	},

	/**
	 * Resumes the current content from pause.
	 * @memberOf HTML5Player
	 */
	resume : function()
	{
		this._manager.resume();
	},

	/**
	 * Stops play and removes media from video player.
	 */
	stop : function ()
	{
		this._manager.stop();
	},

	/**
	 * Adds the content to the queue.
	 * @param {String} id The content's unique string identifier.
	 * @param {Object} options Additional optional parameters.
	 * @param {Number} index Location in queue [default is -1].
	 * @memberOf HTML5Player
	 */
	queue : function(id, options, index)
	{
		this._manager.queueContentWithId(id, options, index);
	},

	queueFromObject : function (object, options, index)
	{
		this._manager.queueFromObject(object, options, index);
	},

	/**
	 * Removes the content from the queue.
	 * @param {String} id The content's unique string identifier.
	 * @memberOf HTML5Player
	 */
	dequeue : function(id)
	{
		this._manager.dequeueContentWithId(id);
	},

	/**
	 * Clear content queue.
	 */
	emptyQueue : function ()
	{
		this._manager.emptyQueue();
	},

	/**
	 * Enable/disable auto-play of content queue.
	 * @param {Boolean} autoplay - enable/disable
	 */
	setQueueAutoplay : function (autoplay)
	{
		this._manager.setQueueAutoplay(autoplay);
	},

	/**
	 * Return content queue.
	 * @returns {Array} content queue
	 */
	getQueue : function ()
	{
		return this._manager.getQueue();
	},

	/**
	 * Relocate the playhead.
	 * @memberOf HTML5Player
	 * @param {Number} seconds - Playhead location to seek to
	 */
	seek : function(seconds)
	{
		this._manager.seek(seconds);
	},

	/**
	 * Resizes the player.
	 * @memberOf HTML5Player
	 * @param {Number} width of player
	 * @param {Number} height of player
	 */
	resize : function(width, height)
	{
		this._manager.resize(width, height);
	},

	/**
	 * Toggles the player to fullscreen.
	 * @memberOf HTML5Player
	 */
	goFullscreen : function()
	{
		this._manager.goFullscreen();
	},

	/**
	 * Set the current content's volume.
	 * @param {float} v The value to set the volume to.
	 * @memberOf HTML5Player
	 */
	setVolume : function(v)
	{
		this._manager.setVolume(v);
	},

	/**
	 * Returns the current content's volume.
	 * @returns {Number} The current volume as float.
	 * @memberOf HTML5Player
	 */
	getVolume : function()
	{
		return this._manager.getVolume();
	},

	/**
	 * Mutes the current content's volume.
	 * @memberOf HTML5Player
	 */
	mute : function()
	{
		this._manager.mute();
	},

	/**
	 * Un-mutes the current content's volume.
	 * @memberOf HTML5Player
	 */
	unmute : function()
	{
		this._manager.unmute();
	},

	/**
	 * Reports if video player is mute.
	 * @returns {boolean} Whether the player is mute.
	 * @memberOf HTML5Player
	 */
	isMuted : function ()
	{
		return this._manager.isMuted();
	},

	/**
	 * Retrieve's the current content entry based on the
	 * provided id.
	 * @param {String} id The content's unique identifier.
	 * @returns {Object} The content's entry.
	 * @memberOf HTML5Player
	 */
	getContentEntry : function (id)
	{
		return this._manager.getContentEntry(id);
	},

	/**
	 * Set ad section (site section) of AdManager.
	 * @param {String} ssid - site section ID
	 * @memberOf HTML5Player
	 */
	setAdSection : function (ssid)
	{
		this._manager.setAdSection(ssid);
	},

	setAdKeyValue : function (key, value)
	{
		this._manager.setAdKeyValue(key, value);
	},

	getAdKeyValues : function ()
	{
		return this._manager.getAdKeyValues();
	},

	clearAdKeyValues : function ()
	{
		this._manager.clearAdKeyValues();
	},

	setTrackingInterval : function (interval)
	{
		this._manager.setTrackingInterval(interval);
	},

	setDataSrc : function (src)
	{
		this._manager.setDataSrc(src);
	},

	switchAdContext : function (key, value)
	{
		this._manager.switchAdContext(key, value);
	},

	switchTrackingContext : function (key, value)
	{
		this._manager.switchTrackingContext(key, value);
	},

	reportAnalytics : function (eventName, data)
	{
		this._manager.reportAnalytics(eventName, data);
	},

	_mungeArgs : function (argv)
	{
		var args = Utils.slice(argv);
		args.unshift(this.getDefaultPlayer());
		return args;
	},

	_onPlayerLoaded : function()
	{
		this.ePlayerLoaded.dispatch();
	},

	_onPlayerLoadError : function()
	{
		this.ePlayerLoadError.dispatch();
	},

	_onPlayerRendered : function()
	{
		this.ePlayerReady.dispatch.apply(this.ePlayerReady, this._mungeArgs(arguments));
	},

	_onContentMetadata : function()
	{
		this.eContentMetadata.dispatch.apply(this.eContentMetadata, this._mungeArgs(arguments));
	},

	_onContentBegin : function()
	{
		this.eContentBegin.dispatch.apply(this.eContentBegin, this._mungeArgs(arguments));
	},

	_onContentPlay : function()
	{
		this.eContentPlay.dispatch.apply(this.eContentPlay, this._mungeArgs(arguments));
	},

	_onContentPause : function()
	{
		this.eContentPause.dispatch.apply(this.eContentPause, this._mungeArgs(arguments));
	},

	_onContentEnd : function()
	{
		this.eContentEnd.dispatch.apply(this.eContentEnd, this._mungeArgs(arguments));
	},

	_onContentComplete : function()
	{
		this.eContentComplete.dispatch.apply(this.eContentComplete, this._mungeArgs(arguments));
	},

	_onContentPlayhead : function()
	{
		this.eContentPlayhead.dispatch.apply(this.eContentPlayhead, this._mungeArgs(arguments));
	},

	_onContentBuffering : function()
	{
		this.eContentBuffering.dispatch.apply(this.eContentBuffering, this._mungeArgs(arguments));
	},

	_onContentQueue : function()
	{
		this.eContentQueue.dispatch.apply(this.eContentQueue, this._mungeArgs(arguments));
	},

	_onContentQueueAutoplay : function()
	{
		this.eContentQueueAutoplay.dispatch.apply(this.eContentQueueAutoplay, this._mungeArgs(arguments));
	},

	_onContentVolume : function()
	{
		this.eContentVolume.dispatch.apply(this.eContentVolume, this._mungeArgs(arguments));
	},

	_onContentError : function()
	{
		this.eContentError.dispatch.apply(this.eContentError, this._mungeArgs(arguments));
	},

	_onContentResize : function()
	{
		this.eContentResize.dispatch.apply(this.eContentResize, this._mungeArgs(arguments));
	},

	_onContentEntryLoad : function()
	{
		this.eContentEntryLoad.dispatch.apply(this.eContentEntryLoad, this._mungeArgs(arguments));
	},

	_onContentEntryLoadError : function()
	{
		this.eContentEntryLoadError.dispatch.apply(this.eContentEntryLoadError, this._mungeArgs(arguments));
	},

	_onAdPlayhead : function()
	{
		this.eAdPlayhead.dispatch.apply(this.eAdPlayhead, this._mungeArgs(arguments));
	},

	_onAdPlay : function()
	{
		this.eAdPlay.dispatch.apply(this.eAdPlay, this._mungeArgs(arguments));
	},

	_onAdEnd : function()
	{
		this.eAdEnd.dispatch.apply(this.eAdEnd, this._mungeArgs(arguments));
	},

	_onAdError : function()
	{
		this.eAdError.dispatch.apply(this.eAdError, this._mungeArgs(arguments));
	},

	_onAdSensitive : function()
	{
		this.eAdSensitive.dispatch.apply(this.eAdSensitive, this._mungeArgs(arguments));
	},

	_onTrackingAdStart : function()
	{
		this.eTrackingAdStart.dispatch.apply(this.eTrackingAdStart, this._mungeArgs(arguments));
	},

	_onTrackingAdComplete : function()
	{
		this.eTrackingAdComplete.dispatch.apply(this.eTrackingAdComplete, this._mungeArgs(arguments));
	},

	_onTrackingAdProgress : function()
	{
		this.eTrackingAdProgress.dispatch.apply(this.eTrackingAdProgress, this._mungeArgs(arguments));
	},

	_onTrackingAdCountdown : function()
	{
		this.eTrackingAdCountdown.dispatch.apply(this.eTrackingAdCountdown, this._mungeArgs(arguments));
	},

	_onTrackingAdClick : function()
	{
		this.eTrackingAdClick.dispatch.apply(this.eTrackingAdClick, this._mungeArgs(arguments));
	},

	_onTrackingContentPlay : function()
	{
		this.eTrackingContentPlay.dispatch.apply(this.eTrackingContentPlay, this._mungeArgs(arguments));
	},

	_onTrackingContentBegin : function()
	{
		this.eTrackingContentBegin.dispatch.apply(this.eTrackingContentBegin, this._mungeArgs(arguments));
	},

	_onTrackingContentProgress : function()
	{
		this.eTrackingContentProgress.dispatch.apply(this.eTrackingContentProgress, this._mungeArgs(arguments));
	},

	_onTrackingContentComplete : function()
	{
		this.eTrackingContentComplete.dispatch.apply(this.eTrackingContentComplete, this._mungeArgs(arguments));
	},

	_onTrackingContentReplay : function()
	{
		this.eTrackingContentReplay.dispatch.apply(this.eTrackingContentReplay, this._mungeArgs(arguments));
	},

	_onTrackingContentSeek : function()
	{
		this.eTrackingContentSeek.dispatch.apply(this.eTrackingContentSeek, this._mungeArgs(arguments));
	},

	_onTrackingContentSeekEnd : function()
	{
		this.eTrackingContentSeekEnd.dispatch.apply(this.eTrackingContentSeekEnd, this._mungeArgs(arguments));
	},

	_onTrackingFullscreen : function()
	{
		this.eTrackingFullscreen.dispatch.apply(this.eTrackingFullscreen, this._mungeArgs(arguments));
	},

	_onTrackingMuted : function()
	{
		this.eTrackingMuted.dispatch.apply(this.eTrackingMuted, this._mungeArgs(arguments));
	},

	_onTrackingPaused : function()
	{
		this.eTrackingPaused.dispatch.apply(this.eTrackingPaused, this._mungeArgs(arguments));
	}

});


// Export public APIs
module.exports = HTML5Player;

},{"../cvp/customevent":16,"../cvp/utils":50,"../vendor/Class":93,"./manager":72}],59:[function(require,module,exports){
/* html5/aspencontroller.js */

// Load dependent modules
var Aspen = require('../aspen/aspen');
var Event = require('../cvp/customevent');
var Log = require('../cvp/log');


// We're allowing the AspenController to be init'ed once,
// assuming that you will not have multiple players on a
// page with different 'site' configurations.  [ASSUME]

var inited = false;
var ready = false;
var log = Log.getLogger().log;

function send(type, data) {
	log('AspenController - send', arguments);

	Aspen.send(type, data);
}

function setup() {
	log('AspenController - setup');

	Aspen.eInitSuccess.removeListener(setup, this);

	ready = true;

	log('AspenController - setup', 'dispatching init success event');
	this.eInitSuccess.dispatch();
}

function init(options) {
	log('AspenController - init', arguments);

	if (inited)
		// disallow subsequent init()s
		return;

	inited = true;

	Aspen.eInitSuccess.addListener(setup, this);
	Aspen.init(options);
}

function isReady() {
	return ready;
}

function getConfig(config) {
	if (!inited) {
		throw new Error('AspenController - getConfig cannot be called until after AspenController has successfully inited!');
	}

	if (!ready) {
		throw new Error('AspenController - getConfig cannot be called until after Aspen has successfully inited!');
	}

	log('AspenController - getConfig', config);
	return Aspen.getConfig(config);
}


// Export public APIs
module.exports = {
	videoCount : 0,
	eInitSuccess : new Event(),
	isReady : isReady,
	getConfig : getConfig,
	send : send,
	init : init
};

},{"../aspen/aspen":1,"../cvp/customevent":16,"../cvp/log":28}],60:[function(require,module,exports){
/* html5/bootstrapper.js */

// Load dependent modules
var Class = require('../vendor/Class');
var Event = require('../cvp/customevent');
var DependencyManager = require('../cvp/util/dependencymanager');
var MappingDependency = require('./dependencies/mapping');
var AppConfigDependency = require('./dependencies/appconfig');
var ContainerDependency = require('./dependencies/container');
var ConfigDependency = require('./dependencies/config');
var AdPolicyDependency = require('./dependencies/adpolicy');
var TrackingPolicyDependency = require('./dependencies/trackingpolicy');
var Utils = require('../cvp/utils');
var PlayerConfig = require('./shared/playerconfig');
var OverwriteManager = require('./overwritemanager');
var Log = require('../cvp/log');

var logger = Log.getLogger('BootStrapper');


var BootStrapManager = DependencyManager.extend({

	setBootstrapper : function (bootstrap) {
		this._bootstrap = bootstrap;
	},

	clearBootstrapper : function () {
		this._bootstrap = null;
	},

	setParams : function (params) {
		this._params = params;
	},

	getParams : function () {
		return this._params;
	}

});


var BootStrapper = Class.extend({

	init : function (mappingUrl, params) {
		this._mappingUrl = mappingUrl;
		this._params = params || {};

		this.eSuccess = new Event();
		this.eFailure = new Event();
	},

	load : function () {

		this._bootManager = new BootStrapManager();
		this._bootManager.setBootstrapper(this);
		this._bootManager.setParams(this._params);

		var mappingDependency = new MappingDependency(this._mappingUrl, true);
		mappingDependency.setContext(this._params.profile);

		this._bootManager.addDependency(mappingDependency);

		this._bootManager.eDependencySuccess.addListener(this._onDependencySuccess, this);
		this._bootManager.eSuccess.addListener(this._success, this);
		this._bootManager.eFailure.addListener(this._failure, this);
		this._bootManager.load();
	},

	_onDependencySuccess : function (dependency, result) {

		if (dependency instanceof MappingDependency) {
			this._mapping = result;
		} else if (dependency instanceof AppConfigDependency) {
			this._appConfig = result;
		} else if (dependency instanceof ContainerDependency) {
			this._containerInfo = result;
		} else if (dependency instanceof ConfigDependency) {
			this._configInfo = result;
		} else if (dependency instanceof AdPolicyDependency) {
			this._adPolicy = result;
		} else if (dependency instanceof TrackingPolicyDependency) {
			this._trackingPolicy = result;
		}

	},

	/**
	 * At this point, the following should true:
	 * - the container is loaded and parsed
	 * - the config is loaded and parsed
	 * - any other initially required dependencies are loaded
	 *     i.e., FW, Omniture, UI-related tools, etc..
	 */
	_success : function () {

		var data = {};

		data.mapping = Utils.extend({}, PlayerConfig.mapping, this._mapping);
		data.appConfig = Utils.extend({}, PlayerConfig.appConfig, this._appConfig);
		data.containerInfo = Utils.extend({}, PlayerConfig.containerInfo, this._containerInfo);
		data.configInfo = Utils.extend({}, PlayerConfig.configInfo, this._configInfo);
		data.adPolicy = Utils.extend({}, PlayerConfig.adPolicy, this._adPolicy);
		data.trackingPolicy = Utils.extend({}, PlayerConfig.trackingPolicy, this._trackingPolicy);

		new OverwriteManager(this._params.flashVars).overwrite(data);

		logger.debug("overwritten data: ", data);

		// Send out the parsed configuration data
		var event = {
			data : data
		};

		this.eSuccess.dispatch(event);
		this._complete();
	},

	_failure : function() {
		this.eFailure.dispatch();
		this._complete();
	},

	_complete : function() {
		if (this._bootManager) {
			this._bootManager.eDependencySuccess.removeListener(this._onDependencySuccess, this);
			this._bootManager.eSuccess.removeListener(this._success, this);
			this._bootManager.eFailure.removeListener(this._failure, this);
			this._bootManager.clearBootstrapper();
			this._bootManager = null;
		}
	}

});


// Export public APIs
module.exports = BootStrapper;

},{"../cvp/customevent":16,"../cvp/log":28,"../cvp/util/dependencymanager":45,"../cvp/utils":50,"../vendor/Class":93,"./dependencies/adpolicy":63,"./dependencies/appconfig":64,"./dependencies/config":65,"./dependencies/container":66,"./dependencies/mapping":67,"./dependencies/trackingpolicy":68,"./overwritemanager":73,"./shared/playerconfig":90}],61:[function(require,module,exports){
/* html5/cms.js */

// Load dependent modules
var App = require('../core/app');
var Class = require('../vendor/Class');
var Event = require('../cvp/customevent');
var ContentCatalogEntry = require('./contententry');
var FakeCatalogEntry = require('./fakecontententry');
var ExpiredCatalogEntry = require('./expiredcontententry');
var Request = require('../cvp/request');
var Utils = require('../cvp/utils');


var CMS = Class.extend({

	init : function() {
		this._catalogDataURL = "";
		this._mediaUrl = "";

		this._requestPendingQueue = [];
		this._requestInProgress = false;
		this._videoCatalog = [];
		this._objectMap = {};

		this._onContentIdRequestComplete = Utils.bind(this._onContentIdRequestComplete, this);
		this._onContentIdRequestIOError = Utils.bind(this._onContentIdRequestIOError, this);
		this._requestCleanup = Utils.bind(this._requestCleanup, this);

		this.eRequestCompleted = new Event("CmsRequestCompletedEvent");
	},

	addContentId : function(contentId) {
		var entry = this.getContentId(contentId);

		if (Utils.isNull(entry))
			this._requestPendingQueue.push(contentId);
		else {
			var index = contentId.split("|")[1];
			contentId = contentId.split("|")[0];
			this._notifyListeners(contentId, contentId, index, "", "");
		}

		this._processNextRequest();
	},

	addContentObject : function (key, object) {
		var keyParts = key.split("|");
		var contentId = keyParts[0];
		// var index = keyParts[1];
		var entry = new FakeCatalogEntry(object);
		var requestIndex = this._videoCatalog.length;

		if (!entry || !entry.hasFiles())
		{
			this._notifyListeners(contentId, contentId, requestIndex, "Invalid data", App.ERROR_CMS_PARSE);
		}
		else
		{
			this._objectMap[key] = entry;
			this._requestPendingQueue.push(key);
		}

		this._processNextRequest();
	},

	getContentId : function(contentId) {
		contentId = contentId.split("|")[0];

		for (var entry, i = 0, ct = this._videoCatalog.length; i < ct; ++i)
		{
			entry = this._videoCatalog[i];

			if (entry.getId() === contentId)
				return entry;
		}

		return null;
	},

	setDataUrl : function(url) {
		this._catalogDataURL = url;
	},

	setMediaUrl : function(url) {
		this._mediaUrl = url;
	},

	_getRequestUrl : function(contentId) {
		var requestUrl;

		var start = this._catalogDataURL.substr(0).search(/\$\{/);
		if (start !== -1)
		{
			var end = this._catalogDataURL.substr(start).search(/\}/);

			if (end !== -1)
			{
				var pattern = this._catalogDataURL.substr(start, end + 1);
				requestUrl = this._catalogDataURL.replace(pattern, contentId);
			}
		}
		else
		{
			requestUrl = this._catalogDataURL + "/" + contentId + ".xml";
		}

		return requestUrl;
	},

	_createContentEntryFromRequestData : function(requestData, contentId) {
		try
		{
			var entry = new ContentCatalogEntry(requestData);
			entry.setMediaUrl(this._mediaUrl);
			return entry;
		}
		catch(e)
		{
			alert(this._getRequestUrl(contentId) + ": " + e.message);
		}

		return null;
	},

	_processNextRequest : function() {
		if (!this._requestInProgress && this._requestPendingQueue.length)
		{
			var nextId = this._requestPendingQueue[0];
			if (nextId in this._objectMap) {
				this._processVideoFromObject(nextId);
			}
			else {
				var contentId = nextId.split("|")[0];
				this._requestVideoWithId(contentId);
			}
		}
	},

	_requestVideoWithId : function(contentId) {
		var url = this._getRequestUrl(contentId);
		var req = Request.getXML({
			url : url
		});

		this._requestInProgress = true;

		req
			.then(this._onContentIdRequestComplete, this._onContentIdRequestIOError)
			.ensure(this._requestCleanup);
	},

	_requestCleanup : function () {
		this._requestInProgress = false;
		this._requestPendingQueue.shift();
		this._processNextRequest();
	},

	_onContentIdRequestComplete : function(data) {
		var request = this._requestPendingQueue[0].split("|");
		var contentId = request[0];
		var requestIndex = request[1];

		if (!data)
		{
			this._notifyListeners(contentId, contentId, requestIndex, "No data", App.ERROR_CMS_PARSE);
			return;
		}

		var entry = this._createContentEntryFromRequestData(data, contentId);

		if (!entry || !entry.hasFiles())
		{
			this._notifyListeners(contentId, contentId, requestIndex, "Invalid data", App.ERROR_CMS_PARSE);
			return;
		}

		if (entry.isExpired())
		{
			// Obliterate downloaded content XML; replace with "expired XML" payload.
			entry = new ExpiredCatalogEntry();
		}

		entry.requestId = contentId;

		this._videoCatalog.push(entry);
		this._notifyListeners(entry.getId(), entry.requestId, requestIndex, "", "");
	},

	_onContentIdRequestIOError : function(error) {
		var request = this._requestPendingQueue[0].split("|");
		var contentId = request[0];
		var requestIndex = request[1];

		this._notifyListeners(contentId, contentId, requestIndex, "IOError : " + error, App.ERROR_CMS_IO);
	},

	_processVideoFromObject : function (key) {
		var entry = this._objectMap[key];
		var keyParts = key.split("|");
		var contentId = keyParts[0];
		var requestIndex = keyParts[1];

		entry.requestId = contentId;
		entry.setMediaUrl(this._mediaUrl);

		this._videoCatalog.push(entry);
		this._notifyListeners(contentId, entry.requestId, requestIndex, "", "");
		this._requestCleanup();
	},

	_notifyListeners : function(contentId, requestId, index, errorMsg, errorCode) {
		this.eRequestCompleted.dispatch(contentId, requestId, index, errorMsg, errorCode);
	}

});


// Export public APIs
module.exports = CMS;

},{"../core/app":8,"../cvp/customevent":16,"../cvp/request":36,"../cvp/utils":50,"../vendor/Class":93,"./contententry":62,"./expiredcontententry":69,"./fakecontententry":71}],62:[function(require,module,exports){
/* html5/contententry.js */

// Load dependent modules
var Class = require('../vendor/Class');
var Utils = require('../cvp/utils');
var XMLUtils = require('../cvp/util/xml');
var Log = require('../cvp/log');


var log = Log.getLogger();
var unsupportedExtensions = [ 'flv' ];
var platformSplitter = /[,\s]+/;

var ContentCatalogEntry = Class.extend({

	init : function (xml) {
		var files, file, fKey, fFallback, fValue, i, endi, ext,
			images, image, iWidth, iHeight, iKey, iValue, lastDot,
			ads, ad, adKey, adValue, urlParts;

		this._xmlEntry = xml.documentElement;
		this._requestId = null;
/*
		this._protectedContent = false;
		this._protectedFiles = [];
*/

		this._mediaUrl = "";
		this._name = XMLUtils.getNodeValue(this._xmlEntry, "slug");
		this._title = XMLUtils.getNodeValue(this._xmlEntry, "headline");
		this._category = XMLUtils.getNodeValue(this._xmlEntry, "category");
		this._trt = +XMLUtils.getNodeValue(this._xmlEntry, "trt");
		this._captionsList = [];

		files = this._xmlEntry.getElementsByTagName("files");

		if (files.length)
		{
			files = XMLUtils.getChildElements(files[0]);

			this._fileList = {};

			for (i = 0, endi = files.length; i < endi; ++i)
			{
				file = files[i];

				if (file.nodeName !== 'file')
					continue;

				fKey = XMLUtils.getAttribute(file, "key") || XMLUtils.getAttribute(file, "bitrate");
				fFallback = XMLUtils.getAttribute(file, "fallback");
				fValue = XMLUtils.getNodeValue(file);

				if (!fValue) {
					log.info("Ignoring file key/bitrate '" + fKey + "' because it has no text() -- no URL!");
					continue;
				}

				// Pull extension from url/path provided by fValue.
				ext = "";
				urlParts = Utils.parseUrl(this.sanitizeUrl(fValue));
				if (urlParts) {
					lastDot = urlParts.file.lastIndexOf(".");
					if (lastDot !== -1) {
						ext = urlParts.file.substring(lastDot + 1);
					}
				}

				log.debug("ContentCatalogEntry", fKey, fValue, ext);

				if (Utils.indexOf(unsupportedExtensions, ext) !== -1) {
					log.info("Ignoring " + fValue + " because extention '" + ext + "' is unsupported.");
					continue;
				}

				if (fKey in this._fileList) {
					var previousUrl = this._fileList[fKey].url;
					log.warn("The file key/bitrate '" + fKey + "' already exists with URL '" + previousUrl + "' and is now being overwritten with " + (previousUrl === fValue ? "the same URL" : "URL '" + fValue + "'") + ".");
				}

				this._fileList[fKey] = {
					url : fValue,
					fallback : fFallback,
					ext : ext
				};

/*
				// Check for authType on the XML Node for protected content
				var fAuthType = XMLUtils.getAttribute(file, "authType");
				if(fAuthType){
					this._protectedContent = true;
					var obj = this._fileList[fKey];
					obj['authType'] = fAuthType;
					this._protectedFiles.push(obj);
				}
*/
			}
		}
		else
		{
			log.warn("ContentCatalogEntry", "No file entries found!");
		}

		images = this._xmlEntry.getElementsByTagName("images");

		if (images.length)
		{
			images = XMLUtils.getChildElements(images[0]);

			this._imageList = {};

			for (i = 0, endi = images.length; i < endi; ++i)
			{
				image = images[i];

				iWidth = XMLUtils.getAttribute(image, "width");
				iHeight = XMLUtils.getAttribute(image, "height");
				iValue = XMLUtils.getNodeValue(image);

				log.debug("ContentCatalogEntry", "images", iWidth, iHeight, iValue);

				iKey = iWidth + "|" + iHeight;
				this._imageList[iKey] = {
					url : iValue,
					width : iWidth,
					height : iHeight
				};
			}
		}

		ads = this._xmlEntry.getElementsByTagName("ads");

		if (ads.length)
		{
			ads = XMLUtils.getChildElements(ads[0]);

			this._adList = {};

			for (i = 0, endi = ads.length; i < endi; ++i)
			{
				ad = ads[i];

				adKey = XMLUtils.getAttribute(ad, "type");
				adValue = XMLUtils.getNodeValue(ad);

				log.debug("ContentCatalogEntry", "ad", adKey, adValue);

				this._adList[adKey] = {
					url : adValue
				};
			}
		}

		var captions = this._xmlEntry.getElementsByTagName("closedCaptions")[0] || null;

		if (captions) {
			var captionsEnabled = Utils.isFlagActive(XMLUtils.getAttribute(captions, "enabled"), false);

			if (captionsEnabled) {

				// grab source nodes that...
				// - @type = "external"
				// - @format = "webvtt"
				// - @platform.split(platformSplitter).indexOf('html5') !== -1
				var sources = Utils.filter(Utils.slice(captions.getElementsByTagName("source")), function (source) {
					var type = XMLUtils.getAttribute(source, "type") || "";
					var format = XMLUtils.getAttribute(source, "format") || "";
					var platforms = (XMLUtils.getAttribute(source, "platform") || "").split(platformSplitter);

					return type === "external" && format === "webvtt" && platforms.indexOf("html5") !== -1;
				});

				// ...and from those source nodes, pull track nodes
				Utils.each(sources, function (source) {
					this._captionsList.push.apply(this._captionsList, Utils.map(Utils.slice(source.getElementsByTagName("track")), function (track) {
						return ({
							lang: XMLUtils.getAttribute(track, "lang") || "",
							label: XMLUtils.getAttribute(track, "label") || "",
							channel: XMLUtils.getAttribute(track, "channel") || "",
							url: XMLUtils.getAttribute(track, "url") || ""
						});
					}));
				}, this);

				log.debug("ContentCatalogEntry", "captions", this._captionsList);
			}
		}

	},

	hasFiles : function() {
		return !Utils.empty(this._fileList);
	},

	setMediaUrl : function(url) {
		this._mediaUrl = url;
	},

	getMediaUrl : function()
	{
		return this._mediaUrl;
	},

	getId : function()
	{
		var attr = XMLUtils.getAttribute(this._xmlEntry, "id");

		if (!Utils.empty(attr))
			return attr;

		return this._requestId;
	},

	getName : function()
	{
		return this._name;
	},

	getTitle : function()
	{
		return this._title;
	},

	getCategory : function()
	{
		return this._category;
	},

	getTrt : function()
	{
		return this._trt;
	},

	isExpired : function ()
	{
		var value = XMLUtils.getNodeValue(this._xmlEntry, "isExpired");
		return Utils.isFlagActive(value, false);
	},

	isAdSensitive : function()
	{
		var value = XMLUtils.getNodeValue(this._xmlEntry, "isAdSensitive");
		return Utils.isFlagActive(value, false);
	},

	getSource : function ()
	{
		var src = XMLUtils.getAttribute(this._xmlEntry, "source");
		return src || undefined;
	},

	getOriginId : function ()
	{
		var originId = XMLUtils.getAttribute(this._xmlEntry, "originId");
		return originId || undefined;
	},

	getMode : function ()
	{
		var isLive = XMLUtils.getAttribute(this._xmlEntry, "isLive");
		return !Utils.empty(isLive) ? "live" : "vod";
	},

	isLive : function ()
	{
		return this.getMode() === "live";
	},

	sanitizeUrl : function (url)
	{
		if (!url)
			return url;

		if (Utils.isRelative(url))
			url = this._mediaUrl + url;

		// There was a token call, append the token call to the url
		if (this.token)
			Utils.augmentQueryString(url, {
				hdnea : this.token
			});

		return url;
	},

	/**
	 * Shallow'ish file list clone
	 * Allows the page to interact with the file list
	 * without altering the data.
	 * @returns {Object} File list
	 */
	getAllFiles : function () {
		var fileList = {};

		Utils.each(this._fileList, function (key, file) {
			fileList[key] = Utils.extend({}, file);
		});
		return fileList;
	},

	getContentUrl : function (quality)
	{
		var url = null,
			dbgFound = false,
			bitrate;

		for (bitrate in this._fileList)
		{
			if (bitrate === quality)
			{
				url = this.sanitizeUrl(this._fileList[bitrate].url);
				dbgFound = true;
				break;
			}
		}

		if (!dbgFound)
			log.warn("Config XML specifies a '" + quality + "' bitrate entry, but there is no <file> node in video XML where bitrate attribute is '" + quality + "'.");
		else if (Utils.empty(url))
			log.warn("text attribute is empty in the video XML for the <file> node whose quality is '" + quality + "'.");

		return url;
	},

	getContentUrlsForExtension : function (ext) {
		var results = [];
		Utils.each(this._fileList, function (key, file) {
			if (file.ext === ext)
				results.push(this.sanitizeUrl(file.url));
		}, this);
		return results;
	},

	getContentUrlsByExtensions : function (extensions) {
		var results = [];
		Utils.each(extensions, function (ext) {
			var urls = this.getContentUrlsForExtension(ext);
			results.push.apply(results, urls);
		}, this);
		return results;
	},

	getContentUrlsByFileKeys : function (filekeys) {
		return Utils.map(filekeys, this.getContentUrl, this);
	},

	getContentUrlFromFallback : function(fallback) {
		var url = null,
			file = null,
			bitrate = '';

		for (bitrate in this._fileList) {
			file = this._fileList[bitrate];
			if (!(Utils.empty(file.fallback)) && fallback === file.fallback) {
				url = this.sanitizeUrl(file.url);
				break;
			}
		}

		return url;
	},

	getContentUrlFromType : function(ext)
	{
		var url = null,
			file = null,
			bitrate;

		for (bitrate in this._fileList)
		{
			file = this._fileList[bitrate];
			if (ext === file.ext)
			{
				url = this.sanitizeUrl(file.url);
				break;
			}
		}
		return url;
	},

	getThumbnailUrl : function(width, height)
	{
		var url = null,
			key = width + "|" + height,
			k;

		for (k in this._imageList)
		{
			if (k === key)
			{
				url = this._imageList[k].url;
				break;
			}
		}
		return url;
	},

	getImageForDimensions : function (width, height)
	{
		log.log("getImageForDimensions", width, height);

		if (Utils.isEmpty(this._imageList)) {
			log.log("getImageForDimensions", "No images!");
			return null;
		}

		var ratio = width / height;
		// var devicePixelRatio = Number(window.devicePixelRatio) || 1;

		// if (devicePixelRatio > 1) {
		// 	width = Math.round(devicePixelRatio * width);
		// 	height = Math.round(devicePixelRatio * height);
		// 	log.log("getImageForDimensions", "Searching for higher-resolution (" + width + "x" + height + ") image ...");
		// }

		var exactImage = this.getThumbnailUrl(width, height);

		if (exactImage) {
			// if there's an image that matches the requested dimensions exactly...
			log.log("getImageForDimensions", "Found exact match!");
			return exactImage;
		}

		// ...otherwise, we go through this...

		function cloneImage(image) {
			var obj = {};
			obj.url = image.url;
			obj.width = Number(image.width) || 1;
			obj.height = Number(image.height) || 1;
			obj.ratio = obj.width / obj.height;
			obj.absDeltaWidth = Math.abs(width - obj.width);
			obj.absDeltaHeight = Math.abs(height - obj.height);
			obj.absDeltaRatio = Math.abs(ratio - obj.ratio);
			return obj;
		}

		var imageArray = [];

		Utils.each(this._imageList, function (key, value) {
			imageArray.push(cloneImage(value));
		});

		if (imageArray.length < 2) {
			// nothing to compare, give 'em the only image they got
			return imageArray[0].url;
		}

		// pull the closest by width
		var sortedByWidth = imageArray.sort(this._compareAbsDeltaWidth);
		var closestByWidth = sortedByWidth[0];
		log.debug("getImageForDimensions", "closest by width", closestByWidth);

		// pull the closest by height
		var sortedByHeight = sortedByWidth.sort(this._compareAbsDeltaHeight);
		var closestByHeight = sortedByHeight[0];
		log.debug("getImageForDimensions", "closest by height", closestByHeight);

		var winner;

		if (closestByWidth === closestByHeight) {
			log.debug("getImageForDimensions", "found an obvious winner (closest by width AND height)");
			winner = closestByWidth;
		}
		else {
			// of the two closest, by width and by height, get the best ratio
			var sortedByRatio = [closestByWidth, closestByHeight].sort(this._compareAbsDeltaRatio);
			log.debug("getImageForDimensions", "comparing closest-by-width and closest-by-height to find the closest in aspect ratio", closestByWidth, closestByHeight);
			winner = sortedByRatio[0];
		}

		log.debug("getImageForDimensions", "winner", winner);
		return winner.url;
	},

	_compareAbsDeltaWidth : function (a, b) {
		return a.absDeltaWidth - b.absDeltaWidth;
	},

	_compareAbsDeltaHeight : function (a, b) {
		return a.absDeltaHeight - b.absDeltaHeight;
	},

	_compareAbsDeltaRatio : function (a, b) {
		return a.absDeltaRatio - b.absDeltaRatio;
	},

	getAdUrl : function(type) {
		return (this._adList && this._adList[type] && this._adList[type].url) || null;
	},

	getClosedCaptions : function () {
		return this._captionsList;
	},

	getXML : function()
	{
		return this._xmlEntry;
	}

});


// Export public APIs
module.exports = ContentCatalogEntry;

},{"../cvp/log":28,"../cvp/util/xml":49,"../cvp/utils":50,"../vendor/Class":93}],63:[function(require,module,exports){
/* html5/dependencies/adpolicy.js */

// Load dependent modules
var Dependency = require('../../cvp/util/dependency');
var AdPolicyParser = require('../parsers/adpolicy');
var Log = require('../../cvp/log');
var App = require('../../core/app');


var logger = Log.getLogger();

var AdPolicyDependency = Dependency.extend({

	init : function()
	{
		this._super.apply(this, arguments);
		this.required = true;

		this._parser = new AdPolicyParser();
		this._parser.eParseCompleted.addListener(this._onParseCompleted, this);
		this._parser.eParseError.addListener(this._onParseError, this);
	},

	setContext : function (context) {
		this._parser.setAdPolicyContext(context);
	},

	load : function()
	{
		logger.info("AdPolicyDependency", "loading xml", this._assetUrl);
		this._parser.parse(this._assetUrl);
	},

	_onParseCompleted : function(adpolicy)
	{
		var type = adpolicy.ads.attr.type.toLowerCase();
		if (type === "freewheel")
		{
			logger.info("AdPolicyDependency", "adding FW dependency");
			this._manager.addDependency(new Dependency(App.AD_MANAGER_URL, true));
		} else if (type === "dfp") {
			logger.info("AdPolicyDependency", "adding DFP dependency");
			this._manager.addDependency(new Dependency(App.AD_DFP_URL, true));
		}
		this._success(adpolicy);
	},

	_onParseError : function()
	{
		this._failure();
	},

	_complete : function ()
	{
		if (this._parser) {
			this._parser.eParseCompleted.removeListener(this._onParseCompleted, this);
			this._parser.eParseError.removeListener(this._onParseError, this);
			this._parser = null;
		}
		this._super();
	},

	getDesc : function()
	{
		return "AdPolicyDependency: " + this._assetUrl;
	}

});


// Export public APIs
module.exports = AdPolicyDependency;

},{"../../core/app":8,"../../cvp/log":28,"../../cvp/util/dependency":44,"../parsers/adpolicy":74}],64:[function(require,module,exports){
/* html5/dependencies/appconfig.js */

// Load dependent modules
var Dependency = require('../../cvp/util/dependency');
var AppConfigParser = require('../parsers/appconfig');
var ContainerDependency = require('./container');
var Utils = require('../../cvp/utils');
var Log = require('../../cvp/log');


var log = Log.getLogger();

var AppConfigDependency = Dependency.extend({

	init : function()
	{
		this._super.apply(this, arguments);
		this.required = true;

		this._parser = new AppConfigParser();
		this._parser.eParseCompleted.addListener(this._onParseCompleted, this);
		this._parser.eParseError.addListener(this._onParseError, this);
	},

	load : function()
	{
		log.info("AppConfigDependency", "loading xml", this._assetUrl);
		this._parser.parse(this._assetUrl);
	},

	_onParseCompleted : function(appConfig)
	{
		if (!appConfig || Utils.empty(appConfig.containerUrl) || Utils.empty(appConfig.configUrl))
		{
			log.error("Unable to retrieve the appConfig file.");
			this._failure();
			return;
		}

		var containerDependency = new ContainerDependency(appConfig.containerUrl, true);
		containerDependency.setConfigUrl(appConfig.configUrl);
		containerDependency.passParams(this._manager.getParams());

		this._manager.addDependency(containerDependency);
		this._success(appConfig);
	},

	_onParseError : function()
	{
		this._failure();
	},

	_complete : function ()
	{
		if (this._parser) {
			this._parser.eParseCompleted.removeListener(this._onParseCompleted, this);
			this._parser.eParseError.removeListener(this._onParseError, this);
			this._parser = null;
		}
		this._super();
	},

	getDesc : function()
	{
		return "AppConfigDependency: " + this._assetUrl;
	}

});


// Export public APIs
module.exports = AppConfigDependency;

},{"../../cvp/log":28,"../../cvp/util/dependency":44,"../../cvp/utils":50,"../parsers/appconfig":75,"./container":66}],65:[function(require,module,exports){
/* html5/dependencies/config.js */

// Load dependent modules
var Dependency = require('../../cvp/util/dependency');
var ConfigParser = require('../parsers/config');
var Utils = require('../../cvp/utils');
var Log = require('../../cvp/log');


var log = Log.getLogger();

var ConfigDependency = Dependency.extend({

	init : function()
	{
		this._super.apply(this, arguments);
		this.required = true;

		this._parser = new ConfigParser();
		this._parser.eParseCompleted.addListener(this._onParseCompleted, this);
		this._parser.eParseError.addListener(this._onParseError, this);
	},

	setContext : function (context) {
		this._parser.setPlayerInstance(context);
	},

	load : function()
	{
		this._parser.parse(this._assetUrl);
	},

	_onParseCompleted : function(configInfo)
	{
		// based on the config, load any additional dependencies
		if (this._manager)
		{
			if (!Utils.empty(configInfo.omniture) && !Utils.empty(configInfo.omniture.omniture_account))
			{
				log.info("ContainerDependency", "adding Omniture dependency");
				// TODO handle omniture dependency
				// this._manager.addDependency(new Dependency(OMNITURE_JS_URL, true));
			}
		}

		this._success(configInfo);
	},

	_onParseError : function()
	{
		this._failure();
	},

	_complete : function ()
	{
		if (this._parser) {
			this._parser.eParseCompleted.removeListener(this._onParseCompleted, this);
			this._parser.eParseError.removeListener(this._onParseError, this);
			this._parser = null;
		}
		this._super();
	},

	getDesc : function()
	{
		return "ConfigDependency: " + this._assetUrl;
	}

});


// Export public APIs
module.exports = ConfigDependency;

},{"../../cvp/log":28,"../../cvp/util/dependency":44,"../../cvp/utils":50,"../parsers/config":76}],66:[function(require,module,exports){
/* html5/dependencies/container.js */

// Load dependent modules
var Dependency = require('../../cvp/util/dependency');
var ContainerParser = require('../parsers/container');
var ConfigDependency = require('./config');
var AdPolicyDependency = require('./adpolicy');
var TrackingPolicyDependency = require('./trackingpolicy');
var Utils = require('../../cvp/utils');
var Log = require('../../cvp/log');


var log = Log.getLogger();

var ContainerDependency = Dependency.extend({

	init : function()
	{
		this._super.apply(this, arguments);
		this.required = true;

		this._parser = new ContainerParser();
		this._parser.eParseCompleted.addListener(this._onParseCompleted, this);
		this._parser.eParseError.addListener(this._onParseError, this);
	},

	setConfigUrl : function (url) {
		this.configUrl = url;
	},

	passParams : function (params) {
		this._parser.setParams(params);
	},

	load : function()
	{
		log.info("ContainerDependency", "loading xml", this._assetUrl);
		this._parser.parse(this._assetUrl);
	},

	_onParseCompleted : function(containerInfo)
	{
		var configDependency = new ConfigDependency(this.configUrl, true);
		configDependency.setContext(containerInfo.playerInstance);
		this._manager.addDependency(configDependency);

		if (!Utils.empty(containerInfo.adPolicySrc)) {
			var adPolicyDependency = new AdPolicyDependency(containerInfo.adPolicySrc, true);
			adPolicyDependency.setContext(containerInfo.adPolicyContext);
			this._manager.addDependency(adPolicyDependency);
		}

		if (!Utils.empty(containerInfo.trackingPolicySrc)) {
			var trackingPolicyDependency = new TrackingPolicyDependency(containerInfo.trackingPolicySrc, false);
			trackingPolicyDependency.setContext(containerInfo.trackingPolicyContext);
			this._manager.addDependency(trackingPolicyDependency);
		}

		this._success(containerInfo);
	},

	_onParseError : function()
	{
		this._failure();
	},

	_complete : function ()
	{
		if (this._parser) {
			this._parser.eParseCompleted.removeListener(this._onParseCompleted, this);
			this._parser.eParseError.removeListener(this._onParseError, this);
			this._parser = null;
		}
		this._super();
	},

	getDesc : function()
	{
		return "ContainerDependency: " + this._assetUrl;
	}

});


// Export public APIs
module.exports = ContainerDependency;

},{"../../cvp/log":28,"../../cvp/util/dependency":44,"../../cvp/utils":50,"../parsers/container":77,"./adpolicy":63,"./config":65,"./trackingpolicy":68}],67:[function(require,module,exports){
/* html5/dependencies/mapping.js */

// Load dependent modules
var Dependency = require('../../cvp/util/dependency');
var MappingParser = require('../parsers/mapping');
var AppConfigDependency = require('./appconfig');
var Utils = require('../../cvp/utils');
var Log = require('../../cvp/log');


var log = Log.getLogger();

var MappingDependency = Dependency.extend({

	init : function()
	{
		this._super.apply(this, arguments);
		this.required = true;

		this._parser = new MappingParser();
		this._parser.eParseCompleted.addListener(this._onParseCompleted, this);
		this._parser.eParseError.addListener(this._onParseError, this);
	},

	setContext : function (context) {
		this._parser.setMappingProfile(context);
	},

	load : function()
	{
		log.info("MappingDependency", "loading xml", this._assetUrl);
		this._parser.parse(this._assetUrl);
	},

	_onParseCompleted : function(mapping)
	{
		// If there's a valid mapping, get the appConfig
		if (!mapping || Utils.empty(mapping.url)) {
			log.error("Unable to retrieve the mapping file.");
			this._failure();
			return;
		}

		// Default to JSONP
		var appConfigUrl = Utils.replaceExtension(mapping.url, "xml", "json");
		this._manager.addDependency(new AppConfigDependency(appConfigUrl, true));

		this._success(mapping);
	},

	_onParseError : function()
	{
		this._failure();
	},

	_complete : function ()
	{
		if (this._parser) {
			this._parser.eParseCompleted.removeListener(this._onParseCompleted, this);
			this._parser.eParseError.removeListener(this._onParseError, this);
			this._parser = null;
		}
		this._super();
	},

	getDesc : function()
	{
		return "MappingDependency: " + this._assetUrl;
	}

});


// Export public APIs
module.exports = MappingDependency;

},{"../../cvp/log":28,"../../cvp/util/dependency":44,"../../cvp/utils":50,"../parsers/mapping":79,"./appconfig":64}],68:[function(require,module,exports){
/* html5/dependencies/trackingpolicy.js */

// Load dependent modules
var Dependency = require('../../cvp/util/dependency');
var TrackingPolicyParser = require('../parsers/trackingpolicy');
var Log = require('../../cvp/log');


var logger = Log.getLogger();

var TrackingPolicyDependency = Dependency.extend({

	init : function()
	{
		this._super.apply(this, arguments);

		this._parser = new TrackingPolicyParser();
		this._parser.eParseCompleted.addListener(this._onParseCompleted, this);
		this._parser.eParseError.addListener(this._onParseError, this);
	},

	setContext : function (context) {
		this._parser.setTrackingPolicyContext(context);
	},

	load : function()
	{
		logger.info("TrackingPolicyDependency", "loading xml", this._assetUrl);
		this._parser.parse(this._assetUrl);
	},

	_onParseCompleted : function(trackingPolicy)
	{
		this._success(trackingPolicy);
	},

	_onParseError : function()
	{
		this._failure();
	},

	_complete : function ()
	{
		if (this._parser) {
			this._parser.eParseCompleted.removeListener(this._onParseCompleted, this);
			this._parser.eParseError.removeListener(this._onParseError, this);
			this._parser = null;
		}
		this._super();
	},

	getDesc : function()
	{
		return "TrackingPolicyDependency: " + this._assetUrl;
	}

});


// Export public APIs
module.exports = TrackingPolicyDependency;

},{"../../cvp/log":28,"../../cvp/util/dependency":44,"../parsers/trackingpolicy":80}],69:[function(require,module,exports){
/* html5/fakecontententry.js */

// Load dependent modules
var ContentCatalogEntry = require('./contententry');
var App = require('../core/app');
var XMLUtils = require('../cvp/util/xml');


var ExpiredCatalogEntry = ContentCatalogEntry.extend({

	init : function ()
	{
		var xmlString = (
			"<video id=\"not_avail\">" +
				"<headline>The requested video is not available.</headline>" +
				"<description></description>" +
				"<isExpired>true</isExpired>" +
				"<isAdSensitive>true</isAdSensitive>" +
				"<trt>0</trt>" +
				"<files><file>" + App.EXPIRED_MP4_URL + "</file></files>" +
				"<images><image width=\"320\" height=\"240\">" + App.EXPIRED_IMG_URL + "</image></images>" +
			"</video>"
		);

		var xml = XMLUtils.stringToDoc(xmlString);

		this._super(xml);

	}

});


// Export public APIs
module.exports = ExpiredCatalogEntry;

},{"../core/app":8,"../cvp/util/xml":49,"./contententry":62}],70:[function(require,module,exports){
(function (console, $hx_exports) { "use strict";
$hx_exports.com = $hx_exports.com || {};
$hx_exports.com.turner = $hx_exports.com.turner || {};
$hx_exports.com.turner.cvp = $hx_exports.com.turner.cvp || {};
$hx_exports.com.turner.cvp.feature = $hx_exports.com.turner.cvp.feature || {};
$hx_exports.com.turner.cvp.feature.ads = $hx_exports.com.turner.cvp.feature.ads || {};
$hx_exports.com.turner.cvp.feature.ads.kvp = $hx_exports.com.turner.cvp.feature.ads.kvp || {};
var $estr = function() { return js_Boot.__string_rec(this,''); };
function $extend(from, fields) {
	function Inherit() {} Inherit.prototype = from; var proto = new Inherit();
	for (var name in fields) proto[name] = fields[name];
	if( fields.toString !== Object.prototype.toString ) proto.toString = fields.toString;
	return proto;
}
var EReg = function(r,opt) {
	opt = opt.split("u").join("");
	this.r = new RegExp(r,opt);
};
EReg.__name__ = true;
EReg.prototype = {
	match: function(s) {
		if(this.r.global) this.r.lastIndex = 0;
		this.r.m = this.r.exec(s);
		this.r.s = s;
		return this.r.m != null;
	}
};
var HxOverrides = function() { };
HxOverrides.__name__ = true;
HxOverrides.cca = function(s,index) {
	var x = s.charCodeAt(index);
	if(x != x) return undefined;
	return x;
};
HxOverrides.substr = function(s,pos,len) {
	if(pos != null && pos != 0 && len != null && len < 0) return "";
	if(len == null) len = s.length;
	if(pos < 0) {
		pos = s.length + pos;
		if(pos < 0) pos = 0;
	} else if(len < 0) len = s.length + len - pos;
	return s.substr(pos,len);
};
HxOverrides.indexOf = function(a,obj,i) {
	var len = a.length;
	if(i < 0) {
		i += len;
		if(i < 0) i = 0;
	}
	while(i < len) {
		if(a[i] === obj) return i;
		i++;
	}
	return -1;
};
HxOverrides.remove = function(a,obj) {
	var i = HxOverrides.indexOf(a,obj,0);
	if(i == -1) return false;
	a.splice(i,1);
	return true;
};
HxOverrides.iter = function(a) {
	return { cur : 0, arr : a, hasNext : function() {
		return this.cur < this.arr.length;
	}, next : function() {
		return this.arr[this.cur++];
	}};
};
var List = function() {
	this.length = 0;
};
List.__name__ = true;
List.prototype = {
	add: function(item) {
		var x = [item];
		if(this.h == null) this.h = x; else this.q[1] = x;
		this.q = x;
		this.length++;
	}
	,iterator: function() {
		return new _$List_ListIterator(this.h);
	}
};
var _$List_ListIterator = function(head) {
	this.head = head;
	this.val = null;
};
_$List_ListIterator.__name__ = true;
_$List_ListIterator.prototype = {
	hasNext: function() {
		return this.head != null;
	}
	,next: function() {
		this.val = this.head[0];
		this.head = this.head[1];
		return this.val;
	}
};
Math.__name__ = true;
var Reflect = function() { };
Reflect.__name__ = true;
Reflect.fields = function(o) {
	var a = [];
	if(o != null) {
		var hasOwnProperty = Object.prototype.hasOwnProperty;
		for( var f in o ) {
		if(f != "__id__" && f != "hx__closures__" && hasOwnProperty.call(o,f)) a.push(f);
		}
	}
	return a;
};
Reflect.isEnumValue = function(v) {
	return v != null && v.__enum__ != null;
};
var Std = function() { };
Std.__name__ = true;
Std.string = function(s) {
	return js_Boot.__string_rec(s,"");
};
Std.parseInt = function(x) {
	var v = parseInt(x,10);
	if(v == 0 && (HxOverrides.cca(x,1) == 120 || HxOverrides.cca(x,1) == 88)) v = parseInt(x);
	if(isNaN(v)) return null;
	return v;
};
var StringBuf = function() {
	this.b = "";
};
StringBuf.__name__ = true;
StringBuf.prototype = {
	add: function(x) {
		this.b += Std.string(x);
	}
	,addSub: function(s,pos,len) {
		if(len == null) this.b += HxOverrides.substr(s,pos,null); else this.b += HxOverrides.substr(s,pos,len);
	}
};
var StringTools = function() { };
StringTools.__name__ = true;
StringTools.fastCodeAt = function(s,index) {
	return s.charCodeAt(index);
};
var Type = function() { };
Type.__name__ = true;
Type.getEnum = function(o) {
	if(o == null) return null;
	return o.__enum__;
};
var _$UInt_UInt_$Impl_$ = {};
_$UInt_UInt_$Impl_$.__name__ = true;
_$UInt_UInt_$Impl_$.gt = function(a,b) {
	var aNeg = a < 0;
	var bNeg = b < 0;
	if(aNeg != bNeg) return aNeg; else return a > b;
};
var Xml = function(nodeType) {
	this.nodeType = nodeType;
	this.children = [];
	this.attributeMap = new haxe_ds_StringMap();
};
Xml.__name__ = true;
Xml.parse = function(str) {
	return haxe_xml_Parser.parse(str);
};
Xml.createElement = function(name) {
	var xml = new Xml(Xml.Element);
	if(xml.nodeType != Xml.Element) throw new js__$Boot_HaxeError("Bad node type, expected Element but found " + xml.nodeType);
	xml.nodeName = name;
	return xml;
};
Xml.createPCData = function(data) {
	var xml = new Xml(Xml.PCData);
	if(xml.nodeType == Xml.Document || xml.nodeType == Xml.Element) throw new js__$Boot_HaxeError("Bad node type, unexpected " + xml.nodeType);
	xml.nodeValue = data;
	return xml;
};
Xml.createCData = function(data) {
	var xml = new Xml(Xml.CData);
	if(xml.nodeType == Xml.Document || xml.nodeType == Xml.Element) throw new js__$Boot_HaxeError("Bad node type, unexpected " + xml.nodeType);
	xml.nodeValue = data;
	return xml;
};
Xml.createComment = function(data) {
	var xml = new Xml(Xml.Comment);
	if(xml.nodeType == Xml.Document || xml.nodeType == Xml.Element) throw new js__$Boot_HaxeError("Bad node type, unexpected " + xml.nodeType);
	xml.nodeValue = data;
	return xml;
};
Xml.createDocType = function(data) {
	var xml = new Xml(Xml.DocType);
	if(xml.nodeType == Xml.Document || xml.nodeType == Xml.Element) throw new js__$Boot_HaxeError("Bad node type, unexpected " + xml.nodeType);
	xml.nodeValue = data;
	return xml;
};
Xml.createProcessingInstruction = function(data) {
	var xml = new Xml(Xml.ProcessingInstruction);
	if(xml.nodeType == Xml.Document || xml.nodeType == Xml.Element) throw new js__$Boot_HaxeError("Bad node type, unexpected " + xml.nodeType);
	xml.nodeValue = data;
	return xml;
};
Xml.createDocument = function() {
	return new Xml(Xml.Document);
};
Xml.prototype = {
	get_nodeName: function() {
		if(this.nodeType != Xml.Element) throw new js__$Boot_HaxeError("Bad node type, expected Element but found " + this.nodeType);
		return this.nodeName;
	}
	,get: function(att) {
		if(this.nodeType != Xml.Element) throw new js__$Boot_HaxeError("Bad node type, expected Element but found " + this.nodeType);
		return this.attributeMap.get(att);
	}
	,set: function(att,value) {
		if(this.nodeType != Xml.Element) throw new js__$Boot_HaxeError("Bad node type, expected Element but found " + this.nodeType);
		this.attributeMap.set(att,value);
	}
	,exists: function(att) {
		if(this.nodeType != Xml.Element) throw new js__$Boot_HaxeError("Bad node type, expected Element but found " + this.nodeType);
		return this.attributeMap.exists(att);
	}
	,attributes: function() {
		if(this.nodeType != Xml.Element) throw new js__$Boot_HaxeError("Bad node type, expected Element but found " + this.nodeType);
		return this.attributeMap.keys();
	}
	,elements: function() {
		if(this.nodeType != Xml.Document && this.nodeType != Xml.Element) throw new js__$Boot_HaxeError("Bad node type, expected Element or Document but found " + this.nodeType);
		var ret;
		var _g = [];
		var _g1 = 0;
		var _g2 = this.children;
		while(_g1 < _g2.length) {
			var child = _g2[_g1];
			++_g1;
			if(child.nodeType == Xml.Element) _g.push(child);
		}
		ret = _g;
		return HxOverrides.iter(ret);
	}
	,elementsNamed: function(name) {
		if(this.nodeType != Xml.Document && this.nodeType != Xml.Element) throw new js__$Boot_HaxeError("Bad node type, expected Element or Document but found " + this.nodeType);
		var ret;
		var _g = [];
		var _g1 = 0;
		var _g2 = this.children;
		while(_g1 < _g2.length) {
			var child = _g2[_g1];
			++_g1;
			if(child.nodeType == Xml.Element && (function($this) {
				var $r;
				if(child.nodeType != Xml.Element) throw new js__$Boot_HaxeError("Bad node type, expected Element but found " + child.nodeType);
				$r = child.nodeName;
				return $r;
			}(this)) == name) _g.push(child);
		}
		ret = _g;
		return HxOverrides.iter(ret);
	}
	,firstElement: function() {
		if(this.nodeType != Xml.Document && this.nodeType != Xml.Element) throw new js__$Boot_HaxeError("Bad node type, expected Element or Document but found " + this.nodeType);
		var _g = 0;
		var _g1 = this.children;
		while(_g < _g1.length) {
			var child = _g1[_g];
			++_g;
			if(child.nodeType == Xml.Element) return child;
		}
		return null;
	}
	,addChild: function(x) {
		if(this.nodeType != Xml.Document && this.nodeType != Xml.Element) throw new js__$Boot_HaxeError("Bad node type, expected Element or Document but found " + this.nodeType);
		if(x.parent != null) x.parent.removeChild(x);
		this.children.push(x);
		x.parent = this;
	}
	,removeChild: function(x) {
		if(this.nodeType != Xml.Document && this.nodeType != Xml.Element) throw new js__$Boot_HaxeError("Bad node type, expected Element or Document but found " + this.nodeType);
		if(HxOverrides.remove(this.children,x)) {
			x.parent = null;
			return true;
		}
		return false;
	}
};
var AdKeyValueConfigParser = $hx_exports.AdKeyValueConfigParser = function() { };
AdKeyValueConfigParser.__name__ = true;
AdKeyValueConfigParser.parse = function(xmlStr) {
	var createXml = function(xmlStr1) {
		var xml = Xml.parse(xmlStr1);
		return new haxe_xml_Fast(xml.firstElement());
	};
	var fast = createXml(xmlStr);
	if(fast == null) return null;
	var getAttrs = function(node) {
		var xml1 = node.x;
		return xml1.attributes();
	};
	var getAttrOr = function(node1,attr,defaultVal) {
		var xml2 = node1.x;
		if(xml2.exists(attr)) return xml2.get(attr); else return defaultVal;
	};
	var parseSetting = function(node2) {
		var setting = new haxe_ds_StringMap();
		if(node2 != null) {
			var $it0 = getAttrs(node2);
			while( $it0.hasNext() ) {
				var attr1 = $it0.next();
				var value = getAttrOr(node2,attr1);
				if(__map_reserved[attr1] != null) setting.setReserved(attr1,value); else setting.h[attr1] = value;
			}
		}
		return setting;
	};
	var keyValues = [];
	var _g = fast.nodes.resolve("keyValue").iterator();
	while(_g.head != null) {
		var keyValue;
		keyValue = (function($this) {
			var $r;
			_g.val = _g.head[0];
			_g.head = _g.head[1];
			$r = _g.val;
			return $r;
		}(this));
		var name;
		if(keyValue.has.resolve("name")) name = keyValue.att.resolve("name"); else name = null;
		var value1;
		if(keyValue.has.resolve("value")) value1 = keyValue.att.resolve("value"); else value1 = null;
		var source;
		if(keyValue.has.resolve("source")) source = keyValue.att.resolve("source"); else source = null;
		var settings = new haxe_ds_StringMap();
		var $it1 = keyValue.get_elements();
		while( $it1.hasNext() ) {
			var el = $it1.next();
			var key = el.get_name();
			var value2 = parseSetting(el);
			if(__map_reserved[key] != null) settings.setReserved(key,value2); else settings.h[key] = value2;
		}
		keyValues.push(new com_turner_cvp_feature_ads_kvp_AdKeyValueVo(name,value1,source,new com_turner_cvp_feature_ads_kvp_AdKeyValueSettingsVo(settings)));
	}
	return new com_turner_cvp_feature_ads_kvp_AdKeyValueConfigVo(keyValues);
};
var com_turner_cvp_feature_ads_kvp_AdKeyValueConfigVo = $hx_exports.com.turner.cvp.feature.ads.kvp.AdKeyValueConfigVo = function(kvps) {
	this.keyValuePairs = kvps;
};
com_turner_cvp_feature_ads_kvp_AdKeyValueConfigVo.__name__ = true;
com_turner_cvp_feature_ads_kvp_AdKeyValueConfigVo.prototype = {
	toString: function() {
		return this.toJson();
	}
	,toJson: function() {
		return JSON.stringify(this);
	}
};
var AdKeyValueEvaluator = $hx_exports.AdKeyValueEvaluator = function(config) {
	this._config = config;
	if(this._config == null) this._config = new com_turner_cvp_feature_ads_kvp_AdKeyValueConfigVo([]);
	this._data = [];
	var _g = 0;
	var _g1 = this._config.keyValuePairs;
	while(_g < _g1.length) {
		var keyValueVo = _g1[_g];
		++_g;
		this._data.push(new com_turner_cvp_feature_ads_kvp__$AdKeyValueEvaluator_AdKeyValueInstance(keyValueVo));
	}
};
AdKeyValueEvaluator.__name__ = true;
AdKeyValueEvaluator.prototype = {
	reset: function() {
		var _g = 0;
		var _g1 = this._data;
		while(_g < _g1.length) {
			var instance = _g1[_g];
			++_g;
			instance.reset();
		}
	}
	,evaluate: function(catalogEntry) {
		var kvps = { };
		this._data = this._data.filter(function(instance) {
			return instance.withinLifetime();
		});
		var _g = 0;
		var _g1 = this._data;
		while(_g < _g1.length) {
			var instance1 = _g1[_g];
			++_g;
			if(instance1.shouldEvaluate()) instance1.evaluate(catalogEntry);
			var pairs = instance1.getPairs();
			var _g2 = 0;
			while(_g2 < pairs.length) {
				var pair = pairs[_g2];
				++_g2;
				kvps[pair.key] = pair.val;
			}
		}
		return kvps;
	}
};
var com_turner_cvp_feature_ads_kvp__$AdKeyValueEvaluator_AdKeyValueInstance = function(vo) {
	this._vo = vo;
	this._frequency = this._vo.settings.frequency;
	this._lifetime = this._vo.settings.lifetime;
	this._maxPairs = this._vo.settings.maxPairs;
	this._totalCount = 0;
	this._contentCount = 0;
};
com_turner_cvp_feature_ads_kvp__$AdKeyValueEvaluator_AdKeyValueInstance.__name__ = true;
com_turner_cvp_feature_ads_kvp__$AdKeyValueEvaluator_AdKeyValueInstance.prototype = {
	reset: function() {
		this._contentCount++;
	}
	,withinLifetime: function() {
		var _g = this._lifetime.scope;
		switch(_g[1]) {
		case 2:
			return true;
		case 0:
			return _$UInt_UInt_$Impl_$.gt(this._lifetime.value,this._totalCount);
		case 1:
			return _$UInt_UInt_$Impl_$.gt(this._lifetime.value,this._contentCount);
		}
	}
	,shouldEvaluate: function() {
		var _g = this._frequency.scope;
		switch(_g[1]) {
		case 2:
			return true;
		case 0:
			return _$UInt_UInt_$Impl_$.gt(this._lifetime.value,this._totalCount);
		case 1:
			return _$UInt_UInt_$Impl_$.gt(this._lifetime.value,this._contentCount);
		}
	}
	,evaluate: function(catalogEntry) {
		this._pairs = [];
		if(this._vo.source != null) {
			var data = com_turner_cvp_haxe_adapters_StringReplace.replace(this._vo.source,catalogEntry);
			var keys = Reflect.fields(data);
			var len;
			if(_$UInt_UInt_$Impl_$.gt(this._maxPairs,0)) len = this._maxPairs; else len = keys.length;
			var _g = 0;
			while(_g < len) {
				var i = _g++;
				var key = keys[i];
				var val = data[key];
				this._pairs.push({ key : key, val : val});
			}
		} else {
			var key1 = this._vo.key;
			var val1 = com_turner_cvp_haxe_adapters_StringReplace.replace(this._vo.val,catalogEntry);
			this._pairs.push({ key : key1, val : val1});
			this._totalCount++;
		}
	}
	,log: function(msg) {
	}
	,getPairs: function() {
		return this._pairs;
	}
};
var com_turner_cvp_feature_ads_kvp_AdKeyValueSettingsVo = function(settings) {
	this.frequency = this.parseScopedConstraint(__map_reserved.frequency != null?settings.getReserved("frequency"):settings.h["frequency"]);
	this.lifetime = this.parseScopedConstraint(__map_reserved.lifetime != null?settings.getReserved("lifetime"):settings.h["lifetime"]);
	this.maxPairs = this.parseMaxPairs(__map_reserved.maxPairs != null?settings.getReserved("maxPairs"):settings.h["maxPairs"]);
};
com_turner_cvp_feature_ads_kvp_AdKeyValueSettingsVo.__name__ = true;
com_turner_cvp_feature_ads_kvp_AdKeyValueSettingsVo.prototype = {
	parseScopedConstraint: function(setting) {
		if(setting == null) setting = new haxe_ds_StringMap();
		var strScope;
		strScope = __map_reserved.scope != null?setting.getReserved("scope"):setting.h["scope"];
		var scope;
		switch(strScope) {
		case "session":
			scope = com_turner_cvp_feature_ads_kvp_Scope.Session;
			break;
		case "ad":
			scope = com_turner_cvp_feature_ads_kvp_Scope.Ad;
			break;
		case "content":
			scope = com_turner_cvp_feature_ads_kvp_Scope.Content;
			break;
		default:
			scope = com_turner_cvp_feature_ads_kvp_Scope.Session;
		}
		var strVal;
		strVal = __map_reserved.value != null?setting.getReserved("value"):setting.h["value"];
		var value;
		var _g = new EReg("^\\d+$","").match(strVal);
		switch(_g) {
		case true:
			value = Std.parseInt(strVal);
			break;
		case false:
			switch(strVal) {
			case "always":
				value = 0;
				break;
			case "once":
				value = 1;
				break;
			default:
				value = 0;
			}
			break;
		}
		var constraint = { scope : scope, value : value};
		return constraint;
	}
	,parseMaxPairs: function(setting) {
		var val;
		if(setting != null && (__map_reserved.value != null?setting.existsReserved("value"):setting.h.hasOwnProperty("value"))) val = Std.parseInt(__map_reserved.value != null?setting.getReserved("value"):setting.h["value"]); else val = 0;
		return val;
	}
	,toString: function() {
		return this.toJson();
	}
	,toJson: function() {
		return JSON.stringify(this);
	}
};
var com_turner_cvp_feature_ads_kvp_AdKeyValueTypes = function() { };
com_turner_cvp_feature_ads_kvp_AdKeyValueTypes.__name__ = true;
var com_turner_cvp_feature_ads_kvp_Scope = { __ename__ : true, __constructs__ : ["Ad","Content","Session"] };
com_turner_cvp_feature_ads_kvp_Scope.Ad = ["Ad",0];
com_turner_cvp_feature_ads_kvp_Scope.Ad.toString = $estr;
com_turner_cvp_feature_ads_kvp_Scope.Ad.__enum__ = com_turner_cvp_feature_ads_kvp_Scope;
com_turner_cvp_feature_ads_kvp_Scope.Content = ["Content",1];
com_turner_cvp_feature_ads_kvp_Scope.Content.toString = $estr;
com_turner_cvp_feature_ads_kvp_Scope.Content.__enum__ = com_turner_cvp_feature_ads_kvp_Scope;
com_turner_cvp_feature_ads_kvp_Scope.Session = ["Session",2];
com_turner_cvp_feature_ads_kvp_Scope.Session.toString = $estr;
com_turner_cvp_feature_ads_kvp_Scope.Session.__enum__ = com_turner_cvp_feature_ads_kvp_Scope;
var com_turner_cvp_feature_ads_kvp_AdKeyValueVo = function(key,val,source,settings) {
	this.key = key;
	this.val = val;
	this.source = source;
	this.settings = settings;
};
com_turner_cvp_feature_ads_kvp_AdKeyValueVo.__name__ = true;
com_turner_cvp_feature_ads_kvp_AdKeyValueVo.prototype = {
	toString: function() {
		return this.toJson();
	}
	,toJson: function() {
		return JSON.stringify(this);
	}
};
var com_turner_cvp_haxe_adapters_StringReplace = function() { };
com_turner_cvp_haxe_adapters_StringReplace.__name__ = true;
com_turner_cvp_haxe_adapters_StringReplace.replace = function(str,entry,uriEncode) {
	if(uriEncode == null) uriEncode = false;
	var ConfigUtils = require('../../cvp/util/configutils');
	return ConfigUtils.stringReplace(str,entry,uriEncode);
};
var com_turner_cvp_haxe_utils_EnumUtils = function() { };
com_turner_cvp_haxe_utils_EnumUtils.__name__ = true;
com_turner_cvp_haxe_utils_EnumUtils.contains = function(e,val) {
	if(val != null && Reflect.isEnumValue(val) && e == Type.getEnum(val)) return true;
	return false;
};
com_turner_cvp_haxe_utils_EnumUtils.isValidOr = function(e,val,defaultVal) {
	return com_turner_cvp_haxe_utils_EnumUtils.contains(e,val)?val:defaultVal;
};
var haxe_IMap = function() { };
haxe_IMap.__name__ = true;
var haxe_ds_StringMap = function() {
	this.h = { };
};
haxe_ds_StringMap.__name__ = true;
haxe_ds_StringMap.__interfaces__ = [haxe_IMap];
haxe_ds_StringMap.prototype = {
	set: function(key,value) {
		if(__map_reserved[key] != null) this.setReserved(key,value); else this.h[key] = value;
	}
	,get: function(key) {
		if(__map_reserved[key] != null) return this.getReserved(key);
		return this.h[key];
	}
	,exists: function(key) {
		if(__map_reserved[key] != null) return this.existsReserved(key);
		return this.h.hasOwnProperty(key);
	}
	,setReserved: function(key,value) {
		if(this.rh == null) this.rh = { };
		this.rh["$" + key] = value;
	}
	,getReserved: function(key) {
		if(this.rh == null) return null; else return this.rh["$" + key];
	}
	,existsReserved: function(key) {
		if(this.rh == null) return false;
		return this.rh.hasOwnProperty("$" + key);
	}
	,keys: function() {
		var _this = this.arrayKeys();
		return HxOverrides.iter(_this);
	}
	,arrayKeys: function() {
		var out = [];
		for( var key in this.h ) {
		if(this.h.hasOwnProperty(key)) out.push(key);
		}
		if(this.rh != null) {
			for( var key in this.rh ) {
			if(key.charCodeAt(0) == 36) out.push(key.substr(1));
			}
		}
		return out;
	}
};
var haxe_xml__$Fast_NodeAccess = function(x) {
	this.__x = x;
};
haxe_xml__$Fast_NodeAccess.__name__ = true;
var haxe_xml__$Fast_AttribAccess = function(x) {
	this.__x = x;
};
haxe_xml__$Fast_AttribAccess.__name__ = true;
haxe_xml__$Fast_AttribAccess.prototype = {
	resolve: function(name) {
		if(this.__x.nodeType == Xml.Document) throw new js__$Boot_HaxeError("Cannot access document attribute " + name);
		var v = this.__x.get(name);
		if(v == null) throw new js__$Boot_HaxeError(this.__x.get_nodeName() + " is missing attribute " + name);
		return v;
	}
};
var haxe_xml__$Fast_HasAttribAccess = function(x) {
	this.__x = x;
};
haxe_xml__$Fast_HasAttribAccess.__name__ = true;
haxe_xml__$Fast_HasAttribAccess.prototype = {
	resolve: function(name) {
		if(this.__x.nodeType == Xml.Document) throw new js__$Boot_HaxeError("Cannot access document attribute " + name);
		return this.__x.exists(name);
	}
};
var haxe_xml__$Fast_HasNodeAccess = function(x) {
	this.__x = x;
};
haxe_xml__$Fast_HasNodeAccess.__name__ = true;
var haxe_xml__$Fast_NodeListAccess = function(x) {
	this.__x = x;
};
haxe_xml__$Fast_NodeListAccess.__name__ = true;
haxe_xml__$Fast_NodeListAccess.prototype = {
	resolve: function(name) {
		var l = new List();
		var $it0 = this.__x.elementsNamed(name);
		while( $it0.hasNext() ) {
			var x = $it0.next();
			l.add(new haxe_xml_Fast(x));
		}
		return l;
	}
};
var haxe_xml_Fast = function(x) {
	if(x.nodeType != Xml.Document && x.nodeType != Xml.Element) throw new js__$Boot_HaxeError("Invalid nodeType " + x.nodeType);
	this.x = x;
	this.node = new haxe_xml__$Fast_NodeAccess(x);
	this.nodes = new haxe_xml__$Fast_NodeListAccess(x);
	this.att = new haxe_xml__$Fast_AttribAccess(x);
	this.has = new haxe_xml__$Fast_HasAttribAccess(x);
	this.hasNode = new haxe_xml__$Fast_HasNodeAccess(x);
};
haxe_xml_Fast.__name__ = true;
haxe_xml_Fast.prototype = {
	get_name: function() {
		if(this.x.nodeType == Xml.Document) return "Document"; else return this.x.get_nodeName();
	}
	,get_elements: function() {
		var it = this.x.elements();
		return { hasNext : $bind(it,it.hasNext), next : function() {
			var x = it.next();
			if(x == null) return null;
			return new haxe_xml_Fast(x);
		}};
	}
};
var haxe_xml_Parser = function() { };
haxe_xml_Parser.__name__ = true;
haxe_xml_Parser.parse = function(str,strict) {
	if(strict == null) strict = false;
	var doc = Xml.createDocument();
	haxe_xml_Parser.doParse(str,strict,0,doc);
	return doc;
};
haxe_xml_Parser.doParse = function(str,strict,p,parent) {
	if(p == null) p = 0;
	var xml = null;
	var state = 1;
	var next = 1;
	var aname = null;
	var start = 0;
	var nsubs = 0;
	var nbrackets = 0;
	var c = str.charCodeAt(p);
	var buf = new StringBuf();
	var escapeNext = 1;
	var attrValQuote = -1;
	while(!(c != c)) {
		switch(state) {
		case 0:
			switch(c) {
			case 10:case 13:case 9:case 32:
				break;
			default:
				state = next;
				continue;
			}
			break;
		case 1:
			switch(c) {
			case 60:
				state = 0;
				next = 2;
				break;
			default:
				start = p;
				state = 13;
				continue;
			}
			break;
		case 13:
			if(c == 60) {
				buf.addSub(str,start,p - start);
				var child = Xml.createPCData(buf.b);
				buf = new StringBuf();
				parent.addChild(child);
				nsubs++;
				state = 0;
				next = 2;
			} else if(c == 38) {
				buf.addSub(str,start,p - start);
				state = 18;
				escapeNext = 13;
				start = p + 1;
			}
			break;
		case 17:
			if(c == 93 && str.charCodeAt(p + 1) == 93 && str.charCodeAt(p + 2) == 62) {
				var child1 = Xml.createCData(HxOverrides.substr(str,start,p - start));
				parent.addChild(child1);
				nsubs++;
				p += 2;
				state = 1;
			}
			break;
		case 2:
			switch(c) {
			case 33:
				if(str.charCodeAt(p + 1) == 91) {
					p += 2;
					if(HxOverrides.substr(str,p,6).toUpperCase() != "CDATA[") throw new js__$Boot_HaxeError("Expected <![CDATA[");
					p += 5;
					state = 17;
					start = p + 1;
				} else if(str.charCodeAt(p + 1) == 68 || str.charCodeAt(p + 1) == 100) {
					if(HxOverrides.substr(str,p + 2,6).toUpperCase() != "OCTYPE") throw new js__$Boot_HaxeError("Expected <!DOCTYPE");
					p += 8;
					state = 16;
					start = p + 1;
				} else if(str.charCodeAt(p + 1) != 45 || str.charCodeAt(p + 2) != 45) throw new js__$Boot_HaxeError("Expected <!--"); else {
					p += 2;
					state = 15;
					start = p + 1;
				}
				break;
			case 63:
				state = 14;
				start = p;
				break;
			case 47:
				if(parent == null) throw new js__$Boot_HaxeError("Expected node name");
				start = p + 1;
				state = 0;
				next = 10;
				break;
			default:
				state = 3;
				start = p;
				continue;
			}
			break;
		case 3:
			if(!(c >= 97 && c <= 122 || c >= 65 && c <= 90 || c >= 48 && c <= 57 || c == 58 || c == 46 || c == 95 || c == 45)) {
				if(p == start) throw new js__$Boot_HaxeError("Expected node name");
				xml = Xml.createElement(HxOverrides.substr(str,start,p - start));
				parent.addChild(xml);
				nsubs++;
				state = 0;
				next = 4;
				continue;
			}
			break;
		case 4:
			switch(c) {
			case 47:
				state = 11;
				break;
			case 62:
				state = 9;
				break;
			default:
				state = 5;
				start = p;
				continue;
			}
			break;
		case 5:
			if(!(c >= 97 && c <= 122 || c >= 65 && c <= 90 || c >= 48 && c <= 57 || c == 58 || c == 46 || c == 95 || c == 45)) {
				var tmp;
				if(start == p) throw new js__$Boot_HaxeError("Expected attribute name");
				tmp = HxOverrides.substr(str,start,p - start);
				aname = tmp;
				if(xml.exists(aname)) throw new js__$Boot_HaxeError("Duplicate attribute");
				state = 0;
				next = 6;
				continue;
			}
			break;
		case 6:
			switch(c) {
			case 61:
				state = 0;
				next = 7;
				break;
			default:
				throw new js__$Boot_HaxeError("Expected =");
			}
			break;
		case 7:
			switch(c) {
			case 34:case 39:
				buf = new StringBuf();
				state = 8;
				start = p + 1;
				attrValQuote = c;
				break;
			default:
				throw new js__$Boot_HaxeError("Expected \"");
			}
			break;
		case 8:
			switch(c) {
			case 38:
				buf.addSub(str,start,p - start);
				state = 18;
				escapeNext = 8;
				start = p + 1;
				break;
			case 62:
				if(strict) throw new js__$Boot_HaxeError("Invalid unescaped " + String.fromCharCode(c) + " in attribute value"); else if(c == attrValQuote) {
					buf.addSub(str,start,p - start);
					var val = buf.b;
					buf = new StringBuf();
					xml.set(aname,val);
					state = 0;
					next = 4;
				}
				break;
			case 60:
				if(strict) throw new js__$Boot_HaxeError("Invalid unescaped " + String.fromCharCode(c) + " in attribute value"); else if(c == attrValQuote) {
					buf.addSub(str,start,p - start);
					var val1 = buf.b;
					buf = new StringBuf();
					xml.set(aname,val1);
					state = 0;
					next = 4;
				}
				break;
			default:
				if(c == attrValQuote) {
					buf.addSub(str,start,p - start);
					var val2 = buf.b;
					buf = new StringBuf();
					xml.set(aname,val2);
					state = 0;
					next = 4;
				}
			}
			break;
		case 9:
			p = haxe_xml_Parser.doParse(str,strict,p,xml);
			start = p;
			state = 1;
			break;
		case 11:
			switch(c) {
			case 62:
				state = 1;
				break;
			default:
				throw new js__$Boot_HaxeError("Expected >");
			}
			break;
		case 12:
			switch(c) {
			case 62:
				if(nsubs == 0) parent.addChild(Xml.createPCData(""));
				return p;
			default:
				throw new js__$Boot_HaxeError("Expected >");
			}
			break;
		case 10:
			if(!(c >= 97 && c <= 122 || c >= 65 && c <= 90 || c >= 48 && c <= 57 || c == 58 || c == 46 || c == 95 || c == 45)) {
				if(start == p) throw new js__$Boot_HaxeError("Expected node name");
				var v = HxOverrides.substr(str,start,p - start);
				if(v != (function($this) {
					var $r;
					if(parent.nodeType != Xml.Element) throw new js__$Boot_HaxeError("Bad node type, expected Element but found " + parent.nodeType);
					$r = parent.nodeName;
					return $r;
				}(this))) throw new js__$Boot_HaxeError("Expected </" + (function($this) {
					var $r;
					if(parent.nodeType != Xml.Element) throw "Bad node type, expected Element but found " + parent.nodeType;
					$r = parent.nodeName;
					return $r;
				}(this)) + ">");
				state = 0;
				next = 12;
				continue;
			}
			break;
		case 15:
			if(c == 45 && str.charCodeAt(p + 1) == 45 && str.charCodeAt(p + 2) == 62) {
				var xml1 = Xml.createComment(HxOverrides.substr(str,start,p - start));
				parent.addChild(xml1);
				nsubs++;
				p += 2;
				state = 1;
			}
			break;
		case 16:
			if(c == 91) nbrackets++; else if(c == 93) nbrackets--; else if(c == 62 && nbrackets == 0) {
				var xml2 = Xml.createDocType(HxOverrides.substr(str,start,p - start));
				parent.addChild(xml2);
				nsubs++;
				state = 1;
			}
			break;
		case 14:
			if(c == 63 && str.charCodeAt(p + 1) == 62) {
				p++;
				var str1 = HxOverrides.substr(str,start + 1,p - start - 2);
				var xml3 = Xml.createProcessingInstruction(str1);
				parent.addChild(xml3);
				nsubs++;
				state = 1;
			}
			break;
		case 18:
			if(c == 59) {
				var s = HxOverrides.substr(str,start,p - start);
				if(s.charCodeAt(0) == 35) {
					var c1;
					if(s.charCodeAt(1) == 120) c1 = Std.parseInt("0" + HxOverrides.substr(s,1,s.length - 1)); else c1 = Std.parseInt(HxOverrides.substr(s,1,s.length - 1));
					buf.b += String.fromCharCode(c1);
				} else if(!haxe_xml_Parser.escapes.exists(s)) {
					if(strict) throw new js__$Boot_HaxeError("Undefined entity: " + s);
					buf.b += Std.string("&" + s + ";");
				} else buf.add(haxe_xml_Parser.escapes.get(s));
				start = p + 1;
				state = escapeNext;
			} else if(!(c >= 97 && c <= 122 || c >= 65 && c <= 90 || c >= 48 && c <= 57 || c == 58 || c == 46 || c == 95 || c == 45) && c != 35) {
				if(strict) throw new js__$Boot_HaxeError("Invalid character in entity: " + String.fromCharCode(c));
				buf.b += "&";
				buf.addSub(str,start,p - start);
				p--;
				start = p + 1;
				state = escapeNext;
			}
			break;
		}
		c = StringTools.fastCodeAt(str,++p);
	}
	if(state == 1) {
		start = p;
		state = 13;
	}
	if(state == 13) {
		if(p != start || nsubs == 0) {
			buf.addSub(str,start,p - start);
			var xml4 = Xml.createPCData(buf.b);
			parent.addChild(xml4);
			nsubs++;
		}
		return p;
	}
	if(!strict && state == 18 && escapeNext == 13) {
		buf.b += "&";
		buf.addSub(str,start,p - start);
		var xml5 = Xml.createPCData(buf.b);
		parent.addChild(xml5);
		nsubs++;
		return p;
	}
	throw new js__$Boot_HaxeError("Unexpected end");
};
var js__$Boot_HaxeError = function(val) {
	Error.call(this);
	this.val = val;
	this.message = String(val);
	if(Error.captureStackTrace) Error.captureStackTrace(this,js__$Boot_HaxeError);
};
js__$Boot_HaxeError.__name__ = true;
js__$Boot_HaxeError.__super__ = Error;
js__$Boot_HaxeError.prototype = $extend(Error.prototype,{
});
var js_Boot = function() { };
js_Boot.__name__ = true;
js_Boot.__string_rec = function(o,s) {
	if(o == null) return "null";
	if(s.length >= 5) return "<...>";
	var t = typeof(o);
	if(t == "function" && (o.__name__ || o.__ename__)) t = "object";
	switch(t) {
	case "object":
		if(o instanceof Array) {
			if(o.__enum__) {
				if(o.length == 2) return o[0];
				var str2 = o[0] + "(";
				s += "\t";
				var _g1 = 2;
				var _g = o.length;
				while(_g1 < _g) {
					var i1 = _g1++;
					if(i1 != 2) str2 += "," + js_Boot.__string_rec(o[i1],s); else str2 += js_Boot.__string_rec(o[i1],s);
				}
				return str2 + ")";
			}
			var l = o.length;
			var i;
			var str1 = "[";
			s += "\t";
			var _g2 = 0;
			while(_g2 < l) {
				var i2 = _g2++;
				str1 += (i2 > 0?",":"") + js_Boot.__string_rec(o[i2],s);
			}
			str1 += "]";
			return str1;
		}
		var tostr;
		try {
			tostr = o.toString;
		} catch( e ) {
			if (e instanceof js__$Boot_HaxeError) e = e.val;
			return "???";
		}
		if(tostr != null && tostr != Object.toString && typeof(tostr) == "function") {
			var s2 = o.toString();
			if(s2 != "[object Object]") return s2;
		}
		var k = null;
		var str = "{\n";
		s += "\t";
		var hasp = o.hasOwnProperty != null;
		for( var k in o ) {
		if(hasp && !o.hasOwnProperty(k)) {
			continue;
		}
		if(k == "prototype" || k == "__class__" || k == "__super__" || k == "__interfaces__" || k == "__properties__") {
			continue;
		}
		if(str.length != 2) str += ", \n";
		str += s + k + " : " + js_Boot.__string_rec(o[k],s);
		}
		s = s.substring(1);
		str += "\n" + s + "}";
		return str;
	case "function":
		return "<function>";
	case "string":
		return o;
	default:
		return String(o);
	}
};
var $_, $fid = 0;
function $bind(o,m) { if( m == null ) return null; if( m.__id__ == null ) m.__id__ = $fid++; var f; if( o.hx__closures__ == null ) o.hx__closures__ = {}; else f = o.hx__closures__[m.__id__]; if( f == null ) { f = function(){ return f.method.apply(f.scope, arguments); }; f.scope = o; f.method = m; o.hx__closures__[m.__id__] = f; } return f; }
if(Array.prototype.indexOf) HxOverrides.indexOf = function(a,o,i) {
	return Array.prototype.indexOf.call(a,o,i);
};
String.__name__ = true;
Array.__name__ = true;
if(Array.prototype.filter == null) Array.prototype.filter = function(f1) {
	var a1 = [];
	var _g11 = 0;
	var _g2 = this.length;
	while(_g11 < _g2) {
		var i1 = _g11++;
		var e = this[i1];
		if(f1(e)) a1.push(e);
	}
	return a1;
};
var __map_reserved = {}
Xml.Element = 0;
Xml.PCData = 1;
Xml.CData = 2;
Xml.Comment = 3;
Xml.DocType = 4;
Xml.ProcessingInstruction = 5;
Xml.Document = 6;
haxe_xml_Parser.escapes = (function($this) {
	var $r;
	var h = new haxe_ds_StringMap();
	if(__map_reserved.lt != null) h.setReserved("lt","<"); else h.h["lt"] = "<";
	if(__map_reserved.gt != null) h.setReserved("gt",">"); else h.h["gt"] = ">";
	if(__map_reserved.amp != null) h.setReserved("amp","&"); else h.h["amp"] = "&";
	if(__map_reserved.quot != null) h.setReserved("quot","\""); else h.h["quot"] = "\"";
	if(__map_reserved.apos != null) h.setReserved("apos","'"); else h.h["apos"] = "'";
	$r = h;
	return $r;
}(this));
})(typeof console != "undefined" ? console : {log:function(){}}, module.exports);

},{"../../cvp/util/configutils":43}],71:[function(require,module,exports){
/* html5/fakecontententry.js */

// Load dependent modules
var ContentCatalogEntry = require('./contententry');
var Utils = require('../cvp/utils');
var XMLUtils = require('../cvp/util/xml');


var FakeCatalogEntry = ContentCatalogEntry.extend({

	init : function(obj) {

		var explicitUrl = null;
		var metadataXml = '';
		var id = obj.id || obj.url;

		if (Utils.isString(obj.url)) {
			explicitUrl = obj.url;
		}

		if (Utils.isObject(obj.metadata)) {
			for (var key in obj.metadata) {
				if (key === 'files' && explicitUrl)
					continue;

				metadataXml += "<" + key + "><![CDATA[" + obj.metadata[key] + "]]></" + key + ">";
			}
		}

		if (explicitUrl) {
			metadataXml += "<files><file><![CDATA[" + explicitUrl + "]]></file></files>";
		}

		var xmlString = (
			"<video id='" + XMLUtils.encodeXml(id) + "'> " +
				metadataXml +
			"</video>"
		);

		var xml = XMLUtils.stringToDoc(xmlString);

		this._super(xml);

	}

});


// Export public APIs
module.exports = FakeCatalogEntry;

},{"../cvp/util/xml":49,"../cvp/utils":50,"./contententry":62}],72:[function(require,module,exports){
/* html5/manager.js */

// Load dependent modules
var Class = require('../vendor/Class');
var Event = require('../cvp/customevent');
var Ajax = require('../cvp/ajax');
var Utils = require('../cvp/utils');
var JsonConverter = require('../cvp/util/jsonconverter');
var Log = require('../cvp/log');
var App = require('../core/app');
var BootStrapper = require('./bootstrapper');
var AspenController = require('./aspencontroller');
var CMS = require('./cms');
var PlayerInstance = require('./player/instance');
var TrackingManager = require('./tracking/trackingmanager');
var AspenTracking = require('./tracking/aspentracking');
var VideoBegetter = require('./player/videobegetter');
// var DomObserver = require('html5/player/domobserver');


var _logger = Log.getLogger();

var CVPManager = Class.extend({

	init : function()
	{
		this._configData = {};

		// Events
		this.ePlayerLoaded = new Event();
		this.ePlayerLoadError = new Event();
		this.ePlayerRendered = new Event();
		this.eContentMetadata = new Event();
		this.eContentBegin = new Event();
		this.eContentPlay = new Event();
		this.eContentPause = new Event();
		this.eContentEnd = new Event();
		this.eContentComplete = new Event();
		this.eContentPlayhead = new Event();
		this.eContentBuffering = new Event();
		this.eContentQueue = new Event();
		this.eContentQueueAutoplay = new Event();
		this.eContentVolume = new Event();
		this.eContentError = new Event();
		this.eContentResize = new Event();
		this.eContentEntryLoad = new Event();
		this.eContentEntryLoadError = new Event();

		this.eAdPlayhead = new Event();
		this.eAdPlay = new Event();
		this.eAdEnd = new Event();
		this.eAdError = new Event();
		this.eAdSensitive = new Event();

		this.eTrackingAdStart = new Event();
		this.eTrackingAdComplete = new Event();
		this.eTrackingAdProgress = new Event();
		this.eTrackingAdCountdown = new Event();
		this.eTrackingAdClick = new Event();

		this.eTrackingContentPlay = new Event();
		this.eTrackingContentBegin = new Event();
		this.eTrackingContentProgress = new Event();
		this.eTrackingContentEnd = new Event();
		this.eTrackingContentComplete = new Event();
		this.eTrackingContentReplay = new Event();
		this.eTrackingContentSeek = new Event();
		this.eTrackingContentSeekEnd = new Event();
		this.eTrackingFullscreen = new Event();
		this.eTrackingMuted = new Event();
		this.eTrackingPaused = new Event();

		// Loading queue
		this._loadingEntryQueue = {};

		this._params = {};
	},

	load : function(params) {
		// Prime a video element to be used later.
		// This will only be successful if the instantiation occurs within
		// scope of a user action event.
		this._videoBegetter = new VideoBegetter();
		// this._domObserver = new DomObserver(this._videoBegetter.getElement());
		// this._domObserver.addMediaListeners();
		this._videoBegetter.prime();

		// TODO normalize the parameter names
		// ..flashVars to params
		// player-specific sections...'flash' for flash, 'html5' for html5, etc..
		this._params = params;

		// hoist flashVars to params
		var flashVars = params.flashVars;
		if (Utils.isSimpleObject(flashVars))
		{
			params.site = flashVars.site;
			params.profile = flashVars.profile;

			params.context = flashVars.context;
			params.contentId = flashVars.contentId;
			params.containerUrl = flashVars.containerUrl;
			params.configUrl = flashVars.configUrl;
			params.aspenContext = flashVars.aspenContext;

			params.dt = Utils.isFlagActive(flashVars.superduperdevtime, false);

			params.requestTimeout = Utils.toInt(flashVars.requestTimeout);
			params.requestRetries = Utils.toInt(flashVars.requestRetries);

			// conditional params that override config xml
			if ('autostart' in flashVars)
				params.autostart = Utils.isFlagActive(flashVars.autostart, false);
			if ('controls' in flashVars)
				params.controls = Utils.isFlagActive(flashVars.controls, true);
		}

		if (params.requestTimeout) {
			Ajax.JSONP.requestTimeout = params.requestTimeout;
		}
		if (params.requestRetries) {
			Ajax.JSONP.maxRetries = params.requestRetries;
		}

		// Calc the mapping url based on the params
		var mappingUrl = this._calculateMappingUrl(this._params.site);
		if (Utils.empty(mappingUrl)) {
			_logger.error("Unable to calculate mapping file url. Failing.");
			this._bootStrapFailure();
			return;
		}

		this._bootStrapper = new BootStrapper(mappingUrl, this._params);
		this._bootStrapper.eSuccess.addListener(this._bootStrapSuccess, this);
		this._bootStrapper.eFailure.addListener(this._bootStrapFailure, this);
		this._bootStrapper.load();

		// init AspenController with the 'site' we have
		AspenController.init({
			dt : this._params.dt,
			context : this._params.aspenContext || this._params.context,
			site : this._params.site
		});
	},

	/**
	 * Calculate the Mapping file url
	 *
	 * @param {String} site - CVP 'site' value for Turner brand/property.
	 * @return {String} The mapping url
	 */
	_calculateMappingUrl : function (site) {
		if (Utils.empty(site))
			return null;

		// Default to always expecting JSON
		// TODO We may want to look for xml by default, and fallback to JSONP if that fails
		var url = Utils.replaceExtension(App.MAPPING_FILE, "xml", "json");

		if (!this._params.dt)
			url = Utils.template(App.MAPPING_PATH, site) + url;
		return url;
	},

	/**
	 * At this point, the following should true:
	 * - the container is loaded and parsed
	 * - the config is loaded and parsed
	 * - any other initially required dependencies are loaded
	 *     i.e., FW, Omniture, UI-related tools, etc..
	 *
	 * @param {Event} event - Triggered on BootStrapper success
	 */
	_bootStrapSuccess : function (event)
	{
		if (!event || !event.data) {
			_logger.error("MainController", "Unable to retrieve config data - failing.");
			this._bootStrapFailure();
			return;
		}

		// TODO validate config data
		this._configData = event.data;
		_logger.debug("MainController", "_bootStrapSuccess");

		// clean config data
		var defaults = {
			autostart : false,
			controls : true,
			muted : false
		};

		var config = this._configData.configInfo;

		if (config)
		{
			// convert strings to booleans
			Utils.each(defaults, function (key, defaultValue) {
				config[key] = Utils.isFlagActive(config[key], defaultValue);
			});

			// convert volume value from "0".."100" to 0..1
			if ('volume' in config)
			{
				var volume = Utils.toInt(config.volume) / 100;
				if (isFinite(volume))
					config.volume = Math.min( Math.max( volume, 0 ), 1 );
			}
		}

		// apply param overrides to configs
		var params = this._params;
		if ('autostart' in params)
			this._configData.configInfo.autostart = params.autostart;
		if ('controls' in params)
			this._configData.configInfo.controls = params.controls;
		if ('iosAirplay' in params)
			this._configData.configInfo.iosAirplay = params.iosAirplay;

		this._playerController = new PlayerInstance(this._videoBegetter, {
			params : this._params,
			configData : this._configData
		});


		this._aspenTracking = new AspenTracking({
			playerController : this._playerController,
			cvpElementId : this.getCurrentPlayer()
		});

		this._trackingManager = new TrackingManager({
			playerController : this._playerController
		});

		this.setTrackingInterval(this._configData.trackingPolicy.interval);

		this._cms = new CMS();
		this._cms.setDataUrl(this._configData.configInfo.dataSrc);
		this._cms.setMediaUrl(this._configData.configInfo.mediaSrc);

		this._listen(true);

		(function (configData) {

			var ad = configData && configData.adPolicy && configData.adPolicy.ads || {};
			var config = configData.configInfo || {};
			var container = configData.containerInfo || {};
			var elementData = {
				cvpElementId : container.elementName,
				adApi : ad.apiUrl,
				adServerRootUrl : ad.adServerRootUrl,
				adNetworkId : ad.adNetworkId,
				adVideoNetworkId : ad.adVideoNetworkId,
				adVideoAssetId : ad.adVideoAssetId,
				adSection : ad.adSection,
				adPlayerProfile : ad.adPlayerProfile,
				renderersUrl : ad.renderersUrl,
				sensitiveFallbackId : ad.sensitiveFallbackId,
				adLiveContentDuration : ad.liveDuration,
				mediaSrc : config.mediaSrc,
				dataSrc : config.dataSrc,
				adPolicyUrl : container.adPolicySrc
			};

			AspenController.send('appConfig', {
				playerConfigUrl : configData.appConfig && configData.appConfig.configUrl,
				containerName : document.URL
			});

			AspenController.send('elementConfig', elementData);

		}).call(this, this._configData);

		this.ePlayerLoaded.dispatch();

		this._bootStrapComplete();
	},

	_listen : function (bool) {
		if (this._isListening === bool) return;
		this._isListening = bool;
		var method = bool ? 'addListener' : 'removeListener';

		this._playerController.eRendered[method](this._onRendered, this);
		this._playerController.eContentMetadata[method](this._onContentMetadata, this);
		this._playerController.eContentBegin[method](this._onContentBegin, this);
		this._playerController.eContentPlay[method](this._onContentPlay, this);
		this._playerController.eContentPause[method](this._onContentPause, this);
		this._playerController.eContentEnd[method](this._onContentEnd, this);
		this._playerController.eContentComplete[method](this._onContentComplete, this);
		this._playerController.eContentPlayhead[method](this._onContentPlayhead, this);
		this._playerController.eContentBuffering[method](this._onContentBuffering, this);
		this._playerController.eContentQueue[method](this._onContentQueue, this);
		this._playerController.eContentQueueAutoplay[method](this._onContentQueueAutoplay, this);
		this._playerController.eContentVolume[method](this._onContentVolume, this);
		this._playerController.eContentError[method](this._onContentError, this);
		this._playerController.eContentResize[method](this._onContentResize, this);

		this._playerController.eAdPlayhead[method](this._onAdPlayhead, this);
		this._playerController.eAdPlay[method](this._onAdPlay, this);
		this._playerController.eAdEnd[method](this._onAdEnd, this);
		this._playerController.eAdError[method](this._onAdError, this);
		this._playerController.eAdSensitive[method](this._onAdSensitive, this);


		this._trackingManager.eTrackingAdStart[method](this._onTrackingAdStart, this);
		this._trackingManager.eTrackingAdComplete[method](this._onTrackingAdComplete, this);
		this._trackingManager.eTrackingAdProgress[method](this._onTrackingAdProgress, this);
		this._trackingManager.eTrackingAdCountdown[method](this._onTrackingAdCountdown, this);
		this._trackingManager.eTrackingAdClick[method](this._onTrackingAdClick, this);

		this._trackingManager.eTrackingContentPlay[method](this._onTrackingContentPlay, this);
		this._trackingManager.eTrackingContentBegin[method](this._onTrackingContentBegin, this);
		this._trackingManager.eTrackingContentProgress[method](this._onTrackingContentProgress, this);
		this._trackingManager.eTrackingContentEnd[method](this._onTrackingContentEnd, this);
		this._trackingManager.eTrackingContentComplete[method](this._onTrackingContentComplete, this);
		this._trackingManager.eTrackingContentReplay[method](this._onTrackingContentReplay, this);
		this._trackingManager.eTrackingContentSeek[method](this._onTrackingContentSeek, this);
		this._trackingManager.eTrackingContentSeekEnd[method](this._onTrackingContentSeekEnd, this);
		// this._trackingManager.eTrackingContentSegmentPlay[method](this._onTrackingContentSegmentPlay, this);
		// this._trackingManager.eTrackingContentSegmentComplete[method](this._onTrackingContentSegmentComplete, this);

		this._trackingManager.eTrackingFullscreen[method](this._onTrackingFullscreen, this);
		this._trackingManager.eTrackingMuted[method](this._onTrackingMuted, this);
		this._trackingManager.eTrackingPaused[method](this._onTrackingPaused, this);


		this._cms.eRequestCompleted[method](this._onCmsRequestCompleted, this);
	},

	/**
	 * Catastrophic failure, some required dependencies were not loaded
	 * Send an error event back to the page
	 */
	_bootStrapFailure : function()
	{
		_logger.debug("MainController", "_bootStrapFailure");
		this.ePlayerLoadError.dispatch();
		this._bootStrapComplete();
	},

	_bootStrapComplete : function ()
	{
		_logger.debug("MainController", "_bootStrapComplete");
		this._bootStrapper.eSuccess.removeListener(this._bootStrapSuccess, this);
		this._bootStrapper.eFailure.removeListener(this._bootStrapFailure, this);
		this._bootStrapper = null;
	},

	render : function(containerElement)
	{
		this._playerController.render(containerElement);
	},

	remove : function()
	{
		this._playerController.remove();
	},

	dispose : function ()
	{
		// this._domObserver.dispose();
		// this._domObserver = null;
		this._videoBegetter.dispose();
		this._videoBegetter = null;
		this._listen(false);
		this._trackingManager.dispose();
		this._trackingManager = null;
		this._aspenTracking.dispose();
		this._aspenTracking = null;
		this._playerController.dispose();
		this._playerController = null;
		this._cms = null;
	},

	_onRendered : function()
	{
		var contentId = this._params.contentId;
		if (!Utils.isNull(contentId))
		{
			_logger.debug("MainController", "Config property 'contentId' was provided with value '" + contentId + "'. Initiating play.");
			this.playContentWithId(contentId);
		}

		this.ePlayerRendered.dispatch();
	},

	playContentWithId : function(id, options)
	{
		if (!Utils.isString(id)) {
			_logger.warn("MainController", "playContentWithId", "A content ID is required.");
			return;
		}

		_logger.debug("MainController", "playContentWithId", id);

		// Attempt to prime element now, because the content xml request
		// is asynchronous and the response will be outside the scope of
		// the user action event -- if there was one; otherwise, this fails.
		this._videoBegetter.prime();

		var index = this._getNextIndexForId(id);
		var key = id + "|" + index;
		_logger.debug("MainController", "playContentWithId", "key", key);
		this._loadingEntryQueue[key] = { play : true, additionalParams : options };

		// starts ajax request for video xml, eventually calling play()
		// (but it wouldn't play on user action without the shim above)
		this._cms.addContentId(key);
	},

	// playFromObject({
	// 	id: 'tvcnn',
	// 	url: '',
	// 	metadata: {
	// 		files: '<groupFiles id="AkamaiHDN2" type="hdn2"><file>http://turnerhd-f.akamaihd.net/z/tvecnn_1@135347/manifest.f4m</file></groupFiles>',
	// 		isLive: true,
	// 		headline: 'CNN News'
	// 	}
	// },
	// {
	// 	accessToken: 'blah',
	// 	accessTokenType: 'Adobe'
	// });
	playFromObject : function (object, options)
	{
		if (!Utils.isString(object.url)) {
			_logger.warn("MainController", "playFromObject", "The property 'url' is required of the object parameter.");
			this.eContentEntryLoadError.dispatch(object.id, "Invalid object configuration", App.ERROR_CMS_PARSE, false);
			return;
		}

		var id = object.id || object.url;

		_logger.debug("MainController", "playFromObject", id);

		// Attempt to prime element now, because the content xml request
		// is asynchronous and the response will be outside the scope of
		// the user action event -- if there was one; otherwise, this fails.
		this._videoBegetter.prime();

		var index = this._getNextIndexForId(id);
		var key = id + "|" + index;

		_logger.debug("MainController", "playFromObject", "key", key);
		this._loadingEntryQueue[key] = { play : true, additionalParams : options };

		this._cms.addContentObject(key, object);
	},

	queueFromObject : function (object, options)
	{
		var id = object.id || object.url;

		_logger.debug("MainController", "queueFromObject", id);

		var index = this._getNextIndexForId(id);
		var key = id + "|" + index;

		_logger.debug("MainController", "queueFromObject", "key", key);
		this._loadingEntryQueue[key] = { play : false, additionalParams : options };

		this._cms.addContentObject(key, object);
	},

	playNextInQueue : function ()
	{
		this._playerController.playNextInQueue();
	},

	replay : function ()
	{
		this._playerController.replay();
	},

	pause : function()
	{
		this._playerController.pause();
	},

	resume : function()
	{
		this._playerController.resume();
	},

	stop : function ()
	{
		this._playerController.stop();
	},

	queueContentWithId : function(id, options /*, index*/)
	{
		// TODO Implement `index`
		_logger.debug("MainController", "queueContentWithId", id);
		var index = this._getNextIndexForId(id);
		var key = id + "|" + index;
		_logger.debug("MainController", "queueContentWithId", "key", key);
		this._loadingEntryQueue[key] = { play : false, additionalParams : options };
		this._cms.addContentId(key);
	},

	dequeueContentWithId : function(id)
	{
		this._playerController.dequeue(id);
	},

	emptyQueue : function ()
	{
		this._playerController.emptyQueue();
	},

	setQueueAutoplay : function (autoplay)
	{
		this._playerController.setQueueAutoplay(autoplay);
	},

	getQueue : function ()
	{
		return this._playerController.getQueue();
	},

	seek : function(seconds)
	{
		this._playerController.seek(seconds);
	},

	resize : function(width, height)
	{
		this._playerController.resize(width, height);
	},

	goFullscreen : function()
	{
		this._playerController.goFullscreen();
	},

	setVolume : function(v)
	{
		this._playerController.setVolume(v);
	},

	getVolume : function()
	{
		return this._playerController.getVolume();
	},

	mute : function()
	{
		this._playerController.mute();
	},

	unmute : function()
	{
		this._playerController.unmute();
	},

	isMuted : function ()
	{
		return this._playerController.isMuted();
	},

	getContentEntry : function (id)
	{
		if (!id || !Utils.isString(id))
			throw new Error("getContentEntry() requires a content ID");

		var catalogEntry = this._cms.getContentId(id);

		if (!catalogEntry)
			return "";

		return JsonConverter.xmlToJson(catalogEntry.getXML());
	},

	getContentEntryObject : function (id)
	{
		if (!id || !Utils.isString(id))
			throw new Error("getContentEntryObject() requires a content ID");

		var catalogEntry = this._cms.getContentId(id);

		if (!catalogEntry)
			return "";

		return JsonConverter.xmlToObject(catalogEntry.getXML());
	},

	setAdSection : function(ssid)
	{
		this._playerController.setAdSection(ssid);
	},

	setAdKeyValue : function(key, value)
	{
		this._playerController.setAdKeyValue(key, value);
	},

	getAdKeyValues : function() {
		return this._playerController.getAdKeyValues();
	},

	clearAdKeyValues : function() {
		this._playerController.clearAdKeyValues();
	},

	setTrackingInterval : function (interval) {
		this._trackingManager.setTrackingInterval(interval);
	},

	setDataSrc : function (src) {
		this._cms.setDataUrl(src);
	},

	reportAnalytics : function (eventName, data) {
		AspenController.send(eventName, data);
	},

	getCurrentPlayer : function() {
		return this._configData.containerInfo.elementName;
	},

	_onContentMetadata : function(contentId /* , duration, width, height */)
	{
		AspenController.send('videoConfig', {
			cvpElementId : this.getCurrentPlayer(),
			videoXmlJson : this.getContentEntryObject(contentId)
		});

		this.eContentMetadata.dispatch.apply(this.eContentMetadata, arguments);
	},

	_onContentBegin : function()
	{
		this.eContentBegin.dispatch.apply(this.eContentBegin, arguments);
	},

	_onContentPlay : function()
	{
		this.eContentPlay.dispatch.apply(this.eContentPlay, arguments);
	},

	_onContentPause : function()
	{
		this.eContentPause.dispatch.apply(this.eContentPause, arguments);
	},

	_onContentEnd : function()
	{
		this.eContentEnd.dispatch.apply(this.eContentEnd, arguments);
	},

	_onContentComplete : function()
	{
		this.eContentComplete.dispatch.apply(this.eContentComplete, arguments);
	},

	_onContentPlayhead : function()
	{
		this.eContentPlayhead.dispatch.apply(this.eContentPlayhead, arguments);
	},

	_onContentBuffering : function()
	{
		this.eContentBuffering.dispatch.apply(this.eContentBuffering, arguments);
	},

	_onContentQueue : function()
	{
		this.eContentQueue.dispatch.apply(this.eContentQueue, arguments);
	},

	_onContentQueueAutoplay : function()
	{
		this.eContentQueueAutoplay.dispatch.apply(this.eContentQueueAutoplay, arguments);
	},

	_onContentVolume : function()
	{
		this.eContentVolume.dispatch.apply(this.eContentVolume, arguments);
	},

	_onContentError : function(contentId, errorMessage)
	{
		AspenController.send('error', {
			cvpElementId : this.getCurrentPlayer(),
			contentId : contentId,
			videoContentCount : undefined,
			errorCode : undefined,
			errorMessage : errorMessage
		});

		this.eContentError.dispatch.apply(this.eContentError, arguments);
	},

	_onContentResize : function()
	{
		this.eContentResize.dispatch.apply(this.eContentResize, arguments);
	},

	_onAdPlayhead : function()
	{
		this.eAdPlayhead.dispatch.apply(this.eAdPlayhead, arguments);
	},

	_onAdPlay : function()
	{
		this.eAdPlay.dispatch.apply(this.eAdPlay, arguments);
	},

	_onAdEnd : function()
	{
		this.eAdEnd.dispatch.apply(this.eAdEnd, arguments);
	},

	_onAdError : function(/* errorMessage */)
	{
		// AspenController.send('error', {
		// 	cvpElementId: this.getCurrentPlayer(),
		// 	contentId: undefined,
		// 	videoContentCount: undefined,
		// 	errorCode: undefined,
		// 	errorMessage: errorMessage
		// });

		this.eAdError.dispatch.apply(this.eAdError, arguments);
	},

	_onAdSensitive : function()
	{
		this.eAdSensitive.dispatch.apply(this.eAdSensitive, arguments);
	},


	_onTrackingAdStart : function()
	{
		this.eTrackingAdStart.dispatch.apply(this.eTrackingAdStart, arguments);
	},

	_onTrackingAdComplete : function()
	{
		this.eTrackingAdComplete.dispatch.apply(this.eTrackingAdComplete, arguments);
	},

	_onTrackingAdProgress : function()
	{
		this.eTrackingAdProgress.dispatch.apply(this.eTrackingAdProgress, arguments);
	},

	_onTrackingAdCountdown : function()
	{
		this.eTrackingAdCountdown.dispatch.apply(this.eTrackingAdCountdown, arguments);
	},

	_onTrackingAdClick : function()
	{
		this.eTrackingAdClick.dispatch.apply(this.eTrackingAdClick, arguments);
	},

	_onTrackingContentPlay : function()
	{
		this.eTrackingContentPlay.dispatch.apply(this.eTrackingContentPlay, arguments);
	},

	_onTrackingContentBegin : function()
	{
		this.eTrackingContentBegin.dispatch.apply(this.eTrackingContentBegin, arguments);
	},

	_onTrackingContentProgress : function()
	{
		this.eTrackingContentProgress.dispatch.apply(this.eTrackingContentProgress, arguments);
	},

	_onTrackingContentEnd : function()
	{
		this.eTrackingContentEnd.dispatch.apply(this.eTrackingContentEnd, arguments);
	},

	_onTrackingContentComplete : function()
	{
		this.eTrackingContentComplete.dispatch.apply(this.eTrackingContentComplete, arguments);
	},

	_onTrackingContentReplay : function()
	{
		this.eTrackingContentReplay.dispatch.apply(this.eTrackingContentReplay, arguments);
	},

	_onTrackingContentSeek : function()
	{
		this.eTrackingContentSeek.dispatch.apply(this.eTrackingContentSeek, arguments);
	},

	_onTrackingContentSeekEnd : function()
	{
		this.eTrackingContentSeekEnd.dispatch.apply(this.eTrackingContentSeekEnd, arguments);
	},

	_onTrackingFullscreen : function()
	{
		this.eTrackingFullscreen.dispatch.apply(this.eTrackingFullscreen, arguments);
	},

	_onTrackingMuted : function()
	{
		this.eTrackingMuted.dispatch.apply(this.eTrackingMuted, arguments);
	},

	_onTrackingPaused : function()
	{
		this.eTrackingPaused.dispatch.apply(this.eTrackingPaused, arguments);
	},

	_removeLoadingQueueEntry : function(key) {
		delete this._loadingEntryQueue[key];
	},

	_onCmsRequestCompleted : function(contentId, requestId, index, errorMsg, errorCode)
	{
		_logger.debug("_onCmsRequestCompleted", arguments);

		var key = requestId + "|" + index;

		// load failure
		if (errorCode) {
			this.eContentEntryLoadError.dispatch(contentId, errorMsg, errorCode, false);
			this._removeLoadingQueueEntry(key);
			return;
		}

		var loadingQueueEntry = this._loadingEntryQueue[key];
		var catalogEntry = this._cms.getContentId(contentId + "|" + index);
		var inQueue = false;

		if (Utils.empty(loadingQueueEntry))
		{
			_logger.warn("onCmsRequestCompleted", "no entry for key '" + key + "' found in loading queue");
			return;
		}

		if (Utils.empty(catalogEntry))
		{
			_logger.error("onCmsRequestCompleted", "no catalogEntry for contentId '" + contentId + "'");
			this._removeLoadingQueueEntry(key);
			return;
		}

		_logger.debug("onCmsRequestCompleted", contentId, loadingQueueEntry.play);

		if (loadingQueueEntry.play)
		{
			// When play is called directly, empty the queue
			this._playerController.emptyQueue();
			this._playerController.play(catalogEntry, loadingQueueEntry.additionalParams);
		}
		else
		{
			this._playerController.queue(catalogEntry, loadingQueueEntry.additionalParams);
			inQueue = true;
		}

		this._removeLoadingQueueEntry(key);

		// load success
		if (errorCode === "") {
			this.eContentEntryLoad.dispatch(contentId, inQueue);
		}
	},

	// kind of like:  this._loadingEntryQueue.filter(hasContentId(contentId)).length + 1
	_getNextIndexForId : function(contentId)
	{
		var index = 0, p, q, id, n;
		for (p in this._loadingEntryQueue)
		{
			q = p.split("|");
			if (q && q.length > 1) {
				id = q[0];
				n = +(q[1]);
				if (id === contentId && !isNaN(n) && n >= index) {
					index = n + 1;
				}
			}
		}
		return index;
	}

});


// Export public APIs
module.exports = CVPManager;

},{"../core/app":8,"../cvp/ajax":10,"../cvp/customevent":16,"../cvp/log":28,"../cvp/util/jsonconverter":46,"../cvp/utils":50,"../vendor/Class":93,"./aspencontroller":59,"./bootstrapper":60,"./cms":61,"./player/instance":86,"./player/videobegetter":88,"./tracking/aspentracking":91,"./tracking/trackingmanager":92}],73:[function(require,module,exports){
/* html5/overwritemanager.js */

// Load dependent modules
var Class		   = require('../vendor/Class'),
	Utils		   = require('../cvp/utils'),
	Log			   = require('../cvp/log'),
	AdPolicyParser = require('./parsers/adpolicy');

var logger = Log.getLogger('OverWriteManager');

// The expected json data structure for overwrites
var model = {
	adPolicy : {
		ads : {
			apiUrl                : "",
			adManRootUrl          : "",
			adServerRootUrl       : "",
			adVideoRootUrl        : "",
			adVideoExtension      : "",
			additionalVideoSegVars: "",
			additionalSyncSegVars : "",
			adSection             : "",
			adNetworkId           : "",
			adVideoNetworkId      : "",
			adVideoAssetId        : "",
			adPlayerProfile       : "",
			renderersUrl          : "",
			externalSlots         : "",
			fallbackId            : "",
			sensitiveFallbackId   : "",
			adLiveContentDuration : "",
			adCuepointTimer       : "",
			attr : {
				type : ""
			}
		}
	}
};

/**
 * Manages configuration overwriting from all possible entry points
 */
var OverwriteManager = Class.extend({

	/**
	 * @param {Object} flashvars - Contains configuration data specified from the flashvars object
	 */
	init : function(flashvars) {
		logger.debug("init() flashvars: ", flashvars);
		this._flashvars = flashvars;
	},

	/**
	 * Perform configuration data overwrites and validation.
	 *
	 * @param {Object} data - The config object to make overwrite changes to
	 */
	overwrite : function(data) {

		// Get normalized data from all entry points and merge them into one object
		var merged = Utils.merge({},
					new FlashVarsEntryPoint(this._flashvars).getData());

		// Attempt to gather query params from the top-level window.
		// NOTE: Accessing window.top.location.href across domains throws an exception.
		try {
			merged = Utils.merge(merged,
				new QueryStringEntryPoint(window.top.location.href).getData());
		} catch (crossOriginException) {
			logger.debug("unable to access top window's query params: " + crossOriginException);
		}

		// Gather query params from *this* window, if we're in an iframe.
		if (window.self !== window.top) {
			merged = Utils.merge(merged,
				new QueryStringEntryPoint(window.location.href).getData());
		}

		logger.debug("merged:", Utils.merge({}, merged));

		// Perform value replacement for special values
		var validated = new ValueValidator().validate(merged);
		logger.debug("validated:", validated);

		// Convert json to xml
		var adXml = AdPolicyParser.jsonToXML(validated.adPolicy);
		logger.debug("adXml", adXml);

		// Feed new xml into parser to overwrite existing config values
		new AdPolicyParser().parseData(adXml, data.adPolicy);
	}

});

/**
 * Base class for configuration overwrite data sources
 */
var OverwriteEntryPoint = Class.extend({

	/**
	 * @returns {Object} Get the normalized entry point data as a simple object
	 */
	getData : function() {
		return this.normalize();
	},

	/**
	 * Format data in a known way
	 */
	normalize : function() {
		logger.error("Subclass must implement");
	},

	/**
	 * Copy properties from a data source object to a new
	 * object. This will <b>only</b> copy over properties that
	 * exist on the check object.
	 *
	 * @param source The data retrieved from the entry point
	 * @param check  The object to check against
	 *
	 * @return		 The new object only containing properties checked against a known data structure
	 */
	getNormalized : function(source, check) {
		var json = {};
		if (!source || !check)
			return json;

		for (var key in source)
		{
			if (source[key] != undefined
				&& check.hasOwnProperty(key))
			{
				if (Utils.isSimpleObject(source[key]))
					json[key] = this.getNormalized(source[key], check[key]);
				else
					json[key] = source[key];
			}
		}

		return json;
	}

});

/**
 * Deals with getting config xml overwrite data via query string parameters
 */
var QueryStringEntryPoint = OverwriteEntryPoint.extend({

	/**
	 * @param url The url to grab query string params from
     */
	init : function(url) {
		logger.debug("QueryStringEntryPoint - url: ", url);
		this._url = url;
		this._data = Utils.getQueryParams(this._url);
	},

	normalize : function() {

		// Properties that can be set via query string parameters
		var allowed = {
			adPolicy : {
				ads : {
					adNetworkId		 : this._data.adNetworkId,
					adVideoNetworkId : this._data.adVideoNetworkId,
					adServerRootUrl  : this._data.adServerRootUrl,
					attr			 : {}
				}
			}
		};

		return this.getNormalized(allowed, model);
	}

});

/**
 * Deals with getting config xml overwrite data via Flashvars
 */
var FlashVarsEntryPoint = OverwriteEntryPoint.extend({

	/**
	 * @param flashvars {Object} - contains configuration data specified from the flashvars object
     */
	init : function(flashvars) {

		this._data = {};

		var overwrite = flashvars[FlashVarsEntryPoint.FLASH_VAR_NAME];

		logger.debug("FlashVarsEntryPoint - overwrite: ", overwrite);

		if (!Utils.isEmpty(overwrite)) {

			this._data = {
				adPolicy : {
					ads : overwrite.ads.adServer
				}
			};

			if (!Utils.isEmpty(overwrite.ads.adServer.type)) {
				this._data.adPolicy.ads.attr = {
					type : overwrite.ads.adServer.type
				};
				delete overwrite.ads.adServer.type;
			}
		}
	},

	normalize : function() {
		return this.getNormalized(this._data, model)
	}

});

// Name of the FlashVar this entry point will look for the overwrite json data
FlashVarsEntryPoint.FLASH_VAR_NAME = "overwrite";

/**
 * Validate properties, replace any special values
 */
var ValueValidator = Class.extend({

	/**
	 * Perform value replacement and validation, then return the updated json data
	 * @param data The object to validate
	 * @returns {*|Object|{}} The object reference with property values validated and modified if necessary
     */
	validate : function (data) {

		validateAds(data.adPolicy);

		return data;
	}

});

function validateAds(data) {

	var json = { attr : {} };

	var DEV				= "dev",
		PROD			= "prod",
		NETWORK_ID_DEV	= "42448",
		NETWORK_ID_PROD = "48804",
		NONE			= "NONE",
		FREEWHEEL		= "FREEWHEEL";

	for (var name in data.ads)
	{
		var value = data.ads[name];

		switch (name)
		{
			case "attr":
				for (name in data.ads.attr) {
					value = data.ads.attr[name];
					if (value.toUpperCase() == FREEWHEEL || value.toUpperCase() == NONE)
						json.attr[name] = value.toUpperCase();
				}
				break;
			case "adNetworkId":
			case "adVideoNetworkId":
				var id;
				if (value == DEV)
					id = NETWORK_ID_DEV;
				else if (value == PROD)
					id = NETWORK_ID_PROD;
				else if (value == NETWORK_ID_DEV || value == NETWORK_ID_PROD)
					id = value;
				if (id)
					json["adNetworkId"] = json["adVideoNetworkId"] = id;
				break;
			case "adServerRootUrl":
				if (value == DEV)
					json[name] = "http://bea4.v.fwmrm.net/";
				break;
			default:
				json[name] = value;
		}
	}

	data.ads = json;
}

// Export public APIs
module.exports = OverwriteManager;

},{"../cvp/log":28,"../cvp/utils":50,"../vendor/Class":93,"./parsers/adpolicy":74}],74:[function(require,module,exports){
/* html5/parsers/adpolicy.js */

// Load dependent modules
var Parser = require('./generic');
var XMLUtils = require('../../cvp/util/xml');
var Utils = require('../../cvp/utils');
var PlayerConfig = require('../shared/playerconfig');
var Log = require('../../cvp/log');
var XML = require('../../cvp/util/xml');
var AdExt = require('../extensions/ad-kvps');

var logger = Log.getLogger('AdPolicyParser');

/**
 * Parse the param nodes from the config.
 * @param {Object} obj - the config object
 * @param {String} name - the config to populate
 * @param {String} value - the value for the name
 */
function parseParams(obj, name, value) {
	switch (name)
	{
		case "adApi":
			obj.apiUrl = value;
			break;
		case "adManRootUrl":
			obj.adManRootUrl = value;
			break;
		case "adServerRootUrl":
			obj.adServerRootUrl = value;
			break;
		case "adVideoRootUrl":
			obj.adVideoRootUrl = value;
			break;
		case "adVideoExtension":
			obj.adVideoExtension = value;
			break;
		case "additionalVideoSegvars":
			obj.additionalVideoSegVars = value;
			break;
		case "additionalSyncSegvars":
			obj.additionalSyncSegVars = value;
			break;
		case "adSection":
			obj.adSection = value;
			break;
		case "adNetworkId":
			obj.adNetworkId = value;
			break;
		case "adVideoNetworkId":
			obj.adVideoNetworkId = value;
			break;
		case "adVideoAssetId":
			obj.adVideoAssetId = value;
			break;
		case "adPlayerProfile":
			obj.adPlayerProfile = value;
			break;
		case "renderersUrl":
			obj.renderersUrl = value;
			break;
		case "externalSlots":
			obj.externalSlots = Utils.isFlagActive(value, obj.externalSlots);
			break;
		case "adLiveContentDuration":
			obj.liveDuration = value;
			break;

		default:
			obj[name] = value;
	}
}

function parseAttributes(obj, node) {
	if (node.hasAttributes)
	{
		if (!obj.attr)
			obj.attr = {};
		XMLUtils.assignAttributes(obj.attr, node.attributes);
	}
}

function findMapping(obj, nodeName) {
	var value = null;
	switch (nodeName)
	{
		case "adServer":
			value = obj.ads;
			break;
	}
	return value;
}

function parseKvps(obj, xml) {
    var xmlStr = XMLUtils.docToString(xml);
    obj.keyValues = AdExt.AdKeyValueConfigParser.parse(xmlStr); 
}

// TODO abstract this out for use in all parsers
function process(xml, objToAssign) {
	if (Utils.isNull(xml))
		return;

	var childNodes = xml.childNodes;
	var obj;

	for (var childNode, i = 0, endi = childNodes.length; i < endi; ++i)
	{
		childNode = childNodes[i];

		obj = findMapping(objToAssign, childNode.nodeName);

		if (!Utils.isNull(obj))
		{
			parseAttributes(obj, childNode);

			if (childNode.childNodes.length)
			{
				process(childNode, obj);
			}
		}
		else if (childNode.nodeName === "param")
		{
			parseParams(objToAssign,
				XMLUtils.getAttribute(childNode, "name"),
				XMLUtils.getAttribute(childNode, "value")
			);
		}
    else if (childNode.nodeName === "keyValuePairs") {
        parseKvps(objToAssign, childNode);
    }
	}
}


var AdPolicyParser = Parser.extend({

	setAdPolicyContext : function (context) {
		this.adPolicyContextName = context || "";
	},

	/**
	 * Parses the ad policy configuration xml
	 * @param xml The xml data to parse
	 * @param vo  [optional] parsed ad policy data to overwrite
     */
	parseData : function (xml, vo) {

		var adPolicy = Utils.extend({}, vo || PlayerConfig.adPolicy);

		var doc = xml.documentElement;

		// read "default" context
		var defaultNode = doc.getElementsByTagName("default");

		// process "default" context
		if (defaultNode.length !== 0) {
			process(defaultNode[0], adPolicy);
		}

		// read specified context
		var context = null;

		var contexts = doc.getElementsByTagName("context");

		if (vo)
			context = contexts[0];
		else {
			for (var i = 0, endi = contexts.length; i < endi; ++i)
			{
				if (XMLUtils.getAttribute(contexts[i], "name") === this.adPolicyContextName)
				{
					context = contexts[i];
					break;
				}
			}
		}

		// process specified context
		if (!Utils.isNull(context))
			process(context, adPolicy);

		logger.debug("adPolicy:", adPolicy);

		this._fireLoadedSuccess(adPolicy);
	}

});

/**
 * Converts a json object into an xml object which the parser
 * can use to overwrite data on the vo.
 *
 * @param {Object} json	The json object to translate
 *
 * @return {XML} The resulting xml with overwritten data
 */
AdPolicyParser.jsonToXML = function(json) {

	var xmlStr = "<policy>" +
					 "<context>" +
						 "<adServer/>" +
					 "</context>" +
				 "</policy>";

	var xml = XML.stringToDoc(xmlStr);

	var adServerNode = xml.getElementsByTagName("adServer")[0];

	var paramExcludes = ["attr"];

	for (var prop in json.ads) {

		if (paramExcludes.indexOf(prop) > -1)
			continue;

		var node = xml.createElement("param");
		var attr = xml.createAttribute("name");
		attr.nodeValue = prop;

		node.setAttributeNode(attr);
		attr = xml.createAttribute("value");
		attr.nodeValue = json.ads[prop];

		node.setAttributeNode(attr);
		adServerNode.appendChild(node);
	}

	if (json.ads.attr && json.ads.attr.type) {
		attr = xml.createAttribute("type");
		attr.nodeValue = json.ads.attr.type;
		adServerNode.setAttributeNode(attr);
	}

	return xml;
};


// Export public APIs
module.exports = AdPolicyParser;

},{"../../cvp/log":28,"../../cvp/util/xml":49,"../../cvp/utils":50,"../extensions/ad-kvps":70,"../shared/playerconfig":90,"./generic":78}],75:[function(require,module,exports){
/* html5/parsers/appconfig.js */

// Load dependent modules
var Parser = require('./generic');
var Request = require('../../cvp/request');


var AppConfigParser = Parser.extend({

	/**
	 * Overriding here to set the JSONP callback function
	 * @param {String} url - URL to request
	 * @returns {Promise} A Thenable to attach success/failure callbacks
	 */
	_createRequest : function (url) {
		return Request.getJSONP({
			url : url,
			jsonpCallback : "cvp_onAppConfigReceived"
		});
	},

	parseData : function(data) {
		var appConfig = data.appConfig;
		this._fireLoadedSuccess(appConfig);
	}

});


// Export public APIs
module.exports = AppConfigParser;

},{"../../cvp/request":36,"./generic":78}],76:[function(require,module,exports){
/* html5/parsers/config.js */

// Load dependent modules
var Parser = require('./generic');
var XMLUtils = require('../../cvp/util/xml');
var Utils = require('../../cvp/utils');
var PlayerConfig = require('../shared/playerconfig');


/**
 * Parse the param nodes from the config.
 * @param {Object} obj - the config object
 * @param {String} name - the config to populate
 * @param {String} value - the value for the name
 */
function parseParams(obj, name, value) {
	switch (name)
	{
		default:
			obj[name] = value;
	}
}

function parseAttributes(obj, node)
{
	if (node.hasAttributes)
	{
		if (!obj.attr)
			obj.attr = {};
		XMLUtils.assignAttributes(obj.attr, node.attributes);
	}
}

function findMapping(obj, nodeName) {
	var value = null;
	switch (nodeName)
	{
		case "akamai2":
			obj.akamai2 = {};
			value = obj.akamai2;
		break;
		case "akamaiSpe":
			obj.akamaiSpe = {};
			value = obj.akamaiSpe;
		break;
		case "tokenAuth":
			obj.token = {};
			value = obj.token;
		break;
	}
	return value;
}

function process(xml, objToAssign) {
	if (Utils.isNull(xml))
		return;

	var childNodes = xml.childNodes;
	var obj;

	for (var childNode, i = 0, endi = childNodes.length; i < endi; ++i) {
		childNode = childNodes[i];
		obj = findMapping(objToAssign, childNode.nodeName);
		if (!Utils.isNull(obj))
		{
			parseAttributes(obj, childNode);
			if (childNode.childNodes.length)
			{
				process(childNode, obj);
			}
		}
		else if (childNode.nodeName === "param")
		{
			parseParams(objToAssign,
				XMLUtils.getAttribute(childNode, "name"),
				XMLUtils.getAttribute(childNode, "value")
			);
		}
	}
}


var ConfigParser = Parser.extend({

	setPlayerInstance : function (playerInstance) {
		this.playerInstanceName = playerInstance || "";
	},

	parseData : function(xml) {
		var configInfo = Utils.extend({}, PlayerConfig.configInfo);
		var doc = xml.documentElement;

		// read "default" context
		var defaultNode = doc.getElementsByTagName("default");

		// process "default" context
		if (defaultNode.length > 0) {
			process(defaultNode[0], configInfo);
		}

		// find specified player context
		var player = null;
		var players = doc.getElementsByTagName("player");
		var node = null;

		while (players.length) {
			node = players[players.length - 1];

			if (XMLUtils.getAttribute(node, "name") === this.playerInstanceName)
			{
				player = node.cloneNode(true);
			}

			doc.removeChild(node);
		}

		// process specified player context
		if (!Utils.isNull(player))
		{
			process(player, configInfo);
		}

		Utils.print(configInfo, "configInfo");
		// log.info('configInfo', configInfo);

		this._fireLoadedSuccess(configInfo);
	}

});


// Export public APIs
module.exports = ConfigParser;

},{"../../cvp/util/xml":49,"../../cvp/utils":50,"../shared/playerconfig":90,"./generic":78}],77:[function(require,module,exports){
/* html5/parsers/container.js */

// Load dependent modules
var Parser = require('./generic');
var XMLUtils = require('../../cvp/util/xml');
var Utils = require('../../cvp/utils');
var PlayerConfig = require('../shared/playerconfig');


function parsePolicies(policiesNode, targetObj) {
	var adPolicySrc = XMLUtils.getParamValue(policiesNode, "adPolicySrc");
	var adPolicyContext = XMLUtils.getParamValue(policiesNode, "adPolicyContext");
	var trackingPolicySrc = XMLUtils.getParamValue(policiesNode, "trackingPolicySrc");
	var trackingPolicyContext = XMLUtils.getParamValue(policiesNode, "trackingPolicyContext");

	if (!Utils.empty(adPolicySrc)) {
		targetObj.adPolicySrc = adPolicySrc;
	}
	if (!Utils.empty(adPolicyContext)) {
		targetObj.adPolicyContext = adPolicyContext;
	}
	if (!Utils.empty(trackingPolicySrc)) {
		targetObj.trackingPolicySrc = trackingPolicySrc;
	}
	if (!Utils.empty(trackingPolicyContext)) {
		targetObj.trackingPolicyContext = trackingPolicyContext;
	}
}


var ContainerParser = Parser.extend({

	setParams : function (params) {
		this._params = params;
	},

	parseData : function (xml) {
		var i = 0;
		var endi = 0;
		var docElement = xml.documentElement;
		var contextName = this._params.context || "";
		var elementName = this._params.element || "";
		var contexts = null;
		var context = null;
		var policiesNodes = [];
		var containerInfo = Utils.extend({}, PlayerConfig.containerInfo);

		// process <default> node
		var defaultNodes = docElement.getElementsByTagName("default");

		if (defaultNodes.length)
		{
			policiesNodes = defaultNodes[0].getElementsByTagName("policies");

			if (policiesNodes.length)
				parsePolicies(policiesNodes[0], containerInfo);
		}

		// save defaults, override with context [next]
		containerInfo.elementName = elementName;

		// process <context> nodes
		contexts = docElement.getElementsByTagName("context");

		if (contexts.length)
		{
			for (endi = contexts.length; i < endi; ++i)
			{
				if (XMLUtils.getAttribute(contexts[i], "name") === contextName)
				{
					context = contexts[i];
					break;
				}
			}

			if (context)
			{
				// TODO this should be filtered by @type="cvp"
				var elementNodes = context.getElementsByTagName("element");
				var elementNode = null;
				var element = null;

				for (i = 0, endi = elementNodes.length; i < endi; ++i)
				{
					elementNode = elementNodes[i];

					if (XMLUtils.getAttribute(elementNode, "id") === elementName)
					{
						element = elementNode;
						break;
					}
				}

				if (!element)
					element = elementNodes[0];

				elementName = XMLUtils.getAttribute(element, "id");
				policiesNodes = element.getElementsByTagName("policies");

				if (policiesNodes.length) {
					parsePolicies(policiesNodes[0], containerInfo);
				}

				var playerNodes = element.getElementsByTagName("player");
				var playerName = XMLUtils.getAttribute(playerNodes[playerNodes.length - 1], "playerInstance");

				if (Utils.empty(playerName))
					playerName = contextName;

				containerInfo.playerInstance = playerName;
			}

			// override defaults, if provided in context
			if (!Utils.empty(elementName)) {
				containerInfo.elementName = elementName;
			}
		}

		Utils.print(containerInfo, "containerInfo");
		// log.info('containerInfo', containerInfo);

		this._fireLoadedSuccess(containerInfo);
	}

});


// Export public APIs
module.exports = ContainerParser;

},{"../../cvp/util/xml":49,"../../cvp/utils":50,"../shared/playerconfig":90,"./generic":78}],78:[function(require,module,exports){
/* html5/parsers/generic.js */

// Load dependent modules
var Class = require('../../vendor/Class');
var Event = require('../../cvp/customevent');
var Utils = require('../../cvp/utils');
var Request = require('../../cvp/request');
var Log = require('../../cvp/log');


var log = Log.getLogger();

var Parser = Class.extend({

	init : function() {
		// binding methods
		this.onLoaded = Utils.bind(this.onLoaded, this);
		this.onLoadError = Utils.bind(this.onLoadError, this);

		// Parsing events
		this.eParseCompleted = new Event();
		this.eParseError = new Event();
	},

	/**
	 * Load and parse the xml
	 * @param {String} url - The url of the xml to load and parse
	 */
	parse : function(url) {
		log.debug("Parser", "Loading the file to parse", url);
		this.load(url);
	},

	load : function(url) {
		this._createRequest(url).then(this.onLoaded, this.onLoadError);
	},

	_createRequest : function (url) {
		return Request.getXML({
			url : url
		});
	},

	onLoaded : function(data) {
		log.debug("Parser", "Data loaded");
		this.parseData(data);
	},

	onLoadError : function(error) {
		log.error("Parser", "onLoadError:", error);
		this._fireLoadedFailure();
	},

	parseData : function(/* xml */) {
		// stub
		log.error("Parser", "`parseData` requires implementation");
	},

	_fireLoadedSuccess : function()
	{
		log.debug("Parser", "successfully Loaded asset");
		this.eParseCompleted.dispatch.apply(this.eParseCompleted, arguments);
	},

	_fireLoadedFailure : function()
	{
		log.error("Parser", "failed to Load asset");
		this.eParseError.dispatch.apply(this.eParseError, arguments);
	}

});


// Export public APIs
module.exports = Parser;

},{"../../cvp/customevent":16,"../../cvp/log":28,"../../cvp/request":36,"../../cvp/utils":50,"../../vendor/Class":93}],79:[function(require,module,exports){
/* html5/parsers/mapping.js */

// Load dependent modules
var Parser = require('./generic');
var Utils = require('../../cvp/utils');
var Request = require('../../cvp/request');


var MappingParser = Parser.extend({

	// Overriding here to set the JSONP callback function
	_createRequest : function (url) {
		return Request.getJSONP({
			url : url,
			jsonpCallback : "cvp_onMappingReceived"
		});
	},

	setMappingProfile : function (context) {
		this.profile = context;
	},

	parseData : function (data) {
		var mapping, mappings, map;

		if (Utils.undef(data) || Utils.undef(data.mappings)) {
			this.eParseError.dispatch();
			return;
		}

		// Find the mapping for the passed in profile
		mappings = data.mappings;

		if (Utils.empty(this.profile)) {
			this.onLoadError(null, "No mapping profile provided", "Invalid mapping profile");
			return;
		}

		for (map in mappings) {
			if (mappings[map].profile === this.profile) {
				mapping = mappings[map];  // grabbing the last one that matches
				// break;  // grabbing the first one that matches
			}
		}

		if (Utils.empty(mapping)) {
			this.onLoadError(null, "No mapping profile '" + this.profile + "' found", "Invalid mapping profile");
			return;
		}

		this._fireLoadedSuccess(mapping);
	}

});


// Export public APIs
module.exports = MappingParser;

},{"../../cvp/request":36,"../../cvp/utils":50,"./generic":78}],80:[function(require,module,exports){
/* html5/parsers/trackingpolicy.js */

// Load dependent modules
var Parser = require('./generic');
var XML = require('../../cvp/util/xml');
var Utils = require('../../cvp/utils');
var PlayerConfig = require('../shared/playerconfig');


/**
 * Parse the param nodes from the config.
 * @param {Object} obj - the config object
 * @param {String} name - the config to populate
 * @param {String} value - the value for the name
 */
function parseParams(obj, name, value) {
	switch (name)
	{
		case "interval":
			obj.interval = value;
		break;
		// default:
		// 	obj[name] = value;
	}
}

function process(xml, objToAssign) {
	if (Utils.isNull(xml))
		return;

	var childNodes = XML.getChildElements(xml);

	for (var childNode, i = 0, endi = childNodes.length; i < endi; ++i)
	{
		childNode = childNodes[i];

		if (childNode.nodeName === "param")
		{
			parseParams(objToAssign,
				XML.getAttribute(childNode, "name"),
				XML.getAttribute(childNode, "value")
			);
		}
	}
}


var TrackingPolicyParser = Parser.extend({

	setTrackingPolicyContext : function (context) {
		this.trackingPolicyContextName = context || "";
	},

	parseData : function(xml) {
		var trackingPolicy = Utils.extend({}, PlayerConfig.trackingPolicy);
		var doc = xml.documentElement;

		// read "default" context
		var defaultNodes = doc.getElementsByTagName("default");

		// process "default" context
		if (defaultNodes.length)
		{
			process(defaultNodes[0], trackingPolicy);
		}

		// read specified context
		var context = null;
		var contextNodes = doc.getElementsByTagName("context");

		for (var node, i = 0, endi = contextNodes.length; i < endi; ++i)
		{
			node = contextNodes[i];

			if (XML.getAttribute(node, "name") === this.trackingPolicyContextName)
			{
				context = node;
				break;
			}
		}

		// process specified context
		if (context)
		{
			process(context, trackingPolicy);
		}

		Utils.print(trackingPolicy, "trackingPolicy");
		// log.info('trackingPolicy', trackingPolicy);

		this._fireLoadedSuccess(trackingPolicy);
	}

});


// Export public APIs
module.exports = TrackingPolicyParser;

},{"../../cvp/util/xml":49,"../../cvp/utils":50,"../shared/playerconfig":90,"./generic":78}],81:[function(require,module,exports){
/* html5/player/baseplayer.js */

// Load dependent modules
var Class = require('../../vendor/Class');
var Event = require('../../cvp/customevent');
var Utils = require('../../cvp/utils');
var ConditionalTask = require('../../cvp/util/conditionaltask');
var Log = require('../../cvp/log');


var log = Log.getLogger();

var BasePlayer = Class.extend({

	init : function(options) {
		this._config = options.config;

		this.options = Utils.extend({
			containerElement : '',
			elementId : '',
			controls : true,
			width : 0,
			height : 0
		}, options);


		this.elementId = this.options.elementId;
		if (Utils.empty(this.elementId))
		{
			log.error("Invalid element id...exiting");
			throw new Error("Invalid element id");
		}

		this.playerOptions = {};

		this.width = this.options.width;
		this.height = this.options.height;

		this.element = null;
		this._isRendered = false;

		this.position = {
			top : null,
			right : null,
			bottom : null,
			left : null
		};

		this.eRendered = new Event();			// After the markup for the media element has been rendered
		this.eContentMetadata = new Event();	// We know the duration, width and height of the content
		this.eContentPlay = new Event();		// After any prerolls, before the video starts playing
		this.eContentPause = new Event();		//
		this.eContentEnd = new Event();			// After the video has completed, before any postrolls have been played
		this.eContentPlayhead = new Event();	//
		this.eContentBuffering = new Event();	//
		this.eContentVolume = new Event();		//
		this.eContentError = new Event();		//
		this.eContentResize = new Event();		//

		this.eContentSeek = new Event();	// emit during user-initiated scrub of content
		this.eContentSeekEnd = new Event();	// emit after user-initiated scrub of content
		this.eFullscreenChange = new Event();		// emit from W3/WebKit/Moz fullscreen-change events
		this.eFullscreenError = new Event();		// emit from W3/WebKit/Moz fullscreen error events
		this.ePlayerMuted = new Event();			// emit when volume changes to/from 0
		this.ePlayerPaused = new Event();			// emit when user pauses or unpauses content
		this.ePlayerStopped = new Event();			// emit when stop() is called
	},

	embed : function (/*containerElement*/) {
		// Implement
	},

	render : function (containerElementId) {
		log.debug("Player", "render");

		this.containerElement = Utils.byId(containerElementId || this.options.containerElement);

		if (Utils.isNull(this.containerElement))
		{
			log.error("Container element could not be found...cannot render...exiting");
			throw new Error("Container could not be found");
		}

		var self = this;
		var condition = function () {
			return !Utils.isNull(Utils.byId(self.elementId));
		};
		var render = function () {
			self._isRendered = true;
			self.element = Utils.byId(self.elementId);
			self.fireRendered();
		};

		// empty out the container node we insert the player into
		if (this.containerElement.hasChildNodes())
			this.containerElement.innerHTML = "";

		this.embed(this.containerElement);

		if (condition()) {
			log.info("Player element was found; render immediately.");
			render();
		}
		else {
			log.info("Player element was not found; render eventually.");
			var task = new ConditionalTask(condition, render);
			task.start();
		}

	},

	rendered : function() {
		return this._isRendered;
	},

	show : function() {
		if (this.element)
		{
			if (this.position.left)
				this.element.style.left = this.position.left;
		}
	},

	hide : function() {
		if (this.element)
		{
			this.position.left = this.element.style.left;
			this.element.style.left = "-5000px";
		}
	},

	remove : function () {
		log.debug("Player", "remove");

		var element = this.containerElement;

		if (Utils.isNull(element)) {
			log.error("Container element could not be found .. cannot remove");
			throw new Error("Container element could not be found");
		}

		while (element.firstChild)
			element.removeChild(element.firstChild);

		if (element.parentNode)
			element.parentNode.removeChild(element);
	},

	dispose : function ()
	{
		this.removePlayerListeners();
		this.remove();
	},

	addPlayerListeners : function() {
	},

	removePlayerListeners : function() {
	},

	fireRendered : function() {
		this.eRendered.dispatch();
	},

	getContentId : function () {
		return this._catalogEntry ? this._catalogEntry.getId() : "";
	},

	firePlay : function() {
		this.eContentPlay.dispatch(this.getContentId(), "");
	},

	fireEnded : function() {
		this.eContentEnd.dispatch(this.getContentId());
	}

});


// Export public APIs
module.exports = BasePlayer;

},{"../../cvp/customevent":16,"../../cvp/log":28,"../../cvp/util/conditionaltask":42,"../../cvp/utils":50,"../../vendor/Class":93}],82:[function(require,module,exports){
/* html5/player/controller.js */

/* eslint no-inline-comments:0, no-multi-spaces:0 */

// Load dependent modules
var Class = require('../../vendor/Class');
var CVP = require('../../cvp/static');
var Utils = require('../../cvp/utils');
var CommandQueue = require('../../cvp/util/commandqueue');
var Event = require('../../cvp/customevent');
var Log = require('../../cvp/log');
var VideoPlayer = require('./videoplayer');
var AdServerProxy = require('../ads/adserverproxy');
/*
	var TokenService = require('html5/token/tokenservice');
*/


var _logger = Log.getLogger("PlayerController");
var NO_MEDIA = "";
var AD_MEDIA = "AD";
var CONTENT_MEDIA = "CONTENT";
var IDLE = 0;
var STOPPING = 1;
var PLAYING = 2;
var SHIMMING_PLAY = 'play';
var SHIMMING_REPLAY = 'replay';

var adBlockId = 0;

var PlayerController = Class.extend({
	init : function(videoBegetter, config)
	{
		this._videoBegetter = videoBegetter;
		this._autostart = 'autostart' in config.configInfo ? !!config.configInfo.autostart : false;
		var controls = 'controls' in config.configInfo ? !!config.configInfo.controls : true;

		this._commandQ = new CommandQueue();
		this._stoppingQ = new CommandQueue();

		var params = config.params;

		this._elementId = params.id;

		this._videoElement = videoBegetter.getElement();
		videoBegetter.setControls(controls);

		this._videoPlayer = new VideoPlayer(this._videoElement, {
			config : config,
			containerElement : "player_container",
			elementId : this._elementId,
			width : params.width,
			height : params.height,
			controls : controls,
			autostart : this._autostart
		});

		this._adProxy = new AdServerProxy(config.adPolicy);

/*
		this._tokenService = new TokenService({
			config: config
		});
		//this._tokenService.ePlayRequest.addListener(this._onContentPlay, this);
		this._tokenService.eLoadToken.addListener(this._playContent, this);
*/

		this._listen(true);

		this._state = IDLE;
		this._listeningForVideoElementActivation = false;
		this._listeningForStart = false;
		this._currentContentId = '';
		this._contentQueue = [];
		this._seekTo = 0;
		this._triggeredAd = false;
		this._contentStarted = false;
		this._fullscreen = false;
		this._queueAutoplay = true;
		this._metadataFired = false;
		this._playEventQueue = [];

		this.eRendered = new Event();
		this.eFullscreenChange = new Event();
		this.eFullscreenError = new Event();
		this.ePlayerPaused = new Event();
		this.ePlayerMuted = new Event();
		this.ePlayerStopped = new Event();

		this.eContentMetadata = new Event();
		this.eContentBegin = new Event();
		this.eContentPlay = new Event();
		this.eContentPause = new Event();
		this.eContentEnd = new Event();
		this.eContentComplete = new Event();
		this.eContentReplay = new Event();
		this.eContentPlayhead = new Event();
		this.eContentSeek = new Event();
		this.eContentSeekEnd = new Event();
		this.eContentBuffering = new Event();
		this.eContentQueue = new Event();
		this.eContentQueueAutoplay = new Event();
		this.eContentVolume = new Event();
		this.eContentError = new Event();
		this.eContentResize = new Event();
		this.eContentStart = new Event();

		this.eAdPlayhead = new Event();
		this.eAdPlay = new Event();
		this.eAdEnd = new Event();
		this.eAdError = new Event();
		this.eAdSensitive = new Event();
		this.eAdClick = new Event();

	},

	_listen : function (bool) {
		if (this._isListening === bool) return;
		this._isListening = bool;
		var method = bool ? 'addListener' : 'removeListener';

		this._videoPlayer.eRendered[method](this._onRendered, this);
		this._videoPlayer.eFullscreenChange[method](this._onFullscreenChange, this);
		this._videoPlayer.eFullscreenError[method](this._onFullscreenError, this);
		this._videoPlayer.ePlayerPaused[method](this._onPlayerPaused, this);
		this._videoPlayer.ePlayerMuted[method](this._onPlayerMuted, this);
		this._videoPlayer.ePlayerStopped[method](this._onPlayerStopped, this);

		this._videoPlayer.eContentMetadata[method](this._onContentMetadata, this);
		// onContentBegin emits here
		this._videoPlayer.eContentPlay[method](this._onContentPlay, this);
		this._videoPlayer.eContentPause[method](this._onContentPause, this);
		this._videoPlayer.eContentEnd[method](this._onContentEnd, this);
		// onContentComplete emits here
		this._videoPlayer.eContentPlayhead[method](this._onContentPlayhead, this);
		this._videoPlayer.eContentSeek[method](this._onContentSeek, this);
		this._videoPlayer.eContentSeekEnd[method](this._onContentSeeked, this);
		this._videoPlayer.eContentBuffering[method](this._onContentBuffering, this);
		this._videoPlayer.eContentVolume[method](this._onContentVolume, this);
		this._videoPlayer.eContentError[method](this._onContentError, this);
		this._videoPlayer.eContentResize[method](this._onContentResize, this);

		this._adProxy.eAdPlayhead[method](this._onAdPlayhead, this);
		this._adProxy.eAdPlay[method](this._onAdPlay, this);
		this._adProxy.eAdEnd[method](this._onAdEnd, this);
		this._adProxy.eAdError[method](this._onAdError, this);
		this._adProxy.eAdClick[method](this._onAdClick, this);
		this._adProxy.eAdReady[method](this._onAdReady, this);
		this._adProxy.eAdStopped[method](this._onPlayerStopped, this);
	},

	render : function(containerElement)
	{
		this._adProxy.setVideoDisplayBase(containerElement);
		this._videoPlayer.render(containerElement);
	},

	remove : function()
	{
		this._videoPlayer.remove();
	},

	dispose : function ()
	{
		_logger.log("dispose");

		this.stop();

		this._commandQ.clear();
		this._commandQ = null;
		this._stoppingQ.clear();
		this._stoppingQ = null;

		// remove listeners
		this._listenForVideoElementActivation(false);
		this._listenForStart(false);
		this._listen(false);

		// dispose and remove references to other modules
		this._adProxy.dispose();
		this._adProxy = null;
		this._videoPlayer.dispose();
		this._videoPlayer = null;
		this._videoBegetter.dispose();
		this._videoBegetter = null;
	},

	_onRendered : function(type)
	{
		_logger.log("_onRendered", type);

		this._commandQ.execute();
		this._commandQ.clear();
		this.eRendered.dispatch();
	},

	_listenForVideoElementActivation : function (bool) {
		if (this._listeningForVideoElementActivation === bool) return;
		this._listeningForVideoElementActivation = bool;
		var method = bool ? 'addListener' : 'removeListener';
		this._videoBegetter.eActivate[method](this._onVideoElementActivation, this);
	},

	_onVideoElementActivation : function ()
	{
		_logger.log("_onVideoElementActivation");

		// Just listen to one event, but react based on the play/replay state.
		this._listenForVideoElementActivation(false);

		switch (this._shimPurpose) {
			case SHIMMING_PLAY:
				this._playMedia();
			break;
			case SHIMMING_REPLAY:
				this.replay();
			break;
		}
	},

	_listenForStart : function (bool) {
		if (this._listeningForStart === bool) return;
		this._listeningForStart = bool;
		var method = bool ? 'addListener' : 'removeListener';
		this._videoPlayer.eContentPlayhead[method](this._onContentStarted, this);
	},

	play : function (catalogEntry, options) {
		_logger.log("play", catalogEntry.getId());

		this._catalogEntry = catalogEntry;
		this._playOptions = options;

		if (this._state !== IDLE) {
			this._stoppingQ.clear();
			this._stoppingQ.push(this._play, this);
			this.stop();
			return;
		}

		this._play();
	},

	_play : function () {
		var catalogEntry = this._catalogEntry;
		var contentId = catalogEntry.getId();

		_logger.log("_play", contentId);

		this._triggeredAd = false;
		this._contentStarted = false;

		if (catalogEntry.isAdSensitive()) {
			this._onAdSensitive(contentId);
		}

/*
		if (this._catalogEntry._protectedContent) {
			this._tokenService.catalogEntry = this._catalogEntry;
			this._tokenService.requestTokenService();
		}
		else {
*/
		if (!this._videoPlayer.rendered())
		{
			_logger.debug("videoPlayer is not rendered; deferring play()");
			this._commandQ.push(this._play, this);
			return;
		}

		this._currentContentId = contentId;
		this._videoBegetter.setPoster(this._getPosterImageForPlayer());

		// _autostart should be set true after the first play,
		// so replay() should fall in here.
		if (this._autostart && this._videoBegetter.isPrimed()) {
			_logger.debug("_play", "autostart enabled and video element is primed; immediate play");

			this._listenForVideoElementActivation(false);
			this._playMedia();
		}
		else {
			_logger.debug("_play", "video element is NOT primed; deferred play via shim");

			this._videoPlayer.removePlayerListeners();

			// If _autostart is true, we attempt to play() the shim
			// even if the element is not primed.
			this._shimPurpose = SHIMMING_PLAY;
			this._listenForVideoElementActivation(true);
			this._videoBegetter.shim({
				autostart : this._autostart
			});
		}
/*
		}
*/
		// Enable autostart on subsequent play() calls.
		this._autostart = true;
	},

	_rewind : function () {
		// Was crashing iPhone (iOS7)...
		// this._videoPlayer.reset();
		// this._executeReplayStrategy( Utils.bind(this.replay, this, this._elementId) );

		var player = this._videoPlayer;
		player.removePlayerListeners();
		player.pause();

		this._videoBegetter.setPoster(this._getPosterImageForPlayer());

		// set up for replay
		this._shimPurpose = SHIMMING_REPLAY;
		this._listenForVideoElementActivation(true);
		this._videoBegetter.shim({
			autostart : false
		});

		// Wait until replay shim loaded before leaving fullscreen player to avoid crash on iOS 7.
		var video = this._videoElement;
		video.addEventListener('loadedmetadata', function waitForLoad() {
			video.removeEventListener('loadedmetadata', waitForLoad, false);
			player.exitFullscreen();
		}, false);

		this._state = IDLE;
	},

	replay : function () {
		_logger.log("replay");

		var instanceId = this._elementId;
		// var instance = CVP.findInstance(instanceId);
		var catalogEntry = this._catalogEntry;

		if (!catalogEntry) {
			_logger.warn("replay", "No previous content entry to replay!");
			return;
		}

		var additionalOptions = this._playOptions;
		var contentId = catalogEntry.getId();

		_logger.log("request to replay contentId '" + contentId + "'");

		var res = CVP.onCallback(instanceId, ["onContentReplayRequest", instanceId, contentId, additionalOptions]);
		// var res = instance.handleCallBack("onContentReplayRequest", instanceId, contentId, additionalOptions);

		// do nothing if the user returns false from the callback!
		if (res === false) {
			_logger.debug("user CB canceled replay request");
			this._rewind();
			return;
		}

		// use result, if an object, as additionalOptions
		if (Utils.isObject(res))
			additionalOptions = res;

		this.eContentReplay.dispatch(contentId, additionalOptions);

		this._play();
	},

	playNextInQueue : function () {
		if (!this._contentQueue.length) {
			_logger.warn("playNextInQueue", "Content queue is empty!");
			return;
		}

		var content = this._contentQueue.shift();

		this.eContentQueue.dispatch(this.getQueue());

		this.play(content.entry, content.options);
	},

	// videoPlayer might not have this catalogEntry
	_getPosterImageForPlayer : function () {
		return this._catalogEntry.getImageForDimensions(this._videoPlayer.getWidth(), this._videoPlayer.getHeight());
	},

	_playMedia : function() {
		_logger.log("_playMedia");

		this._state = PLAYING;

		// reset flag used to force Play event after Metadata event
		this._metadataFired = false;

		this._onContentBegin(this._catalogEntry.getId());

		if (this._adProxy.isEnabled()) {
			this._videoPlayer.disableCrossOrigin();
			this._videoPlayer.hideControls();
			this._adProxy.loadAds(this._catalogEntry);
		}
		else {
			this._playContent();
		}
	},

	_playContent : function()
	{
		_logger.log("_playContent");

		this._listenForStart(true);

		this._adProxy.setVideoStatePlaying();

		this._playContentTime = Utils.now();

		this._videoPlayer.showControls();
		this._videoPlayer.play(this._catalogEntry);
	},

	pause : function()
	{
		_logger.log("pause");

		this._videoPlayer.pause();
	},

	resume : function()
	{
		_logger.log("resume");

		if (this._currentContentId) {
			this._videoPlayer.resume();
		}
	},

	reset : function ()
	{
		_logger.log("reset");

		// TODO - document why this was necessary
		// I presume stop() means starting over, but shouldn't
		// that be play()'s responsibility?  Or post-completion?
		this.clearRestorePlayhead();

		this._listenForVideoElementActivation(false);
		this._listenForStart(false);
	},

	stop : function ()
	{
		_logger.log("stop");

		if (this._state === STOPPING) return;

		this._state = STOPPING;

		this.reset();

		if (this._mediaClass === AD_MEDIA)
		{
			_logger.log('stopping ad');
			this._adProxy.stop();
		}
		else if (this._mediaClass === CONTENT_MEDIA)
		{
			_logger.log('stopping content');
			this._videoPlayer.stop();
		}
		else {
			this._onPlayerStopped();
		}
	},

	queue : function(catalogEntry, options)
	{
		_logger.debug("queue", catalogEntry.getId());

		this._contentQueue.push({
			entry : catalogEntry,
			options : options
		});

		this.eContentQueue.dispatch(this.getQueue());
	},

	dequeue : function (id) {
		var originalLength = this._contentQueue.length;

		for (var entry, i = originalLength - 1; i >= 0; --i)
		{
			entry = this._contentQueue[i].entry;

			if (entry.getId() === id) {
				this._contentQueue.splice(i, 1);
				break;
			}
		}

		if (this._contentQueue.length !== originalLength) {
			this.eContentQueue.dispatch(this.getQueue());
		}
	},

	emptyQueue : function()
	{
		if (this._contentQueue.length) {
			this._contentQueue.length = 0;
			this.eContentQueue.dispatch(this.getQueue());
		}
	},

	setQueueAutoplay : function(autoplay)
	{
		this._queueAutoplay = autoplay;

		this.eContentQueueAutoplay.dispatch(this._queueAutoplay);
	},

	getQueue : function ()
	{
		return Utils.map(this._contentQueue, function (item) {
			return item.entry.getId();
		});
	},

	seek : function(seconds)
	{
		if (this._contentStarted) {
			_logger.log("seeking to " + seconds + " seconds into the content");
			this._videoPlayer.seek(seconds);
		}
		else {
			_logger.debug("only setting playhead restore point if seek() has been called before content started");
			this.setRestorePlayhead(seconds);
		}
	},

	setRestorePlayhead : function (seconds)
	{
		_logger.info("set restore playhead to " + seconds);
		this._seekTo = seconds;
	},

	clearRestorePlayhead : function ()
	{
		this._seekTo = 0;
	},

	resize : function(width, height)
	{
		this._videoPlayer.resize(width, height);
	},

	goFullscreen : function()
	{
		this._videoPlayer.goFullscreen();
	},

	setVolume : function(v)
	{
		this._videoPlayer.setVolume(v);
	},

	getVolume : function()
	{
		return this._videoPlayer.getVolume();
	},

	mute : function()
	{
		this._videoPlayer.mute();
	},

	unmute : function()
	{
		this._videoPlayer.unmute();
	},

	isMuted : function ()
	{
		this._videoPlayer.isMuted();
	},

	setAdSection : function(ssid)
	{
		this._adProxy.setAdSection(ssid);
	},

	getAdSection : function()
	{
		return this._adProxy.getAdSection();
	},

	setAdKeyValue : function (key, value)
	{
		this._adProxy.setAdKeyValue(key, value);
	},

	getAdKeyValue : function (key)
	{
		return this._adProxy.getAdKeyValue(key);
	},

	getAdKeyValues : function ()
	{
		return this._adProxy.getAdKeyValues();
	},

	clearAdKeyValues : function ()
	{
		this._adProxy.clearAdKeyValues();
	},

	getCatalogEntry : function () {
		return this._catalogEntry;
	},

	getPlayerDimensions : function () {
		var dimensions = this._videoPlayer.getDimensions();

		if (this._fullscreen) {
			// assume device screen dimensions
			Utils.extend(dimensions, {
				width : window.screen.width,
				height : window.screen.height
			});
		}

		return dimensions;
	},

	getAdId : function () {
		return this._adProxy.currentAdId;
	},

	getAdType : function () {
		return this._adProxy.getCurrentAdType();
	},

	_onFullscreenChange : function (isFullscreen)
	{
		_logger.log("_onFullscreenChange");

		this._fullscreen = isFullscreen;

		this.eFullscreenChange.dispatch(this._currentContentId, isFullscreen);

		if (this._mediaClass === CONTENT_MEDIA) {
			var dimensions = this.getPlayerDimensions();
			this._onContentResize(dimensions.width, dimensions.height, isFullscreen);
		}
	},

	_onFullscreenError : function ()
	{
		_logger.log("_onFullscreenError");
		this.eFullscreenError.dispatch();
	},

	_onPlayerPaused : function (content, playhead, paused)
	{
		_logger.log("_onPlayerPaused", content, playhead, paused);
		this.ePlayerPaused.dispatch(content, playhead, paused);
	},

	_onPlayerMuted : function (muted)
	{
		_logger.log("_onPlayerMuted", muted);
		this.ePlayerMuted.dispatch(muted);
	},

	_onPlayerStopped : function ()
	{
		_logger.log("_onPlayerStopped");
		this._state = IDLE;
		this._currentContentId = '';
		this._stoppingQ.execute();
		this._stoppingQ.clear();
	},

	_onContentMetadata : function (contentId, duration, width, height)
	{
		_logger.log("_onContentMetadata", contentId, duration, width, height);

		if (!this._metadataFired) {
			this._metadataFired = true;
			this.eContentMetadata.dispatch(contentId, duration, width, height);
		}

		this._dumpPlayEventQueue();
	},

	_onContentBegin : function (contentId)
	{
		_logger.log("_onContentBegin", contentId);

		adBlockId = 0;

		this.eContentBegin.dispatch(contentId, this._playOptions);
	},

	_dumpPlayEventQueue : function () {

		if (!this._playEventQueue.length)
			return;

		for (var i = 0, endi = this._playEventQueue.length; i < endi; ++i) {
			this._fireContentPlay.apply(this, this._playEventQueue[i]);
		}

		this._playEventQueue.length = 0;
	},

	_fireContentPlay : function (contentId, serverIp) {
		this.eContentPlay.dispatch(contentId, serverIp);
	},

	_onContentPlay : function (contentId, serverIp)
	{
		_logger.log("_onContentPlay", contentId, serverIp);

		this._mediaClass = CONTENT_MEDIA;
		this._contentStarted = false;

		if (this._metadataFired) {
			this._fireContentPlay(contentId, serverIp);
		} else {
			this._playEventQueue.push(arguments);
		}
	},

	_onContentPause : function (contentId, paused)
	{
		_logger.log("_onContentPause", contentId, paused);
		this.eContentPause.dispatch(contentId, paused);
		if (paused) {
			this._adProxy.setVideoStatePaused();
		}
		else {
			this._adProxy.setVideoStatePlaying();
		}
	},

	_onContentEnd : function (contentId)
	{
		_logger.log("_onContentEnd", contentId);

		this._mediaClass = NO_MEDIA;

		this.eContentEnd.dispatch(contentId);

		this._adProxy.setVideoStateStopped();

		if (this._adProxy.isEnabled()) {
			this._videoPlayer.hideControls();
			this._videoPlayer.disableCrossOrigin();
			this._adProxy.playPostroll(this._catalogEntry);
		} else {
			this._onContentComplete.apply(this, arguments);
		}
	},

	_continueAutoplay : function ()
	{
		if (this._queueAutoplay && this._contentQueue.length) {
			this.playNextInQueue();
		}
		else {
			this._rewind();
		}
	},

	_onContentComplete : function()
	{
		_logger.log("_onContentComplete");
		this.eContentComplete.dispatch(this._catalogEntry.getId());
		this._videoPlayer.showControls();
		this._adProxy.setVideoStateCompleted();
		this._continueAutoplay();
	},

	_onContentStarted : function (contentId, playhead /* , duration */)
	{
		if (playhead > 0) {
			_logger.log("_onContentStarted", "firing ContentStart because playhead is greater than zero");
			this._listenForStart(false);
			this._contentStarted = true;
			var joinTime = Math.ceil((Utils.now() - this._playContentTime) / 1000);

			// in case Metadata never fires, make sure Play does!
			if (!this._metadataFired)
				this._dumpPlayEventQueue();

			this.eContentStart.dispatch(this._catalogEntry.getId(), joinTime);
			this._seekAhead();
		}
		else {
			_logger.log("_onContentStarted", "playhead still at zero");
		}
	},

	// cannot seek() immediately, wait for playhead to move from zero
	_seekAhead : function ()
	{
		if (this._seekTo) {
			_logger.info("restoring playhead to " + this._seekTo);
			this.seek(this._seekTo);
			this.clearRestorePlayhead();
		}
	},

	_onContentPlayhead : function (contentId, playhead, duration)
	{
		this._currentContentPlayhead = playhead;
		this.eContentPlayhead.dispatch(contentId, playhead, duration, -1);
	},

	_onAdReady : function ()
	{
		_logger.log("_onAdReady");

		this._midrollMap = this._adProxy.getMidrolls();

		if (Utils.isSimpleObject(this._midrollMap) && !Utils.isEmpty(this._midrollMap))
		{
			_logger.log("_onAdReady - midrolls: ", Utils.keys(this._midrollMap));
			this._videoPlayer.eContentPlayhead.addListener(this._onPlayheadForMidrolls, this);
		}
	},

	_onPlayheadForMidrolls : function (contentId, playhead /* , duration */)
	{
		var seconds = Math.floor(playhead);

		if (seconds in this._midrollMap)
		{
			_logger.log("_onPlayheadForMidrolls", "playing midroll at " + seconds + " of midrolls: ", Utils.keys(this._midrollMap));
			this.setRestorePlayhead(playhead);
			this._currentMidroll = this._midrollMap[seconds];
			delete this._midrollMap[seconds];
			this._playMidroll(this._currentMidroll);
		}
	},

	_playMidroll : function (index)
	{
		_logger.log("_playMidroll");
		this._adProxy.setVideoStatePaused();
		this._videoPlayer.disableCrossOrigin();
		this._adProxy.playMidroll(index);
	},

	_resumeFromMidroll : function ()
	{
		_logger.log("_resumeFromMidroll");
		this._playContent();
	},

	_onContentSeek : function (contentId, previousPlayhead, newPlayhead, duration)
	{
		_logger.log("_onContentSeek", contentId, previousPlayhead, newPlayhead, duration);
		this.eContentSeek.dispatch(contentId, previousPlayhead, newPlayhead, duration);
	},

	_onContentSeeked : function (contentId, previousPlayhead, newPlayhead, duration)
	{
		_logger.log("_onContentSeeked", contentId, previousPlayhead, newPlayhead, duration);
		this.eContentSeekEnd.dispatch(contentId, previousPlayhead, newPlayhead, duration);
	},

	_onContentBuffering : function (contentId, buffering)
	{
		_logger.log("_onContentBuffering");
		this.eContentBuffering.dispatch(contentId, buffering);
	},

	_onContentVolume : function (muted, volume)
	{
		_logger.log("_onContentVolume");
		this.eContentVolume.dispatch(muted, volume);
	},

	_onContentError : function (contentId, errorMessage)
	{
		_logger.log("_onContentError", contentId, errorMessage);
		this.eContentError.dispatch(contentId, errorMessage);
		this._continueAutoplay();
	},

	_onContentResize : function (width, height, fullscreen)
	{
		_logger.log("_onContentResize", width, height, fullscreen);
		this.eContentResize.dispatch(width, height, fullscreen);
	},

	_onAdSensitive : function (contentId)
	{
		_logger.debug("_onAdSensitive", contentId);
		this.eAdSensitive.dispatch(contentId);
	},

	_onAdPlayhead : function (playhead, duration)
	{
		this._currentAdPlayhead = playhead;
		this._currentAdDuration = duration;
		this.eAdPlayhead.dispatch(playhead, duration, adBlockId);
	},

	_onAdPlay : function(event)
	{
		var token /*:String*/     = event.token;  // info about ad
		var mode /*:String*/      = event.mode;  // indicates "ad" or "c3break"
		var id /*:String*/        = event.id;  // the ad id
		var duration /*:Number*/  = event.duration;   // the ad duration
		var adType /*:String*/    = event.adType;  // type of ad (pre-roll, mid-roll, or post-roll)
		var adAnalyticsData       = event.adAnalyticsData;  // { adInstances: [] }

		_logger.debug("_onAdPlay", "event", event);

		this._triggeredAd = true;
		this._contentStarted = false;
		this._mediaClass = AD_MEDIA;

		this._videoPlayer.removePlayerListeners();

		this.eAdPlay.dispatch(token, mode, id, duration, adBlockId, adType, adAnalyticsData);
	},

	_onAdEnd : function(event)
	{
		var token /*:String*/     = event.token;  // info about ad
		var mode /*:String*/      = event.mode;  // indicates "ad" or "c3break"
		var id /*:String*/        = event.id;  // the ad id
		var adType /*:String*/    = event.adType;  // type of ad (pre-roll, mid-roll, or post-roll)

		_logger.debug("_onAdEnd", "event", event);

		if (this._triggeredAd) {
			this.eAdEnd.dispatch(token, mode, id, adBlockId, adType);
		}

		this._onAdComplete(adType);
	},

	_onAdError : function(event)
	{
		// var id /*:String*/        = event.id;  // the ad id
		var adType /*:String*/    = event.adType;  // type of ad (pre-roll, mid-roll, or post-roll)
		var errors /*:Array*/     = event.errors;  // list of errors from implementation provider
		var errorMessage          = event.errorMessage;

		_logger.debug("_onAdError", "event", event);

		this.eAdError.dispatch(errorMessage, adBlockId, adType, errors);

		this._onAdComplete(adType);
	},

	_onAdComplete : function(adType) {
		adBlockId += 1;
		this._triggeredAd = false;

		this._mediaClass = NO_MEDIA;

		// Cease functionality if stop() has been called.
		if (this._state === STOPPING) {
			_logger.debug("_onAdComplete", "halting further activities because stop() was called, resetting video player");
			this._videoPlayer.reset();
			return;
		}

		switch (adType)
		{
			case AdServerProxy.PREROLL:
				this._playContent();
			break;
			case AdServerProxy.MIDROLL:
				this._resumeFromMidroll();
			break;
			case AdServerProxy.POSTROLL:
				this._onContentComplete(this._currentContentId);
			break;
			default:
				this._onContentError('Failed to determine adType!');
		}
	},

	_onAdClick : function (event) {
		this.eAdClick.dispatch(event.url);
		this._videoPlayer.showControls();
	}
});


// Export public APIs
module.exports = PlayerController;

},{"../../cvp/customevent":16,"../../cvp/log":28,"../../cvp/static":38,"../../cvp/util/commandqueue":41,"../../cvp/utils":50,"../../vendor/Class":93,"../ads/adserverproxy":52,"./videoplayer":89}],83:[function(require,module,exports){
/* html5/player/domeventhandler.js */

// DOMEventHandler
//
// Encapsulate event listening logic to allow ease of removing listeners and adding them back again.

// Load dependent modules
var Class = require('../../vendor/Class');
var Utils = require('../../cvp/utils');


var DOMEventHandler = Class.extend({

	init : function (element, events, listener, capture) {
		if (!(element || events || listener)) {
			throw new Error('Usage: new DOMEventHandler(element, events, listener, capture).once().while(fn).until(fn)');
		}

		this._enabled = false;
		this._element = element;
		this._events = Utils.isArray(events) ? events : events.split(/\W+/);
		this._listener = listener;
		this._capture = capture || false;
		this._once = false;
		this._precondition = null;
		this._postcondition = null;

		this.listener = Utils.bind(this.listener, this);
		this.addListener = Utils.bind(this.addListener, this);
		this.removeListener = Utils.bind(this.removeListener, this);

		return this;
	},

	// DOMEventHandler.dispose() = stops listening, then nullifies references to elements and functions; cannot resume listening, the Handler is dead and useless
	dispose : function () {
		this.off();
		this._element = null;
		this._events = null;
		this._listener = null;
		this._precondition = null;
		this._postcondition = null;
	},

	listener : function () {
		if (this._precondition && !this._precondition()) {
			this.off();
			return;
		}

		if (this._once) {
			this.off();
		}

		this._listener.apply(this._element, arguments);

		if (this._postcondition && !this._postcondition()) {
			this.off();
		}
	},

	addListener : function (event) {
		this._element.addEventListener(event, this.listener, this._capture);
	},

	removeListener : function (event) {
		this._element.removeEventListener(event, this.listener, this._capture);
	},

	// DOMEventHandler().once() = race condition; the first event triggered wins
	once : function () {
		this._once = true;
		return this;
	},

	// DOMEventHandler().every() = unset once()
	every : function () {
		this._once = false;
		return this;
	},

	// DOMEventHandler().while(function () { return true; 'when condition met' }) = keep listening while condition met, then stop listening
	"while" : function (fn) {
		if (Utils.isFunction(fn)) {
			this._precondition = fn;
		}

		return this;
	},

	// DOMEventHandler().until(function () { return true; 'when condition met' }) = keep listening until condition met, then stop listening
	until : function (fn) {
		if (Utils.isFunction(fn)) {
			this._postcondition = fn;
		}

		return this;
	},

	off : function () {
		if (!this._enabled) {
			return;
		}

		this._enabled = false;

		Utils.each(this._events, this.removeListener);
	},

	on : function () {
		if (this._enabled || !this._element) {
			return;
		}

		this._enabled = true;

		Utils.each(this._events, this.addListener);
	}

});


// Export public APIs
module.exports = DOMEventHandler;

},{"../../cvp/utils":50,"../../vendor/Class":93}],84:[function(require,module,exports){
/* html5/player/fileselection.js */

// Load dependent modules
var Class = require('../../vendor/Class');
var Utils = require('../../cvp/utils');
var ConfigUtils = require('../../cvp/util/configutils');
var Browser = require('../../cvp/browser');
var JsonConverter = require('../../cvp/util/jsonconverter');
var Log = require('../../cvp/log');

var logger = Log.getLogger('FileSelection');

// Local utils
var getKeyFromContentFile = function(file) {
	file = file || {};
	if (file.hasOwnProperty("key"))
		return file.key;
	else if (file.hasOwnProperty("bitrate"))
		return file.bitrate;
};

var getExtensionForContentFile = function(file) {
	var key = getKeyFromContentFile(file);
	if (state.availableFiles.hasOwnProperty(key))
		return state.availableFiles[key].ext;
	return "";
};

var createFilter = function(key, value) {
	return function(obj /*, i, list*/) {
		if (obj.hasOwnProperty(key))
			return obj[key] === value;
		return false;
	};
};

var mapFileTo = function(key, defaultVal) {
	return function(value /*, i, list*/) {
		if (value && value.hasOwnProperty(key))
			return value[key];
		return defaultVal;
	};
};

// map catalogEntry file object to url
var mapFileToUrl = mapFileTo("url", "");

// mapcontent.xml file object to url
var mapContentFileToUrl = mapFileTo("text", "");


// Shared state
var state = {
	config : {},

	supportedFileTypes : [],
	selectedFileKeys : [],
	fallbackFileTypes : ["m3u8", "mp4"],

	availableFiles : {},
	availableFileKeys : [],
	availableFileUrls : [],

	contentEntryObj : {},
	contentEntryFiles : []
};

/**
 * Page selection strategies
 *
 * A page selection callback can return the following:
 *   - an array of contentEntry file objects
 *   - a single url as a string
 *   - a boolean
 *
 *	For the latter, true means we should fallthrough
 *	to config selection. false means we should error out.
 */
var PageSelectionHandler = Class.extend({
	canExecute : function() {
		var params = state.config.params;

		return (
			params.hasOwnProperty("fileSelection")
			|| params.hasOwnProperty("perVideoFallbacks")
		);
	},

	execute : function(catalogEntry) {
		var params = state.config.params,
			retVal;

		// Prefer the fileSelection handler
		if (params.hasOwnProperty("fileSelection")) {
			retVal = this._getPageFileSelection(catalogEntry);

		// Otherwise, if perVideoFallbacks is available, use that
		} else if (params.hasOwnProperty("perVideoFallbacks")) {
			retVal = this._getFileByParameterFallback(catalogEntry);
		}

		// Ensure any files returned are in an array
		if (Utils.isString(retVal)) {
			retVal = [retVal];
		}

		// Validate and sanitize any files
		if (Utils.isArray(retVal)) {
			// Validate
			//	warning only right now
			Utils.each(retVal, function(url) {
				if (Utils.indexOf(state.availableFileUrls, url) === -1) {
					logger.warn("Potentially invalid url: " + url);
				}
			});

			// Sanitize
			retVal = Utils.map(retVal, function(url) {
				return catalogEntry.sanitizeUrl(url);
			});
		}
		return retVal;
	},

	/**
	 * Invoke a page callback, passing the available file
	 * data and the content entry.
	 *
	 * Helper functions to filter the data are available to
	 * the callback.
	 */
	_getPageFileSelection : function(/* catalogEntry */) {
		logger.debug("Using page selection via 'fileSelection'");

		var fileSelectionFunc = state.config.params.fileSelection || null;
		if (!fileSelectionFunc)
			return false;

		// Helper functions for the page to filter the files list
		var context = {
			_makeList : function(list) {
				if (Utils.isString(list))
					list = list.split(",");

				if (!Utils.isArray(list))
					return [];

				return list.map(function(value) {
					return Utils.trim(value);
				});
			},

			filterBy : function(list, key, value) {
				var filter = (Utils.isFunc(key))
					? key
					: createFilter(key, value);

				return Utils.filter(list, filter);
			},

			filterByDimension : function(list, w, h) {
				return this.filterBy(list, function(obj) {
					// coerce
					return obj.width === w && obj.height === h;
				});
			},

			filterByKey : function(list, keys) {
				keys = this._makeList(keys);

				return this.filterBy(list, function(obj) {
					return Utils.indexOf(keys, getKeyFromContentFile(obj)) > -1;
				});
			},

			filterByExtension : function(list, extensions) {
				extensions = this._makeList(extensions);

				return this.filterBy(list, function(obj) {
					return Utils.indexOf(extensions, getExtensionForContentFile(obj)) > -1;
				});
			}
		};

		var selectedFiles = fileSelectionFunc.call(context, state.contentEntryFiles, state.contentEntryObj);

		return (Utils.isArray(selectedFiles)
			? selectedFiles.map(mapContentFileToUrl)
			: selectedFiles
		);
	},

	/**
	 * PerVideoFallback support.
	 *
	 * @deprecated Since version 2.8.0.0. Use _getPageFileSelection.
	 */
	_getFileByParameterFallback : function(catalogEntry) {
		logger.debug("Using page selection via 'perVideoFallbacks'");

		var perVideoFallbacks = state.config.params.perVideoFallbacks || [];
		if (!perVideoFallbacks)
			return false;

		var i = 0,
			endi = perVideoFallbacks.length,
			dvf,
			criteria;

		for (; i < endi; ++i) {
			dvf = perVideoFallbacks[i];
			criteria = ConfigUtils.stringReplace(dvf.criteria, catalogEntry, false);
			if (dvf.evaluate(criteria)) {
				return ConfigUtils.stringReplace(dvf.filter, catalogEntry, false);
			}
		}

		return null;
	}
});

/**
 * Config selection strategies
 */
var ConfigSelectionHandler = Class.extend({
	canExecute : function() {
		// We can always use at least one config strategy
		return true;
	},

	execute : function(catalogEntry) {
		var list = [];

		// Prefer the config-specified file key and extension selections
		if (state.selectedFileKeys.length
			|| state.supportedFileTypes.length) {
			logger.debug("Using config file key selection: " + state.selectedFileKeys);
			logger.debug("Using config file type selection: " + state.supportedFileTypes);

			var fileKeyUrls = catalogEntry.getContentUrlsByFileKeys(state.selectedFileKeys);
			var fileTypeUrls = catalogEntry.getContentUrlsByExtensions(state.supportedFileTypes);

			logger.debug("fileKeyUrls:", fileKeyUrls);
			logger.debug("fileTypeUrls:", fileTypeUrls);

			if (fileKeyUrls && fileKeyUrls.length)
				list.push.apply(list, fileKeyUrls);

			if (fileTypeUrls && fileTypeUrls.length)
				list.push.apply(list, fileTypeUrls);

		// If not available, go to the legacy support
		} else {
			logger.debug("Using config legacy selection");

			var iOSFallbackUrl = this._getFileByFallback(catalogEntry);
			var configFallbackUrl = this._getFileByConfigFallbackParam(catalogEntry);
			var defaultUrls = catalogEntry.getContentUrlsByExtensions(state.fallbackFileTypes);

			logger.debug("iOSFallbackUrl:", iOSFallbackUrl);
			logger.debug("configFallbackUrl:", configFallbackUrl);
			logger.debug("defaultUrls:", defaultUrls);

			if (iOSFallbackUrl)
				list.push(iOSFallbackUrl);

			if (configFallbackUrl)
				list.push(configFallbackUrl);

			if (defaultUrls && defaultUrls.length)
				list.push.apply(list, defaultUrls);
		}
		return list;
	},

	/**
	 * @deprecated Since version 2.8.0.0.
	 */
	_getFileByConfigFallbackParam : function(catalogEntry) {
		var str = state.config.configInfo.fallbackFilenameIOS;

		if (Utils.empty(str))
			return str;

		str = ConfigUtils.stringReplace(str, catalogEntry, false);
		return str;
	},

	/**
	 * @deprecated Since version 2.8.0.0.
	 */
	_getFileByFallback : function(catalogEntry) {
		if (Browser.isOS(Browser.IOS)) {
			return catalogEntry.getContentUrlFromFallback("iOS");
		}
	}
});

/**
 * File selection interface.
 * This will delegate to the proper strategy(s)
 * and return the result.
 */
var FileSelectionHandler = Class.extend({

	init : function(config) {
		state.config = config;

		var delim = /\W+/;
		var configInfo = state.config.configInfo;
		state.supportedFileTypes = Utils.uniq(Utils.filter(configInfo.html5Types.split(delim)));
		state.selectedFileKeys = Utils.uniq(Utils.filter(configInfo.html5Order.split(delim)));

		this._pageSelection = new PageSelectionHandler();
		this._configSelection = new ConfigSelectionHandler();
	},

	/**
	 * Skip page-selection for isExpire()'ed videos.
	 * (The catalogEntry will just have a URL for video-not-available.mp4.)
	 */
	getDefaultPlaylistForEntry : function (catalogEntry) {
		return catalogEntry.getContentUrlsByExtensions(state.fallbackFileTypes);
	},

	/**
	 * Based on the available page and config strategies,
	 * construct and return the appropriate playlist
	 */
	getPlaylistForEntry : function(catalogEntry) {
		this._setState(catalogEntry);
		var list = [];

		var pageSelectionAvailable = this._pageSelection.canExecute();
		var pageSelectionResult = null;

		var configSelectionAvailable = this._configSelection.canExecute();
		var configSelectionResult = null;

		if (pageSelectionAvailable)
			pageSelectionResult = this._pageSelection.execute(catalogEntry);

		// If a list was returned, use the page selection
		if (pageSelectionAvailable
			&& Utils.isArray(pageSelectionResult)) {
			list = list.concat(pageSelectionResult);

		// If not available, or if true is returned, fall through to config selection
		} else if (!pageSelectionAvailable
			|| pageSelectionResult === true
			|| pageSelectionResult === null
			|| pageSelectionResult === undefined) {

			if (configSelectionAvailable) {
				configSelectionResult = this._configSelection.execute(catalogEntry);

				if (Utils.isArray(configSelectionResult))
					list = list.concat(configSelectionResult);
			}
		}

		// de-dupe, remove nulls/empties
		list = Utils.uniq(Utils.filter(list));
		return list;
	},

	_setState : function(catalogEntry) {
		state.availableFiles = catalogEntry.getAllFiles();
		state.availableFileKeys = Utils.keys(state.availableFiles);

		state.availableFileUrls = Utils.map(state.availableFileKeys, function(value, i, list) {
			return mapFileToUrl(state.availableFiles[value], i, list);
		});

		var contentEntryObj = JsonConverter.xmlToObject(catalogEntry.getXML());
		state.contentEntryObj = contentEntryObj;

		// Ensure we only choose file entries from the content.xml that are available in the
		// catalogEntry.
		var contentEntryFiles = [];
		Utils.filter(contentEntryObj.files, function(obj) {
			var file = obj.file,
				key = getKeyFromContentFile(file);

			// TODO need a url comparison as well because the file
			// parsing is different than flash for TVE file lists
			// 	the key needs to be based on more than fileKey
			if (state.availableFiles.hasOwnProperty(key)
				&& file.text === state.availableFiles[key].url)
				contentEntryFiles.push(file);
		});
		state.contentEntryFiles = contentEntryFiles;
	}
});


// Export public APIs
module.exports = FileSelectionHandler;

},{"../../cvp/browser":12,"../../cvp/log":28,"../../cvp/util/configutils":43,"../../cvp/util/jsonconverter":46,"../../cvp/utils":50,"../../vendor/Class":93}],85:[function(require,module,exports){
/* html5/player/fullscreenmonitor.js */

// Load dependent modules
var Utils = require('../../cvp/utils');
var Log = require('../../cvp/log');
var Event = require('../../cvp/customevent');


var logger = Log.getLogger('FullscreenMonitor');

function FullscreenMonitor() {

	this._isFullscreen = false;

	this._onW3FullscreenChangeBind = Utils.bind(this._onW3FullscreenChange, this);
	this._onWebKitFullscreenChangeBind = Utils.bind(this._onWebKitFullscreenChange, this);
	this._onMozFullscreenChangeBind = Utils.bind(this._onMozFullscreenChange, this);

	this._onWebKitBeginFullscreenBind = Utils.bind(this._onWebKitBeginFullscreen, this);
	this._onWebKitEndFullscreenBind = Utils.bind(this._onWebKitEndFullscreen, this);

	this._onW3FullscreenErrorBind = Utils.bind(this._onW3FullscreenError, this);
	this._onWebKitFullscreenErrorBind = Utils.bind(this._onWebKitFullscreenError, this);
	this._onMozFullscreenErrorBind = Utils.bind(this._onMozFullscreenError, this);

	this.emitFullscreenChange = Utils.bind(this.emitFullscreenChange, this);
	this.emitFullscreenError = Utils.bind(this.emitFullscreenError, this);

	this.onFullscreenChange = new Event('fullscreenChange');
	this.onFullscreenError = new Event('fullscreenError');
}

FullscreenMonitor.prototype = {

	setup : function (player) {
		logger.log('setup');

		if (!player || this.player) return;

		this.player = player;

		this._listen(true);
	},

	teardown : function () {
		logger.log('teardown');

		if (!this.player) return;

		this._listen(false);
	},

	dispose : function () {
		this.teardown();
		this.player = null;
	},

	_listen : function (bool) {
		if (this._isListening === bool) return;
		this._isListening = bool;
		var method = bool ? 'addEventListener' : 'removeEventListener';
		var el = this.player.getPlayerElement();

		el[method]("webkitbeginfullscreen", this._onWebKitBeginFullscreenBind, false);
		el[method]("webkitendfullscreen", this._onWebKitEndFullscreenBind, false);

		document[method]("fullscreenchange", this._onW3FullscreenChangeBind, false);
		document[method]("webkitfullscreenchange", this._onWebKitFullscreenChangeBind, false);
		document[method]("mozfullscreenchange", this._onMozFullscreenChangeBind, false);

		document[method]("fullscreenerror", this._onW3FullscreenErrorBind, false);
		document[method]("webkitfullscreenerror", this._onWebKitFullscreenErrorBind, false);
		document[method]("mozfullscreenerror", this._onMozFullscreenErrorBind, false);
	},

	_handleFullscreenChange : function (isFullscreen, fullscreenElement)
	{
		if (!isFullscreen || (isFullscreen && fullscreenElement === this.player.getPlayerElement())) {  // ASSUME: our element gets a fullscreen off before another element goes on
			if (isFullscreen !== this._isFullscreen) {  // only fire when fullscreen has changed
				this._isFullscreen = isFullscreen;
				this.emitFullscreenChange(this._isFullscreen, fullscreenElement);
			}
		}
	},

	_onW3FullscreenChange : function ()
	{
		var fullscreenElement = document.fullscreenElement;
		var fullscreenEnabled = !!(document.fullscreenEnabled || document.fullscreen) || !!(fullscreenElement);
		logger.debug('_onW3FullscreenChange', fullscreenEnabled, fullscreenElement);
		this._handleFullscreenChange(fullscreenEnabled, fullscreenElement);
	},

	_onWebKitFullscreenChange : function ()
	{
		var fullscreenElement = document.webkitFullscreenElement;
		var fullscreenEnabled = !!(document.webkitIsFullScreen) || !!(fullscreenElement);
		logger.debug('_onWebKitFullscreenChange', fullscreenEnabled, fullscreenElement);
		this._handleFullscreenChange(fullscreenEnabled, fullscreenElement);
	},

	_onMozFullscreenChange : function ()
	{
		var fullscreenElement = document.mozFullScreenElement;
		var fullscreenEnabled = !!(document.mozFullScreen || document.fullScreen) || !!(fullscreenElement);
		logger.debug('_onMozFullscreenChange', fullscreenEnabled, fullscreenElement);
		this._handleFullscreenChange(fullscreenEnabled, fullscreenElement);
	},

	_onWebKitBeginFullscreen : function ()
	{
		var fullscreenElement = this.player.getPlayerElement();
		var fullscreenEnabled = true;
		logger.debug('_onWebKitBeginFullscreen', fullscreenEnabled, fullscreenElement);
		this._handleFullscreenChange(fullscreenEnabled, fullscreenElement);
	},

	_onWebKitEndFullscreen : function ()
	{
		var fullscreenElement = null;
		var fullscreenEnabled = false;
		logger.debug('_onWebKitEndFullscreen', fullscreenEnabled, fullscreenElement);
		this._handleFullscreenChange(fullscreenEnabled, fullscreenElement);
	},

	_handleFullscreenError : function ()
	{
		logger.log('_onFullscreenError');
		this.emitFullscreenError();
	},

	_onW3FullscreenError : function ()
	{
		logger.log('_onW3FullscreenError');
		this._handleFullscreenError();
	},

	_onWebKitFullscreenError : function ()
	{
		logger.log('_onWebKitFullscreenError');
		this._handleFullscreenError();
	},

	_onMozFullscreenError : function ()
	{
		logger.log('_onMozFullscreenError');
		this._handleFullscreenError();
	},

	emitFullscreenChange : function (isFullscreen, fullscreenElement) {
		logger.log('emitFullscreenChange');
		this.onFullscreenChange.dispatch(isFullscreen, fullscreenElement);
	},

	emitFullscreenError : function () {
		logger.log('emitFullscreenError');
		this.onFullscreenError.dispatch();
	}

};


// Export public APIs
module.exports = FullscreenMonitor;

},{"../../cvp/customevent":16,"../../cvp/log":28,"../../cvp/utils":50}],86:[function(require,module,exports){
/* html5/player/instance.js */

// Load dependent modules
var PlayerConfig = require('../shared/playerconfig');
var Utils = require('../../cvp/utils');
var PlayerController = require('./controller');


var PlayerInstance = function(primer, data) {

	// copy default playerConfig
	var config = Utils.extend({}, PlayerConfig);

	// mix in provided data
	Utils.extend(config.params, data.params);
	Utils.extend(config, data.configData);

	return new PlayerController(primer, config);
};


// Export public APIs
module.exports = PlayerInstance;

},{"../../cvp/utils":50,"../shared/playerconfig":90,"./controller":82}],87:[function(require,module,exports){
/* html5/player/pilotepisode.js */

// Load dependent modules
var Class = require('../../vendor/Class');
var Event = require('../../cvp/customevent');
var Utils = require('../../cvp/utils');
var Log = require('../../cvp/log');
var Timing = require('../../cvp/util/timing');


var logger = Log.getLogger('PilotEpisode');

var MAX_SESSION_DURATION = 20000;
var MAX_ATTEMPT_DURATION = 5000;

// Capturing any/all media events during PilotEpisode, then re-dispatch after Player re-attached.
var mediaEvents = 'loadstart progress suspend abort error emptied stalled loadedmetadata loadeddata canplay canplaythrough playing waiting seeking seeked ended durationchange timeupdate play pause ratechange resize volumechange'.split(' ');

var MediaError = window.MediaError || function () {};

function isMediaError(error) {
	return error instanceof MediaError && 'code' in error;
}

function isErrorCausedBySrcNotSupported(error) {
	// BUG: On Win8.0 phone, an error code of 1 is emitted for m3u8s that it cannot play.
	var code = error.code;
	var notSupported = code === MediaError.MEDIA_ERR_SRC_NOT_SUPPORTED;
	var invalidError = code === MediaError.MEDIA_ERR_ABORTED;

	if (invalidError) {
		// Not what I wanted to do, but I can't let Win8.0 blame the user for IE not able to play m3u8s.
		try {
			// This is really a readonly property, so I shouldn't be allowed to do this,
			// but I suspect that since error.hasOwnProperty('code') is false, maybe I can set it.
			error.code = MediaError.MEDIA_ERR_SRC_NOT_SUPPORTED;
		} catch (e) {
			logger.debug("Failed to set error.code.  That's fine, I didn't want to do it anyway.");
		}
	}

	return notSupported || invalidError;
}

var PilotEpisode = Class.extend({

	init : function (player) {
		this._started = false;
		this._addedListeners = false;
		this._player = player;
		this._player.eRendered.addListener(this._onPlayerRendered, this);

		this.onBuffering = new Event();
		this.onConcluded = new Event();

		this._pilotEvents = [];

		this._onPilotEvent = Utils.bind(this._onPilotEvent, this);
		this._onPilotTimeout = Utils.bind(this._onPilotTimeout, this);

		// Desktop Safari fires "stalled" event after ~3000ms; setting timeout slightly further out.
		this._pilotTimer = Timing.eventually(this._onPilotTimeout, MAX_ATTEMPT_DURATION);

		this._addEventListener = Utils.bind(this.addEventListener, this);
		this._removeEventListener = Utils.bind(this.removeEventListener, this);
	},

	_onPlayerRendered : function () {
		// Wait until Player is rendered before registering a pointer to the video element.
		this._element = this._player.getVideoElement();
	},

	addEventListener : function (event, listener) {
		this._element.addEventListener(event, listener, (event === 'error'));
	},

	removeEventListener : function (event, listener) {
		this._element.removeEventListener(event, listener, (event === 'error'));
	},

	_queuePilotEvent : function (event) {
		this._pilotEvents.push(event);
		logger.log("queued pilot events: ", this._pilotEvents.length);
	},

	_clearPilotEvents : function () {
		logger.log("clearing queued Pilot events");
		this._pilotEvents.length = 0;
	},

	_dispatchPilotEvent : function (event) {
		logger.log("dispatching Pilot event '" + event.type + "'");
		try {
			this._element.dispatchEvent(event);
		} catch (e) {
			logger.debug("failed to dispatch event '" + event.type + "': " + e.message);
		}
	},

	_dispatchPilotEvents : function () {
		var q = this._pilotEvents;
		var length = q.length;

		logger.log("_dispatchPilotEvents", "queue length = " + length);

		if (!length) return;

		var last = length - 1;

		// Call dispatchEvent for all but the last event in the queue.
		for (var i = 0; i < last; ++i) {
			this._dispatchPilotEvent(q[i]);
		}

		// Assuming the last event in the queue is the current event.
		// Deferring execution to avoid throwing the exception:
		// "Failed to execute 'dispatchEvent' on 'EventTarget': The event is already being dispatched."
		window.setTimeout(Utils.bind(this._dispatchPilotEvent, this, q[last]), 0);

		this._clearPilotEvents();
	},

	_onPilotEvent : function (event) {
		logger.log("_onPilotEvent", event.type);

		var video = event.target;

		switch (event.type) {
			// NOTE: Win8.0 Phone IE does not trigger 'loadedmetadata'.
			// NOTE: Win8.0 Phone IE *does* trigger 'loadeddata', even on m3u8s which it cannot play.
			// Adding 'playing' as a fallback success event -- the fallback failure is the timeout.
			case 'loadedmetadata':
			case 'playing':
				// success - queue 'loadedmetadata' event and end pilot
				this._queuePilotEvent(event);
				this._concludePilot();
			break;
			case 'error':
				// failure - only end pilot on SRC_NOT_SUPPORTED error
				// discard all other errors
				if (video && isMediaError(video.error) && isErrorCausedBySrcNotSupported(video.error)) {
					this._queuePilotEvent(event);
					this._next();
				}
			break;
			case 'waiting':
				// fire immediately, unless we've already done so
				if (!this._pilotBuffering) {
					this._pilotBuffering = true;
					this.onBuffering.dispatch();
				}
				// push timeout farther out
				this._startPilotTimer();
			break;
			case 'progress':
			case 'suspend':
			case 'stalled':
				// push timeout farther out -- don't queue events to replay later
				this._startPilotTimer();
			break;
			default:
				// queue all other events
				this._queuePilotEvent(event);
		}
	},

	_addPilotListener : function (eventName) {
		this._addEventListener(eventName, this._onPilotEvent);
	},

	_addPilotListeners : function () {
		if (this._addedListeners) {
			logger.log("_addPilotListeners", "already listening to events!");
			return;
		}

		logger.log("_addPilotListeners", mediaEvents);

		if (!this._element) {
			logger.error("_addPilotListeners", "reference to video element was not fulfilled");
			this._concludePilot();
			return;
		}

		Utils.each(mediaEvents, this._addPilotListener, this);

		this._addedListeners = true;
	},

	_removePilotListener : function (eventName) {
		this._removeEventListener(eventName, this._onPilotEvent);
	},

	_removePilotListeners : function () {
		if (!this._addedListeners) {
			logger.log("_removePilotListeners", "already NOT listening to events!");
			return;
		}

		logger.log("_removePilotListeners", mediaEvents);

		Utils.each(mediaEvents, this._removePilotListener, this);

		this._addedListeners = false;
	},

	_stopPilotTimer : function () {
		logger.log("_stopPilotTimer");
		this._pilotTimer.stop();
	},

	_hasExceededAttemptMaxDuration : function () {
		var delta = Utils.now() - this._pilotAttemptStart;
		return delta > MAX_ATTEMPT_DURATION;
	},

	_startPilotTimer : function () {
		logger.log("_startPilotTimer");
		this._pilotTimer.start();
	},

	_onPilotTimeout : function () {
		logger.log("_onPilotTimeout");
		if (!this._hasPilotCompleted)
			this._next();
	},

	_concludePilot : function () {
		if (this._hasPilotCompleted) {
			logger.log("_concludePilot", "already concluded!");
			return;
		}

		logger.log("_concludePilot");

		this._hasPilotCompleted = true;

		this._transitionBackToPlayer();
	},

	_teardownPilot : function () {
		this._stopPilotTimer();
		this._removePilotListeners();
	},

	_transitionBackToPlayer : function () {
		this._teardownPilot();
		this.onConcluded.dispatch();
		this._dispatchPilotEvents();
		this.stop();
	},

	_hasExceededSessionMaxDuration : function () {
		var delta = Utils.now() - this._pilotSessionStart;
		return delta > MAX_SESSION_DURATION;
	},

	_next : function () {
		// If there are no more URLs to try, quit miserably.
		if (!this._playlist.length) {
			logger.debug("_next", "no more video URLs to try");
			this._concludePilot();
			return;
		}

		// If we've exceeded our time trial, quit miserably.
		if (this._hasExceededSessionMaxDuration()) {
			logger.debug("_next", "the pilot session has exceeded the maximum allowed duration");
			this._concludePilot();
			return;
		}

		// Continue with next video.

		var url = this._playlist.shift();

		logger.debug("_next", "attempting to play '" + url + "'");

		// Clear the queued events from the previous attempt.
		this._clearPilotEvents();

		// Desktop Safari does not fire "error" event for 404 URLs,
		// so we set up a timer to error-out.
		// Can't use "stalled" event, as Safari may stall on valid URLs.
		// Note when this attempt was started; fail if elapsed time exceeds PILOT_ATTEMPT_TIMEOUT.
		this._pilotAttemptStart = Utils.now();
		this._startPilotTimer();

		this._playUrl(url);
	},

	_playUrl : function (url) {
		logger.log("playing URL '" + url + "'");

		var video = this._element;

		video.src = url;
		video.load();
		video.play();
	},

	_setupPilot : function () {

		this._pilotBuffering = false;

		this._hasPilotCompleted = false;

		// Tell player to stop listening to video element.
		this._player.removePlayerListeners();

		// Intercept video element events until we've determined what URL to play.
		this._addPilotListeners();

	},

	// Preflight a playlist: keep trying videos until success or timeout.
	start : function (playlist) {
		// Stop any previous preflighting, since this object is re-used.
		if (this._started) {
			this.stop();
		}

		logger.log("start");

		this._started = true;
		this._pilotSessionStart = Utils.now();
		this._playlist = playlist;

		this._setupPilot();

		this._next();
	},

	stop : function () {
		if (!this._started) return;

		logger.log("stop");

		this._started = false;
		this._teardownPilot();
		this._clearPilotEvents();
	},

	// A player could reuse the same PilotEpisode object, just call start()
	// with a new playlist.  However, once dispose()'ed, this object is dead.
	dispose : function () {
		logger.log("dispose");

		this.stop();
		this._element = null;
		this._player = null;
		this._pilotTimer = null;
	}

});


// Export public APIs
module.exports = PilotEpisode;

},{"../../cvp/customevent":16,"../../cvp/log":28,"../../cvp/util/timing":48,"../../cvp/utils":50,"../../vendor/Class":93}],88:[function(require,module,exports){
/* html5/player/videobegetter.js */

// Consolidating `DummyPlayStartStrategy` and `VideoPrimer` functionality.

// Load dependent modules
var Class = require('../../vendor/Class');
var Event = require('../../cvp/customevent');
var Log = require('../../cvp/log');
var App = require('../../core/app');
var Utils = require('../../cvp/utils');


var _logger = Log.getLogger('VideoBegetter');

var videoEvents = [
	'click',  // Support for embed players that have no big play button
	'play'  // This is probably all we really need.
];

// Prior to loading actual content, Load a dummy video (ten seconds of black)
// and show the poster for the content that will be played.
//
// On user initialization, listen for the first available
// event and the let controller know the video element is ready for use.
//
// Doing this allows us to have control over the play flow for both ads and content.
// See [CVP-676] for more detail.

var VideoBegetter = Class.extend({

	init : function () {
		_logger.log('init');

		this._listening = false;
		this._primed = false;
		this._showControls = true;
		this._videoElement = document.createElement('video');
		this._onActivation = Utils.bind(this._onActivation, this);

		this.eActivate = new Event();
	},

	dispose : function () {
		_logger.log('dispose');

		this.teardown();
		this._videoElement = null;
	},

	getElement : function () {
		return this._videoElement;
	},

	isPrimed : function () {
		return this._primed;
	},

	// Add/remove listeners for the relevent media callbacks
	listen : function (bool) {
		if (bool === this._listening) return;

		_logger.log('listen', bool);

		this._listening = bool;
		var method = bool ? 'addEventListener' : 'removeEventListener';
		for (var i = 0, len = videoEvents.length; i < len; ++i) {
			this._videoElement[method](videoEvents[i], this._onActivation, true);
		}
	},

	setPoster : function (url) {
		_logger.log('setPoster(url="' + url + '")');

		var video = this._videoElement;

		video.poster = App.BLANK_IMG_URL;

		if (url != null) {
			video.poster = url;
		}
	},

	setControls : function (preferred) {
		_logger.log('setControls(preferred="' + preferred + '")');
		this._showControls = preferred;

		if (!preferred) {
			this.hideControls();
		}
	},

	showControls : function () {
		if (!this._videoElement) {
			_logger.warn('showControls', 'no video element');
			return;
		}

		if (!this._showControls) {
			_logger.log('showControls', 'disallowed by configuration');
			return;
		}

		_logger.log('showControls', 'showing controls');
		this._videoElement.controls = true;
	},

	hideControls : function () {
		if (!this._videoElement) {
			_logger.warn('hideControls', 'no video element');
			return;
		}

		_logger.log('hideControls', 'hiding controls');
		this._videoElement.controls = false;
	},

	prime : function () {
		_logger.log('prime');

		if (this.isPrimed()) {
			this.listen(false);
			return;
		}

		this.listen(true);

		var video = this._videoElement;

		// Provide controls to the user, just in case this fails.
		this.showControls();

		// Remove any currently-loaded video:  it's not necessary to prime.
		// video.removeAttribute('src');  -- throws if you goFullscreen()!
		video.src = App.BLANK_MP4_URL;

		// load() is enough to trigger activation and prime the video element.
		video.load();
	},

	shim : function (opts) {
		var cfg = Utils.extend({}, opts);

		_logger.log('shim', cfg);

		this.listen(true);

		var video = this._videoElement;

		this.showControls();

		video.src = App.BLANK_MP4_URL;

		if (cfg.poster != null)
			this.setPoster(cfg.poster);

		if (cfg.autostart) {
			video.load();
			video.play();
		}
	},

	teardown : function () {
		_logger.log('teardown');

		this.listen(false);
	},

	_onFirstActivation : function () {
		_logger.log('_onFirstActivation');

		// Disallow subsequent calls.
		if (this._primed) return;

		// The video element is primed after the first action.
		this._primed = true;

		// Change the preload attribute from "none" to "metadata"
		// to appease Telemetry, who refuses to load() before play().
		// With @preload="none", you **must** load() before play().
		// But after we've already primed the video element, what's
		// the harm in the user preloading the next ad/content video?
		this._videoElement.preload = 'metadata';
	},

	_onActivation : function () {
		_logger.log('_onActivation');

		// The first successful event is all we need, thank you.
		this.listen(false);

		// Handle first ever activate event.
		if (!this.isPrimed()) {
			this._onFirstActivation();
		}

		// Inform others of activation.
		this.eActivate.dispatch();
	}

});


// Export public APIs
module.exports = VideoBegetter;

},{"../../core/app":8,"../../cvp/customevent":16,"../../cvp/log":28,"../../cvp/utils":50,"../../vendor/Class":93}],89:[function(require,module,exports){
/* html5/player/videoplayer.js */

// Load dependent modules
var BasePlayer = require('./baseplayer');
var PilotEpisode = require('./pilotepisode');
var FileSelectionHandler = require('./fileselection');
var FullscreenMonitor = require('./fullscreenmonitor');
var DOMEventHandler = require('./domeventhandler');
var App = require('../../core/app');
var Browser = require('../../cvp/browser');
var CVP = require('../../cvp/static');
var Utils = require('../../cvp/utils');
var Log = require('../../cvp/log');
var Timing = require('../../cvp/util/timing');


var logger = Log.getLogger('VideoPlayer');
var MediaError = window.MediaError || function () {};

function isMediaError(error) {
	// var result = error instanceof MediaError && !Utils.isEmpty(error);

	// if (!result) {
		// We have to expand the detection, as Win8 IE sets an "error" property
		// on the video element; however, the "code" of that error is not set on
		// that MediaEvent object itself, but is inherited via its prototype.
		// Also, the value of the code was 1 -- MEDIA_ERR_ABORTED -- when the spec
		// states that in the case of an "error" event, the code should be
		// MEDIA_ERR_NETWORK (2) or higher, and all other browsers are using
		// MEDIA_ERR_SRC_NOT_SUPPORTED (4) when they are unable to play m3u8.
		logger.debug("error instanceof MediaError: ", error instanceof MediaError);
		logger.debug("error.code:", error.code);
		logger.debug("Utils.hasOwn(error, 'code'):", Utils.hasOwn(error, 'code'));
	// }

	return error instanceof MediaError && typeof error.code === 'number';
}

function getCvpErrorForMediaError(error) {

	switch (error.code) {
		// Source: http://www.w3.org/TR/html5/video.html#mediaerror
		case MediaError.MEDIA_ERR_ABORTED:
			return CVP.MEDIA_ERR_ABORTED;
		case MediaError.MEDIA_ERR_NETWORK:
			return CVP.MEDIA_ERR_NETWORK;
		case MediaError.MEDIA_ERR_DECODE:
			return CVP.MEDIA_ERR_DECODE;
		case MediaError.MEDIA_ERR_SRC_NOT_SUPPORTED:
			return CVP.MEDIA_ERR_SRC_NOT_SUPPORTED;
	}

	return CVP.MEDIA_ERR_UNKNOWN;
}

var VideoPlayer = BasePlayer.extend({

	init : function(videoElement, options) {
		logger.log("init");

		// Used by this.embed(), which is triggered via this._super().
		this._videoElement = videoElement;

		this._super(options);

		// For iOS 5.0 and later, Safari opts in by default;
		// set 'iosAirplay' to "deny" to disallow AirPlay.
		var airPlaySetting = this._config.configInfo.iosAirplay;

		if (Browser.isOS(Browser.IOS) && !Utils.undef(airPlaySetting)) {
			this.playerOptions['x-webkit-airplay'] = airPlaySetting;
		}

		this.playerOptions.controls = this.options.controls;

		this._playlist = [];

		this._addedListeners = false;

		this._muted = false;
		this._fullscreen = false;

		// Throttling playhead to fire at most once in 250 ms to match what was seen on Flash player.
		this.updatePlayhead = Utils.throttle(this.firePlayhead, 250, { leading : true, trailing : false });

		// Throttle seeking emissions to once per 250ms.
		this.fireSeek = Utils.throttle(this.fireSeek, 250, { leading : true, trailing : false });

		this.fireSeekEnd = Utils.bind(this.fireSeekEnd, this);

		// Manually trigger "seeked" if we have not received seeking/seeked in over a second.
		this._seekEndTimer = Timing.eventually(this.fireSeekEnd, 1000);

		this._pilotEpisode = new PilotEpisode(this);
		// PilotEpisode fires a buffering event outside of the HTML5 video element,
		// as we've removed the player's listeners, but want to inform the user
		// (analytics) that content is forthcoming.  (Necessary after separating PilotMode.)
		// The only reason for this was to appease Analytics, as no callbacks are fired during
		// PilotMode (while various URLs are tried and tested) and as the PilotMode concludes,
		// the callbacks are fired in rapid succession -- we cannot delay; the video is playing!
		// (The alternative is to automatically call onContentBuffering once PilotMode starts,
		// but what if no viable URLs were found, leading to an error -- what was buffered?)
		this._pilotEpisode.onBuffering.addListener(this._onContentBuffering, this);
		this._pilotEpisode.onConcluded.addListener(this._onPilotConcluded, this);

		this._fileSelectionHandler = new FileSelectionHandler(this._config);
		this._fullscreenMonitor = new FullscreenMonitor();
		this.eRendered.addListener(this.onRender, this);

		this._eventListeners = {
			"loadeddata" : Utils.bind(this._onLoadedData, this),
			"play" : Utils.bind(this._onContentPlay, this),
			"ended" : Utils.bind(this._onContentEnd, this),

			"playing" : Utils.bind(this._onContentPlaying, this),
			"pause" : Utils.bind(this._onContentPause, this),
			"timeupdate" : Utils.bind(this._onContentPlayhead, this),

			"error" : Utils.bind(this._onContentError, this),
			"seeking" : Utils.bind(this._onContentSeeking, this),
			"seeked" : Utils.bind(this._onContentSeeked, this),
			"stalled" : Utils.bind(this._onContentBuffering, this),
			"waiting" : Utils.bind(this._onContentBuffering, this),
			"canplay" : Utils.bind(this._onContentBuffered, this),
			"volumechange" : Utils.bind(this._onContentVolume, this)
		};

		this._addEventListener = Utils.bind(this.addEventListener, this);
		this._removeEventListener = Utils.bind(this.removeEventListener, this);
		this._manuallyCallEnded = Utils.bind(this._manuallyCallEnded, this);
		this._playIfPlayerIsPaused = Utils.bind(this._playIfPlayerIsPaused, this);
		this._ensureLoadedMetaDataFiresOnce = Utils.bind(this._ensureLoadedMetaDataFiresOnce, this);

		this._endedWatch = Timing.eventually(this._manuallyCallEnded, 1000);
	},

	_setupEnsurePlayWhenAbleHandler : function () {
		if (this._ensurePlayWhenAbleHandler) return;
		// this.element is set post-rendered
		this._ensurePlayWhenAbleHandler = new DOMEventHandler(this.element, 'canplay', this._playIfPlayerIsPaused, true).once();
	},

	_disposeEnsurePlayWhenAbleHandler : function () {
		if (!this._ensurePlayWhenAbleHandler) return;
		this._ensurePlayWhenAbleHandler.dispose();
		this._ensurePlayWhenAbleHandler = null;
	},

	_setupEnsureLoadedMetaDataHandler : function () {
		if (this._ensureLoadedMetaDataHandler) return;
		// this.element is set post-rendered
		this._ensureLoadedMetaDataHandler = new DOMEventHandler(this.element, 'loadedmetadata loadeddata playing', this._ensureLoadedMetaDataFiresOnce, true).once();
	},

	_disposeEnsureLoadedMetaDataHandler : function () {
		if (!this._ensureLoadedMetaDataHandler) return;
		this._ensureLoadedMetaDataHandler.dispose();
		this._ensureLoadedMetaDataHandler = null;
	},

	_disposeEndedWatch : function () {
		this._endedWatch.stop();
		this._endedWatch = null;
	},

	_setupFullscreenMonitor : function () {
		this._fullscreenMonitor.setup(this);
		this._fullscreenMonitor.onFullscreenChange.addListener(this._onFullscreenChange, this);
		this._fullscreenMonitor.onFullscreenError.addListener(this._onFullscreenError, this);
	},

	_teardownFullscreenMonitor : function () {
		if (!this._fullscreenMonitor) return;
		this._fullscreenMonitor.onFullscreenChange.removeListener(this._onFullscreenChange, this);
		this._fullscreenMonitor.onFullscreenError.removeListener(this._onFullscreenError, this);
		this._fullscreenMonitor.teardown();
	},

	_disposeFullscreenMonitor : function () {
		if (!this._fullscreenMonitor) return;
		this._teardownFullscreenMonitor();
		this._fullscreenMonitor.dispose();
		this._fullscreenMonitor = null;
	},

	addEventListener : function (event, listener) {
		this.element.addEventListener(event, listener, (event === 'error'));
	},

	removeEventListener : function (event, listener) {
		this.element.removeEventListener(event, listener, (event === 'error'));
	},

	showControls : function () {
		if (!this.element) {
			logger.warn("showControls", "no video element");
			return;
		}

		if (!this.playerOptions.controls) {
			logger.log("showControls", "disallowed by configuration");
			return;
		}

		logger.log("showControls", "showing controls");
		this.element.controls = true;
	},

	hideControls : function () {
		if (!this.element) {
			logger.warn("hideControls", "no video element");
			return;
		}

		logger.log("hideControls", "hiding controls");
		this.element.controls = false;
	},

	getPlayerElement : function () {
		return this.element;
	},

	getVideoElement : function () {
		return this.element;
	},

	getPlaybackRate : function () {
		return (this.element && this.element.playbackRate) || 1;
	},

	getCurrentSrc : function () {
		return this.element && this.element.currentSrc;
	},

	getCurrentTime : function () {
		return (this.element && this.element.currentTime) || 0;
	},

	getDuration : function () {
		return (this.element && this.element.duration) || (this._catalogEntry && this._catalogEntry.getTrt()) || 0;
	},

	dispose : function () {
		this.eRendered.removeListener(this.onRender, this);

		this._seekEndTimer.stop();
		this._seekEndTimer = null;

		this._disposeFullscreenMonitor();
		this._disposeEnsurePlayWhenAbleHandler();
		this._disposeEnsureLoadedMetaDataHandler();
		this._disposeEndedWatch();

		this._pilotEpisode.onBuffering.removeListener(this._onContentBuffering, this);
		this._pilotEpisode.onConcluded.removeListener(this._onPilotConcluded, this);

		this.reset();

		this._pilotEpisode.dispose();
		this._pilotEpisode = null;

		this.remove();

		this._videoElement = null;
		this.element = null;
	},

	onRender : function () {
		this._setupEnsurePlayWhenAbleHandler();
		this._setupEnsureLoadedMetaDataHandler();
		this._setupFullscreenMonitor();

		var config = this._config.configInfo;

		if (config.volume)
			this.setVolume(config.volume);

		if (config.muted)
			this.mute();
	},

	firePause : function() {
		var currentTime = this.getCurrentTime();

		this.ePlayerPaused.dispatch(this.getContentId(), this.getCurrentTime(), this._paused);

		this.eContentPause.dispatch(this.getContentId(), this._paused);

		this._lastPlayhead = currentTime;
	},

	fireBuffering : function() {
		this.eContentBuffering.dispatch(this.getContentId(), this._buffering);
	},

	fireMetadata : function() {
		// this.eContentMetadata.dispatch(this.getContentId(), this.element.duration, this.element.videoWidth, this.element.videoHeight);
		// HACK if the browser doesn't give us duration at 'loadedmetadata', then we report what the customer gave us
		this.eContentMetadata.dispatch(this.getContentId(), this.getDuration(), this.element.videoWidth, this.element.videoHeight);
	},

	firePlayhead : function () {
		var currentTime = this.getCurrentTime();

		this.eContentPlayhead.dispatch(this.getContentId(), currentTime, this.getDuration(), -1);

		this._lastPlayhead = currentTime;
	},

	fireSeek : function () {
		var currentTime = this.getCurrentTime();

		this._seeking = true;
		this.eContentSeek.dispatch(this.getContentId(), this._lastPlayhead, currentTime, this.getDuration());

		this._lastPlayhead = currentTime;
	},

	fireSeekEnd : function () {
		if (!this._seeking) return;

		var currentTime = this.getCurrentTime();

		this._seeking = false;
		this._seekEndTimer.stop();
		this.eContentSeekEnd.dispatch(this.getContentId(), this._timeAtSeekBegin, currentTime, this.getDuration());

		this._lastPlayhead = currentTime;
	},

	_getCSSDimensionValue : function (value) {
		var numberRegex = /^\d+$/;
		return numberRegex.test(value) ? value + 'px' : value;
	},

	_setCSSDimension : function (el, prop, value) {
		el.style[prop] = this._getCSSDimensionValue(value);
	},

	_setWidth : function (el, val) {
		this.width = val;
		this._setCSSDimension(el, 'width', val);
	},

	_setHeight : function (el, val) {
		this.height = val;
		this._setCSSDimension(el, 'height', val);
	},

	_setAttribute : function (key, value) {
		try {
			this.element.setAttribute(key, value);
		} catch (e) {
			logger.error('Exception thrown while attempting to set attribute "' + key + '" to value "' + value + '" (' + typeof value + '): ' + e.message);
		}
	},

	_removeAttribute : function (key) {
		try {
			this.element.removeAttribute(key);
		} catch (e) {
			logger.error('Exception thrown while attempting to remove attribute "' + key + '": ' + e.message);
		}
	},

	embed : function (containerElement) {
		// was: Utils.template("<video id=\"{0}\" width={1} height={2} preload=\"none\" controls {3}></video>", this.elementId, this.width, this.height, this.airplay);

		this._videoElement.id = this.elementId;
		this._setWidth(this._videoElement, this.width);
		this._setHeight(this._videoElement, this.height);

		// setting src prior to requesting fullscreen avoids DOM Exception 11 in Mobile Safari
		this._videoElement.src = App.BLANK_MP4_URL;

		this._videoElement.preload = 'none';
		this._videoElement.controls = this.playerOptions.controls;

		// setting container element to the same dimensions as the player to avoid oddities displayed in iOS 7 and in Chrome
		this._setWidth(containerElement, this.width);
		this._setHeight(containerElement, this.height);
		containerElement.appendChild(this._videoElement);

		// verify embedding took place
		if (this._videoElement.parentNode === containerElement) {
			this._videoElement = null;
			return true;
		}

		return false;
	},

	_onPilotConcluded : function () {
		var currentSrc = this.element.currentSrc;
		logger.log("_onPilotConcluded", currentSrc);
		this._currentSrc = currentSrc;
		this.setDefaultOptions();
		this.addPlayerListeners();
	},

	_resetPlayerStatesForPlayStart : function () {
		logger.log("_resetPlayerStatesForPlayStart");

		this._playlist.length = 0;
		this._lastPlayhead = 0;

		this._metadataEmitted = false;
		this._playEmitted = false;

		this._stopped = false;
		this._paused = false;
		this._buffering = false;
		this._seeking = false;

		// Mute status is not altered by content change.
		// Fullscreen status is not altered by content change.
	},

	enableCrossOrigin : function () {
		this._setAttribute('crossOrigin', this.options.crossOrigin || 'anonymous');
	},

	disableCrossOrigin : function () {
		this._removeAttribute('crossOrigin');
	},

	play : function (catalogEntry) {
		logger.log("play");

		var previousEntry = this._catalogEntry;
		var previousVideoUrl = this._currentSrc;

		var currentEntry = this._catalogEntry = catalogEntry || previousEntry;

		this._resetPlayerStatesForPlayStart();

		// If we're replaying/resuming and already know a valid URL, skip pilot.
		// If it's a new catalogEntry, or if we didn't get a valid URL, try pilot.
		if (currentEntry !== previousEntry || !previousVideoUrl)
		{
			this._currentSrc = null;

			this._setPoster();

			if (catalogEntry.getClosedCaptions().length) {
				this.enableCrossOrigin();
				this.element.innerHTML = this._getCaptionTrackMarkup(catalogEntry);
			}
			else {
				this.disableCrossOrigin();
				this.element.innerHTML = '';
			}

			this._updatePlaylist(this._playlist, currentEntry);

			switch (this._playlist.length) {

				// manually trigger failure if no video URLs were selected from <file> nodes
				case 0:
					this.eContentError.dispatch(this.getContentId(), "No content URL selected.");
				return;

				// skip Pilot Mode if there's only one video to try
				case 1:
					logger.log("skipping Pilot Mode as there is only one video in the playlist");
					this._currentSrc = this._playlist[0];
				break;

				// Pilot Mode: try each video in playlist until something works (or we give up)
				default:
					this._pilotEpisode.start(this._playlist);
			}
		}

		// If we have a URL, play it; if we don't, waiting on PilotMode.
		if (this._currentSrc)
		{
			this.setDefaultOptions();
			this.addPlayerListeners();

			var url = this._currentSrc;
			var el = this.element;

			logger.log("playing URL '" + url + "'");

			el.src = url;
			el.load();
			el.play();
		}
	},

	_getCaptionTrackMarkup : function (catalogEntry, language) {
		if (!language) {
			language = "en";
		}

		var tracks = Utils.filter(catalogEntry.getClosedCaptions(), function (track) {
			return track.url;
		});

		var selectedTrack;

		if (tracks.length) {

			// Try to find the specified language track first...
			selectedTrack = Utils.find(tracks, function (track) {
				return track.lang === language;
			});

			// ...if that fails, try a track that doesn't specify language.
			if (!selectedTrack) {
				selectedTrack = Utils.find(tracks, function (track) {
					return !track.lang;
				});
			}

			if (selectedTrack) {
				return '<track kind="captions" label="' + selectedTrack.label + '" srclang="' + selectedTrack.lang + '" src="' + selectedTrack.url + '" default>';
			}
		}

		return '';
	},

	// instead of _getFileUrl... (for file selection)
	_updatePlaylist : function (list, catalogEntry)
	{
		logger.log("_updatePlaylist");

		// empty list
		list.length = 0;

		if (catalogEntry.isExpired()) {
			// TODO - create HTML5 configs for "expired" or "not available" video and poster.
			//
			// var videoNotAvailableUrl = this._config.configInfo.videoNotAvailableUrl;
			//
			// if (!Utils.isEmpty(videoNotAvailableUrl)) {
			// 	// Use the provided URL for expired content.
			// 	list.push(videoNotAvailableUrl);
			// }
			// else {
			// 	// Populate playlist with default file-selection; skip page and config strategies.
			// 	list.push.apply(list, this._fileSelectionHandler.getDefaultPlaylistForEntry(catalogEntry));
			// }
			//
			// Can't use the above because 'videoNotAvailableUrl' might be a Flash video asset.
			// Using our hard-coded MP4 instead.
			list.push(App.EXPIRED_MP4_URL);
		}
		else {
			// Populate playlist via file-selection utilizing page and config strategies.
			list.push.apply(list, this._fileSelectionHandler.getPlaylistForEntry(catalogEntry));
		}

		logger.debug("internal playlist [" + list.length + "]:", list);
	},

	pause : function()
	{
		this.element.pause();
	},

	resume : function()
	{
		this.element.play();
	},

	reset : function ()
	{
		this.pause();
		this._pilotEpisode.stop();
		this.removePlayerListeners();
		this._clearPoster();
		this.hideControls();
		this.disableCrossOrigin();
		// this._removeAttribute('src');  // still need to provide a src to goFullscreen()!
		this.element.src = App.BLANK_MP4_URL;
		this.element.load();
	},

	stop : function ()
	{
		if (this._stopped) return;
		this._stopped = true;
		this.reset();
		this.ePlayerStopped.dispatch(this.getContentId());
	},

	seek : function(seconds)
	{
		this.element.currentTime = seconds;
	},

	setVolume : function(v)
	{
		this.element.volume = v;
	},

	getVolume : function()
	{
		return this.element.volume;
	},

	mute : function()
	{
		this.element.muted = true;
	},

	unmute : function()
	{
		this.element.muted = false;
	},

	isMuted : function ()
	{
		return this.element.muted;
	},

	resize : function (width, height)
	{
		this._setWidth(this.element, width);
		this._setHeight(this.element, height);

		this._onContentResize();
	},

	getFullscreenElement : function () {
		return document.fullscreenElement || document.webkitFullscreenElement || document.webkitCurrentFullScreenElement || document.mozFullscreenElement;
	},

	_requestFullscreen : function (el) {
		var methodNames = [
			'requestFullscreen',  // official spec
			'requestFullScreen',
			'webkitRequestFullscreen',
			'webkitRequestFullScreen',
			'webkitEnterFullscreen',
			'webkitEnterFullScreen',
			'mozRequestFullScreen',
			'msRequestFullscreen'
		];

		for (var name, i = 0, endi = methodNames.length; i < endi; ++i) {
			name = methodNames[i];
			if (name in el && Utils.isFunction(el[name])) {
				return el[name]();
			}
		}
	},

	_exitFullscreen : function () {
		var methodNames = [
			'exitFullscreen',  // official spec
			'cancelFullscreen',
			'cancelFullScreen',
			'webkitExitFullscreen',
			'webkitExitFullScreen',
			'webkitCancelFullscreen',
			'webkitCancelFullScreen',
			'mozCancelFullScreen',
			'msExitFullscreen'
		];

		for (var name, i = 0, endi = methodNames.length; i < endi; ++i) {
			name = methodNames[i];
			if (name in document && Utils.isFunction(document[name])) {
				return document[name]();
			}
		}

		var video = this.getVideoElement();

		if (video === this.getPlayerElement()) {
			// try video fns for iOS
			if (video.webkitDisplayingFullscreen) {
				for (var webkitFns = ['webkitExitFullscreen', 'webkitExitFullScreen'], webkitName, j = 0, endj = webkitFns.length; j < endj; ++j) {
					webkitName = webkitFns[j];
					if (webkitName in video && Utils.isFunction(video[webkitName])) {
						return video[webkitName]();
					}
				}
			}
		}
	},

	exitFullscreen : function () {
		try {
			this._exitFullscreen();
		}
		catch (e) {
			logger.debug("exitFullscreen failed", e);
		}
	},

	goFullscreen : function ()
	{
		var uiElement = this.element;
		if (this.getFullscreenElement() === uiElement) {
			// toggle off
			try {
				this._exitFullscreen();
			} catch (e) {
				logger.error("goFullscreen failed to exit fullscreen", e.message);
			}
		}
		else {
			// toggle on
			try {
				this._requestFullscreen(uiElement);
			} catch (e) {
				logger.error("goFullscreen failed to enter fullscreen", e.message);
			}
		}
	},

	getDimensions : function ()
	{
		return ({
			width : this.getWidth(),
			height : this.getHeight(),
			videoWidth : this.element.videoWidth,
			videoHeight : this.element.videoHeight
		});
	},

	setDefaultOptions : function()
	{
		this.setOptions(this.playerOptions);
	},

	setOptions : function(options)
	{
		options = options || {};

		logger.log("setOptions", options);

		Utils.each(options, function (key, value) {
			if (value === undefined || value === null) {
				this._removeAttribute(key);
			}
			else {
				this._setAttribute(key, value);
			}
		}, this);
	},

	_clearPoster : function()
	{
		this.element.poster = '';
	},

	_setPoster : function(url)
	{
		var image = !arguments.length ? this.getThumbnail() : url;

		logger.log("_setPoster", image);

		// Setting black then setting image because Safari & Chrome
		// won't show image if it was used before then removed and now reused.
		this.element.poster = App.BLANK_IMG_URL;

		if (image) {
			this.element.poster = image;
		}
	},

	getThumbnail : function() {
		return (this._catalogEntry
			? this._catalogEntry.getImageForDimensions(this.getWidth(), this.getHeight())
			: "");
	},

	getWidth : function () {
		var cssWidth = Utils.getStyle(this.getPlayerElement(), 'width');
		return cssWidth ? Utils.toInt(cssWidth) : this.width;
	},

	getHeight : function () {
		var cssHeight = Utils.getStyle(this.getPlayerElement(), 'height');
		return cssHeight ? Utils.toInt(cssHeight) : this.height;
	},

	// Listening to both "loadedmetadata" and "loadeddata" events,
	// since Win8.0 Phones don't emit "loadedmetadata",
	// and fire "loadeddata" for m3u8s that they cannot possibly play.
	_ensureLoadedMetaDataFiresOnce : function ()
	{
		if (this._metadataEmitted) return;

		this._metadataEmitted = true;
		this.fireMetadata();
	},

	_playIfPlayerIsPaused : function ()
	{
		if (this._paused) {
			logger.log("Attempting to force play since the video element was found paused.");
			this.element.play();
		}
	},

	_onLoadedData : function ()
	{
		logger.log("_onLoadedData");

		// Chrome 35 (not 38) pauses when it gets the 'waiting' event on replay.
		// On 'loadeddata', add a listener for 'canplay' event;
		// on 'canplay', attempt to play() again if the player is paused
		// (and remove the event handler from future 'canplay' events).

		this._ensurePlayWhenAbleHandler.on();
	},

	_onContentPlay : function()
	{
		logger.log("_onContentPlay");

		this._ended = false;

		// trigger onContentSeeked if play resumed
		if (this._seeking) {
			this.fireSeekEnd();
		}

		// trigger onContentPause (false) if play resumed (unpaused)
		if (this._paused) {
			this._paused = false;
			this.firePause();
		}

		// only fire onContentPlay at the beginning of content
		if (!this._playEmitted)
		{
			this._playEmitted = true;
			this.firePlay();
			this.firePlayhead();
		}
	},

	_onContentPlaying : function()
	{
		logger.log("_onContentPlaying");

		// remove poster as Android Chrome will happily play video behind it
		this._clearPoster();
	},

	_onContentPause : function()
	{
		logger.log("_onContentPause");
		// trigger onContentPause (true) if paused
		this._paused = true;
		// but do not emit onContentPause at end of content (Flash Parity)
		if (this.getCurrentTime() !== this.getDuration()) {
			this.firePause();
		}
	},

	_onContentPlayhead : function()
	{
		// logger.log("_onContentPlayhead");

		if (this._seeking) {
			// no playhead updates while scrubbing
			return;
		}

		if (this._ended) {
			// no playhead updates after ended
			return;
		}

		var currentPlayhead = this.getCurrentTime();

		// Chrome 18 on Nexus 10 does not fire 'ended' on m3u8
		if (this._isDoingNothing()) {
			logger.debug("Suspect that the video element did not emit 'ended' event.");
			this._startEndWatcher();
		}
		else if (currentPlayhead !== this._lastPlayhead) {
			this._stopEndWatcher();
			this.updatePlayhead();
		}

	},

	_isDoingNothing : function () {
		// Chrome 18 on Nexus 10 does not fire 'ended' on m3u8:
		// The playhead is reset to zero, but it's not playing and oddly it's not paused either.
		return (
			// playhead at zero
			Utils.toInt(this.getCurrentTime()) === 0
			// playhead was recently somewhere near the end
			&& Math.abs(this._lastPlayhead - this.getDuration()) < 2
			// and not stopped, paused, seeking or buffering -- doing nothing!
			&& !this._stopped
			&& !this._paused
			&& !this._seeking
			&& !this._buffering
		);
	},

	_startEndWatcher : function () {
		this._stopEndWatcher();
		if (this._ended) return;
		this._endedWatch.start();
	},

	_manuallyCallEnded : function () {
		this._stopEndWatcher();
		if (this._ended) return;
		logger.debug("Manually calling ended because the video element did not.");
		this._onContentEnd();
	},

	_stopEndWatcher : function () {
		if (!this._endedWatch) return;
		this._endedWatch.stop();
	},

	_onContentSeeking : function ()
	{
		// Only fire seeking event if we've seeked for more than a second.
		// (Holding the playhead in place does not warrant a seeking event.)
		if (Math.abs(this.getCurrentTime() - this._lastPlayhead) < 1.0) {
			return;
		}

		if (!this._seeking) {
			this._timeAtSeekBegin = this._lastPlayhead;
		}

		this.fireSeek();

		// Wait a period of time: If we don't see another "seeking" event, then fire "seeked".
		if (this._seeking) {
			this._deferSeekEnd();
		}
	},

	_deferSeekEnd : function () {
		this._seekEndTimer.start();
	},

	_onContentSeeked : function ()
	{
		if (!this._seeking) {
			logger.debug("received 'seeked' while not seeking?");
			return;
		}

		// Defer, in case there is further seeking in progress.
		this._deferSeekEnd();
	},

	_onContentEnd : function()
	{
		if (this._ended) return;
		this._stopEndWatcher();
		this._ended = true;
		logger.log("_onContentEnd");
		this.firePlayhead();
		this.fireEnded();
	},

	_onContentError : function(event)
	{
		var video = event.target;
		var error = video.error;

		if (video !== this.element) {
			logger.error("How can this be?  We've received an error from an element we're not listening to?");
			return;
		}

		if (!error) {
			logger.error("How can this be?  We've received an 'error' event, but no 'error' property was set on the video element?");
			return;
		}

		// TODO - determine a way of differentiating an error that occurs during PilotMode
		// versus an error that occurs after a successful Pilot run.
		// The reason ... I forgot the reason.
		// I think I was going to filter out "valid" MediaErrors as errors that were greater
		// than 1, but was worried that an error

		// Observed: Switching from pre-roll ad to content video,
		// Chrome is emitting 'error' event with no error property.
		// Ignoring error if no error on video element.
		// Observed: Win8 Phone IE emitting a MediaError event
		// with 'code' of MEDIA_ERR_ABORTED when it should be
		// MEDIA_ERR_SRC_NOT_SUPPORTED.
		if (!isMediaError(error)) {
			logger.warn("Received 'error' media event without accompanying error code; ignoring as false error, so content may continue.");
			return;
		}

		var errorMessage = getCvpErrorForMediaError(error);

		logger.error("_onContentError", errorMessage);

		this.eContentError.dispatch(this.getContentId(), errorMessage);
	},

	_onContentBuffering : function()
	{
		if (this._buffering) {
			logger.log("_onContentBuffering: still buffering");
			return;
		}

		logger.log("_onContentBuffering");

		this._buffering = true;
		this.fireBuffering();
	},

	_onContentBuffered : function()
	{
		if (!this._buffering) {
			logger.log("_onContentBuffered: not even buffering");
			return;
		}

		logger.log("_onContentBuffered");

		this._buffering = false;
		this.fireBuffering();
	},

	_onContentVolume : function()
	{
		logger.log("_onContentVolume");

		var videoMuted = this.element.muted;

		// On volume change, check mute status; fire onPlayerMuted when mute status has changed.
		if (this._muted !== videoMuted) {
			this._muted = videoMuted;
			this.ePlayerMuted.dispatch(videoMuted);
		}

		// onContentVolume is triggered for volume and mute changes.
		this.eContentVolume.dispatch(videoMuted, this.element.volume);
	},

	_onContentResize : function()
	{
		logger.log("_onContentResize");
		this.eContentResize.dispatch(this.getWidth(), this.getHeight(), this._fullscreen);
	},

	_onFullscreenChange : function(isFullscreen /* , fullscreenElement */)
	{
		logger.log("_onFullscreenChange", isFullscreen);
		this._fullscreen = isFullscreen;

		// fullscreen may trigger before we've retrieved content entry, no contentId
		this.eFullscreenChange.dispatch(this._fullscreen);
	},

	_onFullscreenError : function()
	{
		logger.log("_onFullscreenError");
		this.eFullscreenError.dispatch();
	},

	addPlayerListeners : function() {
		logger.log("addPlayerListeners");

		if (!this.element) {
			logger.error("addPlayerListeners", "cannot find element to listen to!");
			return;
		}

		if (this._addedListeners) {
			logger.log("addPlayerListeners", "already listening to events!");
			return;
		}

		this._ensureLoadedMetaDataHandler.on();

		Utils.each(this._eventListeners, this._addEventListener, this);

		this._addedListeners = true;
	},

	removePlayerListeners : function() {
		logger.log("removePlayerListeners");

		if (!this.element) {
			logger.error("removePlayerListeners", "cannot find element to not listen to!");
			return;
		}

		if (!this._addedListeners) {
			logger.log("removePlayerListeners", "already stopped listening to events!");
			return;
		}

		this._ensureLoadedMetaDataHandler.off();

		Utils.each(this._eventListeners, this._removeEventListener, this);

		this._pilotEpisode.stop();

		this._addedListeners = false;
	}
});


// Export public APIs
module.exports = VideoPlayer;

},{"../../core/app":8,"../../cvp/browser":12,"../../cvp/log":28,"../../cvp/static":38,"../../cvp/util/timing":48,"../../cvp/utils":50,"./baseplayer":81,"./domeventhandler":83,"./fileselection":84,"./fullscreenmonitor":85,"./pilotepisode":87}],90:[function(require,module,exports){
/* html5/shared/playerconfig.js */

module.exports = {

	params : {},

	mapping : {},

	appConfig : {},

	containerInfo : {},

	configInfo : {
		html5Order : '',
		html5Types : '',
		omniture : {},
		share : {},
		embed : {}
	},

	adPolicy : {
		ads : {
			attr : {
				type : "NONE"
			}
		}
	},

	trackingPolicy : {
		interval : '50%'
	}

};

},{}],91:[function(require,module,exports){
/* html5/tracking/aspentracking.js */

/* eslint no-inline-comments:0 */

// Load dependent modules
var Class = require('../../vendor/Class');
var Utils = require('../../cvp/utils');
var Log = require('../../cvp/log');
var TrackingManager = require('./trackingmanager');
var AspenController = require('../aspencontroller');
var Errors = require('../../cvp/errors');


var _logger = Log.getLogger('AspenTracking');

var CONTENT_TYPE = 'content';
var AD_TYPE = 'ad';

var toNum = Utils.toFixed;

var AspenTracking = Class.extend({

	init : function (options) {
		_logger.log('init');

		this._playerController = options.playerController;
		this._cvpElementId = options.cvpElementId;

		this._trackingManager = new TrackingManager({
			logger : Log.getLogger('AspenTrackingManager'),
			playerController : this._playerController
		});

		this._isListening = false;
		this.listen(true);

		if (AspenController.isReady()) {
			this.updateTrackingInterval();
		}
		else {
			this.setTrackingInterval('23');
			this._watchAspenInit();
		}

	},

	setTrackingInterval : function (interval) {
		_logger.log('setTrackingInterval', interval);

		interval = Utils.toInt(interval) + 's';

		this._trackingManager.setTrackingInterval(interval);
	},

	switchTrackingIntervalFor : function (metric) {
		var trackingInterval;

		switch (metric) {
			case CONTENT_TYPE:
				trackingInterval = AspenController.getConfig('progressReportInterval');
			break;
			case AD_TYPE:
				trackingInterval = AspenController.getConfig('adProgressReportInterval');
			break;
		}

		if (trackingInterval) {
			_logger.log('setting tracking interval for ' + metric + ' to:', trackingInterval);
			this.setTrackingInterval(trackingInterval);
		}
	},

	updateTrackingInterval : function () {
		_logger.log('updateTrackingInterval');

		this._unwatchAspenInit();

		this.switchTrackingIntervalFor(CONTENT_TYPE);
	},

	_watchAspenInit : function () {
		AspenController.eInitSuccess.addListener(this.updateTrackingInterval, this);
	},

	_unwatchAspenInit : function () {
		AspenController.eInitSuccess.removeListener(this.updateTrackingInterval, this);
	},

	dispose : function () {
		_logger.log('dispose');

		this._unwatchAspenInit();
		this.listen(false);
		this._trackingManager.dispose();
		this._trackingManager = null;
	},

	listen : function (bool) {
		_logger.log('listen', bool);

		if (this._isListening === bool) {
			_logger.log('already ' + (bool ? '' : 'not') + ' listening');
			return;
		}

		this._isListening = bool;

		var method = bool ? 'addListener' : 'removeListener';

		this._trackingManager.eTrackingAdStart[method](this._onTrackingAdStart, this);
		this._trackingManager.eTrackingAdComplete[method](this._onTrackingAdComplete, this);
		this._trackingManager.eTrackingAdProgress[method](this._onTrackingAdProgress, this);
		this._trackingManager.eTrackingAdError[method](this._onTrackingAdError, this);

		this._trackingManager.eTrackingContentBegin[method](this._onTrackingContentBegin, this);
		this._trackingManager.eTrackingContentStart[method](this._onTrackingContentStart, this);
		this._trackingManager.eTrackingContentProgress[method](this._onTrackingContentProgress, this);
		this._trackingManager.eTrackingContentEnd[method](this._onTrackingContentEnd, this);
		this._trackingManager.eTrackingContentComplete[method](this._onTrackingContentComplete, this);
		this._trackingManager.eTrackingContentSeekEnd[method](this._onTrackingContentSeekEnd, this);

		this._trackingManager.eTrackingPaused[method](this._onTrackingPaused, this);
	},

	getCurrentPlayer : function () {
		return this._playerManager.getCurrentPlayer();
	},

	// TODO - we never supported adInfo before! what's diff from ad* events?
	// TODO - figure out how/when to wire this up!  onAdRequestEvent?!? (Flash)
	// this is fired from FW.EVENT_REQUEST_COMPLETE
	// adStart is fired from _fwconst.EVENT_RENDERER && _fwconst.RENDERER_EVENT_IMPRESSION && _fwconst.TIME_POSITION_CLASS_PREROLL
	// so maybe adStart needs to be altered, because it's likely we fire that immediately without waiting if something works
	// TODO - ask why this event exists at all if the data is in adStart/adStop
	_onTrackingAdInfo : function (data)
	{
		_logger.log('_onTrackingAdInfo');

		AspenController.send('adInfo', {
			cvpElementId : this._cvpElementId,
			contentId : data.contentId,  // <string> id of content, as determined by the client.
			videoContentCount : String(AspenController.videoCount),  // <string> The number that indexes this video played in the session
			adSection : data.adSection,  // <string> the site section sent to the ad server
			adAssetId : data.adTitle,  // <string> the video asset id sent to the ad server
			adSlotInfo : undefined  // <string> json format in string for an array of ad units. Each contains properties such as adUnit, time position, cuepoint sequence, ... etc.
		});
	},

	_onTrackingAdError : function (data)
	{
		_logger.log('_onTrackingAdError');

		var errorMessage = data.errorMessage;
		var errorCode = Errors.determineAdErrorCode(errorMessage);

		AspenController.send('error', {
			cvpElementId : this._cvpElementId,
			contentId : data.contentId,  // <string> id of content, as determined by the client.
			videoContentCount : String(AspenController.videoCount),  // <string> The number that indexes this video played in the session
			adType : data.adType,  // <string> adType could be preroll, postroll, midroll, or overlay.
			adBlockId : String(data.adBlock),  // <string> 0 for preroll, 1 for postroll if no middle rolls. For middle roll it's value is based on corresponding segment index, meantime for postroll the value will be 1 plus total number of segments
			errorMessage : errorMessage,
			errorCode : errorCode
		});
	},

	// TODO - investigate when adStart is supposed to actually fire
	_onTrackingAdStart : function (data)
	{
		_logger.log('_onTrackingAdStart');

		AspenController.send('adStart', {
			cvpElementId : this._cvpElementId,
			contentId : data.contentId,  // <string> id of content, as determined by the client.
			videoContentCount : String(AspenController.videoCount),  // <string> The number that indexes this video played in the session
			adType : data.adType,  // <string> adType could be preroll, postroll, midroll, or overlay.
			adBlockId : String(data.adBlock),  // <string> 0 for preroll, 1 for postroll if no middle rolls. For middle roll it's value is based on corresponding segment index, meantime for postroll the value will be 1 plus total number of segments
			adSection : data.adSection,  // <string> the site section sent to the ad server
			adAssetId : data.adTitle,  // <string> the video asset id sent to the ad server
			width : String(data.adWidth),  // <string> width of the ad video.
			height : String(data.adHeight),  // <string> height of the ad video.
			adDuration : String(data.adDuration),  // <string> ad duration in seconds.
			slotId : undefined,  // <string> the id of the ad slot
			adInstances : undefined  // <string> array of ads within the slot (instances contain adId, assetUrl, clickUrl, contentType)
		});
	},

	_onTrackingAdProgress : function (data)
	{
		_logger.log('_onTrackingAdProgress');

		AspenController.send('adProgress', {
			cvpElementId : this._cvpElementId,
			contentId : data.contentId,  // <string> id of content, as determined by the client.
			videoContentCount : String(AspenController.videoCount),  // <string> The number that indexes this video played in the session
			adType : data.adType,  // <string> adType could be preroll, postroll, midroll, or overlay.
			adBlockId : String(data.adBlock),  // <string> 0 for preroll, 1 for postroll if no middle rolls. For middle roll it's value is based on corresponding segment index, meantime for postroll the value will be 1 plus total number of segments
			adPlayHead : toNum(data.playheadTime),  // <string> ad playhead in seconds.
			adDuration : toNum(data.adDuration),  // <string> ad duration in seconds.
			adTimeSpent : toNum(data.adTotalPlayTime),  // TODO - find definition
			slotId : undefined,  // <string> the id of the ad slot
			adInstances : undefined  // <string> array of ads within the slot (instances contain adId, assetUrl, clickUrl, contentType)
		});
	},

	_onTrackingAdComplete : function (data)
	{
		_logger.log('_onTrackingAdComplete');

		AspenController.send('adStop', {
			cvpElementId : this._cvpElementId,
			contentId : data.contentId,  // <string> id of content, as determined by the client.
			videoContentCount : String(AspenController.videoCount),  // <string> The number that indexes this video played in the session
			adType : data.adType,  // <string> adType could be preroll, postroll, midroll, or overlay.
			adBlockId : String(data.adBlock),  // <string> 0 for preroll, 1 for postroll if no middle rolls. For middle roll it's value is based on corresponding segment index, meantime for postroll the value will be 1 plus total number of segments
			adAssetId : data.adTitle,  // <string> the video asset id sent to the ad server
			slotId : undefined  // <string> the id of the ad slot
		});
	},

	_onTrackingContentBegin : function (data)
	{
		_logger.log('_onTrackingContentBegin');

		AspenController.videoCount += 1;

		AspenController.send('contentStart', {
			cvpElementId : this._cvpElementId,
			contentId : data.contentId,  // <string> id of content, as determined by the client.
			videoContentCount : String(AspenController.videoCount),  // <string> The number that indexes this video played in the session
			originId : data.originId,
			playOptions : data.playOptions,  // <string> an object of play options
			mode : data.contentMode,  // <string> the content mode
			propertyName : data.contentSource  // <string> property domain name; eg., tbs, tnt, etc.
		});
	},

	_onTrackingContentStart : function (data)
	{
		_logger.log('_onTrackingContentStart');

		AspenController.send('contentPlay', {
			cvpElementId : this._cvpElementId,
			contentId : data.contentId,  // <string> id of content, as determined by the client.
			videoContentCount : String(AspenController.videoCount),  // <string> The number that indexes this video played in the session
			originId : data.originId,
			contentType : data.contentType,  // <string> can be VOD, LIVE or STREAM
			duration : String(data.length),  // <string> the video duration in seconds
			joinTime : String(data.joinTime),  // <string> join time for this content video on this session, in number of seconds as string.
			mediaPath : data.mediaUrl,  // <string> the URL of the asset we are playing
			bandwidth : undefined,  // <string> the current bandwidth in KBPs
			serverIp : undefined,  // <string> the server ip the player is connected to for streaming video.
			ccAvailable : undefined,  // <string> whether or not closed caption tracks exist in the video xml
			dvrAvailable : undefined  // <string> whether or not dvrEnabled is true in the video xml
		});
	},

	_onTrackingContentProgress : function (data)
	{
		_logger.log('_onTrackingContentProgress');

		var timeSpentPlaying = (
			('totalPlayTime' in data && 'adTotalPlayTime' in data)
			? ((data.totalPlayTime || 0) - (data.adTotalPlayTime || 0)).toFixed(3)
			: undefined
		);

		AspenController.send('contentProgress', {
			cvpElementId : this._cvpElementId,
			contentId : data.contentId,  // <string> id of content, as determined by the client.
			videoContentCount : String(AspenController.videoCount),  // <string> The number that indexes this video played in the session
			originId : data.originId,
			timeSpentBuffering : undefined,  // <string> elapsed buffering time in this session, in seconds.
			timeSpentPlaying : timeSpentPlaying,  // <string> elapsed playback time in this session, in seconds.
			playHead : toNum(data.playheadTime),  // <string> play head position in seconds.
			playerWidth : String(data.playerWidth),  // <string> the width of the player in pixels
			playerHeight : String(data.playerHeight),  // <string> the height of the player in pixels
			ccAvailable : undefined,  // <string> whether or not closed caption tracks exist in the video xml
			ccEnabled : undefined,  // <string> whether or not closed caption is turned on by the user
			ccTrackLang : undefined,  // <string> the closed caption track language
			dvrAvailable : undefined,  // <string> whether or not dvrEnabled is true in the video xml
			dvrEnabled : undefined,  // <string> whehter or not the user is watching in dvr mode
			dvrWindow : undefined,  // <string> window in the live stream where the user can seek for dvr
			dvrStart : undefined  // <string> the epoch based start time of the dvr window
		});
	},

	_onTrackingPaused : function (data)
	{
		_logger.log('_onTrackingPaused');

		AspenController.send('contentPause', {
			cvpElementId : this._cvpElementId,  // <string> Name that is specifically configured for a video player within CVP
			contentId : data.contentId,  // <string> id of content, as determined by the client.
			videoContentCount : String(AspenController.videoCount),  // <string> The number that indexes this video played in the session
			contentPlayHead : toNum(data.playhead),  // <string> play head position in seconds when paused or resumed.
			isPaused : String(data.paused)  // <string> true - when video is paused; false - when video is resumed playing.
		});
	},

	_onTrackingContentSeekEnd : function (data)
	{
		_logger.log('_onTrackingContentSeekEnd');

		AspenController.send('contentSeek', {
			cvpElementId : this._cvpElementId,  // <string> Name that is specifically configured for a video player within CVP
			contentId : data.contentId,  // <string> id of content, as determined by the client.
			videoContentCount : String(AspenController.videoCount),  // <string> The number that indexes this video played in the session
			seekFrom : toNum(data.previousPlayhead),  // <string> the playhead position in seconds when user started seeking.
			seekTo : toNum(data.newPlayhead),  // <string> the position of the video in seconds where the user wanted to seek to.
			dvrAvailable : undefined,  // <string> is dvrEnabled setting from video xml
			dvrEnabled : undefined  // <string> whehter or not the user is watching in dvr mode
		});
	},

	_onTrackingContentEnd : function (data)
	{
		_logger.log('_onTrackingContentEnd');

		AspenController.send('contentStop', {
			cvpElementId : this._cvpElementId,
			contentId : data.contentId,  // <string> id of content, as determined by the client.
			videoContentCount : String(AspenController.videoCount),  // <string> The number that indexes this video played in the session
			playHead : toNum(data.playheadTime),  // <string> play head position in seconds.
			segmentId : undefined  // <string> the index identifying the segment
		});
	},

	_onTrackingContentComplete : function (data)
	{
		_logger.log('_onTrackingContentComplete');

		AspenController.send('contentFinish', {
			cvpElementId : this._cvpElementId,
			contentId : data.contentId,  // <string> id of content, as determined by the client.
			videoContentCount : String(AspenController.videoCount)  // <string> The number that indexes this video played in the session
		});
	}

});


// Export public APIs
module.exports = AspenTracking;

},{"../../cvp/errors":25,"../../cvp/log":28,"../../cvp/utils":50,"../../vendor/Class":93,"../aspencontroller":59,"./trackingmanager":92}],92:[function(require,module,exports){
/* html5/tracking/trackingmanager.js */

/* eslint no-inline-comments:0, quote-props:0 */

// Load dependent modules
var Class = require('../../vendor/Class');
var Event = require('../../cvp/customevent');
var Log = require('../../cvp/log');


var _logger = Log.getLogger('TrackingManager');

var TrackingManager = Class.extend({

	setLogger : function (logger) {
		this._logger = logger;
	},

	init : function (options) {
		this.setLogger(options.logger || _logger);

		this._playerController = options.playerController;

		this._trackingInterval = 0;

		this.eTrackingAdStart = new Event();
		this.eTrackingAdComplete = new Event();
		this.eTrackingAdError = new Event();
		this.eTrackingAdProgress = new Event();
		this.eTrackingAdCountdown = new Event();
		this.eTrackingAdClick = new Event();

		this.eTrackingContentBegin = new Event();
		this.eTrackingContentPlay = new Event();
		this.eTrackingContentStart = new Event();
		this.eTrackingContentProgress = new Event();
		this.eTrackingContentEnd = new Event();
		this.eTrackingContentComplete = new Event();
		this.eTrackingContentReplay = new Event();
		this.eTrackingContentSeek = new Event();
		this.eTrackingContentSeekEnd = new Event();
		// this.eTrackingContentSegmentPlay = new Event();
		// this.eTrackingContentSegmentComplete = new Event();

		this.eTrackingFullscreen = new Event();
		this.eTrackingMuted = new Event();
		this.eTrackingPaused = new Event();

		this.reset();

		this._isListening = false;

		this.listen(true);

	},

	reset : function () {

		this._lastPlayhead = 0;
		this._lastPercent = 0;

		// ad tracking
		this._previousAdPlayhead = 0;
		this._currentAdDuration = 0;
		this._currentAdPlayhead = 0;
		this._totalAdPlayTime = 0;

		// content tracking
		this._previousContentPlayhead = 0;
		this._currentContentPlayhead = 0;

		this._totalGrossPlayTime = 0;

	},

	dispose : function () {
		this.listen(false);
		this._playerController = null;
		this._logger = null;
	},

	getCatalogEntry : function () {
		return this._playerController.getCatalogEntry();
	},

	getPlayerDimensions : function () {
		return this._playerController.getPlayerDimensions();
	},

	getAdId : function () {
		return this._playerController.getAdId();
	},

	getAdType : function () {
		return this._playerController.getAdType();
	},

	getAdSection : function () {
		return this._playerController.getAdSection();
	},

	listen : function (bool)
	{
		if (this._isListening === bool) return;

		this._isListening = bool;

		var method = bool ? 'addListener' : 'removeListener';

		this._playerController.eAdPlay[method](this._onTrackingAdStart, this);
		this._playerController.eAdEnd[method](this._onTrackingAdComplete, this);
		this._playerController.eAdError[method](this._onTrackingAdError, this);
		this._playerController.eAdPlayhead[method](this._onTrackingAdProgress, this);
		this._playerController.eAdPlayhead[method](this._onTrackingAdCountdown, this);
		this._playerController.eAdClick[method](this._onTrackingAdClick, this);

		this._playerController.eContentBegin[method](this._onTrackingContentBegin, this);
		this._playerController.eContentPlay[method](this._onTrackingContentPlay, this);
		this._playerController.eContentStart[method](this._onTrackingContentStart, this);
		this._playerController.eContentPlayhead[method](this._onTrackingContentProgress, this);
		this._playerController.eContentEnd[method](this._onTrackingContentEnd, this);
		this._playerController.eContentComplete[method](this._onTrackingContentComplete, this);
		this._playerController.eContentReplay[method](this._onTrackingContentReplay, this);
		this._playerController.eContentSeek[method](this._onTrackingContentSeek, this);
		this._playerController.eContentSeekEnd[method](this._onTrackingContentSeekEnd, this);
		// this._playerController.eContentSegmentPlay[method](this._onTrackingContentSegmentPlay, this);
		// this._playerController.eContentSegmentComplete[method](this._onTrackingContentSegmentComplete, this);

		this._playerController.eFullscreenChange[method](this._onTrackingFullscreen, this);
		this._playerController.ePlayerPaused[method](this._onTrackingPaused, this);
		this._playerController.ePlayerMuted[method](this._onTrackingMuted, this);
	},

	_canEmitProgress : function (playhead, duration) {
		var value = this._trackingInterval;

		if (!value) {
			return false;
		}

		var thisPlayhead = Math.floor(playhead);
		var thisPercent = Math.floor((playhead / duration) * 100);
		var canEmit = false;

		var match = this._intervalRegex.exec(value);
		var interval = Number(match[1]);
		var metric = match[2];

		// no duration? assume live -- can't do percentage
		if (isNaN(duration) && metric === '%') {
			interval = 30;
			metric = 's';
		}

		if (metric === 's') {
			if (interval > duration) {
				// for content shorter than the requested interval, at least fire a half-way progress event!
				interval = 50;
				metric = '%';
			}
			else
			if (thisPlayhead !== this._lastPlayhead
				&& thisPlayhead % interval === 0)
			{
				canEmit = true;
			}
		}

		if (metric === '%') {
			if (thisPercent !== this._lastPercent
				&& thisPercent < 100
				&& thisPercent % interval === 0)
			{
				canEmit = true;
			}
		}

		if (canEmit) {
			this._lastPlayhead = thisPlayhead;
			this._lastPercent = thisPercent;
		}

		return canEmit;
	},

	_onTrackingAdStart : function (token, mode, id, duration, blockId, adType, adAnalyticsData)
	{
		this._logger.log("_onTrackingAdStart", arguments);

		this._lastPlayhead = 0;
		this._lastPercent = 0;

		this._previousAdPlayhead = 0;
		this._currentAdPlayhead = 0;
		this._currentAdDuration = 0;

		var dimensions = this.getPlayerDimensions();
		var catalogEntry = this.getCatalogEntry();

		this.eTrackingAdStart.dispatch({
			'adTitle' : id, /*:String*/  // title of the ad
			'adWidth' : dimensions.videoWidth,
			'adHeight' : dimensions.videoHeight,
			'adSection' : this.getAdSection(),
			'contentId' : catalogEntry.getId(),
			'originId' : catalogEntry.getOriginId(),
			'mediaUrl' : catalogEntry.getMediaUrl(),
			'adType' : adType, /*:String*/  // type of ad (pre-roll, post-roll, mid-roll)
			'adDuration' : duration, /*:Number*/  // ad duration
			'adBlock' : blockId, /*:Number*/  // ad block number corresponding to the content segment the ad belongs
			'totalPlayTime' : this._totalGrossPlayTime, /*:Number*/  // total time spent watching ads and content
			'adTotalPlayTime' : this._totalAdPlayTime, /*:Number*/  // total time spent watching ads
			'adAnalyticsData' : adAnalyticsData /*:Object*/
		});
	},

	_onTrackingAdComplete : function (token, mode, id, blockId, adType)
	{
		this._logger.log("_onTrackingAdComplete", arguments);

		var catalogEntry = this.getCatalogEntry();

		this.eTrackingAdComplete.dispatch({
			'contentId' : catalogEntry.getId(),
			'adTitle' : id, /*:String*/  // title of the ad
			'adType' : adType, /*:String*/  // type of ad (pre-roll, post-roll, mid-roll)
			'adBlock' : blockId, /*:Number*/  // ad block number corresponding to the content segment the ad belongs
			'totalPlayTime' : this._totalGrossPlayTime, /*:Number*/  // total time spent watching ads and content
			'adTotalPlayTime' : this._totalAdPlayTime /*:Number*/  // total time spent watching ads
		});
	},

	_onTrackingAdError : function (errorMessage, blockId, adType, errors)
	{
		this._logger.log("_onTrackingAdError", arguments);

		var catalogEntry = this.getCatalogEntry();

		this.eTrackingAdError.dispatch({
			'contentId' : catalogEntry.getId(),
			'errorMessage' : errorMessage,
			'adType' : adType,
			'adBlock' : blockId,
			'errors' : errors
		});
	},

	_onTrackingAdProgress : function (playhead, duration, blockId)
	{
		// this._logger.log("_onTrackingAdProgress", arguments);

		this._currentAdPlayhead = playhead;
		this._currentAdDuration = duration;

		var delta = playhead - this._previousAdPlayhead;

		this._previousAdPlayhead = playhead;

		if (delta <= 0 || delta >= 1.25) {
			// seeking?
			return;
		}

		this._totalAdPlayTime += delta;
		this._totalGrossPlayTime += delta;

		if (!this._canEmitProgress(playhead, duration)) {
			// this._logger.log("_onTrackingAdProgress", "disallowed to emit progress event");
			return;
		}

		this.eTrackingAdProgress.dispatch({
			'totalPlayTime' : this._totalGrossPlayTime, /*:Number*/  // total time spent watching ads and content
			'playheadTime' : this._currentAdPlayhead, /*:Number*/  // playhead time for the ad
			'adDuration' : this._currentAdDuration, /*:Number*/  // ad duration
			'grossProgressMarker' : 0, /*:Number*/  // mile marker in the content of where playback is; this is based on duration and interval (includes ad time in duration)
			'adType' : this.getAdType(), /*:String*/  // type of ad (pre-roll, post-roll, mid-roll)
			'adBlock' : blockId, /*:Number*/  // ad block number corresponding to the content segment the ad belongs
			'adTotalPlayTime' : this._totalAdPlayTime /*:Number*/  // total time spent watching ads
		});
	},

	_onTrackingAdCountdown : function (playhead, duration /*, blockId*/)
	{
		// this._logger.log("_onTrackingAdCountdown", arguments);

		var countdown = Math.ceil(duration - playhead);

		if (countdown > 0 && countdown !== this._previousAdCountdown) {

			this._previousAdCountdown = countdown;

			this.eTrackingAdCountdown.dispatch({
				'adMode' : 'standard', /*:String*/  // indicates "ad" or "c3break"  // TODO - uncheat! find real adMode
				'secs' : countdown /*:Number*/   // number of seconds remaining in ad playback
			});

		}

	},

	_onTrackingAdClick : function (url)
	{
		this._logger.log("_onTrackingAdClick", arguments);

		var catalogEntry = this.getCatalogEntry();

		this.eTrackingAdClick.dispatch({
			'contentId' : catalogEntry.getId(), /*:String*/  // id of the current content
			'url' : url /*:String*/                   // URL for the ad click
		});
	},

	_onTrackingContentBegin : function(contentId, playOptions)
	{
		this._logger.log("_onTrackingContentBegin", arguments);

		this.reset();

		var catalogEntry = this.getCatalogEntry();

		this.eTrackingContentBegin.dispatch({
			'contentId' : catalogEntry.getId(), /*:String*/  // id of the current content
			'playOptions' : playOptions,
			'originId' : catalogEntry.getOriginId(),
			'contentMode' : catalogEntry.getMode(),
			'contentSource' : catalogEntry.getSource(),
			'totalPlayTime' : this._totalGrossPlayTime, /*:Number*/  // total time spent watching ads and content
			'adTotalPlayTime' : this._totalAdPlayTime /*:Number*/  // total time spent watching ads
		});
	},

	_onTrackingContentPlay : function()
	{
		this._logger.log("_onTrackingContentPlay", arguments);

		this._lastPlayhead = 0;
		this._lastPercent = 0;

		var catalogEntry = this.getCatalogEntry();

		this.eTrackingContentPlay.dispatch({
			'contentId' : catalogEntry.getId(), /*:String*/  // id of the current content
			'originId' : catalogEntry.getOriginId(),
			'mediaUrl' : catalogEntry.getMediaUrl(),
			'length' : catalogEntry.getTrt(), /*:Number*/  // length of the content in seconds
			'grossLength' : catalogEntry.getTrt(), /*:Number*/  // length of the content including ads if available
			'totalPlayTime' : this._totalGrossPlayTime, /*:Number*/  // total time spent watching ads and content
			'adTotalPlayTime' : this._totalAdPlayTime /*:Number*/  // total time spent watching ads
		});
	},

	_onTrackingContentStart : function (contentId, joinTime)
	{
		this._logger.log("_onTrackingContentStart", arguments);

		var catalogEntry = this.getCatalogEntry();

		this.eTrackingContentStart.dispatch({
			'contentId' : catalogEntry.getId(), /*:String*/  // id of the current content
			'originId' : catalogEntry.getOriginId(),
			'mediaUrl' : catalogEntry.getMediaUrl(),
			'joinTime' : joinTime,
			'length' : catalogEntry.getTrt(), /*:Number*/  // length of the content in seconds
			'grossLength' : catalogEntry.getTrt(), /*:Number*/  // length of the content including ads if available
			'totalPlayTime' : this._totalGrossPlayTime, /*:Number*/  // total time spent watching ads and content
			'adTotalPlayTime' : this._totalAdPlayTime /*:Number*/  // total time spent watching ads
		});
	},

	_onTrackingContentProgress : function (contentId, playhead, duration)
	{
		// this._logger.log("_onTrackingContentProgress", arguments);

		this._currentContentPlayhead = playhead;

		var delta = playhead - this._previousContentPlayhead;

		this._previousContentPlayhead = playhead;

		if (delta <= 0 || delta >= 1.25) {
			// DEBUG: uncomment to spam console
			// this._logger.log("_onTrackingContentProgress", 'ignoring progress during suspected seek');
			return;
		}

		this._totalGrossPlayTime += delta;

		if (!this._canEmitProgress(playhead, duration)) {
			// this._logger.log("_onTrackingContentProgress", 'disallowed to emit progress event');
			return;
		}

		var catalogEntry = this.getCatalogEntry();
		var dimensions = this.getPlayerDimensions();

		this.eTrackingContentProgress.dispatch({
			'contentId' : catalogEntry.getId(), /*:String*/  // id of the current content
			'originId' : catalogEntry.getOriginId(),
			'playerWidth' : dimensions.width,
			'playerHeight' : dimensions.height,
			'percent' : (playhead / duration) * 100, /*:Number*/  // percentage of the content viewed (0-100; not 0-1)
			'totalPlayTime' : this._totalGrossPlayTime, /*:Number*/  // total time spent watching ads and content
			'playheadTime' : this._currentContentPlayhead, /*:Number*/  // playhead time for the content
			'progressMarker' : 0, /*:Number*/       // mile marker in the content of where playback is; this is based on duration and interval
			'grossProgressMarker' : 0, /*:Number*/  // mile marker in the content of where playback is; this is based on duration and interval (includes ad time in duration)
			'adTotalPlayTime' : this._totalAdPlayTime /*:Number*/  // total time spent watching ads
		});
	},

	_onTrackingContentEnd : function()
	{
		this._logger.log("_onTrackingContentEnd", arguments);

		var catalogEntry = this.getCatalogEntry();

		this.eTrackingContentEnd.dispatch({
			'contentId' : catalogEntry.getId(), /*:String*/  // id of the current content
			'totalPlayTime' : this._totalGrossPlayTime, /*:Number*/  // total time spent watching ads and content
			'playheadTime' : this._currentContentPlayhead, /*:Number*/  // playhead time for the content
			'adTotalPlayTime' : this._totalAdPlayTime /*:Number*/  // total time spent watching ads
		});
	},

	_onTrackingContentComplete : function()
	{
		this._logger.log("_onTrackingContentComplete", arguments);

		var catalogEntry = this.getCatalogEntry();

		this.eTrackingContentComplete.dispatch({
			'contentId' : catalogEntry.getId(), /*:String*/  // id of the current content
			'percent' : 100, /*:Number*/  // percentage of the content viewed (0-100; not 0-1)
			'totalPlayTime' : this._totalGrossPlayTime, /*:Number*/  // total time spent watching ads and content
			'playheadTime' : this._currentContentPlayhead, /*:Number*/  // playhead time for the content
			'progressMarker' : 0, /*:Number*/       // mile marker in the content of where playback is; this is based on duration and interval
			'grossProgressMarker' : 0, /*:Number*/  // mile marker in the content of where playback is; this is based on duration and interval (includes ad time in duration)
			'adTotalPlayTime' : this._totalAdPlayTime /*:Number*/  // total time spent watching ads
		});
	},

	_onTrackingContentReplay : function (contentId, additionalParams)
	{
		this._logger.log("_onTrackingContentReplay", arguments);

		this.eTrackingContentReplay.dispatch({
			'contentId' : contentId,
			'additionalParams' : additionalParams
		});
	},

	_onTrackingContentSeek : function (contentId, previousPlayhead, newPlayhead, duration)
	{
		this._logger.log("_onTrackingContentSeek", arguments);

		this.eTrackingContentSeek.dispatch({
			'contentId' : contentId,
			'previousPlayhead' : previousPlayhead,
			'newPlayhead' : newPlayhead,
			'duration' : duration
		});
	},

	_onTrackingContentSeekEnd : function (contentId, previousPlayhead, newPlayhead, duration)
	{
		this._logger.log("_onTrackingContentSeekEnd", arguments);

		this.eTrackingContentSeekEnd.dispatch({
			'contentId' : contentId,
			'previousPlayhead' : previousPlayhead,
			'newPlayhead' : newPlayhead,
			'duration' : duration
		});
	},

	_onTrackingFullscreen : function (contentId, fullscreen)
	{
		this._logger.log("_onTrackingFullscreen", arguments);

		this.eTrackingFullscreen.dispatch({
			'contentId' : contentId,
			'fullscreen' : fullscreen
		});
	},

	_onTrackingMuted : function (contentId, muted)
	{
		this._logger.log("_onTrackingMuted", arguments);

		this.eTrackingMuted.dispatch({
			'contentId' : contentId,
			'muted' : muted
		});
	},

	_onTrackingPaused : function (contentId, playhead, paused)
	{
		this._logger.log("_onTrackingPaused", arguments);

		this.eTrackingPaused.dispatch({
			'contentId' : contentId,
			'playhead' : playhead,
			'paused' : paused
		});
	},

	_intervalRegex : /(\d+)([s%])?/,

	setTrackingInterval : function (value) {

		if (!value) {
			this._trackingInterval = 0;
			return;
		}

		var match = this._intervalRegex.exec(String(value));

		if (!match) {
			this._logger.error('setTrackingInterval param "' + value + '" did not match accepted regex');
			return;
		}

		var number = match[1];
		var metric = match[2] || '%';

		if (number > 100) {
			metric = 's';
		}

		this._trackingInterval = number + metric;

	}

});


// Export public APIs
module.exports = TrackingManager;

},{"../../cvp/customevent":16,"../../cvp/log":28,"../../vendor/Class":93}],93:[function(require,module,exports){
/* Simple JavaScript Inheritance
 * By John Resig http://ejohn.org/
 * MIT Licensed.
 */
// Inspired by base2 and Prototype

var initializing = false, fnTest = /xyz/.test(function(){xyz;}) ? /\b_super\b/ : /.*/;

// The base Class implementation (does nothing)
var Class = function () {};

// Create a new Class that inherits from this class
Class.extend = function extending(prop) {
  var _super = this.prototype;

  // Instantiate a base class (but only create the instance,
  // don't run the init constructor)
  initializing = true;
  var prototype = new this();
  initializing = false;

  // Copy the properties over onto the new prototype
  for (var name in prop) {
    // Check if we're overwriting an existing function
    prototype[name] = typeof prop[name] === "function" &&
      typeof _super[name] === "function" && fnTest.test(prop[name]) ?
      (function(name, fn){
        return function() {
          var tmp = this._super;

          // Add a new ._super() method that is the same method
          // but on the super-class
          this._super = _super[name];

          // The method only need to be bound temporarily, so we
          // remove it when we're done executing
          var ret = fn.apply(this, arguments);
          this._super = tmp;

          return ret;
        };
      })(name, prop[name]) :
      prop[name];
  }

  // The dummy class constructor
  function Class() {
    // All construction is actually done in the init method
    if ( !initializing && this.init )
      this.init.apply(this, arguments);
  }

  // Populate our constructed prototype object
  Class.prototype = prototype;

  // Enforce the constructor to be what we expect
  Class.prototype.constructor = Class;

  // And make this class extendable
  Class.extend = extending;

  return Class;
};

module.exports = Class;

},{}],94:[function(require,module,exports){
/**
 * easyXDM
 * http://easyxdm.net/
 * Copyright(c) 2009-2011, Øyvind Sean Kinsey, oyvind@kinsey.no.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
module.exports = (function (window, document, location, setTimeout, decodeURIComponent, encodeURIComponent) {
/*jslint evil: true, browser: true, immed: true, passfail: true, undef: true, newcap: true*/
/*global JSON, XMLHttpRequest, window, escape, unescape, ActiveXObject */

var channelId = Math.floor(Math.random() * 10000); // randomize the initial id in case of multiple closures loaded
var emptyFn = Function.prototype;
var reURI = /^((https?:)\/\/([^:\/\s]+)(:\d+)?)/; // returns groups for protocol (2), domain (3) and port (4)
var reParent = /[\-\w]+\/\.\.\//; // matches a foo/../ expression
var reDoubleSlash = /([^:])\/\//g; // matches // anywhere but in the protocol
var namespace = ""; // stores namespace under which easyXDM object is stored on the page (empty if object is global)
var easyXDM = {};
var _easyXDM = window.easyXDM; // map over global easyXDM in case of overwrite
var IFRAME_PREFIX = "easyXDM_";
var HAS_NAME_PROPERTY_BUG;

var protoObject = Object.prototype;
var toString = protoObject.toString;
var hasOwnProperty = protoObject.hasOwnProperty;

function has(obj, key) {
    return obj != null && hasOwnProperty.call(obj, key);
};

// http://peter.michaux.ca/articles/feature-detection-state-of-the-art-browser-scripting
function isHostMethod(object, property){
    var t = typeof object[property];
    return t == 'function' ||
    (!!(t == 'object' && object[property])) ||
    t == 'unknown';
}

function isHostObject(object, property){
    return !!(typeof(object[property]) == 'object' && object[property]);
}

// end

// http://perfectionkills.com/instanceof-considered-harmful-or-how-to-write-a-robust-isarray/
function isArray(o){
    return toString.call(o) === '[object Array]';
}

function isString(o){
    return toString.call(o) === '[object String]';
}

function isFunction(o){
    return toString.call(o) === '[object Function]';
}

function isObject(o) {
    return typeof o === 'object' && !!obj;
}

// end

/*
 * Cross Browser implementation for adding and removing event listeners.
 */
var on, un;
if (isHostMethod(window, "addEventListener")) {
    on = function(target, type, listener){
        target.addEventListener(type, listener, false);
    };
    un = function(target, type, listener){
        target.removeEventListener(type, listener, false);
    };
}
else if (isHostMethod(window, "attachEvent")) {
    on = function(object, sEvent, fpNotify){
        object.attachEvent("on" + sEvent, fpNotify);
    };
    un = function(object, sEvent, fpNotify){
        object.detachEvent("on" + sEvent, fpNotify);
    };
}
else {
    throw new Error("Browser not supported");
}

/*
 * Cross Browser implementation of DOMContentLoaded.
 */
var domIsReady = false, domReadyQueue = [], readyState;
if ("readyState" in document) {
    // If browser is WebKit-powered, check for both 'loaded' (legacy browsers) and
    // 'interactive' (HTML5 specs, recent WebKit builds) states.
    // https://bugs.webkit.org/show_bug.cgi?id=45119
    readyState = document.readyState;
    domIsReady = readyState == "complete" || (~ navigator.userAgent.indexOf('AppleWebKit/') && (readyState == "loaded" || readyState == "interactive"));
}
else {
    // If readyState is not supported in the browser, then in order to be able to fire whenReady functions apropriately
    // when added dynamically _after_ DOM load, we have to deduce wether the DOM is ready or not.
    // We only need a body to add elements to, so the existence of document.body is enough for us.
    domIsReady = !!document.body;
}

function dom_onReady(){
    if (domIsReady) {
        return;
    }
    domIsReady = true;
    for (var i = 0; i < domReadyQueue.length; i++) {
        domReadyQueue[i]();
    }
    domReadyQueue.length = 0;
}


if (!domIsReady) {
    if (isHostMethod(window, "addEventListener")) {
        on(document, "DOMContentLoaded", dom_onReady);
    }
    else {
        on(document, "readystatechange", function(){
            if (document.readyState == "complete") {
                dom_onReady();
            }
        });
        if (document.documentElement.doScroll && window === top) {
            var doScrollCheck = function(){
                if (domIsReady) {
                    return;
                }
                // http://javascript.nwbox.com/IEContentLoaded/
                try {
                    document.documentElement.doScroll("left");
                }
                catch (e) {
                    setTimeout(doScrollCheck, 1);
                    return;
                }
                dom_onReady();
            };
            doScrollCheck();
        }
    }

    // A fallback to window.onload, that will always work
    on(window, "load", dom_onReady);
}
/**
 * This will add a function to the queue of functions to be run once the DOM reaches a ready state.
 * If functions are added after this event then they will be executed immediately.
 * @param {function} fn The function to add
 * @param {Object} scope An optional scope for the function to be called with.
 */
function whenReady(fn, scope){
    if (domIsReady) {
        fn.call(scope);
        return;
    }
    domReadyQueue.push(function(){
        fn.call(scope);
    });
}

/**
 * Returns an instance of easyXDM from the parent window with
 * respect to the namespace.
 *
 * @return An instance of easyXDM (in the parent window)
 */
function getParentObject(){
    var obj = parent;
    if (namespace !== "") {
        for (var i = 0, ii = namespace.split("."); i < ii.length; i++) {
            obj = obj[ii[i]];
        }
    }
    return obj.easyXDM;
}

/**
 * Removes easyXDM variable from the global scope. It also returns control
 * of the easyXDM variable to whatever code used it before.
 *
 * @param {String} ns A string representation of an object that will hold
 *                    an instance of easyXDM.
 * @return An instance of easyXDM
 */
function noConflict(ns){

    window.easyXDM = _easyXDM;
    namespace = ns;
    if (namespace) {
        IFRAME_PREFIX = "easyXDM_" + namespace.replace(".", "_") + "_";
    }
    return easyXDM;
}

/*
 * Methods for working with URLs
 */

function isSupportedURI(uri) {
    return reURI.test(uri.toLowerCase());
}

/**
 * Returns  a string containing the schema, domain and if present the port
 * @param {String} url The url to extract the location from
 * @return {String} The location part of the url
 */
function getLocation(url){
    var m = url.toLowerCase().match(reURI);
    if (!m) throw new Error("Cannot obtain supported origin from URL: " + url);
    var proto = m[2], domain = m[3], port = m[4] || "";
    if ((proto == "http:" && port == ":80") || (proto == "https:" && port == ":443")) {
        port = "";
    }
    return proto + "//" + domain + port;
}

/**
 * Resolves a relative url into an absolute one.
 * @param {String} url The path to resolve.
 * @return {String} The resolved url.
 */
function resolveUrl(url){

    // replace all // except the one in proto with /
    url = url.replace(reDoubleSlash, "$1/");

    // If the url is a valid url we do nothing
    if (!url.match(/^(http||https):\/\//)) {
        // If this is a relative path
        var path = (url.substring(0, 1) === "/") ? "" : location.pathname;
        if (path.substring(path.length - 1) !== "/") {
            path = path.substring(0, path.lastIndexOf("/") + 1);
        }

        url = location.protocol + "//" + location.host + path + url;
    }

    // reduce all 'xyz/../' to just ''
    while (reParent.test(url)) {
        url = url.replace(reParent, "");
    }

    return url;
}

/**
 * Appends the parameters to the given url.<br/>
 * The base url can contain existing query parameters.
 * @param {String} url The base url.
 * @param {Object} parameters The parameters to add.
 * @return {String} A new valid url with the parameters appended.
 */
function appendQueryParameters(url, parameters){

    var hash = "", indexOf = url.indexOf("#");
    if (indexOf !== -1) {
        hash = url.substring(indexOf);
        url = url.substring(0, indexOf);
    }
    var q = [];
    for (var key in parameters) {
        if (has(parameters, key)) {
            q.push(key + "=" + encodeURIComponent(parameters[key]));
        }
    }
    return url + (url.indexOf("?") == -1 ? "?" : "&") + q.join("&") + hash;
}


// build the query object either from location.query, if it contains the xdm_e argument, or from location.hash
var query = (function(input){
    input = input.substring(1).split("&");
    var data = {}, pair, i = input.length;
    while (i--) {
        pair = input[i].split("=");
        data[pair[0]] = decodeURIComponent(pair[1]);
    }
    return data;
}(/xdm_e=/.test(location.search) ? location.search : location.hash));

/*
 * Helper methods
 */
/**
 * Helper for checking if a variable/property is undefined
 * @param {Object} v The variable to test
 * @return {Boolean} True if the passed variable is undefined
 */
function undef(v){
    return typeof v === "undefined";
}

/**
 * A safe implementation of HTML5 JSON. Feature testing is used to make sure the implementation works.
 * @return {JSON} A valid JSON conforming object, or null if not found.
 */
var getJSON = function(){
    var obj = {
        a: [1, 2, 3]
    }, json = "{\"a\":[1,2,3]}";

    if (!undef(JSON) && isFunction(JSON.stringify) && JSON.stringify(obj).replace((/\s/g), "") === json) {
        // this is a working JSON instance
        return JSON;
    }

    return null;
};

/**
 * Applies properties from the source object to the target object.<br/>
 * @param {Object} target The target of the properties.
 * @param {Object} source The source of the properties.
 * @param {Boolean} noOverwrite Set to True to only set non-existing properties.
 */
function apply(destination, source, noOverwrite){
    var member;
    for (var prop in source) {
        if (has(source, prop)) {
            if (prop in destination) {
                member = source[prop];
                if (isObject(member)) {
                    apply(destination[prop], member, noOverwrite);
                }
                else if (!noOverwrite) {
                    destination[prop] = source[prop];
                }
            }
            else {
                destination[prop] = source[prop];
            }
        }
    }
    return destination;
}

// This tests for the bug in IE where setting the [name] property using javascript causes the value to be redirected into [submitName].
function testForNamePropertyBug(){
    var form = document.body.appendChild(document.createElement("form")), input = form.appendChild(document.createElement("input"));
    input.name = IFRAME_PREFIX + "TEST" + channelId; // append channelId in order to avoid caching issues
    HAS_NAME_PROPERTY_BUG = input !== form.elements[input.name];
    document.body.removeChild(form);
}

/**
 * Creates a frame and appends it to the DOM.
 * @param config {object} This object can have the following properties
 * <ul>
 * <li> {object} prop The properties that should be set on the frame. This should include the 'src' property.</li>
 * <li> {object} attr The attributes that should be set on the frame.</li>
 * <li> {DOMElement} container Its parent element (Optional).</li>
 * <li> {function} onLoad A method that should be called with the frames contentWindow as argument when the frame is fully loaded. (Optional)</li>
 * </ul>
 * @return The frames DOMElement
 * @type DOMElement
 */
function createFrame(config){
    if (undef(HAS_NAME_PROPERTY_BUG)) {
        testForNamePropertyBug();
    }
    var frame;
    // This is to work around the problems in IE6/7 with setting the name property.
    // Internally this is set as 'submitName' instead when using 'iframe.name = ...'
    // This is not required by easyXDM itself, but is to facilitate other use cases
    if (HAS_NAME_PROPERTY_BUG) {
        frame = document.createElement("<iframe name=\"" + config.props.name + "\"/>");
    }
    else {
        frame = document.createElement("IFRAME");
        frame.name = config.props.name;
    }

    frame.id = frame.name = config.props.name;
    delete config.props.name;

    if (isString(config.container)) {
        config.container = document.getElementById(config.container);
    }

    if (!config.container) {
        // This needs to be hidden like this, simply setting display:none and the like will cause failures in some browsers.
        apply(frame.style, {
            position: "absolute",
            top: "-2000px",
            // Avoid potential horizontal scrollbar
            left: "0px"
        });
        config.container = document.body;
    }

    // HACK: IE cannot have the src attribute set when the frame is appended
    //       into the container, so we set it to "javascript:false" as a
    //       placeholder for now.  If we left the src undefined, it would
    //       instead default to "about:blank", which causes SSL mixed-content
    //       warnings in IE6 when on an SSL parent page.
    var src = config.props.src;
    config.props.src = "javascript:false";

    // transfer properties to the frame
    apply(frame, config.props);

    frame.border = frame.frameBorder = 0;
    frame.allowTransparency = true;
    config.container.appendChild(frame);

    if (config.onLoad) {
        on(frame, "load", config.onLoad);
    }

    // set the frame URL to the proper value (we previously set it to
    // "javascript:false" to work around the IE issue mentioned above)
    if(config.usePost) {
        var form = config.container.appendChild(document.createElement('form')), input;
        form.target = frame.name;
        form.action = src;
        form.method = 'POST';
        if (isObject(config.usePost)) {
            for (var i in config.usePost) {
                if (has(config.usePost, i)) {
                    if (HAS_NAME_PROPERTY_BUG) {
                        input = document.createElement('<input name="' + i + '"/>');
                    } else {
                        input = document.createElement("INPUT");
                        input.name = i;
                    }
                    input.value = config.usePost[i];
                    form.appendChild(input);
                }
            }
        }
        form.submit();
        form.parentNode.removeChild(form);
    } else {
        frame.src = src;
    }
    config.props.src = src;

    return frame;
}

/**
 * Check whether a domain is allowed using an Access Control List.
 * The ACL can contain * and ? as wildcards, or can be regular expressions.
 * If regular expressions they need to begin with ^ and end with $.
 * @param {Array/String} acl The list of allowed domains
 * @param {String} domain The domain to test.
 * @return {Boolean} True if the domain is allowed, false if not.
 */
function checkAcl(acl, domain){
    // normalize into an array
    if (isString(acl)) {
        acl = [acl];
    }
    var re, i = acl.length;
    while (i--) {
        re = acl[i];
        re = new RegExp(re.substr(0, 1) == "^" ? re : ("^" + re.replace(/(\*)/g, ".$1").replace(/\?/g, ".") + "$"));
        if (re.test(domain)) {
            return true;
        }
    }
    return false;
}

/*
 * Functions related to stacks
 */
/**
 * Prepares an array of stack-elements suitable for the current configuration
 * @param {Object} config The Transports configuration. See easyXDM.Socket for more.
 * @return {Array} An array of stack-elements with the TransportElement at index 0.
 */
function prepareTransportStack(config){
    var protocol = config.protocol, stackEls;
    config.isHost = config.isHost || undef(query.xdm_p);

    if (!config.props) {
        config.props = {};
    }
    if (!config.isHost) {
        config.channel = query.xdm_c.replace(/["'<>\\]/g, "");
        config.secret = query.xdm_s;
        config.remote = query.xdm_e.replace(/["'<>\\]/g, "");
        ;
        protocol = query.xdm_p;
        if (config.acl && !checkAcl(config.acl, config.remote)) {
            throw new Error("Access denied for " + config.remote);
        }
    }
    else {
        config.remote = resolveUrl(config.remote);
        config.channel = config.channel || "default" + channelId++;
        config.secret = Math.random().toString(16).substring(2);
        if (undef(protocol)) {
            var localOrigin = getLocation(location.href), remoteOrigin = getLocation(config.remote);
            if (localOrigin == remoteOrigin) {
                /*
                 * Both documents has the same origin, lets use direct access.
                 */
                protocol = "4";
            }
            else if (isHostMethod(window, "postMessage") || isHostMethod(document, "postMessage")) {
                /*
                 * This is supported in IE8+, Firefox 3+, Opera 9+, Chrome 2+ and Safari 4+
                 */
                protocol = "1";
            }
        }
    }
    config.protocol = protocol; // for conditional branching
    switch (protocol) {
        case "1":
            stackEls = [new easyXDM.stack.PostMessageTransport(config)];
            break;
        case "4":
            stackEls = [new easyXDM.stack.SameOriginTransport(config)];
            break;
    }
    // this behavior is responsible for buffering outgoing messages, and for performing lazy initialization
    stackEls.push(new easyXDM.stack.QueueBehavior({
        lazy: config.lazy,
        remove: true
    }));
    return stackEls;
}

/**
 * Chains all the separate stack elements into a single usable stack.<br/>
 * If an element is missing a necessary method then it will have a pass-through method applied.
 * @param {Array} stackElements An array of stack elements to be linked.
 * @return {easyXDM.stack.StackElement} The last element in the chain.
 */
function chainStack(stackElements){
    var stackEl, defaults = {
        incoming: function(message, origin){
            this.up.incoming(message, origin);
        },
        outgoing: function(message, recipient){
            this.down.outgoing(message, recipient);
        },
        callback: function(success){
            this.up.callback(success);
        },
        init: function(){
            this.down.init();
        },
        destroy: function(){
            this.down.destroy();
        }
    };
    for (var i = 0, len = stackElements.length; i < len; i++) {
        stackEl = stackElements[i];
        apply(stackEl, defaults, true);
        if (i !== 0) {
            stackEl.down = stackElements[i - 1];
        }
        if (i !== len - 1) {
            stackEl.up = stackElements[i + 1];
        }
    }
    return stackEl;
}

/**
 * This will remove a stackelement from its stack while leaving the stack functional.
 * @param {Object} element The elment to remove from the stack.
 */
function removeFromStack(element){
    element.up.down = element.down;
    element.down.up = element.up;
    element.up = element.down = null;
}

/*
 * Export the main object and any other methods applicable
 */
/**
 * @class easyXDM
 * A javascript library providing cross-browser, cross-domain messaging/RPC.
 * @version 2.4.20.7
 * @singleton
 */
apply(easyXDM, {
    /**
     * The version of the library
     * @type {string}
     */
    version: "2.4.20.7",
    /**
     * This is a map containing all the query parameters passed to the document.
     * All the values has been decoded using decodeURIComponent.
     * @type {object}
     */
    query: query,
    /**
     * @private
     */
    stack: {},
    /**
     * Applies properties from the source object to the target object.<br/>
     * @param {object} target The target of the properties.
     * @param {object} source The source of the properties.
     * @param {boolean} noOverwrite Set to True to only set non-existing properties.
     */
    apply: apply,

    /**
     * A safe implementation of HTML5 JSON. Feature testing is used to make sure the implementation works.
     * @return {JSON} A valid JSON conforming object, or null if not found.
     */
    getJSONObject: getJSON,
    /**
     * This will add a function to the queue of functions to be run once the DOM reaches a ready state.
     * If functions are added after this event then they will be executed immediately.
     * @param {function} fn The function to add
     * @param {object} scope An optional scope for the function to be called with.
     */
    whenReady: whenReady,
    /**
     * Removes easyXDM variable from the global scope. It also returns control
     * of the easyXDM variable to whatever code used it before.
     *
     * @param {String} ns A string representation of an object that will hold
     *                    an instance of easyXDM.
     * @return An instance of easyXDM
     */
    noConflict: noConflict
});

/*jslint evil: true, browser: true, immed: true, passfail: true, undef: true, newcap: true*/
/*global console, _FirebugCommandLine,  easyXDM, window, escape, unescape, isHostObject, undef, _trace, domIsReady, emptyFn, namespace */

/*jslint evil: true, browser: true, immed: true, passfail: true, undef: true, newcap: true*/
/*global easyXDM, window, escape, unescape, isHostObject, isHostMethod, un, on, createFrame, debug */

/**
 * @class easyXDM.DomHelper
 * Contains methods for dealing with the DOM
 * @singleton
 */
easyXDM.DomHelper = {
    /**
     * Provides a consistent interface for adding eventhandlers
     * @param {Object} target The target to add the event to
     * @param {String} type The name of the event
     * @param {Function} listener The listener
     */
    on: on,
    /**
     * Provides a consistent interface for removing eventhandlers
     * @param {Object} target The target to remove the event from
     * @param {String} type The name of the event
     * @param {Function} listener The listener
     */
    un: un,
    /**
     * Checks for the presence of the JSON object.
     * If it is not present it will use the supplied path to load the JSON2 library.
     * This should be called in the documents head right after the easyXDM script tag.
     * http://json.org/json2.js
     * @param {String} path A valid path to json2.js
     */
    requiresJSON: function(path){
        if (!isHostObject(window, "JSON")) {
            // we need to encode the < in order to avoid an illegal token error
            // when the script is inlined in a document.
            document.write('<' + 'script type="text/javascript" src="' + path + '"><' + '/script>');
        }
    }
};
/*jslint evil: true, browser: true, immed: true, passfail: true, undef: true, newcap: true*/
/*global easyXDM, window, escape, unescape, debug */

(function(){
    // The map containing the stored functions
    var _map = {};

    /**
     * @class easyXDM.Fn
     * This contains methods related to function handling, such as storing callbacks.
     * @singleton
     * @namespace easyXDM
     */
    easyXDM.Fn = {
        /**
         * Stores a function using the given name for reference
         * @param {String} name The name that the function should be referred by
         * @param {Function} fn The function to store
         * @namespace easyXDM.fn
         */
        set: function(name, fn){
            _map[name] = fn;
        },
        /**
         * Retrieves the function referred to by the given name
         * @param {String} name The name of the function to retrieve
         * @param {Boolean} del If the function should be deleted after retrieval
         * @return {Function} The stored function
         * @namespace easyXDM.fn
         */
        get: function(name, del){
            if (!has(_map, name)) {
                return;
            }
            var fn = _map[name];

            if (del) {
                delete _map[name];
            }
            return fn;
        }
    };

}());
/*jslint evil: true, browser: true, immed: true, passfail: true, undef: true, newcap: true*/
/*global easyXDM, window, escape, unescape, chainStack, prepareTransportStack, getLocation, debug */

/**
 * @class easyXDM.Socket
 * This class creates a transport channel between two domains that is usable for sending and receiving string-based messages.<br/>
 * The channel is reliable, supports queueing, and ensures that the message originates from the expected domain.<br/>
 * Internally different stacks will be used depending on the browsers features and the available parameters.
 * <h2>How to set up</h2>
 * Setting up the provider:
 * <pre><code>
 * var socket = new easyXDM.Socket({
 * &nbsp; local: "name.html",
 * &nbsp; onReady: function(){
 * &nbsp; &nbsp; &#47;&#47; you need to wait for the onReady callback before using the socket
 * &nbsp; &nbsp; socket.postMessage("foo-message");
 * &nbsp; },
 * &nbsp; onMessage: function(message, origin) {
 * &nbsp;&nbsp; alert("received " + message + " from " + origin);
 * &nbsp; }
 * });
 * </code></pre>
 * Setting up the consumer:
 * <pre><code>
 * var socket = new easyXDM.Socket({
 * &nbsp; remote: "http:&#47;&#47;remotedomain/page.html",
 * &nbsp; remoteHelper: "http:&#47;&#47;remotedomain/name.html",
 * &nbsp; onReady: function(){
 * &nbsp; &nbsp; &#47;&#47; you need to wait for the onReady callback before using the socket
 * &nbsp; &nbsp; socket.postMessage("foo-message");
 * &nbsp; },
 * &nbsp; onMessage: function(message, origin) {
 * &nbsp;&nbsp; alert("received " + message + " from " + origin);
 * &nbsp; }
 * });
 * </code></pre>
 * If you are unable to upload the <code>name.html</code> file to the consumers domain then remove the <code>remoteHelper</code> property
 * and easyXDM will fall back to using the HashTransport instead of the NameTransport when not able to use any of the primary transports.
 * @namespace easyXDM
 * @constructor
 * @cfg {String/Window} local The url to the local name.html document, a local static file, or a reference to the local window.
 * @cfg {Boolean} lazy (Consumer only) Set this to true if you want easyXDM to defer creating the transport until really needed.
 * @cfg {String} remote (Consumer only) The url to the providers document.
 * @cfg {String} remoteHelper (Consumer only) The url to the remote name.html file. This is to support NameTransport as a fallback. Optional.
 * @cfg {Number} delay The number of milliseconds easyXDM should try to get a reference to the local window.  Optional, defaults to 2000.
 * @cfg {Number} interval The interval used when polling for messages. Optional, defaults to 300.
 * @cfg {String} channel (Consumer only) The name of the channel to use. Can be used to set consistent iframe names. Must be unique. Optional.
 * @cfg {Function} onMessage The method that should handle incoming messages.<br/> This method should accept two arguments, the message as a string, and the origin as a string. Optional.
 * @cfg {Function} onReady A method that should be called when the transport is ready. Optional.
 * @cfg {DOMElement|String} container (Consumer only) The element, or the id of the element that the primary iframe should be inserted into. If not set then the iframe will be positioned off-screen. Optional.
 * @cfg {Array/String} acl (Provider only) Here you can specify which '[protocol]://[domain]' patterns that should be allowed to act as the consumer towards this provider.<br/>
 * This can contain the wildcards ? and *.  Examples are 'http://example.com', '*.foo.com' and '*dom?.com'. If you want to use reqular expressions then you pattern needs to start with ^ and end with $.
 * If none of the patterns match an Error will be thrown.
 * @cfg {Object} props (Consumer only) Additional properties that should be applied to the iframe. This can also contain nested objects e.g: <code>{style:{width:"100px", height:"100px"}}</code>.
 * Properties such as 'name' and 'src' will be overrided. Optional.
 */
easyXDM.Socket = function(config){

    // create the stack
    var stack = chainStack(prepareTransportStack(config).concat([{
        incoming: function(message, origin){
            config.onMessage(message, origin);
        },
        callback: function(success){
            if (config.onReady) {
                config.onReady(success);
            }
        }
    }])), recipient = getLocation(config.remote);

    // set the origin
    this.origin = recipient;

    /**
     * Initiates the destruction of the stack.
     */
    this.destroy = function(){
        stack.destroy();
    };

    /**
     * Posts a message to the remote end of the channel
     * @param {String} message The message to send
     */
    this.postMessage = function(message){
        stack.outgoing(message, recipient);
    };

    stack.init();
};
/*jslint evil: true, browser: true, immed: true, passfail: true, undef: true, newcap: true*/
/*global easyXDM, window, escape, unescape, undef,, chainStack, prepareTransportStack, debug, getLocation */

/**
 * @class easyXDM.Rpc
 * Creates a proxy object that can be used to call methods implemented on the remote end of the channel, and also to provide the implementation
 * of methods to be called from the remote end.<br/>
 * The instantiated object will have methods matching those specified in <code>config.remote</code>.<br/>
 * This requires the JSON object present in the document, either natively, using json.org's json2 or as a wrapper around library spesific methods.
 * <h2>How to set up</h2>
 * <pre><code>
 * var rpc = new easyXDM.Rpc({
 * &nbsp; &#47;&#47; this configuration is equal to that used by the Socket.
 * &nbsp; remote: "http:&#47;&#47;remotedomain/...",
 * &nbsp; onReady: function(){
 * &nbsp; &nbsp; &#47;&#47; you need to wait for the onReady callback before using the proxy
 * &nbsp; &nbsp; rpc.foo(...
 * &nbsp; }
 * },{
 * &nbsp; local: {..},
 * &nbsp; remote: {..}
 * });
 * </code></pre>
 *
 * <h2>Exposing functions (procedures)</h2>
 * <pre><code>
 * var rpc = new easyXDM.Rpc({
 * &nbsp; ...
 * },{
 * &nbsp; local: {
 * &nbsp; &nbsp; nameOfMethod: {
 * &nbsp; &nbsp; &nbsp; method: function(arg1, arg2, success, error){
 * &nbsp; &nbsp; &nbsp; &nbsp; ...
 * &nbsp; &nbsp; &nbsp; }
 * &nbsp; &nbsp; },
 * &nbsp; &nbsp; &#47;&#47; with shorthand notation
 * &nbsp; &nbsp; nameOfAnotherMethod:  function(arg1, arg2, success, error){
 * &nbsp; &nbsp; }
 * &nbsp; },
 * &nbsp; remote: {...}
 * });
 * </code></pre>

 * The function referenced by  [method] will receive the passed arguments followed by the callback functions <code>success</code> and <code>error</code>.<br/>
 * To send a successfull result back you can use
 *     <pre><code>
 *     return foo;
 *     </pre></code>
 * or
 *     <pre><code>
 *     success(foo);
 *     </pre></code>
 *  To return an error you can use
 *     <pre><code>
 *     throw new Error("foo error");
 *     </code></pre>
 * or
 *     <pre><code>
 *     error("foo error");
 *     </code></pre>
 *
 * <h2>Defining remotely exposed methods (procedures/notifications)</h2>
 * The definition of the remote end is quite similar:
 * <pre><code>
 * var rpc = new easyXDM.Rpc({
 * &nbsp; ...
 * },{
 * &nbsp; local: {...},
 * &nbsp; remote: {
 * &nbsp; &nbsp; nameOfMethod: {}
 * &nbsp; }
 * });
 * </code></pre>
 * To call a remote method use
 * <pre><code>
 * rpc.nameOfMethod("arg1", "arg2", function(value) {
 * &nbsp; alert("success: " + value);
 * }, function(message) {
 * &nbsp; alert("error: " + message + );
 * });
 * </code></pre>
 * Both the <code>success</code> and <code>errror</code> callbacks are optional.<br/>
 * When called with no callback a JSON-RPC 2.0 notification will be executed.
 * Be aware that you will not be notified of any errors with this method.
 * <br/>
 * <h2>Specifying a custom serializer</h2>
 * If you do not want to use the JSON2 library for non-native JSON support, but instead capabilities provided by some other library
 * then you can specify a custom serializer using <code>serializer: foo</code>
 * <pre><code>
 * var rpc = new easyXDM.Rpc({
 * &nbsp; ...
 * },{
 * &nbsp; local: {...},
 * &nbsp; remote: {...},
 * &nbsp; serializer : {
 * &nbsp; &nbsp; parse: function(string){ ... },
 * &nbsp; &nbsp; stringify: function(object) {...}
 * &nbsp; }
 * });
 * </code></pre>
 * If <code>serializer</code> is set then the class will not attempt to use the native implementation.
 * @namespace easyXDM
 * @constructor
 * @param {Object} config The underlying transports configuration. See easyXDM.Socket for available parameters.
 * @param {Object} jsonRpcConfig The description of the interface to implement.
 */
easyXDM.Rpc = function(config, jsonRpcConfig){

    // expand shorthand notation
    if (jsonRpcConfig.local) {
        for (var method in jsonRpcConfig.local) {
            if (has(jsonRpcConfig.local, method)) {
                var member = jsonRpcConfig.local[method];
                if (isFunction(member)) {
                    jsonRpcConfig.local[method] = {
                        method: member
                    };
                }
            }
        }
    }

    // create the stack
    var stack = chainStack(prepareTransportStack(config).concat([new easyXDM.stack.RpcBehavior(this, jsonRpcConfig), {
        callback: function(success){
            if (config.onReady) {
                config.onReady(success);
            }
        }
    }]));

    // set the origin
    this.origin = getLocation(config.remote);

    /**
     * Initiates the destruction of the stack.
     */
    this.destroy = function(){
        stack.destroy();
    };

    stack.init();
};
/*jslint evil: true, browser: true, immed: true, passfail: true, undef: true, newcap: true*/
/*global easyXDM, window, escape, unescape, getLocation, appendQueryParameters, createFrame, debug, un, on, apply, whenReady, getParentObject, IFRAME_PREFIX*/

/**
 * @class easyXDM.stack.SameOriginTransport
 * SameOriginTransport is a transport class that can be used when both domains have the same origin.<br/>
 * This can be useful for testing and for when the main application supports both internal and external sources.
 * @namespace easyXDM.stack
 * @constructor
 * @param {Object} config The transports configuration.
 * @cfg {String} remote The remote document to communicate with.
 */
easyXDM.stack.SameOriginTransport = function(config){
    var pub, frame, send, targetOrigin;

    return (pub = {
        outgoing: function(message, domain, fn){
            send(message);
            if (fn) {
                fn();
            }
        },
        destroy: function(){
            if (frame) {
                frame.parentNode.removeChild(frame);
                frame = null;
            }
        },
        onDOMReady: function(){
            targetOrigin = getLocation(config.remote);

            if (config.isHost) {
                // set up the iframe
                apply(config.props, {
                    src: appendQueryParameters(config.remote, {
                        xdm_e: location.protocol + "//" + location.host + location.pathname,
                        xdm_c: config.channel,
                        xdm_p: 4 // 4 = SameOriginTransport
                    }),
                    name: IFRAME_PREFIX + config.channel + "_provider"
                });
                frame = createFrame(config);
                easyXDM.Fn.set(config.channel, function(sendFn){
                    send = sendFn;
                    setTimeout(function(){
                        pub.up.callback(true);
                    }, 0);
                    return function(msg){
                        pub.up.incoming(msg, targetOrigin);
                    };
                });
            }
            else {
                send = getParentObject().Fn.get(config.channel, true)(function(msg){
                    pub.up.incoming(msg, targetOrigin);
                });
                setTimeout(function(){
                    pub.up.callback(true);
                }, 0);
            }
        },
        init: function(){
            whenReady(pub.onDOMReady, pub);
        }
    });
};
/*jslint evil: true, browser: true, immed: true, passfail: true, undef: true, newcap: true*/
/*global easyXDM, window, escape, unescape, getLocation, appendQueryParameters, createFrame, debug, un, on, apply, whenReady, IFRAME_PREFIX*/

/**
 * @class easyXDM.stack.PostMessageTransport
 * PostMessageTransport is a transport class that uses HTML5 postMessage for communication.<br/>
 * <a href="http://msdn.microsoft.com/en-us/library/ms644944(VS.85).aspx">http://msdn.microsoft.com/en-us/library/ms644944(VS.85).aspx</a><br/>
 * <a href="https://developer.mozilla.org/en/DOM/window.postMessage">https://developer.mozilla.org/en/DOM/window.postMessage</a>
 * @namespace easyXDM.stack
 * @constructor
 * @param {Object} config The transports configuration.
 * @cfg {String} remote The remote domain to communicate with.
 */
easyXDM.stack.PostMessageTransport = function(config){
    var pub, // the public interface
 frame, // the remote frame, if any
 callerWindow, // the window that we will call with
 targetOrigin; // the domain to communicate with
    /**
     * Resolves the origin from the event object
     * @private
     * @param {Object} event The messageevent
     * @return {String} The scheme, host and port of the origin
     */
    function _getOrigin(event){
        if ('origin' in event && event.origin) {
            // This is the HTML5 property
            return event.origin;
        }
        if ('uri' in event && event.uri) {
            // From earlier implementations
            return event.uri;
        }
        if ('domain' in event && event.domain) {
            // This is the last option and will fail if the
            // origin is not using the same schema as we are
            return location.protocol + "//" + event.domain;
        }
        throw "Unable to retrieve the origin of the event";
    }

    /**
     * This is the main implementation for the onMessage event.<br/>
     * It checks the validity of the origin and passes the message on if appropriate.
     * @private
     * @param {Object} event The messageevent
     */
    function _window_onMessage(event){
        if (!isString(event.data)) {
            // postMessage also supports passing objects, but easyXDM's messages are always strings
            return;
        }

        var eventOrigin = _getOrigin(event);

        if (!isSupportedURI(eventOrigin)) {
            return;
        }

        var origin = getLocation(eventOrigin);

        if (origin && origin == targetOrigin && isString(event.data) && event.data.substring(0, config.channel.length + 1) == config.channel + " ") {
            pub.up.incoming(event.data.substring(config.channel.length + 1), origin);
        }
    }


    /**
     * This adds the listener for messages when the frame is ready.
     * @private
     * @param {Object} event The messageevent
     */
    // add the event handler for listening
    function _window_waitForReady(event){
        if (event.data == config.channel + "-ready") {
            // replace the eventlistener
            callerWindow = ("postMessage" in frame.contentWindow) ? frame.contentWindow : frame.contentWindow.document;
            un(window, "message", _window_waitForReady);
            on(window, "message", _window_onMessage);
            setTimeout(function(){
                pub.up.callback(true);
            }, 0);
        }
    }

    return (pub = {
        outgoing: function(message, domain, fn){
            callerWindow.postMessage(config.channel + " " + message, domain || targetOrigin);
            if (fn) {
                fn();
            }
        },
        destroy: function(){
            un(window, "message", _window_waitForReady);
            un(window, "message", _window_onMessage);
            if (frame) {
                callerWindow = null;
                frame.parentNode.removeChild(frame);
                frame = null;
            }
        },
        onDOMReady: function(){
            targetOrigin = getLocation(config.remote);
            if (config.isHost) {
                on(window, "message", _window_waitForReady);

                // set up the iframe
                apply(config.props, {
                    src: appendQueryParameters(config.remote, {
                        xdm_e: getLocation(location.href),
                        xdm_c: config.channel,
                        xdm_p: 1 // 1 = PostMessage
                    }),
                    name: IFRAME_PREFIX + config.channel + "_provider"
                });
                frame = createFrame(config);
            }
            else {
                // add the event handler for listening
                on(window, "message", _window_onMessage);
                callerWindow = ("postMessage" in window.parent) ? window.parent : window.parent.document;
                callerWindow.postMessage(config.channel + "-ready", targetOrigin);

                setTimeout(function(){
                    pub.up.callback(true);
                }, 0);
            }
        },
        init: function(){
            whenReady(pub.onDOMReady, pub);
        }
    });
};
/*jslint evil: true, browser: true, immed: true, passfail: true, undef: true, newcap: true*/
/*global easyXDM, window, escape, unescape, debug, undef, removeFromStack*/

/**
 * @class easyXDM.stack.QueueBehavior
 * This is a behavior that enables queueing of messages. <br/>
 * It will buffer incoming messages and dispach these as fast as the underlying transport allows.
 * This will also fragment/defragment messages so that the outgoing message is never bigger than the
 * set length.
 * @namespace easyXDM.stack
 * @constructor
 * @param {Object} config The behaviors configuration. Optional.
 * @cfg {Number} maxLength The maximum length of each outgoing message. Set this to enable fragmentation.
 */
easyXDM.stack.QueueBehavior = function(config){
    var pub, queue = [], waiting = true, incoming = "", destroying, maxLength = 0, lazy = false, doFragment = false;

    function dispatch(){
        if (config.remove && queue.length === 0) {
            removeFromStack(pub);
            return;
        }
        if (waiting || queue.length === 0 || destroying) {
            return;
        }
        waiting = true;
        var message = queue.shift();

        pub.down.outgoing(message.data, message.origin, function(success){
            waiting = false;
            if (message.callback) {
                setTimeout(function(){
                    message.callback(success);
                }, 0);
            }
            dispatch();
        });
    }
    return (pub = {
        init: function(){
            if (undef(config)) {
                config = {};
            }
            if (config.maxLength) {
                maxLength = config.maxLength;
                doFragment = true;
            }
            if (config.lazy) {
                lazy = true;
            }
            else {
                pub.down.init();
            }
        },
        callback: function(success){
            waiting = false;
            var up = pub.up; // in case dispatch calls removeFromStack
            dispatch();
            up.callback(success);
        },
        incoming: function(message, origin){
            if (doFragment) {
                var indexOf = message.indexOf("_"), seq = parseInt(message.substring(0, indexOf), 10);
                incoming += message.substring(indexOf + 1);
                if (seq === 0) {
                    if (config.encode) {
                        incoming = decodeURIComponent(incoming);
                    }
                    pub.up.incoming(incoming, origin);
                    incoming = "";
                }
            }
            else {
                pub.up.incoming(message, origin);
            }
        },
        outgoing: function(message, origin, fn){
            if (config.encode) {
                message = encodeURIComponent(message);
            }
            var fragments = [], fragment;
            if (doFragment) {
                // fragment into chunks
                while (message.length !== 0) {
                    fragment = message.substring(0, maxLength);
                    message = message.substring(fragment.length);
                    fragments.push(fragment);
                }
                // enqueue the chunks
                while ((fragment = fragments.shift())) {
                    queue.push({
                        data: fragments.length + "_" + fragment,
                        origin: origin,
                        callback: fragments.length === 0 ? fn : null
                    });
                }
            }
            else {
                queue.push({
                    data: message,
                    origin: origin,
                    callback: fn
                });
            }
            if (lazy) {
                pub.down.init();
            }
            else {
                dispatch();
            }
        },
        destroy: function(){
            destroying = true;
            pub.down.destroy();
        }
    });
};
/*jslint evil: true, browser: true, immed: true, passfail: true, undef: true, newcap: true*/
/*global easyXDM, window, escape, unescape, undef, getJSON, debug, emptyFn, isArray */

/**
 * @class easyXDM.stack.RpcBehavior
 * This uses JSON-RPC 2.0 to expose local methods and to invoke remote methods and have responses returned over the the string based transport stack.<br/>
 * Exposed methods can return values synchronous, asyncronous, or bet set up to not return anything.
 * @namespace easyXDM.stack
 * @constructor
 * @param {Object} proxy The object to apply the methods to.
 * @param {Object} config The definition of the local and remote interface to implement.
 * @cfg {Object} local The local interface to expose.
 * @cfg {Object} remote The remote methods to expose through the proxy.
 * @cfg {Object} serializer The serializer to use for serializing and deserializing the JSON. Should be compatible with the HTML5 JSON object. Optional, will default to JSON.
 */
easyXDM.stack.RpcBehavior = function(proxy, config){
    var pub, serializer = config.serializer || getJSON();
    var _callbackCounter = 0, _callbacks = {};

    /**
     * Serializes and sends the message
     * @private
     * @param {Object} data The JSON-RPC message to be sent. The jsonrpc property will be added.
     */
    function _send(data){
        data.jsonrpc = "2.0";
        pub.down.outgoing(serializer.stringify(data));
    }

    /**
     * Creates a method that implements the given definition
     * @private
     * @param {Object} The method configuration
     * @param {String} method The name of the method
     * @return {Function} A stub capable of proxying the requested method call
     */
    function _createMethod(definition, method){
        var slice = Array.prototype.slice;

        return function(){
            var l = arguments.length, callback, message = {
                method: method
            };

            if (l > 0 && isFunction(arguments[l - 1])) {
                //with callback, procedure
                if (l > 1 && isFunction(arguments[l - 2])) {
                    // two callbacks, success and error
                    callback = {
                        success: arguments[l - 2],
                        error: arguments[l - 1]
                    };
                    message.params = slice.call(arguments, 0, l - 2);
                }
                else {
                    // single callback, success
                    callback = {
                        success: arguments[l - 1]
                    };
                    message.params = slice.call(arguments, 0, l - 1);
                }
                _callbacks["" + (++_callbackCounter)] = callback;
                message.id = _callbackCounter;
            }
            else {
                // no callbacks, a notification
                message.params = slice.call(arguments, 0);
            }
            if (definition.namedParams && message.params.length === 1) {
                message.params = message.params[0];
            }
            // Send the method request
            _send(message);
        };
    }

    /**
     * Executes the exposed method
     * @private
     * @param {String} method The name of the method
     * @param {Number} id The callback id to use
     * @param {Function} method The exposed implementation
     * @param {Array} params The parameters supplied by the remote end
     */
    function _executeMethod(method, id, fn, params){
        if (!fn) {
            if (id) {
                _send({
                    id: id,
                    error: {
                        code: -32601,
                        message: "Procedure not found."
                    }
                });
            }
            return;
        }

        var success, error;
        if (id) {
            success = function(result){
                success = emptyFn;
                _send({
                    id: id,
                    result: result
                });
            };
            error = function(message, data){
                error = emptyFn;
                var msg = {
                    id: id,
                    error: {
                        code: -32099,
                        message: message
                    }
                };
                if (data) {
                    msg.error.data = data;
                }
                _send(msg);
            };
        }
        else {
            success = error = emptyFn;
        }
        // Call local method
        if (!isArray(params)) {
            params = [params];
        }
        try {
            var result = fn.method.apply(fn.scope, params.concat([success, error]));
            if (!undef(result)) {
                success(result);
            }
        }
        catch (ex1) {
            error(ex1.message);
        }
    }

    return (pub = {
        incoming: function(message, origin){
            var data = serializer.parse(message);
            if (data.method) {
                // A method call from the remote end
                if (config.handle) {
                    config.handle(data, _send);
                }
                else {
                    _executeMethod(data.method, data.id, config.local[data.method], data.params);
                }
            }
            else {
                // A method response from the other end
                var callback = _callbacks[data.id];
                if (data.error) {
                    if (callback.error) {
                        callback.error(data.error);
                    }
                }
                else if (callback.success) {
                    callback.success(data.result);
                }
                delete _callbacks[data.id];
            }
        },
        init: function(){
            if (config.remote) {
                // Implement the remote sides exposed methods
                for (var method in config.remote) {
                    if (has(config.remote, method)) {
                        proxy[method] = _createMethod(config.remote[method], method);
                    }
                }
            }
            pub.down.init();
        },
        destroy: function(){
            for (var method in config.remote) {
                if (has(config.remote, method) && has(proxy, method)) {
                    delete proxy[method];
                }
            }
            pub.down.destroy();
        }
    });
};

return easyXDM;

})(window, document, location, window.setTimeout, decodeURIComponent, encodeURIComponent);

},{}],95:[function(require,module,exports){
/*! SWFObject v2.3.20130521+ <http://github.com/swfobject/swfobject>
    is released under the MIT License <http://www.opensource.org/licenses/mit-license.php>
*/

/* global ActiveXObject */

var swfobject = function () {

	var UNDEF = "undefined",
		OBJECT = "object",
		SHOCKWAVE_FLASH = "Shockwave Flash",
		SHOCKWAVE_FLASH_AX = "ShockwaveFlash.ShockwaveFlash",
		FLASH_MIME_TYPE = "application/x-shockwave-flash",
		EXPRESS_INSTALL_ID = "SWFObjectExprInst",
		DOM_READY_EVENT = "DOMContentLoaded",
		ON_READY_STATE_CHANGE = "onreadystatechange",

		win = window,
		doc = document,
		nav = navigator,

		plugin = false,
		domLoadFnArr = [],
		regObjArr = [],
		objIdArr = [],
		listenersArr = [],
		storedFbContent,
		storedFbContentId,
		storedCallbackFn,
		storedCallbackObj,
		isDomLoaded = false,
		isExpressInstallActive = false,
		dynamicStylesheet,
		dynamicStylesheetMedia,
		autoHideShow = true,
		encodeURIEnabled = false,

	/* Centralized function for browser feature detection
		- User agent string detection is only used when no good alternative is possible
		- Is executed directly for optimal performance
	*/
	ua = (function () {
		var w3cdom = typeof doc.getElementById !== UNDEF && typeof doc.getElementsByTagName !== UNDEF && typeof doc.createElement !== UNDEF,
			u = nav.userAgent.toLowerCase(),
			p = nav.platform.toLowerCase(),
			windows = p ? /win/.test(p) : /win/.test(u),
			mac = p ? /mac/.test(p) : /mac/.test(u),
			webkit = /webkit/.test(u) ? parseFloat(u.replace(/^.*webkit\/(\d+(\.\d+)?).*$/, "$1")) : false, // returns either the webkit version or false if not webkit
			ie = nav.appName === "Microsoft Internet Explorer",
			playerVersion = [0, 0, 0],
			d = null;
		if (typeof nav.plugins !== UNDEF && typeof nav.plugins[SHOCKWAVE_FLASH] === OBJECT) {
			d = nav.plugins[SHOCKWAVE_FLASH].description;
			// nav.mimeTypes["application/x-shockwave-flash"].enabledPlugin indicates whether plug-ins are enabled or disabled in Safari 3+
			if (d && (typeof nav.mimeTypes !== UNDEF && nav.mimeTypes[FLASH_MIME_TYPE] && nav.mimeTypes[FLASH_MIME_TYPE].enabledPlugin)) {
				plugin = true;
				ie = false; // cascaded feature detection for Internet Explorer
				d = d.replace(/^.*\s+(\S+\s+\S+$)/, "$1");
				playerVersion[0] = toInt(d.replace(/^(.*)\..*$/, "$1"));
				playerVersion[1] = toInt(d.replace(/^.*\.(.*)\s.*$/, "$1"));
				playerVersion[2] = /[a-zA-Z]/.test(d) ? toInt(d.replace(/^.*[a-zA-Z]+(.*)$/, "$1")) : 0;
			}
		}
		else if (typeof win.ActiveXObject !== UNDEF) {
			try {
				var a = new ActiveXObject(SHOCKWAVE_FLASH_AX);
				if (a) { // a will return null when ActiveX is disabled
					d = a.GetVariable("$version");
					if (d) {
						ie = true; // cascaded feature detection for Internet Explorer
						d = d.split(" ")[1].split(",");
						playerVersion = [toInt(d[0]), toInt(d[1]), toInt(d[2])];
					}
				}
			}
			catch (e) {}
		}
		return { w3: w3cdom, pv: playerVersion, wk: webkit, ie: ie, win: windows, mac: mac };
	}());

	/* Cross-browser onDomLoad
		- Will fire an event as soon as the DOM of a web page is loaded
		- Internet Explorer workaround based on Diego Perini's solution: http://javascript.nwbox.com/IEContentLoaded/
		- Regular onload serves as fallback
	*/
	(function onDomLoad() {
		if (!ua.w3) { return; }
		// [When executed post-DOM-ready & pre-window-loaded, doc.readyState === "interactive", but there are reports of IE claiming "interactive" when DOM is not actually ready, so leaving out check from 2.3.20130521.]
		// [Sources: <http://bugs.jquery.com/ticket/12282>, <https://github.com/ded/domready/pull/17>]
		if ((typeof doc.readyState !== UNDEF && doc.readyState === "complete") || (typeof doc.readyState === UNDEF && (doc.getElementsByTagName("body")[0] || doc.body))) { // function is fired after onload, e.g. when script is inserted dynamically
			callDomLoadFunctions();
		}
		if (!isDomLoaded) {
			if (typeof doc.addEventListener !== UNDEF) {
				doc.addEventListener(DOM_READY_EVENT, function remove() {
					doc.removeEventListener(DOM_READY_EVENT, remove, false);
					callDomLoadFunctions();
				}, false);
			}
			if (ua.ie) {
				doc.attachEvent(ON_READY_STATE_CHANGE, function detach() {
					if (doc.readyState === "complete") {
						doc.detachEvent(ON_READY_STATE_CHANGE, detach);
						callDomLoadFunctions();
					}
				});
				if (win === top) { // if not inside an iframe
					(function checkDomLoadedIE() {
						if (isDomLoaded) { return; }
						try {
							doc.documentElement.doScroll("left");
						}
						catch (e) {
							setTimeout(checkDomLoadedIE, 10);
							return;
						}
						callDomLoadFunctions();
					}());
				}
			}
			if (ua.wk) {
				(function checkDomLoadedWK() {
					if (isDomLoaded) { return; }
					if (!/^(loaded|complete)$/.test(doc.readyState)) {
						setTimeout(checkDomLoadedWK, 10);
						return;
					}
					callDomLoadFunctions();
				}());
			}
			// [Re-introducing this call from swfobject 2.2 to CYA in case the above checks fail; fallback to window.onload.]
			addLoadEvent(callDomLoadFunctions);
		}
	}());

	function callDomLoadFunctions() {
		if (isDomLoaded || !document.getElementsByTagName("body")[0]) { return; }
		try { // test if we can really add/remove elements to/from the DOM; we don't want to fire it too early
			var t, span = createElement("span");
			span.style.display = "none"; // hide the span in case someone has styled spans via CSS
			t = doc.getElementsByTagName("body")[0].appendChild(span);
			t.parentNode.removeChild(t);
			t = null; // clear the variables
			span = null;
		}
		catch (e) { return; }
		isDomLoaded = true;
		var dl = domLoadFnArr.length;
		for (var i = 0; i < dl; i++) {
			domLoadFnArr[i]();
		}
	}

	function addDomLoadEvent(fn) {
		if (isDomLoaded) {
			fn();
		}
		else {
			domLoadFnArr[domLoadFnArr.length] = fn; // Array.push() is only available in IE5.5+
		}
	}

	/* Cross-browser onload
		- Based on James Edwards' solution: http://brothercake.com/site/resources/scripts/onload/
		- Will fire an event as soon as a web page including all of its assets are loaded
	 */
	function addLoadEvent(fn) {
		if (typeof win.addEventListener !== UNDEF) {
			win.addEventListener("load", fn, false);
		}
		else if (typeof doc.addEventListener !== UNDEF) {
			doc.addEventListener("load", fn, false);
		}
		else if (typeof win.attachEvent !== UNDEF) {
			addListener(win, "onload", fn);
		}
		else if (typeof win.onload === "function") {
			var fnOld = win.onload;
			win.onload = function () {
				fnOld();
				fn();
			};
		}
		else {
			win.onload = fn;
		}
	}

	/* Detect the Flash Player version for non-Internet Explorer browsers
		- Detecting the plug-in version via the object element is more precise than using the plugins collection item's description:
		  a. Both release and build numbers can be detected
		  b. Avoid wrong descriptions by corrupt installers provided by Adobe
		  c. Avoid wrong descriptions by multiple Flash Player entries in the plugin Array, caused by incorrect browser imports
		- Disadvantage of this method is that it depends on the availability of the DOM, while the plugins collection is immediately available
	*/
	function testPlayerVersion() {
		var b = doc.getElementsByTagName("body")[0];
		var o = createElement(OBJECT);
		o.setAttribute("style", "visibility: hidden;");
		o.setAttribute("type", FLASH_MIME_TYPE);
		var t = b.appendChild(o);
		if (t) {
			var counter = 0;
			(function checkGetVariable() {
				if (typeof t.GetVariable !== UNDEF) {
					try {
						var d = t.GetVariable("$version");
						if (d) {
							d = d.split(" ")[1].split(",");
							ua.pv = [toInt(d[0]), toInt(d[1]), toInt(d[2])];
						}
					} catch (e) {
						// t.GetVariable("$version") is known to fail in Flash Player 8 on Firefox
						// If this error is encountered, assume FP8 or lower. Time to upgrade.
						ua.pv = [8, 0, 0];
					}
				}
				else if (counter < 10) {
					counter++;
					setTimeout(checkGetVariable, 10);
					return;
				}
				b.removeChild(o);
				t = null;
				matchVersions();
			}());
		}
		else {
			matchVersions();
		}
	}

	/* Perform Flash Player and SWF version matching; static publishing only
	*/
	function matchVersions() {
		var rl = regObjArr.length;
		if (rl > 0) {
			for (var i = 0; i < rl; i++) { // for each registered object element
				var id = regObjArr[i].id;
				var cb = regObjArr[i].callbackFn;
				var cbObj = { success: false, id: id };
				if (ua.pv[0] > 0) {
					var obj = getElementById(id);
					if (obj) {
						if (hasPlayerVersion(regObjArr[i].swfVersion) && !(ua.wk && ua.wk < 312)) { // Flash Player version >= published SWF version: Houston, we have a match!
							setVisibility(id, true);
							if (cb) {
								cbObj.success = true;
								cbObj.ref = getObjectById(id);
								cbObj.id = id;
								cb(cbObj);
							}
						}
						else if (regObjArr[i].expressInstall && canExpressInstall()) { // show the Adobe Express Install dialog if set by the web page author and if supported
							var att = {};
							att.data = regObjArr[i].expressInstall;
							att.width = obj.getAttribute("width") || "0";
							att.height = obj.getAttribute("height") || "0";
							if (obj.getAttribute("class")) { att.styleclass = obj.getAttribute("class"); }
							if (obj.getAttribute("align")) { att.align = obj.getAttribute("align"); }
							// parse HTML object param element's name-value pairs
							var par = {};
							var p = obj.getElementsByTagName("param");
							var pl = p.length;
							for (var j = 0; j < pl; j++) {
								if (p[j].getAttribute("name").toLowerCase() !== "movie") {
									par[p[j].getAttribute("name")] = p[j].getAttribute("value");
								}
							}
							showExpressInstall(att, par, id, cb);
						}
						else { // Flash Player and SWF version mismatch or an older Webkit engine that ignores the HTML object element's nested param elements: display fallback content instead of SWF
							displayFbContent(obj);
							if (cb) { cb(cbObj); }
						}
					}
				}
				else { // if no Flash Player is installed or the fp version cannot be detected we let the HTML object element do its job (either show a SWF or fallback content)
					setVisibility(id, true);
					if (cb) {
						var o = getObjectById(id); // test whether there is an HTML object element or not
						if (o && typeof o.SetVariable !== UNDEF) {
							cbObj.success = true;
							cbObj.ref = o;
							cbObj.id = o.id;
						}
						cb(cbObj);
					}
				}
			}
		}
	}

	/* Main function
		- Will preferably execute onDomLoad, otherwise onload (as a fallback)
	*/
	domLoadFnArr[0] = function () {
		if (plugin) {
			testPlayerVersion();
		}
		else {
			matchVersions();
		}
	};

	function getObjectById(objectIdStr) {
		var r = null,
			o = getElementById(objectIdStr);

		if (o && o.nodeName.toUpperCase() === "OBJECT") {
			// If targeted object is valid Flash file
			if (typeof o.SetVariable !== UNDEF) {
				r = o;
			} else {
				// If SetVariable is not working on targeted object but a nested object is
				// available, assume classic nested object markup. Return nested object.

				// If SetVariable is not working on targeted object and there is no nested object,
				// return the original object anyway. This is probably new simplified markup.

				r = o.getElementsByTagName(OBJECT)[0] || o;
			}
		}

		return r;
	}

	/* Requirements for Adobe Express Install
		- only one instance can be active at a time
		- fp 6.0.65 or higher
		- Win/Mac OS only
		- no Webkit engines older than version 312
	*/
	function canExpressInstall() {
		return !isExpressInstallActive && hasPlayerVersion("6.0.65") && (ua.win || ua.mac) && !(ua.wk && ua.wk < 312);
	}

	/* Show the Adobe Express Install dialog
		- Reference: http://www.adobe.com/cfusion/knowledgebase/index.cfm?id=6a253b75
	*/
	function showExpressInstall(att, par, replaceElemIdStr, callbackFn) {

		var obj = getElementById(replaceElemIdStr);

		// Ensure that replaceElemIdStr is really a string and not an element
		replaceElemIdStr = getId(replaceElemIdStr);

		isExpressInstallActive = true;
		storedCallbackFn = callbackFn || null;
		storedCallbackObj = { success: false, id: replaceElemIdStr };

		if (obj) {
			if (obj.nodeName.toUpperCase() === "OBJECT") { // static publishing
				storedFbContent = abstractFbContent(obj);
				storedFbContentId = null;
			}
			else { // dynamic publishing
				storedFbContent = obj;
				storedFbContentId = replaceElemIdStr;
			}
			att.id = EXPRESS_INSTALL_ID;
			if (typeof att.width === UNDEF || (!/%$/.test(att.width) && toInt(att.width) < 310)) { att.width = "310"; }
			if (typeof att.height === UNDEF || (!/%$/.test(att.height) && toInt(att.height) < 137)) { att.height = "137"; }
			var pt = ua.ie ? "ActiveX" : "PlugIn",
				fv = "MMredirectURL=" + encodeURIComponent(win.location.toString().replace(/&/g, "%26")) + "&MMplayerType=" + pt + "&MMdoctitle=" + encodeURIComponent(doc.title.slice(0, 47) + " - Flash Player Installation");
			if (typeof par.flashvars !== UNDEF) {
				par.flashvars += "&" + fv;
			}
			else {
				par.flashvars = fv;
			}
			// IE only: when a SWF is loading (AND: not available in cache) wait for the readyState of the object element to become 4 before removing it,
			// because you cannot properly cancel a loading SWF file without breaking browser load references, also obj.onreadystatechange doesn't work
			if (ua.ie && obj.readyState != 4) {
				var newObj = createElement("div");
				replaceElemIdStr += "SWFObjectNew";
				newObj.setAttribute("id", replaceElemIdStr);
				obj.parentNode.insertBefore(newObj, obj); // insert placeholder div that will be replaced by the object element that loads expressinstall.swf
				obj.style.display = "none";
				removeSWF(obj); // removeSWF accepts elements now
			}
			createSWF(att, par, replaceElemIdStr);
		}
	}

	/* Functions to abstract and display fallback content
	*/
	function displayFbContent(obj) {
		if (ua.ie && obj.readyState != 4) {
			// IE only: when a SWF is loading (AND: not available in cache) wait for the readyState of the object element to become 4 before removing it,
			// because you cannot properly cancel a loading SWF file without breaking browser load references, also obj.onreadystatechange doesn't work
			obj.style.display = "none";
			var el = createElement("div");
			obj.parentNode.insertBefore(el, obj); // insert placeholder div that will be replaced by the fallback content
			el.parentNode.replaceChild(abstractFbContent(obj), el);
			removeSWF(obj); // removeSWF accepts elements now
		}
		else {
			obj.parentNode.replaceChild(abstractFbContent(obj), obj);
		}
	}

	function abstractFbContent(obj) {
		var ac = createElement("div");
		if (ua.win && ua.ie) {
			ac.innerHTML = obj.innerHTML;
		}
		else {
			var nestedObj = obj.getElementsByTagName(OBJECT)[0];
			if (nestedObj) {
				var c = nestedObj.childNodes;
				if (c) {
					var cl = c.length;
					for (var i = 0; i < cl; i++) {
						if (!(c[i].nodeType == 1 && c[i].nodeName === "PARAM") && !(c[i].nodeType == 8)) {
							ac.appendChild(c[i].cloneNode(true));
						}
					}
				}
			}
		}
		return ac;
	}

	function createIeObject(url, paramStr) {
		var div = createElement("div");
		div.innerHTML = "<object classid='clsid:D27CDB6E-AE6D-11cf-96B8-444553540000'><param name='movie' value='" + url + "'>" + paramStr + "</object>";
		return div.firstChild;
	}

	/* Cross-browser dynamic SWF creation
	*/
	function createSWF(attObj, parObj, id) {
		var r, el = getElementById(id);
		id = getId(id); // ensure id is truly an ID and not an element

		if (ua.wk && ua.wk < 312) { return r; }

		if (el) {
			var o = (ua.ie) ? createElement("div") : createElement(OBJECT),
				attr,
				attrLower,
				param;

			if (typeof attObj.id === UNDEF) { // if no 'id' is defined for the object element, it will inherit the 'id' from the fallback content
				attObj.id = id;
			}

			// Add params
			for (param in parObj) {
				// filter out prototype additions from other potential libraries and IE specific param element
				if (parObj.hasOwnProperty(param) && param.toLowerCase() !== "movie") {
					createObjParam(o, param, parObj[param]);
				}
			}

			// Create IE object, complete with param nodes
			if (ua.ie) { o = createIeObject(attObj.data, o.innerHTML); }

			// Add attributes to object
			for (attr in attObj) {
				if (attObj.hasOwnProperty(attr)) { // filter out prototype additions from other potential libraries
					attrLower = attr.toLowerCase();

					// 'class' is an ECMA4 reserved keyword
					if (attrLower === "styleclass") {
						o.setAttribute("class", attObj[attr]);
					} else if (attrLower !== "classid" && attrLower !== "data") {
						o.setAttribute(attr, attObj[attr]);
					}
				}
			}

			if (ua.ie) {
				objIdArr[objIdArr.length] = attObj.id; // stored to fix object 'leaks' on unload (dynamic publishing only)
			} else {
				o.setAttribute("type", FLASH_MIME_TYPE);
				o.setAttribute("data", attObj.data);
			}

			el.parentNode.replaceChild(o, el);
			r = o;
		}

		return r;
	}

	function createObjParam(el, pName, pValue) {
		var p = createElement("param");
		p.setAttribute("name", pName);
		p.setAttribute("value", pValue);
		el.appendChild(p);
	}

	/* Cross-browser SWF removal
		- Especially needed to safely and completely remove a SWF in Internet Explorer
	*/
	function removeSWF(id) {
		var obj = getElementById(id);
		if (obj && obj.nodeName.toUpperCase() === "OBJECT") {
			if (ua.ie) {
				obj.style.display = "none";
				(function removeSWFInIE() {
					if (obj.readyState == 4) {
						// This step prevents memory leaks in Internet Explorer
						for (var i in obj) {
							if (typeof obj[i] === "function") {
								obj[i] = null;
							}
						}
						obj.parentNode.removeChild(obj);
					} else {
						setTimeout(removeSWFInIE, 10);
					}
				}());
			}
			else {
				obj.parentNode.removeChild(obj);
			}
		}
	}

	function isElement(id) {
		return (id && id.nodeType && id.nodeType === 1);
	}

	function getId(thing) {
		return (isElement(thing)) ? thing.id : thing;
	}

	/* Functions to optimize JavaScript compression
	*/
	function getElementById(id) {

		// Allow users to pass an element OR an element's ID
		if (isElement(id)) { return id; }

		var el = null;
		try {
			el = doc.getElementById(id);
		}
		catch (e) {}
		return el;
	}

	function createElement(el) {
		return doc.createElement(el);
	}

	// To aid compression; replaces 14 instances of pareseInt with radix
	function toInt(str) {
		return parseInt(str, 10);
	}

	/* Updated attachEvent function for Internet Explorer
		- Stores attachEvent information in an Array, so on unload the detachEvent functions can be called to avoid memory leaks
	*/
	function addListener(target, eventType, fn) {
		target.attachEvent(eventType, fn);
		listenersArr[listenersArr.length] = [target, eventType, fn];
	}

	/* Flash Player and SWF content version matching
	*/
	function hasPlayerVersion(rv) {
		rv += ""; // Coerce number to string, if needed.
		var pv = ua.pv, v = rv.split(".");
		v[0] = toInt(v[0]);
		v[1] = toInt(v[1]) || 0; // supports short notation, e.g. "9" instead of "9.0.0"
		v[2] = toInt(v[2]) || 0;
		return (pv[0] > v[0] || (pv[0] == v[0] && pv[1] > v[1]) || (pv[0] == v[0] && pv[1] == v[1] && pv[2] >= v[2])) ? true : false;
	}

	/* Cross-browser dynamic CSS creation
		- Based on Bobby van der Sluis' solution: http://www.bobbyvandersluis.com/articles/dynamicCSS.php
	*/
	function createCSS(sel, decl, media, newStyle) {
		var h = doc.getElementsByTagName("head")[0];
		if (!h) { return; } // to also support badly authored HTML pages that lack a head element
		var m = (typeof media === "string") ? media : "screen";
		if (newStyle) {
			dynamicStylesheet = null;
			dynamicStylesheetMedia = null;
		}
		if (!dynamicStylesheet || dynamicStylesheetMedia != m) {
			// create dynamic stylesheet + get a global reference to it
			var s = createElement("style");
			s.setAttribute("type", "text/css");
			s.setAttribute("media", m);
			dynamicStylesheet = h.appendChild(s);
			if (ua.ie && typeof doc.styleSheets !== UNDEF && doc.styleSheets.length > 0) {
				dynamicStylesheet = doc.styleSheets[doc.styleSheets.length - 1];
			}
			dynamicStylesheetMedia = m;
		}
		// add style rule
		if (dynamicStylesheet) {
			if (typeof dynamicStylesheet.addRule !== UNDEF) {
				dynamicStylesheet.addRule(sel, decl);
			} else if (typeof doc.createTextNode !== UNDEF) {
				dynamicStylesheet.appendChild(doc.createTextNode(sel + " {" + decl + "}"));
			}
		}
	}

	function setVisibility(id, isVisible) {
		if (!autoHideShow) { return; }
		var v = isVisible ? "visible" : "hidden",
			el = getElementById(id);
		if (isDomLoaded && el) {
			el.style.visibility = v;
		} else if (typeof id === "string") {
			createCSS("#" + id, "visibility:" + v);
		}
	}

	/* Filter to avoid XSS attacks
	*/
	function urlEncodeIfNecessary(s) {
		var regex = /[\\\"<>\.;]/;
		var hasBadChars = regex.exec(s) !== null;
		return hasBadChars && typeof encodeURIComponent !== UNDEF ? encodeURIComponent(s) : s;
	}

	/* Release memory to avoid memory leaks caused by closures, fix hanging audio/video threads and force open sockets/NetConnections to disconnect (Internet Explorer only)
	*/
	(function cleanup() {
		if (ua.ie) {
			window.attachEvent("onunload", function () {
				// remove listeners to avoid memory leaks
				var ll = listenersArr.length;
				for (var i = 0; i < ll; i++) {
					listenersArr[i][0].detachEvent(listenersArr[i][1], listenersArr[i][2]);
				}
				// cleanup dynamically embedded objects to fix audio/video threads and force open sockets and NetConnections to disconnect
				var il = objIdArr.length;
				for (var j = 0; j < il; j++) {
					removeSWF(objIdArr[j]);
				}
				// cleanup library's main closures to avoid memory leaks
				for (var k in ua) {
					ua[k] = null;
				}
				ua = null;
				for (var l in swfobject) {
					swfobject[l] = null;
				}
				swfobject = null;
			});
		}
	}());

	return {
		/* Public API
			- Reference: http://code.google.com/p/swfobject/wiki/documentation
		*/
		registerObject: function (objectIdStr, swfVersionStr, xiSwfUrlStr, callbackFn) {
			if (ua.w3 && objectIdStr && swfVersionStr) {
				var regObj = {};
				regObj.id = objectIdStr;
				regObj.swfVersion = swfVersionStr;
				regObj.expressInstall = xiSwfUrlStr;
				regObj.callbackFn = callbackFn;
				regObjArr[regObjArr.length] = regObj;
				setVisibility(objectIdStr, false);
			}
			else if (callbackFn) {
				callbackFn({ success: false, id: objectIdStr });
			}
		},

		getObjectById: function (objectIdStr) {
			if (ua.w3) {
				return getObjectById(objectIdStr);
			}
		},

		embedSWF: function (swfUrlStr, replaceElemIdStr, widthStr, heightStr, swfVersionStr, xiSwfUrlStr, flashvarsObj, parObj, attObj, callbackFn) {

			var id = getId(replaceElemIdStr),
				callbackObj = { success: false, id: id };

			if (ua.w3 && !(ua.wk && ua.wk < 312) && swfUrlStr && replaceElemIdStr && widthStr && heightStr && swfVersionStr) {
				setVisibility(id, false);
				addDomLoadEvent(function () {
					widthStr += ""; // auto-convert to string
					heightStr += "";
					var att = {};
					if (attObj && typeof attObj === OBJECT) {
						for (var i in attObj) { // copy object to avoid the use of references, because web authors often reuse attObj for multiple SWFs
							att[i] = attObj[i];
						}
					}
					att.data = swfUrlStr;
					att.width = widthStr;
					att.height = heightStr;
					var par = {};
					if (parObj && typeof parObj === OBJECT) {
						for (var j in parObj) { // copy object to avoid the use of references, because web authors often reuse parObj for multiple SWFs
							par[j] = parObj[j];
						}
					}
					if (flashvarsObj && typeof flashvarsObj === OBJECT) {
						for (var k in flashvarsObj) { // copy object to avoid the use of references, because web authors often reuse flashvarsObj for multiple SWFs
							if (flashvarsObj.hasOwnProperty(k)) {

								var key = (encodeURIEnabled) ? encodeURIComponent(k) : k,
									value = (encodeURIEnabled) ? encodeURIComponent(flashvarsObj[k]) : flashvarsObj[k];

								if (typeof par.flashvars !== UNDEF) {
									par.flashvars += "&" + key + "=" + value;
								}
								else {
									par.flashvars = key + "=" + value;
								}

							}
						}
					}
					if (hasPlayerVersion(swfVersionStr)) { // create SWF
						var obj = createSWF(att, par, replaceElemIdStr);
						if (att.id == id) {
							setVisibility(id, true);
						}
						callbackObj.success = true;
						callbackObj.ref = obj;
						callbackObj.id = obj.id;
					}
					else if (xiSwfUrlStr && canExpressInstall()) { // show Adobe Express Install
						att.data = xiSwfUrlStr;
						showExpressInstall(att, par, replaceElemIdStr, callbackFn);
						return;
					}
					else { // show fallback content
						setVisibility(id, true);
					}
					if (callbackFn) { callbackFn(callbackObj); }
				});
			}
			else if (callbackFn) { callbackFn(callbackObj); }
		},

		switchOffAutoHideShow: function () {
			autoHideShow = false;
		},

		enableUriEncoding: function (bool) {
			encodeURIEnabled = (typeof bool === UNDEF) ? true : bool;
		},

		ua: ua,

		getFlashPlayerVersion: function () {
			return { major: ua.pv[0], minor: ua.pv[1], release: ua.pv[2] };
		},

		hasFlashPlayerVersion: hasPlayerVersion,

		createSWF: function (attObj, parObj, replaceElemIdStr) {
			if (ua.w3) {
				return createSWF(attObj, parObj, replaceElemIdStr);
			}
			else {
				return undefined;
			}
		},

		showExpressInstall: function (att, par, replaceElemIdStr, callbackFn) {
			if (ua.w3 && canExpressInstall()) {
				showExpressInstall(att, par, replaceElemIdStr, callbackFn);
			}
		},

		removeSWF: function (objElemIdStr) {
			if (ua.w3) {
				removeSWF(objElemIdStr);
			}
		},

		createCSS: function (selStr, declStr, mediaStr, newStyleBoolean) {
			if (ua.w3) {
				createCSS(selStr, declStr, mediaStr, newStyleBoolean);
			}
		},

		addDomLoadEvent: addDomLoadEvent,

		addLoadEvent: addLoadEvent,

		getQueryParamValue: function (param) {
			var q = doc.location.search || doc.location.hash;
			if (q) {
				if (/\?/.test(q)) { q = q.split("?")[1]; } // strip question mark
				if (!param) {
					return urlEncodeIfNecessary(q);
				}
				var pairs = q.split("&");
				for (var i = 0; i < pairs.length; i++) {
					if (pairs[i].substring(0, pairs[i].indexOf("=")) == param) {
						return urlEncodeIfNecessary(pairs[i].substring((pairs[i].indexOf("=") + 1)));
					}
				}
			}
			return "";
		},

		// For internal usage only
		expressInstallCallback: function () {
			if (isExpressInstallActive) {
				var obj = getElementById(EXPRESS_INSTALL_ID);
				if (obj && storedFbContent) {
					obj.parentNode.replaceChild(storedFbContent, obj);
					if (storedFbContentId) {
						setVisibility(storedFbContentId, true);
						if (ua.ie) { storedFbContent.style.display = "block"; }
					}
					if (storedCallbackFn) { storedCallbackFn(storedCallbackObj); }
				}
				isExpressInstallActive = false;
			}
		},

		version: "2.3"

	};

}();


// Return the module value
module.exports = swfobject;

},{}],96:[function(require,module,exports){
(function (global){
/** @license MIT License (c) copyright 2011-2013 original author or authors */

/**
 * A lightweight CommonJS Promises/A and when() implementation
 * when is part of the cujo.js family of libraries (http://cujojs.com/)
 *
 * Licensed under the MIT License at:
 * http://www.opensource.org/licenses/mit-license.php
 *
 * @author Brian Cavalier
 * @author John Hann
 * @version 2.2.1
 */

'use strict';

// Public API

when.promise   = promise;    // Create a pending promise
when.resolve   = resolve;    // Create a resolved promise
when.reject    = reject;     // Create a rejected promise
when.defer     = defer;      // Create a {promise, resolver} pair

when.join      = join;       // Join 2 or more promises

when.all       = all;        // Resolve a list of promises
when.map       = map;        // Array.map() for promises
when.reduce    = reduce;     // Array.reduce() for promises
when.settle    = settle;     // Settle a list of promises

when.any       = any;        // One-winner race
when.some      = some;       // Multi-winner race

when.isPromise = isPromise;  // Determine if a thing is a promise


/**
 * Register an observer for a promise or immediate value.
 *
 * @param {*} promiseOrValue
 * @param {function?} [onFulfilled] callback to be called when promiseOrValue is
 *   successfully fulfilled.  If promiseOrValue is an immediate value, callback
 *   will be invoked immediately.
 * @param {function?} [onRejected] callback to be called when promiseOrValue is
 *   rejected.
 * @param {function?} [onProgress] callback to be called when progress updates
 *   are issued for promiseOrValue.
 * @returns {Promise} a new {@link Promise} that will complete with the return
 *   value of callback or errback or the completion value of promiseOrValue if
 *   callback and/or errback is not supplied.
 */
function when(promiseOrValue, onFulfilled, onRejected, onProgress) {
	// Get a trusted promise for the input promiseOrValue, and then
	// register promise handlers
	return resolve(promiseOrValue).then(onFulfilled, onRejected, onProgress);
}

/**
 * Trusted Promise constructor.  A Promise created from this constructor is
 * a trusted when.js promise.  Any other duck-typed promise is considered
 * untrusted.
 * @constructor
 * @name Promise
 */
function Promise(then, inspect) {
	this.then = then;
	this.inspect = inspect;
}

Promise.prototype = {
	/**
	 * Register a rejection handler.  Shortcut for .then(undefined, onRejected)
	 * @param {function?} onRejected
	 * @return {Promise}
	 */
	otherwise: function(onRejected) {
		return this.then(undef, onRejected);
	},

	/**
	 * Ensures that onFulfilledOrRejected will be called regardless of whether
	 * this promise is fulfilled or rejected.  onFulfilledOrRejected WILL NOT
	 * receive the promises' value or reason.  Any returned value will be disregarded.
	 * onFulfilledOrRejected may throw or return a rejected promise to signal
	 * an additional error.
	 * @param {function} onFulfilledOrRejected handler to be called regardless of
	 *  fulfillment or rejection
	 * @returns {Promise}
	 */
	ensure: function(onFulfilledOrRejected) {
		return this.then(injectHandler, injectHandler)['yield'](this);

		function injectHandler() {
			return resolve(onFulfilledOrRejected());
		}
	},

	/**
	 * Shortcut for .then(function() { return value; })
	 * @param  {*} value
	 * @return {Promise} a promise that:
	 *  - is fulfilled if value is not a promise, or
	 *  - if value is a promise, will fulfill with its value, or reject
	 *    with its reason.
	 */
	'yield': function(value) {
		return this.then(function() {
			return value;
		});
	},

	/**
	 * Assumes that this promise will fulfill with an array, and arranges
	 * for the onFulfilled to be called with the array as its argument list
	 * i.e. onFulfilled.apply(undefined, array).
	 * @param {function} onFulfilled function to receive spread arguments
	 * @return {Promise}
	 */
	spread: function(onFulfilled) {
		return this.then(function(array) {
			// array may contain promises, so resolve its contents.
			return all(array, function(array) {
				return onFulfilled.apply(undef, array);
			});
		});
	},

	/**
	 * Shortcut for .then(onFulfilledOrRejected, onFulfilledOrRejected)
	 * @deprecated
	 */
	always: function(onFulfilledOrRejected, onProgress) {
		return this.then(onFulfilledOrRejected, onFulfilledOrRejected, onProgress);
	}
};

/**
 * Returns a resolved promise. The returned promise will be
 *  - fulfilled with promiseOrValue if it is a value, or
 *  - if promiseOrValue is a promise
 *    - fulfilled with promiseOrValue's value after it is fulfilled
 *    - rejected with promiseOrValue's reason after it is rejected
 * @param  {*} value
 * @return {Promise}
 */
function resolve(value) {
	return promise(function(resolve) {
		resolve(value);
	});
}

/**
 * Returns a rejected promise for the supplied promiseOrValue.  The returned
 * promise will be rejected with:
 * - promiseOrValue, if it is a value, or
 * - if promiseOrValue is a promise
 *   - promiseOrValue's value after it is fulfilled
 *   - promiseOrValue's reason after it is rejected
 * @param {*} promiseOrValue the rejected value of the returned {@link Promise}
 * @return {Promise} rejected {@link Promise}
 */
function reject(promiseOrValue) {
	return when(promiseOrValue, rejected);
}

/**
 * Creates a {promise, resolver} pair, either or both of which
 * may be given out safely to consumers.
 * The resolver has resolve, reject, and progress.  The promise
 * has then plus extended promise API.
 *
 * @return {{
 * promise: Promise,
 * resolve: function:Promise,
 * reject: function:Promise,
 * notify: function:Promise
 * resolver: {
 *	resolve: function:Promise,
 *	reject: function:Promise,
 *	notify: function:Promise
 * }}}
 */
function defer() {
	var deferred, pending, resolved;

	// Optimize object shape
	deferred = {
		promise: undef, resolve: undef, reject: undef, notify: undef,
		resolver: { resolve: undef, reject: undef, notify: undef }
	};

	deferred.promise = pending = promise(makeDeferred);

	return deferred;

	function makeDeferred(resolvePending, rejectPending, notifyPending) {
		deferred.resolve = deferred.resolver.resolve = function(value) {
			if(resolved) {
				return resolve(value);
			}
			resolved = true;
			resolvePending(value);
			return pending;
		};

		deferred.reject  = deferred.resolver.reject  = function(reason) {
			if(resolved) {
				return resolve(rejected(reason));
			}
			resolved = true;
			rejectPending(reason);
			return pending;
		};

		deferred.notify  = deferred.resolver.notify  = function(update) {
			notifyPending(update);
			return update;
		};
	}
}

/**
 * Creates a new promise whose fate is determined by resolver.
 * @param {function} resolver function(resolve, reject, notify)
 * @returns {Promise} promise whose fate is determine by resolver
 */
function promise(resolver) {
	return _promise(resolver, monitorApi.PromiseStatus && monitorApi.PromiseStatus());
}

/**
 * Creates a new promise, linked to parent, whose fate is determined
 * by resolver.
 * @param {function} resolver function(resolve, reject, notify)
 * @param {Promise?} status promise from which the new promise is begotten
 * @returns {Promise} promise whose fate is determine by resolver
 * @private
 */
function _promise(resolver, status) {
	var self, value, handlers = [];

	self = new Promise(then, inspect);

	// Call the provider resolver to seal the promise's fate
	try {
		resolver(promiseResolve, promiseReject, promiseNotify);
	} catch(e) {
		promiseReject(e);
	}

	// Return the promise
	return self;

	/**
	 * Register handlers for this promise.
	 * @param [onFulfilled] {Function} fulfillment handler
	 * @param [onRejected] {Function} rejection handler
	 * @param [onProgress] {Function} progress handler
	 * @return {Promise} new Promise
	 */
	function then(onFulfilled, onRejected, onProgress) {
		var next = _promise(function(resolve, reject, notify) {
			// if not resolved, push onto handlers, otherwise execute asap
			// but not in the current stack
			handlers ? handlers.push(run) : enqueue(function() { run(value); });

			function run(p) {
				p.then(onFulfilled, onRejected, onProgress)
					.then(resolve, reject, notify);
			}

		}, status && status.observed());

		return next;
	}

	function inspect() {
		return value ? value.inspect() : toPendingState();
	}

	/**
	 * Transition from pre-resolution state to post-resolution state, notifying
	 * all listeners of the ultimate fulfillment or rejection
	 * @param {*|Promise} val resolution value
	 */
	function promiseResolve(val) {
		if(!handlers) {
			return;
		}

		value = coerce(val);
		scheduleHandlers(handlers, value);
		handlers = undef;

		if (status) {
			value.then(
				function () { status.fulfilled(); },
				function(r) { status.rejected(r); }
			);
		}
	}

	/**
	 * Reject this promise with the supplied reason, which will be used verbatim.
	 * @param {*} reason reason for the rejection
	 */
	function promiseReject(reason) {
		promiseResolve(rejected(reason));
	}

	/**
	 * Issue a progress event, notifying all progress listeners
	 * @param {*} update progress event payload to pass to all listeners
	 */
	function promiseNotify(update) {
		if(handlers) {
			scheduleHandlers(handlers, progressing(update));
		}
	}
}

/**
 * Coerces x to a trusted Promise
 *
 * @private
 * @param {*} x thing to coerce
 * @returns {Promise} Guaranteed to return a trusted Promise.  If x
 *   is trusted, returns x, otherwise, returns a new, trusted, already-resolved
 *   Promise whose resolution value is:
 *   * the resolution value of x if it's a foreign promise, or
 *   * x if it's a value
 */
function coerce(x) {
	if(x instanceof Promise) {
		return x;
	}

	if (!(x === Object(x) && 'then' in x)) {
		return fulfilled(x);
	}

	return promise(function(resolve, reject, notify) {
		enqueue(function() {
			try {
				// We must check and assimilate in the same tick, but not the
				// current tick, careful only to access promiseOrValue.then once.
				var untrustedThen = x.then;

				if(typeof untrustedThen === 'function') {
					fcall(untrustedThen, x, resolve, reject, notify);
				} else {
					// It's a value, create a fulfilled wrapper
					resolve(fulfilled(x));
				}

			} catch(e) {
				// Something went wrong, reject
				reject(e);
			}
		});
	});
}

/**
 * Create an already-fulfilled promise for the supplied value
 * @private
 * @param {*} value
 * @return {Promise} fulfilled promise
 */
function fulfilled(value) {
	var self = new Promise(function (onFulfilled) {
		try {
			return typeof onFulfilled == 'function'
				? coerce(onFulfilled(value)) : self;
		} catch (e) {
			return rejected(e);
		}
	}, function() {
		return toFulfilledState(value);
	});

	return self;
}

/**
 * Create an already-rejected promise with the supplied rejection reason.
 * @private
 * @param {*} reason
 * @return {Promise} rejected promise
 */
function rejected(reason) {
	var self = new Promise(function (_, onRejected) {
		try {
			return typeof onRejected == 'function'
				? coerce(onRejected(reason)) : self;
		} catch (e) {
			return rejected(e);
		}
	}, function() {
		return toRejectedState(reason);
	});

	return self;
}

/**
 * Create a progress promise with the supplied update.
 * @private
 * @param {*} update
 * @return {Promise} progress promise
 */
function progressing(update) {
	var self = new Promise(function (_, __, onProgress) {
		try {
			return typeof onProgress == 'function'
				? progressing(onProgress(update)) : self;
		} catch (e) {
			return progressing(e);
		}
	});

	return self;
}

/**
 * Schedule a task that will process a list of handlers
 * in the next queue drain run.
 * @private
 * @param {Array} handlers queue of handlers to execute
 * @param {*} value passed as the only arg to each handler
 */
function scheduleHandlers(handlers, value) {
	enqueue(function() {
		var handler, i = 0;
		while (handler = handlers[i++]) {
			handler(value);
		}
	});
}

/**
 * Determines if promiseOrValue is a promise or not
 *
 * @param {*} promiseOrValue anything
 * @returns {boolean} true if promiseOrValue is a {@link Promise}
 */
function isPromise(promiseOrValue) {
	return promiseOrValue && typeof promiseOrValue.then === 'function';
}

/**
 * Initiates a competitive race, returning a promise that will resolve when
 * howMany of the supplied promisesOrValues have resolved, or will reject when
 * it becomes impossible for howMany to resolve, for example, when
 * (promisesOrValues.length - howMany) + 1 input promises reject.
 *
 * @param {Array} promisesOrValues array of anything, may contain a mix
 *      of promises and values
 * @param howMany {number} number of promisesOrValues to resolve
 * @param {function?} [onFulfilled] DEPRECATED, use returnedPromise.then()
 * @param {function?} [onRejected] DEPRECATED, use returnedPromise.then()
 * @param {function?} [onProgress] DEPRECATED, use returnedPromise.then()
 * @returns {Promise} promise that will resolve to an array of howMany values that
 *  resolved first, or will reject with an array of
 *  (promisesOrValues.length - howMany) + 1 rejection reasons.
 */
function some(promisesOrValues, howMany, onFulfilled, onRejected, onProgress) {

	return when(promisesOrValues, function(promisesOrValues) {

		return promise(resolveSome).then(onFulfilled, onRejected, onProgress);

		function resolveSome(resolve, reject, notify) {
			var toResolve, toReject, values, reasons, fulfillOne, rejectOne, len, i;

			len = promisesOrValues.length >>> 0;

			toResolve = Math.max(0, Math.min(howMany, len));
			values = [];

			toReject = (len - toResolve) + 1;
			reasons = [];

			// No items in the input, resolve immediately
			if (!toResolve) {
				resolve(values);

			} else {
				rejectOne = function(reason) {
					reasons.push(reason);
					if(!--toReject) {
						fulfillOne = rejectOne = identity;
						reject(reasons);
					}
				};

				fulfillOne = function(val) {
					// This orders the values based on promise resolution order
					values.push(val);
					if (!--toResolve) {
						fulfillOne = rejectOne = identity;
						resolve(values);
					}
				};

				for(i = 0; i < len; ++i) {
					if(i in promisesOrValues) {
						when(promisesOrValues[i], fulfiller, rejecter, notify);
					}
				}
			}

			function rejecter(reason) {
				rejectOne(reason);
			}

			function fulfiller(val) {
				fulfillOne(val);
			}
		}
	});
}

/**
 * Initiates a competitive race, returning a promise that will resolve when
 * any one of the supplied promisesOrValues has resolved or will reject when
 * *all* promisesOrValues have rejected.
 *
 * @param {Array|Promise} promisesOrValues array of anything, may contain a mix
 *      of {@link Promise}s and values
 * @param {function?} [onFulfilled] DEPRECATED, use returnedPromise.then()
 * @param {function?} [onRejected] DEPRECATED, use returnedPromise.then()
 * @param {function?} [onProgress] DEPRECATED, use returnedPromise.then()
 * @returns {Promise} promise that will resolve to the value that resolved first, or
 * will reject with an array of all rejected inputs.
 */
function any(promisesOrValues, onFulfilled, onRejected, onProgress) {

	function unwrapSingleResult(val) {
		return onFulfilled ? onFulfilled(val[0]) : val[0];
	}

	return some(promisesOrValues, 1, unwrapSingleResult, onRejected, onProgress);
}

/**
 * Return a promise that will resolve only once all the supplied promisesOrValues
 * have resolved. The resolution value of the returned promise will be an array
 * containing the resolution values of each of the promisesOrValues.
 * @memberOf when
 *
 * @param {Array|Promise} promisesOrValues array of anything, may contain a mix
 *      of {@link Promise}s and values
 * @param {function?} [onFulfilled] DEPRECATED, use returnedPromise.then()
 * @param {function?} [onRejected] DEPRECATED, use returnedPromise.then()
 * @param {function?} [onProgress] DEPRECATED, use returnedPromise.then()
 * @returns {Promise}
 */
function all(promisesOrValues, onFulfilled, onRejected, onProgress) {
	return _map(promisesOrValues, identity).then(onFulfilled, onRejected, onProgress);
}

/**
 * Joins multiple promises into a single returned promise.
 * @return {Promise} a promise that will fulfill when *all* the input promises
 * have fulfilled, or will reject when *any one* of the input promises rejects.
 */
function join(/* ...promises */) {
	return _map(arguments, identity);
}

/**
 * Settles all input promises such that they are guaranteed not to
 * be pending once the returned promise fulfills. The returned promise
 * will always fulfill, except in the case where `array` is a promise
 * that rejects.
 * @param {Array|Promise} array or promise for array of promises to settle
 * @returns {Promise} promise that always fulfills with an array of
 *  outcome snapshots for each input promise.
 */
function settle(array) {
	return _map(array, toFulfilledState, toRejectedState);
}

/**
 * Promise-aware array map function, similar to `Array.prototype.map()`,
 * but input array may contain promises or values.
 * @param {Array|Promise} array array of anything, may contain promises and values
 * @param {function} mapFunc map function which may return a promise or value
 * @returns {Promise} promise that will fulfill with an array of mapped values
 *  or reject if any input promise rejects.
 */
function map(array, mapFunc) {
	return _map(array, mapFunc);
}

/**
 * Internal map that allows a fallback to handle rejections
 * @param {Array|Promise} array array of anything, may contain promises and values
 * @param {function} mapFunc map function which may return a promise or value
 * @param {function?} fallback function to handle rejected promises
 * @returns {Promise} promise that will fulfill with an array of mapped values
 *  or reject if any input promise rejects.
 */
function _map(array, mapFunc, fallback) {
	return when(array, function(array) {

		return promise(resolveMap);

		function resolveMap(resolve, reject, notify) {
			var results, len, toResolve, i;

			// Since we know the resulting length, we can preallocate the results
			// array to avoid array expansions.
			toResolve = len = array.length >>> 0;
			results = [];

			if(!toResolve) {
				resolve(results);
				return;
			}

			// Since mapFunc may be async, get all invocations of it into flight
			for(i = 0; i < len; i++) {
				if(i in array) {
					resolveOne(array[i], i);
				} else {
					--toResolve;
				}
			}

			function resolveOne(item, i) {
				when(item, mapFunc, fallback).then(function(mapped) {
					results[i] = mapped;

					if(!--toResolve) {
						resolve(results);
					}
				}, reject, notify);
			}
		}
	});
}

/**
 * Traditional reduce function, similar to `Array.prototype.reduce()`, but
 * input may contain promises and/or values, and reduceFunc
 * may return either a value or a promise, *and* initialValue may
 * be a promise for the starting value.
 *
 * @param {Array|Promise} promise array or promise for an array of anything,
 *      may contain a mix of promises and values.
 * @param {function} reduceFunc reduce function reduce(currentValue, nextValue, index, total),
 *      where total is the total number of items being reduced, and will be the same
 *      in each call to reduceFunc.
 * @returns {Promise} that will resolve to the final reduced value
 */
function reduce(promise, reduceFunc /*, initialValue */) {
	var args = fcall(slice, arguments, 1);

	return when(promise, function(array) {
		var total;

		total = array.length;

		// Wrap the supplied reduceFunc with one that handles promises and then
		// delegates to the supplied.
		args[0] = function (current, val, i) {
			return when(current, function (c) {
				return when(val, function (value) {
					return reduceFunc(c, value, i, total);
				});
			});
		};

		return reduceArray.apply(array, args);
	});
}

// Snapshot states

/**
 * Creates a fulfilled state snapshot
 * @private
 * @param {*} x any value
 * @returns {{state:'fulfilled',value:*}}
 */
function toFulfilledState(x) {
	return { state: 'fulfilled', value: x };
}

/**
 * Creates a rejected state snapshot
 * @private
 * @param {*} x any reason
 * @returns {{state:'rejected',reason:*}}
 */
function toRejectedState(x) {
	return { state: 'rejected', reason: x };
}

/**
 * Creates a pending state snapshot
 * @private
 * @returns {{state:'pending'}}
 */
function toPendingState() {
	return { state: 'pending' };
}

//
// Utilities, etc.
//

var reduceArray, slice, fcall, nextTick, handlerQueue,
	setTimeout, funcProto, call, arrayProto, monitorApi, undef;

//
// Shared handler queue processing
//
// Credit to Twisol (https://github.com/Twisol) for suggesting
// this type of extensible queue + trampoline approach for
// next-tick conflation.

handlerQueue = [];

/**
 * Enqueue a task. If the queue is not currently scheduled to be
 * drained, schedule it.
 * @param {function} task
 */
function enqueue(task) {
	if(handlerQueue.push(task) === 1) {
		nextTick(drainQueue);
	}
}

/**
 * Drain the handler queue entirely, being careful to allow the
 * queue to be extended while it is being processed, and to continue
 * processing until it is truly empty.
 */
function drainQueue() {
	var task, i = 0;

	while(task = handlerQueue[i++]) {
		task();
	}

	handlerQueue = [];
}

//
// Capture function and array utils
//
/*global setImmediate,process,vertx*/

// Allow attaching the monitor to when() if env has no console
monitorApi = typeof console != 'undefined' ? console : when;

// capture setTimeout to avoid being caught by fake timers used in time based tests
setTimeout = global.setTimeout;
// Prefer setImmediate, cascade to node, vertx and finally setTimeout
nextTick = typeof setImmediate === 'function' ? setImmediate.bind(global)
		: function(task) { setTimeout(task, 0); }; // fallback

// Safe function calls
funcProto = Function.prototype;
call = funcProto.call;
fcall = funcProto.bind
	? call.bind(call)
	: function(f, context) {
		return f.apply(context, slice.call(arguments, 2));
	};

// Safe array ops
arrayProto = [];
slice = arrayProto.slice;

// ES5 reduce implementation if native not available
// See: http://es5.github.com/#x15.4.4.21 as there are many
// specifics and edge cases.  ES5 dictates that reduce.length === 1
// This implementation deviates from ES5 spec in the following ways:
// 1. It does not check if reduceFunc is a Callable
reduceArray = arrayProto.reduce ||
	function(reduceFunc /*, initialValue */) {
		/*jshint maxcomplexity: 7*/
		var arr, args, reduced, len, i;

		i = 0;
		arr = Object(this);
		len = arr.length >>> 0;
		args = arguments;

		// If no initialValue, use first item of array (we know length !== 0 here)
		// and adjust i to start at second item
		if(args.length <= 1) {
			// Skip to the first real element in the array
			for(;;) {
				if(i in arr) {
					reduced = arr[i++];
					break;
				}

				// If we reached the end of the array without finding any real
				// elements, it's a TypeError
				if(++i >= len) {
					throw new TypeError();
				}
			}
		} else {
			// If initialValue provided, use it
			reduced = args[1];
		}

		// Do the actual reduce
		for(;i < len; ++i) {
			if(i in arr) {
				reduced = reduceFunc(reduced, arr[i], i, arr);
			}
		}

		return reduced;
	};

function identity(x) {
	return x;
}

module.exports = when;

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})

},{}]},{},[9])
//# sourceMappingURL=cvp.js.map

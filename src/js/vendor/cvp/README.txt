======================================================================

DESCRIPTION:

Current Version of CVP for expansion is 2.8.10.0. You can see that version number
in the cvp.js by searching for the value "VERSION".

CVP source files can be retrieved from
Unminified versions: http://cvpdev.turner.com/dmtvideo/cvp-js/
Eg for version 2.8.10.0 unminified version of the CVP can be found at http://cvpdev.turner.com/dmtvideo/cvp-js/2.8.8.0/cvp.js


Current version of CVP AUTH MANAGER for expansion is 2.3.1.3. You can see that version number in the cvp.auth.min.js by searching for the value "exports.VERSION".

CVP AUTH MANAGER source file can be retrieved from
https://github.com/TurnerBroadcasting/cnn-go-web-fe/tree/develop/src/scripts/vendor/cvp

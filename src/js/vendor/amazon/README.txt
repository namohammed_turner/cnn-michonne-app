======================================================================

DESCRIPTION:

amdb_helper.js and amzn_ads.js are the files used for getting
amazon video pre-roll ads.
See http://docs.turner.com/pages/viewpage.action?pageId=65952389
for more information about these files.
Note that amzn_ads.js lives at http://c.amazon-adsystem.com/aax2/amzn_ads.js
and is pulled directly into michonne so that we can minimize the external
calls to pull this and minimize the chances that video plays will be
delayed due to having to pull this file externally.
Also note that CVP will be, in the near future, integrating the
amdb_helper.js directly into the CVP and the call to
retrieve the amdb_helper.js will also be in that update.
But, for now, we have to include this in the project. Once it
has been included into the CVP, these 2 files will need to be removed
from the project.


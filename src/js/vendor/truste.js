/* jshint node: false, browser: true */

/**
 * TRUSTe have provided the majority of this code however
 * there are parts of this that have been modified in order
 * for it to run correctly and how we wish on our site.
 */

(function ($, window, document) {

    var $trusteBar = $('.user-msg'),
        $adChoices = $('.m-legal__links:contains("AdChoices")').parent(),
        isNewAgreedUser = false,
        userMessageId = 'tos'; /* id in trinity user message cfg */

    /*
     * CNN CODE: Listener callback to display the truste modal
     */
    function trusteModal(e) {
        e.preventDefault();
        $.getScript(CNN.Truste.modal, function getTrusteModalAnon() {
            try {
                view(trusteId);
            } catch(e) { }
        });
    }

    $.fn.trusteConsent = function(e) {
        var c = this,
        d = $.extend({
            domain: 'cnn.com',
            consentButton: null,
            showConsent: null,
            watchEvery: 500,
            watchFor: 3E4
        }, e),
        h = function() {
            if (!truste.eu || !truste.eu.bindMap) {
                return false;
            }
        },
        f = function() {
            var a = c.find(d.consentButton);

            /* CNN CODE: Display cookie message */
            CNN.userMessage.showMessage();

            a.length > 0 && a.one('click', function() {
                truste.eu && truste.eu.actmessage && truste.eu.actmessage({
                    source: 'preference_manager',
                    message: 'submit_preferences',
                    data: {
                        value: 2
                    }
                });

                /* CNN CODE: Remember that this is a new user */
                isNewAgreedUser = true;

                g();
            });
        },
        g = function() {
            /* CNN CODE */
            /* After consent is given add 'Cookies' link to footer */
            $adChoices.clone().html($('<a />', {
                'class': 'm-legal__links',
                href: '/cookie',
                html: truste.eu.bindMap.icon
            }).on('click', h)).insertAfter($adChoices);
            /* If user previously agreed to TRUSTe, do the deferred cookie check and display message if necessary */
            if (!isNewAgreedUser) {
                CNN.userMessage.showDeferredMessage();
            }
        },
        e = function() {
            var a = window.setInterval(function() {
                0 < c.find('div').length && (f(), window.clearInterval(a));
            }, d.watchEvery);

            window.setTimeout(function() {
                window.clearInterval(a);
            }, d.watchFor);
        };

        if (typeof truste === 'object') {
            try {
                (truste.eu.bindMap.behaviorManager === 'eu' ? function() {
                    if (truste.cma && truste.cma.callApi) {
                        var a = d.domain;
                        return ('implied' === truste.cma.callApi('getConsent', a, a, a).source) ? true : (g(), false);
                    }

                    return false;
                }() && e() : (function() {
                    /* CNN CODE: Non EU user, do the deferred cookie check and display message if necessary */
                    CNN.userMessage.showDeferredMessage();
                }()));
            } catch(e) {}
        }

        return this;
    };

    function initTruste() {
        if (CNN.contentModel.edition === 'international') {
            /* Listen for this event to get triggered by user messages before loading TRUSTe
             * Since user messages are also used on domestic, as per requirements
             * we don't want to display this message to an international user
             * even if they are viewing the domestic site.
             * So we only want to initialize this when on the international site.
             */
            $(document).on(userMessageId + '_userMessageReady', function trusteUserMessageReady() {
                $.getScript(CNN.Truste.notice, function getTrusteScriptAnon() {
                    /* Check if we're on international and truste function exists */
                    if ($.fn.trusteConsent) {
                        /* Initialize TRUSTe consent message */
                        $trusteBar.trusteConsent({
                            consentButton: '.js-user-msg--close, .js-user-msg--agree'
                        });

                    }
                });
            });
        }
        /* Add hooks into page elements that will display modal popover */
        $adChoices.on('click', trusteModal)
    }

    /* Fixes execution on static pages */
    if (typeof $.fn.onZonesAndDomReady === 'function') {
        $(document).onZonesAndDomReady(initTruste);
    } else {
        $(document).ready(initTruste);
    }

})(jQuery, window, document);

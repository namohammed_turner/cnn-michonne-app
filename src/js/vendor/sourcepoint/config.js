/* global jQuery, CNN */

/* SourcePoint bootstrap.js included before this */

if (CNN.Features && CNN.Features.enableSourcePoint === true && CNN.SPConfig && CNN.SPConfig.accountId && CNN.SPConfig.mmsDomain) {
    (function (w) {
        var ad = {
                choice: 'nvs',
                d: {
                    abp: false,
                    abt: false
                },
                info: {
                    cmpgn_id: 0,
                    msg_desc: '',
                    msg_id: 0,
                    prtn_uuid: 'nvs'
                },
                o: {
                    oir: false,
                    wl: false
                },
                u: {
                    bucket: 0,
                    uuid: ''
                }
            },
            checkForMessage,
            opt,
            sendMetrics,
            tmrHandler = null;

        sendMetrics = function () {
            if (typeof w.trackMetrics === 'function') {
                w.trackMetrics({
                    type: 'sourcepoint',
                    data: ad
                });
            }
        };

        checkForMessage = function () {
            if (jQuery('.sp_message:visible').length <= 0 && jQuery('.sp_iframe_container:visible').length <= 0) {
                /* No longer showing message, send choice 'nvs' if we haven't already */
                if (tmrHandler !== null) {
                    clearInterval(tmrHandler);
                    tmrHandler = null;
                }
                sendMetrics();
            }
        };

        w._sp_ = w._sp_ || {};
        w._sp_.config = w._sp_.config || {};
        w._sp_.config.account_id = parseInt(CNN.SPConfig.accountId, 10);
        w._sp_.config.mms_domain = CNN.SPConfig.mmsDomain;

        w._sp_.config.content_control_callback = function () {
            console.log('SP CONTENT LOCK');  /* To be replaced later */
        };

        /* Dynamically set config options from runtime config, if any */
        if (typeof CNN.SPConfig.config === 'object' && CNN.SPConfig.config !== null) {
            for (opt in CNN.SPConfig.config) {
                if (CNN.SPConfig.config.hasOwnProperty(opt)) {
                    w._sp_.config[opt] = CNN.SPConfig.config[opt];
                }
            }
        }

        /* Messaging-specific config */
        w._sp_.mms = w._sp_.mms || {};
        w._sp_.mms.cmd = w._sp_.mms.cmd || [];

        w._sp_.config.mms_client_data_callback = function (data) {
            if (data) {
                try {
                    ad = data || ad;
                    delete ad.info.msg_desc;
                    if (typeof ad.msgID !== 'undefined') {
                        delete ad.msgID;
                    }
                    /* Send data to external platform, if ready */
                    if (CNN.SPConfig.waitForChoice === false || ad.d.abp !== true || ad.info.msg_id === 0 || !ad.info.prtn_uuid) {
                        ad.info.prtn_uuid = ad.info.prtn_uuid || 'nvs';
                        sendMetrics();
                    } else {
                        /* Check every 20 seconds to see if the message frame is still there */
                        tmrHandler = w.setInterval(checkForMessage, 10000);
                    }
                } catch (e) {}
            }
        };

        /* Choice for Whitelist Request Callback */
        w._sp_.config.mms_choice_selected_callback = function (choiceID) {
            if (CNN.SPConfig.waitForChoice === true) {
                ad.choice = choiceID || 'nvs';
                /* Send data to external platform */
                if (tmrHandler !== null) {
                    w.clearInterval(tmrHandler);
                    tmrHandler = null;
                    sendMetrics();
                }
            }
        };

        w._sp_.mms.cmd.push(function () {
            w._sp_.mms.setTargeting('x', 'y');
        });

        /* Tell the messaging library it is good to go */
        w._sp_.mms.cmd.push(function () {
            w._sp_.mms.startMsg();
        });
    }(window));
}

/* SourcePoint mms_client.js and msg.js included after this */

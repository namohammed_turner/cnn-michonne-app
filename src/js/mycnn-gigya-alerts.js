/* global CNN, gigya */

/* Define the functions needed for the tab in here */
(function ($) {
    'use strict';

    var isSubscribed = false,
        displayFeedback,
        successStatus,
        subscribe = 'subscribe',
        unsubscribe = 'unsubscribe';

    CNN.mycnn.alerts = {

        show: function () {
            displayFeedback = false;
            CNN.mycnn.alerts.getAlertNewsletter();
        },

        /**
         * Displays alerts html
         */
        displayAlerts: function () {
            var output = {
                isSubscribed: isSubscribed
            };

            window.dust.render('views/cards/tool/mycnn-alerts', output, function (err, out) {
                if (err) {
                    if (window.console && window.console.error) {
                        console.error(err);
                    }
                } else {
                    CNN.mycnn.updateBody(out);
                    $(document.getElementById('mycnn-alerts-edit-email')).hide();

                    if (displayFeedback) {
                        if (successStatus) {
                            $(document.getElementById('mycnn-alerts-message')).html('CNN Breaking News email alerts successfully saved!');
                        } else {
                            $(document.getElementById('mycnn-alerts-message')).html('Error while processing request.');
                        }
                    }
                    CNN.mycnn.alerts.registerHandlers();
                }
            });
        },

        /**
         * Gets the logged in users subscribed news letters
         */
        getAlertNewsletter: function () {
            gigya.accounts.getAccountInfo({callback: CNN.mycnn.alerts.handleSubscriptionsCallback});
        },

        /**
         * Callback that gigya will call once getSubscriptions has finished
         * @param {object} result object result
         */
        handleSubscriptionsCallback: function (result) {
            if (result.errorCode === 0 && CNN.Utils.exists(result.data) &&
                CNN.Utils.exists(result.data.newsletters) && CNN.Utils.exists(result.data.newsletters.textbreakingnews) &&
                CNN.Utils.exists(result.data.newsletters.textbreakingnews.newsletter_sts) && result.data.newsletters.textbreakingnews.newsletter_sts === 1) {
                isSubscribed = true;
            } else {
                isSubscribed = false;
            }
            CNN.mycnn.alerts.displayAlerts();
        },

        /**
         * Register button clicks
         */
        registerHandlers: function () {
            $(document.getElementById('mycnn-alerts-sign-up-button')).click(function () {
                CNN.mycnn.alerts.processRequest(subscribe);
            });

            $(document.getElementById('mycnn-alerts-unsubscribe-button')).click(function () {
                CNN.mycnn.alerts.processRequest(unsubscribe);
            });

            $(document.getElementById('mycnn-alerts-edit-email')).click(function () {
                CNN.mycnn.details.show();
            });
        },

        /**
         * Sign up user for breaking news
         * @param {string} action - subscribe or unsubscribe
         */
        processRequest: function (action) {
            var nowDate = new Date(),
                formattedDate = nowDate.toJSON();

            if (action === subscribe) {
                gigya.accounts.setAccountInfo({
                    data: {
                        'newsletters.textbreakingnews.newsletter_sts': 1,
                        'newsletters.textbreakingnews.last_update_dte': formattedDate,
                        'newsletters.textbreakingnews.create_dte': formattedDate
                    },
                    callback: CNN.mycnn.alerts.handleCallback
                });
            } else {
                gigya.accounts.setAccountInfo({
                    data: {
                        'newsletters.textbreakingnews.newsletter_sts': 4,
                        'newsletters.textbreakingnews.last_update_dte': formattedDate
                    },
                    callback: CNN.mycnn.alerts.handleCallback
                });
            }
        },

        /**
         * handles callback of subscribing to BN newsletter. Sets success if no error; otherwise false
         * @param  {object} response object
         */
        handleCallback: function (response) {
            displayFeedback = true;
            if (response && response.errorCode === 0) {
                successStatus = true;
            } else {
                successStatus = false;
            }

            /* check response and display */
            CNN.mycnn.alerts.getAlertNewsletter();
        }
    };

}(jQuery));

/* global CNN */

/**
 * Used to set a User's Edition preferences
 *
 * Javascript Module Pattern
 * @see http://www.adequatelygood.com/JavaScript-Module-Pattern-In-Depth.html
 *
 * @name CNN.EditionPrefs
 * @namespace
 * @memberOf CNN
 */
CNN.EditionPrefs = CNN.EditionPrefs || (function ($) {
    'use strict';

    var $editionPicker = $('.js-edition-picker__list'),
        CONST_TTL_FOREVER = 854400, // 97 years
        CONST_EDITION_COOKIE = CNN.EditionCookie,
        currentEdition = CNN.contentModel.edition === 'international' ? 'edition' : 'www',
        userEdition = CNN.Utils.getCookie(CONST_EDITION_COOKIE),
        savedEdition = CNN.Utils.exists(userEdition) ? userEdition : currentEdition;

    /**
     * Entry point for module. Initializes listeners but will
     * not set any temporary cookies
     *
     * @private
     */
    function _init() {
        _bindListeners();
        _indicateCurrentEdition();
    }

    /**
     * Setup required event handlers.
     *
     * @private
     */
    function _bindListeners() {
        $editionPicker.on('click', '.js-edition-preferences__confirm', _confirmEditionSelect);
    }

    /**
     * Ensure Edition picker indicates the current set edition.
     *
     * @private
     */
    function _indicateCurrentEdition() {
        jQuery('.js-edition-option').prop('checked', false).filter('[data-type="' + savedEdition + '"]').prop('checked', true);
        jQuery('.js-edition-preferences__choice').prop('checked', false).filter('[value="' + savedEdition + '"]').prop('checked', true);
    }

    /*
     * Sets the user's default edition to the one given.
     *
     * @param {string} choice - The selected edition choice
     * @param {object} $targetChoice - jQuery element for this choice
     * @private
     */
    function _selectEdition(choice, $targetChoice) {
        _setEditionCookie(choice, CONST_TTL_FOREVER);

        CNN.EditionPicker.togglePicker();
        CNN.EditionPicker.changeEdition(false, $targetChoice);
    }

    /**
     * Event handler for confirming edition selection
     *
     * @private
     * @param {object} event - Event object
     */
    function _confirmEditionSelect(event) {
        var $targetParent = $(event.currentTarget).parent(),
            $editionChoice = $targetParent.find('input').filter(':checked');

        _selectEdition($editionChoice.val(), $editionChoice.parent());
    }

    /**
     * Sets a cookie containing the users edition preference.
     *
     * @param {string} edition - The selected edition choice
     * @param {number} ttl - The time to live value for the cookie
     * @private
     */
    function _setEditionCookie(edition, ttl) {
        CNN.Utils.setCNNCookie(CONST_EDITION_COOKIE, edition, ttl);
    }

    /**
     * Hides the edition preference selections
     *
     * @public
     */
    function hidePreferences() {
        /*
         * Defer the removal of this class as other click
         * handlers rely on the presence of this class
         */
        setTimeout(function () {
            $editionPicker.removeClass('list--set');
        }, 0);
    }

    /**
     * Displays the edition preference banner
     *
     * @public
     * @param {object} event - Event object
     */
    function showPreferences(event) {
        $(event.currentTarget).closest('.js-edition-picker__list').addClass('list--set');
    }

    _init();

    return {
        showPreferences: showPreferences,
        hidePreferences: hidePreferences,
        indicateCurrentEdition: _indicateCurrentEdition
    };

})(jQuery);

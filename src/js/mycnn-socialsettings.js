/* global CNN, MSIB */

(function ($) {
    'use strict';

    CNN.mycnn.socialsettings = {

        /**
         * Gathers JSON object output, sends to Dust file
         * for display on MyCNN page, and registers Unlink button
         */
        show: function () {

            /* Create output var with getIdentity, send JSON object to Dust file, and register the Unlink button */
            var
                output = CNN.mycnn.socialsettings.getIdentity();

            window.dust.render('views/cards/tool/mycnn-social-settings', output, function (err, out) {
                if (err) {
                    console.error(err);
                } else {
                    /* Output JSON object 'output.socialSettings' to Dust template */
                    CNN.mycnn.updateBody(out);

                    /* Register Unlink button */
                    CNN.mycnn.socialsettings.registerUnlink();
                }
            });

        },

        /**
         * Determines social login status of user and builds output object
         * @return {object} JSON object for Dust
         */
        getIdentity: function () {
            var
                cnnUser = CNN.mycnn.user,
                output = {
                    socialSettings: {
                        emailOnly: false,
                        socialOnly: false,
                        socialLinked: false,
                        socialLogin: {
                            multipleLinked: false,
                            socialAccounts: []
                        }
                    }
                };

            /* Check if cnnUser is not undefined and cnnUser.hasAuthority is avail */
            if (typeof cnnUser !== 'undefined' && cnnUser !== null && cnnUser.hasAuthority) {

                /* Check user authorities to determine login type */
                /* If user is logged in via social network and it is linked to email address */
                if (cnnUser.hasAuthority('EMAIL') && cnnUser.hasAuthority('GIGYA')) {

                    output.socialSettings.socialLinked = true;

                    /* Gather Social Network(s) info */
                    CNN.mycnn.socialsettings.getSocialIdentity(output);

                    /* Gather CNNid info */
                    CNN.mycnn.socialsettings.getCNNidInfo(output);

                /* If only logged in via email address and no social network login */
                } else if (cnnUser.hasAuthority('EMAIL') && !cnnUser.hasAuthority('GIGYA')) {

                    output.socialSettings.emailOnly = true;

                    /* Gather CNNid info */
                    CNN.mycnn.socialsettings.getCNNidInfo(output);

                /* If only logged in via social network and doesn't have link to email address */
                } else if (cnnUser.hasAuthority('GIGYA') && !cnnUser.hasAuthority('EMAIL')) {

                    output.socialSettings.socialOnly = true;

                    /* Gather Social Network info */
                    CNN.mycnn.socialsettings.getSocialIdentity(output);
                }

                return output;
            }
        },

        /**
         * Determines social network links and calls getSocialInfo for possible data
         * @param {object} output - Output JSON object
         * @returns {object} adds to JSON object for Dust
         */
        getSocialIdentity: function (output) {
            var cnnUser = CNN.mycnn.user;

            /* Check if cnnUser and cnnUser.gigya aren't null or undefined */
            if (typeof cnnUser !== 'undefined' && cnnUser !== null &&
                typeof cnnUser.gigya !== 'undefined' && cnnUser.gigya !== null) {

                /* Are there more than one social account linked? */
                if (typeof cnnUser.gigya.providers !== 'undefined' && cnnUser.gigya.providers !== null && cnnUser.gigya.providers.length > 1) {
                    output.socialSettings.socialLogin.multipleLinked = true;
                }

                /* Call getSocialInfo to check if Social Account is linked, & gather info if so & add to output */
                CNN.mycnn.socialsettings.getSocialInfo('facebook', output);
                CNN.mycnn.socialsettings.getSocialInfo('googleplus', output);
                CNN.mycnn.socialsettings.getSocialInfo('linkedin', output);
                CNN.mycnn.socialsettings.getSocialInfo('twitter', output);

                return output;
            }
        },

        /**
         * Checks if social network is linked and gathers social network info; nickname, photoURL, and profileURL
         * @param {string} socialNetwork - The name of the Social Network to check for and gather data
         * @param {object} output - Output JSON object
         * @returns {object} adds to JSON object for Dust
         */
        getSocialInfo: function (socialNetwork, output) {
            var cnnUser = CNN.mycnn.user,
                key = socialNetwork,
                gigyaIdentities = {},
                networkToCss = {
                    twitter: 'twitter',
                    facebook: 'facebook',
                    googleplus: 'google-plus',
                    linkedin: 'linked-in'
                };

            if (typeof cnnUser !== 'undefined' && cnnUser !== null &&
                typeof cnnUser.gigya !== 'undefined' && cnnUser.gigya !== null &&

                cnnUser.gigya.identities !== 'undefined' && cnnUser.gigya.identities !== null && cnnUser.gigya.identities) {

                gigyaIdentities = cnnUser.gigya.identities;

                if (gigyaIdentities[key]) {
                    output.socialSettings.socialLogin.socialAccounts.push({
                        network: key,
                        cssClass: networkToCss[key],
                        displayName: gigyaIdentities[key].nickname,
                        photoURL: gigyaIdentities[key].photoURL,
                        profileURL: gigyaIdentities[key].profileURL
                    });

                    return output;
                }
            }
        },

        /**
         * Checks for user authorities, then creates emailLogin object and gathers CNNid info; USERNAME authority and photoURL
         * @param {string} output - The output JSON object
         * @returns {object} adds to JSON object for Dust
         */
        getCNNidInfo: function (output) {
            var cnnUser = CNN.mycnn.user;

            /* Gather CNNid displayName, and avatar URL */
            if (typeof cnnUser !== 'undefined' && cnnUser !== null &&
                typeof cnnUser.msib !== 'undefined' && cnnUser.msib !== null &&
                typeof cnnUser.msib.authorities !== 'undefined' && cnnUser.msib.authorities !== null &&
                CNN.msib.avatarForScreenName && cnnUser.getAuthority) {

                output.socialSettings.socialLogin.emailLogin = {};
                output.socialSettings.socialLogin.emailLogin.displayName = cnnUser.msib.authorities.USERNAME;
                output.socialSettings.socialLogin.emailLogin.photoURL = CNN.msib.avatarForScreenName(60, cnnUser.getAuthority('USERNAME'));

                return output;
            }
        },

        /**
         * Registers Unlink button to call deleteAuthorities() when clicked
         */
        registerUnlink: function () {
            $(document.getElementById('social-settings__unlink')).click(function () {
                CNN.mycnn.socialsettings.deleteAuthorities();
            });
        },

        /**
         * Removes GIGYA authority from CNN.mycnn.user object thus 'unlinking' all Social accounts (can't unlink individual accounts)
         * If social account only then initiate addUsernameAuthority which also deletes GIGYA auth when complete
         * Also removes FACEBOOK, and TWITTER authorities if present since Gigya adds them in as an MSIB hack
         * Then performs logout of user
         */
        deleteAuthorities: function () {
            var cnnUser = CNN.mycnn.user;

            /* Check to make sure cnnUser is real and there is hasAuthority function */
            if (typeof cnnUser !== 'undefined' && cnnUser !== null && cnnUser.hasAuthority) {
                /* if logged in via social account only (never linked to email) then add USERNAME authority
                then callback will check for USERNAME so we can remove the GIGYA authority without issues */
                if (cnnUser.hasAuthority('GIGYA') && !cnnUser.hasAuthority('EMAIL')) {
                    CNN.mycnn.socialsettings.addUsernameAuthority(CNN.mycnn.socialsettings.handleUserEditCallback);
                } else {
                    /* Remove GIGYA authority; thus unlinking all social accounts from email address */
                    CNN.mycnn.socialsettings.removeSocialAuthority('GIGYA', CNN.mycnn.socialsettings.handleUnlinkCallback);
                }
                /* remove twitter and facebook authorities Gigya uses so user can login with Facebook or Twitter later if they want without linking */

                /* remove facebook authority is present */
                if (cnnUser.hasAuthority('FACEBOOK')) {
                    CNN.mycnn.socialsettings.removeSocialAuthority('FACEBOOK');
                }

                /* remove twitter authority is present */
                if (cnnUser.hasAuthority('TWITTER')) {
                    CNN.mycnn.socialsettings.removeSocialAuthority('TWITTER');
                }

                /* End process by logging out the user with time to allow auth removals and success message reading by user */
                setTimeout(function () {
                    var loc = window.location,
                        baseUrl = loc.protocol + '//' + loc.hostname +
                            (loc.port ? (':' + loc.port) : '');

                    /* Logout, then redirect to the login page,
                     * which then redirects to the MyCNN page. */
                    cnnUser.logout(baseUrl + CNN.msib.getLoginUrl(baseUrl + '/mycnn'));
                }, 3000);
            }
        },

        /**
         * Removes authority in user profile and handles callback if given
         * @param {string} authorityName - The name of the Authority to remove
         * @param {function} callback - callback function
         */
        removeSocialAuthority: function (authorityName, callback) {
            var cnnUser = CNN.mycnn.user;

            if (typeof MSIB.msapi !== 'undefined' && typeof MSIB.msapi.authority !== 'undefined' &&
                typeof MSIB.util !== 'undefined' && MSIB.util.getTid && MSIB.util.getAuthid && MSIB.util.getAuthpass) {

                if (cnnUser.hasAuthority(authorityName)) {
                    MSIB.msapi.authority.remove({
                        type: authorityName,
                        tid: MSIB.util.getTid(),
                        authid: MSIB.util.getAuthid(),
                        authpass: MSIB.util.getAuthpass()
                    },
                    callback);
                }
            }
        },

        /**
         * Used as callback when removing an authority.
         * Then sends text to div to let user know it worked and will be logged out.
         * @callback
         * @param {object} xhr - request response
         */
        handleUnlinkCallback: function (xhr) {
            /* possible arguments: xhr, responseText, msg */

            if (typeof CNN.msib !== 'undefined' && CNN.msib.isSuccessStatus) {
                if (CNN.msib.isSuccessStatus(xhr)) {
                    /* Send success message and logout warning to div above content */
                    $(document.getElementById('js-mycnn-social-settings__message')).text('Success! You will now be logged out.');
                } else {
                    $(document.getElementById('js-mycnn-social-settings__message')).text('There was a problem Unlinking your account. Please try again later.');
                }
            }
        },

        /**
         * Adds USERNAME authority so we can remove GIGYA authority since we need to have at least 1 authority
         * Creates a "dummy" USERNAME authority (12 characters), with prefix 'GONE' (so we can easily find and delete later) + random password
         * @param {function} callback - callback function
         */
        addUsernameAuthority: function (callback) {
            var randUserPassword = Math.random().toString(36).substr(2, 8),  /* create random 8 char password */
                paramsEditUser = {
                    type: 'GET',
                    tid: MSIB.util.getTid(),
                    authid: MSIB.util.getAuthid(),
                    authpass: MSIB.util.getAuthpass(),
                    password: randUserPassword,
                    confirmPassword: randUserPassword,
                    username: 'GONE' + Math.random().toString(36).substr(2, 8) // creates GONE + 8 random characters
                };

            if (typeof MSIB.msapi !== 'undefined' && typeof MSIB.msapi.user !== 'undefined' && MSIB.msapi.user.edit) {
                MSIB.msapi.user.edit(paramsEditUser, callback);
            }
        },

        /**
         * Used as callback when adding USERNAME authority.
         * On Success initiates GIGYA authority removal via removeSocialAuthority().
         * @callback
         * @param {object} xhr - response object
         */
        handleUserEditCallback: function (xhr) {
            /* possible arguments: xhr, responseText, msg */

            if (typeof CNN.msib !== 'undefined' && CNN.msib.isSuccessStatus) {
                if (CNN.msib.isSuccessStatus(xhr)) {
                    /* Remove GIGYA authority; thus 'resetting' social account for ability to link again */
                    CNN.mycnn.socialsettings.removeSocialAuthority('GIGYA', CNN.mycnn.socialsettings.handleUnlinkCallback);
                } else {
                    $(document.getElementById('js-mycnn-social-settings__message')).text('There was a problem resetting your account. Please try again later.');
                }
            }
        }
    };
}(jQuery));

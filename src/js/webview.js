/* global CNN, Livefyre, MSIB */

/*
 * Events for webview pages - iOS and Android mobile clients.
 *
 * When a page is encapsulated in a webview, the client-side code will call
 * these functions to notify the containing app when notable events occur.
 *
 * Livefyre comments count is retrieved outside this API - app needs to load
 * comments count even before load the webview.
 *
 * Here is an example to get comments count:
 *      <host>:8080/ncomments/data/ocs/article/2014/11/13/world/comet-landing/index.html:*.json
 * and the response:
 *    {
 *       "siteId": 304762,
 *       "articleId": "article_E55DCE12-F7E7-AFD6-8B0D-A897326A4C72",
 *       "url": "http://cnn-qa.bootstrap.fyre.co/api/v1.1/public/comments/ncomments/MzA0NzYyOmFydGljbGVfRTU1RENFMTItRjdFNy1BRkQ2LThCMEQtQTg5NzMyNkE0Qzcy.json",
 *       "status": "ok",
 *       "statusCode": 200,
 *       "error": false
 *   }
 * App client will invoke the url from the response to get the actual count from Livefyre.
 *
 * NOTE: Michonne controller/munger is updated to handle ncomments/data/ocs/<story-url>:*.json
 *
 */

CNN.SocialConfig = CNN.SocialConfig || {};
CNN.SocialConfig.msib = CNN.SocialConfig.msib || {};

(function ($) {
    'use strict';

    var lftoken;

    $(window).load(function () {
        webviewCallback('ready', 'undefined');
    });

    /**
     * A signal to the mobile app that an event has occured
     * @param {string} status - status string
     * @param {string} query - query string
     */
    function webviewCallback(status, query) {
        if (status === 'ready') {
            CNN.Webview.Livefyre.Clientcallback.authenticationReady(query);
        } else if (status === 'success') {
            CNN.Webview.Livefyre.Clientcallback.authenticationSucceeded(query);
        } else if (status === 'fail') {
            CNN.Webview.Livefyre.Clientcallback.authenticationFailed(query);
        }
    }

    /**
     * Callback to handle sending the value of authentication
     * @param {object} login the value of an authentication response
     * @return {boolean} - true if login is authenticated
     */
    function handleAuthentication(login) {
        var status,
            isAuthenticated,
            query;

        login = login || {};
        status = login.status || 'fail';
        isAuthenticated = login.isAuthenticated || false;
        query = login.msg;

        webviewCallback(status, query);
        CNN.Webview.Livefyre.isAuthenticated = isAuthenticated;

        return isAuthenticated;
    }

    /**
     * Handles errors within the api
     * @param {object} error - error object
     * @returns {object} - LiveFyre error
     */
    function onError(error) {
        error = error || {};
        CNN.Webview.Livefyre.Clientcallback.error(error.type, error.msg);
        CNN.Webview.Livefyre.error = error;

        return CNN.Webview.Livefyre.error;
    }

    /**
     * Livefyre auth delegate to sign in/out user.
     *
     * @param {string} lfToken - livefyre token
     */
    function lfAuthDelegate(lfToken) {
        Livefyre.require(['fyre.conv#3', 'auth'], function (Conv, auth) {
            auth.delegate({
                login: function (cb) {
                    cb(null, {livefyre: lfToken});
                },
                logout: function (cb) {
                    cb(null);
                }
            });
            auth.authenticate({
                livefyre: lfToken
            });
        });
    }

    window.CNN.Webview = window.CNN.Webview || {};

    /**
     * @name Livefyre
     * @memberOf Webview
     */
    CNN.Webview.Livefyre = {
        isAuthenticated: false,
        error: null,

        /**
         * Authenticate user by taking basic credentials.
         *
         * @param {string} tid token id
         * @param {string} authid authorization id
         */
        authenticate: function (tid, authid) {
            var result = {};

            MSIB.msapi.livefyre.getToken({
                tid: tid,
                authid: authid
            }, function (xhr, textStatus, msg) {
                if (CNN.msib.isSuccessStatus(xhr)) {
                    lftoken = xhr.responseJSON.lfauthtoken;
                    lfAuthDelegate(lftoken);
                    result.status = 'success';
                    result.isAuthenticated = true;
                    result.msg = lftoken;
                } else {
                    onError({msg: msg, type: 'authentication'});
                    result.status = 'fail';
                    result.isAuthenticated = false;
                    result.msg = 'undefined';
                }
                handleAuthentication(result);
            });
        },

        /**
         * Authenticate user by just Livefyre token
         * NOTE: Currently Livefyre login (fyre.conv.login) does not have for callback to
         *  decide whether login was success or fail.
         *  So, this API always returns success callback. What's the impact on UI - there will be
         *  no automatic login using tid & authid and user will login using Sign in link.
         *
         * @param {string} token Encoded Lifefyre token
         */
        authenticateLight: function (token) {
            var result = {};

            lfAuthDelegate(token);
            result.status = 'success';
            result.isAuthenticated = true;
            result.msg = token;
            handleAuthentication(result);
        },

        /**
         * Logout the user from Livefyre
         */
        logout: function () {
            CNN.Webview.Livefyre.Clientcallback.logout();
        }
    };

    /*
     * Common callbacks for iOS and Android.
     *
     * iOS will make use of returned callbacks but Android will override these functions.
     */
    CNN.Webview.Livefyre.Clientcallback = {
        authenticationSucceeded: function (query) {
            window.location.hash = CNN.SocialConfig.msib.authUrl + '?status=success&lftoken=' + query;
        },

        authenticationFailed: function (query) {
            window.location.hash = CNN.SocialConfig.msib.authUrl + '?status=fail&lftoken=' + query;
        },

        authenticationReady: function (query) {
            window.location.hash = CNN.SocialConfig.msib.authUrl + '?status=ready&lftoken=' + query;
        },

        error: function (type, msg) {
            window.location.hash = CNN.SocialConfig.msib.errorUrl + '?type=' + type + '&msg=' + msg;
        },

        login: function () {
            window.location.hash = CNN.msib.getLoginUrl();
        },

        logout: function () {
            /* auth.delegate should handle sign out */
        }
    };

}(jQuery));

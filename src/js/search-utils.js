
(function ($) {
    'use strict';

    /*
     * to use: $.urlParam('name_of_url_param')
     * returns: value of name_of_url_param
     */
    $.extend({
        urlParams: function getUrlParams() {
            var vars = [], hash, i,
                hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
            for (i = 0; i < hashes.length; i++) {
                hash = hashes[i].split('=');
                vars.push(hash[0]);
                vars[hash[0]] = hash[1];
            }
            return vars;
        },
        urlParam: function getUrlParam(name) {
            return $.urlParams()[name];
        }
    });

    /*
     * to use: $.objectToString(obj)
     * returns: key1~value1&key2~value2...
     */
    $.extend({
        objectToString: function objToStrHandler(obj) {
            var str = '';
            $.each(obj, function objToStrHandlerEach(k, v) {
                if (typeof k === 'number') {   /* if it's an array, string the array of objects together */
                    str += $.objectToString(v);
                } else {
                    str += k + '~' + v + '&';
                }
            });
            return str;
        }
    });

    /*
     * prototyping
     */
    String.prototype.toTitleCase = function () {
        return this.replace(/\w\S*/g, function upperCaseFirst(txt) {
            return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
        });
    };
}(jQuery));


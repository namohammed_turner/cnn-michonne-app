/* global CNN, store */

CNN.Money = {
    cardLoaded: {
        currencies: false,
        commodities: false,
        markets: false,
        marketsBasic: false
    }
};

CNN.Financial = (function setUpFinancial($, window, document) {

    'use strict';

    var hasLocalStorage = typeof store !== 'undefined' && store.enabled,
        cardLoadPromises = {};
    function startUp() {
        var cardsConfig = {
            currencies: {
                $el: $(document.getElementById('js-financial-currencies')),
                ssiPath: '/.element/ssi/auto/4.0/sect/INTLEDITION/iBUSINESS/wsod_currencies.html'
            },
            commodities: {
                $el: $(document.getElementById('js-financial-commodities')),
                ssiPath: '/.element/ssi/auto/4.0/sect/INTLEDITION/iBUSINESS/wsod_commodities.html'
            },
            markets: {
                $el: $(document.getElementById('js-financial-markets')),
                ssiPath: '/.element/ssi/auto/4.0/sect/MAIN/markets_wsod_expansion.html'
            },
            marketsBasic: {
                $el: $(document.getElementById('js-financial-markets-basic')),
                ssiPath: '/.element/ssi/auto/4.0/sect/MAIN/hp_markets_wsod_expansion.html'
            }
        };
        /* If cards are on the page initialize them. */
        $.each(cardsConfig, _loadCard);
    }

    /**
     * Makes a proxy request for the HTML markup containing
     * the latest data to be displayed for this module.
     *
     * @param {object} cardConfig - Card specific config
     * @return {object} The promise object
     * @private
     */
    function _fetchContentBody(cardConfig) {
        return $.Deferred(function (dfd) {
            $.ajax({
                url: cardConfig.ssiPath,
                dataType: 'html',
                type: 'get',
                success: function (responseHtml) {
                    cardConfig.$el.append(responseHtml);
                    dfd.resolve();
                }
            });
        }).promise();
    }

    /**
     * Changes the currently active tab on the card as well
     * as revealing the new associated content and hiding the old.
     *
     * @param {object} $targetCard - The card jQuery element
     * @return {function} The event handler
     * @private
     */
    function _tabChangeHandler($targetCard) {
        var $quoteContentEls = $targetCard.find('.js-quote-items'),
            $quoteHeaders = $targetCard.find('.js-quote-headers');

        return function (event) {
            var $targetTab = $(event.currentTarget),
                tabIndex = $targetTab.index();

            $targetTab.addClass('m-financial__tabs-item--active')
                .siblings().removeClass('m-financial__tabs-item--active');

            $quoteContentEls.removeClass('m-financial__quote-content--active')
                .eq(tabIndex).addClass('m-financial__quote-content--active');

            $quoteHeaders.hide()
                .eq(tabIndex).show();

            dataStore($targetCard, 'tabs', tabIndex);
        };
    }

    /**
     * Handles the loading of a new card element.
     *
     * @private
     * @param {string} name - element name
     * @param {object} cardConfig - card config object
     */
    function _loadCard(name, cardConfig) {
        if (CNN.Money.cardLoaded[name] === true) {
            return;
        }

        $.Deferred(function (dfd) {
            if (cardConfig.$el.length) {
                $.when(_fetchContentBody(cardConfig)).done(function () {
                    _bindEvents(cardConfig.$el);
                    restoreState(cardConfig.$el);

                    dfd.resolve(cardConfig.$el);
                });

                CNN.Money.cardLoaded[name] = true;

                cardLoadPromises[name] = dfd.promise();
            }
        });
    }

    /**
     * Setup required event handlers.
     *
     * @param {object} $targetCard - The card jQuery element
     * @private
     */
    function _bindEvents($targetCard) {
        $targetCard.on('click', '.js-tab-item', _tabChangeHandler($targetCard));
    }

    /**
     * Restore the last state the user left the
     * card in - including the active tab.
     *
     * @param {object} $targetCard - The card jQuery element
     * @public
     */
    function restoreState($targetCard) {
        $targetCard.find('.js-tab-item').eq(dataStore($targetCard, 'tabs')).click();
    }

    /**
     * Get or set data in browser local storage. The
     * number of params passed determines if this is
     * a set or get call.
     *
     * @param {object} $targetCard - The card jQuery element
     * @param {string} cardState - Specific state to apply to reference key
     * @param {*} data - What is to be stored
     * @return {*} Result of the set or get function call
     * @public
     */
    function dataStore($targetCard, cardState, data) {
        var key = $targetCard.attr('id') + '_' + cardState + '_' + CNN.Edition;

        if (hasLocalStorage) {
            if (typeof data === 'undefined') {
                return store.get(key);
            } else {
                return store.set(key, data);
            }
        }
    }

    /**
     * Get the associated load promise object for
     * a specific card element.
     *
     * @param {object} cardName - The name of the card type
     * @return {object} Promise object
     * @public
     */
    function getLoadPromise(cardName) {
        return cardLoadPromises[cardName] || $.Deferred().reject();
    }

    /**
     * Prevents default submit behaviour.
     *
     * @return {boolean} Falsey value
     * @public
     */
    function stopSubmit() {
        return false;
    }

    /* Expose public methods*/
    return {
        restoreState: restoreState,
        dataStore: dataStore,
        checkLoaded: getLoadPromise,
        stopSubmit: stopSubmit,
        init: startUp
    };

}(jQuery, window, document));

if (typeof CNN.contentModel === 'object' && !CNN.contentModel.lazyLoad) {
    jQuery(document).onZonesAndDomReady(function () {
        'use strict';
        /*
        * Populates divs with ID with HTML by making an ajax call
        */
        CNN.Financial.init();
    });
}

if (CNN.Utils.existsObject(CNN.Financial) && typeof CNN.Financial.init === 'function') {
    CNN.Financial.init();
}

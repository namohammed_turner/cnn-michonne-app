/* global CNN */

CNN.Carousel = CNN.Carousel || {};
CNN.Features = CNN.Features || {};

/**
 * Provides functions to render Ad on a slide
 * for a given wrapper
 *
 * @name AdSlide
 * @namespace
 * @memberOf CNN
 *
 * @param {string} wrapperId - The id or class of the wrapper which will have
 *                             the special Ad.
 * @param {boolean} isId - true if the wrapperId is an element id, false if
 *                         it is a class name.
 * @param {string} singleton - The request URL for the singleton script to
 *                             request for this ad, if any.
 */
CNN.AdSlide = function (wrapperId, isId, singleton) {
    'use strict';

    /* Only setup if we've enabled gallery ads */
    if (CNN.Features.enableGalleryAds && !CNN.Features.displayDisplayAds) {
        this.wrapperId = wrapperId;
        this.wrapperEl = undefined;
        this.adSlideEl = undefined;
        this.adPrevEl = undefined;
        this.adNextEl = undefined;
        this.singleton = singleton;
        this.clickCounter = 0;

        this.initAdSlide(wrapperId, isId);
    }
};

CNN.AdSlide.prototype = {

    /**
     * Get the wrapper to place the Ad
     * @param {string} wrapperId - The id or class of the wrapper which will have the special Ad.
     * @param {boolean} isId - true if the wrapperId is an element id, false if it is a class name.
     * @returns {Element} the gallery element
     */
    getWrapper: function getWrapper(wrapperId, isId) {
        'use strict';

        var wrapperSelector,
            $adWrapper;

        wrapperSelector = isId ? wrapperId : ('.' + wrapperId);

        if (isId) {
            $adWrapper = jQuery(document.getElementById(wrapperId)).eq(0);
        } else {
            $adWrapper = jQuery(wrapperSelector).eq(0);
        }
        this.wrapperEl = $adWrapper;

        return $adWrapper;
    },

    /**
     * Get the Ad slide in the gallery
     * @return {Element} the gallery ad slide element
     */
    getAdSlide: function getAdSlide() {
        'use strict';

        var adSlideSelector = '.el-carousel__wrapper .ad-slide',
            $adWrapper = this.wrapperEl || this.getWrapper(),
            $adSlide;

        $adSlide = jQuery($adWrapper).find(adSlideSelector).eq(0);
        this.adSlideEl = $adSlide;

        return $adSlide;
    },

    /**
     * Previous navigation arrow on the Ad slide
     * @return {Element} the prev nav arrow element
     */
    getAdSlidePrev: function getAdSlidePrev() {
        'use strict';

        var adPrevSelector = '.ad-slide__prev',
            $adSlide = this.adSlideEl || this.getAdSlide(),
            $adPrev;


        $adPrev = jQuery($adSlide).find(adPrevSelector).eq(0);
        this.adPrevEl = $adPrev;

        return $adPrev;
    },

    /**
     * Next navigation arrow on the Ad slide
     * @return {Element} the next nav arrow element
     */
    getAdSlideNext: function getAdSlideNext() {
        'use strict';

        var adNextSelector = '.ad-slide__next',
            $adSlide = this.adSlideEl || this.getAdSlide(),
            $adNext;


        $adNext = jQuery($adSlide).find(adNextSelector).eq(0);
        this.adNextEl = $adNext;

        return $adNext;
    },

    /**
     * Fetch the singleton ad for display within the wrapper
     *
     * @param {string} _adSingleton - js to render the singleton ad
     */
    getSingletonAd: function getSingletonAd(_adSingleton) {
        'use strict';

        var self = this;
        jQuery.getScript(this.singleton)
            .done(function (_script, _textStatus) {
                jQuery(self).trigger('ad-single-loaded');
            })
            .fail(function (_jqxhr, _settings, _exception) {
                if (typeof console === 'object' && typeof console.log === 'function') {
                    console.log('Could not load singleton script');
                }
            });
    },

    /**
     * Show the Ad slide
     */
    showAdSlide: function showAdSlide() {
        'use strict';
        var $adSlide = this.adSlideEl || this.getAdSlide(),
            isSingletonEmpty = true,
            adSlideIframe;

        jQuery(document).on('GPTRenderComplete', function adSingletonBody(e) {
            if (typeof e.originalEvent.detail !== 'undefined' &&
                e.originalEvent.detail !== null &&
                typeof e.originalEvent.detail.pos !== 'undefined' &&
                Array.isArray(e.originalEvent.detail.pos) &&
                e.originalEvent.detail.pos.length > 0 &&
                typeof e.originalEvent.detail.empty === 'boolean') {

                isSingletonEmpty = (e.originalEvent.detail.pos[0] === 'mod') ? e.originalEvent.detail.empty : true;
                adSlideIframe = $adSlide.find('iframe');

                if (adSlideIframe.length > 0 && !isSingletonEmpty) {
                    $adSlide.removeClass('ad-slide--inactive').addClass('ad-slide--active');
                }
            }
        });
    },

    /**
     * Hide the Ad slide
     */
    hideAdSlide: function hideAdSlide() {
        'use strict';
        var $adSlide = this.adSlideEl || this.getAdSlide();

        $adSlide.removeClass('ad-slide--active').addClass('ad-slide--inactive');
        this.removeCloseAd();

        /* Refresh carousel state after injected ad code executes */
        if (typeof CNN.Carousel.refreshCarousels === 'function') {
            setTimeout(CNN.Carousel.refreshCarousels, 0);
        }
    },

    /**
     * Show the ad slide and bind event handlers
     */
    showAdHandler: function showAdHandler() {
        'use strict';

        this.showAdSlide();
        this.bindCloseAd();
    },

    /**
     * Trigger the ad display based on click count
     */
    adTriggerHandler: function adTriggerHandler() {
        'use strict';
        var clickMatch = (CNN.AdsConfig && CNN.AdsConfig.galleryAdClicks) ? CNN.AdsConfig.galleryAdClicks : 5;

        this.clickCounter++;
        if (this.clickCounter === clickMatch) {
            this.clickCounter = 0;
            this.getSingletonAd();
            jQuery(this).on('ad-single-loaded', this.showAdHandler);
        }
    },

    /**
     * Handles the close ad event
     */
    closeAdHandler: function closeAdHandler() {
        'use strict';

        this.removeCloseAd();
        this.hideAdSlide();
        this.clickCounter = -1;
    },

    /**
     * Add the event listener for click on wrapper
     */
    bindWrapperClick: function bindWrapperClick() {
        'use strict';
        var $adWrapper = this.wrapperEl || this.getWrapper();

        $adWrapper.on('click', jQuery.proxy(this.adTriggerHandler, this));
    },

    /**
     * Add the event listener for the ad close button
     */
    bindCloseAd: function bindCloseAd() {
        'use strict';
        var $adPrev = this.adPrevEl || this.getAdSlidePrev(),
            $adNext = this.adNextEl || this.getAdSlideNext(),
            $adSlide = this.adSlideEl || this.getAdSlide();

        $adPrev.on('click', jQuery.proxy(this.closeAdHandler, this));
        $adNext.on('click', jQuery.proxy(this.closeAdHandler, this));

        /* Close ad on any gallery thumbnail clicks */
        $adSlide.siblings('.js-owl-filmstrip').one('click', jQuery.proxy(this.closeAdHandler, this));
    },

    /**
     * Initialize the Ad slide
     *
     * @param {string} wrapperId - The id or class of the wrapper which will have the special Ad.
     * @param {boolean} isId - true if the wrapperId is an element id, false if it is a class name.
     */
    initAdSlide: function initAdSlide(wrapperId, isId) {
        'use strict';

        this.getWrapper(wrapperId, isId);
        this.bindWrapperClick();
        this.bindCloseAd();
    },

    /**
     * Removes the event listener for click on wrapper
     */
    removeWrapperClick: function removeWrapperClick() {
        'use strict';
        var $adWrapper = this.wrapperEl || this.getWrapper();
        $adWrapper.off('click');
    },

    /**
     * Removes the event listener for the ad close button
     */
    removeCloseAd: function removeCloseAd() {
        'use strict';
        var $adPrev = this.getAdSlidePrev(),
            $adNext = this.getAdSlideNext();
        $adPrev.off('click');
        $adNext.off('click');
    }
};

(function (NS) {
    'use strict';

    if (typeof NS.renderAds === 'function' || NS.Features.enableEpicAds !== true) { return; }

    NS.renderAds = function (elSelector, adSelector, options) {
        var $ads = jQuery(elSelector).not('.m-navigation'),
            $ad,
            i,
            adsLen;

        for (i = 0, adsLen = $ads.length; i < adsLen; i++) {

            $ad = $ads.eq(i);

            /* check if this ad is inside a staggered zone a */
            if ($ad.parents('.zn-staggered').length) {
                /* continue if this function isn't called by the staggered zone script */
                if (options.staggeredZone === false) { continue; }

                jQuery($ad).next(adSelector).append($ad.val());
            } else {
                if (typeof $ad[0] !== 'undefined') {
                    jQuery($ad).next(adSelector).html($ad.val());
                }
            }
        }
    };

    jQuery(document).onZonesAndDomReady(function () {
        NS.renderAds('.js-adtag', '.js-adcontent', {staggeredZone:false});
    });

}(CNN));

/**
 * Sets the companion ad state as a property on the namespace called
 * companionAdState. The possible states are stored in a formatted
 * string array where each state is ordered by the minimum width for the
 * state from least to greatest.
 *
 * Example of what the breakpoints parameter can look like:
 *     'mobile:0', 'tablet:700', 'desktop:1150'
 *
 * @param {object} win - The browser's window object.
 *
 * @param {object} doc - The browser's document object.
 *
 * @param {object} ns - The namespace object where the property will
 * live.
 *
 * @param {array} adStates - The array of possible states the companion
 * ad can be in and their associated minimum widths.
 */
(function setCompanionAdState(win, doc, ns, adStates) {
    'use strict';

    var lib;

    if (ns.Features.enableEpicAds !== true) {
        ns.companionAdState = '';
        return;
    }

    /**
     * This is an inner library for parsing the client's viewport and
     * how to categorize it for companion ads.
     */
    lib = {
        /**
         * Pulls information from the browser about the client's
         * viewport size.
         *
         * @returns {object} A simple object with the properties of
         * width and height beoth resolving to numbers.
         */
        getViewport: function getViewportSize() {
            var myHeight = 0;

            if (typeof win.innerWidth === 'number') {
                /* Non-IE */
                myHeight = win.innerHeight;
            } else if (doc.documentElement && (doc.documentElement.clientWidth || doc.documentElement.clientHeight)) {
                /* IE 6+ in 'standards compliant mode' */
                myHeight = doc.documentElement.clientHeight;
            } else if (doc.body && (doc.body.clientWidth || doc.body.clientHeight)) {
                /* Lower IE compatible */
                myHeight = doc.body.clientHeight;
            }
            return {
                width: ns.CurrentSize && typeof ns.CurrentSize.getClientWidth === 'function' ? ns.CurrentSize.getClientWidth() : 0,
                height: myHeight
            };
        },

        /**
         * The label returned has an associated width that is less then
         * or equal to the client's viewport but greater then any other
         * labels before it.
         *
         * @param {array} breakpoints - An array of formatted strings
         * ordered from least to greatest miniumn widths to label.
         *
         * @returns {string} The label associated with the state that
         * the browser falls under.
         */
        getCompanionAdStateLabel: function getTheReadableLabelForCompanionAdState(breakpoints) {
            var width = this.getViewport().width,
                i = breakpoints.length,
                info;

            do {
                i -= 1;
                info = breakpoints[i] || {};
            } while (width < info.minWidth && i >= 0);

            return info.label || '';
        }
    };

    ns.companionAdState = ns.companionAdState || '';

    if (typeof ns.companionAdState !== 'string' || (typeof ns.companionAdState === 'string' && ns.companionAdState === '')) {
        ns.companionAdState = lib.getCompanionAdStateLabel(adStates);
    }
}(window, document, CNN, (CNN && CNN.AdsConfig && CNN.AdsConfig.companionAdStates) ? CNN.AdsConfig.companionAdStates : []));

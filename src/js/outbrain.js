/* global CNN, fastdom */

CNN.Features = CNN.Features || {};
CNN.Outbrain = CNN.Outbrain || {};

CNN.Outbrain.init = function () {
    'use strict';

    var $ = jQuery,
        $outbrainWidgets = $('.js-m-outbrain'),
        hideOutbrainBreakpoint = 640; /* 8 cols */

    CNN.CurrentSize = CNN.CurrentSize || {};
    CNN.CurrentSize.getClientWidth = CNN.CurrentSize.getClientWidth || $.noop;

    /**
     * Disables the rendering of additional Outbrain widgets
     * that are placed in the in the left rail of the page when
     * the viewport is below our desired display threshold.
     *
     * @private
     */
    function _disableAdditionalWidgets() {
        if (CNN.CurrentSize.getClientWidth() <= hideOutbrainBreakpoint) {
            $outbrainWidgets.filter(function () {
                return $(this).closest('.pg-rail').length;
            }).removeClass('OUTBRAIN');
        }
    }

    /**
     * Load the Outbrain core library which in turn
     * will render any Outbrain widgets on the page.
     *
     * @private
     */
    function _loadOutbrain() {
        $.ajax({
            url: '//widgets.outbrain.com/outbrain.js',
            dataType: 'script',
            cache: true
        });
    }

    function _hideAdOutbrain() {
        fastdom.mutate(function hideAdOutbrain() {
            if (CNN.contentModel.pageType === 'section' &&
                CNN.Utils.existsObject(CNN.ToggleOutbrain) &&
                CNN.Utils.existsObject(CNN.ToggleOutbrain[CNN.contentModel.sectionName])) {
                jQuery('div.m-outbrain[data-widget-id=' + CNN.ToggleOutbrain[CNN.contentModel.sectionName].zone.outBrainId + ']').hide();
            }
        });
    }

    _disableAdditionalWidgets();
    _loadOutbrain();
    _hideAdOutbrain();

    /* To render the outbrain widget below 300x250 ad */
    if (CNN.Features.enableZoneOutbrain) {

        jQuery(document).on('GPTRenderComplete ProgrammaticResizeComplete', function () {

            fastdom.measure(function gatherOutbrainInfo() {
                var $adDiv,
                    $adFrame,
                    adBody,
                    adHtml,
                    pos,
                    adHeight = 0,
                    hideOutbrainId;
                if (CNN.contentModel.pageType === 'section' &&
                    CNN.Utils.existsObject(CNN.ToggleOutbrain) &&
                    CNN.Utils.existsObject(CNN.ToggleOutbrain[CNN.contentModel.sectionName]) &&
                    CNN.Utils.existsObject(CNN.ToggleOutbrain[CNN.contentModel.sectionName].zone)) {
                    $adDiv = jQuery('.ad-' + CNN.ToggleOutbrain[CNN.contentModel.sectionName].zone.adId);
                    $adFrame = $adDiv.find('iframe')[0];
                    adBody = ($adFrame && $adFrame.contentDocument.body) || {};
                    adHtml = jQuery(adBody).html() || '';
                    pos = $adDiv.parent().attr('data-ad-position');
                    hideOutbrainId = CNN.ToggleOutbrain[CNN.contentModel.sectionName].zone.outBrainId;
                    if (adHtml.length > 0) {
                        adHeight = $adFrame.height;
                    }

                    fastdom.mutate(function updateOutbrain() {
                        jQuery('div.m-outbrain[data-widget-id=' + hideOutbrainId + ']').hide();
                        if (pos === 'desktop' && adHeight === '250') {
                            jQuery('div.m-outbrain[data-widget-id=' + hideOutbrainId + ']').show();
                        }
                    });
                }
            });
        });
    }
};

if (CNN.Features.enableOutbrain && typeof CNN.contentModel === 'object' && !CNN.contentModel.lazyLoad) {
    jQuery(document).onZonesAndDomReady(function () {
        'use strict';
        CNN.Outbrain.init();
    });
}


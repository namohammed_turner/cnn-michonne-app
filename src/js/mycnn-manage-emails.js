/* global CNN */

var MSIB = window.MSIB || {};

/* Define the functions needed for the tab in here */
(function ($) {
    'use strict';

    var emails = [],
        primaryEmail = '';

    CNN.mycnn.emails = {

        show: function () {
            if (CNN.mycnn !== undefined) {

                if (emails.length === 0) {
                    CNN.mycnn.emails.getEmails();
                } else {
                    CNN.mycnn.emails.displayEmails();
                }
            }
        },
        displayEmails: function () {
            var output = {emails: []}, index;
            for (index = 0; index < emails.length; index++) {
                if (emails[index].email !== primaryEmail) {
                    output.emails.push(emails[index]);
                }

            }
            output.primaryEmail = primaryEmail;
            window.dust.render('views/cards/tool/mycnn-manage-emails', output, function (err, out) {
                if (err) {
                    CNN.mycnn.updateBody(err);
                } else {
                    CNN.mycnn.updateBody(out);
                    CNN.mycnn.emails.registerHandlers();
                }
            });
        },
        getEmails: function () {
            var
                param = {
                    tid: MSIB.util.getTid(),
                    authid: MSIB.util.getAuthid(),
                    authpass: MSIB.util.getAuthpass()
                };

            MSIB.msapi.getUserInfo(param, CNN.mycnn.emails.handleGetUserInfoCallback);
        },
        deleteEmail: function (emailid) {
            var
                param = {
                    tid: MSIB.util.getTid(),
                    authid: MSIB.util.getAuthid(),
                    authpass: MSIB.util.getAuthpass(),
                    email: emailid
                };

            MSIB.msapi.email.remove(param, CNN.mycnn.emails.handleDeleteEmailCallback);
        },
        registerHandlers: function () {
            $('.msib-email-link').click(function () {
                var emailid = $(this).data('emailid');
                CNN.mycnn.emails.deleteEmail(emailid);
            });
            $(document.getElementById('msib-profile')).click(function () {
                CNN.mycnn.showTab('account');
            });
        },

        /**
         * @param {boolean} result MSIB object result
         */
        handleGetUserInfoCallback: function (result) {

            if (result !== null && result !== 'undefined' && result.responseJSON.emails) {
                emails = result.responseJSON.emails;
                primaryEmail = result.responseJSON.preferredEmail;
                CNN.mycnn.emails.displayEmails();
            }
        },

        /**
         * @param {boolean} result MSIB object result
         */
        handleDeleteEmailCallback: function (result) {

            if (result && result.responseJSON.success) {
                CNN.mycnn.emails.getEmails();
            }
        }
    };

}(jQuery));

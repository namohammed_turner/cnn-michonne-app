
/**
 * CSIManager
 * version:$Id: csiManager.js,v 1.1 2010/10/20 20:26:09 mhynson Exp $
 *
 * @param {object} caller - The calling object.
 */
function CSIManager(caller) {
    'use strict';

    var csiMgr = this,
        localUserAgent = navigator.userAgent.toLowerCase(),
        csiManagerOnLoad;

    if (caller && CSIManager.getInstance && (caller !== CSIManager.getInstance)) {
        if ((navigator.userAgent.indexOf('Safari') === -1) && (navigator.userAgent.indexOf('Opera') === -1)) {
            throw new Error('There is no public constructor for CSIManager.');
        }
    }
    csiMgr.CSIObjects = [];
    csiMgr.delayedCSIList = [];
    csiMgr.domIDConfig = [];
    csiMgr.domOnLoad = [];
    csiMgr.domOnBeforeLoad = [];
    csiMgr.useDelayedCSI = false;
    csiMgr.numberofRequests = 0;
    csiMgr.iframeOffset = 0;
    csiMgr.queueAllCalls = false;
    csiMgr.queuedCallArray = [];
    csiMgr.eventTypes = []; /* indexed array and lookup for function based on array type */
    csiMgr.currentEventType = '';
    csiMgr.eventTypeFunctions = [];
    csiMgr.watchEventTypeNodes = [];

    /* Refresh Manager parameters */
    csiMgr.isPolling = false;
    csiMgr.pollingInterval = 10000;
    csiMgr.minimumInterval = 9999;
    csiMgr.domRefreshHook = false;
    csiMgr.pollingUrl = false;

    if ((localUserAgent.indexOf('msie') > -1) && (navigator.platform.indexOf('Mac') > -1)) {
        csiMgr.useDelayedCSI = true;
    }

    csiMgr.jQuerySupport = (typeof jQuery === 'undefined') ? false : true;
    csiMgr.noFramework = (!csiMgr.jQuerySupport) ? true : false;

    csiManagerOnLoad = function () {
        csiMgr.initialize();
    };

    jQuery(document).onZonesAndDomReady(csiManagerOnLoad);
}

CSIManager.__instance__ = null;  /* define the static property */

CSIManager.getInstance = function () {
    'use strict';

    if (this.__instance__ === null) {
        this.__instance__ = new CSIManager(CSIManager.getInstance);
    }
    return this.__instance__;
};

CSIManager.getInstance();

function revertToCallObject(callObj, regEventType) {
    'use strict';

    CSIManager.getInstance().callObject(callObj, regEventType);
}


CSIManager.prototype.initialize = function () {
    'use strict';

    var lastDiv;

    if (!this.useDelayedCSI) {
        this.queueAllCalls = true;
        lastDiv = document.createElement('div');
        lastDiv.setAttribute('id', 'csimanagerdiv');
        document.body.appendChild(lastDiv);
        lastDiv = document.createElement('div');
        lastDiv.setAttribute('id', 'csimanagerdivdelayed');
        document.body.appendChild(lastDiv);
        this.useDelayedCSI = true;
        this.queueAllCalls = false;
        this.processAnyQueuedCalls();
        this.queuedCallArray = null;
    }
};

CSIManager.prototype.processAnyQueuedCalls = function () {
    'use strict';

    var qCounter, queuedObj;

    if (this.queuedCallArray && this.queuedCallArray !== null) {
        for (qCounter = 0; qCounter < this.queuedCallArray.length; qCounter++) {
            queuedObj = this.queuedCallArray[qCounter];
            this.queuedCallArray[qCounter] = '';
            if (queuedObj) {
                this.call(queuedObj);
            }
        }
    }
};

CSIManager.prototype.addOnLoadById = function (id, func) {
    'use strict';

    var arr = this.domOnLoad[id];
    if (!arr) {
        arr = [];
    }
    arr.push(func);
    this.domOnLoad[id] = arr;
};

CSIManager.prototype.addOnBeforeLoadById = function (id, func) {
    'use strict';

    var arr = this.domOnBeforeLoad[id];
    if (!arr) {
        arr = [];
    }
    arr.push(func);
    this.domOnBeforeLoad[id] = arr;
};

CSIManager.prototype.setConfigForId = function (id, obj) {
    'use strict';

    this.domIDConfig[id] = obj;
};

CSIManager.prototype.getConfigForId = function (id) {
    'use strict';

    var retObj = this.domIDConfig[id];
    if (!retObj) {
        retObj = {};
    }
    return retObj;
};

CSIManager.prototype.registerEventType = function (evType, evFunction) {
    'use strict';

    if (evType && evFunction) {
        this.eventTypes[evType] = evFunction;
    }
};

CSIManager.prototype.callObject = function (callObj, eventType) {
    'use strict';

    var resultObj,
        url,
        args,
        domId,
        funcObj,
        breakCache,
        overrideID,
        forcediframe,
        queuedObj,
        internalDomId,
        domList,
        functionList,
        newCSI,
        today,
        currTime,
        iframeArgs,
        csiMgr,
        realIframeUrl,
        iframeObj,
        containerDiv;

    this.currentEventType = eventType;

    if (callObj) {
        resultObj = callObj;
        url = resultObj.url;
        args = resultObj.args;
        domId = resultObj.domId;
        funcObj = resultObj.funcObj;
        breakCache = resultObj.breakCache;
        overrideID = resultObj.overrideID;
        forcediframe = resultObj.isIframeForced;

        if (this.queueAllCalls) {
            queuedObj = {};
            queuedObj.url = url;
            queuedObj.args = args;
            queuedObj.domId = domId;
            queuedObj.funcObj = funcObj;
            queuedObj.breakCache = breakCache;
            queuedObj.overrideID = overrideID;
            this.queuedCallArray.push(queuedObj);
        } else {
            /* Let's look to see the documentState is complete but the useDelayedCSI flag hasn't been set yet. If so then initialize the manager */
            if ((!this.useDelayedCSI) && (document && document.readyState && (document.readyState === 'complete'))) {
                this.initialize();
            }
            if (forcediframe === undefined || forcediframe === false) {
                forcediframe = false;
                if (url.indexOf('http') === 0) {
                    forcediframe = true;
                    if (url.indexOf(window.location.hostname) > -1) {
                        forcediframe = false;
                    }
                }
            }

            this.numberofRequests++;
            internalDomId = 'csi' + (this.iframeOffset + this.numberofRequests);
            if (overrideID) {
                internalDomId = overrideID;
            }
            domList = [];
            functionList = [];
            if (url.indexOf(document.domain) === -1 && url.indexOf('http') > -1) {
                return false;
            }
            if (domId.join) {
                domList = domId;
            } else {
                domList.push(domId);
            }

            if (funcObj) {
                if (funcObj.join) {
                    functionList = funcObj;
                } else {
                    functionList.push(funcObj);
                }
            }
            this.CSIObjects[internalDomId] = {
                functionList: functionList,
                dom: domList,
                url: url,
                args: args,
                csiRequestNum: this.numberofRequests,
                disableCache: breakCache
            };

            newCSI = {};
            newCSI.src = url;
            newCSI.id = internalDomId;
            newCSI.domId = domList;
            newCSI.args = args;
            newCSI.breakCache = breakCache;
            newCSI.csiRequestNum = this.numberofRequests;
            this.delayedCSIList[this.delayedCSIList.length] = newCSI;

            today = new Date();
            currTime = today.getTime() % 60;
            iframeArgs = breakCache ? 'time=' + currTime : '';
            if (args !== '') {
                iframeArgs += (iframeArgs !== '' ? '&' : '') + args;
            }

            if (forcediframe === false) {
                try {
                    if (funcObj) {
                        try {
                            csiMgr = this;
                            jQuery.ajax(url, {
                                method: 'get',
                                parameters: iframeArgs + (iframeArgs !== '' ? '&' : '') + 'csiID=' + internalDomId,
                                onComplete: function csiOnComplete(response) {
                                    var startStr, startPos, myCode, obj;
                                    /* Response must be either text/xml or text/plain.
                                       Using text/plain, and doing string manipulation to
                                       turn into JSON. */
                                    if (response && response.responseText && response.responseText.indexOf('<textarea id="jsCode">') !== -1) {
                                        startStr = '<textarea id="jsCode">';
                                        startPos = response.responseText.indexOf(startStr) + startStr.length;
                                        myCode = response.responseText.slice(startPos, response.responseText.indexOf('</textarea>'));
                                        obj = jQuery.parseJSON(myCode);
                                        csiMgr.callBackJS(obj, internalDomId);
                                    }
                                    if (response && (!response.responseText)) {
                                        callObj.isIframeForced = true;
                                        CSIManager.getInstance().callObject(callObj, eventType);
                                    }
                                },
                                onException: function csiOnException(x, o) {
                                    throw new Error('Exception for url [' + url + ']: ' + o.messageText);
                                },
                                onFailure: function csiOnFailure() {
                                    throw new Error('Failure');
                                }
                            });
                        } catch (err) {
                            console.log('CatchFailure for url [' + url + ']: ' + err.message);
                        }
                    } else {
                        throw new Error('NoCallBackFunction');
                    }
                } catch (err) {
                    forcediframe = true;
                }
            }
            if (forcediframe) {
                if (this.useDelayedCSI) {
                    realIframeUrl = (url.indexOf('?') < 0) ? url + '?' + iframeArgs + (iframeArgs !== '' ? '&' : '') + 'csiID=' + internalDomId : url + '&csiID=' + internalDomId;
                    iframeObj = document.createElement('iframe');
                    iframeObj.setAttribute('src', realIframeUrl);
                    iframeObj.setAttribute('id', 'csiDataIframe' + internalDomId);
                    iframeObj.setAttribute('name', 'csiDataIframe' + internalDomId);
                    iframeObj.setAttribute('width', '10');
                    iframeObj.setAttribute('height', '10');
                    iframeObj.setAttribute('style', 'visibility:hidden;position:absolute;top:0px;left:-100px;');
                    iframeObj.style.top = '0px';
                    iframeObj.style.left = '-100px';
                    iframeObj.style.position = 'absolute';
                    containerDiv = document.createElement('div');
                    containerDiv.setAttribute('id', 'csiIframeObjs' + internalDomId);
                    containerDiv.appendChild(iframeObj);
                    if (document.getElementById('csimanagerdiv')) {
                        document.getElementById('csimanagerdiv').appendChild(containerDiv);
                        /* This is a hack to work around IE not wanting to set iframe's src reliably after the dom has been loaded */
                        if (navigator.userAgent.indexOf('MSIE') !== -1) {
                            window.setTimeout('var tmpIframObj=document.getElementById("csiDataIframe' + internalDomId + '");if(tmpIframObj.readyState==="uninitialized"){tmpIframObj.src=tmpIframObj.getAttribute("src");tmpIframObj.position="absolute";tmpIframObj.style.left="-100px";}', 500);
                        }
                    }
                } else {
                    iframeObj = document.createElement('iframe');
                    iframeObj.setAttribute('src', url + '?' + iframeArgs + (iframeArgs !== '' ? '&' : '') + 'csiID=' + internalDomId);
                    iframeObj.setAttribute('id', 'csiDataIframe' + internalDomId);
                    iframeObj.setAttribute('name', 'csiDataIframe' + internalDomId);
                    iframeObj.setAttribute('width', '10');
                    iframeObj.setAttribute('height', '10');
                    iframeObj.setAttribute('style', 'visibility:hidden;position:absolute;top:0px;left:-100px;');
                    iframeObj.style.top = '0px';
                    iframeObj.style.left = '-100px';
                    iframeObj.style.position = 'absolute';
                    containerDiv = document.createElement('div');
                    containerDiv.setAttribute('id', 'csiIframeObjs' + internalDomId);
                    containerDiv.appendChild(iframeObj);
                }
            }
        }
    }
};


CSIManager.prototype.call = function (callObj, regEventType, forcedIframe) {
    'use strict';

    /* making call function backwards compatible to the callObject. */
    var callObjCarrier = {};
    callObjCarrier.url = callObj.url;
    callObjCarrier.args = callObj.args;
    callObjCarrier.domId = callObj.domId;
    callObjCarrier.funcObj = callObj.funcObj;
    callObjCarrier.breakCache = callObj.breakCache;
    callObjCarrier.overrideID = callObj.overrideID;
    callObjCarrier.isIframeForced = forcedIframe;
    revertToCallObject(callObjCarrier, regEventType);
};

CSIManager.prototype.callBackHtml = function (html, id) {
    'use strict';

    var htmlContainerObj = false,
        htmlContentArea = 0,
        previousTopVal;

    if (document.getElementById) {
        htmlContainerObj = document.getElementById(id);
        if (!htmlContainerObj) {
            if (this.CSIObjects[id] && this.CSIObjects[id].dom) {
                id = this.CSIObjects[id].dom;
                htmlContainerObj = document.getElementById(id);
            }
        }
    } else if (document.all) {
        htmlContainerObj = document.all[id];
        if (!htmlContainerObj) {
            if (this.CSIObjects[id] && this.CSIObjects[id].dom) {
                id = this.CSIObjects[id].dom;
                htmlContainerObj = document.all[id];
            }
        }
    }
    if (htmlContainerObj) {
        htmlContainerObj.innerHTML = html;
    }
    /* force a refresh of the content area */
    if (htmlContentArea) {
        previousTopVal = htmlContentArea.style.top || '0px';
        htmlContentArea.style.top = '1px';
        htmlContentArea.style.top = previousTopVal;
    }
};

CSIManager.prototype.callBackJS = function (jsonObj, csiID) {
    'use strict';

    var functionList,
        domList,
        functionLength,
        domListLength,
        lastDomID,
        lastFunctionObj,
        fCounter,
        i,
        funcCall,
        configObj,
        beforeLoadFuncArr,
        onLoadFunctionArr,
        realFunc;

    if (this.CSIObjects[csiID]) {
        functionList = this.CSIObjects[csiID].functionList;
        domList = this.CSIObjects[csiID].dom;
        if (functionList) {
            functionLength = functionList.length;
            domListLength = domList.length;
            if (functionLength !== domListLength) {
                if (domListLength < functionLength) {
                    lastDomID = domList[domListLength - 1];
                    for (i = domListLength; i < functionLength; i++) {
                        domList.push(lastDomID);
                    }
                    domListLength = domList.length;
                } else {
                    lastFunctionObj = functionList[functionLength - 1];
                    for (i = functionLength; i < domListLength; i++) {
                        functionList.push(lastFunctionObj);
                    }
                    functionLength = functionList.length;
                }
            }

            for (fCounter = 0; fCounter < functionList.length; fCounter++) {
                funcCall = functionList[fCounter];
                if (funcCall) {
                    configObj = this.getConfigForId(domList[fCounter]);
                    beforeLoadFuncArr = this.domOnBeforeLoad[domList[fCounter]];
                    if (beforeLoadFuncArr) {
                        for (i = 0; i < beforeLoadFuncArr.length; i++) {
                            realFunc = beforeLoadFuncArr[i];
                            realFunc(jsonObj, domList[fCounter], configObj);
                        }
                    }
                    this.callBackHtml(funcCall(jsonObj, domList[fCounter], configObj, this.currentEventType), domList[fCounter]);
                    onLoadFunctionArr = this.domOnLoad[domList[fCounter]];
                    if (onLoadFunctionArr) {
                        for (i = 0; i < onLoadFunctionArr.length; i++) {
                            realFunc = onLoadFunctionArr[i];
                            realFunc(jsonObj, domList[fCounter], configObj);
                        }
                    }
                }
            }
            this.CSIObjects[csiID] = '';
        }
    }
};

CSIManager.prototype.delayedProcessing = function () {
    'use strict';

    var iframeOwner,
        iframeHtmlSrc,
        incCounter,
        src,
        id,
        today,
        breakCache,
        currTime,
        args;

    if (document.body && document.body.innerHTML && this.useDelayedCSI) {
        iframeOwner = document.getElementById('csimanagerdivdelayed') || document.all.csimanagerdivdelayed;
        iframeHtmlSrc = '';

        for (incCounter = 0; incCounter < this.delayedCSIList.length; incCounter++) {
            src = this.delayedCSIList[incCounter].src;
            id = this.delayedCSIList[incCounter].id;
            today = new Date();
            breakCache = this.delayedCSIList[incCounter].breakCache;
            currTime = today.getTime() % 60;
            args = breakCache ? '&time=' + currTime : '';
            if (this.delayedCSIList[incCounter].args !== '') {
                args = args + '&' + this.delayedCSIList[incCounter].args;
            }

            iframeHtmlSrc += '<iframe src="' + src + '?csiID=' + id + args + '" name="csiDataIframe' + id + '" id="csiDataIframe' + id + '" width="10" height="10" style="visibility:hidden;position:absolute;top:0px;left:-100px;"></iframe>';
        }
        if (iframeOwner) {
            iframeOwner.innerHTML = iframeHtmlSrc;
        }
    }
};

CSIManager.prototype.handleClientData = function (cliWinbj, cliDoc) {
    'use strict';

    var targetWindow = top,
        docId = '',
        paramStr,
        keyValPairs,
        counter,
        keyVal,
        wId = '',
        rawHtml,
        rawJS;

    try {
        if (parent.CSIManager) {
            targetWindow = parent;
        }
    } catch (err) {} /* cancel the error */

    paramStr = cliWinbj.location.hash;
    if (!paramStr) {
        paramStr = cliWinbj.location.search.substring(1);
    }

    keyValPairs = paramStr.split('&');
    for (counter = 0; counter < keyValPairs.length; counter++) {
        keyVal = keyValPairs[counter].split('=');
        if (keyVal[0] === 'csiID') {
            counter = keyValPairs.length + 1;
        }
    }

    if (cliWinbj.name && (cliWinbj.name.indexOf('csiDataIframe') === 0)) {
        wId = cliWinbj.name.substring(13);
    }
    if (wId === '' && cliWinbj.frameElement && cliWinbj.frameElement.id  && (cliWinbj.frameElement.id.indexOf('csiDataIframe') === 0)) {
        wId = cliWinbj.frameElement.id.substring(13);
    }
    docId = wId;
    if (cliDoc.mainForm.htmlArea && cliDoc.mainForm.htmlArea.value) {
        rawHtml = cliDoc.mainForm.htmlArea.value;
        if (rawHtml) {
            targetWindow.CSIManager.getInstance().callBackHtml(rawHtml, docId);
        }
    } else if (cliDoc.mainForm.jsCode.value) {
        rawJS = cliDoc.mainForm.jsCode.value;
        if (rawJS) {
            targetWindow.CSIManager.getInstance().callBackJS(jQuery.parseJSON(rawJS), docId);
        }
    }
};


CSIManager.prototype.registerEventTypeNode = function (eventId, eventType) {
    'use strict';

    if (this.eventTypes[eventType]) {
        this.watchEventTypeNodes[eventId] = eventType;
    }

    if (!this.isPolling) {
        this.startPoll();
    }
};

/* REFRESH MANAGER PROTOTYPES */

CSIManager.prototype.setPollingUrl = function (url, pollInterval) {
    'use strict';

    this.pollingUrl = url;
    if (pollInterval && pollInterval > this.minimumInterval) {
        this.pollingInterval = pollInterval;
    }
};

CSIManager.prototype.startPoll = function () {
    'use strict';

    if (!this.isPolling && (this.pollingUrl)) {
        this.pollingId = window.setTimeout(function csiDoPoll() { CSIManager.getInstance().poll(); }, this.pollingInterval);
        this.isPolling = true;
    }
};

CSIManager.prototype.poll = function () {
    'use strict';

    var iframeObj;

    /* we'll do this easy for now and just get an iframe, BUT we should add in the activex mshtml behavior here as well as some "ajax" buzzword magic. */
    if (!this.domRefreshHook) {
        iframeObj = document.createElement('iframe');
        this.domRefreshHook = document.body.appendChild(iframeObj);
    }
    this.domRefreshHook.setAttribute('src', this.pollingUrl);
};

CSIManager.prototype.handleData = function (obj) {
    'use strict';

    var i, id, eventTypeName, eventFunction;

    this.isPolling = false;
    if (obj.intervalTime) {
        if (obj.intervalTime > this.minimumInterval) {
            this.pollingInterval = obj.intervalTime;
        }
    }

    if (obj.entries) {
        for (i = 0; i < obj.entries.length; i++) {
            id = obj.entries[i];
            eventTypeName = this.watchEventTypeNodes[id];
            if (eventTypeName) {
                eventFunction = this.eventTypes[eventTypeName];
                eventFunction(eventTypeName, id);
            }
        }
    }
    this.startPoll();
};

CSIManager.prototype.reset = function () {
    'use strict';

    this.eventTypes = []; /* indexed array and lookup for function based on array type */
    this.currentEventType = '';
    this.eventTypeFunctions = [];
    this.watchEventTypeNodes = [];
};
/* END REFRESH MANAGER PROTOTYPES */

/* global CNN */

/**
 * CNN AudioPlayer
 *
 * Embeds the audio player on a page using jPlayer which is included as a bower dependency
 *
 * @see http://www.jplayer.org/latest/developer-guide/
 */
(function ($, window, document, undefined) {
    'use strict';

    /**
     * @property {object} scriptTag
     * Properties for creating the script tag URL.
     *
     * @constant {string} SCRIPT_ID
     * ID required by the Audio Player API.
     */
    var config = {
            scriptTag: {
                src: CNN.Host.assetPath + '/js/vendor/jquery.jplayer.min.js'
            }
        },
        SCRIPT_ID = 'jquery-jplayer';

    if (typeof window.CNN.AudioPlayer === 'function') { return; }

    /**
     * Checks whether something is a callback function or not
     *
     * @param {function} func - The function in question
     * @returns {boolean} Whether the param is a callback function
     *
     * @private
     */
    function _isFunction(func) {
        return func !== undefined && typeof func === 'function';
    }

    /**
     * Add script tag to the page of the audio player
     *
     * @param {function} callback - Optional callback function after load
     * @private
     */
    function _injectScript(callback) {
        $(document).onZonesAndDomReady(function audioPlayerDocumentReady() {
            var tag = document.createElement('script'),
                firstScriptTag = document.getElementsByTagName('script')[0];

            tag.src = config.scriptTag.src;
            tag.id = SCRIPT_ID;

            // only care about this if we have a callback function
            if (_isFunction(callback)) {
                tag.onload = tag.onreadystatechange = function () {
                    if (!this.readyState || this.readyState === 'loaded' || this.readyState === 'complete') {
                        callback();
                        tag.onload = tag.onreadystatechange = null;
                    }
                };
            }

            firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
        });
    }

    /**
     * An event handler to track audio plays.
     *
     * An audio play occurs when audio is started from the beginning of the file
     *
     * @param {object} event           - Required: A jPlayer event object
     * @param {object} data            - Required: Additional data to track
     * @param {string} data.auth_state - Required: requires authentication or does not require authentication
     * @param {string} data.title      - Required: The title of the audio file
     *
     * @requires window.trackMetrics
     * @fires window.trackMetrics()
     *
     * @see http://jplayer.org/latest/developer-guide/#jPlayer-event-object
     * @private
     */
    function _trackAudioPlay(event, data) {
        /* check if the play event started at 0 seconds */
        if (event.jPlayer.status.currentTime === 0) {

            if (typeof CNN.Features !== 'object' || CNN.Features.enableOmniture !== false) {
                try {
                    window.trackMetrics({
                        type: 'cnnaudio-start',
                        data: {
                            adobe_hash_id: data.adobe_hash_id,
                            auth_state: data.auth_state,
                            id: data.id,
                            title: data.title,
                            mvpd: data.mvpd
                        }
                    });
                } catch (e) {
                    /* Ignore if unable to track */
                }
            }
        }
    }

    /**
     * Main function that initializes a new jPlayer audio player
     *
     * @param {string} playerId - Required: the div ID of the player
     *
     * @see http://www.jplayer.org/latest/developer-guide/#jPlayer-constructor
     * @private
     */
    function _initSingle(playerId) {
        var $player = $(document.getElementById(playerId));

        $player.jPlayer({
            ready: function () {
                $player.jPlayer('setMedia', {
                    mp3: $player.data('file')
                }).bind($.jPlayer.event.play, function audioPlayerEventHandler(event) {
                    _trackAudioPlay(event, {
                        adobe_hash_id: '', // optional
                        auth_state: 'does not require authentication',
                        id: '', // optional
                        title: $player.data('title'),
                        mvpd: '' // optional
                    });
                });
            },
            supplied: 'mp3',
            swfPath: '/assets/',
            volume: 1,
            wmode: 'window',
            solution: 'html, flash',
            errorAlerts: false,
            warningAlerts: false
        });
    }

    /**
     * Inititialization function that adds a script to the page if necessary
     *
     * @param {function} callback - Optional callback function after script is loaded
     *
     * @fires _injectScript()
     * @public
     */
    CNN.AudioPlayer = function (callback) {
        // If there's no AudioPlayer script on the page, add it
        if (document.getElementById(SCRIPT_ID) === null) {
            _injectScript(callback);
        } else if (_isFunction(callback)) {
            // call directly if script is already loaded
            callback();
        }
    };

    /**
     * Initializes a single audio player
     *
     * @param {string} playerId - Required: the div ID of the player
     *
     * @fires CNN.AudioPlayer()
     * @public
     */
    CNN.AudioPlayer.init = function (playerId) {
        // Call this first to ensure the player script is added to the page
        CNN.AudioPlayer(function audioPlayerInit() {
            _initSingle(playerId);
        });
    };
})(jQuery, window, document);


var CNN = window.CNN || {};

(function ($, window, document) {
    'use strict';

    var $appia = $('.js-appia'),
        cfg = CNN.AdsConfig.appia,
        utils = CNN.Utils;

    /**
     * Verifies the Appia config
     * @private
     * @return {boolean} true if the config is valid
     */
    function _isValidConfig() {
        return utils.existsObject(cfg) &&
            typeof cfg.id === 'string' && typeof cfg.script === 'string';
    }

    /**
     * Checks that the user is on an iOS or Android device
     * @private
     * @return {boolean} true if we've detected a mobile device
     */
    function _isMobile() {
        return $('html').is('.android, .ios');
    }

    /**
     * Initialize Appia
     * @private
     */
    function _init() {
        /* only init for mobile */
        if (_isMobile()) {
            /* set the div's ID */
            $appia.attr('id', cfg.id);
            /* load the script */
            $.getScript(cfg.script);
        } else {
            /* delete element for other devices */
            $appia.remove();
        }
    }

    /* Initialize Appia */
    $(document).onZonesAndDomReady(function () {
        if (CNN.Features.enableAppia && _isValidConfig()) {
            _init();
        }
    });
}(jQuery, window, document));

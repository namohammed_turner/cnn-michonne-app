/* global CNN */

CNN.Sponsor = {
    path: location.pathname,
    src: CNN.Host.sponsorContent
};

CNN.Sponsor.src += CNN.Sponsor.path;
if (CNN.Sponsor.path.lastIndexOf('/') === (CNN.Sponsor.path.length - 1)) {
    CNN.Sponsor.src += 'index.html';
}

/* Set the source of the iframe */
jQuery('.sponsor-content').attr('src', CNN.Sponsor.src);


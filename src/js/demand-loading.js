/* global CNN, picturefill, OBR, PostRelease, jQuery, document, window */

/* eslint-disable vars-on-top */
CNN.Features = CNN.Features || {};
CNN.DemandLoadConfig = CNN.DemandLoadConfig || {};
CNN.contentModel = CNN.contentModel || {};

var $page = jQuery('.pg-wrapper'),
    isIZLEnabled = (CNN.contentModel.enableIntelligentLoad ? true : false),
    enableContainerInjection = false,
    enableOutbrain = (CNN.Features.enableOutbrain ? true : false),
    loadAllZonesLazy = (CNN.contentModel.loadAllZonesLazy ? true : false),
    renderedZoneIds = [],
    renderZoneIds = [],
    windowWidth,
    visibleZone = '',
    zoneStorageName = 'zoneView',
    zoneStorageView,
    zonesLoadedBuffer = 2,
    zonesLoading = 0,
    zonesToPreload = 0,
    zonesToLoad = 0,
    columnWidth = 80,
    isPictureFillEnabled = (CNN.contentModel.enablePictureFill ? true : false);
/* eslint-enable vars-on-top */

CNN.Window = CNN.Window || {};
CNN.Window.width = CNN.Window.width || '';
CNN.Columns = CNN.Columns || {
    4: (columnWidth * 4),
    5: (columnWidth * 5),
    6: (columnWidth * 6),
    7: (columnWidth * 7),
    8: (columnWidth * 8),
    9: (columnWidth * 9),
    10: (columnWidth * 10),
    11: (columnWidth * 11),
    12: (columnWidth * 12)
};

CNN.Zones = CNN.Zones || {};
CNN.Zones.runningZoneIds = [];
CNN.Zones.loadedZoneIds = [];

enableContainerInjection = (CNN.Features && CNN.Features.enableContainerInjection) || false;

CNN.processedContainers = [];
CNN.serverSentZones = (loadAllZonesLazy === true) ? 0 : 1;
CNN.Containers = {};

/* Clicktale marker variable (JIRA: CNNWE-221) */
CNN.iZLoading = (CNN.iZLoading ? true : false);

/**
 * Calcuates the zone content window width
 */
function setZoneWindowWidth() {
    'use strict';

    windowWidth = CNN.CurrentSize.getClientWidth();
    /*
        As a guide, window widths translate in the following ways
        when determining breakpoints for zone injection:
        "mobile" viewport breakpoint: 480
        "tablet" viewport breakpoint: 640
        "desktop" viewport breakpoint: 800
    */
    CNN.Window.width = '0';
    CNN.Columns.current = '12';
    if (windowWidth < 400) { /* 4 columns */
        CNN.Columns.current = '4';
    } else if (windowWidth >= 400 && windowWidth < 480) { /* 5 columns */
        CNN.Columns.current = '5';
    } else if (windowWidth >= 480 && windowWidth < 560) { /* 6 columns */
        CNN.Columns.current = '6';
    } else if (windowWidth >= 560 && windowWidth < 640) { /* 7 columns */
        CNN.Window.width = '640';
        CNN.Columns.current = '7';
    } else if (windowWidth >= 640 && windowWidth < 720) { /* 8 columns */
        CNN.Window.width = '640';
        CNN.Columns.current = '8';
    } else if (windowWidth >= 720 && windowWidth < 800) { /* 9 columns */
        CNN.Window.width = '640';
        CNN.Columns.current = '9';
    } else if (windowWidth >= 800 && windowWidth < 880) { /* 10 columns */
        CNN.Window.width = '800';
        CNN.Columns.current = '10';
    } else if (windowWidth >= 880 && windowWidth < 960) { /* 11 columns */
        CNN.Window.width = '800';
        CNN.Columns.current = '11';
    } else if (windowWidth >= 960) { /* 12 or more columns */
        CNN.Window.width = '800';
        CNN.Columns.current = '12';
    }
}

/**
 * Replace subs, if any, in HTML
 * @param {string} html - HTML from ajax call to append zones
 * @param {object} subs - substitution object, if any
 * @returns {string} - New HTML with any substitutions made
 */
function doSubstitutions(html, subs) {
    'use strict';

    var i,
        key,
        keys,
        newHtml = html;

    if (typeof subs === 'object' && subs !== null) {
        keys = Object.keys(subs);
        for (i = 0; i < keys.length; i++) {
            key = keys[i];
            if (CNN.contentModel.hasOwnProperty(key) === true) {
                newHtml = newHtml.replace(subs[key], CNN.contentModel[key]);
            }
        }
    }
    return newHtml;
}

/**
 * This is to append 2+ zones into the page with response after the Ajax call.
 * @param {string} data html response from ajax call to append zones
 * @param {object} options - params used when rendering a zone
 */
function zoneLoadHandler(data, options) {
    'use strict';

    var $data,
        $zone = options.zone,
        append = options.append,
        idx = options.idx,
        id = options.id,
        subs = options.subs,
        content = CNN.Zones;

    evalZoneLoadingState('end');

    /* for IZL mode use html node of JSON for html */
    if (isIZLEnabled === true) {
        if (typeof data.html === 'string' && data.html.length !== 0) {
            $data = jQuery(doSubstitutions(data.html, subs));
        } else {
            /* sneaky empty zones */
            $data = jQuery();
        }
    } else if (typeof data === 'string' && data.length !== 0) {
        $data = jQuery(doSubstitutions(data, subs));
    } else {
        $data = jQuery();
    }

    if (append === true) {
        $zone.after($data.addClass('zn-loaded zn-ondemand zn--idx-' + idx));
    } else {
        /* Need to change the classname once the empty zone is loaded with content */
        $data.removeClass('zn-empty zn--idx-').addClass('zn-loaded zn-ondemand zn--idx-' + idx);
        /* Note to developer: empty zones will completely remove existing section tag from DOM */
        $zone.replaceWith($data);
    }

    jQuery(document).trigger('zoneRenderComplete', id);

    /* Remove the zone ID from the list of running zone IDs */
    if (content.runningZoneIds.indexOf(id) !== -1) {
        content.runningZoneIds.splice(content.runningZoneIds.indexOf(id), 1);
    }

    /* Add the zone ID to the list of loaded zone IDs */
    if (content.loadedZoneIds.indexOf(id) === -1) {
        content.loadedZoneIds.push(id);
    }

    /* if no html data was returned this time, try next zone */
    if ($data.length === 0) {
        jQuery(document).trigger('loadNextZone');
    }

    /* Load any containers associated with this zone */
    setZoneWindowWidth();
    CNN.Containers = content.containers ? content.containers : {};
    loadContainers(id);
}

/**
 * This is to append a container the page with response after the Ajax call.
 * @param {string} data html response from ajax call to append zones
 * @param {object} options - params used when rendering a container in a zone
 */
function containerLoadHandler(data, options) {
    'use strict';

    var $data;

    if (typeof data === 'string' && data.length !== 0) {
        $data = jQuery(data);

        options = options || {};

        if (jQuery('#' + $data.attr('id')).length === 0) {
            jQuery('#' + options.zoneId + ' .zn__column--idx-' + options.position).after(data);
        }
        jQuery('#' + $data.attr('id')).addClass('zn__columns-' + CNN.Columns.current);
    }
}

/**
 * Makes an ajax call to get data for a container
 * @param {object} options - params used when rendering a zone
 */
function containerHandler(options) {
    'use strict';

    var url = '/data/ocs/container/' + (options.uri || '') + ':*/views/containers/common/container-manager.html';

    jQuery.ajax({
        url: url,
        dataType: 'html',
        success: function (opts, data) {
            containerLoadHandler(data, opts);
            CNN.DemandLoading.process();
        }.bind(this, options),
        error: function (opts, data) {
            containerLoadHandler(data, opts);
            CNN.DemandLoading.process();
        }.bind(this, options)
    });
}

/**
 * Loads containers from a list of containers
 * @param {string} context - the zone to target when injecting containers
 */
function loadContainers(context) {
    'use strict';

    var i = 0,
        j = 0,
        containers,
        breakpoints,
        position,
        items;

    /* delete the previously loaded breakpoint containers */
    function processContainers(breakpoints) {
        if (breakpoints[CNN.Columns.current]) {
            items = breakpoints[CNN.Columns.current].items || [];
            for (; i < items.length; i++) {
                if (document.getElementsByClassName(items[i].class).length !== 0 || items[i].class === '*') {
                    containers = items[i].containers || [];
                    for (; j < containers.length; j++) {
                        containerHandler(containers[j]);
                    }
                }
            }
        }
    }

    function getContainers(breakpoints) {
        if (breakpoints[CNN.Columns.current]) {
            processContainers(breakpoints);
        }
    }

    if (enableContainerInjection === true && typeof CNN.Containers === 'object' && CNN.Containers !== null) {
        if (typeof context === 'string' && CNN.Containers[context]) {
            /* Single context */
            breakpoints = CNN.Containers[context].breakpoints || {};
            getContainers(breakpoints);
        } else {
            /* process entire dom */
            for (position in CNN.Containers) {
                if (CNN.Containers.hasOwnProperty(position)) {
                    breakpoints = CNN.Containers[position] || {};
                    getContainers(breakpoints);
                }
            }
        }
    }
}

/**
 * Sets up loading state if we are waiting on ajax request
 *
 * @param {string} trigger - 'start' or 'end'
 */
function evalZoneLoadingState(trigger) {
    'use strict';

    var zoneLoadDiv = 'js-zone-loading';

    if (trigger === 'end') {
        zonesLoading--;
    } else {  /* 'start' */
        zonesLoading++;
    }

    if (zonesLoading > 0) {
        /* check to see if zone loading div exists or not */
        if (jQuery(document.getElementById(zoneLoadDiv)).length === 0) {
            $page.find('section').last().after('<div id="' + zoneLoadDiv + '"></div>');
        }

        jQuery(document.getElementById(zoneLoadDiv)).html('<div class="zn-state--loading"></div>');

        /* set Clicktale Marker only after initial zones have loaded (only on user scroll-triggered loads) */
        if (isIZLEnabled === true && CNN.iZLoading === false && CNN._zonesRenderedEvent) {
            CNN.iZLoading = true;
        }
    } else {
        if (jQuery(document.getElementById(zoneLoadDiv)).length !== 0) {
            jQuery(document.getElementById(zoneLoadDiv)).html('');
        }

        /* set Clicktale Marker */
        if (isIZLEnabled === true && CNN.iZLoading === true) {
            CNN.iZLoading = false;
        }
    }
}

/**
 * Removes containers from the dom
 */
function cleanContainers() {
    'use strict';

    /* Only do work if previous !== current */
    if (CNN.Columns.previous !== CNN.Columns.current) {
        jQuery('.zn__columns-' + CNN.Columns.previous).remove();
    }
}

/*
 * bind zoneRenderComplete event.
 */
jQuery(document).on('zoneRenderComplete', function zoneRenderedComplete(event, zoneId) {
    /* To call dependent js after zone added to page */
    'use strict';

    var $zoneElement = jQuery(document.getElementById(zoneId));

    if (typeof CNN.Carousel === 'object' && CNN.Carousel !== null && typeof CNN.Carousel.restore === 'function') {
        CNN.Carousel.restore($zoneElement);
    }

    CNN.DemandLoading.addElement(document);

    if (isPictureFillEnabled === true) {
        if (window.picturefill) {
            picturefill();
        }
    } else {
        CNN.ResponsiveImages.queryEqjs(document);
    }

    renderedZoneIds.push(zoneId);

    /*
     * Intelligent Zone Loading work: Trigger event to load ads when initial zone is loaded
     * Requires enabling enableIntelligentLoad in Trinity
     */
    if (isIZLEnabled === false) {
        if (renderedZoneIds.length === (renderZoneIds.length - CNN.serverSentZones)) {
            jQuery(document).triggerZonesRendered();
        }
    } else if ((CNN._zonesRenderedEvent === false) && (zonesToPreload <= renderedZoneIds.length)) {
        jQuery(document).triggerZonesRendered();
    } else if (CNN._zonesRenderedEvent && typeof zoneId === 'string') {
        jQuery(document).trigger('onIZSecondaryLoad', [zoneId]);
    }

    jQuery(document).trigger('onZoneRendered');

    /* Remove hidden class upon the first zone render when loadAllZonesLazy is true */
    jQuery('body').removeClass('pg-hidden');
});

/*
 * bind zoneRenderComplete event.
 */
jQuery(document).on('loadNextZone', function loadNextZone(_event) {
    /* To call dependent js after zone added to page */
    'use strict';

    /* we've encountered an empty zone from OCS so bump up the # of zones to load to compensate */
    zonesToLoad++;

    CNN.DemandLoading.updateZones();
});

/*
 * bind events and call 3rd party items like ads, tool cards etc;
*/
function allZonesAndDomReadyHandler() {
    'use strict';

    CNN.DemandLoading.pageBottom = false;

    $page.find('section.zn-empty').hide();
}

/* tasks to only be fired ONLY once on "start" */
jQuery(document).onZonesAndDomReady(function demandZonesDomReadyHandler(_event) {
    'use strict';

    /* CNN.MultiAds.init is defined in ads-epic.js */
    if (typeof CNN.MultiAds === 'object' && CNN.MultiAds !== null && typeof CNN.MultiAds.init === 'function') {
        CNN.MultiAds.init();
    }

    if (enableOutbrain === true && typeof CNN.Outbrain === 'object' && CNN.Outbrain !== null &&
        typeof CNN.Outbrain.init === 'function' && (CNN.DemandLoading.thirdParty.outbrain !== true)) {

        CNN.Outbrain.init();
        CNN.DemandLoading.thirdParty.outbrain = true;
    }

    allZonesAndDomReadyHandler();
});

/* when in IZL mode, we'll need some additional events to fire to handle zones being loaded on scroll events */
if (isIZLEnabled === true) {
    /* this is triggered after all the preloaded zones have been loaded */
    jQuery(document).onZonesAndDomReady(function iZLZonesDomReadyHandler(_event) {
        'use strict';

        /* trigger nativo for preloaded zones 1 - 3 */
        if (typeof PostRelease !== 'undefined') {
            PostRelease.Start();
        }

        CNN.DemandLoading.updateZoneView();
    });

    /* for each set of zones that load after the initial preloaded zones, perform these tasks on it */
    jQuery(document).on('onIZSecondaryLoad', function iZLSecondaryLoadHandler(event, zoneId) {
        'use strict';

        var context;

        if (typeof zoneId !== 'string' || zoneId.length === 0) {
            return;
        }

        context = '#' + zoneId;

        /* trigger outbrain modules */
        if (enableOutbrain === true && typeof OBR !== 'undefined') {
            OBR.extern.researchWidget();
        }

        /* perform responsive image magic */
        if (isPictureFillEnabled === true) {
            if (window.picturefill) {
                picturefill();
            }
        } else {
            CNN.ResponsiveImages.queryEqjs(document);
        }

        /* trigger new ads only in newly loaded zones */
        CNN.MultiAds.update([context]);

        allZonesAndDomReadyHandler();

        CNN.DemandLoading.updateZoneView();
    });
}

/* There are two states for demand-load elements: loaded and not-loaded. */
(function ($) {
    'use strict';

    var scope = 'img[data-demand-load="not-loaded"]',
        enabled = CNN.DemandLoadConfig.enableForImages || false,
        threshold = CNN.DemandLoadConfig.threshold || 200,
        furthestCutoff = 0;

    setZoneWindowWidth();

    /**
     * Set the data attribute of elements.
     *
     * @param {object} $image - jQuery image selection
     * @param {boolean|string} [defaultValue='not-loaded']  - default data attribute value
     */
    function setDataAttr($image, defaultValue) {
        var dataValue = 'not-loaded';

        if (defaultValue === true) {
            dataValue = 'loaded';
        } else if (typeof defaultValue === 'string') {
            dataValue = defaultValue;
        }
        $image.attr('data-demand-load', dataValue);
    }

    CNN.DemandLoading = {
        zoneImages: {},

        thirdParty: {
            money: {
                currencies: false,
                market: false
            },
            outbrain: false
        }
    };

    /**
     * Loads a zone via jquery ajax
     * @param {string} id the zone id
     * @param {object} options - params used when rendering a loaded zone
     */
    function zoneLoader(id, options) {
        var i,
            key,
            newSubs,
            subs,
            url,
            zoneDataType = (isIZLEnabled === true) ? 'json' : 'html',
            templateFileSuffix = (isIZLEnabled === true) ? '.izl' : '.html';

        if (typeof id === 'string') {
            if (typeof options.path === 'string' && options.path.length !== 0) {
                /* Set the url using the path, since we have it */
                url = '/data/ocs/section/' + options.path + ':' + id + '/views/zones/common/zone-manager' + templateFileSuffix;
            }

            /* Add subs, if any */
            if (options.subs) {
                subs = Object.keys(options.subs);
                if (subs.length > 0) {
                    newSubs = {};
                    for (i = 0; i < subs.length; i++) {
                        key = subs[i];
                        if (typeof options.subs[key] === 'string' && options.subs[key].length !== 0) {
                            url += '/' + key + '=' + options.subs[key];
                            newSubs[key] = RegExp('\\$\\$' + options.subs[key].slice(2, -2) + '\\$\\$', 'g');
                        }
                    }
                    options.subs = newSubs;
                } else {
                    options.subs = null;
                }
            }

            CNN.Zones.runningZoneIds.push(id);

            if (typeof url === 'string') {
                evalZoneLoadingState('start');

                $.ajax({
                    url: url,
                    dataType: zoneDataType,
                    success: function (id, opts, data) {
                        zoneLoadHandler(data, {
                            zone: opts.zone,
                            append: opts.append,
                            subs: opts.subs,
                            idx: opts.zoneIdx,
                            id: id
                        });
                    }.bind(this, id, options),
                    error: function (id, opts, data) {
                        zoneLoadHandler(data.responseText, {
                            zone: opts.zone,
                            append: opts.append,
                            subs: opts.subs,
                            idx: opts.zoneIdx,
                            id: id
                        });
                    }.bind(this, id, options)
                });
            }
        }
    }

    /**
     * Are we done loading zones?
     *
     * @param {number} idx - the index of the zone just loaded
     * @returns {boolean} - true if that was the last zone to load, otherwise false
     */
    function shouldZoneLoaderStop(idx) {
        var loopIdx = (CNN.serverSentZones === 1) ? idx : (idx + 1);

        return (isIZLEnabled === true && ((zonesToLoad === 0) || (loopIdx >= zonesToLoad))) ? true : false;
    }

    /**
     * Adds the proper index file extension, removes the leading slash.
     *
     * @param {string} path - the uri to sanitize
     * @returns {string} - The sanitized path
     */
    function sanitizeIndexPath(path) {
        if (path.lastIndexOf('/') === (path.length - 1)) {
            path += 'index.html';
        } else if (path === '') {
            path = 'index.html';
        } else if (path !== 'index.html' && path.indexOf('/index.html') === -1) {
            path += '/index.html';
        }

        path = path.replace(/\/\/+/g, '/');

        if (path.charAt(0) === '/') {
            path = path.substring(1);
        }
        return path;
    }

    /**
     * Mark zone as currently visible, in case page gets reloaded.
     *
     * @param {object} lastZone - The zone element of the zone to mark visible.
     * @returns {string} - The zone id of the zone marked visible, if any.
     */
    function markVisibleZone(lastZone) {
        var idxClass;

        if (typeof lastZone === 'undefined') {
            return '';
        }

        /*
         * "saves" the zone you're viewing in case we trigger auto refresh
         * If we don't like the location.hash method, we can try adding an event to fire before
         * the refresh timer in page-event.js that appends the value as a hash or query param right before
         * refreshing the page
         */
        try {
            idxClass = lastZone.attr('class').match(/zn--idx-(\d+)/);
            if (idxClass !== null && idxClass.length > 1) {
                CNN.Utils.storeLocalValue(zoneStorageName, idxClass[1], 'session');
            }
        } catch (err) {
            /* The lastZone has no class or zone index */
            return '';
        }

        /* return the id of the zone's that visible to user */
        return lastZone.attr('id') || '';
    }

    /**
     * Monitor if all images have loaded within just a zone.
     *
     * @param {string} zoneId - The zone id to monitor images for.
     * @param {function} [callback] - Function to call when all zone images are loaded.
     */
    CNN.DemandLoading.allZoneImagesLoaded = function (zoneId, callback) {
        CNN.DemandLoading.zoneImages[zoneId] = {
            images: jQuery('#' + zoneId + ' img'),
            count: 0
        };

        CNN.DemandLoading.zoneImages[zoneId].images.load(function () {
            CNN.DemandLoading.zoneImages[zoneId].count++;

            if (CNN.DemandLoading.zoneImages[zoneId].count === CNN.DemandLoading.zoneImages[zoneId].images.length) {
                if (typeof callback === 'function') {
                    callback();
                }
            }
        });
    };

    /**
     * Zone visibility tracking sauce.
     */
    CNN.DemandLoading.updateZoneView = function updateZoneView() {
        var currentPosition = CNN.CurrentSize.getClientScrollTop(),
            lastZone,
            isZoneFound = false,
            viewPosition = currentPosition + jQuery(window).height();

        if (jQuery('section.zn-loaded').length !== 0) {
            jQuery('section.zn-loaded').each(function (index) {
                if (jQuery(this).offset().top > viewPosition) {
                    if (index !== 0) {
                        visibleZone = markVisibleZone(lastZone);
                    } else {
                        visibleZone = markVisibleZone($(this));
                    }

                    isZoneFound = true;

                    return false;
                } else {
                    lastZone = $(this);
                }
            });

            /* user scrolled to last zone too fast (due to throttled scroll event) so let's play catch up */
            if (!isZoneFound) {
                visibleZone = markVisibleZone(lastZone);
            }
        }
    };

    /**
     * To load the zones.
     */
    CNN.DemandLoading.updateZones = function updateZonesHandler() {
        var $zone,
            append = false,
            content = CNN.Zones,
            id,
            ids,
            idx = 0,
            path = (CNN.contentModel.pageType === 'video') ? 'videos/index.html' : sanitizeIndexPath(document.location.pathname),
            zoneIdx = $page.find('section.zn-loaded:not(.nativeSponsor)').length,
            zones = content.zones || {},
            zonesMinWidth = zones.minWidth || {},
            zVal,
            $prevZone,
            $currZone,
            firstZoneId,
            previewId,
            previewZoneIdx = 0,
            uri,
            subs;

        ids = zonesMinWidth[CNN.Window.width] || CNN.contentModel.zoneIds || [];

        /*
            Get the section doc._id from the path when the following conditions exists
            1. the uri starts with /preview
            2. the uri contains section_
        */
        if (path.indexOf('preview/') === 0 && path.indexOf('/section_') !== -1) {
            previewId = path.split('section_')[1].replace('/index.html', '');
            /*
                Replace the zone Ids in the array with the preview version
                example:
                Replace index.html:homepage2-zone-1 with
                section/preview/workspace-adslaton/section_homepage2:homepage2-zone-1
            */
            for (; previewZoneIdx < ids.length; previewZoneIdx++) {
                if (ids[previewZoneIdx].id.indexOf(previewId) !== -1) {
                    ids[previewZoneIdx].uri = 'section/' + path.replace('/index.html', '');
                }
            }
        }

        if (CNN.serverSentZones === 1 && ids.length !== 0) {
            /*
                If CNN.serverSentZones is 1 then we need to force the first
                zones' containers to load.

                The zone id could be either of the following:
                1. path/index.html:zn-zoneId
                2. zn-zoneId

                ids is an array of zone ids
            */
            firstZoneId = ids[0].id;
            CNN.Containers = content.containers ? content.containers[firstZoneId] : {};
            setZoneWindowWidth();
            loadContainers(firstZoneId);
        }

        if (renderZoneIds.length === 0) {
            for (; idx < ids.length; idx++) {
                if (typeof ids[idx].id === 'string' && ids[idx].id.length !== 0) {
                    id = ids[idx].id;
                    renderZoneIds.push(id);
                    $currZone = $('.zn--idx-' + idx);
                    $prevZone = $('.zn--idx-' + (idx - 1));
                    if ($currZone.length === 0 && $prevZone.length === 1) {
                        $prevZone.after('<section class="zn--idx-' + idx + ' zn-empty"></section>');
                    }
                }
            }
        }

        /* load the next zone */
        for (; zoneIdx < ids.length; zoneIdx++) {
            zVal = ids[zoneIdx] || {};
            id = zVal.id;
            if (typeof id !== 'string' || id.length === 0 || content.loadedZoneIds.indexOf(id) !== -1) {
                continue;
            }

            uri = (typeof zVal.uri === 'string') ? zVal.uri : (zones.baseUri || '');
            subs = Array.isArray(zVal.subs) ? zVal.subs : (zones.baseSubs || null);

            if (uri.length !== 0) {
                if (content.loadedZoneIds.indexOf(id) !== -1) {
                    continue;
                }
                append = false;
                $zone = $('.zn--idx-' + zoneIdx + '.zn-empty');
                if ($zone.length === 0) {
                    $zone = $(document.querySelectorAll('.zn-loaded')).last();
                    append = true;
                }
                if ($zone.length === 1) {
                    if (content.runningZoneIds.indexOf(id) !== -1) {
                        break;
                    }
                    zoneLoader(id, {
                        append: append,
                        path: uri,
                        subs: subs,
                        zone: $zone,
                        zoneIdx: zoneIdx
                    });

                    /* we only want to load a set number of zones at a time in scroll-to-load mode */
                    if (shouldZoneLoaderStop(zoneIdx)) {
                        break;
                    }
                }
            } else {
                $zone = $(document.getElementById(id));

                if ($zone.length === 1) {
                    if (content.runningZoneIds.indexOf(id) !== -1) {
                        break;
                    }
                    zoneLoader(id, {
                        append: false,
                        path: path,
                        subs: subs,
                        zone: $zone,
                        zoneIdx: zoneIdx
                    });

                    /* we only want to load a set number of zones at a time in scroll-to-load mode */
                    if (shouldZoneLoaderStop(zoneIdx)) {
                        break;
                    }
                }
            }
        }
    };

    function reOrderZones(modifiers) {
        var baseZones = CNN.Zones.zones,
            minWidthZones = baseZones.minWidth,
            zoneSet,
            widthZones,
            i,
            modifier,
            destination,
            source;

        modifiers = modifiers || [];

        for (i = 0; i < modifiers.length; i++) {
            modifier = modifiers[i];

            for (widthZones in minWidthZones) {
                if (minWidthZones.hasOwnProperty(widthZones)) {
                    zoneSet = minWidthZones[widthZones];

                    source = zoneSet.indexOf(modifier.sourceZone);
                    destination = zoneSet.indexOf(modifier.destinationZone);

                    if (source !== -1 && destination !== -1) {
                        zoneSet.splice(destination, 0, zoneSet.splice(source, 1)[0]);
                    }

                    minWidthZones[widthZones] = zoneSet;
                }
            }

            baseZones.minWidth = minWidthZones;
        }

        CNN.Zones.zones = baseZones;
    }

    if (CNN.contentModel.lazyLoad) {
        if (isIZLEnabled === true) {
            /* Check for izl generated local storage value on page load and set zonesToPreload */
            zoneStorageView = CNN.Utils.getLocalValue(zoneStorageName);

            if (typeof zoneStorageView === 'string') {
                zonesToPreload = zoneStorageView / 1 + 1;

                CNN.Utils.deleteLocalValue(zoneStorageName);
            }

            /* We may need extra zones loaded based on viewport so check */
            if (zonesToPreload <= 2) {
                if (CNN.Window.width !== '0') {
                    zonesToPreload = 3;
                } else {
                    /* For mobile viewport, load only one additional zone */
                    zonesToPreload = 2;
                }
            }

            /* Check zones config to see if we're doing some injection */
            jQuery.grep(CNN.Zones.zones.minWidth[CNN.Window.width], function (zone, index) {
                var isInjected = (zone.id.indexOf('injection-zone') !== -1);

                if (isInjected && (index < zonesToPreload)) {
                    zonesToPreload++;
                }

                return isInjected;
            });

            /* Check to ensure zones to preload is never higher than zones array due to zone injection */
            if (zonesToPreload > CNN.Zones.zones.minWidth[CNN.Window.width].length) {
                zonesToPreload = CNN.Zones.zones.minWidth[CNN.Window.width].length;
            }

            if (CNN.Window.width === '0') {
                /* For mobile don't 'buffer' the zones by 2 */
                zonesLoadedBuffer = 1;
            }

            zonesToPreload = zonesToPreload - CNN.serverSentZones;

            /* set zonesToLoad initially to the amount we preload so that zonesToPreload NEVER changes */
            zonesToLoad = zonesToPreload;

            jQuery(document).ready(function updateIZLZones() {
                /* Check to see if Optimizely wants to reorder zones */
                if (CNN.IZL && CNN.IZL.Optimizely && CNN.IZL.Optimizely.zoneOrder) {
                    reOrderZones(CNN.IZL.Optimizely.zoneOrder);
                }

                CNN.DemandLoading.updateZones();
            });
        } else {
            /* if we're not in IZL scoll-to-load mode then this will load ALL zones */
            CNN.DemandLoading.updateZones();
        }

        /* Attach event when in scroll to load mode to load zones */
        if (isIZLEnabled === true) {
            jQuery(window).throttleEvent('scroll', function loadZonesOnScroll() {
                var currentPosition = CNN.CurrentSize.getClientScrollTop(),
                    curZonePosition,
                    numRenderedZones = jQuery('section.zn-loaded:not(.nativeSponsor)').length - jQuery('section.zn-ondemand:not(.nativeSponsor)').length,
                    realZonesRendered = (CNN.serverSentZones) ? renderZoneIds.slice(0, CNN.serverSentZones).concat(renderedZoneIds) : renderedZoneIds,
                    numZonesRenderable = renderZoneIds.length - numRenderedZones,
                    bufferZones = [],
                    i;

                CNN.DemandLoading.updateZoneView();

                if (currentPosition <= 0 || CNN._zonesRenderedEvent === false) {
                    return;
                }

                curZonePosition = renderedZoneIds.indexOf(visibleZone) + 1;

                /* Check to make sure we haven't rendered all zones so we don't fire off updateZones needlessly */
                if (renderedZoneIds.length < numZonesRenderable) {
                    /* pop off ids of zones that are below current position and make sure empty (undefined) zones are not counted within the buffer */
                    for (i = (curZonePosition + CNN.serverSentZones); i < realZonesRendered.length; i++) {
                        if (typeof realZonesRendered[i] !== 'undefined') {
                            bufferZones.push(realZonesRendered[i]);
                        }
                    }

                    if (bufferZones.length < zonesLoadedBuffer) {
                        zonesToLoad = curZonePosition + zonesLoadedBuffer;

                        /* Check to see if they has fast scrolled to the bottom of the page and if so load all remaining zones */
                        if (zonesToLoad > numZonesRenderable ||
                            (jQuery('footer.l-footer').length > 0 &&
                            (currentPosition + jQuery(window).height() >= jQuery('footer.l-footer').first().offset().top))) {
                            zonesToLoad = numZonesRenderable;
                        }

                        CNN.DemandLoading.updateZones();
                    }
                }
            }, 50);
        }
    }

    CNN.DemandLoading.allLoaded = false;
    CNN.DemandLoading.elements = [];
    CNN.DemandLoading.pageBottom = enabled ? false : true;

    /**
     * Manually add an element to CNN.DemandLoading.elements. This will allow
     * for elements lazily-loaded onto the page to be tracked with data
     * attributes.
     *
     * @param {array|object} elements - Element(s) to add to DemandLoading
     *
     * @returns {array} - Array of modified elements
     */
    CNN.DemandLoading.addElement = function addDemandLoadingElement(elements) {
        var i = 0,
            $elements = $('img', elements);

        if ($elements.length === 0) {
            $elements = $(elements);
        }

        if ($elements.length > 0) {
            CNN.DemandLoading.allLoaded = false;
            setDataAttr($elements);

            for (i = 0; i < $elements.length; i++) {
                CNN.DemandLoading.elements.push($elements[i]);
            }
        }

        return CNN.DemandLoading.process();
    };

    /**
     * Determines if the user is below the previous furthestCutoff point, and
     * calls a function to load new elments if so; those new elements will be
     * returned by the function. Also, this will set pageBottom if the bottom
     * of the document is reached.
     *
     * @returns {array} - Array of modified elements
     */
    CNN.DemandLoading.update = function updateDemandLoadingCutoff() {
        var changed = false,
            cutoff = CNN.CurrentSize.getClientScrollTop() + $(document).height() + threshold;

        if (cutoff > furthestCutoff) {
            furthestCutoff = cutoff;

            if (furthestCutoff >= $(document).height()) {
                CNN.DemandLoading.pageBottom = true;
            }

            changed = CNN.DemandLoading.process();
        }

        return changed;
    };

    /**
     * Determines if any not-loaded elements are above the vertical cutoff,
     * and if so, sets data-demand-load to loaded. Returns those elements,
     * or the 0-length array, if no elements are found, to check against.
     * Also sets allLoaded if no not-loaded images are found.
     *
     * @returns {array} - Array of modified elements
     */
    CNN.DemandLoading.process = function processDemandLoading() {
        var changed = [],
            elements,
            elementsLength,
            i,
            newElements,
            $this;
        /*  widthCutoff;
         *
         * This last variable declaration is commented out, it goes with the
         * commented out code below.  See the description below for why this
         * is commented out.
         */

        if (CNN.DemandLoading.elements.length === 0) {
            CNN.DemandLoading.elements = $(scope);
        }

        elements = CNN.DemandLoading.elements;
        elementsLength = elements.length || 0;

        if (elementsLength === 0) {
            CNN.DemandLoading.allLoaded = true;
        } else {
            newElements = [];

            for (i = 0; i < elementsLength; i++) {
                $this = $(elements[i]);

                /* The demand loading of carousels caused issues in galleries. Here is the
                 * previous code, commented out, for us to tackle again later, once we have
                 * more time.  Follow-up:  Owl 2.x provides this, so we'll use that.
                 *
                 * x2, in order to get the next page of images in a carousel in advance.
                 * widthCutoff = CNN.CurrentSize.getClientWidth() * 2;
                 *
                 * Only check the furthestCutoff if we haven't hit pageBottom.
                 * This is to avoid the case where we hit pageBottom, stop updating furthestCutoff... but
                 * now images have loaded, pushing the page length downwards, and a carousel at the bottom
                 * of the page will now fail the "less than furthestCutoff" test when sliding new images in.
                 * if ((!CNN.DemandLoading.pageBottom && elementOffset.top <= furthestCutoff && elementOffset.left <= widthCutoff) ||
                 *     (CNN.DemandLoading.pageBottom && elementOffset.left <= widthCutoff)) {
                 */
                if ($this.offset() && $this.offset().top <= furthestCutoff) {
                    setDataAttr($this, true);
                    changed.push($this[0]);
                } else {
                    newElements.push($this[0]);
                }
            }

            if (changed.length !== 0) {
                CNN.DemandLoading.elements = newElements;
                CNN.DemandLoading.allLoaded = false;
            }
        }

        return changed;
    };

    /**
     * Resets the elements list and re-processes the page (using above).
     *
     * @returns {array} - Array of modified elements
     */
    CNN.DemandLoading.reProcess = function reProcessDemandLoading() {
        CNN.DemandLoading.elements = $(scope);

        return CNN.DemandLoading.process();
    };

    jQuery(window).throttleEvent('resize', function injectContainers() {
        /* Store the state of the previous column size */
        CNN.Columns.previous = CNN.Columns.current;
        cleanContainers();
        setZoneWindowWidth();
        loadContainers();
    }, 50, this);

})(jQuery);

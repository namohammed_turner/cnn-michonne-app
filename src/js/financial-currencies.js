/* global CNN */

CNN.MarketsConfig = CNN.MarketsConfig || {};
CNN.Money = CNN.Money || {};

CNN.Money.currencies = function () {
    'use strict';

    var $ = jQuery,
        converterUrl = CNN.MarketsConfig.converterService,
        $converterForm;

    /**
     * Makes a proxy request for the text response to the
     * conversion information entered into the form and
     * injects this response into the output form field.
     *
     * @return {function} The event handler
     * @private
     */
    function _conversionHandler() {
        var $conversionOutput = $converterForm.find('.js-conversion-output');

        return function () {
            var $selectLists = $converterForm.find('.js-field-list'),
                amountVal = $converterForm.find('.js-field-item').first().val(),
                fromVal = $selectLists.first().val(),
                toVal = $selectLists.last().val();

            $.ajax({
                /* url = converterUrl + '/convert/base/quote/amount' */
                url: converterUrl + '/1/' + fromVal + '/' + toVal + '/' + amountVal,
                dataType: 'html',
                type: 'get',
                success: function (responseHtml) {
                    $conversionOutput.val(responseHtml.replace('= ', ''));
                }
            });
        };
    }

    /**
     * Swaps the values currently in the 'to' and 'from' currency
     * converter form fields. This function will also trigger a
     * change event so the resulting calculation request is fired.
     *
     * @return {function} The event handler
     * @private
     */
    function _conversionSwapHandler() {
        var $selectLists = $converterForm.find('.js-field-list'),
            $fromList = $selectLists.first(),
            $toList = $selectLists.last();

        return function () {
            var currentFrom = $fromList.val(),
                currentTo = $toList.val();

            $fromList.val(currentTo);
            $toList.val(currentFrom).change();
        };
    }

    /**
     * Setup required event handlers.
     *
     * @param {object} $el - The card jQuery element
     * @private
     */
    function _bindEvents($el) {
        $el.on('blur change', '.js-field-item, .js-field-list', _conversionHandler())
            .on('click', '.js-conversion-flip', _conversionSwapHandler())
            .on('submit', '.js-currency-converter', CNN.Financial.stopSubmit);
    }

    /* If this card is on the page initialize this module. */
    if (typeof CNN.Financial.checkLoaded === 'function') {
        $.when(CNN.Financial.checkLoaded('currencies')).done(function ($el) {
            $converterForm = $el.find('.js-currency-converter');
        }, _bindEvents);
    }

};

if (typeof CNN.contentModel === 'object' && !CNN.contentModel.lazyLoad) {
    jQuery(document).onZonesAndDomReady(function () {
        'use strict';
        /*
        * HTML that this function is used on is pulled in by ajax at ssiPath
        * /.element/ssi/auto/4.0/sect/INTLEDITION/iBUSINESS/wsod_currencies.html
        */
        CNN.Money.currencies();
    });
}

if (CNN.Utils.existsObject(CNN.Money)) {
    if (typeof CNN.Money.currencies === 'function' && (CNN.Money.cardLoaded.currencies === true) &&
        (CNN.DemandLoading.thirdParty.money.currencies !== true)) {
        CNN.Money.currencies();

        CNN.DemandLoading.thirdParty.money.currencies = true;
    }
}

/* global CNN, CSIManager, trackMetrics, sectionList */

(function ($) {
    'use strict';

    var _search = {};

    $(document).onZonesAndDomReady(function documentReadyHandler() {
        var callObj,
            textParam = $.urlParam('text') ? $.trim($.urlParam('text')) :
                ($.urlParam('query') ? $.trim($.urlParam('query')) : ''),
            currentSection = $.urlParam('sections') || '',
            searchWithSubSections = '',
            checkText,
            searchTerm = $.trim(textParam),
            categoryParentIdArray = ['Everything_P', 'STORIES_P', 'VIDEOS_P', 'PHOTOS_P', 'INTERACTIVES_P', 'IREPORT_P'],
            timeParentIdArray = ['Anytime_P', 'PASTDAY_P', 'PASTWEEK_P', 'PASTMONTH_P', 'PASTYEAR_P'],
            $sectionFilter = $('.search-cat-list .facet_list'),
            $filterHolder = $('.filterHolder'),
            filterHolderHeight,
            sectionFilterWidth = $sectionFilter[0].getBoundingClientRect().width,
            sortOn,
            $adSlotContainer = $('.display-facets .cn--adcontainer'),
            $adSlotLock = $adSlotContainer.find('#adcontainer2'),
            $clearText = $('.clearSearch'),
            $footerElement,
            footerPosition,
            adSlotValue,
            $searchBody = jQuery('body'),
            scrollPosition;

        try {
            do {
                checkText = searchTerm;
                searchTerm = decodeURIComponent(checkText);
            } while (searchTerm !== checkText);
        } catch (error) {
            /* Catch malformed data and reload without it to prevent script errors */
            window.location.href = CNN.Search.searchUrl;
            checkText = '';
        }
        searchTerm = checkText.replace(/[\s<>]+/g, '+');

        _search = {
            input: [$(document.getElementById('searchInputTop'))],
            term: searchTerm,
            url: CNN.Search.queryUrl,
            args: {
                page: 1,
                npp: 10,
                start: 1,
                text: textParam,
                type: 'all',
                bucket: true,
                sort: 'relevance'
            },
            filtered: false,
            filteredBy: [],
            firstLoad: true,
            emptyQuery: false
        };

        try {
            _search.query = $.trim(decodeURIComponent(_search.term.replace(/\++/g, ' ')));
        } catch (error) {
            /* Catch malformed data */
            _search.query = '';
        }

        setDefaultSearchSectionOnMobilePageLoad();
        setSelectedSearchSection();
        setDefaultSearchSection();

        function setDefaultSearchSectionOnMobilePageLoad() {
            $('.sortOptions, .sectionOptions').find('li:not(.clicked)').hide();
        }
        function setValueFromSubSectionName(sectionValue, sectionListItem) {
            var s;

            for (s = 0; s < sectionListItem.subSections.length; s++) {
                if ((sectionValue === sectionListItem.subSections[s])) {
                    searchWithSubSections = sectionListItem.name + ',' + sectionListItem.subSections.join(',');
                    currentSection = sectionListItem.name;
                }
            }
        }
        function setSectionValue(sectionValue) {
            var t;

            for (t = 0; t < sectionList.length; t++) {
                if (sectionValue === sectionList[t].name) {
                    searchWithSubSections = sectionValue;
                    if (sectionList[t].subSections.length > 0) {
                        searchWithSubSections += ',' + sectionList[t].subSections.join(',');
                    }
                } else {
                    /* To select the section name if we search with subsection.
                     * For Example: If we search with 'china', 'regions', It has to selct 'News' as section name in search result page.
                     */
                    setValueFromSubSectionName(sectionValue, sectionList[t]);
                }
            }
        }
        function setSelectedSearchSection() {
            if (currentSection) {
                /* Match against sections on the CNN (not CNN Money) */
                setSectionValue(currentSection);
                $filterHolder.find('input[name=sections]').val(currentSection);
                if (searchWithSubSections !== '') {
                    _search.args.section = decodeURIComponent(searchWithSubSections);
                    $(document.getElementById('left_' + currentSection)).prop('checked', true);
                    $('.sectionOptions').find('li').hide();
                    $(document.getElementById(currentSection)).addClass('clicked').show();
                }
            }
        }
        function setDefaultSearchSection() {
            if (searchWithSubSections === '') {
                if (document.getElementById('left_allcnn')) {
                    $(document.getElementById('left_allcnn')).prop('checked', true);
                    $(document.getElementById('allcnn')).addClass('clicked').show();
                } else {
                    $(document.getElementById('left_news')).prop('checked', true);
                    $(document.getElementById('news')).addClass('clicked').show();
                }
            }
        }

        function getDates(val) {
            var date = new Date(),
                time = date.getTime(),
                _ayearago = new Date(time - (3600 * 24 * 365 * 1000)),
                _amonthago = new Date(time - (3600 * 24 * 30 * 1000)),
                _aweekago = new Date(time - (3600 * 24 * 7 * 1000)),
                _adayago = new Date(time - (3600 * 24 * 1 * 1000)),
                today = {
                    yyyy: date.getFullYear(),
                    mm: ((date.getMonth() + 1) < 10) ? '0' + (date.getMonth() + 1) : date.getMonth() + 1,
                    dd: (date.getDate() < 10) ? '0' + date.getDate() : date.getDate()
                },
                ayearago = {
                    yyyy: _ayearago.getFullYear(),
                    mm: ((_ayearago.getMonth() + 1) < 10) ? '0' + (_ayearago.getMonth() + 1) : _ayearago.getMonth() + 1,
                    dd: (_ayearago.getDate() < 10) ? '0' + _ayearago.getDate() : _ayearago.getDate()
                },
                aweekago = {
                    yyyy: _aweekago.getFullYear(),
                    mm: ((_aweekago.getMonth() + 1) < 10) ? '0' + (_aweekago.getMonth() + 1) : _aweekago.getMonth() + 1,
                    dd: (_aweekago.getDate() < 10) ? '0' + _aweekago.getDate() : _aweekago.getDate()
                },
                adayago = {
                    yyyy: _adayago.getFullYear(),
                    mm: ((_adayago.getMonth() + 1) < 10) ? '0' + (_adayago.getMonth() + 1) : _adayago.getMonth() + 1,
                    dd: (_adayago.getDate() < 10) ? '0' + _adayago.getDate() : _adayago.getDate()
                },
                amonthago = {
                    yyyy: _amonthago.getFullYear(),
                    mm: ((_amonthago.getMonth() + 1) < 10) ? '0' + (_amonthago.getMonth() + 1) : _amonthago.getMonth() + 1,
                    dd: (_amonthago.getDate() < 10) ? '0' + _amonthago.getDate() : _amonthago.getDate()
                },
                start = (val === 'PASTYEAR') ? ayearago.yyyy + '-' + ayearago.mm + '-' + ayearago.dd :
                    (val === 'PASTMONTH') ? amonthago.yyyy + '-' + amonthago.mm + '-' + amonthago.dd :
                        (val === 'PASTWEEK') ? aweekago.yyyy + '-' + aweekago.mm + '-' + aweekago.dd :
                            (val === 'PASTDAY') ? adayago.yyyy + '-' + adayago.mm + '-' + adayago.dd : '',
                end = (val === 'Past years') ? (today.yyyy - 1) + '-' + today.mm + '-' + today.dd : '',
                polars = {
                    start: start,
                    end: end
                };

            return polars;
        }

        /* Added by Howie */
        $('input[name=dredate]').click(function datesClickHandler() {
            var currentElement = $(this),
                rel = currentElement.attr('name'),
                val = currentElement.attr('id'),
                dates = [],
                isCategory = false;

            highlightSelectedElement(isCategory, val);
            $(document.getElementById('timeDropList')).val(val.concat('_D'));
            if (val === 'Anytime') {
                delete _search.args.startDate;
                delete _search.args.endDate;
            } else {
                _search.filtered = true;
                _search.filteredBy.push(rel);
                /* set the proper params */
                dates = getDates(val);
                _search.dateSort = val;
                if (dates.start !== '') {
                    _search.args.startDate = dates.start;
                }
                if (dates.end !== '') {
                    _search.args.endDate = dates.end;
                }
                _search.args[rel] = val;
            }
            _search.args.page = 1;
            _search.args.start = 1;
            callObj.args = setSearchParams();
            /* fetch the results */
            $(document.getElementById('cnnSearchLoader')).show('fast');
            CSIManager.getInstance().callObject(callObj);
        });

        $(document.getElementById('timeDropList')).change(function selectTimeHandler() {
            var currentElement = $(this),
                rel = currentElement.children(':selected').attr('name'),
                val = currentElement.children(':selected').attr('id'),
                dates = [],
                isCategory = false,
                dredate;

            val = val.substring(0, val.length - 2);
            $.each($('input[name=dredate]:radio'), function () {
                dredate = $(this);
                if (dredate.attr('id') === val) {
                    dredate.prop('checked', true);
                } else {
                    dredate.prop('checked', false);
                }
            });
            highlightSelectedElement(isCategory, val);

            if (val === 'Anytime') {
                delete _search.args.startDate;
                delete _search.args.endDate;
            } else {
                _search.filtered = true;
                _search.filteredBy.push(rel);
                /* set the proper params */
                dates = getDates(val);
                _search.dateSort = val;
                if (dates.start !== '') {
                    _search.args.startDate = dates.start;
                }
                if (dates.end !== '') {
                    _search.args.endDate = dates.end;
                }
                _search.args[rel] = val;
            }
            _search.args.page = 1;
            _search.args.start = 1;
            callObj.args = setSearchParams();
            /* fetch the results */
            $(document.getElementById('cnnSearchLoader')).show('fast');
            CSIManager.getInstance().callObject(callObj);
        });

        $('input:radio').click(function collectionClickHandler() {
            var currentElement = $(this),
                action = currentElement.attr('value'),
                rel = currentElement.attr('name');

            if (rel === 'collection' && action === 'Everything') {
                action = 'STORIES,VIDEOS,PHOTOS,INTERACTIVES,IREPORT';
            } else if (rel === 'section') {
                $('.sectionOptions').find('li').hide();
                $(document.getElementById(action)).addClass('clicked').show();
                if (action === 'allcnn') {
                    action = '';
                    $filterHolder.find('input[name=sections]').val('');
                } else {
                    $filterHolder.find('input[name=sections]').val(action);
                    setSectionValue(action);
                    action = searchWithSubSections;
                }
            }

            _search.filtered = true;
            _search.filteredBy.push(rel);
            if (action !== '') {
                _search.args[rel] = action;
            } else {
                delete _search.args[rel];
            }
            _search.args.page = 1;
            _search.args.start = 1;
            callObj.args = setSearchParams();

            /* fetch the results */
            if (typeof _search.query !== 'undefined' && _search.query.length > 0) {
                $(document.getElementById('cnnSearchLoader')).show('fast');
                CSIManager.getInstance().callObject(callObj);
            }
        });

        $(document.getElementById('catDropList')).change(function selectCatHandler() {
            var currentElement = $(this),
                optId = currentElement.children(':selected').attr('id'),
                rel = 'collection',
                val = '',
                newOptId = optId.substring(0, optId.length - 1),
                isCategory = true;

            $.each($('input[name=collection]:checkbox'), function () {
                if (currentElement.attr('id') === newOptId) {
                    currentElement.prop('checked', 'checked');
                } else {
                    currentElement.attr('checked', false);
                }
            });
            highlightSelectedElement(isCategory, newOptId);

            switch (optId) {
            case 'EverythingD':
                val = 'STORIES,VIDEOS,PHOTOS,INTERACTIVES,IREPORT';
                break;
            case 'STORIESD':
                val = 'STORIES';
                break;
            case 'VIDEOSD':
                val = 'VIDEOS';
                break;
            case 'PHOTOSD':
                val = 'PHOTOS';
                break;
            case 'INTERACTIVESD':
                val = 'INTERACTIVES';
                break;
            case 'IREPORTD':
                val = 'IREPORT';
                break;
            }

            _search.filtered = true;
            _search.filteredBy.push(rel);
            _search.args[rel] = val;
            _search.args.page = 1;
            _search.args.start = 1;
            callObj.args = setSearchParams();

            /* fetch the results */
            if (typeof _search.query !== 'undefined' && _search.query.length > 0) {
                $(document.getElementById('cnnSearchLoader')).show('fast');
                CSIManager.getInstance().callObject(callObj);
            }
        });

        $(document.getElementById('cnn-sort')).change(function sortOnHandler() {
            sortOn = $(this).val();
            $(document.getElementById('cnn-sort2')).val(sortOn);
            _search.args.sort = sortOn;
            callObj.args = setSearchParams();
            /* fetch the results */
            if (typeof _search.query !== 'undefined' && _search.query.length > 0) {
                $(document.getElementById('cnnSearchLoader')).show('fast');
                CSIManager.getInstance().callObject(callObj);
            }
        });

        $(document.getElementById('cnn-sort2')).change(function sortOn2Handler() {
            sortOn = $(this).val();
            $(document.getElementById('cnn-sort')).val(sortOn);
            _search.args.sort = sortOn;
            callObj.args = setSearchParams();
            /* fetch the results */
            if (typeof _search.query !== 'undefined' && _search.query.length > 0) {
                $(document.getElementById('cnnSearchLoader')).show('fast');
                CSIManager.getInstance().callObject(callObj);
            }
        });

        /**
         * Local function used to highlight the background for the selected/unselected
         * input element - checkbox or radio button by applying appropriate style classes.
         *
         * @param {boolean} isCategory - true for category elements; false for time elements
         * @param {string} elemId - selected element id
         */
        function highlightSelectedElement(isCategory, elemId) {

            var elementParentIdArray = (isCategory) ? categoryParentIdArray : timeParentIdArray || [],
                len = elementParentIdArray.length || 0,
                elemIdChecked = $(document.getElementById(elemId)).is(':checked') || false,
                currIdChecked,
                parentId,
                elem,
                i;

            for (i = 0; i < len; i++) {
                elem = $(document.getElementById(elementParentIdArray[i]));
                parentId = elementParentIdArray[i].substring(0, elementParentIdArray[i].length - 2) || '';
                currIdChecked = $(document.getElementById(parentId)).is(':checked') || false;
                if (elemId === parentId && elemIdChecked) {
                    elem.addClass('search-facet-bg-selected');
                } else if (!currIdChecked) {
                    elem.removeClass('search-facet-bg-selected');
                }
            }
        }

        function clearSearchFilters(fromSuggestedResults) {
            var theFilter;
            $.each(_search.filteredBy, function filterByHandler(i) {
                theFilter = _search.filteredBy[i];
                delete _search.args[theFilter];
            });

            delete _search.args.startDate;
            delete _search.args.endDate;

            _search.filteredBy = [];
            _search.filtered = false;

            if (fromSuggestedResults) {
                window.location.href = '/search/?text=' + encodeURIComponent(_search.term);
            } else {
                callObj.args = setSearchParams();
                CSIManager.getInstance().callObject(callObj);
            }
        }

        function setSearchParams() {
            var _args = '', _sep = '';
            $.each(_search.args, function addParamsToArgs(k, v) {
                if (k !== 'dredate') {
                    _args += _sep + k + '=' + encodeURIComponent(v);
                    _sep = '&';
                }
            });
            return _args;
        }

        function showSearchSuggest() {
            if ((typeof _search.args.startDate === 'undefined' ||
                _search.args.startDate === null) &&
                $(document.getElementById('Everything')).prop('checked')) {
                /* No restrictions on search */
                $(document.getElementById('cnnDidYouMeanBlock')).show();
                $(document.getElementById('cnnTryDifferentOptionsBlock')).hide();
            } else {
                /* Limited search */
                $(document.getElementById('cnnDidYouMeanBlock')).hide();
                $(document.getElementById('cnnTryDifferentOptionsBlock')).show();
            }
            $('.display-no-results').show();
        }

        function displaySuggestedResults() {
            var _content = '<ul>';

            $('<li><a href="/search/?text=' + _search.term + '" id="cnnSearchRefineClearAll">Clear Any Search Filters</a></li>')
                .insertBefore('div#cnnSearchTips ul li:first');

            if (typeof _search.results !== 'object') {
                _search.results = {
                    count: 0,
                    dym: {
                        correctedResults: []
                    },
                    term: ''
                };
            }
            $.each(_search.results.dym.correctedResults, function outputCorrectedResult(i) {
                var rslt = _search.results.dym.correctedResults[i],
                    type = rslt.mediaType,
                    date = rslt.mediaDateUts.replace(/\//g, '.'),
                    imgUrl,
                    assetUrl;

                _content += '<li>';
                /* START: gallery */
                if (type === 'gallery') {
                    assetUrl = CNN.Host.assetPath + '/assets/photo-bnw.svg';
                    imgUrl = (rslt.metadata.media.thumbnail.url) ? rslt.metadata.media.thumbnail.url : assetUrl;
                    _content += '<div><a href="' + rslt.url + '"><span class="image">';
                    _content += '<img title="' + rslt.title + '" alt="' + rslt.title + '" src="' + imgUrl + '"/>';
                    _content += '</span></a></div>';
                }
                /* END: gallery */

                _content += '<div><a href="' + rslt.url + '"><span class="title">' + rslt.title  + '</span>';
                _content += '<span class="date"> (' + date + ')</span></a></div>';
                _content += '<div><a href="' + rslt.url + '"><span class="desc">' + rslt.metadata.media.excerpt  + '</span></a></div>';

                /* END: article */
                _content += '</li>';

            });
            _content += '</ul>';
            $(document.getElementById('textResultsContainer')).html(_content);
        }

        function updateAnalytics() {
            if (_search.results.count === 0) {
                $('meta[name="cnn.omniture.search_results_count"]').attr('content', 'zero');
            } else {
                $('meta[name="cnn.omniture.search_results_count"]').attr('content', _search.results.count);
            }
            $('meta[name="cnn.omniture.search_term"]').attr('content', _search.results.term);
            $('meta[name="cnn.omniture.search_results_page"]').attr('content', parseInt(_search.args.page, 10));

            try {
                trackMetrics({
                    type: 'cnnsearch-results',
                    data: {
                        search_results_count: (_search.results.count <= 0 ? 'zero' : _search.results.count),
                        search_results_page: _search.args.page,
                        search_term: _search.term
                    }
                });
            } catch (e) {}
        }

        function handleSearchResults(o) {
            var msg = '',
                resultContent,
                spotCount = 0,
                $cnnSearchDidYouMean = $(document.getElementById('cnnSearchDidYouMean')),
                $cnnSearchDYMLink = $(document.getElementById('cnnSearchDYMLink')),
                $cnnSearchDidYouMeanWithResults = $(document.getElementById('cnnSearchDidYouMeanWithResults')),
                $cnnSearchDYMWithResultsLink = $(document.getElementById('cnnSearchDYMWithResultsLink'));

            $(document.getElementById('cnnSearchLoader')).hide('fast');
            $(document.getElementById('cnnContentColumn')).show('fast');
            _search.results = {
                count: o.metaResults.all,
                content: o.results[0],
                start: o.criteria[0].startAt,
                term: o.criteria[0].queries[0],
                end: o.criteria[0].maxResults,
                spotlights: o.spotlights,
                buckets: o.buckets,
                dym: o.didYouMean
            };

            _search.spotlights = _search.results.spotlights;
            if (typeof _search.results.spotlights === 'object' &&
                typeof _search.results.spotlights[0] === 'object' &&
                typeof _search.results.spotlights[0].id !== 'undefined' &&
                _search.results.spotlights[0].id === 'cnnSpotlight' &&
                Array.isArray(_search.results.spotlights[0].items) &&
                _search.results.spotlights[0].items.length > 0) {

                $.each(_search.results.spotlights[0].items, function (i, item) {
                    resultContent = {};
                    resultContent.id = item.id;
                    resultContent.mediaDateUts = item.mediaDateUts;
                    resultContent.title = item.title;
                    resultContent.description = item.title;
                    resultContent.url = item.url;
                    if (typeof item.metadata !== 'undefined' &&
                        typeof item.metadata.media !== 'undefined' &&
                        typeof item.metadata.media.image !== 'undefined') {
                        resultContent.thumbnail = item.metadata.media.image;
                        if (typeof item.metadata.media.excerpt === 'string' &&
                            item.metadata.media.excerpt.length > 0) {
                            resultContent.description = item.metadata.media.excerpt;
                        }
                    }

                    /* Keep profile page link at the top within spotlights */
                    _search.results.content.splice(spotCount, 0, resultContent);
                    spotCount++;
                });
            }

            if (_search.firstLoad) {
                $(document.getElementById('Everything')).prop('checked', true);
                $('input[name="dredate"][value="Anytime"]').prop('checked', true);
                _search.firstLoad = false;
            }

            if (_search.results.count < 1) {
                if ($.trim(_search.term) === '') {
                    $(document.getElementById('cnnSearchNoMatch')).hide('fast');
                    $(document.getElementById('cnnSearchEmpty')).show('fast');
                } else {
                    msg = decodeURIComponent(_search.term.replace(/\+/g, ' '));
                    $(document.getElementById('cnnSearchDidNotMatch')).text(msg);
                    $(document.getElementById('cnnSearchNoMatch')).show('fast');
                    $(document.getElementById('cnnSearchEmpty')).hide('fast');
                    if ($.trim(_search.results.dym.prompt) !== '') {
                        $cnnSearchDYMLink.attr('href', '/search/?text=' + _search.results.dym.prompt);
                        $cnnSearchDYMLink.html(_search.results.dym.prompt);
                        $cnnSearchDidYouMean.css('display', 'block');
                    }
                }
                $('.display-results').hide();
                $('.display-no-results').show();

                displaySuggestedResults();
                showSearchSuggest();

                if (typeof CNN.Features === 'object' && CNN.Features.enableOmniture !== false) {
                    if (typeof $.fn.onAnalyticsMetadataReady === 'function') {
                        $(document).onAnalyticsMetadataReady(updateAnalytics);
                    } else {
                        updateAnalytics();
                    }
                }

                /* clear all filters functionality */
                $(document.getElementById('cnnSearchRefineClearAll')).click(function clearAllClickHandler() {
                    clearSearchFilters(true);
                });
            } else {
                displayResults();    /* display the content */
                displayPagination(); /* display the pagination */
                displaySummary();    /* display the summary */

                if (typeof _search.results.dym.prompt === 'string' && _search.results.dym.prompt.length > 0) {
                    $cnnSearchDYMWithResultsLink.attr('href', '/search/?text=' + _search.results.dym.prompt);
                    $cnnSearchDYMWithResultsLink.html(_search.results.dym.prompt);
                    $cnnSearchDidYouMeanWithResults.css('display', 'block');
                }
            }
            return (msg !== '' ? msg + ' did not match any documents' : _search.results.count + ' Results');
        } /* end: handleSearchResults */

        /* results config */
        callObj = {
            url: _search.url,
            args: '',
            domId: 'cnnSearchSummaryResults',
            funcObj: handleSearchResults
        };

        /* set the search parameters, fetch the results */
        if (typeof _search.query !== 'undefined' && _search.query.length > 0) {
            $(document.getElementById('cnnSearchLoader')).show('fast');

            /* fetch the results */
            callObj.args = setSearchParams();
            CSIManager.getInstance().callObject(callObj);

            /* Drop the search term into each input box */
            $.each(_search.input, function addTermToInput() {
                var currentElement = $(this);

                currentElement.val(_search.query);
                $clearText.show();
                currentElement.blur(function addTermToInputBlur() {
                    currentElement.val(currentElement.val().toLowerCase());
                });
            });
        } else {
            $(document.getElementById('cnnSearchNoMatch')).hide('fast');
            $(document.getElementById('cnnSearchEmpty')).show('fast');
            $('.display-results').hide();
            $('.display-no-results').show();
            $('#cnnTryDifferentOptionsBlock').hide();

            displaySuggestedResults();
            showSearchSuggest();
            if (typeof CNN.Features === 'object' && CNN.Features.enableOmniture !== false) {
                if (typeof $.fn.onAnalyticsMetadataReady === 'function') {
                    $(document).onAnalyticsMetadataReady(updateAnalytics);
                } else {
                    updateAnalytics();
                }
            }

            /* clear all filters functionality */
            $(document.getElementById('cnnSearchRefineClearAll')).click(function clearAllClickHandler() {
                clearSearchFilters(true);
            });
        }


        function displaySummary() {
            var _summary,
                resultCount = (_search.results.count < _search.results.end) ? _search.results.count : _search.results.end;

            _summary = 'Displaying results ' + _search.results.start + '-' + resultCount +
            ' of ' + _search.results.count + ' for <span class="cnnSearchTerm">' +
            _search.query + '</span>';

            if (typeof CNN.Features === 'object' && CNN.Features.enableOmniture !== false) {
                if (typeof $.fn.onAnalyticsMetadataReady === 'function') {
                    $(document).onAnalyticsMetadataReady(updateAnalytics);
                } else {
                    updateAnalytics();
                }
            }

            $('.display-no-results').hide();
            $(document.getElementById('cnnSearchSummary')).html(_summary);
            $('.display-results').show();
        }

        function displayResults() {
            var _content = '';
            $.each(_search.results.content, function addSearchResultToContent(i) {
                var rslt = _search.results.content[i];

                _content += '<article class="cd cd--card cd--idx-0 cd--large cd--horizontal';
                if (rslt.thumbnail) {
                    _content += ' cd--has-media';
                    if (rslt.collection === 'Videos') {
                        _content += ' cd--video';
                    }
                }
                _content += '"><div class="cd__wrapper" data-analytics="_cn-results_stack-large-horizontal_">';
                if (rslt.thumbnail) {
                    _content += '<div class="cnnVRimgBGSearch" style="background-image: url(' +
                        rslt.thumbnail + ')"><a href="' + rslt.url + '">';
                    if (rslt.collection === 'Videos') {
                        _content += '<span class="cnnVRimgLink"></span></a></div>';
                    } else {
                        _content += '<span class="cnnVRphotoLink"></span></a></div>';
                    }
                }

                _content += '<div><h3 class="cd__headline"><a href="' + rslt.url + '"><span class="cd__headline-text">' +
                    rslt.title + '</span></a></h3>' +
                    '<span class="cd__timestamp">' + rslt.mediaDateUts + '</span>' +
                    '<div class="cd__description">' + rslt.description + '</div>' +
                    '</div></div></article></li>';
            });


            $(document.getElementById('textResultsContainer')).html(_content);
            $('html, body').scrollTop(0);
        }


        function displayPagination() {
            var pagesToDisplay = _search.args.npp,
                RESULTS_PER_PAGE = 10,
                PAGINATION_START_COUNT = 1,
                PAGINATION_MAX_VISIBLE_COUNT = 5,
                PAGINATION_MORE_VISIBLE_COUNT = 2,
                PAGINATION_NEED_MORE_VISIBLE_COUNT = 3,
                start = parseInt(_search.args.page, RESULTS_PER_PAGE),
                curr = start,
                end = curr + PAGINATION_MORE_VISIBLE_COUNT,
                _pagination = '',
                totalPages = Math.ceil(parseInt(_search.results.count, RESULTS_PER_PAGE) / pagesToDisplay),
                page;

            if (curr <= PAGINATION_NEED_MORE_VISIBLE_COUNT) {
                start = PAGINATION_START_COUNT;
                end = (end >= totalPages) ? totalPages : PAGINATION_MAX_VISIBLE_COUNT;
            } else {
                start = curr - PAGINATION_MORE_VISIBLE_COUNT;
            }
            if (end >= totalPages) {
                start = (totalPages <= PAGINATION_MAX_VISIBLE_COUNT) ? PAGINATION_START_COUNT : end - PAGINATION_MORE_VISIBLE_COUNT;
                end = totalPages;
            }

            if (curr > PAGINATION_START_COUNT) {
                _pagination += '<div class="pagination-bar"><div class="pagination-arrow pagination-arrow-left">' +
                    '<a class="cnnSearchPageLink" name="cnnSearchPage' + (curr - 1) + '">' +
                    '<span class="left image image-left-active"/><span class="right text text-active">Previous</span></a></div><div class="pagination-digits">';
            } else {
                _pagination += '<div class="pagination-bar"><div class="pagination-arrow pagination-arrow-left">' +
                    '<span class="left image image-left-deactive"/><span class="right text text-deactive">Previous</span></div><div class="pagination-digits">';
            }

            for (page = start; page <= end; page++) {
                _pagination += (page === curr ? '<a class="cnnSearchPageLink cnnAlt"' : '<a class="cnnSearchPageLink"') +
                    'name="cnnSearchPage' + page + '">' + page + '</a>' + (page < end ? ' ' : '');
            }

            if (curr < totalPages) {
                _pagination += '</div><div class="pagination-arrow pagination-arrow-right"><a class="cnnSearchPageLink" name="cnnSearchPage' +
                    (curr + 1) + '"><span class="left text text-active">Next</span><span class="right image image-right-active"/></a></div></div></li>';
            } else {
                _pagination += '</div><div class="pagination-arrow pagination-arrow-right"><span class="left text text-deactive">Next</span>' +
                    '<span class="right image image-right-deactive"/></a></div></div></li>';
            }

            $(document.getElementById('cnnSearchPagination')).html(_pagination);
            $(document.getElementById('cnnSearchPagination')).show();

            $('a.cnnSearchPageLink').each(function addParamsToLink() {
                var currentElement = $(this),
                    _page = currentElement.attr('name').substring(13);
                currentElement.click(function addParamsOnClick() {
                    _search.args.page = _page;
                    _search.args.start = (_page * _search.args.npp) - 9;

                    callObj.args = setSearchParams();

                    $(document.getElementById('cnnSearchLoader')).show('fast');
                    CSIManager.getInstance().callObject(callObj);
                });
            });
            if (typeof CNN !== 'undefined' && CNN !== null) {
                if (typeof CNN.Features === 'object' && CNN.Features.enableShareButtons === true &&
                    typeof CNN.gigyaShareBar === 'function') {

                    CNN.gigyaShareBar();
                }
            }

        }

        /* Show/Hide functionality for Drop down (section, sort by date/Relevance) */

        $('.sortOptions, .sectionOptions').on('click', function () {
            var currentElement = $(this);

            if (!currentElement.hasClass('dropExpand')) {
                currentElement.addClass('dropExpand').find('li').show();
                if (currentElement.hasClass('sectionOptions')) {
                    $searchBody.addClass('noscroll');
                }
            } else {
                currentElement.removeClass('dropExpand').find('li:not(.clicked)').hide();
                $searchBody.removeClass('noscroll');
            }
        });

        /* Sort functionaluty from Relavance, Date and section dropdown for mobile layout */

        $('.sortOptions, .sectionOptions').on('click', 'li', function sortFromDropDown(event) {
            var currentElement = $(this),
                $parentElement = currentElement.parent();

            if (!$parentElement.hasClass('dropExpand')) {
                $parentElement.addClass('dropExpand');
                $parentElement.find('li').show();
                if ($parentElement.hasClass('sectionOptions')) {
                    $searchBody.addClass('noscroll');
                }
            } else {
                $parentElement.removeClass('dropExpand');
                $searchBody.removeClass('noscroll');
                $parentElement.find('li').removeClass('clicked').hide();
                currentElement.addClass('clicked').show();
                sortOn = currentElement.attr('id');
                if ($parentElement.hasClass('sortOptions')) {
                    _search.args.sort = sortOn;
                } else {
                    $(document.getElementById('left_' + sortOn)).prop('checked', true);
                    if (sortOn === 'allcnn') {
                        sortOn = '';
                        $filterHolder.find('input[name=sections]').val('');
                    } else {
                        $filterHolder.find('input[name=sections]').val(sortOn);
                        setSectionValue(sortOn);
                        sortOn = searchWithSubSections;
                    }
                    _search.args.section = sortOn;
                }
                callObj.args = setSearchParams();
                /* fetch the results */
                if (typeof _search.query !== 'undefined' && _search.query.length > 0) {
                    CSIManager.getInstance().callObject(callObj);
                }
            }
            event.stopPropagation();
        });

        /* Clear the search text when we click on clear link */

        $clearText.on('click', function clearSearchText() {
            $(document.getElementById('searchInputTop')).val('');
            $(this).hide();
        });

        /* Toggle clear text link on search box */

        $(document.getElementById('searchInputTop')).on('keyup', function toggleClearText() {
            if ($(this).val() === '') {
                $clearText.hide();
            } else {
                $clearText.show();
            }
        });

        /* Left rail ad lock on search result page */

        jQuery(window).throttleEvent('scroll', function adLockOnSearchPage() {
            scrollPosition = $(this).scrollTop();
            $footerElement = (jQuery('.appia-container.js-appia').length > 0) ? jQuery('.appia-container.js-appia')[0] : jQuery('footer.l-footer')[0];
            footerPosition = $footerElement.getBoundingClientRect();
            filterHolderHeight = $filterHolder[0].getBoundingClientRect().top + $filterHolder[0].getBoundingClientRect().height;
            if (scrollPosition > 10) {
                $searchBody.removeClass('scrollUp').addClass('scrollDown');
            } else {
                $searchBody.removeClass('scrollDown').addClass('scrollUp');
            }

            if ($adSlotContainer[0].getBoundingClientRect().top <= filterHolderHeight) {
                $adSlotLock.addClass('el__locked').css({
                    top: filterHolderHeight + 10,
                    width: sectionFilterWidth
                });
            } else {
                $adSlotLock.removeClass('el__locked').css({
                    left: 'auto',
                    top: 'auto'
                });
            }
            adSlotValue = $adSlotLock[0].getBoundingClientRect();
            if (footerPosition.top <= (adSlotValue.top + adSlotValue.height)) {
                $adSlotLock.css({
                    top: footerPosition.top - adSlotValue.height - 5
                });
            }
        });
    });
}(jQuery));


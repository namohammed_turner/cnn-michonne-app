/* global CNN, dust, fastdom */

/**
 * @module mailchimp-subscribe
 *
 * Render the necessary template(s) and image branding for each
 * mailchimp newsletter subscription card
 *
 */
(function subscribe($j, win, doc, ns, fastdom) {
    'use strict';

    if (!ns.hasOwnProperty('newsletterSubscriptionCard')) {
        ns.newsletterSubscriptionCard = (function newsletterSubscribe() {
            /**
             * @var {Object} cmcsBrand
             *
             * @var {String} cmcsBrand.inputcontext
             *
             * contains the class name set on card inputs above the footer
             *
             * @var {String} cmcsBrand.uicontext
             *
             * DOM node to place the result of the view render
             *
             * @var {Object} cmcsfooter
             *
             * @var {String} cmcsfooter.inputcontext
             *
             * contains the class name to set on the footer card input
             *
             * @var {String} cmcsfooter.uicontext
             *
             * DOM node to place the result of the view render
             *
             * @var {Function} dustRenderPromise
             *
             * assigned to the structure promise on init
             *
             * @var {Object} mailchimp
             *
             * michonne runtime config
             *
             * @var {Object} state
             *
             * an object containing the various forms states e.g. success/error
             * addtional DOM nodes representing the state of the forms should be
             * added here
             *
             * @var {String} state.errorblock
             *
             * DOM node to place the string result of failUI
             *
             * @var {String} state.form
             *
             * the classname of the form to submit
             *
             * @var {String} state.section
             *
             * @var {String} state.submitbutton
             *
             * the classname of the submit button
             *
             * @var {String} state.success
             *
             * sets the form success state on successful submission
             */
            var
                cmcsfooter = {
                    inputcontext:   'cmcs-input-footer',
                    uicontext:      '.js-cmcs-footer-card'
                },
                mailchimp = CNN.Mailchimp || {},
                sectIsEnabled = mailchimp.section,
                state = {
                    errorblock:     '.js-subscribe-error',
                    form:           '.js-cmcs-subscription-form',
                    section:        CNN.contentModel.sectionName || '',
                    submitbutton:   '.js-mc-submit',
                    success:        'submit-success'
                },

                /**
                 * @var setfeedback
                 *
                 * An object representing the message labels that are shown to the
                 * user based on their action (e.g., if the user entered an invalid
                 * email). If other messages need to be shown to the user then add
                 * them to this object.
                 *
                 * @var {string} setfeedback.email
                 * The text the end user sees when attempting to submit the form
                 * with a malformed email address
                 *
                 * @var {string} setfeedback.success
                 * The text the end user sees on successful form submit
                 *
                 * @var {string} setfeedback.invalidType
                 * The text the end user sees on failure
                 *
                 * @private
                 */
                setfeedback = {
                    email:          'Your e-mail address is invalid.',
                    success:        'Check your email to confirm your subscription.',
                    invalidType:    'Your submission was not accepted.'
                },

                uicmcsFooter,
                uicmcsBrand,
                uicmcsEmbedded,
                uicmcsLandingPage,
                wrapperClass,
                /**
                 * @method structure
                 *
                 * @description
                 * creates the template model for the subscription card
                 * where uicmcsFooter is the data passed to the footer view and
                 * uicmcsBrand is the data passed to the cards above the footer
                 *
                 * @param {object} card
                 * pal config model
                 *
                 * @returns {object}
                 * a promise object that when resolved successfully will set the `this`
                 * context of the callback to an object comprised of branded card
                 * and footer card data
                 *
                 * @private
                 */
                structure = function configuration(card) {
                    var data,
                        dfr = $j.Deferred();

                    if (typeof card === 'object' && card.activelist && card.cardtext && card.branding && !card.landingPageSource) {

                        data = {
                            activeList: card.activelist,
                            base:       card.baselist,
                            formAction: card.action,
                            intro:      card.cardtext.intro,
                            label:      card.cardtext.label,
                            title:      card.cardtext.title
                        };
                        uicmcsFooter = $j.extend(
                            true,
                            {
                                enableInFooter:         sectIsEnabled[state.section].enableFooter,
                                overrideSectionFooter:  card.overrideSectionFooter,
                                footer:                 true
                            },
                            data,
                            cmcsfooter
                        );
                        uicmcsBrand = $j.extend(
                            true,
                            {
                                enableInclude:      sectIsEnabled[state.section].enableInclude,
                                cmsinclude:         true,
                                hasBranding:        card.branding.imageUrl,
                                imageBreakpoints:   CNN.ImageBreakpoints,
                                imageCutPaths:      {
                                    mini:       CNN.Host.chrome + card.branding.paths.mini.uri,
                                    xsmall:     CNN.Host.chrome + card.branding.paths.xsmall.uri,
                                    small:      CNN.Host.chrome + card.branding.paths.small.uri,
                                    medium:     CNN.Host.chrome + card.branding.paths.medium.uri,
                                    large:      CNN.Host.chrome + card.branding.paths.large.uri,
                                    full16x9:   CNN.Host.chrome + card.branding.paths.full16x9.uri
                                }
                            },
                            data
                        );
                        uicmcsEmbedded = $j.extend(
                            true,
                            {
                                articleEmbed:   card.articleEmbed || false,
                                media: CNN.Host.chrome + card.branding.paths.small.uri
                            },
                            data
                        );

                        dfr.resolveWith({footer: uicmcsFooter, branded: uicmcsBrand, embedded: uicmcsEmbedded});
                    } else if (typeof card === 'object' && card.activelist && card.cardtext && card.landingPageSource.length > 0) {

                        data = {
                            activeList:            card.activelist,
                            base:                  card.baselist,
                            formAction:            card.action,
                            label:                 card.cardtext.label,
                            landingPageSignupText: card.cardtext.landingPageSignupText,
                            landingPageSource:     card.landingPageSource
                        };
                        uicmcsLandingPage = $j.extend(
                            true,
                            {
                                enableInclude:      sectIsEnabled[state.section].enableInclude,
                                cmsinclude:         true
                            },
                            data
                        );
                        dfr.resolveWith({landingPage: uicmcsLandingPage});
                    } else {
                        dfr.reject();
                    }

                    return dfr.promise().fail(function structureFail() {
                        console.log('Newsletter subscription card failed before render');
                        return;
                    });
                },

                /**
                 * @method render
                 *
                 * @description
                 * Renders the subscription card u.i.
                 * The @link includeIE's second url parameter will override the
                 * mailchimp runtime config section footer display setting
                 *
                 * @param {Object} structuredData
                 * an array of objects containing the data for each view
                 *
                 * @returns {Function}
                 * promise object
                 *
                 * @private
                 */
                render = function setViewData(structuredData) {
                    var data,
                        dfr                 = $j.Deferred(),
                        success             = false,
                        view                = 'views/cards/tool/mailchimp-subscribe-form';

                    data = structuredData;
                    if (data[0].enableInFooter === true && data[0].overrideSectionFooter === false) {
                        data[0].enableInFooter = false;
                    }
                    if ((data[0].enableInFooter) || (data[0].overrideSectionFooter)) {
                        dust.render(view, data[0], function renderFooter(error, out) {
                            if (error) {
                                dfr.reject();
                                console.log('Rendering footer subscription card failed during - ' + error);
                            }
                            $j(data[0].uicontext)
                                .eq(0)
                                .html(out)
                                .closest('.cmcs-subscription-wrapper')
                                .addClass('cmcs-footer-active');
                            success = true;
                        });
                    }

                    if (data[1].enableInclude) {
                        dust.render(view, data[1], function renderInclude(error, out) {
                            if (error) {
                                dfr.reject(error);
                                console.log('Including subscription card failed during - ' + error);
                            }
                            wrapperClass = '.' + data[1].label + '-card-wrapper';
                            $j(wrapperClass)
                                .empty()
                                .html(out);
                            success = true;
                        });
                    }

                    if (data[2].articleEmbed) {
                        dust.render(view, data[2], function renderInclude(error, out) {
                            if (error) {
                                dfr.reject(error);
                                console.log('Embedding subscription card failed during - ' + error);
                            }
                            wrapperClass = '.' + data[2].label + '-embed-wrapper';
                            $j(wrapperClass)
                                .empty()
                                .html(out)
                                .children('.js-cmcs-subscription-form')
                                .addClass('cmcs-embedded');
                            success = true;
                        });
                    }

                    if (success) {
                        dfr.resolve();
                    } else {
                        dfr.reject();
                    }

                    if (typeof CNN.ResponsiveImages === 'object' && CNN.ResponsiveImages !== null) {
                        fastdom.mutate(function () {
                            CNN.ResponsiveImages.queryEqjs(document);
                        });
                    }

                    return dfr.promise().then(handleForm).fail(function renderFail() {
                        console.log('Newsletter subscription card failed to render');
                        return;
                    });
                },

                /**
                 * @method mergeTracking
                 *
                 * @description
                 * merges source data into newsletter merge fields
                 * for audience development tracking
                 *
                 * @param {Object} target
                 * original object containing the merge_fields property
                 *
                 * @param {Object} source
                 * source object to merge from
                 *
                 * @returns {object}
                 * The updated target object
                 *
                 * @private
                 */
                mergeTracking = function audienceTracking(target, source) {
                    var prop,
                        src = '';

                    if (target && typeof target === 'object') {
                        if (!source) {
                            return target;
                        }

                        for (prop in source) {
                            if (source.hasOwnProperty(prop) && !Array.isArray(source[prop])) {
                                src += source[prop] + ':';
                            }
                        }

                        src = src.substr(0, src.length - 1);
                        target.merge_fields.SOURCE = src;
                    }

                    return target;
                },

                /** @method setCustomSource
                *
                * @description
                * get the source value from query parameter or from cms include url2
                *
                * @param {String} source
                * source to be passed
                *
                * @returns {String} source
                * The custom source value
                *
                * @private
                */
                setCustomSource = function getCustomSource(source) {

                    var customSource = '',
                        reg,
                        qs;

                    reg = new RegExp('[?&]' + source + '=([^&#]*)', 'i');
                    qs = reg.exec(window.location.href);
                    customSource = Array.isArray(qs) && qs.length > 0 ? qs[1] : null;

                    if (customSource === null &&
                        typeof state.landingPageSource === 'string' &&
                        state.landingPageSource.length > 0) {
                        customSource = state.landingPageSource;
                    } else if (customSource === null &&
                        typeof ns.contentModel.sectionName === 'string' &&
                        ns.contentModel.sectionName.length > 0) {
                        customSource = ns.contentModel.sectionName;
                    }

                    return customSource;
                },

                /**
                 * @method subscribe
                 *
                 * @description
                 * subscribe the user to the requested list
                 * Posts to the service layer that communicates with Mailchimp's 3.0 API.
                 *
                 * @param {String} url
                 * the request endpoint & form action URL
                 *
                 * @param {object} data
                 * the user's email address
                 *
                 * @private
                 */
                subscribe = function subscribe(url, data) {

                    var dataString,
                        payload,
                        msg;

                    payload = mergeTracking(data, {
                        edition: ns.contentModel.edition,
                        pageType: ns.contentModel.pageType,
                        sectionName: setCustomSource('SOURCE')
                    });

                    try {
                        dataString = JSON.stringify(payload);
                    } catch (error) {
                        failUI(setfeedback.invalidType);
                        return;
                    }

                    $j.ajax({
                        type:           'POST',
                        url:            url,
                        data:           dataString,
                        dataType:       'json',
                        contentType:    'application/json',
                        cache:          false,
                        success: function handleSuccess(_data, status, xhr) {
                            if (xhr.status === 200) {
                                thirdPartyOnSubscribeHandler('bing');
                                successUI(setfeedback.success);
                            }
                        },
                        error: function handleError(xhr, status, _error) {
                            if (status === 'error' && xhr.responseText) {
                                msg = JSON.parse(xhr.responseText);
                                failUI(msg.message);
                            } else {
                                return;
                            }
                        }
                    });
                },

                /**
                 * @method thirdPartyOnSubscribeHandler
                 *
                 * @description
                 * Some third party companies need to collect data when a subscribe is done
                 * this encapsulates that ugliness in one area
                 *
                 * @param {string} thirdPartyName - name of third party identifier
                 * @private
                 */
                thirdPartyOnSubscribeHandler = function onSubscribeHandler(thirdPartyName) {
                    if (typeof thirdPartyName === 'string' && thirdPartyName.length > 0) {
                        if (thirdPartyName === 'bing') {
                            try {
                                /* Per requirements in ticket: CNN-24307 */
                                window.uetq = window.uetq || [];
                                window.uetq.push(
                                    { 'ec':'newsletter', 'ea':'button', 'el':'subscribe', 'ev':0 }
                                );
                            } catch (err) {
                                console.log(err);
                            }
                        }
                    }
                },

                /**
                 * @method validateEmail
                 *
                 * @description
                 * checks for well formed email address
                 * true: subscribe the user to the list
                 * false: display the error to the user
                 *
                 * @param {string} url - url to validate
                 * @param {object} data - data object
                 *
                 * @private
                 */
                validateEmail = function validate(url, data) {
                    var check = /^([\w\.\'\+\-]+)\@([\w\-]{1,63}\.)+[\w\-]{2,63}$/;

                    if (!(check.test(state.input.val()))) {
                        resetErrorUI();
                        failUI(setfeedback.email);
                    } else {
                        subscribe(url, data);
                    }
                },

                /**
                 * @method handleForm
                 *
                 * @description
                 * get the context for the form submitted
                 *
                 * @event click
                 * get `this` context of submit & email input
                 *
                 * @event submit
                 * prevent the default form action
                 * structure the payload
                 * disable the submit button
                 *
                 * @private
                 */
                handleForm = function setContext() {
                    $j(state.submitbutton).on('click', function getContext(_e) {
                        state.ok        = $j(this).closest(state.form).find('.js-cnn-sub-ok');
                        state.error     = $j(this).siblings('.cmcs-input-email').find('.js-subscribe-error');
                        state.input     = $j(this).siblings('.cmcs-input-email').find('[type=email]');
                        state.submit    = $j(this);
                        state.landingPageSource = $j(this).data('src') || null;
                    });

                    $j('.js-cmcs-footer-card' + ',' + wrapperClass).find('form').on('submit', function submit(e) {
                        var arr     = $j(e.target).serializeArray(),
                            data    = {},
                            url     = e.target.action;

                        e.preventDefault();

                        $j.each(arr, function parseArray(i, b) {
                            data[b.name] = b.value;
                        });
                        data.merge_fields = {};

                        fastdom.mutate(function disableInputWhileProcessing() {
                            state.submit.prop('disabled', true);
                        });

                        validateEmail(url, data);
                    });
                },

                /**
                 * @method failUI
                 *
                 * @description
                 * update state.errorblock with the fail message
                 *
                 * @param {string} msg - message to use
                 *
                 * @private
                 */
                failUI = function failed(msg) {
                    fastdom.mutate(function setFailMsg() {
                        resetErrorUI();
                        state.submit
                            .siblings('.cmcs-input-email')
                            .find(state.errorblock)
                            .append(msg);
                    });
                },

                /**
                 * @method successUI
                 *
                 * @description
                 * update state.success with success message
                 *
                 * @param {string} msg - message to use
                 *
                 * @private
                 */
                successUI = function success(msg) {
                    fastdom.mutate(function createSuccessMsg() {
                        state.submit.closest(state.form)
                            .addClass(state.success);

                        state.submit
                            .closest(state.form)
                            .find('.js-cnn-news-ok')
                            .text(msg);

                        resetFormUI();
                    });
                },

                /**
                 * @method resetFormUI
                 *
                 * @description
                 * toggles the default/success state of the form
                 * enables the submit button
                 *
                 * @event click
                 * get the closest email input and clears the value
                 * sets the default state of the form
                 *
                 * @private
                 */
                resetFormUI = function showForm() {
                    state.submit.prop('disabled', false);
                    state.ok.on('click', function showForm(_e) {
                        fastdom.mutate(function resetInputValue() {
                            var $form = $j(_e.target).closest(state.form);
                            state.input = $form.find('.cmcs-input-email').find('[type=email]');
                            state.submit = $form.find(state.submitbutton);
                            state.error = $form.find('.js-subscribe-error');
                            resetErrorUI();
                            state.input.val('');
                            state.submit.closest(state.form)
                                .removeClass('submit-success');
                        });
                    });
                },

                /**
                 * @method resetErrorUI
                 *
                 * @description
                 * empties all error blocks
                 *
                 * @private
                 */
                resetErrorUI = function resetErrorBox() {
                    var context = state.error;

                    if (context) {
                        context.each(function clearErrorBlock(i, f) {
                            $j(f).empty();
                        });
                    }

                    state.submit.prop('disabled', false);
                };

            return {
                /**
                 * @method init
                 *
                 * @description
                 * initialize
                 *
                 * @param  {obj} card
                 * the pal card model
                 *
                 * @public
                 */
                init: function process(card) {
                    structure(card).then(function () {
                        if (!card.landingPageSource) {
                            render([this.footer, this.branded, this.embedded]);
                        } else {
                            render(['', this.landingPage, '']);
                        }
                    }).then(function () {
                        ns.DemandLoading.addElement($j('.cmcs-eqjs'));
                    });
                }
            };
        })();
    }

}(jQuery, window, document, CNN, fastdom));

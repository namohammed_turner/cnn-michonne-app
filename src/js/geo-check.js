/* global CNN */

CNN.Features = CNN.Features || {};
CNN.VideoConfig = CNN.VideoConfig || {};
CNN.VideoConfig.liveStream = CNN.VideoConfig.liveStream || {};


/**
 * Geo-checking code used for video live streaming.
 *
 * @name CNN.GeoCheck
 * @namespace
 * @memberOf CNN
 */
CNN.GeoCheck = (function ($, doc) {
    'use strict';

    var geoData = null;

    CNN._geoCheckReadyEvent = false;

    /* Extend jQuery with "onGeoCheckReady" event handler function and "triggerGeoCheckReady" to trigger the event. */
    $.fn.extend({
        /* Add event handler for geo check ready event */
        onGeoCheckReady: function (handler) {
            if (typeof handler === 'function') {
                if (CNN._geoCheckReadyEvent === true) {
                    handler($);
                } else {
                    $(doc).on('geoCheckReady', {handler: handler}, function _geoCheckReadyEventHandler(event) {
                        event = event || {};
                        if (typeof event.data === 'object' && event.data !== null && typeof event.data.handler === 'function') {
                            event.data.handler($);
                        }
                    });
                }
            }
        },
        triggerGeoCheckReady: function () {
            CNN._geoCheckReadyEvent = true;
            $(doc).trigger('geoCheckReady');
        }
    });

    /* Initialize the Geo Data */
    (function () {
        var fields,
            val;

        /* Grab GeoIP data from cookie */
        val = CNN.Utils.getCookie('geoData');
        if (val !== null) {
            fields = val.split('|');
            if (fields.length > 4) {
                geoData = {
                    /* See Fastly docs for these values at https://docs.fastly.com/guides/vcl/geoip-related-vcl-features */
                    city: fields[0],     /* city, e.g.: Atlanta or London -- mixed case, Latin-1 character set */
                    region: fields[1],   /* region, e.g.: GA or H9 -- see http://www.maxmind.com/download/geoip/misc/region_codes.csv */
                    postCode: fields[2], /* postal_code, e.g.: 30309 or EC4N -- may be partial, see docs */
                    country: fields[3],  /* country_code, e.g.: US or GB -- ISO3166-1 2-character country codes, plus some extras */
                    continent: fields[4] /* continent_code, one of AF (Africa), AS (Asia), EU (Europe), NA (N. America), OC (Oceania), SA (S. America) */
                };
            }
        }
        $(doc).triggerGeoCheckReady();
    })();

    /**
     * Get the GeoIP data
     *
     * @public
     * @returns {object} - Object containing the geo data, or null if not set/none found.
     */
    function getData() {
        return geoData;
    }

    /**
     * Check the geo-location data returned from the API against the targets array
     * to see if we are included.  Note that if there is no overlap, by default we
     * will "fail true", so things still work when the API doesn't.
     *
     * @public
     * @param {array} targets - The array of target strings to check against, in order
     * @returns {boolean} - true if in targetted area, false if not
     */
    function inTarget(targets) {
        var i,
            flag,
            scpat,
            sccpat,
            state = true,
            t;

        if (geoData !== null && Array.isArray(targets) === true && targets.length > 0) {
            scpat = geoData.country + ':' + geoData.region;
            sccpat = scpat + ':' + geoData.city.toLowerCase();
            for (i = 0; i < targets.length; i++) {
                t = targets[i];
                if (t.charAt(0) === '-') {
                    t = t.slice(1);
                    flag = false;
                } else {
                    flag = true;
                }
                if (t === '*') {
                    state = flag;
                } else if (t.charAt(0) === '#') {
                    if (geoData.continent === t.slice(1)) {
                        state = flag;
                    }
                } else if ((t.length === 2 && geoData.country === t) ||
                    (t.length === 5 && scpat === t) || (t.length > 6 && sccpat === t)) {
                    state = flag;
                }
            }
        }
        return state;
    }

    /**
     * Return the current checked state of the API.
     *
     * @public
     * @returns {boolean} true if the API has been checked, false if not
     */
    function isReady() {
        return CNN._geoCheckReadyEvent;
    }

    return {
        getData: getData,
        inTarget: inTarget,
        isReady: isReady
    };
})(jQuery, document);


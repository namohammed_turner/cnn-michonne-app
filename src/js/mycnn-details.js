/* global CNN */

var MSIB = window.MSIB || {},
    moment = window.moment || {};

/* Define the functions needed for the tab in here */
(function ($) {
    'use strict';

    var currentEmailAuthority = '',
        currentPreferredEmail = '',
        displaySuccess;

    CNN.mycnn.details = {

        show: function () {
            var params;

            if (CNN.mycnn.details.isMSIBValid()) {
                params = {
                    tid: MSIB.util.getTid(),
                    authid: MSIB.util.getAuthid()
                };
                MSIB.msapi.user.get(params, CNN.mycnn.details.getUserDetailsCallback);
            } else {
                CNN.mycnn.updateBody('Error processing request.');
            }
        },

        isMSIBValid: function () {
            if (typeof MSIB.util !== 'undefined' && typeof MSIB.util.getTid !== 'undefined' &&
                typeof MSIB.util.getAuthid !== 'undefined' && typeof MSIB.settings !== 'undefined' &&
                typeof MSIB.settings.appID !== 'undefined' && typeof MSIB.util.getAuthpass !== 'undefined') {
                return true;
            } else {
                return false;
            }
        },

        getUserDetailsCallback: function (result) {

            var output = result.responseJSON;

            if (typeof result.responseJSON !== 'undefined') {

                if (result.responseJSON.preferredEmail !== null && typeof result.responseJSON.preferredEmail !== 'undefined') {
                    currentPreferredEmail = result.responseJSON.preferredEmail;
                }

                if (typeof result.responseJSON.authorities !== 'undefined' && typeof result.responseJSON.authorities.EMAIL !== 'undefined') {
                    currentEmailAuthority = result.responseJSON.authorities.EMAIL;
                    output.showPasswordField = true;
                } else {
                    output.showPasswordField = false;
                }
            }

            window.dust.render('views/cards/tool/mycnn-account-details', output, function (err, out) {
                if (err) {
                    if (window.console && window.console.error) {
                        console.error(err);
                    }
                } else {
                    CNN.mycnn.details.loadAccountDetails(out);
                }
            });

            CNN.mycnn.details.populateDateDropdown(result.responseJSON.birthday);
        },

        /**
         * populate date of birth drop down
         * @param {string} dob (mm/dd/yyyy) - user's date of birth
         */
        populateDateDropdown: function (dob) {

            var today = moment(),
                birthDate,
                monthtext = moment.monthsShort(),
                /*
                 * The year is a prepopulated list of years staring from
                 * today's date - 100 years. Expecting no one over 100 years
                 * of age to sign up for an account
                 */
                thisyear = today.year(),
                birthMonth = $(document.getElementById('birth-month')),
                birthYear = $(document.getElementById('birth-year')),
                option,
                month,
                year,
                m = 0,
                y = 0;

            if (dob !== null && dob.length === 10) {
                birthDate = moment(dob, 'MM/DD/YYYY');
                month = birthDate.month() + 1;
                year = birthDate.year();
            }

            for (m = 0; m < 12; m++) {
                option = new Option(monthtext[m], m + 1);

                if (m + 1 === month) {
                    option.selected = true;
                }
                birthMonth.append(option);
            }

            /* years is 100 years prior to current year */

            for (y = 0; y < 100 ; y++) {
                option = new Option(thisyear, thisyear);

                if (thisyear === year) {
                    option.selected = true;
                }
                birthYear.append(option);
                thisyear -= 1;
            }
        },

        /**
         * updates body content with Account Details tabs
         * @param {string} htmlStr - the initial form html from ajax
         */
        loadAccountDetails: function (htmlStr) {
            CNN.mycnn.updateBody(htmlStr);
            CNN.mycnn.details.registerHandlers();

            if (displaySuccess) {
                displaySuccess = false;
                $('#mycnn-account-details-error').html('Account Details successfully saved.')
                    .attr('class', 'mycnn-details-success').show();
            }
        },

        /* enables cancel button */
        enableCancel: function () {
            $(document.getElementById('mycnn-account-details-cancel')).prop('disabled', false);
        },

        registerHandlers: function () {
            var $fieldSelector = $('.mycnn-details-inner-container .mycnn-details-data');

            $(document.getElementById('mycnn-account-details-save')).click(function () {
                CNN.mycnn.details.handleSave();
            });
            $(document.getElementById('msib-logout')).click(function () {
                CNN.mycnn.user.logout('/');
            });
            $fieldSelector.find('input, select').on('input change', function () {
                CNN.mycnn.details.enableCancel();
            });
            $(document.getElementById('mycnn-account-details-cancel')).click(function () {
                CNN.mycnn.details.show();
            });
        },

        handleSave: function () {

            /* update userinfo */
            var params = {},
                error = '',
                dob = '',
                password = document.getElementById('password') ? $(document.getElementById('password')).val() : '',
                birthMonth = $(document.getElementById('birth-month')).val(),
                birthYear = $(document.getElementById('birth-year')).val(),
                currentPassword = document.getElementById('current-password') ? $(document.getElementById('current-password')).val() : '',
                confirmPassword = document.getElementById('confirm-password') ? $(document.getElementById('confirm-password')).val() : '',
                firstName = $(document.getElementById('first-name')).val(),
                lastName = $(document.getElementById('last-name')).val(),
                preferredEmail = $(document.getElementById('email')).val(),
                zip = $(document.getElementById('zip')).val(),
                gender = $(document.getElementById('gender')).val(),
                errorContainer = $('#mycnn-account-details-error'),
                errorHeader = '<span class="mycnn-account-error__message">Please correct the following errors:</span>',
                errorList = '<li class="mycnn__error-item">';

            errorContainer.attr('class', 'mycnn-details-error').html(errorHeader).hide();

            if (firstName === '') {
                error += errorList + 'First Name is a required field.</li>';
            }

            if (lastName === '') {
                error +=  errorList + 'Last Name is a required field.</li>';
            }

            /* if email is not entered or invalid, force to enter a valid email */
            if (preferredEmail === '' || !CNN.social.isValidEmail(preferredEmail)) {
                error +=  errorList + 'Please provide a valid email address.</li>';
            }

            if (currentPassword && currentPassword.length > 1) {
                if (password === '') {
                    error +=  errorList + 'Please confirm your new password. </li>';
                } else if (password !== confirmPassword) {
                    error +=  errorList + 'Passwords must match. Please try again. </li>';
                }
            } else {
                if ((password !== '' && password.length > 1) || (confirmPassword !== '' && confirmPassword.length > 1)) {
                    error +=  errorList + 'Please enter your current password. </li>';
                }
            }

            if (error.length > 0) {
                errorContainer.append('<ul>' + error + '</ul>').show();
                return;
            }

            if (birthMonth !== '' && birthYear !== '') {
                dob = birthMonth + '/01/' + birthYear;
            }

            params = {
                tid: MSIB.util.getTid(),
                authid: MSIB.util.getAuthid(),
                authpass: MSIB.util.getAuthpass(),
                firstName: firstName,
                lastName: lastName,
                dateOfBirth: dob,
                'address.zip': zip,
                'attributes["gender_cde"]': gender
            };

            /* update password */
            if (typeof currentPassword !== 'undefined' &&
                currentPassword !== null && currentPassword.length > 0 &&
                password === confirmPassword && currentEmailAuthority !== '') {
                params.currentPassword = currentPassword;
                params.password = password;
                params.confirmPassword = confirmPassword;
            }

            /* update email address */
            if (preferredEmail !== currentPreferredEmail) {
                params.emailAddress = preferredEmail;

                /* If the user has an email/password login, then update
                 * that as well. */
                if (currentEmailAuthority !== '') {
                    params.emailAuthority = preferredEmail;
                }
            }

            MSIB.msapi.user.edit(params,
                CNN.mycnn.details.handleResultCallback);
        },

        /**
         * displays error message if the response was not success
         * @param {object} response - msib response
         */
        handleResultCallback: function (response) {

            var errors = [],
                i = 0,
                error = $(document.getElementById('mycnn-account-details-error')),
                errorMessage = error.html(),
                errorList = '<li class="mycnn__error-item">';
            if (errorMessage.indexOf('success') > -1) {
                errorMessage = '';
            }

            if (response) {
                if (response.responseJSON.success) {
                    displaySuccess = true;
                    CNN.mycnn.details.show();
                } else {
                    errors = response.responseJSON.errors;
                    errorMessage = '';
                    for (i = 0; i < errors.length; i++) {
                        /* html encode error message */
                        if (errors[i].error === 'emailInvalidFormat' && errors[i].field === 'emailAddress') {
                            errorMessage += errorList + 'Please provide a valid email address.</li>';
                        } else if (errors[i].error === 'invalidLength' && errors[i].field === 'password') {
                            errorMessage += errorList + 'Password must be between 6-15 characters and is case sensitive. Please enter a new password.</li>';
                        } else {
                            errorMessage += errorList + '' + errors[i].message;
                        }
                    }
                    if (errorMessage) {
                        error.append('<ul>' + errorMessage + '</ul>').show();
                    }
                }
            }
        }
    };

}(jQuery));

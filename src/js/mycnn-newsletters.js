/* global CNN */

var MSIB = window.MSIB || {};

/* Define the functions needed for the tab in here */
(function ($) {
    'use strict';

    var
        newsletterMap = {
            /* THIS SHOULD NOT BE HERE */
            news: [
                {
                    id: 'member_services',
                    name: 'Updates from CNN',
                    description: 'Sign up now to hear about new products, services, and other information from CNN',
                    frequency: 'Daily',
                    state: false
                },
                {
                    id: 'cnn-gutcheck',
                    name: 'CNN Politics Gut Check',
                    description: 'A daily political newsletter aimed at telling you what happened in politics in 5 minutes or less.',
                    frequency: 'Daily',
                    state: false
                },
                {
                    id: 'cnn-sundaysotu',
                    name: 'State of the Union',
                    description:
                    'Get a sneak peek on what to watch this week in politics, in a special Sunday edition of the "Political Ticker" newsletter.',
                    frequency: 'Weekly',
                    state: false
                },
                {
                    id: 'cnn-dailytop10',
                    name: 'Today\'s Top Video',
                    description: 'See the 10 most popular videos on CNN.com each day.',
                    frequency: 'Daily',
                    state: false
                },
                {
                    id: 'cnn-htmlquicknews',
                    name: 'CNN Morning News Update (HTML)',
                    description: 'Start your day with a rundown of the top news stories from CNN.com.',
                    frequency: 'Daily',
                    state: false
                },
                {
                    id: 'cnn-textquicknews',
                    name: 'CNN Morning News Update (Text)',
                    description: 'Start your day with a rundown of the top news stories from CNN.com.',
                    frequency: 'Daily',
                    state: false
                },
                {
                    id: 'intl-insideafrica',
                    name: 'Inside Africa',
                    description: 'An inside look at trends across Africa.',
                    frequency: 'daily',
                    state: false
                },
                {
                    id: 'intl-htmleuheadlines',
                    name: 'CNN Europe Editorial Note',
                    description: 'Daily news headlines from across Europe',
                    frequency: 'Daily',
                    state: false
                },
                {
                    id: 'intl-htmlasiaheadlines',
                    name: 'CNN Asia Editorial Note',
                    description: 'Daily news headlines from across Asia.',
                    frequency: 'Daily',
                    state: false
                },
                {
                    id: 'hln_promotions',
                    name: 'HLN Special Offers & Promotions',
                    description: 'Be among the first to hear about HLN special offers, sweepstakes, contests and promotions',
                    frequency: 'Weekly',
                    state: false
                },
                {
                    id: 'cnn_promotions',
                    name: 'CNN Special Offers & Promotions',
                    description: 'Be among the first to hear about CNN special offers, sweepstakes, contests and promotions.',
                    frequency: 'Weekly',
                    state: false
                }
            ]
        },
        subscriptions = [],
        errorMessage = '';

    CNN.mycnn.newsletters = {
        show: function () {
            /* reset error message */
            errorMessage = '';
            CNN.mycnn.newsletters.getSubscriptions();
        },

        /**
         * Display list of newsletter via dust and if they are subscribed to
         */
        displayNewsLetters: function () {

            var output = {},
                index;

            output.newsletters = newsletterMap;

            /* set newsletter state based on what the user is subscribed to */
            for (index = 0; index < output.newsletters.news.length; index++) {
                if ($.inArray(output.newsletters.news[index].id, subscriptions) === -1) {
                    output.newsletters.news[index].state = false;
                } else {
                    output.newsletters.news[index].state = true;
                }
            }

            window.dust.render('views/cards/tool/mycnn-newsletters', output, function (err, out) {
                if (err) {
                    if (window.console && window.console.error) {
                        console.log(err);
                    }
                } else {
                    CNN.mycnn.updateBody(out);
                    /* check emailid exists */
                    CNN.mycnn.newsletters.checkEmailExists();
                    CNN.mycnn.newsletters.registerHandlers();
                }
            });
        },

        /**
         * Gets the logged in users subscribed news letters
         */
        getSubscriptions: function () {
            var
                params = {
                    tid: MSIB.util.getTid(),
                    authid: MSIB.util.getAuthid()
                };
            MSIB.msapi.news.get(params, CNN.mycnn.newsletters.handleSubscriptionsCallback);
        },

        /**
         * Callback that MSIB will call once getSubscriptions has finished
         * @param {object} result MSIB object result
         */
        handleSubscriptionsCallback: function (result) {

            var activeNewsletters = [],
                newsletters,
                i;

            if (result !== null && result !== 'undefined' && result.responseJSON.newsletters) {
                newsletters = result.responseJSON.newsletters;
                for (i in newsletters) {
                    if (newsletters[i].status === 'active' || newsletters[i].status === 'pending') {
                        activeNewsletters.push(newsletters[i].name);
                    }
                }
            }
            subscriptions = activeNewsletters;

            CNN.mycnn.newsletters.displayNewsLetters();
        },

        /**
         * Set up call back handlers
         */
        registerHandlers: function () {

            $('.mycnn-newsletter-subscribe .button-subscribe').on('click', function (_e) {
                $(this).addClass('hide').siblings('.button-unsubscribe').removeClass('hide');
                CNN.mycnn.newsletters.saveSubscription($(this).attr('name'));
            });

            $('.mycnn-newsletter-subscribe .button-unsubscribe').on('click', function (_e) {
                $(this).addClass('hide').siblings('.button-subscribe').removeClass('hide');
                CNN.mycnn.newsletters.saveSubscription($(this).attr('name'), true);
            });
        },

        /**
         * Save subscription- triggered on button click.
         * @param {string} newsletterName
         * @param {boolean} unsubscribe - optional, passed on unsubscribe button click
         */

        saveSubscription: function (newsletterName, unsubscribe) {
            var appid = MSIB.settings.appID,
                tid = MSIB.util.getTid(),
                authid = MSIB.util.getAuthid(),
                authpass = MSIB.util.getAuthpass(),
                params = {
                    appid: appid,
                    tid: tid,
                    authid: authid,
                    authpass: authpass,
                    name: newsletterName
                };

            if (unsubscribe) {
                MSIB.msapi.news.unsubscribe(params, newsletterName);
            } else {
                MSIB.msapi.news.subscribe(params, newsletterName);
            }
        },

        /**
         * MSIB callback handler
         * @param {String} newsletterName - the newsletter that you have un/subscribed to
         * @returns {function} - function to process response
         */
        handleCallback: function (newsletterName) {
            return function (response) {
                CNN.mycnn.newsletters.processResponse(response, newsletterName);
            };
        },

        /**
         * Check whether email id exists
         */
        checkEmailExists: function () {
            CNN.mycnn.user.reload(function (user) {
                if (user.loggedIn) {
                    if (typeof CNN.mycnn.user === 'object' && typeof CNN.mycnn.user.msib === 'object' && CNN.mycnn.user.msib.preferredEmail === null) {
                        errorMessage = 'An email address is required to subscribe to CNN Newsletters. ' +
                            '<a href="javascript:void(0);" id="mycnn-acc-page">Please add an email address to your CNN profile here.</a>';
                        $(document.getElementById('mycnn-newsletter-message')).html(errorMessage);
                        $('.mycnn-newsletters :input').attr('disabled', true);
                        $(document.getElementById('newsletter-subscribe')).attr('disabled', true);
                        $(document.getElementById('mycnn-acc-page')).click(function () {
                            CNN.mycnn.details.show();
                        });
                    }
                }
            });
        },

        /**
         * Process MSIB response with newsletter
         * @param {object} response - response object
         * @param {string} newsletterName - newsletter name
         */
        processResponse: function (response, newsletterName) {
            var errors = [],
                i = 0;

            if (response && !response.responseJSON.success) {

                errors = response.responseJSON.errors;
                errorMessage += 'Error while processing request for:' + newsletterName + '<br>';

                for (i = 0; i < errors.length; i++) {
                    /* html encode error message */
                    errorMessage += $('<div/>').text(errors[i].message).html() + '<br>';
                }
                $(document.getElementById('mycnn-newsletter-message')).html(errorMessage);
            }
        }
    };
}(jQuery));


/* global CNN */

var MSIB = window.MSIB || {};

(function ($) {
    'use strict';

    var AVATAR_SIZE = 120;

    CNN.mycnn = {
        user: {},

        /* list of the mycnn tool cards to interact with on initialization */
        cards: {
            avatar: {
                jsIdent: '.js-mycnn-avatar'
            },
            leftrail: {
                jsIdent: '.js-mycnn-left-rail'
            },
            bodycontent: {
                jsIdent: '.js-mycnn-body-content'
            }
        },

        /* list of the tabs on the mycnn page */
        tabs: {
            stream: {
                title: 'Your Follows'
            },
            account: {
                title: 'Account Details'
            },
            /* Temporarily comment this out while an issue is tracked down
            updateavatar: {
                title: 'Update Avatar'
            },
            socialsettings: {
                title: 'Social Settings'
            },
            */
            alerts: {
                title: 'Alerts'
            }
            /* Temporarily comment this out while an issue is tracked down
            newsletters: {
                title: 'Newsletters'
            }
            */
        },
        /* list of the tabs with emailaddress on the mycnn page */
        tabsWithEmail: {
            stream: {
                title: 'Your Follows'
            },
            account: {
                title: 'Account Details'
            },
            /* Temporarily comment this out while an issue is tracked down
            updateavatar: {
                title: 'Update Avatar'
            },
            socialsettings: {
                title: 'Social Settings'
            },
            */
            emailaddresses: {
                title: 'Email Addresses'
            },
            alerts: {
                title: 'Alerts'
            }
            /* Temporarily comment this out while an issue is tracked down
            newsletters: {
                title: 'Newsletters'
            }
            */
        },

        /**
         * directs card 'traffic' to the necessary function(s)
         * @param {string} cardType - the card type
         * @param {object} domObject - the document object
         */
        initializeCard: function (cardType, domObject) {
            switch (cardType) {
            case 'avatar' :
                CNN.mycnn.showAvatar(domObject);
                break;
            case 'leftrail' :
                CNN.mycnn.buildLeftRail(domObject);
                break;
            case 'bodycontent' :
                CNN.mycnn.initializeBody();
                break;
            }
        },

        /**
         * builds out avatar card html and adds
         * @param {object} domObj - the document object to add the html to
         */
        showAvatar: function (domObj) {

            if (typeof CNN.mycnn !== 'undefined' && typeof CNN.mycnn.user !== 'undefined' &&
                typeof CNN.mycnn.user.getAvatar !== 'undefined') {

                /* will be custom url based on MSIB data, default image size of 180 */
                CNN.mycnn.user.getAvatar(AVATAR_SIZE).appendTo(domObj);
                $('.mycnn-logout-container').show();
            }
        },

        /**
         * builds out left rail card html and adds
         * @param {object} domObj - the document object to add the html to
         */
        buildLeftRail: function (domObj) {

            var listStr = '',
                select = null,
                tabs = '',
                param = {
                    tid: MSIB.util.getTid(),
                    authid: MSIB.util.getAuthid(),
                    authpass: MSIB.util.getAuthpass()
                },
                emailCount = 0;

            MSIB.msapi.getUserInfo(param, function (result) {
                if (result !== null && result !== 'undefined' && result.responseJSON.emails) {
                    emailCount = result.responseJSON.emails.length;
                }
                if (emailCount <= 1) {
                    tabs = CNN.mycnn.tabs;
                } else {
                    tabs = CNN.mycnn.tabsWithEmail;
                }

                select = $('<div class="select mycnn-nav"><select></select></div>');

                $.each(tabs, function (key, value) {
                    listStr += '<div class="nav-item">';
                    listStr += '<a href="javascript:void(0);" onclick="CNN.mycnn.showTab(\'' + key + '\');">' + value.title + '</a></div>';
                    select.find('select').append('<option data-key="' + key + '">' + value.title + '</option>');
                });

                select.find('select').on('change', function (_e) {
                    CNN.mycnn.showTab($(this.selectedOptions).attr('data-key'));
                });

                $(domObj).html(listStr).append(select);
            });
        },

        /**
         * controls what happens for each tab
         * @param {string} tab - the tab's id string
         */
        showTab: function (tab) {
            /*
             * define what functions, method, and magic to call for each tab,
             * you can define functions for each tab in the corresponding
             * mycnn-{tabname}.js file
             */
            switch (tab) {
            case 'account':
                CNN.mycnn.details.show();
                break;
            case 'updateavatar':
                CNN.mycnn.updateAvatar.show();
                break;
            case 'socialsettings':
                CNN.mycnn.socialsettings.show();
                break;
            case 'emailaddresses':
                CNN.mycnn.emails.show();
                break;
            case 'alerts':
                CNN.mycnn.alerts.show();
                break;
            case 'newsletters':
                CNN.mycnn.newsletters.show();
                break;
            case 'stream':
                CNN.mycnn.stream.show();
                break;
            }
        },

        /**
         * sets the default page to Your Follows
         */
        initializeBody: function () {
            CNN.mycnn.stream.show();
        },

        /**
         * updates body content card with html
         * @param {string} htmlStr - the html to add to the body content
         */
        updateBody: function (htmlStr) {
            $(CNN.mycnn.cards.bodycontent.jsIdent).html(htmlStr);
        },

        /**
         * performs the actions for logged in users
         */
        doLoggedIn: function () {
            /* begin check for different mycnn cards and create their initial states */
            $.each(CNN.mycnn.cards, function (key, value) {
                if (($(value.jsIdent)).length) {
                    ($(value.jsIdent)).each(function () {
                        CNN.mycnn.initializeCard(key, this);
                    });
                }
            });
        },

        /**
         * performs the actions for logged out visitors
         */
        doLoggedOut: function () {
            /* keep this for now to distinguish states until we write the code for logged out users */
            window.location.href = CNN.msib.getLoginUrl();
        },

        /**
         * Initialization function.
         * @param {object} cnnUser - user object returned by MSIB
         */
        initialize: function (cnnUser) {
            /* save user state */
            CNN.mycnn.user = cnnUser;

            /* user is logged in */
            if (cnnUser.loggedIn) {
                CNN.mycnn.doLoggedIn();
            } else {
                CNN.mycnn.doLoggedOut();
            }
        }

    };

    /* add mycnn's init function to be called once MSIB winds up and swings */
    $(function () {
        if (CNN.Utils.existsObject(CNN.msib) && typeof CNN.msib.onInitialized === 'function') {
            CNN.msib.onInitialized(CNN.mycnn.initialize);
        }
    });

}(jQuery));


/* global CNN, gigya */

(function ($) {
    'use strict';

    var params = {},
        /* CnnUser from init */
        user,
        gigyaUser,
        /* Gigya login timestamp from the user's clock.  See isPrincipalExpired(). */
        /* gigyaLoginTs, */
        redirectUrl,
        redirectId,
        /* Functions to execute when the user has successfully logged in. */
        loginActions = [];

    /**
     * Parses a URL with optional query string into a mapping of parameters.
     *
     * This only handles single string values for simplicity.  Multiple
     * parameters with the same name will only map to a one of the values;
     * which of the values is chosen is left undefined.
     *
     * @param {string} url - The full URL.
     * @return {object} The parsed parameters.
     */
    function parseQueryParams(url) {
        var params = {},
            div = url.indexOf('?'),
            qs;

        if (div >= 0) {
            qs = url.substr(div);
            qs.replace(new RegExp('([^?=&]+)(=([^&]*))?', 'g'),
                function ($0, $1, $2, $3) {
                    params[$1] = decodeURIComponent($3);
                });
        }

        return params;
    }

    /**
     * Clear the error banner.
     */
    function clearError() {
        showError('');
        $('.social-error').hide();
    }

    /**
     * Display a plain text error message.
     * @param {string} s - The message to display.
     */
    function showError(s) {
        var errorText = 'Please correct the following errors:';
        $('.social-error').html('<span class="social-error__message">' + errorText + '</span><ul><li class="social-error__error-item">' + s + '</li>');
        $('.social-error').show();
    }

    /**
     * Sanitize a potential redirect URL.
     * @param {string} url - The URL to check.
     * @param {string} def - The default URL.
     * @return {string} The sanitized URL, or the default if the URL is invalid.
     */
    function sanitizeRedirectUrl(url, def) {
        var urlRx = /^https?:\/\/(([-a-z0-9]+\.)*cnn\.com)(:[0-9]+)?\/?/,
            matches;

        if (!url || typeof url !== 'string') {
            return def;
        }

        url = url.replace(/^\s+|\s+$/g, '');
        if (url === '') {
            return def;
        }

        matches = url.match(urlRx);
        if (!matches) {
            return def;
        }

        return url;
    }

    /**
     * Redirect back to the site.
     */
    function redirectBack() {
        window.location.href = redirectUrl;
    }

    /**
     * Update the UI to reflect the current login state.
     * @param {boolean} skipReload - true to just update the panels without
     *                               reloading the data from the server.
     * @param {boolean} registering - true to set the new account register success message
     */
    function updatePanels(skipReload, registering) {
        var loginPanel = $(document.getElementById('msib-login-panel')),
            infoPanel = $(document.getElementById('msib-info-panel'));

        function fillPanels() {
            if (user.loggedIn) {
                if ($(document.getElementById('msib-display-username'))) {
                    $(document.getElementById('msib-display-username')).text(user.displayName);
                }
                if ($(document.getElementById('msib-display-msg'))) {
                    if (registering) {
                        $(document.getElementById('msib-display-msg')).text('Thank you for registering!');
                    } else {
                        $(document.getElementById('msib-display-msg')).text('Welcome Back!');
                    }
                }

                infoPanel.show();

                postLogin(function () {
                    redirectId = window.setTimeout(redirectBack, 2000);
                });
            } else {
                /*
                 * This happens when the user isn't logged-in or there was
                 * a problem retrieving the user's info.
                 * Retrieving the user's info may be a transient problem,
                 * so just act like the user isn't logged in for now.
                 */
                loginPanel.show();
            }
        }

        /* Hide all panels while we look up the user's info. */
        $('.msib-panel').hide();

        if (skipReload) {
            fillPanels();
        } else {
            user.reload(fillPanels);
        }
    }

    /**
     * Perform queued actions for after the user has been logged in.
     * @param {function} callback - The final callback (no params).
     */
    function postLogin(callback) {
        (function next() {
            var action;

            if (loginActions.length === 0) {
                callback();
            } else {
                action = loginActions.shift();
                action(next);
            }
        }());
    }

    function onAccountLoggedIn() {
        /* set livefyre auth token */
        CNN.accounts.setLivefyreToken(false, gigyaUser, updatePanels);
    }

    /**
     * Update the UI to indicate that the user has successfully logged out.
     */
    function onAccountsLoggedOut() {
        $('.msib-panel').hide();
        $(document.getElementById('msib-logout-panel')).show();

        redirectId = window.setTimeout(redirectBack, 2000);
    }

    /**
     * Fired when Gigya successfully logs the user in
     * @param {object} gigyaResponse - See http://developers.gigya.com/020_Client_API/010_Socialize/socialize.login#response
     */
    function onGigyaLoggedIn(gigyaResponse) {
        clearError();

        gigyaUser = gigyaResponse.user || {};
        /* never used: gigyaLoginTs = new Date(); */

        /* verify gigya logged in state and then save in global gigya variable */
        if (gigyaUser.errorCode === 0) {
            gigya.accounts.getAccountInfo({
                callback: function handleBrand(brandResponse) {
                    /* Check if the Turner_Brand field is set to the correct value */
                    if (CNN.Utils.exists(brandResponse.data) && CNN.Utils.exists(brandResponse.data.Turner_Brand) &&
                        brandResponse.data.Turner_Brand === 'CNN') {
                        onAccountLoggedIn();
                    } else {
                        gigya.accounts.setAccountInfo({
                            data: {
                                Turner_Brand: 'CNN'
                            },
                            callback: onAccountLoggedIn
                        });
                    }
                }
            });
        } else {
            /* something funky happened with Gigya */
            window.location.reload();
        }
    }

    /**
     * Perform UI logout actions
     * @returns {boolean} - always false
     */
    function accountsLogout() {
        /* Cancel any pending redirect. */
        window.clearTimeout(redirectId);
        redirectId = undefined;

        clearError();

        gigya.accounts.logout({
            callback: function () {
                CNN.accounts.clearLivefyre();

                gigyaUser = undefined;
                /* never used: gigyaLoginTs = undefined; */

                onAccountsLoggedOut();
            }
        });

        return false;
    }

    /**
     * Perform post-initialization steps.
     * @param {CnnUser} cnnUser - The composite user
     */
    function postInit(cnnUser) {
        user = cnnUser;
        params = parseQueryParams(document.location.href);

        CNN.accounts.loadGigya(postGigyaInit);
    }

    function showGigyaScreen() {
        gigya.accounts.showScreenSet({
            screenSet: 'CNN-RegistrationLogin',
            containerID: 'js-gigya-widget',
            onError: function errorHandler(eventObj) {
                /* screensets aren't loading */
                if (window.console) {
                    console.log('Gigya screenset failed to load', eventObj);
                }
            },
            onAfterSubmit: function afterSubmit(_eventObj) {
            },
            onAfterScreenLoad: function afterScreenLoad(_eventObj) {
            },
            onHide: function hide(eventObj) {
                if (eventObj.reason && eventObj.reason === 'finished') {
                    showGigyaScreen();
                }
            }
        });
    }

    /**
     * Perform post-Gigya-initialization steps.
     * @param {boolean} success - Whether the Gigya init was successful.
     */
    function postGigyaInit(success) {
        if (!(success && CNN.accounts.isLoaded)) {
            /* Gigya failed to load. */
            return;
        }

        /*
         * We must set a global handler for onLogin instead of in showLoginUI.
         * If showLoginUI's authFlow falls back to "redirect" mode, then
         * its onLogin event will never fire.  The global onLogin event will.
         */
        gigya.socialize.addEventHandlers({
            onLogin: onGigyaLoggedIn
        });

        showGigyaScreen();

        /* Sanitize the redirect URL and update the redirect links. */
        redirectUrl = sanitizeRedirectUrl(params.r, '/');
        $(document.getElementById('msib-redirect-link')).prop('href', redirectUrl);
        $(document.getElementById('msib-redirect-link')).prop('href', redirectUrl);

        /* Select the initial action to perform. */
        switch (params.a) {
        case 'follow':
            /*
             * Don't trigger the action if the user is already logged-in to
             * prevent CSRF.
             */
            if (!user.loggedIn && params.topic && CNN.follow && CNN.follow.follow) {
                loginActions.push(function (callback) {
                    CNN.follow.follow(params.topic, function () {
                        callback();
                    });
                });
            }
            updatePanels(true);
            break;
        case 'logout':
            accountsLogout();
            break;
        default:
            updatePanels(true);
        }
    }

    $(function () {
        /* visible the error panel by default */
        $(document.getElementById('msib-initializing-panel')).show();
        $(document).onZonesAndDomReady(function loginDOMReady() {
            if (CNN.Utils.existsObject(CNN.accounts) && typeof CNN.accounts.onInitialized === 'function') {
                CNN.accounts.onInitialized(postInit);
            }
        });
    });

}(jQuery));

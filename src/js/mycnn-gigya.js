/* global CNN, gigya */

(function ($) {
    'use strict';

    var AVATAR_SIZE = 120;

    CNN.mycnn = {
        user: {},

        /* list of the mycnn tool cards to interact with on initialization */
        cards: {
            avatar: {
                jsIdent: '.js-mycnn-avatar'
            },
            leftrail: {
                jsIdent: '.js-mycnn-left-rail'
            },
            bodycontent: {
                jsIdent: '#js-mycnn-body-content'
            }
        },

        /* list of the tabs on the mycnn page */
        tabs: {
            account: {
                title: 'Account Details'
            },
            alerts: {
                title: 'Alerts'
            }
        },

        /**
         * directs card 'traffic' to the necessary function(s)
         * @param {string} cardType - the card type
         * @param {object} domObject - the document object
         */
        initializeCard: function (cardType, domObject) {
            switch (cardType) {
            case 'avatar' :
                CNN.mycnn.showAvatar(domObject);
                break;
            case 'leftrail' :
                CNN.mycnn.buildLeftRail(domObject);
                break;
            case 'bodycontent' :
                CNN.mycnn.initializeBody();
                break;
            }
        },

        /**
         * builds out avatar card html and adds
         * @param {object} domObj - the document object to add the html to
         */
        showAvatar: function (domObj) {
            if (typeof CNN.mycnn !== 'undefined' && typeof CNN.mycnn.user !== 'undefined' &&
                typeof CNN.mycnn.user.getAvatar !== 'undefined') {

                /* will be custom url based on MSIB data, default image size of 180 */
                CNN.mycnn.user.getAvatar(AVATAR_SIZE).appendTo(domObj);
                $('.mycnn-logout-container').show();
                $(document.getElementById('msib-logout')).click(function () {
                    CNN.mycnn.user.logout('/');
                });
            }
        },

        /**
         * builds out left rail card html and adds
         * @param {object} domObj - the document object to add the html to
         */
        buildLeftRail: function (domObj) {
            var listStr = '',
                select = null,
                tabs = CNN.mycnn.tabs;

            select = $('<div class="select mycnn-nav"><select></select></div>');

            $.each(tabs, function (key, value) {
                listStr += '<div class="nav-item">';
                listStr += '<a href="javascript:void(0);" onclick="CNN.mycnn.showTab(\'' + key + '\');">' + value.title + '</a></div>';
                select.find('select').append('<option data-key="' + key + '">' + value.title + '</option>');
            });

            select.find('select').on('change', function (_e) {
                CNN.mycnn.showTab($(this.selectedOptions).attr('data-key'));
            });

            $(domObj).html(listStr).append(select);
        },

        /**
         * controls what happens for each tab
         * @param {string} tab - the tab's id string
         */
        showTab: function (tab) {
            /*
             * define what functions, method, and magic to call for each tab,
             * you can define functions for each tab in the corresponding
             * mycnn-{tabname}.js file
             */
            switch (tab) {
            case 'account' :
                CNN.mycnn.details.show();
                break;
            case 'socialsettings' :
                CNN.mycnn.socialsettings.show();
                break;
            case 'alerts' :
                CNN.mycnn.alerts.show();
                break;
            case 'newsletters' :
                CNN.mycnn.newsletters.show();
                break;
            }
        },

        /**
         * sets the default page to Your Follows
         */
        initializeBody: function () {
            CNN.mycnn.details.show();
        },

        details: {
            show: function (message) {
                var initialShow = true;

                message = message || '';

                gigya.accounts.showScreenSet({
                    screenSet: 'MYCNN-ProfileUpdate',
                    containerID: 'js-mycnn-body-content',
                    onError: function errorHandler(eventObj) {
                        /* screenset failed to load */
                        if (window.console) {
                            console.log('Gigya screenset failed to load', eventObj);
                        }
                    },
                    onBeforeSubmit: function beforeSubmit(_eventObj) {
                        message = '';
                    },
                    onAfterScreenLoad: function afterScreenLoad(_eventObj) {
                        if (message !== '' && initialShow) {
                            $(document.getElementById('js-mycnn-body-content_content')).prepend('<div class="mycnn__details-msg">' + message + '</div>');
                            initialShow = false;
                        }
                    },
                    onAfterSubmit: function afterSubmit(eventObj) {
                        var submitMsg = '';

                        if (eventObj.response && eventObj.response.status &&
                            eventObj.response.status === 'OK') {
                            switch (eventObj.screen) {
                            case 'gigya-update-profile-screen':
                                submitMsg = 'Profile updated';

                                if (eventObj.response.UID && CNN.Utils.exists(CNN.accounts.cfg.livefyre.pingEndpoint)) {
                                    CNN.accounts.pingLivefyre(eventObj.response.UID);
                                }
                                break;
                            case 'gigya-change-password-screen':
                                submitMsg = 'Password changed';
                                break;
                            }
                            CNN.mycnn.details.show(submitMsg);
                        }
                    }
                });
            }
        },

        /**
         * updates body content card with html
         * @param {string} htmlStr - the html to add to the body content
         */
        updateBody: function (htmlStr) {
            $(document.getElementById('js-mycnn-body-content')).html(htmlStr);
        },

        /**
         * performs the actions for logged in users
         */
        doLoggedIn: function () {
            /* begin check for different mycnn cards and create their initial states */
            $.each(CNN.mycnn.cards, function (key, value) {
                if (($(value.jsIdent)).length) {
                    ($(value.jsIdent)).each(function () {
                        CNN.mycnn.initializeCard(key, this);
                    });
                }
            });
        },

        /**
         * performs the actions for logged out visitors
         */
        doLoggedOut: function () {
            /* keep this for now to distinguish states until we write the code for logged out users */
            window.location.href = CNN.accounts.getLoginUrl();
        },

        /**
         * Initialization function.
         * @param {object} cnnUser - user object returned by MSIB
         */
        initialize: function (cnnUser) {
            /* save user state */
            CNN.mycnn.user = cnnUser;

            /* user is logged in */
            if (cnnUser.loggedIn) {
                CNN.mycnn.doLoggedIn();
            } else {
                CNN.mycnn.doLoggedOut();
            }
        }
    };

    /* add mycnn's init function to be called once MSIB winds up and swings */
    $(function () {
        if (CNN.Utils.existsObject(CNN.accounts) && typeof CNN.accounts.onInitialized === 'function') {
            CNN.accounts.onInitialized(CNN.mycnn.initialize);
        }
    });

}(jQuery));


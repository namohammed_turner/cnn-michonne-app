/* global Modernizr, _jsmd, jsmd, fastdom */

var CNN = window.CNN || {},
    ezt = window.ezt || [];  /* For Quantcast */

CNN.Analytics = CNN.Analytics || {};
CNN.contentModel = CNN.contentModel || {};
CNN.Gallery = CNN.Gallery || {};

CNN.Analytics.utils = (function analyticsUtils() {
    'use strict';

    var sections = {
        videos: {
            section: 'videos'
        },
        'watch-cnn': {
            section: 'watch-cnn'
        },
        intl_homepage: {
            section: 'cnn homepage',
            subsection: ''
        },
        homepage: {
            section: 'cnn homepage',
            subsection: ''
        },
        us: {
            section: 'us',
            subsection: ''
        },
        world: {
            section: 'world',
            subsection: ''
        },
        politics: {
            section: 'politics',
            subsection: ''
        },
        tech: {
            section: 'tech',
            subsection: ''
        },
        health: {
            section: 'health',
            subsection: ''
        },
        entertainment: {
            section: 'entertainment',
            subsection: ''
        },
        living: {
            section: 'living',
            subsection: ''
        },
        /* travel sections */
        aviation: {
            section: 'travel',
            subsection: 'aviation'
        },
        destinations: {
            section: 'travel',
            subsection: 'destinations'
        },
        foodanddrink: {
            section: 'travel',
            subsection: 'foodanddrink'
        },
        hotels: {
            section: 'travel',
            subsection: 'hotels'
        },
        intl_travel: {
            section: 'travel',
            subsection: ''
        },
        travel: {
            section: 'travel',
            subsection: ''
        },
        /* world sections */
        china: {
            section: 'world',
            subsection: 'china'
        },
        africa: {
            section: 'world',
            subsection: 'africa'
        },
        americas: {
            section: 'world',
            subsection: 'americas'
        },
        asia: {
            section: 'world',
            subsection: 'asia'
        },
        europe: {
            section: 'world',
            subsection: 'europe'
        },
        middleeast: {
            section: 'world',
            subsection: 'middleeast'
        },
        /* sport sections */
        football: {
            section: 'sport',
            subsection: 'football'
        },
        motorsport: {
            section: 'sport',
            subsection: 'motorsport'
        },
        sailing: {
            section: 'sport',
            subsection: 'sailing'
        },
        golf: {
            section: 'sport',
            subsection: 'golf'
        },
        tennis: {
            section: 'sport',
            subsection: 'tennis'
        },
        horseracing: {
            section: 'sport',
            subsection: 'horseracing'
        },
        equestrian: {
            section: 'sport',
            subsection: 'equestrian'
        },
        skiing: {
            section: 'sport',
            subsection: 'skiing'
        },
        /* style sections */
        architecture: {
            section: 'style',
            subsection: 'architecture'
        },
        arts: {
            section: 'style',
            subsection: 'arts'
        },
        autos: {
            section: 'style',
            subsection: 'autos'
        },
        design: {
            section: 'style',
            subsection: 'design'
        },
        fashion: {
            section: 'style',
            subsection: 'fashion'
        },
        luxury: {
            section: 'style',
            subsection: 'luxury'
        },
        style: {
            section: 'style',
            subsection: ''
        }
    };

    CNN.omniture = CNN.omniture || {};
    /**
     * Converts the width of the window in pixels to the number of columns
     * @returns {string} viewport  - Number of columns based on the window width.
     */
    function gridSize() {
        var winWidth = CNN.CurrentSize.getClientWidth(),
            eightColBreakpoint = 640,
            twelveColBreakpoint = 960,
            fourteenColBreakpoint = 1120,
            viewport = 'no-value-set';

        if (winWidth >= fourteenColBreakpoint) {
            viewport = '14col';
        } else if ((winWidth < fourteenColBreakpoint) && (winWidth >= twelveColBreakpoint)) {
            viewport = '12col';
        } else if ((winWidth < twelveColBreakpoint) && (winWidth >= eightColBreakpoint)) {
            viewport = '8col';
        } else {
            viewport = '4col';
        }
        return viewport;
    }

    /**
     * Renaming template_type terms as specified by Analytic's team.
     * Purposely using this function to change the outgoing template_type
     * right before it is set so that the code above can key on the values
     * that are coming back from Pal Server and not need to be changed everytime
     * Analytics renames a term.
     * @param {string} tmplType the original name for the type of template
     * @return {string} the name of the type of template for analytics purposes
     */
    function renamingTemplateType(tmplType) {
        var renameMap = {};

        renameMap.section = 'section front';
        renameMap.special = 'specials';
        renameMap.videos = 'video';

        if (typeof tmplType === 'string' && tmplType in renameMap) {
            tmplType = renameMap[tmplType];
        }

        if (typeof CNN.contentModel !== 'undefined' &&
            typeof CNN.contentModel.sectionName !== 'undefined' &&
            (CNN.contentModel.sectionName === 'homepage' ||
            CNN.contentModel.sectionName === 'intl_homepage')) {
            tmplType = 'index';
        }

        return tmplType;
    }

    /**
     * Stores former query string data into a cookie
     * @param {string} cookie - Semicolon separated cookie values
     * @param {string} expire - date value for the expiration in UTC Formatted string [optional]
     * @param {string} path - path value [optional]
     */
    function setCookie(cookie, expire, path) {
        var cookies = cookie.split(';'),
            i = 0;

        expire = (typeof expire === 'string' && expire.length > 0) ? ';expires=' + expire : '';
        path = (typeof path === 'string' && path.length > 0) ? ';path=' + path : ';path=/';

        for (; i < cookies.length; i++) {
            document.cookie = cookies[i] + path + expire + ';';
        }

    }

    /**
     * Stores former query string data into a cookie
     * @param {object} event - event of the click handler
     * @param {string} cookie - Semicolon separated cookie values
     */
    function clickEventTracker(event, cookie) {
        if (typeof cookie === 'string'  && cookie.length > 0) {
            setCookie(cookie);
        }

        if (event.currentTarget.attributes.hasOwnProperty('data-vr-track')) {
            window._vrtrack({
                track_url: event.currentTarget.href,
                event_type: 'partner'
            });
        }
    }

    /**
     * Check to see if the Special's URI matches a value set in trinity configs.
     *
     * @param  {String}  uri
     * The URI of the special.
     *
     * @param  {String}  sectionName
     * The name of the section.
     *
     * @return {Boolean}  - Is the Special's URI a in the trinity configs?
     */
    function isSpecialActingAsASectionFront(uri, sectionName) {
        var rollups = (CNN.analyticsConfig && CNN.analyticsConfig.rollups &&
                CNN.analyticsConfig.rollups[sectionName]) || null;

        return (rollups && rollups.uris.indexOf(uri) !== -1) ? true : false;
    }

    /**
     * Adds the CNN.omniture object to the page
     * This is a stub object model for us to add/modify
     */
    function addMetadataObject() {
        var templateType = CNN.contentModel.pageType,
            defaultBlankValue = 'no-value-set',
            friendlyName =  defaultBlankValue,
            viewport = gridSize(),
            firstCarousel = jQuery(CNN.Carousel.carouselSelector).first(),
            galleryName = defaultBlankValue,
            gallerySlide = defaultBlankValue,
            publishDate = defaultBlankValue,
            videoPlayerCount = jQuery('.js-media__video').length,
            environment = CNN.contentModel.env || defaultBlankValue,
            showName = (CNN.contentModel.analytics && CNN.contentModel.analytics.showName) || '', /* For this variable Analytics want a blank string */
            pageHeadline = CNN.contentModel.title || defaultBlankValue,
            dateParts = [],
            cap_author = defaultBlankValue,
            cap_genre = defaultBlankValue,
            cap_franchise = defaultBlankValue,
            cap_media_type = defaultBlankValue,
            cap_topic = defaultBlankValue,
            cap_content_type = defaultBlankValue,
            contentType = 'adbp:none',
            fullGallery = 0,
            brandingContentZone,
            brandingContentContainer,
            brandingContentCard,
            brandingContentPage,
            brandingGallery = '',
            contentId;

        /* Overrides requested by Analytics */
        if (typeof CNN.contentModel === 'object' &&
            typeof CNN.contentModel.pageType === 'string' &&
            typeof CNN.contentModel.sectionName === 'string') {

            if (CNN.contentModel.pageType === 'section') {
                switch (CNN.contentModel.sectionName) {
                case 'homepage':
                case 'intl_homepage':
                    pageHeadline = defaultBlankValue;
                }
            }

            if (CNN.contentModel.pageType === 'special' &&
                CNN.contentModel.sectionName === 'politics' &&
                isSpecialActingAsASectionFront(CNN.contentModel.uri, CNN.contentModel.sectionName)) {

                templateType = 'section';
            }
        }

        /* On Error Pages (i.e. 404 pages), set templateType only */
        if (typeof CNN.contentModel !== 'undefined' &&
            typeof CNN.contentModel.layout !== 'undefined' &&
            CNN.contentModel.layout === 'error') {
            templateType = 'error';
        }

        /* Set gallery info if on gallery/full gallery or an article that has a pageTop gallery */
        if (firstCarousel.length) {
            switch (templateType) {
            case 'article':
                galleryName = firstCarousel.data('galleryname');
                gallerySlide = firstCarousel.find('[data-slidename]').first().data('slidename');
                break;
            case 'gallery':
                galleryName = pageHeadline;
                gallerySlide = firstCarousel.find('[data-slidename]').first().data('slidename');
                fullGallery = 1;
                break;
            }
        }

        if (typeof CNN.contentModel.analytics !== 'undefined') {

            /* login pages, etc don't have publishDates */
            if (typeof CNN.contentModel.analytics.publishDate !== 'undefined') {
                publishDate = CNN.contentModel.analytics.publishDate;
                dateParts = CNN.contentModel.analytics.publishDate.split('T')[0].split('-');
                if (dateParts.length === 3) {
                    publishDate = dateParts[0] + '/' + dateParts[1] + '/' + dateParts[2];
                }
            }

            cap_author = CNN.contentModel.analytics.author || defaultBlankValue;
            cap_genre = CNN.contentModel.analytics.cap_genre || defaultBlankValue;
            cap_franchise = CNN.contentModel.analytics.cap_franchise || defaultBlankValue;
            cap_media_type = CNN.contentModel.analytics.cap_mediaType || defaultBlankValue;
            cap_topic = CNN.contentModel.analytics.cap_topics || defaultBlankValue;
            cap_content_type = CNN.contentModel.pageType || defaultBlankValue;

            brandingContentPage = CNN.contentModel.analytics.branding_content_page || defaultBlankValue;
            brandingContentZone = CNN.contentModel.analytics.branding_content_zone || defaultBlankValue;
            brandingContentContainer = CNN.contentModel.analytics.branding_content_container || defaultBlankValue;
            brandingContentCard = CNN.contentModel.analytics.branding_content_card || defaultBlankValue;

            contentId = CNN.contentModel.analytics.contentId || defaultBlankValue;

            if (cap_content_type === 'gallery' &&
                typeof CNN.contentModel.analytics.branding_content_page === 'string' &&
                CNN.contentModel.analytics.branding_content_page !== 'default') {
                brandingGallery = CNN.contentModel.analytics.branding_content_page;
            }
        }

        jQuery.extend(CNN.omniture, {

            /* Branding values */
            branding_ad_page:           defaultBlankValue,
            branding_ad_zone:           defaultBlankValue,
            branding_ad_container:      defaultBlankValue,
            branding_ad_card:           defaultBlankValue,
            branding_content_page:      brandingContentPage,
            branding_content_zone:      brandingContentZone,
            branding_content_container: brandingContentContainer,
            branding_content_card:      brandingContentCard,
            branding_gallery:           brandingGallery,

            /* CAP values to be populated by CMS Team */
            cap_author:                 cap_author,
            cap_content_type:           cap_content_type,
            cap_genre:                  cap_genre,
            cap_franchise:              cap_franchise,
            cap_media_type:             cap_media_type,
            cap_show_name:              showName,
            cap_topic:                  cap_topic,

            /* Video values to be populated by Video Team */
            video_opportunity:          videoPlayerCount,

            /* Unique ID populated by the CMS*/
            content_id:                 contentId,

            /* Variable left to be set  */
            content_type:               contentType,
            friendly_name:              friendlyName,
            full_gallery:               fullGallery,
            gallery_name:               galleryName,
            gallery_slide:              gallerySlide,
            grid_size:                  viewport,
            headline:                   pageHeadline,
            ireport_assignment:         defaultBlankValue,
            publish_date:               publishDate,
            rs_flag:                    environment,
            section:                    getSectionSubsection(CNN.contentModel),
            template_type:              renamingTemplateType(templateType)
        });

        if (CNN.omniture.template_type === 'error') {
            CNN.omniture.section = ['error', defaultBlankValue];
        }

        /* Fire an event so that we can call track metrics only after the basic page meta data object is ready */
        if (typeof jQuery.fn.triggerAnalyticsMetadataReady === 'function') {
            jQuery(document).triggerAnalyticsMetadataReady();
        } else {
            jQuery(document).trigger('cnn-analytics-metadata-added');
        }
    }

    /**
     * Detects Flash player version and sets to omniture, if flash is enabled in the browser.
     @returns {string} version  - Flash Player version
     **/
    function getFlashVersion() {
        var version,
            axo;
        try {
            try {
                axo = new window.ActiveXObject('ShockwaveFlash.ShockwaveFlash.6');
                try {
                    axo.AllowScriptAccess = 'always';
                } catch (e) {
                    version = '6,0,0';
                }
            } catch (e) {}
            version = new window.ActiveXObject('ShockwaveFlash.ShockwaveFlash').GetVariable('$version').replace(/\D+/g, ',').match(/^,?(.+),?$/)[1];
        } catch (e) {
            /* other browsers */
            try {
                if (navigator.mimeTypes['application/x-shockwave-flash'].enabledPlugin) {
                    version = (navigator.plugins['Shockwave Flash 2.0'] || navigator.plugins['Shockwave Flash']).description.replace(/\D+/g, ',').match(/^,?(.+),?$/)[1];
                }
            } catch (e) {}
        }
        return (typeof version === 'undefined') ? 0 : version;
    }

    CNN.omniture.flashVersion = getFlashVersion();

    /**
     * Section value needs to have a section and subsection as array elements
     * for the onmiture object, this function provides the values Analytics
     * is expecting.  At this point no subsection pages are completed, so the
     * subsections will be done once the pages are added, in the mean time the
     * subsection will be blank as requested.
     *
     * @param {object} contentModel - the contentModel object
     * @returns {array} section  - section and subsection hash
     */
    function getSectionSubsection(contentModel) {
        var section = '',
            subsection = '',
            contentName = '',
            cm = contentModel,
            cma = contentModel.analytics || {};

        if (typeof cm.sectionName !== 'undefined') {
            contentName = cm.sectionName;
        } else if (typeof cma.parentPath !== 'undefined') {
            contentName = (cma.parentPath.indexOf('/') > -1) ? cma.parentPath.split('/')[1] : cma.parentPath;
        } else if (typeof cm.layout !== 'undefined' && cm.layout === 'search') {
            contentName = 'search';
        } else {
            contentName = 'no-value-set';
        }

        if (sections[contentName]) {
            section = sections[contentName].section;
            subsection = sections[contentName].subsection || '';
        } else {
            section = contentName;
            subsection = '';
        }

        if (typeof cma.subSectionName !== 'undefined' && cma.subSectionName !== '') {
            subsection = cma.subSectionName;
        }

        return [section, subsection];
    }

    /**
     * Puts the jsmd script on page
     * We can move this out to a separate vendor file
     */
    function jsmdTracking() {
        var pageURL;

        try {
            pageURL = location.href.toLowerCase();
            window.jsmd = _jsmd.init();

            if (pageURL.indexOf('/.element/ssi/ads.iframes/') === -1 &&
                pageURL.indexOf('/doubleclick/dartiframe.html') === -1 &&
                pageURL.indexOf('/search/') === -1) {

                if (_jsmd.plugin.gQuery('refresh')) {
                    jsmd.trackMetrics('dynamic-autoRefresh', 'autorefresh', 'cnn-autorefresh');
                } else if (CNN.omniture.template_type_content !== 'gallery') {
                    jsmd.send();
                }
            }
        } catch (e) {
            console.log('JSMD/Analytics error: ', e);
        }
    }

    /**
     * Adds tracking tag to the url
     * @param {object} scopeObject - the  dom object which specfies the scope of tracking tag addition
     */
    function addTrackingTags(scopeObject) {
        var headerNavContext = jQuery('.nav--plain-header'),
            footerNavContext = jQuery('footer.l-footer'),
            trackingTagFactory = CNN.Analytics.hpt;

        jQuery(scopeObject).find('a').each(function () {
            var $this = jQuery(this),
                hrefUrl = $this.attr('href'),
                scope;

            if (typeof hrefUrl === 'string') {
                /* This takes care of header links that are not article related */
                scope = 'page';
                if ($this.closest('.nav', headerNavContext).length > 0) {
                    scope = 'header';
                } else if ($this.closest('.nav-flyout__menu-item', headerNavContext).length > 0) {
                    scope = 'header';
                } else if ($this.closest('.l-footer', footerNavContext).length > 0) {
                    scope = 'footer';
                }

                trackingTagFactory.getHptValues($this, scope).done(function haveHptValues(qsValue) {
                    var cookie;

                    if (typeof qsValue === 'object' && hrefUrl.indexOf(qsValue.hptValue) === -1) {
                        cookie = 'hpt=' + qsValue.hptValue + ';' + 'hpt2=' + qsValue.hpt2Value;
                        $this.click(jQuery.proxy(function (cookieStr, event) {
                            CNN.Analytics.utils.clickEventTracker(event, cookieStr);
                        }, this, cookie));
                    } else if (typeof $this.data('vr-track') === 'string' && hrefUrl.length > 1) {
                        $this.click(CNN.Analytics.utils.clickEventTracker);
                    }
                });
            }
        });
    }

    /**
     * adds social media interaction metric tracking.  this is not to be confused with the social sharebar.  that metric
     * tracking is already handled.  this tracks the navigation to the specific social media pages for show/specials/profiles.
     */
    function addSocialMediaInteractionMetrics() {
        var followAnchors = jQuery('a', 'div.metadata-header__follow-icon'),
            whatsappAnchor = jQuery('div.share-bar-whatsapp-container'),
            mainNavFollowAnchors = jQuery('a', 'div.nav-flyout-footer__social');

        followAnchors.click(function () {
            return commonSocialMediaInteractionMetrics(this, false);
        });

        whatsappAnchor.click(function () {
            var whatsappData = document.querySelector('.share-bar-whatsapp-container').dataset,
                whatsappUrl = whatsappData.url + ' ' + whatsappData.title + ' ' + whatsappData.storyurl;

            if (jQuery('html').hasClass('phone')) {
                document.location = whatsappUrl;
            }

            return commonSocialMediaInteractionMetrics(this, false);
        });

        mainNavFollowAnchors.click(function () {
            return commonSocialMediaInteractionMetrics(this, true);
        });
    }

    /**
     * Common function to add mobile menu metric tracking.
     *
     * @param {string} vertical
     * The name of the vertical to track. This value is used to hook into the
     * body class and to send the name of the vertical
     * to analytics. Example: `".pg-politics"` and
     * `{ interation: "politics:menu" }`
     */
    function addVerticalMenuInteractionMetrics(vertical) {
        jQuery('.js-navigation-hamburger', '.pg-' + vertical).click(function () {
            try {
                window.trackMetrics({
                    type: 'user-interaction',
                    data: {
                        interaction: vertical + ':menu'
                    }
                });
            } catch(err) {}
        });
    }

    /**
     * Common function to add social media metric tracking.
     *
     * @param {string} socialMedia
     * Name of the social media that is being clicked. E.g. 'whatsapp'
     * @param {boolean} isMainNavAnchor
     * Is this the main navigation a-tag for this choice
     * @returns {boolean} always true
     */
    function commonSocialMediaInteractionMetrics(socialMedia, isMainNavAnchor) {
        var socialMediaName = socialMedia.getAttribute('data-social-media-name');

        if (typeof window.trackMetrics === 'function') {
            try {
                window.trackMetrics({
                    type: 'social-click',
                    data: {
                        clickObj: {
                            socialType: 'social: ' + socialMediaName,
                            isMainNav: isMainNavAnchor
                        }
                    }
                });
            } catch (e) {
            }
        }
        return true;
    }

    return {
        addVerticalMenuInteractionMetrics: addVerticalMenuInteractionMetrics,
        gridSize: gridSize,
        renamingTemplateType: renamingTemplateType,
        addMetadataObject: addMetadataObject,
        jsmdTracking: jsmdTracking,
        addTrackingTags: addTrackingTags,
        setCookie: setCookie,
        clickEventTracker: clickEventTracker,
        addSocialMediaInteractionMetrics: addSocialMediaInteractionMetrics
    };

}());

/**
 * namespace containing functions to build the hpt tracking tags
 */
CNN.Analytics.hpt = (function getHptAnonymous() {
    'use strict';

    var containers = {
            'list-hierarchical-small-horizontal': 'lead+list:lead+headlines-w/images',
            'list-hierarchical-xs': 'lead+list:lead+headlines',
            'list-large-horizontal': 'list:full-cards',
            'list-large-vertical': 'vertical-strip:full-cards',
            'list-small-horizontal': 'list:headlines+images',
            'list-small-vertical': 'vertical-strip:headlines+images',
            'list-xs': 'headlines',
            'grid-medium': 'grid:grid(spaced)',
            'grid-small': 'grid:grid',
            'stack-large-horizontal': 'list:full-cards(spaced)',
            'stack-large-vertical': 'vertical-strip:full-cards(spaced)',
            'stack-medium-horizontal': 'list:headlines+images(spaced)',
            'stack-medium-vertical': 'vertical-strip:headlines+images(spaced)',
            'carousel-large-strip': 'carousel:large',
            'carousel-medium-matrix': 'carousel:double-deck',
            'carousel-medium-strip': 'carousel:standard',
            'stack-ul': 'unknown name',
            'section-preview': 'unknown name'
        },
        zones = {
            'left-fluid-right-stack': 'hero-3',
            'left-fluid-bg-bleed': 'superhero',
            'left-fluid': 'priority+2',
            'right-stack-bg-inline': 'hero-plus',
            'right-stack-bg-bleed': 'full-bleed',
            staggered: 'staggered',
            balanced: 'balanced',
            '60-40': 'priority+1',
            '30-70': 'grid-right',
            '40-60': 'unknown name',
            '70-30': 'unknown name',
            body: 'unknown name',
            'single-column': 'unknown name',
            'zone-background': 'unknown name',
            'zone-banner': 'unknown name',
            'zone-header': 'unknown name'
        };

    /**
     * Translates the zone template names that the code uses into the proper names for analytics.
     * This is the same name as it is in the CMS3 tools.
     * @param  {string} zoneTemplate the original zone template name that the code uses
     * @return {string}              the zone name that analytics is expecting (what it is in the tools)
     */
    function getEditorialZoneNomenclature(zoneTemplate) {
        var zoneName = '';

        zoneTemplate = (typeof zoneTemplate === 'undefined') ? '' : zoneTemplate.replace(/^zn-/, '');

        if (zones[zoneTemplate]) {
            zoneName = zones[zoneTemplate];
        } else {
            zoneName = 'no-value-set';
        }

        return zoneName;
    }

    /**
     * Translates the container template names that the code uses into the proper names for analytics.
     * This is the same as it is in the CMS3 tools.
     * @param  {string} containerTemplate the original template name that the code uses
     * @return {string}                   the container name that analytics is expecting (what it is in the tools)
     */
    function getEditorialContainerNomenclature(containerTemplate) {
        var containerName = '';

        containerTemplate = (typeof containerTemplate === 'undefined') ? '' : containerTemplate.replace(/^cn-/, '');

        if (containers[containerTemplate]) {
            containerName = containers[containerTemplate];
        } else {
            containerName = 'no-value-set';
        }

        return containerName;
    }

    /**
     * gets attribute values to set up the tracking tag
     * @param  {object} $currElement - the current element
     * @param  {string} scopeflag   - the scope of the item
     * @return {object}             - a list of attributes that are used to build the tracking tags
     */
    function getAttributionValues($currElement, scopeflag) {
        var attributionValues = {},
            defaultBlankValue = 'no-value-set',
            tagAnalytics = defaultBlankValue,
            templateType = CNN.contentModel.pageType,
            sectionName = CNN.contentModel.sectionName || defaultBlankValue,
            pageIndicator = CNN.contentModel.type || defaultBlankValue,
            globalIndicator = defaultBlankValue,
            cardName = defaultBlankValue,
            /* cardType = defaultBlankValue, */
            zoneName = defaultBlankValue,
            zoneType = defaultBlankValue,
            zoneId = defaultBlankValue,
            linkType = defaultBlankValue,
            linkText = 'zone-level',
            containerClasses = [],
            containerLabel = defaultBlankValue,
            containerType = defaultBlankValue,
            containerId = defaultBlankValue,
            elParent = $currElement.closest('[data-analytics]'),
            elZone = $currElement.closest('[class^=\'zn zn-\']'),
            elZoneTop = $currElement.closest('section.zn'),
            elContainerTop = $currElement.closest('.cn'),
            elHeaderAnalytics = $currElement.closest('[data-analytics-header]'),
            level1 = defaultBlankValue,
            level2 = defaultBlankValue,
            level3 = defaultBlankValue,
            levels = [],
            utils = CNN.Analytics.utils,
            dataVal; /* temp variable */

        linkText = ($currElement.text().length > 0) ? $currElement.text().toLowerCase() : 'zone-level';
        if (elZone.length) {
            if (elZone[0].tagName !== 'A' && elZone[0].className) {
                dataVal = elZone[0].className;
                if (typeof dataVal !== 'undefined') {
                    zoneType = (dataVal.split(' ').length > 1) ? dataVal.split(' ')[2] : defaultBlankValue ;
                }
            }
        }

        if (elZoneTop.length) {
            dataVal = elZoneTop.data('zone-label');
            if (typeof dataVal !== 'undefined' && dataVal.length) {
                zoneName = dataVal;
            }

            dataVal = elZoneTop.data('vr-zone');
            if (typeof dataVal !== 'undefined' && dataVal.length) {
                zoneId = dataVal;
            }
        }

        if (elParent.length) {
            dataVal = elParent.data('analytics');
        } else if ($currElement[0].className.indexOf('cd-banner') >= 0) {
            dataVal = $currElement.nextAll('[data-analytics]').data('analytics');
        } else {
            dataVal = undefined;
        }

        if (typeof dataVal !== 'undefined' && dataVal.length) {
            tagAnalytics = dataVal.split('_');
            tagAnalytics = (tagAnalytics.length) ?  tagAnalytics : [];
            containerLabel = tagAnalytics[0].length > 0 ? tagAnalytics[0] : defaultBlankValue;
            containerLabel = containerLabel.replace(/\s/g, '-');
            containerType = tagAnalytics[1];
            /* never used: cardType = (tagAnalytics.length > 2) ? tagAnalytics[2] : defaultBlankValue; */

            if (tagAnalytics.length >= 4 && tagAnalytics[3].length > 0) {
                linkType = tagAnalytics[3];
            } else if ($currElement.text().length === 0 || $currElement.text().indexOf('<img') >= 0) {
                linkType = 'image';
                linkText = 'image';
            } else {
                linkType = 'text';
            }
        }

        if ($currElement.eq(0).text()) {
            cardName = $currElement.eq(0).text();
            cardName = cardName.replace(/[ ]/g, '-');
            if (cardName.indexOf('<img') >= 0) {
                cardName = 'image-file';
            }
        }

        dataVal = elHeaderAnalytics.data('analytics-header');
        if (scopeflag === 'header' && typeof dataVal === 'string' && dataVal.split('_').length) {
            globalIndicator = 'header';
            levels = dataVal.split('_');
            levels = (levels.length) ?  levels : [];
            level1 = (levels[0] === 'main-menu' || levels[0] === 'flyout-menu') ? levels[1].toLowerCase() : defaultBlankValue;
            level2 = (levels[0] === 'flyout-submenu') ? levels[1].toLowerCase() : defaultBlankValue;
            level3 = defaultBlankValue;
        }

        dataVal = elParent.data('analytics');
        if (scopeflag === 'footer' && typeof dataVal === 'string' && dataVal.split('_').length) {
            levels = dataVal.split('_');
            levels = (levels.length) ?  levels : [];
            if (levels.length === 1) {
                globalIndicator = 'footer';
                level1 = levels[0].toLowerCase();
                level2 = defaultBlankValue;
                level3 = defaultBlankValue;
            }
            if (levels.length === 2) {
                globalIndicator = levels[0].toLowerCase();
                level1 = levels[1].toLowerCase();
                level2 = defaultBlankValue;
                level3 = defaultBlankValue;
            }
            if (levels.length === 3) {
                globalIndicator = levels[0].toLowerCase();
                level1 = levels[1].toLowerCase();
                level2 = levels[2].toLowerCase();
                level3 = defaultBlankValue;
            }
        }

        if (scopeflag === 'page' && elContainerTop.length) {
            dataVal = elContainerTop[0].className;
            if (typeof dataVal !== 'undefined') {
                containerClasses = dataVal.split(/\s+/);
                if (containerClasses.length) {
                    containerId = containerClasses[containerClasses.length - 1];
                    containerId = containerId.replace(/_/g, '-');
                }
            }
        }

        attributionValues = {
            linkText: linkText,
            hpt_page_pageIndicator: pageIndicator,
            hpt_page_gridSize: utils.gridSize(),
            hpt_page_section: sectionName,
            hpt_page_template: utils.renamingTemplateType(templateType),
            hpt_page_zoneId: zoneId,
            hpt_page_zoneLabel: zoneName,
            hpt_page_containerId: containerId,
            hpt_page_containerLabel: containerLabel,

            hpt2_page_pageIndicator: pageIndicator,
            hpt2_page_gridSize: utils.gridSize(),
            hpt2_page_section: sectionName,
            hpt2_page_template: utils.renamingTemplateType(templateType),
            hpt2_page_zoneId: zoneId,
            hpt2_page_zoneType: getEditorialZoneNomenclature(zoneType),
            hpt2_page_containerId: containerId,
            hpt2_page_containerType: getEditorialContainerNomenclature(containerType),

            hpt_header_globalIndicator: globalIndicator,
            hpt_header_gridSize: utils.gridSize(),
            hpt_header_section: sectionName,
            hpt_header_template: utils.renamingTemplateType(templateType),
            hpt_header_level1: level1,
            hpt_header_level2: level2,
            hpt_header_level3: level3,
            hpt_header_linkElement: linkText,
            hpt_header_linkType: linkType,

            hpt2_header_globalIndicator: globalIndicator,
            hpt2_header_gridSize: utils.gridSize(),
            hpt2_header_section: sectionName,
            hpt2_header_template: utils.renamingTemplateType(templateType),
            hpt2_header_level1: level1,
            hpt2_header_level2: level2,
            hpt2_header_level3: level3,
            hpt2_header_linkElement: linkText,
            hpt2_header_linkType: linkType
        };

        return attributionValues;
    }

    /**
     * Reads the data-filter-* attribute from the link element to
     * determine if there are any filters that need to be applied to the
     * assembled hpt value.
     *
     * There are three possible filter attributes: data-filter-hpt,
     * data-filter-hpt1 and data-filter-hpt2. The first is a general
     * filter that is applied to both hpt1 and hpt2. The other two are
     * more specific. The general filter is mutually exclusive to the
     * hpt specific ones. This means that if a specific filter it will
     * be used instead of the general filter otherwise it falls back to
     * the general filter. If no filter is found then the hpt value
     * passes though with no changes.
     *
     * The filter itself uses the same format as hpt with underscores to
     * separate out values. If one of the values are to not be touched
     * then don't put anything in the underscore. For example
     *
     *     <a href="..." data-filter-hpt1="_23col">..</a>
     *
     * This would modify the column size porition of the hpt1 value to
     * be 23col but leave everything else intact.
     *
     *     <a href="..." data-filter-hpt="super-page_____custom-zone-type">...</a>
     *
     * This would modify the zone type and global indicator passed by
     * both hpt1 and hpt2 while keeping the rest of the data intact.
     *
     * @private
     *
     * @param {string} type
     * The filter type to use. Can be hpt1 or hpt2.
     *
     * @param {object} $currElement
     * jQuery element of the current link that is being processed.
     *
     * @param {string} hptValue
     * The current hpt value that needs to be filtered.
     *
     * @returns {object}
     * A promise for the value to be filled once the filter has been
     * applied.
     */
    function hptFilter(type, $currElement, hptValue) {
        var promiseMe = jQuery.Deferred();

        fastdom.measure(function getTheAttributes() {
            var generalFilter = $currElement.data('filter-hpt'),
                specificFilter = $currElement.data('filter-' + type),
                hasGeneralFilter = typeof generalFilter === 'string',
                hasSpecificFilter = typeof specificFilter === 'string',
                filter = '',
                hptValues = hptValue.split('_'),
                values,
                i,
                ilen;

            if (hasSpecificFilter || hasGeneralFilter) {
                filter = hasSpecificFilter ? specificFilter : generalFilter;

                values = filter.split('_');
                for (i = 0, ilen = values.length; i < ilen; i += 1) {
                    if (hptValues[i] && values[i] !== '') {
                        hptValues[i] = values[i];
                    }
                }

                hptValue = hptValues.join('_');
            }

            promiseMe.resolve(hptValue);
        });

        return promiseMe.promise();
    }

    /**
     * Creates the object that holds hptValue and hpt2Value.
     * This also will remove any character(s) outside the ascii range
     * from the hpt values.
     *
     * @private
     *
     * @param {string} qsVal
     * The homepage tracking value for hpt1.
     *
     * @param {string} qsVal2
     * The homepage tracking value for hpt2.
     *
     * @returns {object}
     * The object used to hold the homepage tracking encoded values.
     */
    function createTrackingTagObject(qsVal, qsVal2) {
        return {
            hptValue: CNN.Utils.b64Encode(qsVal.replace('\u2026', '')).replace(/\r\n/g, ''),
            hpt2Value: CNN.Utils.b64Encode(qsVal2.replace('\u2026', '')).replace(/\r\n/g, '')
        };
    }

    /**
     * builds the tracking tags from the list of attributes
     * @param  {object} $currElement - the current element
     * @param  {string} scopeflag   - the scope of the element
     * @return {object}             - the two complete tracking tags
     */
    function getHptValues($currElement, scopeflag) {
        var qsVals = getAttributionValues($currElement, scopeflag),
            qsVal = '',
            qsVal2 = '';

        if (scopeflag === 'page') {
            qsVal = qsVals.hpt_page_pageIndicator + '_' +
            qsVals.hpt_page_gridSize + '_' +
            qsVals.hpt_page_section + '_' +
            qsVals.hpt_page_template + '_' +
            qsVals.hpt_page_zoneId + '_' +
            qsVals.hpt_page_zoneLabel + '_' +
            qsVals.hpt_page_containerId + '_' +
            qsVals.hpt_page_containerLabel;

            qsVal2 = qsVals.hpt2_page_pageIndicator + '_' +
            qsVals.hpt2_page_gridSize + '_' +
            qsVals.hpt2_page_section + '_' +
            qsVals.hpt2_page_template + '_' +
            qsVals.hpt2_page_zoneId + '_' +
            qsVals.hpt2_page_zoneType + '_' +
            qsVals.hpt2_page_containerId + '_' +
            qsVals.hpt2_page_containerType;
        } else {
            qsVal = qsVals.hpt_header_globalIndicator + '_' +
            qsVals.hpt_header_gridSize + '_' +
            qsVals.hpt_header_section + '_' +
            qsVals.hpt_header_template + '_' +
            qsVals.hpt_header_level1 + '_' +
            qsVals.hpt_header_level2 + '_' +
            qsVals.hpt_header_linkElement + '_' +
            qsVals.hpt_header_linkType;

            qsVal2 = qsVals.hpt2_header_globalIndicator + '_' +
            qsVals.hpt2_header_gridSize + '_' +
            qsVals.hpt2_header_section + '_' +
            qsVals.hpt2_header_template + '_' +
            qsVals.hpt2_header_level1 + '_' +
            qsVals.hpt2_header_level2 + '_' +
            qsVals.hpt2_header_linkElement + '_' +
            qsVals.hpt2_header_linkType;
        }

        return jQuery.when(hptFilter('hpt1', $currElement, qsVal), hptFilter('hpt2', $currElement, qsVal2)).then(createTrackingTagObject);
    }

    return {
        getHptValues: getHptValues
    };

}());


CNN.Gallery.analytics = (function () {
    'use strict';

    var dataGalleryName = 'galleryname',
        slideItemSelector = '[data-slidename]',
        dataSlideCount = 'slide-count';

    /**
     * Public function to get carousel meta information.
     * @param {string|object} carousel - the object or string selector for the carousel.
     * @param {boolean} isJumbotron - true if this carousel is part of a jumbotron
     * @returns {string} carousel content information object
     */
    function getCarouselInformation(carousel, isJumbotron) {
        var $carousel = jQuery(carousel),
            mediaTypes = {},
            matches = [],
            jumbotronId = '',
            jumbotronDetailsContainer = {},
            /* jumbotronSlideContentType = '', */
            isGallery = false,
            contentTypes = [],
            contentType = '',
            layout = '',
            i = 1;

        isGallery = (typeof $carousel.data('is-gallery') === 'boolean') ? $carousel.data('is-gallery') : false;
        mediaTypes = {video: 0, article: 0, gallery: 0, image: 0, hyperlink: 0, matched: 0};

        jQuery('div.owl-item div[data-analytics]', carousel).each(function () {
            var $this = jQuery(this),
                analytics;

            analytics = $this.data('analytics').split('_');
            contentTypes.push(analytics[2] || '');
            layout = analytics[1];
        });
        contentType = contentTypes.length > 0 ? contentTypes[0] : contentType;

        for (; i < contentTypes.length; i++) {
            if (contentTypes[i] !== contentType) {
                contentType = 'multimedia';
                break;
            }
        }

        /*  Content types are determined manually until something is added to the jumbotron api to make this easier. */
        if (typeof isJumbotron === 'boolean' && isJumbotron) {
            if (carousel.className === 'owl-nav') {
                jumbotronId = carousel.offsetParent.id.split('_')[1];
            } else {
                jumbotronId = carousel.id.split('_')[1];
            }

            jumbotronDetailsContainer = document.getElementById('cn-jumbotron-details-container_' + jumbotronId);

            /* This finds the content type of the big jumbotron slide above the carousel */
            jQuery.each(jQuery(jumbotronDetailsContainer).find('.js-jumbotron-card-wrapper.jumbotron-card-wrapper.active-card'), function (k, activeCard) {

                /* Must check for articles first, sometimes there are video articles
                 which resolves to just article.  Therefore, search for those first.  */
                matches = activeCard.firstChild.className.match(/cd\-\-article/);
                if (matches === null) {
                    matches = activeCard.firstChild.className.match(/cd\-\-(video|image|gallery|hyperlink)/);
                }

                /* never used:
                if (matches !== null) {
                    jumbotronSlideContentType = matches[0].replace('cd--', '');
                }
                */
            });

            /*  inspect each article tag in the detailsContainer to determine the content of each card/slide */
            jQuery.each(jQuery(jumbotronDetailsContainer).find('article.cd'), function (k, articleTag) {

                /* Count the various content types */
                if (articleTag.className.indexOf('cd--video') > -1) {
                    mediaTypes.video++;
                } else if (articleTag.className.indexOf('cd--article') > -1) {
                    mediaTypes.article++;
                } else if (articleTag.className.indexOf('cd--image') > -1) {
                    mediaTypes.image++;
                } else if (articleTag.className.indexOf('cd--gallery') > -1) {
                    mediaTypes.gallery++;
                } else if (articleTag.className.indexOf('cd--hyperlink') > -1) {
                    mediaTypes.hyperlink++;
                }
            });

            if (mediaTypes.video > 0) {
                contentType = 'video';
                mediaTypes.matched++;
            }
            if (mediaTypes.article > 0) {
                contentType = 'article';
                mediaTypes.matched++;
            }
            if (mediaTypes.image > 0) {
                contentType = 'image';
                mediaTypes.matched++;
            }
            if (mediaTypes.hyperlink > 0) {
                contentType = 'hyperlink';
                mediaTypes.matched++;
            }
            if (mediaTypes.gallery > 0) {
                contentType = 'gallery';
                mediaTypes.matched++;
            }
            if (mediaTypes.matched > 1) {
                contentType = 'multimedia';
            }
        }


        contentType = contentType.length > 0 ? 'carousel_' + contentType : contentType;

        return {contentType: contentType, carouselType: layout, isGallery: isGallery};
    }

    /**
     * Public function to get the name of the gallery.
     * @param {string|jQuery-Object} gallery - the object or string selector for the gallery.
     * @returns {string} - The gallery name, if defined.
     */
    function getGalleryName(gallery) {
        return (typeof jQuery(gallery).data(dataGalleryName) !== 'undefined') ? jQuery(gallery).data(dataGalleryName) : '';
    }


    /**
     * Public function to get the name of the slide.
     * @param {string|jQuery-Object} gallery - the object or string selector for the gallery.
     * @returns {string} - The image name of the current slide in the gallery.
     */
    function getGallerySlide(gallery) {
        var slideNumber = -1,
            imageName = '',
            $gallery = jQuery(gallery),
            slideList = $gallery.find(slideItemSelector) || [];

        slideNumber = $gallery.data('active') || '';
        if (jQuery.isNumeric(slideNumber) && slideNumber > 0) {
            imageName = jQuery(slideList[slideNumber - 1]).find('img').attr('src');
            if (typeof imageName !== 'undefined') {
                imageName = imageName.substring(imageName.lastIndexOf('/') + 1);
            } else {
                imageName = '';
            }
        }

        return imageName;
    }


    /**
     * Public Function to get the slide count for the gallery
     * @param {string|object} gallery - the object or string selector for the gallery.
     * @param {boolean} isJumbotron - true if this gallery is part of a jumbotron
     * @returns {number} The current slide count value.
     */
    function getSlideCount(gallery, isJumbotron) {
        var carouselFilmstrip = [],
            slideCount = 0;

        if (isJumbotron) {
            carouselFilmstrip = jQuery(CNN.Jumbotron.Constants.CONST_SMALL_CAROUSEL_SELECTOR);

            carouselFilmstrip.find('.owl-item').each(function (idx, val) {
                if (!jQuery(val).hasClass('cloned')) {
                    slideCount++;
                }
            });

        } else {
            slideCount = (typeof jQuery(gallery).data(dataSlideCount) !== 'undefined') ? jQuery(gallery).data(dataSlideCount) : '' ;
        }

        return slideCount;
    }

    function getCaption(context) {
        var caption = jQuery('.owl-stage', context).find('.owl-item.active .el__resize img:first').attr('alt');
        if (typeof caption !== 'string') {
            return '';
        }
        return caption.replace(/[\u0250-\ue007]/g, '').substr(0, 40);
    }


    /**
     * Public Function to identifiy the specific active slide in the current gallery
     * @param {string|object} gallery - the object or string selector for the gallery.
     * @param {boolean} isJumbotron - true if this gallery is part of a jumbotron.
     * @returns {number} - Current slide number
     */
    function getSlideNumber(gallery, isJumbotron) {
        var carouselFilmstrip = [],
            slideNumber = 0;

        if (isJumbotron) {
            carouselFilmstrip = jQuery(CNN.Jumbotron.Constants.CONST_SMALL_CAROUSEL_SELECTOR);
            carouselFilmstrip.find('.owl-item').each(function (idx, val) {
                if (!jQuery(val).hasClass('cloned')) {
                    slideNumber++;
                    if (jQuery(val).hasClass('active')) {
                        return false;
                    }
                }
            });
        } else {
            slideNumber = getCaption(gallery);
        }
        return slideNumber;
    }

    /**
     * Public function to get the meta data for the carousel or galleries.
     * @param {object} carousel - carousel object
     * @param {boolean} isJumbotron - true if this carousel is part of a jumbotron
     * @returns {object} carousel meta data
     */
    function getCarouselMetaData(carousel, isJumbotron) {
        var metaData = {};

        metaData                = getCarouselInformation(carousel, isJumbotron);
        metaData.galleryName    = getGalleryName(carousel);
        metaData.galleryType    = metaData.isGallery ? 'photo gallery' : 'carousel';
        metaData.imageName      = metaData.galleryType !== 'carousel' ? getGallerySlide(carousel) : '';
        metaData.slideCount     = getSlideCount(carousel, isJumbotron);
        metaData.slideNumber    = getSlideNumber(carousel, isJumbotron);
        if (metaData.galleryType !== 'carousel') {
            metaData.carouselType = '';
        }

        return metaData;
    }

    /**
     * Private function to determine if carousel
     * is a jumbotron.
     * @param {object} trackingObj - collected data about the gallery/carousel.
     * @param {jQuery-event} e - the click event, modified to have isJumbotron property.
     */
    function checkIfJumbotron(trackingObj, e) {
        if (typeof e.isJumbotron !== 'undefined' && e.isJumbotron === true) {
            trackingObj.data.page_view = true;
            trackingObj.data.carousel_type = 'jumbotron';
        }
    }

    /**
     * Private function to track the gallery click.
     * @param {jQuery-event} e - the click event.
     */
    function trackGalleryClick(e) {

        /*
         * Galleries sometimes contain cards instead of images.
         * How do does analytics want to handle that?
         */
        var viewportSize = CNN.CurrentSize.getClientWidth() * CNN.CurrentSize.getClientHeight(),
            minSize = (viewportSize / 2) + 1,
            carouselSize = e.target.clientWidth * e.target.clientHeight,
            carouselMetaData = getCarouselMetaData(e.currentTarget, e.isJumbotron),
            trackingTagFactory = CNN.Analytics.hpt,
            trackgalleryClickFlag = false;

        trackingTagFactory.getHptValues(jQuery(e.currentTarget), 'page').done(function haveHptValues(trackingTags) {
            var
                trackingObj = {
                    type: 'cnngallery-click',
                    data: {
                        page_view: false,
                        gallery_name: carouselMetaData.galleryName,
                        image_name: carouselMetaData.imageName,
                        content_type: carouselMetaData.contentType,
                        carousel_type: carouselMetaData.carouselType,
                        gallery_type: carouselMetaData.galleryType,
                        gallery_slide_count: carouselMetaData.slideCount,
                        gallery_slide: carouselMetaData.slideNumber,
                        hpt: trackingTags.hptValue,
                        hpt2: trackingTags.hpt2Value
                    }
                };

            /* Test to indicate whether the carousel is larger than 50% of the page width. */
            trackingObj.data.page_view = carouselSize >= minSize ? true : false;

            /* Test to indicate whether the gallery item is clicked */
            if (e.type === 'carousel-item-clicked') {
                trackingObj.type = 'user-interaction';
                trackingObj.data.interaction = 'video carousel';
                trackgalleryClickFlag = true;
                window.trackMetrics(trackingObj);
            }

            checkIfJumbotron(trackingObj, e);

            CNN.ads.events.refreshGalleryRailAd(e);
            if (window.trackMetrics !== null && window.trackMetrics !== undefined && e.type === 'carousel-action-event') {
                window.trackMetrics(trackingObj);
            }

            /* Test to indicate whether the gallery item is clicked and if user-interaction callback has been sent. */
            if (e.type === 'carousel-item-clicked' && trackgalleryClickFlag === true && window.trackMetrics !== null && window.trackMetrics !== undefined) {
                trackingObj.type = 'cnngallery-click';
                window.trackMetrics(trackingObj);
            }

        });
    }


    /**
     * Public function to initiliaze the omniture click tracking for all galleries.
     * @param {DOM-Element} context - the scope of the init function
     */
    function init(context) {
        jQuery(CNN.Carousel.carouselSelector, context).off('carousel-action-event').on('carousel-action-event', trackGalleryClick);
        jQuery(CNN.Carousel.carouselSelector, context).off('carousel-item-clicked').on('carousel-item-clicked', trackGalleryClick);
    }

    /**
     * Public function that scans the page for all jumbotrons and then registers
     * listeners for events that are from clicking the navigation or swiping
     * the filmstrip. When an event is triggered some analytics metadata is collected
     * and added to the omniture object and sent out via JSMD.
     */
    function initJumbotronTracking() {
        var jumbotrons,
            jumbotronHandler,
            i = 0;

        try {
            jumbotronHandler = function (e, jumbotronActionEvent) {
                if (typeof jumbotronActionEvent !== 'undefined' &&
                    jumbotronActionEvent.actionType === CNN.Jumbotron.Constants.CONST_EVENT_CAROUSEL_ACTION_TYPE_CLICK) {
                    e.isJumbotron = true;
                    trackGalleryClick(e);
                } else if (e.type === 'click' || e.type === 'changed' || e.type === 'dragged') {
                    e.isJumbotron = true;
                    trackGalleryClick(e);
                }
            };

            if (window.cnnJumbotronManager && window.cnnJumbotronManager.getJumbotrons) {

                jumbotrons = window.cnnJumbotronManager.getJumbotrons();

                for (; i < jumbotrons.length; i++) {
                    if (jumbotrons[i] instanceof CNN.Jumbotron.Container &&
                        typeof jumbotrons[i].jumbotronElementRaw !== 'undefined') {

                        jQuery(jumbotrons[i].jumbotronElementRaw).on('carousel-action-event', jumbotronHandler);

                        /* Used to listen to track a navigation within the jumbotron */
                        /* For touch, listen to the owl change. For non-touch, listen for a nav arrow click */
                        if (Modernizr.touchevents) {
                            jQuery(jumbotrons[i].jumbotronElementRaw).find('.cn-jumbotron-card-details').on(CNN.Carousel.events.changed, jumbotronHandler);
                        } else {
                            jQuery(jumbotrons[i].jumbotronElementRaw).find('.owl-nav').on('click', jumbotronHandler);
                            jQuery(jumbotrons[i].jumbotronElementRaw).find('.jumbotron-small-carousel').on(CNN.Carousel.events.dragged, jumbotronHandler);
                        }


                    }
                }
            }
        } catch (err) {
            console.log('Error caught in initJumbotronTracking: %s', err.message);
        }
    }

    return {
        init: init,
        initJumbotronTracking: initJumbotronTracking,
        trackGalleryClick: trackGalleryClick
    };
}());


/**
 * Adds Quantcast values for the page
 *
 * @param {string} edition - Either 'domestic' or 'international'
 */
function initQuantcastValues(edition) {
    'use strict';

    var analObj,
        getAuthors,
        nvs = 'no value set',
        page,
        partner,
        section,
        section2,
        topLevel;

    try {
        analObj = _jsmd.init().mdata;
    } catch (e) {
        console.log(e);
    }
    if (edition === 'domestic') {
        topLevel = 'CNN Domestic';
    } else {
        topLevel = 'CNN International';
    }
    try {
        section = (analObj.business.cnn.page.section[0] || nvs).toLowerCase();
    } catch (e) {
        section = 'QC Tag Error';
    }
    try {
        section2 = ((analObj.business.cnn.page.section[1] || '').split(':')[1] || nvs).toLowerCase();
    } catch (e) {
        section2 = 'QC Tag Error';
    }
    try {
        partner = (analObj.business.cnn.page.branding_content_partner.replace(/^.:/, '') || nvs).toLowerCase();
    } catch (e) {
        partner = 'QC Tag Error';
    }

    page = location.pathname.replace(/(?:index)?\..*$/, '').toLowerCase();

    getAuthors = function () {
        var author = '',
            authors,
            authors2,
            count = 0,
            i;

        try {
            authors = analObj.business.cnn.page.author;
            if (!authors) {
                return ',By Author>Section>Sub-Section>Page.no value set.' + section + '.' + section2 + '.' + page;
            }
            authors2 = authors.replace(/[^a-zA-Z]and[^a-zA-Z]|([^,;]+)?([^a-zA-Z]|^)by[^a-zA-Z]|;|&/g, ',').replace(/,[^,]*cnn[^,]*/g, '').replace(/\./g, '').split(',');
            for (i = 0; i < authors2.length; i++) {
                if (authors2[i].match(/[a-zA-Z]/)) {
                    author += ',By Author>Section>Sub-Section>Page.' + authors2[i].trim().toLowerCase() + '.' +
                        section + '.' + section2 + '.' + page;
                    count++;
                }
            }
            if (count === 0) {
                return ',By Author>Section>Sub-Section>Page.' + authors + '.' + section + '.' + section2 + '.' + page;
            }
            return author;
        } catch (e) {
            return ',By Author>Section>Sub-Section>Page.QC Tag Error.' + section + '.' + section2 + '.' + page;
        }
    };

    ezt.push({
        qacct: 'p-D1yc5zQgjmqr5',
        labels: 'By Network>Section>Sub-Section>Page.' + topLevel + '.' + section + '.' + section2 + '.' +
            page + getAuthors() + ',By Branding Partner>Network>Section.' + partner + '.' + topLevel +
            '.' + section + ',By Section>Network.' + section + '.' + topLevel,
        uid: '',
        event: 'refresh'
    });
}


/**
 * adds analytics to twitter widget tweet button
 *
 * @param {object} evt - event object
 */
function twttrWidgetTweetButtonClickHandler(evt) {
    'use strict';

    if (!evt) { return; }

    if (typeof window.trackMetrics === 'function') {
        try {
            window.trackMetrics({
                type: 'social-click',
                data: {
                    clickObj: {socialType: 'social: twitter'}
                }
            });
        } catch (e) {}
    }
}

/**
 * Initialize analytics subsystems and values
 */
function initAnalytics() {
    'use strict';

    var utils = CNN.Analytics.utils;

    /* Adding the cnn_metadata object */
    utils.addMetadataObject();

    /* Adding jsmd scipt */
    utils.jsmdTracking();

    /* Adding tracking tags to pre-loaded modules */
    utils.addTrackingTags(document);

    utils.addSocialMediaInteractionMetrics();
    utils.addVerticalMenuInteractionMetrics('politics');

    /* Adding carousel/gallery tracking */
    CNN.Gallery.analytics.init(document);

    /* Adding jumbotron tracking */
    jQuery(document).on('jumbotron-initialization-complete', CNN.Gallery.analytics.initJumbotronTracking);

    /* Videx CNNGo logo interaction tracking */
    videoLeafGoInteraction();

    /* Add Quantcast Values */
    initQuantcastValues(CNN.contentModel.edition || 'domestic');
}


/**
 * adds user interaction tracking to the featured show cnngo logo
 */
function videoLeafGoInteraction() {
    'use strict';

    var logo;

    if (CNN.contentModel.pageType === 'video') {
        logo = jQuery('.cnngo-logo');

        if (typeof window.trackMetrics === 'function' && logo.length > 0) {
            /* if the aux value for the featured show is not set then the anchor will not exist */
            logo.closest('a').on('click', function () {
                try {
                    window.trackMetrics({
                        type: 'user-interaction',
                        data: {
                            interaction: 'videoleaf:cnngo'
                        }
                    });
                } catch (e) {}
            });
        }
    }
}


/**
 * Adds the cnn_metadata object and analytics tracking for elements on the page.
 * Includes tracking parameters for pre-loaded modules as well as lazy-loaded ones.
 */
if (typeof CNN.contentModel === 'object' && CNN.contentModel.lazyLoad) {
    jQuery(document).onZonesAndDomReady(function zonesAndDomReadyForAnalytics(_event) {
        'use strict';

        initAnalytics();
    });

    if (CNN.contentModel.enableIntelligentLoad) {
        jQuery(document).on('onIZSecondaryLoad', function intelligentZoneForAnalytics(event, zoneId) {
            'use strict';

            if (typeof zoneId === 'string') {
                CNN.Analytics.utils.addTrackingTags('#' + zoneId);
                CNN.Gallery.analytics.init('#' + zoneId);
            }
        });
    }
} else {
    jQuery(document).ready(function domReadyForAnalytics() {
        'use strict';

        initAnalytics();
    });
}

/**
 * twitter widgets are delivered via postmessage
 * handles tweet button clicks on load
 * twitter widgets are called via webtag
 */
jQuery(window).on('load', function () {
    'use strict';

    if (window.hasOwnProperty('twttr') && window.twttr.hasOwnProperty('widgets')) {
        window.twttr.ready(function handleTwitterWidgetButtonClick() {
            window.twttr.events.bind('click', twttrWidgetTweetButtonClickHandler);
        });
    }
});

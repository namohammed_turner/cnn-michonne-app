/* global CNN, fastdom, Modernizr */

(function createAnimationFeature(doc, ns, jQuery, Modernizr) {
    'use strict';

    ns.ANIMATIONS = ns.ANIMATIONS || {};

    /**
     * Determines if the animation was passed with a specific cut
     *
     * @param {string} cut - The cut being checked for.
     * @param {object} cuts - The list of cuts.
     *
     * @return {boolen} true if the cut exists and false if it doesn't
     */
    function cutExists(cut, cuts) {
        return (typeof cuts[cut] === 'string' && cuts[cut].length > 0);
    }

    /**
     * Sets the source and the sets up iphones/ipads for inline auto playing
     *
     * @param {object}   animationConfig - Describes the animation object.
     *
     */
    ns.ANIMATIONS.loadAnimation = function (animationConfig) {
        if (Modernizr.ios) {
            ns.ANIMATIONS.iphoneInlineAutoplay(animationConfig.animationId);
        }
        if (animationConfig.cuts) {
            if (animationConfig.pageType === 'article' && !animationConfig.isBackground) {
                if (Modernizr.phone && cutExists('dataSrcPhonePageTop', animationConfig.cuts)) {
                    fastdom.mutate(
                        function () {
                            jQuery(doc.getElementById(animationConfig.animationId)).html('<source src="' + animationConfig.cuts.dataSrcPhonePageTop + '" type="video/mp4"></source>');
                        }
                    );
                } else {
                    if (animationConfig.isPageTop === true) {
                        if (animationConfig.appearance === 'standard' && cutExists('dataSrcMedium', animationConfig.cuts)) {
                            fastdom.mutate(
                                function () {
                                    jQuery(doc.getElementById(animationConfig.animationId)).html('<source src="' + animationConfig.cuts.dataSrcMedium + '" type="video/mp4"></source>');
                                }
                            );
                        } else if (animationConfig.appearance === 'fullwidth' && cutExists('dataSrcLarge', animationConfig.cuts)) {
                            fastdom.mutate(
                                function () {
                                    jQuery(doc.getElementById(animationConfig.animationId)).html('<source src="' + animationConfig.cuts.dataSrcLarge + '" type="video/mp4"></source>');
                                }
                            );
                        }
                    } else if (animationConfig.appearance === 'fullwidth' && cutExists('dataSrcLarge', animationConfig.cuts)) {
                        fastdom.mutate(
                            function () {
                                jQuery(doc.getElementById(animationConfig.animationId)).html('<source src="' + animationConfig.cuts.dataSrcLarge + '" type="video/mp4"></source>');
                            }
                        );
                    } else if ((animationConfig.appearance === 'standard' || animationConfig.appearance === 'expandable') && cutExists('dataSrcXsmall', animationConfig.cuts)) {
                        fastdom.mutate(
                            function () {
                                jQuery(doc.getElementById(animationConfig.animationId)).html('<source src="' + animationConfig.cuts.dataSrcXsmall + '" type="video/mp4"></source>');
                            }
                        );
                    }
                }
            } else if (animationConfig.pageType === 'section' || animationConfig.pageType === 'special') {
                fastdom.measure(
                    function measureForSectionAnimation() {
                        var animationContainerWidth = jQuery(doc.getElementById(animationConfig.animationId)).width(),
                            sizedUrl = animationConfig.url.replace('org', 'w_' + animationContainerWidth);
                        fastdom.mutate(
                            function writeSectionAnimation() {
                                jQuery(doc.getElementById(animationConfig.animationId)).wrap('<a href="' + animationConfig.targetUrl + '"></a>');
                                jQuery(doc.getElementById(animationConfig.animationId)).html('<source src="' + sizedUrl + '" type="video/mp4"></source>');
                            }
                        );
                    }
                );
            } else if (animationConfig.isBackground) {
                fastdom.mutate(
                    function () {
                        jQuery(doc.getElementById(animationConfig.animationId)).html('<source src="' + animationConfig.cuts.dataSrcFull16x9 + '" type="video/mp4"></source>');
                    }
                );
            }
        }
    };

    /**
     * Updates the video tag for iphones and ipads to be able to auto play
     * and play inline.
     *
     * @param {string}   videoId - Id of the video that should be updted.
     *
     */
    ns.ANIMATIONS.iphoneInlineAutoplay = function (videoId) {
        fastdom.mutate(
            function () {
                var video = jQuery(doc.getElementById(videoId))[0];
                ns.makeVideoPlayableInline(video, !video.hasAttribute('muted'), false);
            }
        );
    };
})(document, CNN, jQuery, Modernizr);

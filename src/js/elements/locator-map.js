/* global CNN, nokia */

/* http://cnnpreview.cnn.com:82/web.projects/ngarcha/tools/locator.map */

(function ($, window, document) {
    'use strict';

    var mapConfig = {
        version: '2.5.3',
        markers: {
            normal: {
                image: CNN.Host.assetPath + '/assets/locator-map-marker.png',
                size: [13.5, 45]
            },
            small: {
                image: CNN.Host.assetPath + '/assets/locator-map-marker-small.png',
                size: [5.5, 18]
            }
        }
    };

    function _injectMinimap() {
        return $('<div />', {
            'class': 'el-embed-locator-minimap__content'
        }).appendTo(mapConfig.$mapCanvas)[0];
    }

    function _loadMapAssets() {
        /* Load in nokia SDK if it isn't already on the page from another map */
        if (typeof window.nokia === 'undefined') {
            $.getScript('//js.api.here.com/se/' + mapConfig.version + '/jsl.js?blank=true', function () {
                nokia.Features.load(nokia.Features.getFeaturesFromMatrix(['maps']), _initMap, null, null, false);
            });
        }
    }

    function _initMap() {
        var map,
            minimap,
            _setOverview,
            _addMarkers;

        nokia.Settings.set('appId', 'r2jct5woc6zEmY6u8zP3');
        nokia.Settings.set('authenticationToken', 'VbnjVcoJ7JRefKXHYCKV_w');

        map = new nokia.maps.map.Display(mapConfig.$mapCanvas[0], {
            zoomLevel: mapConfig.data.zoom,
            center: [mapConfig.data.latitude, mapConfig.data.longitude],
            baseMapType: nokia.maps.map.Display[mapConfig.data.type],
            components: mapConfig.data.components ? $.map(mapConfig.data.components, function (componentName) {
                return new nokia.maps.map.component[componentName]();
            }) : []     /* Allows the setting for defaults */
        });

        _addMarkers = function (targetMap, markerConfig) {
            var markerIndex = 0,
                markersCount = mapConfig.data.markers.length;

            for (; markerIndex < markersCount; markerIndex++) {
                targetMap.objects.add(new nokia.maps.map.Marker(mapConfig.data.markers[markerIndex], {
                    icon: markerConfig.image,
                    anchor: new nokia.maps.util.Point(markerConfig.size[0], markerConfig.size[1])
                }));
            }
        };

        /* As these are injected mainly on story pages don't let scrolling
         * these often long pages interfere/get caught by the map
         */
        if (mapConfig.data.disableScrollWheel) {
            map.removeComponent(map.getComponentById('zoom.MouseWheel'));
        }

        if (mapConfig.data.markers) {
            _addMarkers(map, mapConfig.markers.normal);
        }

        /* This config key is our hook to determine if the map is
         * utilising a minimap overlay ontop of the original map
         */
        if (mapConfig.data.zoomDiff) {
            _setOverview = function (obj, key, value) {
                if (key === 'center') {
                    minimap.set(key, value);
                } else {
                    value = (value - mapConfig.data.zoomDiff >= minimap.minZoomLevel) ? value - mapConfig.data.zoomDiff : minimap.minZoomLevel;
                    minimap.set(key, value);
                }
            };

            minimap = new nokia.maps.map.Display(_injectMinimap(), {
                zoomLevel: mapConfig.data.zoom - mapConfig.data.zoomDiff,
                center: map.center
            });

            map.set('observers', {
                center: _setOverview,
                zoomLevel: _setOverview
            });

            if (mapConfig.data.markers) {
                _addMarkers(minimap, mapConfig.markers.small);
            }
        }
    }

    /**
     * Public function to initialise the creation of a map embed
     * @param {object} data - config object used to setup map
     *
     * @author Nav Garcha <nav.garcha@turner.com>
     */
    CNN.LocatorMap = function (data) {
        if (typeof data !== 'undefined' && typeof data.id !== 'undefined') {
            mapConfig.data = data;
            mapConfig.$mapCanvas = $(document.getElementById('mapToolCanvas-' + mapConfig.data.id));

            _loadMapAssets();
        }
    };

}(jQuery, window, document));

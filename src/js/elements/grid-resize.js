/* global CNN, Modernizr */

/*
 * Adjusts height of cards to match the tallest card
 * Used for grids and double-deck carousel presently
 */

(function ($j, win) {
    'use strict';

    CNN.GridResize = {
        /**
         * Adjusts height of cards by finding the largest card height
         * and setting it for other cards in the given grid/dom
         *
         * @param {object} grid - The dom object to operate on.
         */
        fixHeight: function fixHeight(grid) {
            var $gridN = $j(grid),
                colNum = Math.round($gridN.eq(0).width() / $j('.cn__column', $gridN).eq(0).width()),
                rowNum = Math.ceil($gridN.find('.cn__column').length / colNum),
                len = colNum * rowNum,
                maxHeightArr = new Array(rowNum),
                i,
                j,
                mhIndx,
                $setHt,
                $cnt,
                cdHeight,
                cntHeight,
                max = 0,
                $cards;

            if ($gridN.attr('class').indexOf('carousel') !== -1) { return; }

            if ($gridN.hasClass('cn-grid') && !$gridN.hasClass('cn-grid-add-carousel')) {

                /* find tallest .cd--card in a row */
                for (i = 0; i < len; i++) {
                    mhIndx = Math.floor(i / colNum);
                    cdHeight = $gridN.find('.cd').eq(i).height();
                    if (maxHeightArr[mhIndx] === undefined || maxHeightArr[mhIndx] === null || cdHeight > maxHeightArr[mhIndx]) {
                        maxHeightArr[mhIndx] = cdHeight;
                    }
                }
                /* adjust cd__content height for each child in a row */
                for (j = 0; j < len; j++) {
                    $setHt = $gridN.find('.cd').eq(j);
                    cdHeight = $setHt.height();
                    $cnt = $j('.cd__content', $setHt);
                    cntHeight = $cnt.height();
                    $cnt.height((cntHeight + (maxHeightArr[Math.floor(j / colNum)] - cdHeight)) + 'px');
                }
            } else {

                /* find tallest .cd--card */
                $cards = $j('.cd', grid).each(function () {
                    var height = $j(this).height();
                    if (height > max) {
                        max = height;
                    }
                });

                /* adjust cd__content height for each child */
                $cards.each(function () {
                    var $this = $j(this),
                        height = $this.height(),
                        $content = $j('.cd__content', $this),
                        contentHeight = $content.height();

                    $content.height((contentHeight + (max - height)) + 'px');

                });
            }
        },

        /**
         * Adjusts height of cards by finding the largest card height
         *
         * @param {object} $grids - The dom object to operate on.
         */
        fixGridHeights: function fixGridHeights($grids) {
            var i,
                len;

            if ($grids) {
                len = $grids.length;
                for (i = 0; i < len; i++) {
                    /* reset height so that we can find the tallest */
                    $grids.eq(i).find('.cd__content').css('height', 'auto');
                    CNN.GridResize.fixHeight($grids[i]);
                }
            }
        }
    };

    if (!Modernizr.flexbox) {
        $j(win).load(function () { CNN.GridResize.fixGridHeights($j('.cn-grid')); });
        $j(win).throttleEvent('resize', CNN.GridResize.fixGridHeights, 100, $j('.cn-grid'));
    }
})(jQuery, window);


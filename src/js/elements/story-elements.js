/* global CNN, fastdom, Modernizr, FAVE */

jQuery(document).onZonesAndDomReady(function () {
    'use strict';

    fastdom.measure(function () {
        var galleryWidth,
            imgWidth = jQuery('.js__image--expandable').width(),
            resizedWin = false,
            teaseImgDelay = 700,
            videoWidth,
            galleryArrowHeight = 30,
            isDesktop = (Modernizr && !Modernizr.phone && !Modernizr.mobile && !Modernizr.tablet) ? true : false,
            isMobile = jQuery('html.mobile').length !== 0 ? true : false,
            $window = jQuery(window),
            initialLoad = true,
            closeHeight = '36px',
            exists = CNN.Utils.exists;

        CNN.contentModel = CNN.contentModel || {};
        CNN.CurrentSize = CNN.CurrentSize || {};
        CNN.CurrentSize.getClientWidth = CNN.CurrentSize.getClientWidth || jQuery.noop;
        CNN.minWidthForExpandElements = 480;
        CNN.showGalleryCaptionWidth = 640;
        CNN.orientationLandscape = false;
        CNN.adLockingDisableWidth = 960;
        CNN.adLockEnable = (exists(CNN.AdsConfig) && CNN.AdsConfig.enableAdLock === true &&
            CNN.CurrentSize.getClientWidth() >= CNN.adLockingDisableWidth &&
            jQuery('.pg-rail.pg-rail-tall__rail').length !== 0 && jQuery('.pg-rail-tall__head').length !== 0) ? true : false;

        /**
         * Initializes an event handler for the given selector
         *
         * @param {string} selector - selector to attach the event handler to
         * @param {string} eventType - the type of event
         * @param {function} callback - callback function when event is triggered
         */
        function setupEventHandler(selector, eventType, callback) {
            /*
             * 'off' needs to be called first to remove any existing
             * handlers to prevent re-registering when window is resized
             */
            fastdom.mutate(function () {
                jQuery(selector).off(eventType).on(eventType, callback);
            });
        }

        /* Image expand */
        function initExpandImages() {
            if (CNN.CurrentSize.getClientWidth() > CNN.minWidthForExpandElements) {
                setupEventHandler('.js__image--expandable', 'click', function () {
                    var $this = jQuery(this);

                    fastdom.mutate(function () {
                        var $wrapper = $this.parents('.el__embedded');

                        $wrapper.find('.el__image__close--expandable').show();
                        $this.stop().animate({
                            width: '100%',
                            'margin-top': closeHeight,
                            'margin-right': 0
                        }, teaseImgDelay, function () {
                            fastdom.mutate(function () {
                                CNN.ResponsiveImages.processChildrenImages(this);
                                $wrapper.addClass('el__embedded--open').height($this.outerHeight());
                            }.bind(this));
                        }).show().addClass('el__image--expandfull').removeClass('el__image--expandable');
                    });
                });
                setupEventHandler('.js__image__close--expandable', 'click', function (event) {
                    var $this = jQuery(this),
                        $wrapper = $this.parents('.el__embedded');

                    fastdom.mutate(function () {
                        $wrapper.removeClass('el__embedded--open').css('height', 'auto');
                        $this.hide();
                        $wrapper.find('.js__image--expandable').stop().animate({
                            width: imgWidth,
                            'margin-top': '10px',
                            'margin-right': '30px'
                        }, teaseImgDelay, function () {
                            fastdom.mutate(function () {
                                CNN.ResponsiveImages.processChildrenImages(this);
                            }.bind(this));
                        }).show().addClass('el__image--expandable').removeClass('el__image--expandfull');
                    });
                    event.stopPropagation();
                });
                fastdom.mutate(function initShowImage() {
                    jQuery('.el__image--expandable .el__image__expand--expandable').show();
                    jQuery('.el__image--expandfull .el__image__close--expandable').show();
                });
            } else {
                fastdom.mutate(function initMobileSizeImage() {
                    jQuery('.el__image__close--expandable').hide();
                    jQuery('.el__image__expand--expandable').hide();
                    jQuery('.js__image--expandable').off();
                });
            }
        }

        /* Video standard and expand */
        function expandVideo(className, $this, e, inMutate) {
            var _containerId,
                $_video,
                initExpandVideo,
                isEvent = (e ? true : false),
                playerInstance;

            if (isEvent === true || inMutate === true) {
                $_video = $this.find('.media__video--responsive');
                _containerId = $_video.attr('id');

                /*
                 * If you want to play HTML5 video on mobile devices in a click event,
                 * the play() call cannot be within a callback, promise, setTimeout, or anything
                 * that causes a delay after the user interaction. Therefore this if statement
                 * is implemented outside of the fastdom measures and mutates.
                 */
                if (CNN.VideoPlayer.getLibraryName(_containerId) === 'fave') {
                    playerInstance = FAVE.player.getInstance(_containerId);
                    /* Play/resume video */
                    playerInstance.resume();
                }
            }

            initExpandVideo = function () {
                var $wrapper = $this.parents('.el__embedded'),
                    $thumbnail = $this.find('.media__video--thumbnail'),
                    $closeButton = $this.find('.el__storyelements--close'),
                    $redSpinner = $wrapper.find('.video-red-spinner'),
                    $video = $_video || $this.find('.media__video--responsive'),
                    $objectElem = $video.find('object').get(0),
                    $videoElem = $video.find('video').get(0),
                    containerId = _containerId || $video.attr('id'),
                    initVideoPlayer,
                    localUserAgent = navigator.userAgent.toLowerCase();

                if (typeof videoWidth === 'undefined') {
                    videoWidth = $this.width();
                }

                /* If not in an event handler */
                if (isEvent === false && inMutate !== true &&
                   CNN.VideoPlayer.getLibraryName(containerId) === 'fave') {

                    playerInstance = FAVE.player.getInstance(containerId);
                    /* Play/resume video */
                    playerInstance.resume();
                }

                initVideoPlayer = function () {
                    $thumbnail.css('position', '');

                    $this.stop().animate({
                        width:'100%',
                        'margin-top': closeHeight,
                        'margin-right': 0
                    }, teaseImgDelay, function () {
                        fastdom.mutate(function () {
                            if (CNN.VideoPlayer.getLibraryName(containerId) === 'videoLoader') {
                                if (isDesktop) {
                                    $redSpinner.show();
                                    if ((localUserAgent.indexOf('msie') !== -1 || localUserAgent.indexOf('trident') !== -1) && typeof $objectElem !== 'undefined') {
                                        $objectElem.replayContent();
                                    }
                                } else {
                                    if (typeof $videoElem !== 'undefined') {
                                        $videoElem.play();
                                    }
                                }
                                $this.find('.el__video__play-button').triggerHandler('click');
                            }
                            $wrapper.addClass('el__embedded--open');
                        });
                    });

                    $this.show().addClass('el__video--expandfull').removeClass(className);
                    $video.show();
                    if (window.innerWidth > CNN.minWidthForExpandElements) {
                        $closeButton.show();
                        $closeButton.css('display', 'block');
                    } else {
                        $closeButton.hide();
                        $closeButton.css('display', 'none');
                    }
                    /* Listen for orientationchange in order to show or hide the close button */
                    if (FAVE.settings.enabled) {
                        jQuery(window).on('orientationchange', function () {
                            fastdom.mutate(function () {
                                if (window.innerWidth > CNN.minWidthForExpandElements) {
                                    $closeButton.show();
                                    $closeButton.css('display', 'block');
                                } else {
                                    $closeButton.hide();
                                    $closeButton.css('display', 'none');
                                }
                            });
                        });
                    }
                };

                if (inMutate === true) {
                    initVideoPlayer();
                } else {
                    fastdom.mutate(initVideoPlayer);
                }
            };

            if (inMutate === true) {
                initExpandVideo();
            } else {
                fastdom.measure(initExpandVideo);
            }
        }

        function collapseVideo(className, $this, e) {
            fastdom.measure(function () {
                var $wrapper = $this.parents('.el__embedded'),
                    endSlateSelectors = '.js-media__video .js-video__end-slate',
                    $thumbnail = $wrapper.find('.media__video--thumbnail'),
                    $video = $wrapper.find('.media__video--responsive'),
                    $redSpinner = $wrapper.find('.video-red-spinner'),
                    $videoEndSlate,
                    $objectElem = $video.find('object').get(0),
                    $videoElem = $video.find('video').get(0),
                    containerId = $video.attr('id'),
                    localUserAgent = navigator.userAgent.toLowerCase(),
                    playerInstance;

                $this = $this.parent();

                fastdom.mutate(function () {
                    $wrapper.removeClass('el__embedded--open');

                    $this.stop().animate({
                        width: videoWidth,
                        'margin-top': '10px',
                        'margin-right': '30px'
                    }, teaseImgDelay, function clearStyle() {
                        fastdom.mutate(function () {
                            $this.css('width', '');
                        });
                    });
                    $this.find('.el__video__close--expandable').hide();
                    $this.addClass(className).removeClass('el__video--expandfull');
                    $video.hide();
                    $thumbnail.show();
                    $thumbnail.css('position', 'relative');

                    if (CNN.VideoPlayer.getLibraryName(containerId) === 'fave') {
                        playerInstance = FAVE.player.getInstance(containerId);
                        playerInstance.pause();
                    } else {
                        if (isDesktop) {
                            $redSpinner.hide();
                            /* To stop the video in Article Leaf page plays in the background even after closing in Internet Explorer */
                            if ((localUserAgent.indexOf('msie') !== -1 || localUserAgent.indexOf('trident') !== -1) && typeof $objectElem !== 'undefined') {
                                $objectElem.stopContent();
                            }
                        } else {
                            /* To stop the video in Article Leaf page plays in the background even after closing in devices */
                            if (typeof $videoElem !== 'undefined') {
                                $videoElem.pause();
                            }
                        }
                    }
                });

                $videoEndSlate = $wrapper.find(endSlateSelectors).eq(0);

                fastdom.mutate(function () {
                    $videoEndSlate.removeClass('video__end-slate--active').addClass('video__end-slate--inactive');
                    $wrapper.find('.js-video-demand').show();
                });
            });
            e.stopPropagation();
        }

        function faveExpandVideos() {
            fastdom.measure(function () {
                setupEventHandler('.js__video--standard .media__video--thumbnail', 'click', function (event) {
                    expandVideo('el__video--standard', jQuery(this).parents('.js__video--standard'), event);
                });

                setupEventHandler('.js__video--expandable .media__video--thumbnail', 'click', function (event) {
                    expandVideo('el__video--expandable', jQuery(this).parents('.js__video--expandable'), event);
                });

                setupEventHandler('.js__video__close--standard', 'click', function (event) {
                    collapseVideo('el__video--standard', jQuery(this), event);
                });

                setupEventHandler('.js__video__close--expandable', 'click', function (event) {
                    collapseVideo('el__video--expandable', jQuery(this), event);
                });

                fastdom.mutate(function () {
                    jQuery('.el__video--expandfull .el__video__close--standard').show();
                    jQuery('.el__video--expandfull .el__video__close--expandable').show();
                    jQuery('.el__video--expandable .el__video__expand--expandable').show();
                    jQuery('.el__video--standard .el__video__expand--standard').show();
                    jQuery('.js-video-demand').show();
                });

                initialLoad = false;
            }.bind(this));
        }

        function videoLoaderExpandVideos() {
            fastdom.measure(function () {
                if (CNN.CurrentSize.getClientWidth() > CNN.minWidthForExpandElements) {
                    setupEventHandler('.js__video--standard', 'click', function (event) {
                        expandVideo('el__video--standard', jQuery(this), event);
                    });

                    setupEventHandler('.js__video--expandable', 'click', function (event) {
                        expandVideo('el__video--expandable', jQuery(this), event);
                    });

                    setupEventHandler('.js__video__close--standard', 'click', function (event) {
                        collapseVideo('el__video--standard', jQuery(this), event);
                    });

                    setupEventHandler('.js__video__close--expandable', 'click', function (event) {
                        collapseVideo('el__video--expandable', jQuery(this), event);
                    });

                    fastdom.mutate(function () {
                        jQuery('.el__video--expandfull .el__video__close--standard').show();
                        jQuery('.el__video--expandfull .el__video__close--expandable').show();
                        jQuery('.el__video--expandable .el__video__expand--expandable').show();
                        jQuery('.el__video--standard .el__video__expand--standard').show();
                        jQuery('.js-video-demand').show();
                    });
                } else {
                    fastdom.mutate(function () {
                        jQuery('.el__video__close--standard').hide();
                        jQuery('.el__video__expand--standard').hide();
                        jQuery('.el__video__close--expandable').hide();
                        jQuery('.el__video__expand--expandable').hide();

                        if (CNN.orientationLandscape === true) {
                            jQuery('.js-video-demand').show();
                        } else {
                            jQuery('.js-video-demand').hide();
                        }
                    });
                }

                if (initialLoad === false && CNN.orientationLandscape === true) {
                    jQuery('.js__video--standard').each(function () {
                        var $this = jQuery(this),
                            $jsVideoDemand = $this.find('.js-video-demand'),
                            $elVideoExpand;

                        if ($this.find('.js-el__video__play-button').size() === 0) {
                            $elVideoExpand = $this.find('.el__video__expand--standard');
                            fastdom.mutate(function ($jsVD, $elVE) {
                                $jsVD.hide();
                                if (CNN.orientationLandscape === true) {
                                    expandVideo('el__video--standard', $elVE, null, true);
                                }
                            }.bind(this, $jsVideoDemand, $elVideoExpand));
                        } else {
                            fastdom.mutate(function ($jsVD) {
                                $jsVD.show();
                            }.bind(this, $jsVideoDemand));
                        }
                    });

                    jQuery('.js__video--expandable').each(function () {
                        var $this = jQuery(this),
                            $jsVideoDemand = $this.find('.js-video-demand'),
                            $elVideoExpand;

                        if ($this.find('.js-el__video__play-button').size() === 0) {
                            $elVideoExpand = $this.find('.el__video__expand--expandable');
                            fastdom.mutate(function ($jsVD, $elVE) {
                                $jsVD.hide();
                                if (CNN.orientationLandscape === true) {
                                    expandVideo('el__video--expandable', $elVE, null, true);
                                }
                            }.bind(this, $jsVideoDemand, $elVideoExpand));
                        } else {
                            fastdom.mutate(function ($jsVD) {
                                $jsVD.show();
                            }.bind(this, $jsVideoDemand));
                        }
                    });
                }

                initialLoad = false;
            }.bind(this));
        }

        function initExpandVideos() {
            if (FAVE.settings.enabled) {
                faveExpandVideos();
            } else {
                videoLoaderExpandVideos();
            }
        }

        /* Gallery standard and expand */
        function expandGallery(className, $this, e) {
            if ($this.is('.el__gallery--expandfull')) {
                return;
            }

            if (typeof galleryWidth === 'undefined') {
                galleryWidth = $this.width();
            }

            if (CNN.CurrentSize.getClientWidth() > CNN.minWidthForExpandElements) {
                fastdom.measure(function () {
                    var $wrapper = $this.parents('.el__embedded'),
                        $gallery = $this,
                        $teaseImage = $this.find('.el__gallery--teaseimage'),
                        $carouselWrapper = $this.find('.el-carousel__wrapper'),
                        $thisCarousel = $carouselWrapper.find('.js-owl-carousel'),
                        $thisFilmstrip = $carouselWrapper.find('.js-owl-filmstrip'),
                        isInitialized = !!$carouselWrapper.attr('data-initialized'),
                        imgHeight,
                        showCarousel;

                    showCarousel = function ($wrapper) {
                        $wrapper.css({
                            opacity: '1',
                            position: 'relative',
                            'z-index': '0'
                        });

                        $carouselWrapper.attr('data-initialized', 'true');
                    };

                    fastdom.mutate(function () {
                        /* set img height */
                        $carouselWrapper.css({
                            display: 'block',
                            visibility: 'visible',
                            opacity: '0'
                        });

                        $gallery.stop().animate({
                            width: $wrapper.width() + 'px',
                            'margin-top': closeHeight,
                            'margin-right': 0
                        }, teaseImgDelay, function () {
                            fastdom.mutate(function () {
                                $gallery.css('width', '100%');

                                imgHeight = ($wrapper.width() * 9 / 16) / 2 - galleryArrowHeight;

                                $thisCarousel.siblings('.ad-slide').find('.ad-slide__prev, .ad-slide__next').css('top', imgHeight + 'px');

                                $wrapper.addClass('el__embedded--open');

                                $wrapper.css('overflow', 'hidden');

                                $gallery.css({
                                    'min-height': $teaseImage.height() + 'px',
                                    'max-height': $teaseImage.height() + 'px'
                                });

                                setTimeout(function (gallery, carousel) {
                                    fastdom.mutate(function () {
                                        gallery.css({
                                            'max-height': carousel.height() * 1.5 + 'px'
                                        });
                                        CNN.Carousel.refreshCarousels();
                                    });
                                }, 100, $gallery, $thisCarousel);

                                if (isInitialized === false || $thisCarousel.data('owl.carousel').width() !== $gallery.width()) {
                                    $thisCarousel.on('resized.owl.carousel', {$wrapper: $carouselWrapper}, function (e) {
                                        fastdom.mutate(function () {
                                            showCarousel(e.data.$wrapper);
                                            jQuery(this).off('resized.owl.carousel');
                                        }.bind(this));
                                    });

                                    $thisFilmstrip.on('resized.owl.carousel', function (e) {
                                        fastdom.mutate(function () {
                                            CNN.Carousel.owl().setNumberOfItems(e);
                                            e.relatedTarget.to(0);
                                            jQuery(this).off('resized.owl.carousel');
                                        }.bind(this));
                                    });

                                    $thisCarousel.data('owl.carousel').onResize();
                                    $thisFilmstrip.data('owl.carousel').onResize();

                                } else {
                                    showCarousel($carouselWrapper);
                                }

                                $gallery.addClass('el__gallery--expandfull').removeClass(className);

                                if (typeof $carouselWrapper.attr('isExtended') === 'undefined' ||
                                    $carouselWrapper.attr('isExtended') === 'false') {

                                    setTimeout(function () {
                                        fastdom.mutate(function () {
                                            fixCaptionBottom(jQuery($carouselWrapper.find('.owl-wrapper-outer')), true, true);
                                        });
                                    }, (1000 * 2));

                                    $carouselWrapper.attr('isExtended', 'true');
                                }
                            }.bind(this));
                        });
                    }.bind(this));

                });

            } else {
                e.stopPropagation();
            }
        }

        function collapseGallery(className, $this, e) {
            fastdom.measure(function () {
                var $wrapper = $this.parents('.el__embedded'),
                    $carouselWrapper = $wrapper.find('.el-carousel__wrapper'),
                    $owlWrapperOuter = $carouselWrapper.find('.owl-wrapper-outer'),
                    $galleryCaption = $owlWrapperOuter.find('.el__gallery-caption'),
                    $gallery = $this.parents('.el__gallery--expandfull');

                fastdom.mutate(function () {
                    $wrapper.css({
                        'max-height': '',
                        overflow: ''
                    });

                    $carouselWrapper.css({
                        position: 'absolute',
                        opacity: 0
                    });

                    $wrapper.removeClass('el__embedded--open');

                    $gallery.css({
                        'min-height': '',
                        'max-height': ''
                    });

                    $gallery.stop().animate({
                        width: galleryWidth,
                        'margin-top': '10px',
                        'margin-right': '30px'
                    }, teaseImgDelay, function () {
                        fastdom.mutate(function () {
                            jQuery(this).addClass(className).removeClass('el__gallery--expandfull');
                        }.bind(this));
                    });

                    $carouselWrapper.attr('isExtended', 'false');

                    $gallery.addClass(className).removeClass('el__gallery--expandfull');

                    if ($galleryCaption.hasClass('el__gallery-caption--closed')) {
                        $galleryCaption.toggleClass('el__gallery-caption--closed');
                        $galleryCaption.text($gallery.hasClass('el__gallery-caption--closed') ? 'Show Caption' : 'Hide Caption');
                    }
                }.bind(this));
            }.bind(this));
            e.stopPropagation();
        }

        function initExpandGallery() {
            if (window.innerWidth > CNN.minWidthForExpandElements) {
                setupEventHandler('.js__gallery--standard', 'click', function (event) {
                    expandGallery('el__gallery--standard', jQuery(this), event);
                });

                setupEventHandler('.js__gallery--expandable', 'click', function (event) {
                    expandGallery('el__gallery--expandable', jQuery(this), event);
                });

                setupEventHandler('.js__gallery__close--standard', 'click', function (event) {
                    collapseGallery('el__gallery--standard', jQuery(this), event);
                });

                setupEventHandler('.js__gallery__close--expandable', 'click', function (event) {
                    collapseGallery('el__gallery--expandable', jQuery(this), event);
                });

                jQuery('.js__gallery__close--standard, .js__gallery__close--expandable').trigger('click');

                fastdom.mutate(function () {
                    jQuery('.el-carousel__wrapper').not(jQuery('.zn-large-media .el-carousel__wrapper,' +
                        ' .el__embedded--fullstandardwidth .el-carousel__wrapper,' +
                        '.pg-special-article__head .el-carousel__wrapper'))
                        .css({
                            opacity: '0',
                            position: '',
                            'z-index': ''
                        });
                });
            } else {
                fastdom.mutate(function () {
                    jQuery('.el-carousel__wrapper').css({
                        opacity: '1',
                        position: 'relative',
                        'z-index': '0'
                    });
                });
            }

            /* hide gallery captions by default for mobile viewport but not on resize */
            if (resizedWin === false && window.innerWidth <= CNN.showGalleryCaptionWidth) {
                hideAllCaptions();
            }
        }

        /* hide gallery captions by default  */
        function hideAllCaptions() {
            fastdom.measure(function () {
                jQuery('.js__leafmedia--gallery').each(function (i, val) {
                    fastdom.mutate(function ($title, $caption) {
                        $title.hide();
                        $caption.addClass('el__gallery-caption--closed').text('Show Caption');
                    }.bind(this, jQuery('.el__gallery_image-title', val), jQuery('.el__gallery-caption', val)));
                });
            });
        }

        function toggleCaptions(el, _speed) {
            var $this = jQuery(el),
                $stage = $this.closest('.owl-stage'),
                $sibs = $stage.find('.el__gallery_image-title'),
                $gallery = $stage.find('.el__gallery-caption');

            fastdom.mutate(function () {
                if (CNN.CurrentSize.getClientWidth() > CNN.minWidthForExpandElements) {
                    if ($gallery.hasClass('el__gallery-caption--closed')) {
                        $gallery.removeClass('el__gallery-caption--closed');
                        $sibs.show();
                    } else {
                        $gallery.addClass('el__gallery-caption--closed');
                        $sibs.hide();
                    }
                } else {
                    $gallery.toggleClass('el__gallery-caption--closed');
                    $sibs.toggle();
                }

                CNN.Carousel.refreshCarousels();

                $gallery.text($gallery.hasClass('el__gallery-caption--closed') ? 'Show Caption' : 'Hide Caption');
            });
        }

        function fixCaptionBottom(gallery, initialLoad, reload) {
            var max = 0,
                height = 0,

                /* find tallest .owl-item */
                $cards = jQuery('.owl-item', gallery).each(function () {
                    height = jQuery(this).height();
                    if (initialLoad) {
                        jQuery(this).attr('owlItemHeight', height);
                    }
                    if (height > max) {
                        max = height;
                    }
                });

            if (initialLoad) {
                jQuery(gallery).attr('max', max);
            }

            /* adjust el__gallery-showhide bottom */
            $cards.each(function () {
                var $this = jQuery(this),
                    $content = jQuery('.el__gallery-showhide', $this);
                if (initialLoad || reload) {
                    $content.css('bottom', ($this.attr('owlItemHeight') - jQuery(gallery).attr('max')) + 'px');
                } else {
                    $content.css('bottom', '0px');
                }
            });
        }

        function fixGalleryCaptionBottoms($gallery) {
            if (!$gallery) { return; }

            fastdom.mutate(function () {
                var i,
                    len;

                for (i = 0, len = $gallery.length; i < len; i++) {
                    fixCaptionBottom($gallery[i], true, true);
                }
            });
        }

        function expandStoryElements() {
            initExpandImages();
            initExpandVideos();
            initExpandGallery();
        }

        expandStoryElements();

        /* @TODO: The resize event doesn't get added for mobile devices. We need to identify the root cause for triggering resize while scrolling in Android */
        if (!isMobile) {
            jQuery(window).resize(function () {
                resizedWin = true;
                expandStoryElements();
                setTimeout(function () {
                    fixGalleryCaptionBottoms(jQuery('.owl-wrapper-outer'));
                }, (1000 * 2));
            });
        } else {
            $window.on('orientationchange', function modeChange() {
                if (typeof CNN.Carousel.resetOnResize === 'function') {
                    CNN.Carousel.resetOnResize();
                }
            });
        }

        /* To apply hover on/off styles on images for touch devices */
        (function applyTouchHovers() {
            var i = 0,
                touchHoverElements = [];

            touchHoverElements = [
                {selector: '.js__image--expandable', className: 'el__image--touchhover'},
                {selector: '.js__video--standard', className: 'el__video--touchhover'},
                {selector: '.js__video--expandable', className: 'el__video--touchhover'},
                {selector: '.js__gallery--standard', className: 'el__gallery--touchhover'},
                {selector: '.js__gallery--expandable', className: 'el__gallery--touchhover'}
            ];

            function indicateTouching(elem, hoverClass) {
                /* addListener method defined in event-listener-with-options.js */
                var $target = jQuery(elem).children().last(),
                    addEventListenerWithOptions = CNN.eventListenerWithOptions && CNN.eventListenerWithOptions.addListener;

                function addHoverClass() { $target.addClass(hoverClass); }
                function removeHoverClass() { $target.removeClass(hoverClass); }

                addEventListenerWithOptions(elem, 'touchstart', addHoverClass, {passive: true});
                addEventListenerWithOptions(elem, 'touchcancel', removeHoverClass, {passive: true});
                addEventListenerWithOptions(elem, 'touchend', removeHoverClass, {passive: true});
            }

            function handleTouchHoverEach(index, element) {
                indicateTouching(element, touchHoverElements[i].className);
            }

            for (i = 0; i < touchHoverElements.length; i++) {
                jQuery(touchHoverElements[i].selector).each(handleTouchHoverEach);
            }
        }());

        /* To show or hide caption */
        jQuery('.js-el__gallery-caption').on('click', function () {
            fastdom.measure(function () {
                toggleCaptions(this);
            }.bind(this));
        });

    })
});

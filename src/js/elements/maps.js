/* global CNN, fastdom, Modernizr */

(function createMapModule($, win, ns, doc, Modernizr) {
    'use strict';
    ns.MAPS = ns.MAPS || {};

    ns.MAPS.defaults = {
        BOTTOM_RIGHT: 'Bottom Right',
        TOP_RIGHT: 'Top Right',
        TOP_LEFT: 'Top Left',
        INSET_GAP: 20,
        MIN_INSET_ZOOM: 2,
        MAX_INSET_ZOOM:21,
        MARKER_ICON_WIDTH: 10,
        MARKER_GUTTER: 15,
        CALLOUT_COLOR: '#CC0000',
        DEFAULT_COLOR: '#404040'
    };

    function _getBaseConfig(mapConfig) {
        var config = mapConfig,
            baseConfig = {},
            mapPosition;

        mapPosition = new win.google.maps.LatLng({lat: config.lat, lng: config.lng});
        baseConfig.mapConfig = {
            center: mapPosition,
            zoom: config.zoomLevel,
            mapTypeId: config.style,
            mapTypeControl: false,
            scrollwheel: false,
            streetViewControl: false
        };
        baseConfig.insetConfig = {
            center: mapPosition,
            zoom: config.inset.zoomLevel,
            mapTypeId: config.style,
            disableDefaultUI: true,
            disableDoubleClickZoom: true,
            draggable: false,
            scrollwheel: false,
            zoomControl: false
        };

        return baseConfig;
    }

    function _convertBoundsToNWSE(bounds) {
        var north = bounds.getNorthEast().lat(),
            west = bounds.getSouthWest().lng(),
            south = bounds.getSouthWest().lat(),
            east = bounds.getNorthEast().lng();

        return [north, south, east, west];
    }

    function _addCallout(calloutBounds) {
        var bounds = calloutBounds,
            rectangleHolder;

        rectangleHolder = new win.google.maps.Rectangle({
            bounds: {
                north: bounds[0],
                south: bounds[1],
                east: bounds[2],
                west: bounds[3]
            },
            fillOpacity: 0.0,
            strokeColor: ns.MAPS.defaults.CALLOUT_COLOR,
            strokeOpacity: 0.8,
            strokeWeight: 2
        });

        return rectangleHolder;
    }

    function _getMarkerStyle(currentMarkerConfig) {
        var fillColor,
            iconRotation,
            iconWidth = ns.MAPS.defaults.MARKER_ICON_WIDTH,
            markerGutter = ns.MAPS.defaults.MARKER_GUTTER,
            markerIcon,
            position = currentMarkerConfig.alignment,
            style,
            svgPath;

        switch (position) {
        case 'Bottom':
            style = 'translateY(calc(50% + ' + ((iconWidth / 2) + markerGutter) + 'px))';
            iconRotation = 90;
            break;

        case 'Bottom Left':
            style = 'translateX(calc(-50% - ' + ((iconWidth / 2) + markerGutter) + 'px)) translateY(calc(50% + ' + ((iconWidth / 2) + markerGutter) + 'px))';
            iconRotation = 135;
            break;

        case 'Bottom Right':
            style = 'translateX(calc(50% + ' + ((iconWidth / 2) + markerGutter) + 'px)) translateY(calc(50% + ' + ((iconWidth / 2) + markerGutter) + 'px))';
            iconRotation = 45;
            break;

        case 'Left':
            style = 'translateX(calc(-50% - ' + ((iconWidth / 2) + markerGutter) + 'px))';
            iconRotation = 180;
            break;

        case 'Right':
            style = 'translateX(calc(50% + ' + ((iconWidth / 2) + markerGutter) + 'px))';
            iconRotation = 0;
            break;

        case 'Top':
            style = 'translateY(calc(-50% - ' + ((iconWidth / 2) + markerGutter) + 'px))';
            iconRotation = 270;
            break;

        case 'Top Left':
            style = 'translateX(calc(-50% - ' + ((iconWidth / 2) + markerGutter) + 'px)) translateY(calc(-50% - ' + ((iconWidth / 2) + markerGutter) + 'px))';
            iconRotation = 225;
            break;

        case 'Top Right':
            style = 'translateX(calc(50% + ' + ((iconWidth / 2) + markerGutter) + 'px)) translateY(calc(-50% - ' + ((iconWidth / 2) + markerGutter) + 'px))';
            iconRotation = 315;
            break;

        default:
            style = 'translateX(calc(50% + ' + ((iconWidth / 2) + markerGutter) + 'px))';
            iconRotation = 0;
        }

        if (currentMarkerConfig.style === 'callout') {
            fillColor = ns.MAPS.defaults.CALLOUT_COLOR;
        } else {
            fillColor = ns.MAPS.defaults.DEFAULT_COLOR;
        }

        svgPath = (currentMarkerConfig.style === 'labelOnly') ? 'M 0, 0 m -5, 0 a 4,4 0 1,0 8,0 a 4,4 0 1,0 -8,0' : 'M 0, 0 m -5, 0 a 4,4 0 1,0 8,0 a 4,4 0 1,0 -8,0 L 35, 0';

        markerIcon = {
            path: svgPath,    // Marker Icon SVG path
            fillColor: fillColor,
            fillOpacity: 1,
            scale: 1,
            strokeColor: fillColor,
            strokeWeight: 1,
            rotation: iconRotation
        };

        return {
            style: style,
            icon: markerIcon
        }
    }

    function _addMarker(index, mainMap, markerConfig) {
        var map = mainMap,
            marker,
            markerIcon,
            markerStyle = _getMarkerStyle(markerConfig[index]),
            position = new win.google.maps.LatLng({lat: markerConfig[index].lat, lng: markerConfig[index].lng});

        marker = new win.RichMarker({
            position: position,
            map: map,
            draggable: false,
            flat: true,
            anchor: win.RichMarkerPosition.MIDDLE,
            content: '<div class="' + markerConfig[index].style + '" style="transform: ' + markerStyle.style + '; -webkit-transform: ' + markerStyle.style + '">' + markerConfig[index].label + '</div>'
        });

        markerIcon = new win.google.maps.Marker({
            position: position,
            icon: markerStyle.icon
        });

        markerIcon.setMap(map);   // To add the marker icon to the map.

        return marker;
    }

    function _getInsetPosition(insetConfig) {
        var position = insetConfig.alignment,
            style = {};

        if (position === ns.MAPS.defaults.BOTTOM_RIGHT || position === ns.MAPS.defaults.TOP_RIGHT) {
            style.right = ns.MAPS.defaults.INSET_GAP;
        } else {
            style.left = ns.MAPS.defaults.INSET_GAP;
        }

        if (position === ns.MAPS.defaults.TOP_RIGHT || position === ns.MAPS.defaults.TOP_LEFT) {
            style.top = ns.MAPS.defaults.INSET_GAP;
        } else {
            style.bottom = ns.MAPS.defaults.INSET_GAP;
        }

        return style;
    }

    function _updateZoom(newZoom, config) {
        var zoomChange = newZoom - config.zoomLevel,
            insetZoom = config.inset.zoomLevel;

        if ((config.inset.zoomLevel + zoomChange >= ns.MAPS.defaults.MIN_INSET_ZOOM)
               && (config.inset.zoomLevel + zoomChange <= ns.MAPS.defaults.MAX_INSET_ZOOM)) {
            return insetZoom += zoomChange;
        }
    }

    function _initMap(config) {
        var baseConfig,
            calloutBounds,
            calloutRectangle,
            finalBounds,
            gMap,
            $insetContainer = $(doc.getElementById('overlay__' + config.mapId)),
            $mapContainer = $(doc.getElementById('container__' + config.mapId)),
            $thumbnailImage = $(doc.getElementById('thumbnail__' + config.mapId)),
            i,
            insetMap,
            insetStyle,
            updatedZoom;

        if (typeof win.google.maps !== 'undefined') {
            baseConfig = _getBaseConfig(config);

            if (config.scale === true) {
                baseConfig.mapConfig.scaleControl = true;
            }

            if (config.inset.alignment !== 'undefined' && config.inset.alignment === ns.MAPS.defaults.BOTTOM_RIGHT) {
                baseConfig.mapConfig.zoomControlOptions = { position: win.google.maps.ControlPosition.RIGHT_TOP };
            } else {
                baseConfig.mapConfig.zoomControlOptions = { position: win.google.maps.ControlPosition.RIGHT_BOTTOM };
            }

            gMap = new win.google.maps.Map(doc.getElementById(config.mapId), baseConfig.mapConfig);

            for(i = 0; i < config.markers.length; i++) {
                _addMarker(i, gMap, config.markers);
            }

            if (!Modernizr.phone && config.inset.visible === true) {
                insetStyle = _getInsetPosition(config.inset);
                insetMap = new win.google.maps.Map(doc.getElementById('overlay__' + config.mapId), baseConfig.insetConfig);

                win.google.maps.event.addListener(gMap, 'bounds_changed', function () {
                    insetMap.setCenter(gMap.getCenter());
                    updatedZoom = _updateZoom(gMap.getZoom(), config);

                    if(typeof updatedZoom !== 'undefined') {
                        insetMap.setZoom(updatedZoom);
                    }

                    if (config.inset.callout === true) {

                        if (config.inset.zoomLevel < config.zoomLevel) {
                            calloutBounds = gMap.getBounds();
                            finalBounds = _convertBoundsToNWSE(calloutBounds);

                            fastdom.mutate(function () {
                                if (typeof calloutRectangle !== 'undefined') {
                                    calloutRectangle.setMap(null);
                                }
                                calloutRectangle = _addCallout(finalBounds);
                                calloutRectangle.setMap(insetMap);
                            });
                        }
                    }
                });

                $insetContainer.css(insetStyle);
            } else {
                $insetContainer.hide();
            }

            win.google.maps.event.addListener(gMap, 'idle', function () {
                $mapContainer.removeClass('inactive').addClass('active');
                $thumbnailImage.hide('slow');
            });
        }
    }

    ns.MAPS.loadMap = function (config) {
        $(doc).onZonesAndDomReady(function () {
            _initMap(config);
        });
    }

}(jQuery, window, CNN, document, Modernizr));

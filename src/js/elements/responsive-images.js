/* global CNN, eqjs, picturefill */

CNN.DemandLoadConfig = CNN.DemandLoadConfig || {};

(function ($, eqjs) {
    'use strict';

    var isLazyLoad,
        isPictureFillEnabled;

    CNN.ResponsiveImages = {};

    /**
     * Generic function to process responsive images. The code is used in both
     * events below, in addition to other files that load image content, such as
     * carousel.js, so this function intentionally exists in the global scope.
     *
     * All of the following code relies on the `data-eq-state` attribute being
     * successfully set by eq.js — if it fails, most of the images cannot render
     * because they cannot determine what size image they should be loading.
     * This failure will not be frequent, because eq.js is in the build script
     * and always executes before this JS file.
     *
     * @see http://docs.turner.com/display/CNNEXPANSION/Responsive+images+in+Michonne
     *
     * @param {string} elements - The jQuery filter string for the elements to process
     */
    CNN.ResponsiveImages.process = function processResponsiveImages(elements) {
        var dataTag = '[data-demand-load=loaded]',
            $elements = $(elements),
            $processedElements = $();

        function filterAndProcessImages(filterSelector, imageCut) {
            var $images = $elements.filter(filterSelector).filter(dataTag).not($processedElements),
                $image,
                len,
                i;

            for (i = 0, len = $images.length; i < len; i++) {
                $image = $images.eq(i);
                $image.attr('src', $image.data(imageCut));
            }

            /* Avoids double processing images and so overrides preside over defaults */
            $processedElements = $processedElements.add($images);
        }

        /* Override: process smaller horizontal containers to utilise 1x1 aspect images */
        filterAndProcessImages('.cd--horizontal[data-eq-state$="xsmall"] img[data-src-mini1x1]' +
                                ', .cd--horizontal[data-eq-state$="small"] img[data-src-mini1x1]', 'src-mini1x1');

        /* Override: process zone backgrounds */
        filterAndProcessImages('.zn[data-eq-state$="full16x9"] .zn-top img[data-src-full16x9]', 'src-full16x9');

        /* Default: process containers that utilise 16x9 aspect images */
        filterAndProcessImages('img[data-eq-state$="mini"][data-src-mini]', 'src-mini');
        filterAndProcessImages('img[data-eq-state$="xsmall"][data-src-xsmall]', 'src-xsmall');
        filterAndProcessImages('img[data-eq-state$="small"][data-src-small]', 'src-small');
        filterAndProcessImages('img[data-eq-state$="medium"][data-src-medium]', 'src-medium');
        filterAndProcessImages('img[data-eq-state$="large"][data-src-large]', 'src-large');
    };

    /**
    * Fire eq.js and supply a callback to ensure that image src replacements
    * happen after the data-eq-state is set.
    *
    * @param {String|Object<NodeList>|Object<jQuery>} [scope=document.querySelectorAll('[data-eq-pts]')]  - The image elements to process.
    */
    CNN.ResponsiveImages.queryEqjs = function queryResponsiveImagesEqjs(scope) {
        var isScoped = CNN.Utils.existsObject(scope),
            $elements = (isScoped) ? $(scope).filter('[data-eq-pts]') : $('[data-eq-pts]'),
            elementsArray = [];

        /* If filtered elements are empty, perhaps try finding nested ones. */
        if (isScoped && $elements.length === 0) {
            $elements = $(scope).find('[data-eq-pts]');
        }

        /* Create or use existing array to send to eqjs. */
        elementsArray = ($elements instanceof jQuery) ? $elements.toArray() : $elements;

        /* Run eqjs and process elements on callback. */
        if (elementsArray.length === 0) {
            return;
        }

        eqjs.query(elementsArray, function responsiveImageQueryCallback() {
            /* Set demand loading images that need to be loaded. */
            if (!CNN.DemandLoading.allLoaded) {
                CNN.DemandLoading.update();
            }

            if ($elements.length > 0) {
                CNN.ResponsiveImages.process($elements);
            }
        });
    };

    /**
     * Listen for window scroll and throttle to prevent excessive calls.
     * No need to update the eqjs size, as the page hasn't changed size.
     * First we update the demand loading cutoff point, which may call
     * the demand loading process function only if necessary, and will
     * return any newly loading images. If so, we will need to set their
     * sources with the responsive images process.
     * Finally, kill this scroll event if the page bottom is reached or
     * if all images have been loaded.
     */
    CNN.ResponsiveImages.scroll = function scrollResponsiveImages() {
        var elements;

        if (CNN._zonesAndDomReadyEvent && (CNN.DemandLoading.pageBottom || CNN.DemandLoading.allLoaded)) {
            $(window).off('scroll.responsiveImages');
        } else {
            elements = CNN.DemandLoading.update();

            if (elements.length > 0) {
                CNN.ResponsiveImages.process(elements);
            }
        }
    };

    /**
     * A modification of the above scroll function, used in cases where
     * additional images are loaded on the page, and previous variables
     * for furthestCutoff and allLoaded must be ignored to load the new
     * images (example use: mega-nav.js).
     */
    CNN.ResponsiveImages.reload = function reloadResponsiveImages() {
        /* Using process instead of update to skip the furthestCutoff check. */
        var elements = CNN.DemandLoading.reProcess();

        if (elements.length > 0) {
            CNN.ResponsiveImages.process(elements);
        }
    };

    /**
     * Loads for all responsive images of the child elements for the given parent.
     *
     * @param {object} parentEl - parent element to search for child elements within.
     */
    CNN.ResponsiveImages.processChildrenImages = function reloadChildrenResponsiveImages(parentEl) {
        var isLazyLoad = (CNN.contentModel && typeof CNN.contentModel.lazyLoad === 'boolean') ? CNN.contentModel.lazyLoad : false,
            isPictureFillEnabled = (CNN.contentModel && typeof CNN.contentModel.enablePictureFill === 'boolean') ? CNN.contentModel.enablePictureFill : false;

        if (isLazyLoad && isPictureFillEnabled) {
            if (window.picturefill) {
                picturefill();
            }
        } else {
            eqjs.query(parentEl.querySelectorAll('[data-eq-pts]'), function (elements) {
                if (elements.length > 0) {
                    CNN.ResponsiveImages.process(elements);
                }
            });
        }
    };

    /**
     * Listen for window resize and throttle to prevent excessive calls.
     * Update the eqjs size for all elements on the page.
     * Only attempt to load new images if all elements aren't already loaded.
     * All loaded images will need to be updated on every resize, thus we don't
     * care about whether or not demand loading found any new images.
     */
    CNN.ResponsiveImages.resize = function resizeResponsiveImages() {
        CNN.ResponsiveImages.queryEqjs(document);
    };

    isLazyLoad = (CNN.contentModel && typeof CNN.contentModel.lazyLoad === 'boolean') ? CNN.contentModel.lazyLoad : false;
    isPictureFillEnabled = (picturefill && CNN.contentModel && typeof CNN.contentModel.enablePictureFill === 'boolean') ? CNN.contentModel.enablePictureFill : false;

    if (isLazyLoad === true && isPictureFillEnabled === true) {
        $(window).throttleEvent('resize.responsiveImages', picturefill, 100);
        picturefill();
    } else if (isPictureFillEnabled === false) {
        $(window).throttleEvent('scroll.responsiveImages', CNN.ResponsiveImages.scroll, 50);
        $(window).throttleEvent('resize.responsiveImages', CNN.ResponsiveImages.resize, 100);

        /* Load images on page load. This function is essentially the "initial scroll", if you will. */
        CNN.ResponsiveImages.queryEqjs(document);
    }

})(jQuery, eqjs);

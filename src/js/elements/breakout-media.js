/* global CNN, eqjs */

(function (env, $, eqjs, ns) {
    'use strict';
    /**
     * This is an eqjs processor to process all zones that need the
     * first container's card media element moved from the card and
     * placed at a different location on the zone.
     *
     * When the media element is moved from the card it is replaced
     * with a placeholder element so that the media element can be
     * placed correctly in the card once the default state is reached.
     *
     * This module expects the following minimum markup on the page to
     * work:
     *
     *     <div data-breakout-id="js-breakout-1"> <!-- <== This is the breakout block -->
     *         <div data-breakout="js-breakout-1-destination" data-eq-pts="[breakout:(breakpoint value here), default:(breakpoint value here)]"></div>
     *         <div data-breakout="js-breakout-1-source></div>
     *     </div>
     *
     * NOTE: The example above does not take into account the proper
     * layout of our code just show the relation of the elements to each
     * other.
     **/
    ns.Breakout = ns.Breakout || {
        /**
         * This function is called by eqjs when there has been a width
         * change detected on the destination element of the breakout
         * block.
         *
         * If the state is breakout then the source element is pulled
         * from the page and placed in the destination element. The
         * source element is replaced with a placeholder element on the
         * page.
         *
         * If the state is default then the source element is pulled
         * from the destination element and replaces the placeholder
         * element.
         **/
        process: function processBreakoutEQStates() {
            var $breakouts = $('[data-breakout-id]'),
                $destination,
                $parent,
                $placeholder,
                $source,
                breakoutId,
                destinationSelector,
                hasPlaceholder,
                i,
                placeholderSelector,
                sourceSelector,
                state;

            for (i = $breakouts.length - 1; i >= 0; i -= 1) {
                $parent = $breakouts.eq(i);
                breakoutId = $parent.attr('data-breakout-id');

                /* CSS selectors to find the child elements of the breakout block */
                sourceSelector = '[data-breakout="' + breakoutId + '-source"]';
                destinationSelector = '[data-breakout="' + breakoutId + '-destination"]';
                placeholderSelector = '[data-breakout="' + breakoutId + '-placeholder"]';

                /* Placeholder element is used to determine where the state of the source element. */
                $placeholder = $parent.find(placeholderSelector);
                hasPlaceholder = ($placeholder.length === 0);

                /* Destination element is used to determine the overall state of the breakout block. */
                $destination = $parent.find(destinationSelector);
                state = ($destination.attr('data-eq-state') || '').replace(/^(\S+\s+)*/, '');

                switch (state) {
                case 'breakout':
                    if (hasPlaceholder) {
                        $source = $parent.find(sourceSelector);
                        $source.before('<div data-breakout="' + breakoutId + '-placeholder"></div>').detach().appendTo($destination);
                    }
                    break;
                case 'default':
                    if (!hasPlaceholder) {
                        $source = $parent.find(sourceSelector);
                        $placeholder.after($source.detach()).remove();
                    }
                    break;
                }

                /* Process responsive images in breakout block */
                CNN.ResponsiveImages.processChildrenImages($destination[0]);
            }
        },
        /**
         * Triggers an eqjs query to check if there have been any
         * changes to the eq state for the destination element. Updates
         * the data-eq-state for the destination element if a new
         * element breakpoint has been reached.
         **/
        evaluate: function evaluateBreakoutEQPoints() {
            eqjs.query(env.doc.querySelectorAll('[data-breakout][data-eq-pts]'), ns.Breakout.process);
        }
    };

    if (ns.Breakout.hasOwnProperty('evaluate') && typeof ns.Breakout.evaluate === 'function') {
        ns.Breakout.evaluate();

        $(env.win).resize(ns.Breakout.evaluate);
    }
})({win: window, doc: document}, jQuery, eqjs, window.CNN);

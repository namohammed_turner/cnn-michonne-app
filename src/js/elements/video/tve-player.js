/* global CNN */

CNN.INJECTOR.executeFeature('videoLoader', {params:'?version=latest&client=expansion'}).then(function () {
    'use strict';

    /**
     * Javascript to create new document fragments for Watch CNN.
     *
     * @name CNN.VideoDocFragment
     * @namespace
     * @memberOf CNN
     */
    CNN.VideoDocFragment = CNN.VideoDocFragment || {};

    /**
     * Functionality unique to the Authenticated Player.
     *
     * Javascript Module Pattern
     * @see http://www.adequatelygood.com/JavaScript-Module-Pattern-In-Depth.html
     *
     * @name CNN.VideoDocFragment.authPlayer
     * @namespace
     * @memberOf  CNN
     *
     * @requires CNN.VideoAjax.NowPlaying
     * @todo  Consider internationalization.
     */
    CNN.VideoDocFragment.authPlayer = CNN.VideoDocFragment.authPlayer || (function authPlayer() {

        var pub = CNN.VideoDocFragment.BaseClass(),
            config = {},
            cvpStream = null,
            cvpConfigObj = {};

        config = {
            playerId: (window.location.pathname.split('/')[3] || '').toLowerCase(),
            tvePlayerId: 'js-tve-player',
            $authPlayer: jQuery(document.getElementById('js-tve-player-desktop')),
            authPlayerTitleSelector: '.js-authstream__title'
        };

        /* Wait until the document is ready and then listen for both possible events */
        jQuery(function authPlayerDocReady() {
            window.CNNVideoAPILoadCompleteHandlers = window.CNNVideoAPILoadCompleteHandlers || [];

            pub.setEventName(CNN.VideoAjax.NowPlaying.getEventName());
            pub.setEventName(CNN.VideoAjax.LiveStream.getEventName());

            window.CNNVideoAPILoadCompleteHandlers.push(videoLoadCompleteHandler);
        });

        function videoLoadCompleteHandler() {
            var isAuthPlayer = false;

            if (typeof window.CNN.VideoAjax === 'undefined' || typeof window.CNN.VideoAjax.NowPlaying === 'undefined') {
                throw 'Cannot fetch Now Playing Schedule. CNN.VideoAjax is undefined.';
            }

            window.CNN.VideoDocFragment.newFragment('authPlayer', {
                network: config.playerId,
                selector: config.authPlayerTitleSelector
            });

            switch (config.playerId) {
            case 'cnn':
                cvpStream = 'cvptve/cvpstream1';
                isAuthPlayer = true;
                break;
            case 'hln':
                cvpStream = 'cvptve/cvpstream2';
                isAuthPlayer = true;
                break;
            case 'live-1':
                cvpStream = 'cvplive/cvpstream1';
                break;
            case 'live-2':
                cvpStream = 'cvplive/cvpstream2';
                break;
            case 'live-3':
                cvpStream = 'cvplive/cvpstream3';
                break;
            case 'live-4':
                cvpStream = 'cvplive/cvpstream4';
                break;
            default:
                throw 'playerId is not properly set';
            }

            pub.setCvpConfig();

            if (isAuthPlayer === true && window.CNN.VideoPlayer.isMobileClient()) {

                jQuery.getScript(CNN.Host.assetPath + '/js/tve-mobile.min.js')
                    .done(function getTveMobileScriptDoneHandler() {
                        window.CNN.TVEMobile.isMobile();
                    })
                    .fail(function getTveMobileScriptFailHandler() {
                        throw 'Unable to fetch /js/tve-mobile.min.js';
                    });

            } else if (isAuthPlayer === false) {
                /* load CVP without authentication and initialize video */
                jQuery(document.getElementById(config.tvePlayerId)).show();
                if (CNN.VideoPlayer.apiInitialized) {
                    window.cnnVideoManager.renderMultipleContainers({videoConfig:cvpConfigObj});
                } else {
                    CNN.VideoPlayer.addVideo(cvpConfigObj);
                }


                window.CNN.VideoAjax.NowPlaying.init();

                if (typeof window.cnnVideoManager !== 'undefined') {
                    CNN.VideoPlayer.initVideos();
                }
            }
        }

        /**
         * Fetch the data from the now-playing-schedule.json file.
         *
         * @param   {object} card  - The individual card to work with.
         *
         * @memberOf CNN.VideoDocFragment.authPlayer
         * @requires CNN.VideoAjax.NowPlaying
         * @requires CNN.VideoAjax.LiveStream
         */
        pub.fetchData = function (card) {
            var i = 0,
                cardStr = '',
                nowPlayingReady = false,
                liveStreamReady = false,
                checkAjaxCompleteTimer = null;

            function setFetchOnNowInterval() {
                nowPlayingReady = (nowPlayingReady) ? true : !CNN.VideoAjax.NowPlaying.getIsFetching();
                liveStreamReady = (liveStreamReady) ? true : !CNN.VideoAjax.LiveStream.getIsFetching();
            }

            /* Stall execution of the function until both event listeners fired at least once. */
            for (i = 120; i >= 0; i--) {
                checkAjaxCompleteTimer = setTimeout(setFetchOnNowInterval, 500);

                /* Stop checking once both variables are true. */
                if (nowPlayingReady === liveStreamReady === true) {
                    clearTimeout(checkAjaxCompleteTimer);
                    checkAjaxCompleteTimer = null;

                    break;
                } else {
                    /* This is taking too long. Exit */
                    return;
                }
            }

            /* Set the raw data for the player from either the NowPlaying data or LiveStream data */
            card.data.raw = CNN.VideoAjax.NowPlaying.filterNetwork(config.playerId) || CNN.VideoAjax.LiveStream.filterNetwork(config.playerId);

            /* Stringify the result for change comparison */
            cardStr = JSON.stringify(card.data.raw);

            /* Only update the DOM if the data has changed */
            if (cardStr !== card.data.raw) {
                card.data.string = cardStr;

                card.docFragment = CNN.VideoDocFragment.fragments.Player();
                card.docFragment.buildDocFragment(card);

                pub.updateDom(card);
            }
        };

        /**
         * Function to repaint the DOM.
         *
         * @param   {object} card  - The card to update.
         *
         * @memberOf CNN.VideoDocFragment.authPlayer
         */
        pub.updateDom = function (card) {
            if (typeof card === 'undefined') {
                return;
            }

            card.$parent.empty().append(card.docFragment.frag);
            jQuery('.js-authstream__caption').replaceWith('<div class="media__caption">' + card.data.raw.description + '</div>');
        };

        /**
         * Getter for playerId
         *
         * @returns {string}  - the ID of the player.
         * @memberOf CNN.VideoDocFragment.authPlayer
         */
        pub.getPlayerId = function () {
            return config.playerId;
        };

        pub.setCvpConfig = function () {
            cvpConfigObj = {
                video: cvpStream,
                width: '100%',
                height: '100%',
                section: config.$authPlayer.find('.js-auth__edition').val(),
                profile: 'expansion',
                context: 'watch_cnn',
                network: config.$authPlayer.find('.js-auth__network').val(),
                markupId: config.tvePlayerId,
                autostart: true
            };
        };

        return pub;
    }());
});

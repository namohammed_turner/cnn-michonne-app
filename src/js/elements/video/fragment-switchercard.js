/* global CNN */

/**
 * Javascript to create new document fragmetns for Watch CNN.
 *
 * @name CNN.VideoDocFragment
 * @namespace
 * @memberOf CNN
 */
CNN.VideoDocFragment = CNN.VideoDocFragment || {};
CNN.VideoDocFragment.fragments = CNN.VideoDocFragment.fragments || {};

/**
 * Constructor for creating SwitcherCards. This will create the document-fragment skeleton and add the classes.
 *
 * Javascript Module Pattern
 * @see http://www.adequatelygood.com/JavaScript-Module-Pattern-In-Depth.html
 *
 * @example
 * <!-- Renders the following: -->
 * <div class="media">
 *     <a href="/about/tv">
 *         <img src="//i.cdn.turner.com/cnn/2013/images/12/09/newsroom_tve-300x169-mb-2.jpg" alt="CNN Newsroom">
 *     </a>
 *     <a href="/about/tv">
 *         <div class="media__overlay"></div>
 *         <div class="media__over-text">Now Playing</div>
 *     </a>
 * </div>
 * <div class="cd__content">
 *     <div class="cd__kicker">CNN TV</div>
 *     <h3 class="cd__headline">
 *         <a href="/about/tv">
 *             <span class="cd__headline-text">CNN Newsroom</span>
 *         </a>
 *     </h3>
 * </div>
 * <div class="el-action-bar el-action-bar--watch-cnn">
 *     <a class="el-button el-button--watch-cnn" href="/">Watch CNN</a>
 * </div>
 *
 * @name CNN.VideoDocFragment.fragments.SwitcherCards
 * @namespace
 * @memberOf  CNN
 */
CNN.VideoDocFragment.fragments.SwitcherCard = CNN.VideoDocFragment.fragments.SwitcherCard || (function SwitcherCard() {
    'use strict';

    /* Create a closure. Variables in here will be unique the instance. */
    return function SwitcherCard() {
        var pub = {};

        pub.elms = {};
        pub.frag = null;

        /**
         * Create the base skeleton of the document fragment.
         *
         * @param   {object} data  - Data returned from the server.
         * @returns {object}       - the document fragment skeleton.
         *
         * @memberOf CNN.VideoDocFragment.fragments.SwitcherCards
         */
        function createCard(data) {
            /* Create all the document elements. */
            var i = 0,
                media = {
                    tag: document.createElement('div'),
                    link: document.createElement('a'),
                    overlay: document.createElement('div'),
                    overlayText: document.createElement('div'),
                    image: {
                        tag: document.createElement('img'),
                        link: document.createElement('a')
                    }
                },
                container = {
                    tag: document.createElement('div'),
                    kicker: document.createElement('div'),
                    headline: {
                        tag: document.createElement('h3'),
                        link: document.createElement('a'),
                        span: document.createElement('span')
                    }
                },
                actionBar = {
                    tag: document.createElement('div'),
                    link: document.createElement('a')
                };

            /* Add the class names */
            media.tag.className               = 'media';
            container.kicker.className        = 'cd__kicker';
            container.headline.span.className = 'cd__headline-text';
            container.headline.tag.className  = 'cd__headline';
            container.tag.className           = 'cd__content';
            actionBar.link.className          = 'el-button el-button--watch-cnn';
            actionBar.tag.className           = 'el-action-bar el-action-bar--watch-cnn';

            /* Add static text */
            actionBar.link.innerHTML     = 'Watch CNN';

            /* Nest the fragments */
            media.image.link.appendChild(media.image.tag);
            media.tag.appendChild(media.image.link);
            container.headline.link.appendChild(container.headline.span);
            container.headline.tag.appendChild(container.headline.link);
            container.tag.appendChild(container.kicker);
            container.tag.appendChild(container.headline.tag);
            actionBar.tag.appendChild(actionBar.link);

            /* Loop through all the cards to find the one for the TVE Player */
            for (i = CNN.VideoDocFragment.cards.length - 1; i >= 0; i--) {
                if (CNN.VideoDocFragment.cards[i].type === 'authPlayer') {
                    /* If the network matches the TVE, add the overlay */
                    if (CNN.VideoDocFragment.cards[i].network.toLowerCase() === data.id.toLowerCase()) {
                        media.overlay.className     = 'media__overlay';
                        media.overlayText.className = 'media__over-text';
                        media.overlayText.innerHTML = 'Now Playing';

                        media.link.appendChild(media.overlay);
                        media.link.appendChild(media.overlayText);
                        media.tag.appendChild(media.link);
                    }
                }

                media.link.appendChild(media.overlay);
                media.link.appendChild(media.overlayText);
            }

            return {
                media: media,
                container: container,
                actionBar: actionBar
            };
        }

        /**
         * Add server data to the document fragment and finish bulding the final fragment.
         *
         * @param   {object} card  - The card to work with.
         *
         * @memberOf CNN.VideoDocFragment.fragments.SwitcherCards
         */
        pub.buildDocFragment = function (card) {
            var data = card.data.raw,
                elms = card.docFragment.elms,
                cnnxStreamUrl = CNN.CNNXStreamUrl, /* get cnnx url value from runtime config */
                liveStreamUrl = '/tv/watch/', /* live streams are to play within expansion */
                streamUrl = '';

            if (typeof card === 'undefined') {
                throw 'card is not defined';
            } else if (card.data.raw === null) {
                throw 'card.data.raw is null. Cannot build fragment.';
            }

            /* Populate the Elements */
            elms = createCard(data);

            /* Determine streamUrl to use */
            if (cnnxStreamUrl && (data.id === 'CNN' || data.id === 'HLN')) {
                streamUrl = cnnxStreamUrl + data.id;
            } else {
                streamUrl = liveStreamUrl + data.id;
            }

            /* Populate HTML. In theory, these should be the only parts that change. */
            elms.media.image.tag.src               = data.images['120x68'];
            elms.media.image.tag.alt               = data.program;
            elms.media.image.link.href             = streamUrl;
            elms.media.link.href                   = streamUrl;
            elms.container.kicker.innerHTML        = data.kicker || data.id + ' TV';
            elms.container.headline.span.innerHTML = data.program;
            elms.container.headline.link.href      = streamUrl;
            elms.actionBar.link.href               = streamUrl;

            /* Create and assemble the final document fragment. */
            card.docFragment.frag = document.createDocumentFragment();

            card.docFragment.frag.appendChild(elms.media.tag);
            card.docFragment.frag.appendChild(elms.container.tag);
            card.docFragment.frag.appendChild(elms.actionBar.tag);

            /* Cache the generated elements */
            card.docFragment.elms = elms;
        };

        return pub;
    };
}());

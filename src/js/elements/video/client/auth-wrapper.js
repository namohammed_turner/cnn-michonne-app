/* global CVP, CNN, Modernizr */

CNN.omniture = CNN.omniture || {};
CNN.VideoConfig = CNN.VideoConfig || {};
CNN.VideoPlayer = CNN.VideoPlayer || {};

/**
 * Authenticates a user with a TempPass (for a CNNgo Free Preview) or
 * with MVPD (Multichannel Video Programming Distributor) provider credentials,
 * and plays the CNNgo authenticated live stream.
 * @see {@link http://docs.turner.com/display/CNNX/10-Minute+Preview#id-10-MinutePreview-2.0FunctionalRequirements|
 * Free Preview Use Cases and Functional Requirements}
 * @see {@link http://docs.turner.com/display/MPDTCV/CVP+Auth+JS+Library+with+TVE+Picker|Auth Library and TVE Picker}
 * @see {@link http://docs.turner.com/display/analytics/TVE+Picker+Page+API|Analytics TVE Picker Page API}
 * @see {@link http://cvpdev.turner.com/dmtvideo/jeff/ngtv/auth.html|Implementation based on the CVP Team's Auth demo}
 * @module playCnnGo
 * @param {string} wrapperId - The unique HTML elment ID of the container housing all the CNNgo UI elements.
 * @param {string} playerDivId - The unique HTML element ID of the element where the CNNgo authenticated live stream will be embedded.
 * @param {string} network - Video Loader parameter, received from dust template, e.g. cnn or cnn-test.
 * @param {string} section - Video Loader parameter, received from dust template, e.g. domestic or international.
 * @param {string} context - Video Loader parameter, received from dust template, e.g. us, world, video, depending on what page the player is loaded in.
 * @param {string} adsection - Video Loader parameter, received from dust template.
 */
CNN.VideoPlayer.playCnnGo = function playCnnGo(wrapperId, playerDivId, network, section, context, adsection) {
    'use strict';
    var adobeHashId = '',
        authNExpireTime,
        authNExpireTimeInMilliseconds,
        authWrapper,
        /* authZExpireTime, */
        autoAuth = true,
        clientId = 'CNN',
        cnnGoPlayer,
        customType1Inbetweener,
        freePreviewId,
        goFreePreviewConfig = CNN.GoFreePreviewConfig || {},
        isInternetExplorer = Modernizr.ie,
        lastExpiredCheck = 0,
        mvpdConfigObject,
        persistentCtaElementClone,
        playerExpired = false,
        playerInitialized = false,
        playVideoCalled = false,
        showExpiredMessage = false,
        slatesWrapper,
        transitionTime = 500,
        videoId,
        wrapper,
        wrapperHeight,
        wrapperMinHeight = 300,
        wrapperMinWidth = 400,
        wrapperWidth,
        /* Elements */
        closeButtonElement,
        countdownContainerElement,
        countdownElement,
        countdownElementClone = false,
        cobrandingContainerElement,
        cobrandingElement,
        mediaVideoThumbnailElement,
        persistentCtaElement,
        playerElement,
        /* videoSlateCloseButtonElement,
        videoSlateElement, */
        watchLiveTvCtaElement;
    freePreviewId = goFreePreviewConfig.temppass || '';

    /**
     * Can set CNN.goActiveUntil to the Free Preview expiration time
     * as a Javascript/Node epoch integer (unix timestamp in milliseconds) to disable page auto refresh
     * until the free preview expires or is closed by the user.
     * @method goActiveUntil
     * @param {number} timestampInMilliseconds
     * Free Preview expiration time in milliseconds.
     * Comes from the onMetadata callback during the authentication process.
     */
    function goActiveUntil(timestampInMilliseconds) {
        CNN.goActiveUntil = typeof timestampInMilliseconds === 'number' ? timestampInMilliseconds : 0;
    }

    function hideVisibleSlate() {
        var slates = slatesWrapper.find('.animate-show');
        slates.removeClass('animate-show');
        /*
         * Half second timeout coincides with the half second opacity CSS transition.
         * This timeout is used because if we remove the 'visible' class with the other classes above,
         * the opacity CSS transition does not occur.
         */
        setTimeout(function () {
            slates.removeClass('visible');
        }, transitionTime);
    }

    function playerAvailable() {
        return CNN.Utils.exists(cnnGoPlayer) && playerInitialized && !playerExpired;
    }

    function showMediaThumbnail() {
        if (playerAvailable()) {
            cnnGoPlayer.pause();
        }
        hideVisibleSlate();
        slatesWrapper.removeClass('animate-show minimum-dimensions');
        setTimeout(function () {
            slatesWrapper.removeClass('visible');
        }, transitionTime);
        mediaVideoThumbnailElement.removeClass('animate-hide');
        watchLiveTvCtaElement.show();
        goActiveUntil();
    }

    /**
     * Check the wrapper dimensions and increase it if the minimum dimensions of 400px by 300px
     * are not satisfied. This must be done before playing video because in Google Chrome,
     * plugins that have dimensions less than 400px width AND/OR 300px height face increased
     * load times and require users to make an additional click to start video play.
     * @method toggleWrapperDimensions
     */
    function toggleWrapperDimensions() {
        if (slatesWrapper.hasClass('minimum-dimensions')) {
            setTimeout(function () {
                slatesWrapper.removeClass('minimum-dimensions');
                if (playerAvailable()) {
                    cnnGoPlayer.resize(wrapperWidth, wrapperHeight, 0.25);
                }
            }, goFreePreviewConfig.removeMinimumDimensionsTimeout);
        } else if (wrapperHeight < wrapperMinHeight || wrapperWidth < wrapperMinWidth) {
            slatesWrapper.addClass('minimum-dimensions');
        }
    }

    function showSlate(cssClass) {
        var selectorClasses = '.' + cssClass,
            addedClasses = 'visible animate-show',
            slate = slatesWrapper.find('.' + cssClass);
        if (slate.hasClass('visible') && slate.hasClass('animate-show')) {
            return;
        }
        hideVisibleSlate();
        mediaVideoThumbnailElement.addClass('animate-hide');
        selectorClasses += ', .slates-wrapper';
        /* Add classes to show the desired slate */
        wrapper.find(selectorClasses).addClass(addedClasses);
        slatesWrapper.find('.current-slate').removeClass('current-slate');
        /* Make this slate the current slate */
        slate.addClass('current-slate');

        if (cssClass !== 'current-slate' && cssClass !== 'block-slate') {
            toggleWrapperDimensions();
        }
    }

    function stopPlayer() {
        if (playerAvailable()) {
            cnnGoPlayer.stop();
            playerExpired = true;
            playVideoCalled = false;
        }
    }

    function showErrorSlate(code) {
        stopPlayer();
        /* Authorization failed because user has no subscription for the requested resource. */
        if (code === 'Z100') {
            slatesWrapper.find('.subscription-error-message').show();
        } else {
            /* User is not authenticated (N000, N005, N111, or DRM error codes) */
            slatesWrapper.find('.default-error-message').show();
        }
        showSlate('error-slate');
    }

    function showPicker() {
        CNN.omniture.mvpd = 'no mvpd set';
        showMediaThumbnail();
        stopPlayer();
        authWrapper.showExpiredMessage(true);
        playerExpired = true;
        /* Auth library call to open the TVE picker */
        authWrapper.getAuthentication();
    }

    function removeCobranding() {
        cobrandingElement.empty();
    }

    function displayCobranding(mvpdConfig) {
        var cobrand,
            image;

        removeCobranding();
        /* Verify that you have received an mvpdConfig and that it has cobranding */
        if (mvpdConfig && mvpdConfig.cobrand && mvpdConfig.cobrand.length > 0) {
            cobrand = mvpdConfig.cobrand[0];
            image = jQuery('<img/>', {
                src: cobrand.url
            });
            image.appendTo(cobrandingElement);
            cobrandingContainerElement.show();
        }
    }

    /**
     * Plays the authenticated live stream using the token generated in the authentication and authorization process.
     * @method play
     * @param {string} token - Generated token to use
     */
    function play(token) {
        playerExpired = false;
        if (playerAvailable() && !playVideoCalled) {
            videoId = playerElement.data('video-id');
            window.cnnVideoManager.playVideo(playerDivId, videoId, {
                accessToken: token.accessToken,
                accessTokenType: token.accessTokenType,
                mvpd: token.mvpd,
                tokenParams: token.tokenParams,
                mvpdId: mvpdConfigObject.id,
                adobeHashId: adobeHashId
            });
            playVideoCalled = true;
            goActiveUntil(authNExpireTimeInMilliseconds);
        } else if (!playerAvailable()) {
            setTimeout(function () {
                play(token);
            }, 1000);
        }
    }

    function convertSec(sec) {
        var hours,
            minutes,
            seconds;

        if (sec > 0) {
            hours = Math.floor(sec / 3600);
            minutes = hours > 0 ? Math.floor((sec - (hours * 3600)) / 60) : Math.floor(sec / 60);
            seconds = Math.floor(sec % 60);
            hours = hours < 10 ? '0' + hours : hours;
            minutes = minutes < 10 ? '0' + minutes : minutes;
            seconds = seconds < 10 ? '0' + seconds : seconds;
            return (hours > 0 ? hours + ':' : '') + minutes + ':' + seconds;
        } else {
            return '00:00';
        }
    }

    /**
     * Counts down to the expiration time.
     * @method runExpireCheck
     */
    function runExpireCheck() {
        var now = new Date(),
            authNtime = (authNExpireTime - now) / 1000,
            convertedTime;
        if (lastExpiredCheck === 0) {
            lastExpiredCheck = authNtime;
        }
        if (mvpdConfigObject.id === freePreviewId) {
            convertedTime = convertSec(authNtime);
            if (countdownElement && countdownElement.length > 0) {
                countdownElement.html(convertedTime);
            }
            if (countdownElementClone && countdownElementClone.length > 0) {
                countdownElementClone.html(convertedTime);
            }
        }
        if (authWrapper.hasAuthN()) {
            if (authNtime > 0) {
                if (lastExpiredCheck > authNtime + 60) {
                    lastExpiredCheck = authNtime;
                    authWrapper.checkAuthentication();
                }
                setTimeout(runExpireCheck, 1000);
            } else {
                authWrapper.checkAuthentication();
            }
        }
    }

    function setEventListeners() {
        /* Click event to reload the page when auth error occurs */
        slatesWrapper.find('.retry').on('click', function () {
            window.location.reload();
            return false;
        });

        /* Click event to pause the player and close the UI */
        closeButtonElement.on('click', function () {
            showMediaThumbnail();
            /* Send analytics */
            window.sendHP10Interaction('sign-in exit');
        });

        /* Remove existing events from the Watch Live TV CTA */
        watchLiveTvCtaElement.off();

        /* Click event to return to a slate after closing the UI */
        watchLiveTvCtaElement.on('click', function () {
            if (playerAvailable()) {
                cnnGoPlayer.resume();
                showSlate('current-slate');
                goActiveUntil(authNExpireTimeInMilliseconds);
            } else {
                if (CNN.Utils.exists(authWrapper)) {
                    showPicker();
                } else {
                    showSlate('current-slate');
                }
            }
            return false;
        });

        /* Click event for the end slate sign in link */
        slatesWrapper.find('.end-slate .cta .smart-link').on('click', function () {
            /* Send analytics */
            window.sendHP10Interaction('sign-in expired');
        });

        /* Click event for the video slate persistent CTA smart link */
        slatesWrapper.on('click', '.video-slate .persistent-cta .smart-link', function () {
            /* Send analytics */
            window.sendHP10Interaction('experience cnngo');
        });
    }

    /**
     * Setup CVP Auth library options, callbacks, and initialize.
     * @method AUTH_WRAPPER
     * @return {object} AUTH_WRAPPER instance with exposed functions
     */
    function AUTH_WRAPPER() {
        var AUTH,
            AUTH_READY = false,
            AUTH_N_STATUS = false,
            isPagePreview = true,
            PICKER_STATES = {
                findbylogo: 'tve:picker:icon',
                findbyname: 'tve:picker:list',
                noprovider: 'tve:picker:provider not available',
                'default': 'tve:picker:default'
            },
            SELECT_PICKER_STATE = '';

        function logout() {
            if (AUTH_READY) {
                AUTH.logout();
            }
        }

        function getAuthentication() {
            AUTH.getAuthentication();
        }

        function getAuthorization(broadcaster, feature) {
            AUTH.checkAuthorization(broadcaster, feature);
        }

        function checkAuthentication() {
            AUTH.isAuthenticated();
        }

        function checkFeature(value) {
            return AUTH.checkFeature(value);
        }

        function getMetadata(value, arg) {
            AUTH.getMetadata(value, arg);
        }

        function login(id) {
            AUTH.login(id);
        }

        function isInvalidMpvdId(mvpdId) {
            return mvpdId === null;
        }
        function pickerTrackMetrics(data) {
            var analyticsData,
                noMvpdSet = 'no mvpd set';
            if (CNN.Utils.exists(data) && typeof data.type === 'string') {
                analyticsData = {
                    page_name: data.page_name || PICKER_STATES['default'],
                    tve_mvpd: data.tve_mvpd || noMvpdSet,
                    authentication_state: 'requires authentication',
                    presentation_template: 'nvs',
                    orientation: 'nvs',
                    hash_id: data.tve_mvpd && data.tve_mvpd !== noMvpdSet ? jQuery.md5(data.tve_mvpd.toLowerCase()) : noMvpdSet,
                    free_preview: (CNN.omniture && CNN.omniture.free_preview) || 'nvs'
                };
                if (data.is_page_preview) {
                    analyticsData.is_page_preview = data.is_page_preview;
                }
                /* Send analytics */
                try {
                    window.trackMetrics({
                        type: data.type,
                        data: analyticsData
                    });
                } catch (e) {}
            }
        }

        function init() {
            if (!AUTH_READY) {
                AUTH = CVP.AuthManager.init({
                    site: 'cnn',
                    clientId: clientId,
                    vendorEnv: goFreePreviewConfig.adobeEnvironment,
                    refreshlessLogin: true,
                    refreshlessLogout: true,
                    hiddenMvpdIds: [freePreviewId],
                    /*
                     * Need to host the AccessEnablerLoader.swf locally
                     * to work around Google Chrome 53 third-party .swf file blocking.
                     */
                    adobeSwfUrl: goFreePreviewConfig.adobeSwfPath,
                    /* Start Auth Manager callbacks */
                    onInitReady: function () {
                        AUTH_READY = true;
                        AUTH.isAuthenticated();
                    },
                    onInitFailure: function () {
                    },
                    onAuthenticationPassed: function (mvpdId, mvpdConfig) {
                        var data = JSON.parse(localStorage.getItem('CNNgoFreePreview'));

                        if (isInvalidMpvdId(mvpdId)) {
                            this.onAuthenticationFailed();
                            return;
                        }

                        CNN.omniture.mvpd = (typeof mvpdId === 'string') ? mvpdId.toLowerCase() : mvpdId;

                        if (!AUTH_N_STATUS && SELECT_PICKER_STATE === 'success') {
                            pickerTrackMetrics({
                                type: 'picker-pageview',
                                page_name: 'tve: successful login',
                                tve_mvpd: mvpdId
                            });
                        }

                        AUTH_N_STATUS = true;

                        getMetadata('TTL_AUTHN');
                        getAuthorization(clientId, 'ngtv');
                        mvpdConfigObject = mvpdConfig || {};
                        jQuery([countdownContainerElement, cobrandingContainerElement]).each(function () {
                            this.hide();
                        });

                        /* Use case 1, 2b, 4 */
                        if (mvpdConfigObject.id === freePreviewId) {
                            autoAuth = false;
                            countdownContainerElement.show();
                        } else { /* Use case 5 */
                            autoAuth = true;
                            /* Track free preview metrics */
                            if (data && !data.dayAuthenticated) {
                                data.dayAuthenticated = data.dayCount;
                                localStorage.setItem('CNNgoFreePreview', JSON.stringify(data));
                            }
                            displayCobranding(mvpdConfigObject);
                        }
                    },
                    onAuthenticationFailed: function (code, _msg) {
                        AUTH_N_STATUS = false;
                        if (autoAuth) {
                            autoAuth = false;
                            authWrapper.login(freePreviewId);
                        } else if (code === 'N111') {
                            /* Use Case 7: TempPass expires from countdown/while playing */
                            showPicker();
                        } else {
                            /* Use Case 6: not authenticated */
                            if (code === 'N005') {/* The MVPD picker was canceled */
                                showMediaThumbnail();
                            } else if (code !== 'N000') {/* Adobe Errors */
                                showErrorSlate(code);
                            } else {/* User is not authenticated */
                                showPicker();
                            }
                        }
                    },
                    onAuthorizationPassed: function (_resource) {
                        lastExpiredCheck = 0;
                        runExpireCheck();
                        play(AUTH.getAccessToken());
                    },
                    onAuthorizationFailed: function (_resource, errorCode, _errorString) {
                        showErrorSlate(errorCode);
                    },
                    onTrackingData: function (trackingEventType, trackingData) {
                        /*
                         * trackingData array:
                         * [0] Whether the token request was successful [true/false] and if true:
                         * [1] MVPD ID [string]
                         * [2] User ID (md5 hashed) [string]
                         * [3] Whether it was cached or not [true/false]
                         */
                        if (trackingEventType && trackingEventType === 'authenticationDetection') {
                            if (trackingData && trackingData[0] === true) {
                                adobeHashId = jQuery.md5(trackingData[1].toLowerCase());
                            }
                        }
                    },
                    onMetadataStatus: function (key, value) {
                        switch (key) {
                        case 'TTL_AUTHN':
                            authNExpireTime = new Date(Number(value));
                            authNExpireTimeInMilliseconds = parseInt(value, 10);
                            break;
                        case 'TTL_AUTHZ':
                            /* never used: authZExpireTime = new Date(Number(value)); */
                            break;
                        }
                    },
                    onError: function (_source, _data) {
                        /* Catch Adobe errors located in the data object */
                    },
                    onPickerCloseClicked: function (_state) {
                    },
                    onPickerClosed: function () {
                    },
                    onPickerHelpClicked: function (_state) {
                        window.open(goFreePreviewConfig.helpLink, '_blank');
                    },
                    onPickerMvpdSelected: function (mvpdId) {
                        if (SELECT_PICKER_STATE !== '') {
                            pickerTrackMetrics({
                                type: 'picker-click',
                                page_name: PICKER_STATES[SELECT_PICKER_STATE],
                                tve_mvpd: mvpdId
                            });
                        }
                    },
                    onPickerOpened: function () {
                        document.body.querySelector('#mvpdpicker > .steps').classList.remove('expired');
                        if (showExpiredMessage) {
                            document.body.querySelector('#mvpdpicker > .steps').classList.add('expired');
                            showExpiredMessage = false;
                        }
                    },
                    onPickerStateChange: function (state) {
                        /* Close "Watch Now" slate */
                        if (state === 'success') {
                            SELECT_PICKER_STATE = state;
                            showSlate('cnngo-loading-slate');
                            AUTH.closePicker();
                        } else if (state !== 'signin' && state !== 'success') {
                            if (SELECT_PICKER_STATE !== '') {
                                isPagePreview = false;
                            }
                            SELECT_PICKER_STATE = state;
                            pickerTrackMetrics({
                                type: 'picker-pageview',
                                page_name: PICKER_STATES[state],
                                tve_mvpd: 'no mvpd set',
                                is_page_preview: isPagePreview
                            });
                        }
                    }
                    /* End Auth Manager callbacks */
                });
            }
            return {};
        }

        /* return public interface */
        return {
            init: function () {
                return init();
            },
            logout: function () {
                logout();
            },
            getAuthentication: function () {
                getAuthentication();
            },
            getAuthorization: function (broadcaster, feature) {
                getAuthorization(broadcaster, feature);
            },
            checkAuthentication: function () {
                checkAuthentication();
            },
            checkFeature: function (value) {
                return checkFeature(value);
            },
            hasAuthN: function () {
                return AUTH_N_STATUS;
            },
            getMetadata: function (value, arg) {
                getMetadata(value, arg);
            },
            login: function (id) {
                login(id);
            },
            showExpiredMessage: function (show) {
                showExpiredMessage = show;
            }
        };
    }

    function initializeAuthLibrary() {
        authWrapper = AUTH_WRAPPER();
        authWrapper.init();
    }

    /*
     * Keep the video player in the background until video playback begins.
     * Needs to be done in Internet Explorer because Internet Explorer
     * does not follow the z-index layering rule that a child element's
     * z-index can only be less than or equal to its parent element's z-index.
     */
    function putHtmlObjectInTheBackground() {
        playerElement.addClass('background-layer-for-internet-explorer');
    }

    function videoPinnerClonePersistentCta() {
        /* Create a clone of the top right persistent call-to-action to use when the free preview video is pinned */
        persistentCtaElementClone = persistentCtaElement.clone();
        customType1Inbetweener.html(persistentCtaElementClone);
        /* If this is a free preview, make the clone countdown too */
        countdownElementClone = (CNN.Utils.exists(mvpdConfigObject) && mvpdConfigObject.id === freePreviewId) ?
            persistentCtaElementClone.find('.countdown') : false;
    }

    function videoPinnerCheck(thisObj) {
        if (CNN.Features.enableVideoPinning) {
            if (CNN.Utils.exists(thisObj.videoPinner)) {
                videoPinnerClonePersistentCta();
                thisObj.videoPinner.setIsPlaying(true);
                thisObj.videoPinner.handleOnVideoPlay();
                thisObj.videoPinner.animateDown();
            }
        }
    }

    function onPlay(thisObj) {
        if (isInternetExplorer) {
            playerElement.removeClass('background-layer-for-internet-explorer');
        }
        showSlate('video-slate');
        videoPinnerCheck(thisObj);
    }

    /*
     * Create our own type1 inbetweener-pinner outside of the Video Pinner JavaScript,
     * to use as a container for the cloned persistent call-to-action.
     * Our inbetweener will pick up the necessary CSS positioning when the
     * Video Pinner JavaScript is initialized, since it shares the same
     * "js-inbetweener-pinner--type1" class name that is used for positioning in the
     * Video Pinner JavaScript.
     */
    function videoPinnerPrep(containerClassId) {
        customType1Inbetweener = jQuery('<div class="js-inbetweener-pinner--type1 inbetweener-pinner--type1 free-preview-video-pinning"></div>');
        customType1Inbetweener.insertAfter(containerClassId + ' object');
    }

    function videoPinnerSetup(thisObj, containerId) {
        var containerClassId;
        if (CNN.Features.enableVideoPinning) {
            containerClassId = '#' + containerId;
            if (jQuery(document.getElementById(containerId)).parents('.js-pg-rail-tall__head').length > 0) {
                videoPinnerPrep(containerClassId);
                thisObj.videoPinner = new CNN.VideoPinner(containerClassId);
                thisObj.videoPinner.setIsVideoCollection(false);
                thisObj.videoPinner.init();
            }
        }
    }

    /**
     * Setup Video Loader configs, callbacks, and embed the video player.
     * @method setupPlayer
     */
    function setupPlayer() {
        var configObj,
            callbackObj;
        if (!playerAvailable()) {
            configObj = {
                thumb: 'none',
                video: '',
                width: '100%',
                height: '100%',
                section: section,
                context: context,
                cvpContext: 'cnngo',
                cvpId: 'cnngo',
                profile: 'expansion',
                network: network,
                markupId: playerDivId,
                adsection: adsection,
                frameWidth: '100%',
                frameHeight: '100%',
                autostart: true,
                allowFullScreen: 'false',
                analytics: 'jsmdCNNgoFreePreview',
                aspenContext: goFreePreviewConfig.aspenContext
            };
            callbackObj = {
                onPlayerReady: function (containerId) {
                    playerInitialized = true;
                    CNN.VideoPlayer.handleAdOnCVPVisibilityChange(containerId, CNN.pageVis.isDocumentVisible());
                    cnnGoPlayer = window.cnnVideoManager.getPlayerByContainer(containerId).videoInstance.cvp;
                    videoPinnerSetup(this, containerId);
                },
                onContentBegin: function (containerId, _cvpId, _contentId) {
                    CNN.VideoPlayer.reverseAutoMute(containerId);
                },
                onContentPlay: function () {
                    onPlay(this);
                },
                onAdPlay: function () {
                    onPlay(this);
                },
                onCVPVisibilityChange: function (containerId, _cvpId, visible) {
                    CNN.VideoPlayer.handleAdOnCVPVisibilityChange(containerId, visible);
                }
            };
            /* Embed video player */
            if (CNN.VideoPlayer.apiInitialized) {
                CNN.VideoPlayer.render(configObj, callbackObj);
            } else {
                CNN.VideoPlayer.addVideo(configObj, callbackObj);
            }
            if (isInternetExplorer) {
                putHtmlObjectInTheBackground();
            }
        }
    }

    function start() {
        CNN.omniture.mvpd = 'no mvpd set';
        showSlate('cnngo-loading-slate');
        /*
         * As a means of optimizing video load time, we make the call to setup and embed the video player,
         * then make the call to initialize the auth library, so that the video embed and authentication are done in parallel.
         * This theoretically decreases load time by at least 2 seconds.
         */
        setupPlayer();
        try {
            initializeAuthLibrary();
        } catch (e) {
            showErrorSlate();
        }
    }

    /**
     * Run a Geo-location check using the JavaScript API sited in "CNN.GeoCheck".
     * @method liveVideoGeoCheck
     */
    function liveVideoGeoCheck() {
        if (CNN.VideoConfig.liveStream && CNN.VideoConfig.liveStream.geoCheckEnabled) {
            jQuery(document).onGeoCheckReady(function doGeoChecking() {
                if (CNN.GeoCheck.inTarget(CNN.VideoConfig.liveStream.geoCheckTargets)) {
                    /* In country, so start the player and auth setup */
                    if (CNN.VideoConfig.liveStream.flashSlate.enabled) {
                        if (Modernizr && !Modernizr.phone && !Modernizr.mobile && !Modernizr.tablet && !CNN.VideoPlayer.isFlashInstalled()) {
                            /* Show flash installation instructions on desktop only */
                            showSlate('flash-slate');
                        } else {
                            start();
                        }
                    } else {
                        start();
                    }
                } else {
                    /* Not in country, so show the "block" end slate */
                    showSlate('block-slate');
                }
            });
        } else {
            if (CNN.VideoConfig.liveStream.flashSlate.enabled) {
                if (Modernizr && !Modernizr.phone && !Modernizr.mobile && !Modernizr.tablet && !CNN.VideoPlayer.isFlashInstalled()) {
                    /* Show flash installation instructions on desktop only */
                    showSlate('flash-slate');
                } else {
                    start();
                }
            } else {
                start();
            }
        }
    }

    function setWrapperElements() {
        closeButtonElement = wrapper.find('.close-button');
        cobrandingContainerElement = wrapper.find('.cobranding-container');
        cobrandingElement = wrapper.find('.cobranding');
        countdownContainerElement = wrapper.find('.countdown-container');
        countdownElement = (countdownElement = wrapper.find('.countdown')).length > 0 ? countdownElement : false;
        mediaVideoThumbnailElement = wrapper.find('.media__video--thumbnail');
        persistentCtaElement = wrapper.find('.persistent-cta');
        playerElement = wrapper.find('#' + playerDivId);
        slatesWrapper = wrapper.find('.slates-wrapper');
        /* never used...
        videoSlateCloseButtonElement = wrapper.find('.video-slate .close-button');
        videoSlateElement = wrapper.find('.video-slate');
        */
        watchLiveTvCtaElement = wrapper.find('.watch-live-tv-cta');
        wrapperHeight = wrapper.height();
        wrapperWidth = wrapper.width();
    }

    function checkForWrapper() {
        if ((wrapper = jQuery(document.getElementById(wrapperId))).length === 0) {
            throw 'An element with the ID "' + wrapperId + '" could not be found.';
        } else {
            setWrapperElements();
        }
    }

    /**
     * Initialize the CNNgo Authentication Package.
     * First run a Geo Check, then setup the player, the authentication library, and event listeners.
     * @method initializeCnnGoAuthPackage
     */
    function initializeCnnGoAuthPackage() {
        checkForWrapper();
        liveVideoGeoCheck();
        setEventListeners();
    }

    initializeCnnGoAuthPackage();
};

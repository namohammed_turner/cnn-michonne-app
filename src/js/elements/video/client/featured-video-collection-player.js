/* global CNN, CNNVIDEOAPI, Modernizr, FAVE, fastdom */

/* TODO: Make a base class for this. Used to be video-client-base.js but removed this due to jshint issues */
CNN.Features = CNN.Features || {};
CNN.OutbrainVideoKPISrc = (CNN.Features.enableOutbrainVideoKPI && CNN.OutbrainVideoKPISrc) || '';
CNN.VIDEOCLIENT = CNN.VIDEOCLIENT || {};
CNN.VideoConfig = CNN.VideoConfig || {};
CNN.VideoConfig.collection = CNN.VideoConfig.collection || {};

(function ($) {
    'use strict';
    /* Toggle show/hide the description in the featured video section */
    $('.video__description-toggle').click(function () {
        var link = $(this);
        $('.video__description').toggle('slide', function () {
            if ($(this).is(':visible')) {
                link.text('Hide Description');
                link.removeClass('show-desc').addClass('hide-desc');
            } else {
                link.text('Show Description');
                link.removeClass('hide-desc').addClass('show-desc');
            }
        });
    });
    $('.js__video__collection__close--expandable').click(function closeCollectionPlayer(e) {
        var $videoplayer = $(e.target).parent(),
            $mediaText = $videoplayer.parent().find('.media__over-text');

        $videoplayer.removeClass('el__featured-video--open');
        $mediaText.closest('.cd--active').removeClass('cd--active');
        $mediaText.remove();
    });
}(jQuery));

/**
 * This is a collection of functions that supports the sunrise effect of
 * embedded video collections on leaf pages.
 *
 * @param {object} ns - Namespace where the function will live.
 *
 * @param {object} $j - The jQuery library.
 */
(function setupSunriseHandler(ns, $j) {
    'use strict';

    /**
     * Triggers the sunrise effect by adding a class on the markup. CSS
     * transistions kick in to make the player feel like it sunrises.
     *
     * @param {object} evt - The object holding information about the
     * event. See jQuery's documentation for "on".
     *
     * @param {string} _videoId - The video ID of the player that
     * triggered the event.
     */
    ns.triggerSunrise = ns.triggerSunrise || function riseAndShine(evt, _videoId) {
        var $videoplayer,
            $mediaText;

        if (evt.target) {
            $videoplayer = $j(evt.target).parent().find('.el__featured-video');
            if ($videoplayer.hasClass('el__featured-video--expandable') || $videoplayer.hasClass('el__featured-video--standard')) {
                $videoplayer.addClass('el__featured-video--open');
                $mediaText = $videoplayer.parent().find('.media__over-text');
                $mediaText.closest('.cd--has-media').addClass('cd--active');
                $mediaText.show();
                $videoplayer.closest('.el__leafmedia--expandable').addClass('expandable-opacity');
            }
        }
    };
}(CNN.VIDEOCLIENT, jQuery));

/**
 * @name CNN.VIDEOCLIENT.FeaturedVideoCollectionHandler
 * @namespace
 * @memberOf  CNN
 *
 * @description
 * Constructor for creating the handler for Featured Video Player on the video landing page.
 * This Object is handles the specific behavior/funcationality for the featured video player on the
 * video landing page that requires a play in page video on the top of the page and the video collection it
 * is a part of underneath that video.
 * When the video completes, it will play the next video in the collection, change the description on the page
 * and select the appropriate video card in the carousel.
 *
 * @param   {string} viPlayerId  - The elemend ID for the div that holds the play in page video player
 * @param   {string} viColId  - The elemend ID for the div that holds the Carousel that displays the video collection
 * @param   {string} viDescId  - The elemend ID for the div that holds the description for the currently playing video
 * @param   {object} viColObj  - json object that has all the video collection objects in it.
 * @param   {object} viTitleId - the element ID for the title
 * @param   {object} viDurId - the element ID for the duration
 * @param   {object} viSourceId - the element ID for the source
 * @param   {boolean} viCarouselItemsPlayInPlayer - true if clicking on the items in the carousel should play the video in the
 *                                                  video player above the carousel, false if it should use the default
 *                                                  behavior of linking to the leaf page for that video.
 *                                                  Default is false.
 * @param   {boolean} viCarouselClickAutostartsVideo - works in conjuction with viCarouselItemsPlayInPlayer. if
 *                                                  viCarouselItemsPlayInPlayer is set to false, then this does nothing.
 *                                                  set to true if you want a click on any item in the carousel
 *                                                  to autostart that video if the video player hasn't been set to
 *                                                  autostart, false otherwise.
 *                                                  Default is false
 *
 * @param   {string} viPlaylist - The playlist string value associated
 *                                with this video collection. For
 *                                analytics it is used as an ID for the
 *                                video collection.
 *
 */
CNN.VIDEOCLIENT.FeaturedVideoCollectionHandler = function (viPlayerId,
    viColId,
    viDescId,
    viColObj,
    viTitleId,
    viDurId,
    viSourceId,
    viCarouselItemsPlayInPlayer,
    viCarouselClickAutostartsVideo,
    viPlaylist) {
    'use strict';
    var self = this;
    this.carouselItemsPlayInPlayer = viCarouselItemsPlayInPlayer || false;
    this.carouselClickAutostartsVideo = viCarouselClickAutostartsVideo || false;
    this.videoPlayerDivId = viPlayerId;
    this.videoDescriptionDivId = viDescId;
    this.videoCollectionDivId = viColId;
    this.videoCollectionObject = viColObj;
    this.videoTitleDivId = viTitleId;
    this.videoDurationDivId = viDurId;
    this.videoSourceDivId = viSourceId;
    this.videoCollection = viPlaylist;
    this.moveToNextTimeout = 0;

    this.eventCallbackObject = {

        onPlayerReady: function (containerId) {
            var containerClassId;
            /* Send analytics data to Aspen */
            if (CNN.Features.enableAspen) {
                CNN.VideoPlayer.reportLoadTime(containerId);
            }

            CNN.VideoPlayer.handleInitialExpandableVideoState(containerId);

            CNN.VideoPlayer.handleAdOnCVPVisibilityChange(containerId, CNN.pageVis.isDocumentVisible());

            if (self.enableVideoPinning) {
                containerClassId = '#' + containerId;
                if (jQuery(containerClassId).parents('.js-pg-rail-tall__head').length > 0) {
                    this.videoPinner = new CNN.VideoPinner(containerClassId);
                    this.videoPinner.setIsVideoCollection(true);
                    this.videoPinner.init();
                } else {
                    CNN.VideoPlayer.hideThumbnail(containerId);
                }
            } else {
                CNN.VideoPlayer.hideThumbnail(containerId);
            }
        },
        onContentEntryLoad: function (containerId, _playerId, contentid, _isQueue) {

            CNN.VideoPlayer.showSpinner(containerId);
            CNN.VideoPlayer.isFirstVideoInCollection(containerId, contentid);
        },
        onAdPlay: function (containerId, _cvpId, _token, _mode, _id, _duration, blockId, _adType) {
            clearTimeout(self.moveToNextTimeout);
            /* Add VisualRevenue video event on Ad play */
            if (CNN.Features.enableVisualRevenueVideo && Array.isArray(window._vrq)) {
                window._vrq.push(['video', 'adroll', self.currentVideoId]);
            }
            /* Add Outbrain Video tracking event on pre-roll ad play */
            if (blockId === 0 && typeof window.obApi === 'function') {
                window.obApi('track', 'Video View');
            }
            CNN.VideoPlayer.hideSpinner(containerId);

            if (self.enableVideoPinning) {
                if (typeof this.videoPinner !== 'undefined' && this.videoPinner !== null) {
                    this.videoPinner.setIsPlaying(true);
                    this.videoPinner.handleOnVideoPlay();
                    this.videoPinner.animateDown();
                }
            }
        },
        onAdEnd: function (_containerId, _cvpId, _token, _mode, _id, _duration, _blockId, _adType) {
            /* Add VisualRevenue video event on Ad end */
            if (CNN.Features.enableVisualRevenueVideo && Array.isArray(window._vrq)) {
                window._vrq.push(['video', 'adstop', self.currentVideoId]);
            }
        },
        onContentPlay: function (containerId, _cvpId, _event) {
            var collectionId,
                playerInstance;
            /*
             * When the video content starts playing, the companion ad
             * layout (if it was set when the ad played) should switch
             * back to epic ad layout. onContentPlay calls updateCompanionLayout
             * with the 'restoreEpicAds' layout to make this switch and also
             * restores the freewheel tag to the page so that the companion ad
             * will show up if a user clicks on another video
             */
            if (CNN.companion && typeof CNN.companion.updateCompanionLayout === 'function') {
                CNN.companion.updateCompanionLayout('restoreEpicAds');
                CNN.companion.updateCompanionLayout('restoreFreewheel');
            }
            CNN.VideoPlayer.hideSpinner(containerId);
            clearTimeout(self.moveToNextTimeout);

            if (self.currentVideoId) {
                /* Add VisualRevenue video event on video play */
                if (CNN.Features.enableVisualRevenueVideo && Array.isArray(window._vrq)) {
                    window._vrq.push(['video', 'play', self.currentVideoId]);
                }

                /* Send analytics data to Aspen */
                if (CNN.Features.enableAspen) {
                    if (CNN.VideoPlayer.getLibraryName(containerId) === 'fave') {
                        playerInstance = FAVE.player.getInstance(containerId) || null;
                    } else {
                        playerInstance = containerId && window.cnnVideoManager.getPlayerByContainer(containerId).videoInstance.cvp || null;
                    }
                    if (playerInstance !== null) {
                        self.findNextVideo(self.currentVideoId);
                        if (jQuery.isArray(self.videoCollectionObject) && self.videoCollectionObject.length > 0 &&
                            typeof self.videoCollectionObject[0].videoUrl === 'string') {
                            collectionId = self.videoCollectionObject[0].videoUrl.replace(/^.*\/video\/playlists\/(.+)\/?$/, '$1');
                        } else {
                            collectionId = '';
                        }
                        playerInstance.reportAnalytics('videoPageData', {
                            videoCollection: collectionId,
                            videoBranding: CNN.omniture.branding_content_page,
                            templateType: CNN.omniture.template_type,
                            nextVideo: self.nextVideoId,
                            previousVideo: self.prevVideoId,
                            referrerType: '',
                            referrerUrl: document.referrer
                        });
                    }
                }
            }

            if (self.enableVideoPinning) {
                if (typeof this.videoPinner !== 'undefined' && this.videoPinner !== null) {
                    this.videoPinner.setIsPlaying(true);
                    this.videoPinner.handleOnVideoPlay();
                    this.videoPinner.animateDown();
                }
            }
        },
        onContentMetadata: function (containerId, _playerId, metadata, _contentId, _duration, _width, _height) {
            if (CNN.Features.enableVideoExperienceUnification &&
                CNN.Utils.exists(metadata)) {
                try {
                    if (CNN.VideoPlayer.getLibraryName(containerId) === 'fave') {
                        CNN.Videx.EmbedButton.updateCode(metadata);
                    } else {
                        CNN.Videx.EmbedButton.updateCode(JSON.parse(metadata));
                    }
                } catch (e) {
                    console.log('Invalid video metadata JSON.');
                }
            }
        },
        onContentBegin: function (containerId, _cvpId, contentId) {
            CNN.VideoPlayer.reverseAutoMute(containerId);
            CNN.VideoPlayer.isFirstVideoInCollection(containerId, contentId);
            /*
             * Before the video ad starts, the freewheel companion ad
             * html needs to be placed on the page so that it can be
             * triggered by Freewheel to display the companion ad.
             * onContentBegin triggers updateCompanionLayout which
             * handles the logic to switch from epic to companion ads
             * with the 'removeEpicAds' layout
             */
            if (CNN.companion && typeof CNN.companion.updateCompanionLayout === 'function') {
                CNN.companion.updateCompanionLayout('removeEpicAds');
            }
            clearTimeout(self.moveToNextTimeout);
            if (CNN.contentModel.pageType === 'section' && CNN.contentModel.sectionName === 'videos') {
                fastdom.mutate(function () {
                    CNN.share.reloadShareBar();
                });
            }
            self.updateCurrentlyPlaying(contentId);
            jQuery(document).triggerVideoContentStarted();
        },
        onContentComplete: function (_containerId, _cvpId, contentId) {
            /*
             * Navigate to the next video onContentComplete which fires
             * after the postroll. We do not need this call inside onContentEnd
             * since that would cause the postrolls to not play.
             */
            self.navigateToNextVideo(contentId);
        },
        onContentEnd: function (_containerId, _cvpId, _contentId) {
            /* Add VisualRevenue video event on video end */
            if (CNN.Features.enableVisualRevenueVideo && Array.isArray(window._vrq)) {
                window._vrq.push(['video', 'stop', self.currentVideoId]);
            }

            /*
             * When the video content ends playing, remove the epic ad
             * in preparation to display a companion ad
             */
            if (CNN.companion && typeof CNN.companion.updateCompanionLayout === 'function') {
                CNN.companion.updateCompanionLayout('removeEpicAds');
            }
        },
        onCVPVisibilityChange: function (containerId, _cvpId, visible) {
            CNN.VideoPlayer.handleAdOnCVPVisibilityChange(containerId, visible);
        }
    };
    this.initialize();
};

CNN.VIDEOCLIENT.FeaturedVideoCollectionHandler.prototype = {
    /**
     * Initialize the Component
     *
     * @memberOf CNN.VIDEOCLIENT.FeaturedVideoCollectionHandler
     */
    initialize: function initialize() {
        'use strict';
        var carousel = jQuery('div#' + this.videoCollectionDivId);
        this.isVideoSectionFront = jQuery('.pg.pg-videos').length > 0;
        /*
         * Enable Video Pinning when:
         * On video section front with Video Experience Unification and
         * Video Experience Unification Video Pinning enabled
         * OR on article page with video pinning enabled
         * AND on desktop
         * TO DO: roll the essence of this check into the CNN.VideoPinner namespace
         * so it becomes globally available
         */
        this.enableVideoPinning = ((this.isVideoSectionFront &&
            CNN.Features.enableVideoExperienceUnification &&
            CNN.Features.enableVideoExperienceUnificationVideoPinning) ||
            (!this.isVideoSectionFront && CNN.Features.enableVideoPinning)) &&
            (Modernizr && !Modernizr.phone && !Modernizr.mobile && !Modernizr.tablet);
        this.currentVideoId = jQuery('div#' + this.videoPlayerDivId).data().videoId;
        this.nextVideoId = '';
        this.prevVideoId = '';
        if (this.carouselItemsPlayInPlayer) {
            carousel.find('.cn__column.carousel__content__item').find('a').removeAttr('href');
            jQuery(carousel).on('click', '.cn__column.carousel__content__item', jQuery.proxy(this.onVideoCarouselItemClicked, this));
            carousel.parent().on('click', '.js__video__collection__close--expandable', jQuery.proxy(this.onCloseButtonClicked, this));
        }
    },
    onCloseButtonClicked: function onCloseButtonClicked(_evt) {
        'use strict';
        CNNVIDEOAPI.CNNVideoManager.getInstance().stopVideo(this.videoPlayerDivId);
    },
    /**
     * Event Handler for when an item in the carousel gets clicked
     *
     * @param   {object} evt  - The Event Object
     *
     * @memberOf CNN.VIDEOCLIENT.FeaturedVideoCollectionHandler
     */
    onVideoCarouselItemClicked: function onVideoCarouselItemClicked(evt) {
        'use strict';

        var articleElem,
            overrides = {
                videoCollection: this.videoCollection,
                autostart: false
            },
            playerInstance,
            shouldStartVideo = false,
            thumbImageElem,
            thumbImageLargeSource,
            videoId,
            videoPlayer;
        try {
            articleElem = jQuery(evt.currentTarget).find('article');
            thumbImageElem = jQuery(articleElem).find('.media__image');
            videoId = articleElem.data().videoId;
            if (CNN.VideoPlayer.getLibraryName(this.videoPlayerDivId) === 'fave') {
                playerInstance = FAVE.player.getInstance(this.videoPlayerDivId);
                if (CNN.Utils.existsObject(playerInstance) &&
                    typeof playerInstance.getVideoData === 'function' &&
                    typeof playerInstance.getVideoData().id === 'string' &&
                    playerInstance.getVideoData().id.length > 0 &&
                    playerInstance.getVideoData().id !== videoId) {
                    /* Remove videoobject metadata script.If the user click other than initial loaded video */
                    jQuery(articleElem).closest('.cn-carousel-medium-strip').parent().find('script[name="metaScript"]').remove();
                    playerInstance.play(videoId, overrides);
                }
            } else {
                videoPlayer = CNNVIDEOAPI.CNNVideoManager.getInstance().getPlayerByContainer(this.videoPlayerDivId);
                if (videoPlayer && videoPlayer.videoInstance) {
                    /*
                     * if videoPlayer.videoInstance.cvp is null that means it's not initialized yet so
                     * pass in the thumbnail, too.
                     */
                    if (!videoPlayer.videoInstance.cvp) {
                        if (typeof thumbImageElem !== 'undefined' && thumbImageElem !== null) {
                            thumbImageLargeSource = thumbImageElem.data() && thumbImageElem.data().srcLarge ? thumbImageElem.data().srcLarge : 'none';
                        }
                        overrides.thumb = thumbImageLargeSource ? thumbImageLargeSource : 'none';
                        shouldStartVideo = true;
                    }
                    if (videoPlayer.videoInstance.config) {
                        if (videoPlayer.videoInstance.config.video !== videoId) {
                            /* Remove videoobject metadata script.If the user click other than initial loaded video */
                            jQuery(articleElem).closest('.cn-carousel-medium-strip').parent().find('script[name="metaScript"]').remove();
                            CNNVIDEOAPI.CNNVideoManager.getInstance().playVideo(this.videoPlayerDivId, videoId, overrides);
                        }
                        /* Video player isn't autoplay, so init it */
                        if (shouldStartVideo && this.carouselClickAutostartsVideo) {
                            try {
                                videoPlayer.videoInstance.start();
                            } catch (startError) {

                            }
                        }
                    }
                }
            }
            this.updateCurrentlyPlaying(videoId);
        } catch (error) {

        }
        jQuery(this.getCollectionCarouselElement()).trigger('VIDEO_CAROUSEL_ITEM_CLICKED', [videoId]);
    },
    /**
     * Retrieve the event callback handler that will be passed to the
     * video loader
     *
     * @returns {object} The event callback object
     *
     * @memberOf CNN.VIDEOCLIENT.FeaturedVideoCollectionHandler
     */
    getEventCallbackObject: function getEventCallbackObject() {
        'use strict';
        return this.eventCallbackObject;
    },
    /**
     * Find the next videoId to be played from the collection
     *
     * @param   {string} currentVideoId  - The video uri of the current video
     * @returns {number} The index in the collection of the next video to be played
     *
     * @memberOf CNN.VIDEOCLIENT.FeaturedVideoCollectionHandler
     */
    findNextVideo: function findNextVideo(currentVideoId) {
        'use strict';
        var i,
            nextIndex = -1,
            self = this,
            vidObj;
        if (jQuery.isArray(this.videoCollectionObject) && this.videoCollectionObject.length > 0) {
            for (i = 0; i < this.videoCollectionObject.length; i++) {
                vidObj = this.videoCollectionObject[i];
                if (typeof vidObj !== 'undefined' && vidObj.videoId) {
                    if (vidObj.videoId === currentVideoId) {
                        if (i < this.videoCollectionObject.length - 1) {
                            nextIndex = (i + 1);
                        } else {
                            nextIndex = 0;
                        }
                        break;
                    }
                }
            }

            if (nextIndex < 0) {
                nextIndex = 0;
            }

            this.nextVideoId = self.videoCollectionObject[nextIndex].videoId;
        }
        return nextIndex;
    },
    /**
     * Play the next video in the collection. This takes in the currently playing video uri,
     * determines it's placement in the video collection and then selects the next video that should
     * play
     *
     * @param {string} currentVideoId - The video uri of the current video
     *
     * @memberOf CNN.VIDEOCLIENT.FeaturedVideoCollectionHandler
     */
    navigateToNextVideo: function navigateToNextVideo(currentVideoId) {
        'use strict';

        var nextPlay,
            nextPlayIndex,
            nextVideoPlayTimeout = CNN.VideoConfig.collection.playNextVideoTimeout || 3000,
            self = this,
            overrides;

        nextPlayIndex = this.findNextVideo(currentVideoId);
        if (nextPlayIndex >= 0) {
            this.moveToNextTimeout = setTimeout(function () {
                nextPlay = self.videoCollectionObject[nextPlayIndex].videoId;
                overrides = {
                    videoCollection: self.videoCollection,
                    autostart: true
                };
                if (CNN.VideoPlayer.getLibraryName(self.videoPlayerDivId) === 'fave') {
                    FAVE.player.getInstance(self.videoPlayerDivId).play(nextPlay, overrides);
                } else {
                    CNNVIDEOAPI.CNNVideoManager.getInstance().playVideo(self.videoPlayerDivId, nextPlay, overrides);
                }
                self.updateCurrentlyPlaying(nextPlay);
            }, nextVideoPlayTimeout);
        } else {
            if (self.enableVideoPinning) {
                if (typeof this.videoPinner !== 'undefined' && this.videoPinner !== null) {
                    this.videoPinner.setIsPlaying(false);
                }
            }
        }
    },
    /**
     * Retrieves the indices of the currently playing video as well as the next video that in the queue
     *
     *
     * @param   {string} currentVideoId  - The video uri of the current video
     * @returns {object} Json object that has the current video index and the next video index that should be played
     *
     * @memberOf CNN.VIDEOCLIENT.FeaturedVideoCollectionHandler
     */
    getCurrentlyPlayingConfig: function getCurrentlyPlayingConfig(currentVideoId) {
        'use strict';
        var i,
            retConfig = {
                currentIndex: 0,
                nextIndex: 0
            },
            vidObj;
        if (jQuery.isArray(this.videoCollectionObject) && this.videoCollectionObject.length > 0) {
            for (i = 0; i < this.videoCollectionObject.length; i++) {
                vidObj = this.videoCollectionObject[i];
                if (typeof vidObj !== 'undefined' && vidObj.videoId) {
                    if (vidObj.videoId === currentVideoId) {
                        if (i < this.videoCollectionObject.length - 1) {
                            retConfig.nextIndex = (i + 1);
                            retConfig.currentIndex = i;
                        } else {
                            retConfig.nextIndex = 0;
                            retConfig.currentIndex = i;
                        }
                        break;
                    }
                }
            }
        }
        return retConfig;
    },
    /**
     * Retreives the DOM element for the carousel that contains the video collection cards
     *
     *
     * @returns {object} The DOM element for the  carousel or an empty object if not found.
     *
     * @memberOf CNN.VIDEOCLIENT.FeaturedVideoCollectionHandler
     */
    getCollectionCarouselElement: function getCollectionCarouselElement() {
        'use strict';

        var carouselElem;

        try {
            carouselElem = jQuery(document.getElementById(this.videoCollectionDivId.replace('#', '')));
        } catch (error) {}
        return carouselElem;

    },
    /**
     * Retreives the OWL carousel object that contains the video collection cards
     *
     *
     * @returns {object} The Owl carousel or an empty object if not found.
     *
     * @memberOf CNN.VIDEOCLIENT.FeaturedVideoCollectionHandler
     */
    getCollectionCarousel: function getCollectionCarousel() {
        'use strict';

        var carousel,
            owl;

        try {
            carousel = this.getCollectionCarouselElement();
            if (carousel) {
                if (carousel.find('.js-owl-carousel')) {
                    carousel = carousel.find('.js-owl-carousel');
                    owl = carousel.data('owl.carousel');
                }
            }
        } catch (error) {}
        return owl || {};
    },
    /**
     * Updates the page's description and the selected index of the carousel for the selected video.
     * This will change the displayed description, select the index of the video in the carousel, remove the "Now Playing" off
     * of the previous playing video and adds that "Now Playing" to the new video, while swapping out the video play icon too.
     * @param {string} currentVideoId - The video uri of the current video
     *
     * @memberOf CNN.VIDEOCLIENT.FeaturedVideoCollectionHandler
     */
    updateCurrentlyPlaying: function updateCurrentlyPlaying(currentVideoId) {
        'use strict';
        var videoPlayConfig,
            videoObj,
            owl,
            previous,
            domain = CNN.Host.domain,
            /* sourceElement,
            prependText = 'Source: ',
            $embeddedPlayer, */
            $owlElement,
            $nowPlayingVideo,
            gigyaShareElement,
            whatsappShareElement,
            $carouselContentItems = jQuery('.carousel__content__item', document.getElementById(this.videoCollectionDivId));

        gigyaShareElement = jQuery('div.js-gigya-sharebar');
        whatsappShareElement = jQuery('div.share-bar-whatsapp-container');

        if (this.currentVideoId !== currentVideoId) {
            this.prevVideoId = this.currentVideoId;
            this.currentVideoId = currentVideoId;
        }
        videoPlayConfig = this.getCurrentlyPlayingConfig(currentVideoId);
        if (videoPlayConfig && typeof videoPlayConfig.currentIndex !== 'undefined') {
            owl = this.getCollectionCarousel();
            $nowPlayingVideo = CNN.Utils.exists(owl._items) ? owl._items[videoPlayConfig.currentIndex] :
                $carouselContentItems.eq(videoPlayConfig.currentIndex);
            if (owl) {
                previous = jQuery(owl._items).find('.media__over-text');
                if (previous.length > 0) {
                    previous.parent().find('.media__icon').show();
                    previous.remove();
                }
                $owlElement = CNN.Utils.exists(owl.$element) ? jQuery(owl.$element) :
                    $carouselContentItems.has('.media__over-text');
                if ($owlElement.length > 0) {
                    $owlElement.find('.cd').removeClass('cd--active');
                    $owlElement.find('.cd--spon__overlay__media').removeClass('cd--spon__overlay__media--active');
                    $owlElement.find('.media__over-text').remove();
                    $owlElement.find('.media__icon').show();
                }
                jQuery($nowPlayingVideo).find('.cd').addClass('cd--active');
                if ($nowPlayingVideo.find('.media a:first-child').length > 0) {
                    if (CNN.Features.enableVideoExperienceUnification && Modernizr && Modernizr.phone) {
                        $nowPlayingVideo
                            .find('.media a:first-child')
                            .append('<div class="media__over-text">Now Playing</div>');
                    } else {
                        $nowPlayingVideo
                            .find('.media a:first-child')
                            .append('<div class="media__over-text">Now Playing</div>')
                            .end()
                            .find('.media__icon')
                            .hide();
                    }
                } else {
                    $nowPlayingVideo
                        .find('.cd--spon__overlay__media')
                        .addClass('cd--spon__overlay__media--active')
                        .end()
                        .find('.cd--spon__overlay__media-wrapper')
                        .append('<div class="media__over-text media__over-text--active sponsored-playing">Sponsor Content</div>');

                    jQuery(document).trigger('CNNSponVideoPlay');
                }

                if (typeof owl.to === 'function') {
                    owl.to(videoPlayConfig.currentIndex);
                }
            }
            videoObj = this.videoCollectionObject[videoPlayConfig.currentIndex] || {};

            CNN.Videx.Metadata.init(videoObj, this);

            /* never used...
            $embeddedPlayer = jQuery(document.getElementById(this.videoCollectionDivId)).parent().find('.el__featured-video');
            */

            /*
             * To show the Source on top of video player for embedded collection and videos section front
             */
            /* never used...
            if (this.isVideoSectionFront || $embeddedPlayer.hasClass('el__featured-video--expandable')) {
                sourceElement = jQuery(document.getElementById(this.videoSourceDivId.replace('#', '')));
            } else {
                prependText = '| Video Source: ';
                sourceElement = jQuery(document.getElementById('js-pagetop_video_source'));
            }
            */
        }

        if (typeof gigyaShareElement !== 'undefined' && CNN.contentModel.pageType !== 'article') {
            jQuery(gigyaShareElement).attr('data-title', videoObj.title || '');
            jQuery(gigyaShareElement).attr('data-description', videoObj.description || '');
            jQuery(gigyaShareElement).attr('data-link', domain + videoObj.videoUrl || '');
            jQuery(gigyaShareElement).attr('data-image-src', videoObj.videoImage || '');
        }

        if (typeof whatsappShareElement !== 'undefined') {
            jQuery(whatsappShareElement).attr('data-title', videoObj.title || '');
            jQuery(whatsappShareElement).attr('data-storyurl', domain + videoObj.videoUrl || '');
        }
    }
};

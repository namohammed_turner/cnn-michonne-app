/* global CNN, FAVE, MobileDetect, document, window, OBR */

CNN.Features = CNN.Features || {};
CNN.contentModel = CNN.contentModel || {};
CNN.VideoConfig = CNN.VideoConfig || {};
CNN.Utils = CNN.Utils || {};
CNN.VideoPlayer = CNN.VideoPlayer || {};
CNN.Videx = CNN.Videx || {};

(function () {
    'use strict';

    var
        ajaxOpts = {
            cache: false,
            async: true,
            contentType: 'application/json;',
            dataType: 'json',
            method: 'GET',
            timeout: 15000
        },
        corePlayerReadyTriggered = false,
        count = Number(0),
        env = CNN.Host.domain,
        getocs = {
            ext: ':*.json',
            playlist: '/data/ocs/playlist/videos/'
        },
        md = new MobileDetect(navigator.userAgent),
        mobileSectionConfig = CNN.SectionConfig.mobile.contexts,
        defaultAdSection = mobileSectionConfig['default'].adsection,
        network = CNN.VideoConfig.network,
        /* used to instantiate a fave player w/o the context warning */
        sectionName = CNN.contentModel.sectionName || 'homepage';

    if (!(CNN.Features.enableOneTapToPlay && FAVE.settings.enabled && FAVE.settings.oneTapEnabledPage)) {
        return;
    }

    CNN.Videx.mobile = {};

    init();

    /**
     * initialize this script on mobile devices
     */
    function init() {
        var isMobile = Boolean(md.mobile());
        if(isMobile) {
            setCorePlayerReady();
            hasVideoWebPack();
        }
    }

    /**
     * set handlers on video links
     */
    function setCorePlayerReady() {
        jQuery(document).on('corePlayerReady', function () {

            if (!corePlayerReadyTriggered) {
                corePlayerReadyTriggered = true;

                /* if the player isn't ready and the user clicks a link they will get the video leaf page */
                jQuery(document).on('click', 'article[data-video-id], li.icon-video > a[data-video-id]', function (e) {
                    e.preventDefault();
                    e.stopPropagation();
                    handleTap(jQuery(this), false);
                });

                if (CNN.Features.enableOutbrainOneTapToPlay) {
                    jQuery(document).on('click', '.OUTBRAIN a[data-video-id]', function (e) {
                        e.preventDefault();
                        e.stopPropagation();
                    });

                    jQuery(document).on('mouseup', '.OUTBRAIN a[data-video-id]', function (_e) {
                        handleTap(jQuery(this), true);
                    });
                }
            }
        });
    }

    /* if player dependencies haven't been loaded then load them */
    function hasVideoWebPack() {
        CNN.INJECTOR.executeFeature('video').then(function () {
            setHtml();
            CNN.VideoPlayer.injectFave(initConfig(), initCallbacks());
            if (CNN.contentModel.hasVideo === false) {
                jQuery('<link rel="stylesheet" type="text/css" href="' + FAVE.settings.cssUrl + '" >').appendTo('head');
            }
        });
    }

    /* creates the necessary HTML */
    function setHtml() {
        var frag = document.createDocumentFragment(),
            oneTapVideoContainer = document.createElement('div');

        oneTapVideoContainer.id = getContainerId();
        CNN.Videx.mobile.oneTapContainer = oneTapVideoContainer;

        frag.appendChild(CNN.Videx.mobile.oneTapContainer);

        document.body.appendChild(frag);
    }

    /* get the node id of the player */
    function getContainerId() {
        return initConfig().markupId || '';
    }

    /* get the current video id */
    function getVideoId() {
        return CNN.Videx.mobile.videoId || '';
    }

    /* get the video playlist */
    function getVideoCollection() {
        return CNN.Videx.mobile.playlist || [];
    }

    /* get the video collection id */
    function getVideoCollectionId() {
        var videoUrl = CNN.Videx.mobile.videoUrl;
        return (typeof videoUrl === 'string') ? videoUrl.replace(/^.*\/video\/playlists\/(.+)\/?$/, '$1') : '';
    }

    /**
     * sets the initial config for the empty player
     *
     * @param {string} videoId - video ID string
     * @param {string} section - section name
     * @param {string} adSection - ad section name
     *
     * @returns {object} - config object
     */
    function initConfig(videoId, section, adSection) {
        return {
            adsection: adSection || '',
            thumb: 'none',
            video: videoId || '',
            width: '100%',
            height: '100%',
            section: CNN.contentModel.edition || 'domestic',
            profile: 'expansion',
            network: network,
            markupId: 'one-tap-video',
            frameWidth: '100%',
            frameHeight: '100%',
            autostart: false,
            oneTapPlay: true,
            freewheel: {
                siteSectionIdSelection: function siteSectionIdSelection(configs, videoJSON) {
                    var ssid = '';
                    if (typeof videoJSON === 'object' && typeof videoJSON.adSection === 'string') {
                        if (videoJSON.adSection.indexOf('const-') === 0) {
                            ssid = (function () {
                                try {
                                    return mobileSectionConfig[videoJSON.sectionName].adSectionOverrideKeys[videoJSON.adSection];
                                } catch (e) {
                                    if (mobileSectionConfig[videoJSON.sectionName] === 'object' &&
                                        mobileSectionConfig[videoJSON.sectionName].adsection === 'string') {
                                        return mobileSectionConfig[videoJSON.sectionName].adsection;
                                    } else {
                                        return defaultAdSection;
                                    }
                                }
                            }());
                        } else {
                            ssid = videoJSON.adSection;
                        }
                    }

                    return ssid;
                }
            },
            context: section || sectionName
        };
    }

    /**
     * Player event callbacks
     *
     * @returns {object} - callbacks object
     */
    function initCallbacks() {
        return {
            onPlayerReady: function (containerId, _playerId) {
                jQuery(document).triggerCorePlayerReady();
                CNN.Videx.mobile.oneTapPlayer = FAVE.player.getInstance(containerId);
            },

            onContentEntryLoad: function (_containerId, _playerId, contentid, _isQueue) {
                CNN.Videx.mobile.videoId = contentid;
            },

            onAdPlay: function (_containerId, _playerId, _token, _mode, _id, _duration, blockId, _adType) {
                /* Add VisualRevenue video event on Ad play */
                if (CNN.Features.enableVisualRevenueVideo && Array.isArray(window._vrq)) {
                    window._vrq.push(['video', 'adroll', getVideoId()]);
                }
                /* Add Outbrain Video tracking event on pre-roll ad play */
                if (blockId === 0 && typeof window.obApi === 'function') {
                    window.obApi('track', 'Video View');
                }
            },

            onAdEnd: function (_containerId, _playerId, _token, _mode, _id, _blockId, _adType) {
                /* Add VisualRevenue video event on Ad end */
                if (CNN.Features.enableVisualRevenueVideo && Array.isArray(window._vrq)) {
                    window._vrq.push(['video', 'adstop', getVideoId()]);
                }
            },

            onContentPlay: function (containerId, _playerId, contentId) {
                var nextVideo = ((count + 1) >= getVideoCollection().length) ? 0 : count + 1,
                    previousVideo = (count === 0) ? 0 : count - 1;

                if (count === 0) {
                    getPlaylist(contentId)
                        .then(function () {
                            CNN.Videx.mobile.playlist = this.currentVideoCollection;

                            setAspen(nextVideo, previousVideo);

                            if (getVideoCollectionId() === '' &&
                                Array.isArray(CNN.Videx.mobile.playlist) &&
                                typeof CNN.Videx.mobile.playlist[0] === 'object' &&
                                typeof CNN.Videx.mobile.playlist[0].videoUrl === 'string') {
                                CNN.Videx.mobile.videoUrl = CNN.Videx.mobile.playlist[0].videoUrl;
                            }
                        });
                } else {
                    setAspen(nextVideo, previousVideo);
                }

                /* Add VisualRevenue video event on video play */
                if (CNN.Features.enableVisualRevenueVideo && Array.isArray(window._vrq)) {
                    window._vrq.push(['video', 'play', containerId]);
                }
            },

            onContentEnd: function (containerId, _playerId, _contentId) {
                if (CNN.Features.enableVisualRevenueVideo && Array.isArray(window._vrq)) {
                    window._vrq.push(['video', 'stop', containerId]);
                }
            },

            onContentComplete: function (containerId, _playerId, contentId) {
                count = ((count) < getVideoCollection().length) ? count : 0 ;
                contentId = getVideoCollection()[count].videoId;
                CNN.Videx.mobile.oneTapPlayer.play(contentId, {
                    /* Per the Analytics Team, subsequent videos in the playlist should be set to autostart true */
                    autostart: true,
                    videoCollection: getVideoCollectionId()
                });
            },

            onTrackingFullscreen: function (_containerId, _playerId, dataObj) {
                /*
                 * TODO: Create a Method in FAVE API to get the active Video Element.
                 */
                var iosPresentationMode = jQuery(CNN.Videx.mobile.oneTapPlayer.getVideoPlayer().element).find('> video[src]')[0].webkitPresentationMode;

                if (typeof dataObj === 'object' && dataObj.fullscreen === false &&
                   (FAVE.Utils.os !== 'iOS' || iosPresentationMode === 'inline')) {

                    exitFullScreen();
                }
            }
        };
    }

    /* reset page refresh, hide & stop the player when exiting full screen */
    function exitFullScreen() {
        CNN.goActiveUntil = 0;
        CNN.Videx.mobile.oneTapPlayer.pause();
    }

    /**
     * send analytics data to Aspen
     *
     * @param {Number} nextVideo
     * index of the next video in the playlist
     *
     * @param {Number} previousVideo
     * index of the previous video in the playlist
     */
    function setAspen(nextVideo, previousVideo) {
        if (CNN.Features.enableAspen) {
            CNN.Videx.mobile.oneTapPlayer.reportAnalytics('videoPageData', {
                videoCollection: getVideoCollectionId(),
                videoBranding: CNN.omniture.branding_content_page,
                templateType: CNN.omniture.template_type,
                nextVideo: getVideoCollection()[nextVideo].videoId,
                previousVideo: getVideoCollection()[previousVideo].videoId,
                referrerType: '',
                referrerUrl: document.referrer
            });
        }

        count++;
    }

    /**
     * get the playlist data for a single video
     *
     * @param {string} videoId - video ID to get the playlist for
     * @returns {Promise} - Get playlist data promise
     */
    function getPlaylist(videoId) {
        var dfr = jQuery.Deferred();

        ajaxOpts.url = env + getocs.playlist + videoId + getocs.ext;

        jQuery.ajax(ajaxOpts)
            .done(function (data, _status) {
                dfr.resolveWith(data);
            })
            .fail(function (_xhr, _status) {
                dfr.reject();
            });

        return dfr.promise().fail(function ocsFail() {
            console.log('Failed to retrieve playlist');
        });
    }

    /**
     * on click handler for video links on mobile devices
     * extracts the video id attribute from the link clicked
     * which is then used to make an OCS call to retrieve that videos' ssid,
     * section name and playlist
     *
     * @param {object} $this
     * Reference to the node clicked
     *
     * @param {boolean} outbrain
     * is this an Outbrain link?
     *
     */
    function handleTap($this, outbrain) {
        var id = $this[0].dataset.videoId;

        /* set the global count to 0 when a user clicks a link */
        count = Number(0);

        /*
         * we need the videoUrl in order to retrieve the video collection id
         * requesting from OCS takes too long for this still to be considered a user gesture
         * so pulling the visual revenue data attribute if available since it contains the same
         * info we would make a call out to OCS for. Fallback is the videoPlaylistUrl data attribute.
         */
        CNN.Videx.mobile.videoUrl = $this[0].dataset.vrContentbox || $this[0].dataset.videoPlaylistUrl || null;

        /* turn off page refresh while user is in full screen */
        if (CNN.VideoConfig.liveStream.pageRefreshTime > 0) {
            CNN.goActiveUntil = (CNN.VideoConfig.liveStream.pageRefreshTime * 60000) + Date.now();
        }

        CNN.Videx.mobile.oneTapPlayer.play(id, {
            /* Per the Analytics Team, the initial video tapped should be set to autostart false */
            autostart: false,
            videoCollection: getVideoCollectionId()
        });

        /*
         * Call the enterFullscreen function after the play function, to ensure that the play function's default
         * 'non-fullscreen' mode does not override the 'fullscreen' mode set by the enterFullscreen function.
         */
        CNN.Videx.mobile.oneTapPlayer.enterFullscreen();

        if (outbrain &&
            typeof OBR === 'object' &&
            typeof OBR.extern === 'object' &&
            typeof OBR.extern.callClick === 'function') {

            try {
                OBR.extern.callClick({
                    link: $this[0].href
                });
            } catch (e) {
                console.log('Outbrain callClick failed');
            }
        }
    }

}());

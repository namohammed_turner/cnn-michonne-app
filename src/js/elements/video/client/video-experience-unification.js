/* global CNN, fastdom, Modernizr, FAVE */

(function (ns, $j) {
    'use strict';

    if (!ns.hasOwnProperty('Videx')) {
        ns.Videx = {};
    }

    /**
     * Handles metadata display of a collection's active video
     *
     * @return {Object} init
     */
    ns.Videx.Metadata = (function () {

        var stripHash = ns.Utils.stripHash,
            isPhone = ns.Utils.isPhone;

        /**
         * Sets video description, title and video duration for the active video
         * @param {Object} videoObj
         * an object representing the active video in the collection
         * @param {Object} context - an object representing the state of the video collection
         */
        function setVideoMeta(videoObj, context) {
            setDescription(getDescription(context), videoObj, context);
            setDuration(getDuration(context), videoObj);
            setTitle(getTitle(context), videoObj);
            setDateCreated(videoObj);
            setSourceName(getSourceName(context), videoObj);
        }

        /**
         * set Date created if applicable
         * @param {object} videoObj - video object
         */
        function setDateCreated(videoObj) {
            var dateCreated = document.querySelector('.metadata__data-added');

            if (dateCreated !== null) {
                videoObj.dateCreated = videoObj.dateCreated || '';

                fastdom.mutate(function () {
                    dateCreated.innerHTML = 'Added on ' + videoObj.dateCreated;
                });
            }
        }

        /**
         * update the description node
         * @param {HTMLElement} div - the DOM node to place the description in
         * @param {object} videoObj - video object
         * @param {object} context - context obejct
         */
        function setDescription(div, videoObj, context) {
            var description = '',
                state = getID('js-video_source-' + trimColID(context));

            if (context.isVideoSectionFront) {
                description = (videoObj.descriptionText) ? videoObj.descriptionText : '';
            } else if (Array.isArray(videoObj.descriptionText)) {
                videoObj.descriptionText.forEach(function (text) {
                    description += text + '';
                });
            }

            if (ns.Features.enableVideoExperienceUnification && div !== null) {
                fastdom.mutate(function () {
                    div.innerHTML = description;
                });
            } else {
                fastdom.mutate(function () {
                    $j('.media__video-description')
                        .eq(0)
                        .children('p')
                        .html(description);
                });
            }

            if (state !== null) {
                fastdom.mutate(function () {
                    state.innerHTML = willLinkWrap(videoObj, context);
                });
            }
        }

        /**
         * update the duration node
         * an empty string is always passed to videoObj.duration
         * from initializeVideoAndCollection()
         * in /views/elements/featured-video-collection.dust
         * @param {HTMLElement} div - the DOM node to place the duration in
         * @param {object} videoObj - video object
         */
        function setDuration(div, videoObj) {
            videoObj.duration = videoObj.duration || null;
            if (div !== null) {
                fastdom.mutate(function () {
                    div.innerHTML = (videoObj.duration) ? videoObj.duration : '';
                });
            }
        }

        /**
         * update the sourceName node
         * an empty string is always passed to videoObj.sourceName
         * from initializeVideoAndCollection()
         * in /views/elements/featured-video-collection.dust
         * @param {HTMLElement} span - the DOM node to place the sourceName in
         * @param {object} videoObj - video object
         */
        function setSourceName(span, videoObj) {
            var showSourceSpanContent = '';
            if (ns.Utils.exists(videoObj.sourceName) && videoObj.sourceName !== '') {
                if (ns.Utils.exists(videoObj.sourceLink) && videoObj.sourceLink !== '') {
                    showSourceSpanContent = '<a href="' + videoObj.sourceLink + '">' + 'Source: ' + videoObj.sourceName + '</a>';
                } else {
                    showSourceSpanContent = 'Source: ' + videoObj.sourceName;
                }
            }
            if (span !== null) {
                fastdom.mutate(function () {
                    span.innerHTML = showSourceSpanContent;
                });
            }
        }

        /**
         * update title nodes with the current video title
         * @param {HTMLElement} div - the DOM node to place the title in
         * @param {object} videoObj - video object
         *
         * @var {HTMLElement} navHeadline
         * @var {HTMLElement} pageHeadline
         * @var {string} title
         */
        function setTitle(div, videoObj) {
            var navHeadline = getID('js-nav-section-article-title'),
                pageHeadline = document.querySelector('h1.pg-headline'),
                title = (videoObj.title) ? videoObj.title : '';

            fastdom.mutate(function () {
                if (pageHeadline !== null) {
                    pageHeadline.innerHTML = title;
                }
                if (navHeadline !== null) {
                    navHeadline.innerHTML = title;
                }
                if (div !== null) {
                    div.innerHTML = title;
                }
            });
        }

        /**
         * get the description node
         * @param {object} context - context object
         * @return {HTMLElement} - description DOM node
         */
        function getDescription(context) {
            return (isPhone() || isBreakPoint()) ? getMobileDescription(context) : getID(context.videoDescriptionDivId);
        }

        /**
         * get the mobile description DOM node
         * @param {object} context - context object
         * @return {HTMLElement} - mobile description DOM node
         */
        function getMobileDescription(context) {
            return getID('js-mobile-video_description-' + trimColID(context));
        }

        /**
         * get the source node
         * @param {object} context - context object
         * @return {HTMLElement} - source DOM node
         */
        function getSourceName(context) {
            return (isPhone() || isBreakPoint()) ? getMobileSource(context) : getID(context.videoSourceDivId);
        }

        /**
         * get the mobile source DOM node
         * @param {object} context - context object
         * @return {HTMLElement} - mobile source DOM node
         */
        function getMobileSource(context) {
            return getID('js-mobile-video_source-' + trimColID(context));
        }

        /**
         * get the duration node
         * @param {Object} context - context object
         * @returns {HTMLElement} - duration DOM node
         */
        function getDuration(context) {
            return (isPhone() || isBreakPoint(context)) ? null : getID(context.videoDurationDivId);
        }

        /**
         * get a DOM node ID
         * @param {HTMLElement} node - the DOM node to extract the ID from
         * @returns {string} - DOM node ID
         */
        function getID(node) {
            return document.getElementById(stripHash(node));
        }

        /**
         * get the ID of the title node
         * @param {Object} context - context object
         * @returns {string} - title DOM id
         */
        function getMobileTitle(context) {
            return getID('js-mobile-video-headline-' + trimColID(context));
        }

        /**
         * get the title node based on context
         * @param {Object} context - context object
         * @return {HTMLElement} - title DOM node
         */
        function getTitle(context) {
            return (isPhone() || isBreakPoint()) ? getMobileTitle(context) : getID(context.videoTitleDivId);
        }

        /**
         * videx Unification breakpoint is 960px
         * @return {Boolean} true if less than 960px
         */
        function isBreakPoint() {
            return ($j(window).width() < 960) ? true : false;
        }

        /**
         * get the container id
         * @param {Object} context - context object
         * @return {string} - container ID
         */
        function trimColID(context) {
            return context.videoCollectionDivId.slice(3);
        }

        /**
         * wrap the source in an anchor (if applicable)
         * @param {Object} videoObj - video object
         * @param {Object} _context - context object
         * @returns {string} the linked text
         */
        function linkWrapper(videoObj, _context) {
            if (ns.Features.enableVideoExperienceUnification) {
                return '<a href=' + videoObj.sourceLink + '>Source: ' + videoObj.sourceName + '</a>';
            } else {
                return 'Source: <a href=' + videoObj.sourceLink + '>' + videoObj.sourceName + '</a>';
            }
        }

        /**
         * determine if the source text should be wrapped in an anchor
         * @param {object} videoObj - video object
         * @param {object} context - context object
         * @returns {string} - source text, possibly wrapped in an anchor
         */
        function willLinkWrap(videoObj, context) {
            return (videoObj.sourceLink !== '') ? linkWrapper(videoObj, context) : 'Source: ' + videoObj.sourceName;
        }

        return {
            /**
             * initializes this script and sets a throttled handler for the resize event
             * @param {object} videoObj - video object
             * @param {object} context - context object
             */
            init: function (videoObj, context) {
                if (ns.contentModel.sectionName === 'videos' || ns.contentModel.pageType === 'video') {
                    if (ns.Utils.existsObject(videoObj)) {
                        if (!isPhone()) {
                            $j(window).throttleEvent('resize', function () {
                                fastdom.mutate(function () {
                                    setVideoMeta(videoObj, context);
                                });
                            }, 100);
                        }
                        setVideoMeta(videoObj, context);
                    }
                }
            }
        };
    }());

    /**
     * Handles the See More button functionality for the stacked video list below the video player on phones
     *
     * @return {object} init - init handler
     */
    ns.Videx.SeeMore = (function seeMore() {

        var totalNumber = 0,
            currentlyDisplaying = 0,
            incrementValue = 3,
            $carousel;

        function moreVideos() {
            var i,
                startIndex = currentlyDisplaying + 1,
                endIndex = currentlyDisplaying + incrementValue,
                childSelector;

            totalNumber = (totalNumber > 0) ? totalNumber : $carousel.children().length;
            endIndex = (totalNumber < endIndex) ? totalNumber : endIndex;
            for (i = startIndex; i <= endIndex; i++) {
                childSelector = ':nth-child(' + i + ')';
                $carousel.find(childSelector).css('display', 'block');
                currentlyDisplaying = endIndex;
            }
            if (endIndex === totalNumber) {
                $j('.el__video-collection__see-more').css('display', 'none');
            } else {
                $j('.el__video-collection__see-more').css('display', 'block');
            }
        }

        function onSeeMoreReady() {
            $j('.el__video-collection__see-more').click(function seeMoreClick() {
                moreVideos();
            });
        }

        function init(carousel) {
            $carousel = carousel;
            moreVideos();
            onSeeMoreReady();
        }

        return {
            init: init
        };
    }());

    /**
     * Handles the Embed Button button functionality on video section front and video leaf pages.
     *
     * @return {Object} updateCode handler
     */
    ns.Videx.EmbedButton = (function embedButton() {
        var button,
            buttonContainer,
            codeContainer,
            videoIdReplacementString,
            iframe,
            isDesktop = Modernizr && !Modernizr.mobile && !Modernizr.phone && !Modernizr.tablet;

        function getIframe(metadata) {
            if (!ns.Utils.exists(iframe)) {
                if (FAVE.settings.enabled) {
                    iframe = FAVE.settings.iframe.replace('{edition}', FAVE.settings.server.edition)
                        .replace('{env}', FAVE.settings.server.environment);
                    videoIdReplacementString = '{video}';
                } else {
                    iframe = CNN.VideoConfig.embedLinks.embedLinkPattern.replace('{domain}', CNN.Host.domain);
                    videoIdReplacementString = '{videoId}';
                }
            }

            return iframe.replace(videoIdReplacementString, metadata.id);
        }

        /**
         * Called in CVP's onContentMetadata callback to update the HTML iframe embed code.
         *
         * @param {Object} metadata - metadata object to use
         */
        function updateCode(metadata) {
            if (isDesktop) {
                if (typeof metadata === 'object' &&
                    (typeof metadata.id === 'string' && metadata.id.length > 0) &&
                    typeof metadata.isEmbeddable === 'string' &&
                    (metadata.isEmbeddable === 'yes' || metadata.isEmbeddable === 'true')) {

                    if (!ns.Utils.exists(buttonContainer)) {
                        fastdom.measure(function () {
                            buttonContainer = $j('.el__videoexperience__collection .sharebar-video-embed-container');
                            button = $j('.el__videoexperience__collection .sharebar-video-embed-container .gig-button');
                            codeContainer = buttonContainer.next('.sharebar-video-embed-field');

                            fastdom.mutate(function () {
                                button.on('click', function videoEmbedCodeButtonClick() {
                                    codeContainer.toggleClass('display-sharebar-embed-item');
                                    codeContainer.select();
                                });
                                buttonContainer.addClass('display-sharebar-embed-item');
                            });
                        });
                    }

                    fastdom.mutate(function () {
                        buttonContainer.addClass('display-sharebar-embed-item');
                        codeContainer.removeClass('display-sharebar-embed-item');
                        codeContainer.val(getIframe(metadata));
                    });
                } else if (ns.Utils.exists(buttonContainer)) {
                    buttonContainer.removeClass('display-sharebar-embed-item');
                }
            }
        }

        return {
            updateCode: updateCode
        };
    }());
}(CNN, jQuery));

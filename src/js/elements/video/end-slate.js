/* global CNN, eqjs, dust, FAVE */

CNN.Features = CNN.Features || {};

/**
 * Provides functions to fetch the elements for the base Video End Slate
 * and render them. This is meant to be extended by spcific implementations.
 *
 * @name VideoEndSlateBase
 * @namespace
 * @memberOf CNN
 *
 * @param {string} containerId - The DOM id of the element that will have the end slate
 *                  associated with it.
 * @param {boolean} endSlateFlag - flag to turn on/off the video end-slate [optional]
 */
CNN.VideoEndSlateBase = function (containerId, endSlateFlag) {
    'use strict';
    /* Use the parameter if set, then the runtime setting, default to true if nothing set */
    this.endSlateFlag = (typeof endSlateFlag === 'boolean') ? endSlateFlag :
        ((typeof CNN.Features.enableVideoEndSlate === 'boolean') ? CNN.Features.enableVideoEndSlate : true);
    this.containerId = containerId;
    this.initialize();
};
/**
 * Static utility function for allowing child classes to extend the VideoEndSlateBase
 *
 * @param {object} base - the base object to inherit from
 * @param {object} sub - the child object
 */
CNN.VideoEndSlateBase.extend = function (base, sub) {
    'use strict';
    var origProto = sub.prototype,
        key;
    sub.prototype = Object.create(base.prototype);
    /*
     * No calls are being made to .hasOwnProperty within this loop to ensure it
     * works on IE8
     */
    for (key in origProto) {
        if (origProto.hasOwnProperty(key)) {
            sub.prototype[key] = origProto[key];
        }
    }
    /* Remember the constructor property was set wrong, let's fix it */
    sub.prototype.constructor = sub;
    /*
     * In ECMAScript5+ (all modern browsers), you can make the constructor property
     * non-enumerable if you define it like this instead
     */
    try {
        Object.defineProperty(sub.prototype, 'constructor', {
            enumerable: false,
            value: sub
        });
    } catch (error) {
        /* IE <= 8 will throw an error here because MS can't follow any kind of recommended standards. */
    }
};

CNN.VideoEndSlateBase.prototype = {
    /**
     * Retrieve the DOM id of the element that contains the video player
     * @returns {string} - DOM id
     */
    getVideoPlayerContainerId: function getVideoPlayerContainerId() {
        'use strict';

        return this.containerId;
    },

    /**
     * Renders the recommended videos carousel on the end slate
     * @param {object} $carouselContainer - the carousel container on the end slate
     * @param {array} data - array of the newly created recommended elements
     */
    renderRecommendedVideos: function renderRecommendedVideos($carouselContainer, data) {
        'use strict';

        var $carousel = $carouselContainer.data('owl.carousel');

        this.setResponsiveState();
        $carouselContainer.empty().append(data);

        function preventEventBubbling(e) {
            e.stopPropagation();
            e.preventDefault();
            return false;
        }

        /* prevent bubbling of all these events */

        $carouselContainer.on(CNN.Carousel.events.change, preventEventBubbling);
        $carouselContainer.on(CNN.Carousel.events.changed, preventEventBubbling);
        $carouselContainer.on(CNN.Carousel.events.drag, preventEventBubbling);
        $carouselContainer.on(CNN.Carousel.events.initialize, preventEventBubbling);
        $carouselContainer.on(CNN.Carousel.events.initialized, preventEventBubbling);
        $carouselContainer.on(CNN.Carousel.events.resized, preventEventBubbling);
        $carouselContainer.on(CNN.Carousel.events.navigationInitialized, preventEventBubbling);

        if (typeof $carousel !== 'undefined') {
            $carousel.destroy();
        }

        $carouselContainer.owlCarousel({
            mouseDrag:true,
            touchDrag:true,
            nav: true,
            navText: ['', ''],
            dots: false,
            autoWidth: true,
            autoHeight: true,
            margin: 20,
            rewindNav: true,
            responsive: false,
            scrollPerPage: false,
            items: 3,
            itemsDesktop: [1100, 3],
            itemsDesktopSmall: [900, 3],
            itemsTablet: [600, 2],
            itemsMobile: false,
            loop: true
        });
    },

    /**
     * Fetches the related video xml and creates elements for the
     * recommended videos carousel
     * @param {array} recommendedVideoContents - array to hold the recommended videos
     * @return {object} the promise object
     */
    getRecommendedVideos: function getRecommendedVideos(recommendedVideoContents) {
        'use strict';

        var relatedUrl = this.getRelatedContentUrl(),
            fetchUrl,
            endSlateCarouselId = 'end-slate-carousel' + Math.floor(Math.random() * 100);

        return jQuery.Deferred (function (dprom) {
            if (typeof relatedUrl === 'undefined') {
                dprom.resolve();
            } else {
                fetchUrl = relatedUrl.substring(relatedUrl.indexOf('/video/data/3.0'));
                jQuery.ajax({
                    url: fetchUrl,
                    type: 'GET',
                    dataType: 'json',
                    error: function (jqXHR, textStatus, errorThrown) {
                        if (typeof console !== 'undefined') {
                            console.log(errorThrown);
                        }
                    },
                    success: function (relatedXml) {
                        /* data, textStatus, jqXHR*/
                        var carouselData = {
                            relatedVideos: {
                                id: endSlateCarouselId,
                                containerContents: [],
                                layout: 'carousel-medium-strip',
                                type: 'container'
                            }
                        };

                        if (typeof relatedXml !== 'undefined' && relatedXml !== null && typeof relatedXml.videos !== 'undefined') {
                            jQuery(relatedXml.videos).each(function (index, elem) {
                                carouselData.relatedVideos.containerContents.push({
                                    cardContents: {
                                        headlineText: elem.headline,
                                        iconType: 'video',
                                        media: {
                                            contentType: 'image',
                                            cutFormat: '16:9',
                                            duration: elem.duration,
                                            elementContents: {
                                                imageAlt: elem.headline,
                                                imageUrl: elem.thumbnail_url,
                                                cuts: {
                                                    small: {
                                                        uri: elem.endslate_url_small
                                                    },
                                                    medium: {
                                                        uri: elem.endslate_url_medium
                                                    },
                                                    large: {
                                                        uri: elem.endslate_url_large
                                                    }
                                                },
                                                type: 'element'
                                            }
                                        },
                                        type: 'card',
                                        url: elem.clickback_url.substring('/video/?/video').replace('/video/?/video', '/videos'),
                                        videoId: elem.id
                                    },
                                    contentType: 'video',
                                    type: 'card'
                                });
                            });
                            dust.render('views/pages/common/video-end-slate-carousel', carouselData, function (err, out) {
                                recommendedVideoContents.push(out);
                            });
                        }

                        dprom.resolve();
                    }
                });
            }
        }).promise();
    },

    /**
     * Update the title/headline for the currently playing video. This value
     * should come from the "headline" property of the metadata for the video
     * and not from the title/caption/headline for the article or section
     * that the video is embedded on.
     *
     * @param {object} metaData - The metadata for the currently playing video
     */
    setCurrentVideoHeadline: function setCurrentVideoHeadline(metaData) {
        'use strict';

        var endSlate = this.getEndSlateForContainer(this.getContainerId()),
            headlineElem,
            metaJson = typeof metaData === 'string' ? JSON.parse(metaData) : metaData;

        if (typeof endSlate !== 'undefined' && endSlate) {
            headlineElem = jQuery(endSlate).find('.js-cd__headline-text');
            if (headlineElem) {
                headlineElem.text(metaJson.headline);
            }
        }
    },

    /**
     * Get recommended videos and render on the end slate
     * @param {object} metaData - the metadata object from CVP
     * @param {boolean} showCarousel - true if the carousel should be shown, false otherwise
     */
    fetchAndShowRecommendedVideos: function fetchAndShowRecommendedVideos(metaData, showCarousel) {
        'use strict';

        var recommendedVideoContents = [],
            carouselContainerSelector = '.js-video__end-slate__tertiary .js-video__end-slate__carousel',
            $carouselContainer;

        showCarousel = showCarousel || true;
        this.$videoEndSlate = this.getEndSlateForContainer();
        this.setCurrentVideoHeadline(metaData);
        $carouselContainer = this.$videoEndSlate.find(carouselContainerSelector);

        this.setRelatedContentUrl(metaData);
        if (showCarousel) {
            this.fetchRecommendedVideos($carouselContainer, recommendedVideoContents);
        }
    },

    /**
     * retrieves the related video content and makes the calls to render the carousel
     *
     * @param {object} carouselContainer - The DOM element of the owl carousel that holds the
     *                  related video data
     * @param {array} recommendedVideoContents - array of related videos
     */
    fetchRecommendedVideos: function fetchRecommendedVideos(carouselContainer, recommendedVideoContents) {
        'use strict';

        var self = this;

        jQuery.when(this.getRecommendedVideos(recommendedVideoContents)).done(function () {
            self.renderRecommendedVideos(carouselContainer, recommendedVideoContents);
        });

    },

    /**
     * Destroy the owl carousel and hide the related videos end slate
     */
    destroy: function destroy() {
        'use strict';

        var $carouselContainer = this.getCurrentCarousel(),
            $carousel = $carouselContainer.data('owl.carousel');

        this.hideEndSlateForContainer();
        $carouselContainer.empty();

        if (typeof $carousel !== 'undefined') {
            $carousel.destroy();
        }
    },

    /**
     * Retreives the Owl Carousel for the end slate, if it exists
     *
     * @returns {object} the owl carousel, if it exists
     */
    getCurrentCarousel:function getCurrentCarousel() {
        'use strict';

        var carouselContainerSelector = '.js-video__end-slate__tertiary .js-video__end-slate__carousel',
            $carouselContainer;

        this.$videoEndSlate = this.getEndSlateForContainer();
        $carouselContainer = this.$videoEndSlate.find(carouselContainerSelector);
        return $carouselContainer;
    },

    /**
     * Set the correct responsive state on the end-slate elements
     * @param {boolean} showSlate - true if the endslate should be shown, false otherwise
     */
    setResponsiveState: function setResponsiveState(showSlate) {
        'use strict';

        showSlate = showSlate || false;
        if (typeof this.getContainerId() !== 'undefined' && typeof $videoEndSlate === 'undefined') {
            this.$videoEndSlate = this.getEndSlateForContainer();
        }

        eqjs.query(this.$videoEndSlate);
        if (showSlate) {
            this.showEndSlateForContainer();
        }
    },

    /**
     * Returns the container id
     * @returns {string} - container ID
     */
    getContainerId: function getContainerId() {
        'use strict';

        return this.containerId;
    },

    /**
     * Replay video event handler
     */
    onReplayClickedHandler: function onReplayClickedHandler() {
        'use strict';

        var $endSlate = this.getEndSlateForContainer(),
            containerId = this.getVideoPlayerContainerId();

        $endSlate.removeClass('video__end-slate--active').addClass('video__end-slate--inactive');
        if (CNN.VideoPlayer.getLibraryName(containerId) === 'fave') {
            FAVE.player.getInstance(containerId).replay();
        } else {
            window.cnnVideoManager.getPlayerByContainer(containerId).replay();
        }

    },

    /**
     * Initialize the end slate
     */
    initialize: function initialize() {
        'use strict';

        this.addEventListeners();
    },

    /**
     * Adds required event listeners
     */
    addEventListeners: function addEventListeners() {
        'use strict';

        var endSlate = this.getEndSlateForContainer();

        endSlate.on('click', '.js-video__end-slate__replay', jQuery.proxy(this.onReplayClickedHandler, this));
        endSlate.on('click', '.js-video__end-slate__replay-text', jQuery.proxy(this.onReplayClickedHandler, this));
        endSlate.on('click', '.js-el__video__replayer-wrapper', jQuery.proxy(this.onReplayClickedHandler, this));

    },

    /**
     * Removes event listeners
     */
    removeEventListeners: function addEventListeners() {
        'use strict';

        var endSlate = this.getEndSlateForContainer();

        endSlate.off('click', '.js-video__end-slate__replay');
        endSlate.off('click', '.js-video__end-slate__replay-text');
    },

    /**
     * Implementation method for showing the end slate
     *
     * @param {object} endSlate - The end slate element
     */
    handleShowEndSlate: function handleShowEndSlate(endSlate) {
        'use strict';

        if (typeof endSlate !== 'undefined' && endSlate !== null) {
            this.setResponsiveState();
            endSlate.removeClass('video__end-slate--inactive').addClass('video__end-slate--active');
        }
    },

    /**
     * Implementation method for hiding  the end slate
     *
     * @param {object} endSlate - The end slate element
     */
    handleHideEndSlate: function handleHideEndSlate(endSlate) {
        'use strict';

        if (typeof endSlate !== 'undefined' && endSlate !== null) {
            this.setResponsiveState();
            endSlate.removeClass('video__end-slate--active').addClass('video__end-slate--inactive');
        }
    },

    /**
     * Looks up the end slate for the current container and hides it
     */
    hideEndSlateForContainer: function hideEndSlateForContainer() {
        'use strict';

        var $endSlate = this.getEndSlateForContainer();

        this.handleHideEndSlate($endSlate);
    },

    /**
     * Looks up the end slate for the current container and shows it
     */
    showEndSlateForContainer: function showEndSlateForContainer() {
        'use strict';

        var $endSlate = this.getEndSlateForContainer();

        this.handleShowEndSlate($endSlate);
    },

    /**
     * Set the related contents url as obtained from metaData
     * @param {string} metaData - the metadata object from CVP
     */
    setRelatedContentUrl: function setRelatedContentUrl(metaData) {
        'use strict';

        var metaJson;

        metaJson = typeof metaData === 'string' ? JSON.parse(metaData) : metaData;
        this.relatedContentUrl = metaJson.relatedVideosJson;
    },

    /**
     * Get the related contents url
     * @return {string} relatedContentUrl - the url to related videos
     */
    getRelatedContentUrl: function getRelatedContentUrl() {
        'use strict';

        return this.relatedContentUrl;
    },

    /**
     * Get the video end slate element
     * @returns {object} - the video end slate object
     */
    getEndSlateElement: function getEndSlateElement() {
        'use strict';

        return this.$videoEndSlate;
    },

    /**
     * Check if the end slate is enabled
     *
     * @returns {boolean} - true if enabled, false otherwise
     */
    isEndSlateEnabled: function () {
        'use strict';

        return this.endSlateFlag;
    }
};

/**
 * Provides functions to fetch the elements on the Video End Slate and render them.
 * The Video End Slate should appear when a play in page video finishes and there is
 * no auto-navigation to the next video as on pages like video leaf and video landing
 *
 * @name CNN.VideoEndSlate
 * @namespace
 * @memberOf CNN
 * @param {string} containerId - containerId of the video player container
 * @param {boolean} [endSlateFlag] - true if end slate is enabled
 */
CNN.VideoEndSlate = function (containerId, endSlateFlag) {
    'use strict';

    this.relatedContentUrl = '';
    this.$videoEndSlate = {};
    CNN.VideoEndSlateBase.call(this, containerId, endSlateFlag);
};

CNN.VideoEndSlate.prototype = {
    /**
     * override of the getEndSlateForContainer for base video implementations
     * @returns {object} - jQuery endSlate DOM element object
     */
    getEndSlateForContainer: function getEndSlateForContainer() {
        'use strict';

        var $endSlate = jQuery(document.getElementById(this.getContainerId())).parent().find('.js-video__end-slate').eq(0);
        return $endSlate;
    }

};

CNN.VideoEndSlateBase.extend(CNN.VideoEndSlateBase, CNN.VideoEndSlate);

/**
 * Provides functions to fetch the elements on the Video End Slate in a jumbotron
 * and render them.
 *
 * @name JumbotronVideoEndSlate
 * @namespace
 * @memberOf CNN
 *
 * @param {object} jumbotron - The jumbotron object
 * @param {string} mode - the current mode of the jumbotron (mobile|desktop|tablet)
 */
CNN.JumbotronVideoEndSlate = function (jumbotron, mode) {
    'use strict';

    this.jumbotron = jumbotron;
    this.mode = mode || 'desktop';
    CNN.VideoEndSlate.call(this, jQuery(jumbotron.jumbotronElementRaw).attr('id'), true);

};

/**
 * Overrides of the base class or jumbotron specific methods
 */
CNN.JumbotronVideoEndSlate.prototype = {
    /**
     * override of addEventListeners
     */
    addEventListeners: function addEventListeners() {
        'use strict';

        CNN.VideoEndSlateBase.prototype.addEventListeners.call(this);
        if (typeof this.jumbotron !== 'undefined') {
            jQuery(this.jumbotron.jumbotronElementRaw).on('CVPEvent', jQuery.proxy(this.onVideoEventHandler, this));
            jQuery(this.jumbotron).on('jumbotron-item-selected', jQuery.proxy(this.hideEndSlateForContainer, this));
            jQuery(this.jumbotron).on('jumbotron-mode-change', jQuery.proxy(this.onJumbotronModeChangeHandler, this));
        }
    },

    /**
     * override of removeEventListeners
     */
    removeEventListeners: function removeEventListeners() {
        'use strict';

        CNN.VideoEndSlateBase.prototype.removeEventListeners.call(this);
        if (typeof this.jumbotron !== 'undefined') {
            jQuery(this.jumbotron.jumbotronElementRaw).off('CVPEvent');
            jQuery(this.jumbotron).off('jumbotron-item-selected');
            jQuery(this.jumbotron).off('jumbotron-mode-change');
        }
    },

    /**
     * Event handler for when the jumbotron fires off the event for when it's mode
     * has changed. Eg mobile mode -> tablet mode
     *
     * @param {object} event - the Event object
     * @param {string} mode - the mode (mobile|desktop|tablet)
     */
    onJumbotronModeChangeHandler: function onJumbotronModeChangeHandler(event, mode) {
        'use strict';

        this.mode = mode;
        this.destroy();
    },

    /**
     * Event handler for video events that happen in the jumbotron, such as
     * video starts/completions/etc
     *
     * @param {object} event - the event object
     * @param {object} eventInst - additional information about the event
     */
    onVideoEventHandler: function onVideoEventHandler(event, eventInst) {
        'use strict';

        if (eventInst && eventInst.eventType) {
            switch (eventInst.eventType) {
            case 'onContentMetadata':
                this.fetchAndShowRecommendedVideos(eventInst.metadata, this.mode !== 'mobile');
                break;
            case 'onContentComplete':
                this.showEndSlateForContainer();
                break;
            case 'onContentPlay':
                this.hideEndSlateForContainer();
                break;

            }
        }
    },

    /**
     * Override of the getVideoPlayerContainerId method. The lookup for the video player
     * in a jumbotron is different than the base
     * @returns {string} - video player container ID
     */
    getVideoPlayerContainerId: function getVideoPlayerContainerId() {
        'use strict';

        var container;

        try {
            container = jQuery(jQuery(document.getElementById(this.getContainerId())).find('.js-jumbotron-video-player').eq(0));

            return container.attr('id');
        } catch (error) {
            return this.containerId;
        }
    },

    /**
     * Override of the getEndSlateForContainer method.
     * @returns {object} - jQuery endSlate DOM element object
     */
    getEndSlateForContainer: function getEndSlateForContainer() {
        'use strict';

        var $endSlate = jQuery(document.getElementById(this.getContainerId())).find('.js-video__end-slate').eq(0);

        return $endSlate;
    }
};

CNN.VideoEndSlateBase.extend(CNN.VideoEndSlateBase, CNN.JumbotronVideoEndSlate);

(function (jQuery) {
    'use strict';

    /*
     * wait until window load and then initialize end slates for any jumbotrons
     * that are on the page
     */
    jQuery(window).load(function () {
        var initJumbotronSlates = function () {
            var i,
                jumbotrons = [];

            window.CNN.jumbotronEndSlates = window.CNN.jumbotronEndSlates || [];
            if (window.cnnJumbotronManager && window.cnnJumbotronManager.getJumbotrons) {
                jumbotrons = window.cnnJumbotronManager.getJumbotrons();
                for (i = 0; i < jumbotrons.length; i++) {
                    if (jumbotrons[i] instanceof CNN.Jumbotron.Container) {
                        window.CNN.jumbotronEndSlates.push(new CNN.JumbotronVideoEndSlate(jumbotrons[i], 'tablet'));
                    }
                }
            }
        };
        if (window.cnnJumbotronManager) {
            initJumbotronSlates();
        } else {
            jQuery(document).on('jumbotron-initialization-complete', initJumbotronSlates);
        }
    });

}(jQuery));

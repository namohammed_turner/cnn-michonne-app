/* global CNN, fastdom, FAVE */

/**
 * @module VideoPinner
 * @description
 * pin Story Top Video once user scrolls pass it
 * pin only if video is playing (either video or Ad)
 * @param {string} pageTopContainerId - container ID for page top video player
 * @returns {object} - module exports
 */
CNN.VideoPinner = function (pageTopContainerId) {
    'use strict';

    var inbetweener_pinner1_target_class = 'js-inbetweener-pinner--type1',
        inbetweener_pinner2_target_class = 'js-inbetweener-pinner--type2',
        inbetweener_unpinner_target_class = 'js-inbetweener-unpinner',
        inbetweener_pinner1_class = 'inbetweener-pinner--type1',
        inbetweener_pinner2_class = 'inbetweener-pinner--type2',
        inbetweener_unpinner_class = 'inbetweener-unpinner',
        isPlaying = false,
        isVideoCollection = false,
        videopinning_classOff = 'videopinning--off',
        videopinning_classOn = 'videopinning--on',
        video = jQuery('.js-pg-rail-tall__head .media__video--responsive object, .js-pg-rail-tall__head .media__video--responsive > div.theoplayer'),
        videoWrapper = jQuery('.js-pg-rail-tall__head .js-media__video'),
        videoWrapperOffSet = videoWrapper.offset().left,
        videoTitle,
        videoCollectionTitle,
        videoCollectionTitleText,
        $body = jQuery('body'),
        $inbetweener_pinner1 = [], /* handles the animation from original to pinned video */
        $inbetweener_pinner2 = [], /* handles the animation from starting at railTop to pinn the video straight down  */
        $inbetweener_unpinner = [], /* handles the animation from pinned to original video */
        $storytop_container = jQuery(pageTopContainerId), /* container anchored in story content */
        $videoplayer = $storytop_container.find('object, > div.theoplayer'),
        footerLocked = false,
        carouselLoaded = jQuery.Deferred(),
        containerId = pageTopContainerId.replace(/#/, '');

    /* register for carousel loaded event */
    if (!(CNN.VideoConfig && CNN.VideoConfig.collection && CNN.VideoConfig.collection.delayVideoCarousel)) {
        /* Feature flag of video carousel. */
        carouselLoaded.resolve({});
    }

    jQuery(document).onCarouselLoaded(
        function onCarouselLoadedHandler() {
            carouselLoaded.resolve({});
        }
    );

    /**
     * @method handleOnVideoPlay
     *
     * @description
     * This function is called by featured-video-collection-player.js on CVP adPLay and ContentPlay events
     * We get the title from the current active owl carousel card for pre-roll ads, and on video starts
     *
    */
    function handleOnVideoPlay() {
        var videoData;
        if (isVideoCollection) {
            if (CNN.VideoPlayer.getLibraryName(containerId) === 'fave') {
                /*
                 * Get the data from the video content JSON to populate the headline
                 * instead of getting it from the Owl carousel.
                 */
                videoData = FAVE.player.getInstance(containerId).getVideoData();
                videoCollectionTitleText = videoData.headline;
            } else {
                videoCollectionTitleText = jQuery('.js-owl-carousel  .owl-item.active  .cd__headline-text').first().text();
            }

            if ($body.hasClass(videopinning_classOn)) {
                jQuery(videoCollectionTitle).find('.cd__headline-text').text(videoCollectionTitleText);
                jQuery('.js-pinned-video-collection-title .cd__headline-text').text(videoCollectionTitleText);
            } else {
                jQuery(videoCollectionTitle).find('.cd__headline-text').text(videoCollectionTitleText);
            }
        }
    }

    function setIsVideoCollection(isPageTopAVideoCollection) {
        isVideoCollection  = isPageTopAVideoCollection;
    }

    /**
     * @method setIsPlaying
     * @description
     * Video shouldn't pin unless the ad or the vidoe is currently playing
     *
     * @param {boolean} isContentPlaying
     * indicates if the actually video content or ad is playing
    */
    function setIsPlaying(isContentPlaying) {
        isPlaying = isContentPlaying;
    }

    /**
     * @method unpin
     * @description
     * handle remove the pinnied from the rail back to the original position
     *
     * @param {object} videoWrapper
     * wrapper element
     * @param {object} video
     * video element
    */
    function unpin(videoWrapper, video) {
        fastdom.measure(function () {
            var navBottom = jQuery('.nav')[0].getBoundingClientRect().bottom;

            if ($body.hasClass(videopinning_classOn)) {
                fastdom.mutate(function () {
                    /* Video: resize */
                    videoWrapper.css('height', '');
                    video.attr('style', '');
                    /* Transition effects */
                    $videoplayer.removeClass('fade-in');

                    if (videoTitle.length) {
                        videoTitle.removeClass('fade-in');
                        videoTitle.detach();
                    }

                    if (isVideoCollection) {
                        if (typeof videoCollectionTitle !== 'undefined' && videoCollectionTitle !== null) {
                            jQuery(videoCollectionTitle).removeClass('fade-in');
                            jQuery(videoCollectionTitle).detach();
                        }
                    }

                    $body.removeClass(inbetweener_pinner1_class);
                    $body.removeClass(inbetweener_pinner2_class);
                    $body.removeClass(videopinning_classOn);
                    $body.addClass(videopinning_classOff);

                    video.css('left', 'auto');
                    $inbetweener_unpinner.attr('style', '');
                    $inbetweener_pinner1.css({left: videoWrapper.offset().left});
                    $inbetweener_pinner2.css({left:jQuery('.pg-rail')[0].getBoundingClientRect().left, top: navBottom - 158});
                    $inbetweener_pinner2.removeClass('no-transition');
                    /* Video: fade back in */
                    setTimeout(function () {
                        $videoplayer.addClass('fade-in');
                    }, 300);
                });
            }
        });
    }

    /**
     * @method animateDown
     * @description
     * handle pinning video to the right rail from top rail straight down vertically
     *
    */
    function animateDown() {
        var navBottom = jQuery('.nav')[0].getBoundingClientRect().bottom,
            $truncCaption;

        if (!$body.hasClass(videopinning_classOn) && isPlaying) {
            if (videoWrapper[0].getBoundingClientRect().bottom  < navBottom) {
                fastdom.measure(function () {
                    var rightRailLeft = jQuery('.pg-rail')[0].getBoundingClientRect().left,
                        existingTitle = $storytop_container.find('.el__storyelement__title'),
                        videoWrapperWidth = videoWrapper.width(),
                        existingVideoCollectionTitle = jQuery('.js-pinned-video-collection-title .cd__headline-text');


                    if (rightRailLeft === 10) {
                        return;
                    }

                    fastdom.mutate(function () {
                        /* Transition effects */
                        $videoplayer.removeClass('fade-in');

                        if (videoTitle.length) {
                            videoTitle.removeClass('fade-in');
                        }

                        if (isVideoCollection) {
                            if (videoCollectionTitle.length) {
                                videoCollectionTitle.removeClass('fade-in');
                            }
                        }

                        $body.removeClass(videopinning_classOff);
                        $body.addClass(videopinning_classOn + ' ' + inbetweener_pinner2_class);
                        $inbetweener_pinner2.css({left: rightRailLeft, top: navBottom});
                        videoWrapper.css('height', videoWrapperWidth * 9 / 16);

                        if (!existingTitle.length) {
                            if (videoTitle.length) {
                                videoTitle.appendTo($storytop_container);
                            }
                        }

                        if (isVideoCollection) {
                            if (!existingVideoCollectionTitle.length) {
                                if (typeof videoCollectionTitle !== 'undefined' && videoCollectionTitle !== null) {
                                    jQuery(videoCollectionTitle).appendTo($storytop_container);
                                }
                            } else {
                                existingVideoCollectionTitle.text(videoCollectionTitleText);
                            }
                        }

                        video.css({
                            position: 'fixed',
                            top: navBottom + 10,
                            left: rightRailLeft,
                            width: '300px',
                            height: 300 * 9 / 16,
                            'z-index': '10'
                        });

                        $storytop_container.find('.el__storyelement__title').css({
                            position: 'fixed',
                            'background-color': '#fff',
                            left: rightRailLeft,
                            top: jQuery(video)[0].getBoundingClientRect().bottom,
                            width: '300px',
                            'z-index': '10'
                        });

                        setTimeout(function () {
                            $truncCaption = jQuery(document).find('.pinCurrentPlay').parent();
                            video.addClass('fade-in');
                            if (videoTitle.length) {
                                videoTitle.addClass('fade-in');
                                $truncCaption.find('.el__storyelement__header').dotdotdot();
                            }

                            if (isVideoCollection) {
                                if (typeof videoCollectionTitle !== 'undefined' && videoCollectionTitle !== null) {
                                    jQuery(videoCollectionTitle).addClass('fade-in');
                                }
                                $truncCaption.find('.cd__headline-text').dotdotdot();
                            }
                            scrollPinnedPlayer();
                        }, 300);
                    });
                });
            }
        }
    }

    /**
     * Scroll the pinning video when footer module reaches it
     *
     */

    function scrollPinnedPlayer() {
        var navBottom = jQuery('.nav')[0].getBoundingClientRect().bottom,
            footerPosition = jQuery('footer.l-footer')[0].getBoundingClientRect(),
            $videoCaption = $storytop_container.find('.el__storyelement__title'),
            captionPosition = $videoCaption.length > 0 ? $videoCaption[0].getBoundingClientRect() : {},
            videoPosition = video[0].getBoundingClientRect(),
            $combinedElement = video.add($inbetweener_pinner1).add($inbetweener_pinner2),
            setTop;

        if (footerPosition.top <= captionPosition.bottom || footerLocked) {
            footerLocked = true;
            setTop = footerPosition.top - captionPosition.height - 5;
            if (!$inbetweener_pinner2.hasClass('no-transition')) {
                $inbetweener_pinner2.addClass('no-transition');
            }
            $videoCaption.css({
                top: setTop
            });
            $combinedElement.css({
                top: setTop - videoPosition.height
            });
        }

        if (videoPosition.top > navBottom + 10) {
            footerLocked = false;
            $combinedElement.css({
                top: navBottom + 10
            });
            $videoCaption.css({
                top: videoPosition.height + navBottom + 10
            });
        }
    }

    /**
     * Initialize
     *  - setup transition elements
     *  - setup scroll listener
     *  - setup browser reize listener - setups up new position for pinned player
     */
    function init() {
        /* Transitions Elements: Setup transition effect elements */
        var inbetweener_pinner_container = document.createElement('div'),
            inbetweener_pinner_container2 = document.createElement('div'),
            inbetweener_unpinner_container = document.createElement('div'),
            imageBG,
            navBottom = jQuery('.nav')[0].getBoundingClientRect().bottom,
            videoPinner;

        inbetweener_pinner_container.className = inbetweener_pinner1_target_class + ' ' + inbetweener_pinner1_class;
        inbetweener_pinner_container2.className = inbetweener_pinner2_target_class + ' ' + inbetweener_pinner2_class;
        inbetweener_unpinner_container.className = inbetweener_unpinner_target_class + ' ' + inbetweener_unpinner_class;

        $videoplayer.before(inbetweener_unpinner_container);
        $videoplayer.before(inbetweener_pinner_container);
        $videoplayer.before(inbetweener_pinner_container2);

        $inbetweener_pinner1 = $storytop_container.find('.' + inbetweener_pinner1_target_class);
        $inbetweener_pinner2 = $storytop_container.find('.' + inbetweener_pinner2_target_class);
        $inbetweener_unpinner = $storytop_container.find('.' + inbetweener_unpinner_target_class);

        $inbetweener_pinner1.css({left:videoWrapperOffSet, top: videoWrapper[0].getBoundingClientRect().top});
        $inbetweener_pinner2.css({left:jQuery('.pg-rail')[0].getBoundingClientRect().left, top: navBottom - 158});
        $inbetweener_unpinner.css({left:jQuery('.pg-rail')[0].getBoundingClientRect().left - videoWrapperOffSet, top: navBottom});


        /* set pinner's initial position */
        video.css({'z-index': '4'});

        imageBG = videoWrapper.find('.media__video--thumbnail .media__image');
        videoTitle = videoWrapper.children('.el__storyelement__title');


        if (videoTitle.length) {
            videoTitle = jQuery(videoTitle).first().clone();
            videoTitle.children().not('.el__storyelement__header').remove();
            videoTitle.prepend(jQuery('<span class=\'pinCurrentPlay\'>Now Playing </span>'));
        }

        if (isVideoCollection) {
            videoCollectionTitle = document.createElement('div');
            videoCollectionTitle.className = 'media__caption el__storyelement__title js-pinned-video-collection-title';
            carouselLoaded.then(
                function carouselLoadedThenHandler() {
                    if (CNN.VideoPlayer.getLibraryName(containerId) === 'fave') {
                        videoCollectionTitleText = jQuery('.js-owl-carousel .carousel__content__item .cd__headline-text').first().clone();
                    } else {
                        videoCollectionTitleText = jQuery('.js-owl-carousel  .owl-item.active  .cd__headline-text').first().clone();
                    }

                    if (videoCollectionTitleText.length) {
                        jQuery(videoCollectionTitle).prepend(jQuery('<span class=\'pinCurrentPlay\'>Now Playing </span>'));
                        jQuery(videoCollectionTitleText).appendTo(videoCollectionTitle);
                    }
                }
            );
        }

        if (imageBG.length) {
            jQuery(imageBG).first().attr('src', CNN.Host.assetPath + '/assets/video_pinned_white_bg.jpg');
        } else {
            /* this is leaf page with videoCollection in pageTop. */
            imageBG = document.createElement('img');
            imageBG.setAttribute('src', CNN.Host.assetPath + '/assets/video_pinned_white_bg.jpg');
            videoWrapper.find('.media__video--thumbnail').first().prepend(imageBG);
        }

        videoPinner = (function createScrollSemaphore() {

            function doScrollWork() {
                var navBottom = jQuery('.nav')[0].getBoundingClientRect().bottom,
                    $truncCaption,
                    videoWrapperPosition = videoWrapper[0].getBoundingClientRect();

                if ((videoWrapperPosition.bottom - (videoWrapperPosition.height * 0.6)) < navBottom) {
                    if (!$body.hasClass(videopinning_classOn) && isPlaying) {
                        fastdom.measure(function () {
                            var rightRailLeft = jQuery('.pg-rail')[0].getBoundingClientRect().left,
                                existingTitle = $storytop_container.find('.el__storyelement__title').not('.js-pinned-video-collection-title'),
                                existingVideoCollectionTitle = jQuery('.js-pinned-video-collection-title .cd__headline-text'),
                                videoWrapperWidth = videoWrapper.width();

                            if (rightRailLeft === 10) {
                                return;
                            }

                            fastdom.mutate(function () {
                                /* Transition effects */
                                $videoplayer.removeClass('fade-in');

                                if (videoTitle.length) {
                                    videoTitle.removeClass('fade-in');
                                }

                                if (isVideoCollection) {
                                    if (videoCollectionTitle.length) {
                                        videoCollectionTitle.removeClass('fade-in');
                                    }
                                }

                                $body.removeClass(videopinning_classOff);
                                $body.addClass(videopinning_classOn + ' ' + inbetweener_pinner1_class);
                                $inbetweener_pinner1.css({left: rightRailLeft, top: navBottom});
                                $inbetweener_unpinner.css({left: rightRailLeft - videoWrapperOffSet, top: navBottom});
                                videoWrapper.css('height', videoWrapperWidth * 9 / 16);

                                if (!existingTitle.length) {
                                    if (videoTitle.length) {
                                        videoTitle.appendTo($storytop_container);
                                    }
                                }

                                if (isVideoCollection) {
                                    if (!existingVideoCollectionTitle.length) {
                                        if (typeof videoCollectionTitle !== 'undefined' && videoCollectionTitle !== null) {
                                            jQuery(videoCollectionTitle).appendTo($storytop_container);
                                        }
                                    } else {
                                        existingVideoCollectionTitle.text(videoCollectionTitleText);
                                    }
                                }

                                video.css({
                                    position: 'fixed',
                                    top: navBottom + 10,
                                    left: rightRailLeft,
                                    width: '300px',
                                    height: 300 * 9 / 16,
                                    /*
                                     * For HTML5 video, the z-index of the player container needs to be
                                     * above the inbetweener and headline DIVs to ensure that when the
                                     * player's context menu is viewed on desktop, there's no overlap.
                                     */
                                    'z-index': CNN.VideoPlayer.getLibraryName(containerId) === 'fave' ? '11' : '10'
                                });

                                $storytop_container.find('.el__storyelement__title').css({
                                    position: 'fixed',
                                    'background-color': '#fff',
                                    left: rightRailLeft,
                                    top: jQuery(video)[0].getBoundingClientRect().bottom,
                                    width: '300px',
                                    'margin-top': '0',
                                    'z-index': '10'
                                });

                                setTimeout(function () {
                                    $truncCaption = jQuery(document).find('.pinCurrentPlay').parent();
                                    video.addClass('fade-in');
                                    if (videoTitle.length) {
                                        videoTitle.addClass('fade-in');
                                        $truncCaption.find('.el__storyelement__header').dotdotdot();
                                    }
                                    if (isVideoCollection) {
                                        if (typeof videoCollectionTitle !== 'undefined' && videoCollectionTitle !== null) {
                                            jQuery(videoCollectionTitle).addClass('fade-in');
                                        }
                                        $truncCaption.find('.cd__headline-text').dotdotdot();
                                    }
                                    jQuery(document).find('.pinCurrentPlay').parent().find('.cd__headline-text').dotdotdot();
                                }, 300);
                            });
                        });
                    } else {
                        /* Move the pinned player when footer reaches to it. */

                        scrollPinnedPlayer();
                    }
                } else {
                    unpin(videoWrapper, video);
                }
            }

            function doResizeWork() {
                fastdom.measure(function () {
                    if ($body.hasClass(videopinning_classOn)) {
                        if (jQuery('.pg-rail')[0].getBoundingClientRect().left === 10) {
                            unpin(videoWrapper, video);
                        } else {
                            fastdom.mutate(function () {
                                $videoplayer.css('left', jQuery('.pg-rail')[0].getBoundingClientRect().left);
                                $inbetweener_pinner1.css({left: jQuery('.pg-rail')[0].getBoundingClientRect().left});
                                $inbetweener_pinner2.css({left: jQuery('.pg-rail')[0].getBoundingClientRect().left});
                                $inbetweener_unpinner.css({left:jQuery('.pg-rail')[0].getBoundingClientRect().left - videoWrapperOffSet});
                                if (videoTitle.length) {
                                    videoTitle.css('left', jQuery('.pg-rail')[0].getBoundingClientRect().left);
                                }

                                if (CNN.Utils.exists(videoCollectionTitle)) {
                                    jQuery(videoCollectionTitle).css('left', jQuery('.pg-rail')[0].getBoundingClientRect().left);
                                }
                            });
                        }
                    }

                    if ($body.hasClass(videopinning_classOff)) {
                        $inbetweener_pinner1.css({left: videoWrapper.offset().left});
                        $inbetweener_pinner2.css({left:jQuery('.pg-rail')[0].getBoundingClientRect().left, top: navBottom - 158});
                    }
                });
            }

            return {
                scroll: doScrollWork,
                resize: doResizeWork
            };
        }());

        jQuery(document).onZonesAndDomReady(function () {
            jQuery(window).on('scroll', videoPinner.scroll);
            jQuery(window).on('resize', videoPinner.resize);
        });
    }
    return {
        init: init,
        setIsPlaying: setIsPlaying,
        animateDown: animateDown,
        setIsVideoCollection: setIsVideoCollection,
        handleOnVideoPlay: handleOnVideoPlay
    };
};

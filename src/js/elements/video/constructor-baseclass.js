/* global CNN */

/**
 * Javascript to create new document fragmetns for Watch CNN.
 *
 * @name CNN.VideoDocFragment
 * @namespace
 * @memberOf CNN
 */
CNN.VideoDocFragment = CNN.VideoDocFragment || {};
CNN.VideoDocFragment.cards = CNN.VideoDocFragment.cards || [];

/**
 * Base class for creating WatchCNN document fragments.
 *
 * Javascript Module Pattern
 * @see http://www.adequatelygood.com/JavaScript-Module-Pattern-In-Depth.html
 *
 * @name CNN.VideoDocFragment.BaseClass
 * @namespace
 * @memberOf  CNN
 */
CNN.VideoDocFragment.BaseClass = CNN.VideoDocFragment.BaseClass || (function BaseClass() {
    'use strict';

    /* Create a closure. Variables in here will be unique the instance. */
    return function VideoDocFragment() {
        var pub = {},
            type = '',
            eventNames = [],
            registeredEvents = [],
            eventRegistered = false;

        /**
         * Add a unique event listener for each fragment-type and `eventName`.
         *
         * @memberOf CNN.VideoDocFragment.BaseClass
         */
        function registerEvent() {
            var i = 0,
                j = 0,
                regEvts = [];

            function BaseClassEventHandlerCallback() {
                var i = 0;

                for (i = CNN.VideoDocFragment.cards.length - 1; i >= 0; i--) {
                    if (typeof CNN.VideoDocFragment.cards === 'undefined') {
                        throw 'CNN.VideoDocFragment.cards is undefined. Cannot register event listeners.';
                    }

                    if (CNN.VideoDocFragment.cards[i].type === type) {
                        pub.fetchData(CNN.VideoDocFragment.cards[i]);
                    }
                }
            }

            /* Look to see if each eventName was already registered. if not, register it. */
            for (i = eventNames.length - 1; i >= 0; i--) {
                /* Reset eventRegistered for each iteration. */
                eventRegistered = false;

                for (regEvts = registeredEvents, j = regEvts.length - 1; j >= 0; j--) {
                    if (eventNames[i] === regEvts[j]) {
                        /* Event already exists. Next! */
                        eventRegistered = true;

                        break;
                    }
                }

                if (eventRegistered !== true) {
                    /* New event. Register it */
                    registeredEvents.push(eventNames[i]);

                    /* Add the event listener */
                    jQuery(document).on(eventNames[i], BaseClassEventHandlerCallback);
                }
            }
        }

        /**
         * Initilization function
         *
         * @param   {string} typeArg
         * The fragment-type to be displayed. This will be unique based on the
         * source of the data and the type of element needed to be rendered.
         *
         * @param   {object} config
         * Configuration object used for init functions.
         *
         * @param   {string} config.network
         * The name of the network.
         *
         * @param   {string} config.selector
         * jQuery formatted selector. e.g. '.js-player'.
         *
         * @memberOf CNN.VideoDocFragment.BaseClass
         */
        pub.init = function (typeArg, config) {
            if (typeof typeArg !== 'string') {
                throw 'typeArg is required to be a string.';
            } else if (typeof config !== 'object') {
                throw 'config object is not properly defined.';
            } else if (typeof config.network !== 'string') {
                throw 'config.string must be defiend.';
            } else if (typeof config.selector !== 'string') {
                throw 'config.string must be defined.';
            }

            type = typeArg;

            /* Wait until document ready */
            jQuery(function initVideoDocFragment() {
                var i = 0;

                /* Don't add cards if they've already been accounted for. */
                for (i = CNN.VideoDocFragment.cards.length - 1; i >= 0; i--) {
                    if (CNN.VideoDocFragment.cards[i].id === config.network + '-' + type) {
                        return;
                    }
                }

                CNN.VideoDocFragment.cards.push(new CNN.VideoDocFragment.CardConstructor(type, config.network, config.selector));

                registerEvent();
            });
        };

        /**
         * Public method to set the event name.
         *
         * @param   {string} name  - The event name to set.
         *
         * @memberOf CNN.VideoDocFragment.BaseClass
         */
        pub.setEventName = function (name) {
            var i = 0;

            if (typeof name === 'undefined') {
                throw 'name is not defiend.';
            }

            for (i = eventNames.length - 1; i >= 0; i--) {
                if (eventNames[i] === name) {
                    throw name + ' is already a registered event of ' + pub.id;
                }
            }

            eventNames.push(name);
        };

        /**
         * Fetch the data specific to the data-source. This function is intended to be overriden with each instantiation.
         *
         * @abstract
         * @memberOf CNN.VideoDocFragment.BaseClass
         */
        pub.fetchData = function () {
            /* possible arguments: card */
            throw 'fetchData must be implemented by subclass.';
        };

        /**
         * Function to repaint the DOM.
         *
         * @param   {object} card  - The CMS card to update.
         *
         * @memberOf CNN.VideoDocFragment.BaseClass
         */
        pub.updateDom = function (card) {
            if (typeof card === 'undefined') {
                throw 'card is not defiend.';
            }

            card.$parent.empty().append(card.docFragment.frag);
        };

        return pub;
    };
}());

/**
 * Create the basic model for the card fragments.
 *
 * @name CNN.VideoDocFragment.CardConstructor
 * @memberOf  CNN
 * @constructor
 *
 * @param   {string} type
 * The fragment-type to be displayed. This will be unique based on the source of
 * the data and the type of element needed to be rendered.
 *
 * @param   {string} network
 * The name of the network.
 *
 * @param   {string} selector
 * jQuery formatted selector. e.g. '.js-player'.
 *
 * @returns {object}         - Basic card object.
 */
CNN.VideoDocFragment.CardConstructor = function (type, network, selector) {
    'use strict';
    var pub = {};

    if (typeof type !== 'string') {
        throw 'type is not a string.';
    } else if (typeof network !== 'string') {
        throw 'network is not a string.';
    } else if (typeof selector !== 'string') {
        throw 'selector is not a string.';
    }

    pub.id = network + '-' + type;
    pub.type = type;
    pub.network = network;
    pub.$parent = jQuery(selector);

    pub.data = {
        raw: {},
        src: {},
        string: {}
    };

    pub.docFragment = {};

    return pub;
};

/**
 * Javascript to create new document fragments for Watch CNN.
 *
 * @name CNN.VideoDocFragment.newFragment
 * @memberOf CNN
 *
 * @example
 * // Wait until document is ready.
 * jQuery(function () {
 *     // Assume the script loaded successfuly and CNN.VideoDocFragment.player exists.
 *     window.CNN.VideoDocFragment.newFragment('player', {
 *         network: 'cnn',
 *         selector: '.js-player'
 *     });
 * });
 *
 * @param {string} type
 * The fragment-type to be displayed. This will be unique based on the source of
 * the data and the type of element needed to be rendered.
 *
 * @param {object} config
 * Configuration object used for init functions.
 */
CNN.VideoDocFragment.newFragment = function (type, config) {
    'use strict';

    if (CNN.VideoDocFragment[type]) {
        CNN.VideoDocFragment[type].init(type, config);
    } else {
        throw type + ' is not a valid property of CNN.VideoDocFragment.';
    }
};


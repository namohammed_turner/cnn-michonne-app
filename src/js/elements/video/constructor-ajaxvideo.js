/* global CNN */

/**
 * Base class for interacting with external server files.
 * Periodoicly polls the server for new data.
 *
 * Javascript Module Pattern
 * @see http://www.adequatelygood.com/JavaScript-Module-Pattern-In-Depth.html
 *
 * @name CNN.VideoAjax
 * @namespace
 * @memberOf CNN
 *
 * @example
 * // Wait until document is ready.
 * jQuery(function () {
 *     var namespace = 'NowPlaying';
 *
 *     // Always make sure that the file exists and is loaded before any referencing files depend on it.
 *     if (typeof window.CNN.VideoAjax === 'undefined' || typeof CNN.VideoAjax[namespace] === undefined) {
 *         throw 'Cannot fetch data from the server. CNN.VideoAjax or the namespace is not defined';
 *     }
 *
 *     // If everything looks good and the file is properly loaded, add an event listener.
 *     document.addEventListener(pub.getEventName(), function handler() {
 *        // Add custom callback code here.
 *     }, false);
 *
 *     // Then, init the function
 *     CNN.VideoAjax[namespace].init();
 * });
 *
 */
CNN.VideoAjax = CNN.VideoAjax || (function () {
    'use strict';

    /**
     * Create a closure. Variables in here will be unique the instance.
     *
     * @param {object} customConfig - Custom configuration object to extend the default configurations.
     * @returns {object} - a unique video ajax object instance
     */
    return function (customConfig) {
        /**
         * @property {object}   pub
         * Temporary variable collecting all public methods and members.
         *
         * @property {object}   defaults
         * Default configuation to be extended.
         *
         * @property {string}   defaults.id
         * Name of the instance. Useful for debugging.
         *
         * @property {string}   defaults.eventName
         * Name of the custom event.
         *
         * @property {integer}  defaults.ajaxFrequency
         * Calculated number of milliseconds to re-poll the
         * nowPlayingSchedule.json (Minutes * Seconds * Miliseconds).
         *
         * @property {string}   config
         * Configuration to be used.
         *
         * @property {object}   serverData
         * Cached data object retrieved from ajax request.
         *
         * @property {object}   fetchOnNowTimer
         * setTimeout value for periodic polling
         *
         * @property {event}    autoUpdateEvent
         * Custom event to alert the document when the serverData is repolled.
         *
         * @memberOf CNN.VideoAjax
         */
        var pub = {},
            defaults = {
                id: 'VideoAjax',
                eventName: 'CNN_VideoAjax_autoUpdated',
                ajaxFrequency: 5 * 60 * 1000
            };

        pub.config = jQuery.extend({}, defaults, customConfig);
        pub.autoUpdateEvent = jQuery.Event(pub.config.eventName);
        pub.fetchOnNowTimer = null;
        pub.isFetching = false;
        pub.serverData = null;

        /**
         * Function for displaying jQuery Ajax Errors
         *
         * @param {jqXHR}   data        - Required. jQuery Ajax jqXHR.
         * @param {string} [reason='']  - Text status.
         *
         * @see http://api.jquery.com/jQuery.ajax/
         *
         * @memberOf CNN.VideoAjax
         */
        pub.logError = function (data, reason) {
            if (typeof reason !== 'undefined') {
                throw reason;
            }

            throw data;
        };

        /**
         * Fires pub.fetchExternalData, caches the value, and dispatches an event.
         *
         * @memberOf CNN.VideoAjax
         * @private
         */
        pub.updateOnNowData = function () {
            this.fetchExternalData(jQuery.proxy(function fetchExternalDataCallback(data) {
                this.serverData = data;

                /* Fire an event */
                jQuery(document).trigger(this.autoUpdateEvent);
            }, this));
        };

        /**
         * ID of the instance. Useful for debugging.
         *
         * @type {string}
         */
        pub.id = pub.config.id;

        /**
         * Inititialization function.
         */
        pub.init = function () {
            this.updateOnNowData();
            this.startPeriodicFetch();
        };

        /**
         * Stop the periodical polling.
         */
        pub.stopPeriodicFetch = function () {
            if (this.fetchOnNowTimer !== null) {
                clearInterval(this.fetchOnNowTimer);
                this.fetchOnNowTimer = null;
            }
        };

        /**
         * Start the periodical polling. Fires the custom event `autoUpdateEvent`.
         */
        pub.startPeriodicFetch = function () {
            if (this.fetchOnNowTimer === null) {
                this.fetchOnNowTimer = setInterval(jQuery.proxy(this.updateOnNowData, this), this.config.ajaxFrequency);
            }
        };

        /**
         * AJAX call to data.cnn.com.
         */
        pub.fetchExternalData = function () {
            throw 'fetchExternalData in the ajax was intended to be overridden.';
        };

        /**
         * Taking the unedited JSON object and extracting data specific to the parent site.
         *
         * @param {string} filter - Required. The name of the network to filter by.
         * @returns {object} - filtered data object
         */
        pub.filterNetwork = function (filter) {
            var i = 0,
                obj = null;

            /* Make sure there is server data to filter */
            if (typeof this.serverData === 'undefined' || this.serverData === null) {
                return obj;
            }

            /* Find the matching filter in the show data feed. */
            for (i = this.serverData.channel.length - 1; i >= 0; i--) {
                if (this.serverData.channel[i].id.toLowerCase() === filter.toLowerCase()) {
                    obj = this.serverData.channel[i];

                    break;
                }
            }

            return obj;
        };

        /**
         * Simple getter for returning the cached serverData
         *
         * @returns {object} serverData
         */
        pub.getServerData = function () {
            return this.serverData;
        };

        /**
         * Simple getter for returning the custom event name when the ajax file is auto-updated.
         *
         * @returns {string} this.config.eventName
         */
        pub.getEventName = function () {
            return this.config.eventName;
        };

        pub.getIsFetching = function () {
            return this.isFetching;
        };

        pub.setIsFetching = function (bool) {
            this.isFetching = bool;
        };

        return pub;
    };
}());

/* global CNN */

/**
 * Javascript for interacting with the now-playing-schedule.json file.
 * Periodoicly polls the server for new data.
 *
 * Javascript Module Pattern
 * @see http://www.adequatelygood.com/JavaScript-Module-Pattern-In-Depth.html
 *
 * @name CNN.VideoAjax.NowPlaying
 * @namespace
 * @memberOf CNN
 *
 * @example
 * // Wait until document is ready.
 * jQuery(function () {
 *     var namespace = 'NowPlaying';
 *
 *     // Always make sure that the file exists and is loaded before any referencing files depend on it.
 *     if (typeof window.CNN.VideoAjax === 'undefined' || typeof CNN.VideoAjax[namespace] === undefined) {
 *         throw 'Cannot fetch data from the server. CNN.VideoAjax or the namespace is not defined';
 *     }
 *
 *     // If everything looks good and the file is properly loaded, add an event listener.
 *     document.addEventListener(pub.getEventName(), function handler() {
 *        // Add custom callback code here.
 *     }, false);
 *
 *     // Then, init the function
 *     CNN.VideoAjax[namespace].init();
 * });
 *
 */
CNN.VideoAjax.NowPlaying = CNN.VideoAjax.NowPlaying || (function NowPlaying() {
    'use strict';

    var
        pub = CNN.VideoAjax({
            id: 'NowPlaying',
            eventName: 'CNN_NowPlaying_autoUpdated'
        });

    /**
     * AJAX call to data.cnn.com.
     *
     * @param {function} callback   - Required callback parameter.
     */
    pub.fetchExternalData = function (callback) {
        /* Don't fetch the data if there's already an AJAX request in progress. */
        if (pub.getIsFetching() === true) { return; }

        /* If callback paramerter is defined, make sure it is a function. */
        if (typeof callback === 'undefined' || typeof callback !== 'function') {
            throw 'callback parameter is not defined or is not a function. Ajax request was not sent.';
        }

        /* Set isFetching to prevent multiple duplicate AJAX requests. */
        pub.setIsFetching(true);

        /* Fire AJAX request. */
        jQuery.ajax({
            url: CNN.Host.nowPlaying + '/jsonp/video/nowPlayingSchedule.json',
            type: 'get',
            error: function (data, status) {
                pub.logError(data, status);
            },
            success: function (data) {
                callback(data);
            },
            timeout: function (data, status) {
                pub.logError(data, status);
            },
            complete: function () {
                /* Reset isFetching to allow future requests. */
                pub.setIsFetching(false);
            },
            dataType: 'jsonp',
            jsonpCallback: 'nowPlayingScheduleCallbackWrapper'
        });
    };

    return pub;
}());


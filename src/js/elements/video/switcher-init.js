/* global CNN */

CNN.INJECTOR.executeFeature('videoLoader', {params:'?version=latest&client=expansion'}).then(function () {
    'use strict';

    window.CNN.VideoDocFragment.autoInit = window.CNN.VideoDocFragment.autoInit || false;

    if (window.CNN.VideoDocFragment.autoInit !== true) {
        jQuery(function togglePlayerOnReady() {
            var i,
                $x,
                $i,
                type,
                network,
                isMobile;

            if (typeof window.CNN.VideoAjax === 'undefined' || typeof window.CNN.VideoAjax.NowPlaying === 'undefined') {
                throw 'Cannot fetch Now Playing Schedule. CNN.VideoAjax is undefined.';
            } else if (typeof window.CNN.VideoDocFragment === 'undefined' || typeof window.CNN.VideoDocFragment.newFragment === 'undefined') {
                throw 'CNN.VideoDocFragment or CNN.VideoDocFragment.newFragment is undefiend';
            }

            $x = jQuery('.js-tve-toggle__config');
            isMobile = window.CNN.VideoPlayer.isMobileClient();

            for (i = $x.length - 1; i >= 0; i--) {
                $i = $x.eq(i);
                type = $i.find('.js-tve-toggle__type').val();
                network = $i.find('.js-tve-toggle__network').val();

                /* displays auth-switcher tool-card only for non-mobile clients */
                /* If it's not mobile AND if it's not the authPlayer */
                if (!isMobile && type === 'authPlayerToggle') {
                    window.CNN.VideoAjax.NowPlaying.init();
                } else if (type === 'livePlayerToggle') {
                    window.CNN.VideoAjax.LiveStream.init();
                }
                window.CNN.VideoDocFragment.newFragment(type, {
                    network: network,
                    selector: '.js-now-playing--' + network
                });
            }
        });
    }

    /* Prevent multiple initations */
    window.CNN.VideoDocFragment.autoInit = true;
});

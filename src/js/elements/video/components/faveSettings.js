/* global CNN, FAVE, Modernizr */

(function () {
    'use strict';

    FAVE.settings.freewheel = FAVE.settings.freewheel || {};
    FAVE.settings.freewheel.enabled = CNN.Features.enableFreewheel;
    FAVE.settings.freewheel.networkId = CNN.AdsConfig.fwNetworkId;

    /**
     * Creates an object of Freewheel key/value pairs.
     * @returns {object} keyValuePairs
     */
    FAVE.settings.freewheel.keyValuePairs = (function freewheelKeyValuePairs() {
        /*
         * Key/value pairs to be added to the Freewheel context's key/value pair set.
         * The values must be of type string or function. Value of type function returns
         * a string or an object. If the value is a function, it will
         * be invoked in the Freewheel component when key value pairs are being added
         * to the Freewheel context.
         */
        var keyValuePairs = {
            adkey: CNN.getAdkey,
            capTopics: CNN.getCapTopics,
            guID: window.turner_getGuid,
            krux: window.krux_getFWKeyValues,
            krux_user: window.krux_getUser,
            proximicData: CNN.getProximicData,
            protocol: (window.location.protocol === 'https:') ? 'ssl' : 'non-ssl',
            refdom: CNN.getRefDom,
            transactionID: window.turner_getTransactionId
        };

        if (CNN.Features.enableAmazonVideoAds === true &&
            typeof window.AmazonDirectMatchBuy === 'object' &&
            typeof window.AmazonDirectMatchBuy.init === 'function') {
            keyValuePairs.amazon = window.amznads && window.amznads.getTargeting;
            window.AmazonDirectMatchBuy.init({
                amznkey: CNN.AdsConfig.amazon.amznkey,
                timeout: typeof CNN.AdsConfig.amazon.timeout === 'number' ?
                    CNN.AdsConfig.amazon.timeout : 2000
            });
        }

        return keyValuePairs;
    })();

    /**
     * Sets the Site Section ID (SSID) based on page type, section name, and video content metadata.
     * This function is invoked within the FAVE Library's Freewheel component before an ad request.
     *
     * @see {@link http://docs.turner.com/display/MND/Freewheel+Site+Section+ID+hierarchies+on+CNN.com|Freewheel Site Section ID hierarchies on CNN.com}
     * @name siteSectionIdSelection
     * @param {object} configs - the configs passed into the player when instantiated
     * @param {object} videoJSON - the video metadata object
     * @returns {string} siteSectionId - the derived site section ID
     */
    FAVE.settings.freewheel.siteSectionIdSelection = function siteSectionIdSelection(configs, videoJSON) {
        var sectionConfig = (Modernizr.phone) ?
                CNN.SectionConfig.mobile.contexts :
                CNN.SectionConfig.desktop.contexts,
            sectionName = CNN.contentModel.sectionName,
            defaultAdSection = sectionConfig['default'].adsection,
            hasConfigsAdSection,
            siteSectionId,
            constantSsidPrefix = FAVE.settings.freewheel.constantSsidPrefix,
            existsObject = CNN.Utils.existsObject,
            existsString = CNN.Utils.existsString,
            sectionConfigHasOverrideKey = function (sectName, key) {
                return (existsObject(sectionConfig[sectName]) &&
                    existsObject(sectionConfig[sectName].adSectionOverrideKeys) &&
                    existsString(sectionConfig[sectName].adSectionOverrideKeys[key])) ? true : false;
            },
            sectionConfigHasAdSection = function (sectName) {
                return (existsObject(sectionConfig[sectName]) &&
                    existsString(sectionConfig[sectName].adsection)) ? true : false;
            };

        switch (CNN.contentModel.pageType) {

        /* Section fronts */
        case 'section':
            /* Video section front */
            if (CNN.contentModel.sectionName === 'videos') {
                /* If we have a SSID in the video JSON, use it. */
                if (existsString(videoJSON.adSection) && videoJSON.adSection.indexOf(constantSsidPrefix) === -1) {
                    siteSectionId = videoJSON.adSection;
                } else if (sectionConfigHasAdSection(sectionName)) {
                    /* Look in the section config, and use the adsection value relevant to the section name to get an SSID. */
                    siteSectionId = sectionConfig[sectionName].adsection;
                } else {
                    siteSectionId = defaultAdSection;
                }
            } else {
                /* All other section fronts */

                /* If the adsection passed into the configs is a SSID, use it. */
                if (existsString(configs.adsection) && configs.adsection.indexOf(constantSsidPrefix) === -1) {
                    siteSectionId = configs.adsection;
                } else if (sectionConfigHasOverrideKey(sectionName, 'const-article-pagetop')) {
                    siteSectionId = sectionConfig[sectionName].adSectionOverrideKeys['const-article-pagetop'];
                } else if (sectionConfigHasAdSection(sectionName)) {
                    siteSectionId = sectionConfig[sectionName].adsection;
                } else {
                    siteSectionId = defaultAdSection;
                }
            }
            break;

        /* Video leaf pages */
        case 'video':
            /* Use the section name of the current video */
            sectionName = videoJSON.sectionName;
            /* If we have a SSID in the video JSON, use it. */
            if (existsString(videoJSON.adSection) && videoJSON.adSection.indexOf(constantSsidPrefix) === -1) {
                siteSectionId = videoJSON.adSection;
            } else if (sectionConfigHasOverrideKey(sectionName, 'const-video-leaf')) {
                siteSectionId = sectionConfig[sectionName].adSectionOverrideKeys['const-video-leaf'];
            } else if (sectionConfigHasOverrideKey('default', 'const-video-leaf')) {
                siteSectionId = sectionConfig['default'].adSectionOverrideKeys['const-video-leaf'];
            } else {
                siteSectionId = defaultAdSection;
            }
            break;

        /* Article, profile, show, and special pages */
        case 'article':
        case 'profile':
        case 'show':
        case 'special':
            /* If we have a page level SSID in the configs.adsection, use it. */
            hasConfigsAdSection = existsString(configs.adsection);
            if (hasConfigsAdSection && configs.adsection.indexOf(constantSsidPrefix) === -1) {
                siteSectionId = configs.adsection;

            } else if (hasConfigsAdSection &&
                configs.adsection.indexOf(constantSsidPrefix) !== -1 &&
                sectionConfigHasOverrideKey(sectionName, configs.adsection)) {

                /*
                 * If configs.adsection begins with "const-" (it's a constant), look up the constant value
                 * in the section config to get the SSID.
                 */
                siteSectionId = sectionConfig[sectionName].adSectionOverrideKeys[configs.adsection];

            } else if (existsString(videoJSON.adSection) && videoJSON.adSection.indexOf(constantSsidPrefix) === -1) {
                /* If we have a SSID in the video JSON, use it. */
                siteSectionId = videoJSON.adSection;

            } else if (existsString(videoJSON.sectionName) &&
                hasConfigsAdSection &&
                configs.adsection.indexOf(constantSsidPrefix) !== -1 &&
                sectionConfigHasOverrideKey(videoJSON.sectionName, configs.adsection)) {

                /*
                 * If we have the video section name, and the page level value in the configs.adsection begins
                 * with "const-" (it's a constant), look up the constant value in the section config to get the SSID.
                 */
                siteSectionId = sectionConfig[videoJSON.sectionName].adSectionOverrideKeys[configs.adsection];

            } else if (hasConfigsAdSection &&
                configs.adsection.indexOf(constantSsidPrefix) !== -1 &&
                sectionConfigHasOverrideKey('default', configs.adsection)) {

                /*
                 * If we have the video section name, and the page level value in the configs.adsection begins
                 * with "const-" (it's a constant), look up the constant value in the section config to get the SSID.
                 */
                siteSectionId = sectionConfig['default'].adSectionOverrideKeys[configs.adsection];

            } else {
                siteSectionId = defaultAdSection;
            }
            break;

        default:
            siteSectionId = defaultAdSection;
            break;
        }

        return siteSectionId;
    };
})();

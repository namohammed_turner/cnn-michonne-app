/* global CNN, _jsmd, window, FAVE */

/**
 * Omniture Analytics tracking
 *
 * @name Omniture
 * @namespace
 * @memberOf FAVE.components
 *
 * @return {Object}
 * an object representing the video player event model
 */

(function (ns, win) {
    'use strict';

    ns.components = ns.components || {};
    ns.components.Omniture = function Omniture(ctx) {
        this.settings = ns.settings.jsmd;
        this.autostart = ctx.configs.autostart;
        this.isLive = ctx.configs.isLive || false;
        this.hasCb = (ns.Utils.existsObject(ctx.jsmd) && Object.keys(ctx.jsmd).length > 0) ? true : false;
        this.munger = Object.create(null);
        this.omniture = Object.create(null);
        this.omniture.content = ctx.videoJSON || Object.create(null);
        this.omniture.content.video_player = this.settings.videoPlayer;
        this.jsmd = (this.hasCb) ? ctx.jsmd : Object.create(null);

        if (ns.Utils.existsObject(ctx.videoJSON)) {
            if (ctx.videoJSON.trt) {
                ctx.videoJSON.trt = String(ctx.videoJSON.trt);
            }

            if (ns.Utils.exists(ctx.videoJSON.urlRelateds)) {
                this.munger.urls = Object.create(null);
                this.munger.urls.relateds = (ctx.videoJSON.urlRelateds.length > 0) ? ctx.videoJSON.urlRelateds : '';
                ctx.videoJSON.urls = this.munger.urls;
                this.omniture.content = ctx.videoJSON;
            }

            if (ns.Utils.exists(ctx.videoJSON.videoIdTimeStamp)) {
                this.munger.timestamp = ctx.videoJSON.videoIdTimeStamp;
                ctx.videoJSON.timestamp = this.munger.timestamp;
                this.omniture.content = ctx.videoJSON;
            }

            if (ctx.videoJSON.dateCreatedMilliSeconds && ctx.videoJSON.dateCreated) {
                this.munger.dateCreated = Object.create(null);
                this.munger.dateCreated.text = ctx.videoJSON.dateCreated;
                this.munger.dateCreated.uts = String(ctx.videoJSON.dateCreatedMilliSeconds);
                ctx.videoJSON.dateCreated = this.munger.dateCreated;
                this.omniture.content = ctx.videoJSON;
            }

            if (typeof ctx.videoJSON.metas === 'object' && typeof ctx.videoJSON.metas.branding === 'string') {
                this.munger.show_name = ctx.videoJSON.metas.branding;
                ctx.videoJSON.show_name = this.munger.show_name;
                this.omniture.content = ctx.videoJSON;
            }
        }

        if (typeof ctx.configs.videoCollection === 'string') {
            this.video_collection = ctx.configs.videoCollection;
        } else {
            this.video_collection = '';
        }

        this.omniture.content.video_collection = this.video_collection;

        delete this.munger;
    };

    ns.components.Omniture.prototype.applyCustom = function (event) {
        var fn = (this.hasCb && this.jsmd[event]) ? this.jsmd[event] : null;
        return (typeof fn === 'function') ? fn.apply(this, arguments) : null;
    };

    ns.components.Omniture.prototype.visibilityHelper = function () {
        if (typeof FAVE.pageVis === 'object' &&
            typeof FAVE.pageVis.isDocumentVisible === 'function') {
            this.omniture.content.video_focus = FAVE.pageVis.isDocumentVisible() ? 'in_focus' : 'out_of_focus';
        }
    };

    ns.components.Omniture.prototype.onPlayerReady = function (_containerId, _playerId, _contentId) {
        this.applyCustom('onPlayerReady');
    };

    ns.components.Omniture.prototype.onContentEntryLoad = function (_containerId, _playerId, _contentId) {
        this.applyCustom('onContentEntryLoad');
    };

    ns.components.Omniture.prototype.onContentMetadata = function (_containerId, _playerId, _contentId, _duration, _wth, _ht) {
        this.applyCustom('onContentMetadata');
    };

    ns.components.Omniture.prototype.onContentBegin = function (_containerId, _playerId, _contentId) {
        if (!win.jsmd && _jsmd) {
            win.jsmd = _jsmd.init();
        }

        this.omniture.Meta = Object.create(new win._jsmd.plugin.gCNNVideoCollection());
        this.applyCustom('onContentBegin');
    };

    ns.components.Omniture.prototype.onContentPlay = function (_containerId, playerId, _contentId) {
        var evt;

        if (this.isLive) {
            this.omniture.content.player_type = 'live';
            this.omniture.content.isAutoStart = typeof this.autostart === 'boolean' ? this.autostart : false;
            window.live_interval = 60;
            evt = 'cnnvideo-live';
        } else {
            evt = (typeof this.autostart === 'boolean' && this.autostart) ? 'cnnvideo-autostart' : 'cnnvideo-start';
        }
        this.visibilityHelper();
        win.trackVideoEvent(this.omniture.content, evt, playerId);
        this.applyCustom('onContentPlay');
    };

    ns.components.Omniture.prototype.onContentComplete = function (_containerId, _playerId, _contentId) {
        this.applyCustom('onContentComplete');
    };

    ns.components.Omniture.prototype.onContentEnd = function (_containerId, playerId, _contentId) {
        this.visibilityHelper();
        win.trackVideoEvent(this.omniture.content, 'cnnvideo-complete', playerId);
        this.applyCustom('onContentEnd');
    };

    ns.components.Omniture.prototype.onContentPlayhead = function (_containerId, playerId, _contentId, playhead, duration) {
        if (typeof this.omniture.Meta !== 'undefined' &&
            typeof playhead !== 'undefined' &&
            !this.omniture.Meta.get(playerId, 'hasScrubbed')) {
            /* Send video progress reports for non-live videos only  */
            if (!this.isLive) {
                if (playhead > (duration * 0.75) && !this.omniture.Meta.get(playerId, 'isSeventyFive')) {
                    this.visibilityHelper();
                    win.trackVideoEvent(this.omniture.content, 'cnnvideo-seventyfive', playerId);
                } else if (playhead > (duration * 0.5) && !this.omniture.Meta.get(playerId, 'isHalf')) {
                    this.visibilityHelper();
                    win.trackVideoEvent(this.omniture.content, 'cnnvideo-fifty', playerId);
                } else if (playhead > (duration * 0.25) && !this.omniture.Meta.get(playerId, 'isTwentyFive')) {
                    this.visibilityHelper();
                    win.trackVideoEvent(this.omniture.content, 'cnnvideo-twentyfive', playerId);
                } else if (playhead > (duration * 0.1) && !this.omniture.Meta.get(playerId, 'isTen')) {
                    this.visibilityHelper();
                    win.trackVideoEvent(this.omniture.content, 'cnnvideo-ten', playerId);
                }
            }
        }

        this.applyCustom('onContentPlayhead');
    };

    ns.components.Omniture.prototype.onAdPlay = function (_containerId, playerId, _token, _mode, _id, duration, _blockId, adType) {
        try {
            this.omniture.content.ad_duration = duration;
            this.omniture.content.adType = adType;
            this.visibilityHelper();
            win.trackVideoEvent(this.omniture.content, 'cnnvideo-preroll', playerId);
        } catch (e) {
            console.error('Omniture onAdPlay error:', e);
        }

        this.applyCustom('onAdPlay');
    };

    ns.components.Omniture.prototype.onAdEnd = function (_containerId, playerId, _token, _mode, _id, _blockId, adType) {
        try {
            this.omniture.content.adType = adType;
            this.visibilityHelper();
            win.trackVideoEvent(this.omniture.content, 'cnnvideo-adcomplete', playerId);
        } catch (e) {
            console.error('Omniture onAdEnd error:', e);
        }

        this.applyCustom('onAdEnd');
    };

    ns.components.Omniture.prototype.onContentPause = function (_containerId, playerId, _contentId, paused) {
        try {
            this.omniture.content.paused = paused;
            this.visibilityHelper();
            win.trackVideoEvent(this.omniture.content, 'cnnvideo-pause', playerId);
        } catch (e) {
            console.error('Omniture onContentPause error:', e);
        }

        this.applyCustom('onContentPause');
    };

    ns.components.Omniture.prototype.onTrackingContentSeek = function (_containerId, playerId, _dataObj) {
        this.visibilityHelper();
        win.trackVideoEvent(this.omniture.content, 'cnnvideo-scrub', playerId);

        this.applyCustom('onTrackingContentSeek');
    };

    ns.components.Omniture.prototype.onContentBuffering = function (_containerId, playerId, _contentId, buffering) {
        try {
            this.omniture.content.buffering = buffering;
            this.visibilityHelper();
            win.trackVideoEvent(this.omniture.content, 'cnnvideo-buffer', playerId);
        } catch (e) {
            console.error('Omniture onContentBuffering error:', e);
        }

        this.applyCustom('onContentBuffering');
    };

    ns.components.Omniture.prototype.onTrackingContentProgress = function (_containerId, _playerId, _dataObj) {
        this.visibilityHelper();
        /* win.trackVideoEvent(this.omniture.content, 'cnnvideo-progress', _playerId); */

        this.applyCustom('onTrackingContentProgress');
    };

    /* Add the JSMD/Omniture component to the FAVE settings */
    if (CNN.Features.enableOmniture) {
        ns.settings.omniture = ns.settings.omniture || {};
        ns.settings.omniture.component = ns.components.Omniture;
    }
}(FAVE, window));

/* global CNN */

/**
 * Javascript to create new document fragmetns for Watch CNN.
 *
 * @name CNN.VideoDocFragment
 * @namespace
 * @memberOf CNN
 */
CNN.VideoDocFragment = CNN.VideoDocFragment || {};
CNN.VideoDocFragment.fragments = CNN.VideoDocFragment.fragments || {};

/**
 * Constructor for creating a Player. This will create the document-fragment skeleton and add the classes.
 *
 * Javascript Module Pattern
 * @see http://www.adequatelygood.com/JavaScript-Module-Pattern-In-Depth.html
 *
 * @example
 * <!-- Renders the following: -->
 * <div class="cd__kicker">CNN TV</div>
 * <h3 class="cd__headline"><span class="cd__headline-text">CNN Newsroom</span></h3>
 * <div class="el__tve-player__timestamp sprite-clock-icon">5pm ET / 2pm PT</div>
 *
 * @name CNN.VideoDocFragment.fragments.Player
 * @namespace
 * @memberOf  CNN
 */
CNN.VideoDocFragment.fragments.Player = CNN.VideoDocFragment.fragments.Player || (function playerFragment() {
    'use strict';

    /* Create a closure. Variables in here will be unique the instance. */
    return function playerFragment() {
        var pub = {};

        pub.elms = {};
        pub.frag = null;

        /**
         * Create the base skeleton of the document fragment.
         *
         * @returns {object}   - the document fragment skeleton.
         *
         * @memberOf CNN.VideoDocFragment.fragments.Player
         */
        function createCard() {
            var kicker = {
                    tag: document.createElement('div'),
                    link: document.createElement('a')
                },
                headline = {
                    tag: document.createElement('h3'),
                    span: document.createElement('span')
                },
                timestamp = document.createElement('div');

            /* Add the class names */
            kicker.tag.className        = 'cd__kicker';
            headline.span.className     = 'cd__headline-text';
            headline.tag.className      = 'cd__headline';
            timestamp.className         = 'el__tve-player__timestamp sprite-clock-icon';

            /* Nest the fragments */
            headline.tag.appendChild(headline.span);

            return {
                kicker: kicker,
                headline: headline,
                timestamp: timestamp
            };
        }

        /**
         * Add server data to the document fragment and finish bulding the final fragment.
         *
         * @param   {object} card  - The card to work with.
         *
         * @memberOf CNN.VideoDocFragment.fragments.Player
         */
        pub.buildDocFragment = function (card) {
            var data,
                elms;

            if (typeof card === 'undefined') {
                throw 'card is not defined';
            } else if (card.data.raw === null) {
                throw 'card.data.raw is null';
            }

            data = card.data.raw;
            elms = card.docFragment.elms;

            /* Populate the Elements */
            elms = createCard();

            elms.headline.span.innerHTML = data.program;

            /* Set anchor link for kicker text only for CNN/HLN. Live 1-4 kicker text will not be linked */
            if (data.id === 'CNN' || data.id === 'HLN') {
                elms.kicker.link.innerHTML = data.kicker || data.id + ' TV';
                elms.kicker.tag.appendChild(elms.kicker.link);
                if (data.id === 'CNN') {
                    elms.kicker.link.href = '/CNN/Programs';
                } else {
                    elms.kicker.link.href = 'http://www.hlntv.com';
                }
            }  else {
                elms.kicker.tag.innerHTML = data.kicker || data.id + ' TV';
            }

            /* Create and assemble the final document fragment. */
            card.docFragment.frag = document.createDocumentFragment();

            card.docFragment.frag.appendChild(elms.kicker.tag);
            card.docFragment.frag.appendChild(elms.headline.tag);

            /* if show time is available, display it. Usually valid for TVE shows only */
            if (typeof data.time !== 'undefined') {
                elms.timestamp.innerHTML = data.time;
                card.docFragment.frag.appendChild(elms.timestamp);
            }

            /* Cache the generated elements */
            card.docFragment.elms = elms;
        };

        return pub;
    };
}());


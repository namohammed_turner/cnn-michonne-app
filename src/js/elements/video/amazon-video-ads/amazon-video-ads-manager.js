/* global CNN, AmazonDirectMatchBuy */

CNN.AdsConfig = CNN.AdsConfig || {};
CNN.AdsConfig.amazon = CNN.AdsConfig.amazon || {};
CNN.AmazonVideoAds = CNN.AmazonVideoAds || {};
CNN.Features = CNN.Features || {};

/**
 * This Singleton object is used to add the Amazon video ads behavior to videos.
 * Amazon video ads will be replace certain FW pre-roll ads using an amazon
 * algorithm that will sent back video ad data once a request threshold is reached.
 *
 */
CNN.AmazonVideoAds.Manager = function () {
    'use strict';

    /**
     * Initialize the amazon ads system
     */
    this.initialize = function () {
        var config = {
            amznkey: CNN.AdsConfig.amazon.amznkey || '3159',
            timeout: (typeof CNN.AdsConfig.amazon.timeout === 'number' ? CNN.AdsConfig.amazon.timeout : 2000)
        };

        if (CNN.Features.enableAmazonVideoAds === true && typeof AmazonDirectMatchBuy === 'object') {
            AmazonDirectMatchBuy.init(config);
        }
    };

    /**
     * The callback that will be used for the onAdSetup
     * CVP event that will set up the amazon key/value
     * pairs
     * @param {string} containerId - The DOM element id for the video player
     * @param {string} _cvpId - The CVP id
     */
    this.cvpAdSetup = function (containerId, _cvpId) {
        if (CNN.Features.enableAmazonVideoAds && typeof AmazonDirectMatchBuy === 'object') {
            AmazonDirectMatchBuy.setAdKeyValuePairs(window.cnnVideoManager.getPlayerByContainer(containerId).videoInstance.cvp);
            AmazonDirectMatchBuy.requestAdRefresh();
        }
    };

    /**
     * Add the necessary callbacks for CVP so that amazon
     * functions get called during the onAdSetup method.
     * @param {object} videoCallbacks - video callbacks object
     */
    this.addAmazonCallback = function (videoCallbacks) {
        if (typeof videoCallbacks !== 'undefined') {
            videoCallbacks.onAdSetup = this.cvpAdSetup;
        }
    };

    this.initialize();
};

/**
 * Retrieve the singleton instance.
 * @returns {object} - Amazon Ad Manager object
 */
CNN.AmazonVideoAds.Manager.getInstance = function () {
    'use strict';

    if (typeof window.amazonAdManager === 'undefined') {
        window.amazonAdManager = new CNN.AmazonVideoAds.Manager();
    }
    return window.amazonAdManager;
};


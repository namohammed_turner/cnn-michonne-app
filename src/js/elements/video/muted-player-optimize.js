/* global CNN */

CNN.VideoConfig = CNN.VideoConfig || {};
CNN.VideoConfig.mutedPlayer = CNN.VideoConfig.mutedPlayer || {};

CNN.mutedPlayer = window.CNN.mutedPlayer || (function mutedPlayer() {
    'use strict';

    var configObj = {},
        callbackObj = {},
        mutedPlayerCounter,
        mutedPlayerInit = false,
        mutedPlayerStopped = false,
        domId = '',
        pub = {};

    configObj = {
        video: 'cvptve_prev/muted_cnn_300x169_hprr',
        width: '100%',
        height: '100%',
        section: CNN.Edition,
        profile: 'expansion',
        context: 'tve_prev',
        network: CNN.VideoConfig.network || 'cnn',
        markupId: null,
        autostart: true,
        adsection: '',
        frameWidth: '300px',
        frameHeight: '169px'
    };

    function createDesktopCallbackObject() {
        callbackObj = {
            onContentPlay: function (_containerId, _cvpId, _event) {
                /* Need to determine state for Safari power saving mode */
                mutedPlayerInit = true;
            },
            onCVPVisibilityChange: function (containerId, playerId, visible) {
                var mutedPlayerInstance = window.cnnVideoManager.getPlayerByContainer(containerId),
                    $countDownSpinner = CNN.mutedPlayer.getMutedPlayerSlates('muted-player__countdown-spinner', containerId),
                    $stopSlate = CNN.mutedPlayer.getMutedPlayerSlates('muted-player__stop-slate', containerId),
                    $preSlate = CNN.mutedPlayer.getMutedPlayerSlates('muted-player__pre-slate', containerId);

                if (mutedPlayerInit && visible) {
                    /* stop counter if running */
                    if (typeof mutedPlayerCounter !== 'undefined' && mutedPlayerCounter !== null) {
                        mutedPlayerCounter.stop();
                    }

                    /* hide spinner if visible */
                    if ($countDownSpinner.is(':visible')) {
                        $countDownSpinner.hide();
                    }

                    /* hide stop-slate if visible */
                    if ($stopSlate.is(':visible')) {
                        $stopSlate.hide();
                    }

                    /* if muted player is not started, let's start it */
                    if (mutedPlayerStopped) {
                        /* show pre-slate */
                        $preSlate.show();

                        /* Hide pre-slate after showing it for 2.5 seconds to
                           give enough startup time for stream.
                           This is just an estimate and if we run into any
                           problems with that estimate being too high or too
                           low, or as a future improvement, we may be able to
                           listen for some event from the player to let us know
                           when it has really started playing. */
                        setTimeout(function () {
                            $preSlate.hide();
                        }, 2500);

                        /* play stream */
                        mutedPlayerInstance.replay();
                        mutedPlayerStopped = false;
                    }
                } else if (mutedPlayerInit) {
                    /* Show countdown spinner when muted player is still running
                       but is out of focus or out of viewport */
                    if (!mutedPlayerStopped) {
                        $countDownSpinner.show();
                    }

                    /* If a previous counter has been initialized, stop it to
                       prevent double countdown */
                    if (typeof mutedPlayerCounter !== 'undefined' && mutedPlayerCounter !== null) {
                        mutedPlayerCounter.stop();
                    }

                    /* Check if player visibility is false immediately after
                       visibilty was true. We check this by checking if the
                       pre-slate image is visible. */
                    if ($preSlate.is(':visible')) {
                        /* hide countdown spinner if visible */
                        if ($countDownSpinner.is(':visible')) {
                            $countDownSpinner.hide();
                        }

                        /* show stop-slate when stream stops */
                        $stopSlate.show();

                        /* stop stream */
                        mutedPlayerInstance.stop();
                        mutedPlayerStopped = true;
                    } else {
                        /* defines countdown object for muted player */
                        mutedPlayerCounter = new CNN.mutedPlayer.countdown({
                            /* Number of seconds to countdown before stopping
                               the player */
                            seconds: CNN.VideoConfig.mutedPlayer.stopInterval || 30,

                            /* callback function which gets called when the
                               counter ends */
                            onCounterEnd: function () {
                                /* hide countdown spinner if visible */
                                if ($countDownSpinner.is(':visible')) {
                                    $countDownSpinner.hide();
                                }

                                /* show stop-slate when stream stops */
                                $stopSlate.show();

                                /* stop stream */
                                mutedPlayerInstance.stop();
                                mutedPlayerStopped = true;
                            }
                        });

                        /* starts muted player countdown */
                        mutedPlayerCounter.start();
                    }
                }
            }
        };
    }

    /**
     * Initializes and optimizes muted player.
     *
     * @param   {string} markupId  -  Target video container passed from dust
     * @param   {string} adSection  - adSection passed from dust
     */
    pub.init = function (markupId, adSection) {
        if (typeof window.CNN.VideoPlayer === 'undefined') {
            throw 'Cannot initialize Muted Player, CNN.VideoPlayer is undefiend';
        }

        /* Set domId/configObj parameters */
        domId = document.getElementById(markupId);
        configObj.markupId = markupId;
        configObj.adsection = adSection || '';

        /* Set class on parent to designate the card as a vertical card */
        jQuery(domId).parents('.cd').addClass('cd--vertical');

        /* Conditionally show content depending on mobile/non-flash checks */
        if (window.CNN.VideoPlayer.isFlashInstalled()) {
            createDesktopCallbackObject();

            if (CNN.VideoPlayer.apiInitialized) {
                window.cnnVideoManager.renderMultipleContainers({
                    videoConfig: configObj,
                    eventsCallback: callbackObj
                });
            } else {
                CNN.VideoPlayer.addVideo(configObj, callbackObj);
            }
        }

        if (window.CNN.VideoPlayer.isMobileClient()) {
            jQuery('.cd--tool__muted-player .is-not-mobile .js-media__video').hide();
        }
    };

    /**
     * Returns jQuery object for slates and spinner for muted player.
     * If the DOM element does not exist, this function will add it and returns
     * back the added jQuery object.
     *
     * @param {string} className - Target CSS class name
     * @param {string} container - Target video container
     *
     * @returns {jQuery} - jQuery DOM element object for slate or spinner
     */
    pub.getMutedPlayerSlates = function (className, container) {
        var $playerContainer = jQuery(document.getElementById(container)),
            $selector = $playerContainer.find('.' + className);

        if ($selector.length === 0) {
            $playerContainer.prepend('<div class="' + className + '"/>');
        }

        return $selector;
    };

    /**
     * Countdown function for use in optimizing muted player
     *
     * @param   {object} options  - countdown options
     */
    pub.countdown = function (options) {
        var timer,
            self = this,
            seconds = options.seconds,
            counterEnd = options.onCounterEnd;

        function _decrementCounter() {
            if (seconds <= 0) { /* to protect against counter running forever */
                counterEnd();
                self.stop();
            }
            seconds -= 1;
        }

        self.start = function () {
            clearInterval(timer);
            timer = 0;
            seconds = options.seconds;
            timer = setInterval(_decrementCounter, 1000);
        };

        self.stop = function () {
            clearInterval(timer);
        };
    };

    return pub;
}());

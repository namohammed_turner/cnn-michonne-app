/* global CNN */

/**
 * Javascript for interacting with the various stream_dvr_?.xml files and converting those into a single JSON object.
 * Periodoicly polls the server for new data.
 *
 * Javascript Module Pattern
 * @see http://www.adequatelygood.com/JavaScript-Module-Pattern-In-Depth.html
 *
 * @name CNN.VideoAjax.LiveStream
 * @namespace
 * @memberOf CNN
 *
 * @example
 * // Wait until document is ready.
 * jQuery(function () {
 *     var namespace = 'LiveStream';
 *
 *     // Always make sure that the file exists and is loaded before any referencing files depend on it.
 *     if (typeof window.CNN.VideoAjax === 'undefined' || typeof CNN.VideoAjax[namespace] === undefined) {
 *         throw 'Cannot fetch data from the server. CNN.VideoAjax or the namespace is not defined';
 *     }
 *
 *     // If everything looks good and the file is properly loaded, add an event listener.
 *     document.addEventListener(pub.getEventName(), function handler() {
 *        // Add custom callback code here.
 *     }, false);
 *
 *     // Then, init the function
 *     CNN.VideoAjax[namespace].init();
 * });
 *
 */
CNN.VideoAjax.LiveStream = CNN.VideoAjax.LiveStream || (function LiveStream() {
    'use strict';

    var pub = CNN.VideoAjax({
        id: 'LiveStream',
        eventName: 'CNN_LiveEvent_autoUpdated'
    });

    /**
     * AJAX call to data.cnn.com.
     *
     * @param {function} callback   - Required callback parameter.
     */
    pub.fetchExternalData = function (callback) {
        var serverData = {};

        /* Don't fetch the data if there's already an AJAX request in progress. */
        if (pub.getIsFetching() === true) { return; }

        /* If callback paramerter is defined, make sure it is a function. */
        if (typeof callback === 'undefined' || typeof callback !== 'function') {
            throw 'callback parameter is not defined or is not a function. Ajax request was not sent.';
        }

        /**
         * Reusable function for multiple AJAX requests for the same data at different URLs.
         *
         * @param   {integer} id  - The numeric ID for the `stream_dvr`.
         *
         * @returns {jQuery.fn}   - The AJAX request.
         *
         * @memberOf CNN.VideoAjax
         * @private
         */
        function callServers(id) {
            /* Fire AJAX request. */
            return jQuery.ajax({
                url: '/CNNLiveFlash/StreamStatus/metadata/stream_dvr_' + id + '.xml',
                type: 'get',
                error: function (data, status) {
                    pub.logError(data, status);
                },
                success: function (data) {
                    serverData['live' + id] = {
                        xml: data,
                        thumbnail: undefined,
                        isStreaming: false
                    };
                },
                timeout: function (data, status) {
                    pub.logError(data, status);
                },
                dataType: 'xml',
                crossDomain: false
            });
        }

        /**
         * Parse multiple XML documents and create a single JSON object. The object must be formatted to work with the video fragments their being sent to.
         *
         * @returns {object}  - Object containing a single array to mimmick the NowPlayingSchedule.json file.
         *
         * @memberOf CNN.VideoAjax
         * @private
         */
        function createDataObject() {
            var key,
                channel = [],
                currentObj = {};

            for (key in serverData) {
                /* If the stream is actually streaming, add it as a new channel. */
                if (key in serverData && (typeof serverData[key].isStreaming !== 'undefined' && serverData[key].isStreaming !== false)) {
                    /* Set the current context */
                    currentObj = jQuery(serverData[key].xml).find('stream').first();

                    /* Create the new object. */
                    channel.push({
                        id: serverData[key].id, /* Purposefully use the stored ID value instead of the index, incase something doesn't jive. */
                        url: currentObj.find('affiliate url').text(),
                        kicker: currentObj.find('affiliate headline').text(),
                        program: currentObj.find('title').text(),
                        images: {
                            '120x68': serverData[key].thumbnail
                        },
                        description: currentObj.find('description').text(),
                        streamIndex: serverData[key].index
                    });
                }
            }

            /* Make sure that the streams show up in numerical order. */
            channel.sort(function sortStreams(a, b) {
                return parseInt(a.streamIndex, 10) - parseInt(b.streamIndex, 10);
            });

            return {channel: channel};
        }

        /* Set isFetching to prevent multiple duplicate AJAX requests. */
        pub.setIsFetching(true);

        /* Create a deffered promise */
        jQuery.when(callServers(1), callServers(2), callServers(3), callServers(4)).done(function deferredCallback() {
            /* Call pipeline.xml to get a list of thumbnails and live streams */
            jQuery.ajax({
                url: '/.element/ssi/www/auto/2.0/video/xml/pipeline.xml',
                type: 'get',
                error: function (data, status) {
                    pub.logError(data, status);
                },
                success: function (data) {
                    var i = 0,
                        videoId = '',
                        $video = jQuery(data).find('video');

                    /* Loop through the returned videos and save their thumbnail url.*/
                    for (i = $video.length - 1; i >= 0; i--) {
                        videoId = parseInt($video.eq(i).find('video_url').text().substring(7), 10);

                        if (!isNaN(videoId)) {
                            serverData['live' + videoId].id = 'live-' + videoId;
                            serverData['live' + videoId].thumbnail = $video.eq(i).find('image_url').text();
                            serverData['live' + videoId].isStreaming = true;
                            serverData['live' + videoId].streamIndex = videoId;
                        }
                    }

                    /* Reset isFetching to allow future requests. */
                    pub.setIsFetching(false);

                    /* Run the custom callback. */
                    callback(createDataObject());
                },
                timeout: function (data, status) {
                    pub.logError(data, status);
                },
                dataType: 'xml',
                crossDomain: false
            });
        });
    };

    return pub;
}());


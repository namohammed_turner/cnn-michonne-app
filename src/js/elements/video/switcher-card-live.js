/* global CNN */

/**
 * Javascript to create new document fragmetns for Watch CNN.
 *
 * @name CNN.VideoDocFragment
 * @namespace
 * @memberOf CNN
 */
CNN.VideoDocFragment = CNN.VideoDocFragment || {};

/**
 * Functionality unique to Authenticated Player Toggles.
 *
 * Javascript Module Pattern
 * @see http://www.adequatelygood.com/JavaScript-Module-Pattern-In-Depth.html
 *
 * @name CNN.VideoDocFragment.livePlayerToggle
 * @namespace
 * @memberOf  CNN
 *
 * @requires CNN.VideoAjax.LiveStream
 */
CNN.VideoDocFragment.livePlayerToggle = CNN.VideoDocFragment.livePlayerToggle || (function livePlayerToggle() {
    'use strict';

    var pub = CNN.VideoDocFragment.BaseClass();

    /* Wait until the document is ready */
    jQuery(function livePlayerToggleDocReady() {
        pub.setEventName(CNN.VideoAjax.LiveStream.getEventName());
    });

    /**
     * Fetch the data from the various stream_dvr_?.xml files.
     * Since the returned data will contain the values for multiple streams that
     * should be represented as individual cards, a `childCards` memeber is
     * added to the CMS card object.
     *
     * @param   {object} card  - The individual CMS card to work with.
     *
     * @memberOf CNN.VideoDocFragment.livePlayerToggle
     */
    pub.fetchData = function (card) {
        var i = 0,
            cardStr = '',
            childCard = {};

        card.data.raw = CNN.VideoAjax.LiveStream.getServerData();
        cardStr = JSON.stringify(card.data.raw);

        /* Only update the DOM if the data has changed */
        if (cardStr !== card.data.raw && card.data.raw !== null) {
            card.childCards = [];
            card.data.string = cardStr;

            /* Create a child card for each json-formatted xml data object. */
            for (i = card.data.raw.channel.length - 1; i >= 0; i--) {
                childCard = new CNN.VideoDocFragment.CardConstructor('livePlayerToggle', card.data.raw.channel[i].id, '');
                childCard.data.raw = card.data.raw.channel[i];
                childCard.docFragment = CNN.VideoDocFragment.fragments.SwitcherCard();

                childCard.docFragment.buildDocFragment(childCard);

                /* Add cards to the public space for debugging. */
                card.childCards.push(childCard);
            }

            /* Send the entire CMS card be updated */
            pub.updateDom(card);
        }
    };

    /**
     * Function to repaint the DOM.
     * Since multiple faux-cards were created outside of CMS, layout-specific elements will also need to be created and wrapped around the card.
     *
     * @param   {object} card  - The CMS card to update.
     *
     * @memberOf CNN.VideoDocFragment.livePlayerToggle
     */
    pub.updateDom = function (card) {
        var i = 0,
            articleWrapper, divWrapper,
            items = card.childCards,
            $gallery = jQuery('.cd--tool__live-switcher').parents('.js-owl-carousel').data('owlCarousel');

        if (typeof card === 'undefined') {
            throw 'card is not defined.';
        }

        /* Lets remove the first item from the carousel because we will be dynamically populating it */
        $gallery.removeItem(0);

        /* Dynamically create card objects for each stream */
        for (i = items.length - 1; i >= 0; i--) {
            articleWrapper = document.createElement('article');
            articleWrapper.className = 'cd cd--tool cd--tool__live-switcher';
            articleWrapper.appendChild(items[i].docFragment.frag);

            divWrapper = document.createElement('div');
            divWrapper.className = 'carousel__content__item';
            divWrapper.appendChild(articleWrapper);

            $gallery.addItem(divWrapper);

        }

    };

    return pub;
}());

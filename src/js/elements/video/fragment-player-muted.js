/* global CNN */

/**
 * Javascript to create new document fragmetns for Watch CNN.
 *
 * @name CNN.VideoDocFragment
 * @namespace
 * @memberOf CNN
 */
CNN.VideoDocFragment = CNN.VideoDocFragment || {};
CNN.VideoDocFragment.fragments = CNN.VideoDocFragment.fragments || {};

/**
 * Constructor for creating a MutedPlayer. This will create the document-fragment skeleton and add the classes.
 *
 * Javascript Module Pattern
 * @see http://www.adequatelygood.com/JavaScript-Module-Pattern-In-Depth.html
 *
 * @name CNN.VideoDocFragment.fragments.MutedPlayer
 * @namespace
 * @memberOf CNN
 */
CNN.VideoDocFragment.fragments.MutedPlayer = CNN.VideoDocFragment.fragments.MutedPlayer || (function MutedPlayer() {
    'use strict';

    return function MutedPlayer() {
        var pub = {};

        pub.elms = {};
        pub.frag = null;

        /**
         * Create the base skeleton of the document fragment.
         *
         * @param {object} data - the page data object
         * @returns {object} - the document fragment skeleton.
         *
         * @memberOf CNN.VideoDocFragment.fragments.MutedPlayer
         */
        function createCard(data) {
            var image = {
                    tag: document.createElement('img'),
                    imageLink: document.createElement('a')
                },
                content = {
                    tag: document.createElement('div'),
                    headline: {
                        tag: document.createElement('h3'),
                        link: document.createElement('a'),
                        span: document.createElement('span')
                    },
                    timestamp: document.createElement('div')
                },
                watchCnnUrl = (CNN.CNNXStreamUrl && !window.CNN.VideoPlayer.isMobileClient()) ?
                    CNN.CNNXStreamUrl + data.id : '/tv/watch/cnn/';  /* Determine streamUrl to use based on if mobile or not */

            /* Add the class names */
            content.headline.span.className = 'cd__headline-text';
            content.headline.tag.className  = 'cd__headline';
            content.timestamp.className     = 'cd__auxiliary';
            content.tag.className           = 'cd__content';

            /* Add static text */
            content.headline.link.href      = watchCnnUrl;
            image.imageLink.href            = watchCnnUrl;

            /* Nest the fragments */
            content.headline.tag.appendChild(content.headline.link);
            content.headline.link.appendChild(content.headline.span);
            content.tag.appendChild(content.headline.tag);
            content.tag.appendChild(content.timestamp);
            image.imageLink.appendChild(image.tag);

            return {
                image: image,
                content: content
            };
        }

        /**
         * Add server data to the document fragment and finish bulding the final
         * fragment.
         *
         * @param   {object} card
         * The card to work with.
         *
         * @param   {boolean} [noMutedPlayer=false]
         * Boolean value to determine if the muted player is used, or if the
         * image slate should be displayed instead.
         *
         * @memberOf CNN.VideoDocFragment.fragments.MutedPlayer
         */
        pub.buildDocFragment = function (card, noMutedPlayer) {
            var data = card.data.raw,
                elms = card.docFragment.elms;

            if (typeof noMutedPlayer === 'undefined') {
                noMutedPlayer = false;
            }

            /* Populate the Elements */
            elms = createCard(data);

            elms.image.tag.alt                   = data;
            elms.image.tag.src                   = data.images['120x68'];
            elms.content.headline.span.innerHTML = data.program;
            elms.content.timestamp.innerHTML     = data.time;

            /* Create and assemble the final document fragment. */
            card.docFragment.frag = document.createDocumentFragment();

            if (noMutedPlayer === true) {
                card.docFragment.frag.appendChild(elms.image.imageLink);
            }

            card.docFragment.frag.appendChild(elms.content.tag);

            /* Cache the generated elements */
            card.docFragment.elms = elms;
        };

        return pub;
    };
}());

/**
 * Functionality unique to the Muted Player.
 *
 * Javascript Module Pattern
 * @see http://www.adequatelygood.com/JavaScript-Module-Pattern-In-Depth.html
 *
 * @name CNN.VideoDocFragment.mutedPlayer
 * @namespace
 * @memberOf  CNN
 * @requires  CNN.VideoAjax.NowPlaying
 * @requires  CNN.VideoPlayer
 */
CNN.VideoDocFragment.mutedPlayer = (function mutedPlayerFragment() {
    'use strict';

    var pub = CNN.VideoDocFragment.BaseClass();

    /* Wait until the document is ready */
    jQuery(function mutedPlayerDocReady() {
        pub.setEventName(CNN.VideoAjax.NowPlaying.getEventName());
    });

    /**
     * Fetch the data from the now-playing-schedule.json file.
     *
     * @param   {object} card  - The individual card to work with.
     *
     * @memberOf CNN.VideoDocFragment.authPlayerToggle
     */
    pub.fetchData = function (card) {
        var cardStr = '',
            noMutedPlayer = typeof card.$parent.data('show-player') === 'undefined' ||
                            typeof CNN.VideoPlayer === 'undefined' ||
                            !CNN.VideoPlayer.isFlashInstalled() ||
                            CNN.VideoPlayer.isMobileClient();

        card.data.raw = CNN.VideoAjax.NowPlaying.filterNetwork(card.network);
        cardStr = JSON.stringify(card.data.raw);

        /* Only update the DOM if the data has changed */
        if (cardStr !== card.data.raw) {
            card.data.string = cardStr;

            card.docFragment = CNN.VideoDocFragment.fragments.MutedPlayer();
            card.docFragment.buildDocFragment(card, noMutedPlayer);

            pub.updateDom(card);
        }
    };

    return pub;
}());

/* global CNN, CVP */

CNN.VideoConfig = CNN.VideoConfig || {};
CNN.VideoPlayer = CNN.VideoPlayer || {};

/**
 * Provides functionality for showing the simple copy embed code link.
 * This utilizes flash for clipboard operations
 *
 * @name SimpleEmbedCodeCopy
 * @namespace
 * @memberOf CNN.VideoPlayer
 *
 * @param {string} videoContainerId - The DOM id of the video element
 * @param {boolean} embedDivId - DOM Id of the element where the embed link will
 *                              be displayed.
 * @param {object} parentElement - The DOM element that the embed container
 *                                  will be a child of. Note: This should be a
 *                                  single DOM element, not an array of elements
 */
CNN.VideoPlayer.SimpleEmbedCodeCopy = function (videoContainerId, embedDivId, parentElement) {
    'use strict';

    this.videoContainerId = videoContainerId;
    this.embedDivId = embedDivId;
    this.parentElement = jQuery(parentElement);
};

CNN.VideoPlayer.SimpleEmbedCodeCopy.prototype = {
    /**
     * Displays confirmation text after a successful copy of the embed code into
     * the clipboard.
     */
    onSuccessfulCopy: function onSuccessfulCopy() {
        'use strict';

        if (typeof this.parentElement !== 'undefined') {
            this.parentElement.find('.metadata__embed-video-confirmation').remove();
            this.parentElement.append('<span class="metadata__embed-video-confirmation"><p>Embed Code copied successfully</p></span>');
            setTimeout(jQuery.proxy(function () {
                (jQuery(this.parentElement.find('.metadata__embed-video-confirmation'))).slideUp(500);
            }, this), 3000);
        }
    },

    /**
     * Creates the parent container for the embed link element(s). This will
     * house the div that houses the flash movie, etc.
     *
     */
    generateEmbedContainer: function generateEmbedContainer() {
        'use strict';

        if (typeof this.parentElement !== 'undefined') {
            this.parentElement.find(this.getEmbedContainerId()).remove();
            this.movieDivId = this.getEmbedContainerId() + '--movie';
            this.parentElement.append('<span class="metadata__embed-video-button" id="' +
                this.getEmbedContainerId() + '"><div id="' + this.movieDivId + '"/></span>');
        }

    },

    /**
     * Retrieves the DOM element for the embed code display, if it exists
     *
     * @returns {object} the DOM element for the embed link, if it exists
     */
    getEmbedContainer: function getEmbedContainer() {
        'use strict';

        var id = this.getEmbedContainerId();

        return jQuery('#' + id);
    },

    /**
     * Retrieves the DOM element Id for the embed link.
     *
     * @returns {string} the DOM element Id for the embed link.
     */
    getEmbedContainerId: function getEmbedContainerId() {
        'use strict';

        return this.embedDivId;
    },

    /**
     * Retrieve the DOM id of the element that contains the video player
     *
     * @returns {string} The DOM id for the video container
     */
    getVideoContainerId: function getVideoContainerId() {
        'use strict';

        return this.videoContainerId;
    },

    /**
     * Show the embed link for the given metadata. If the metadata has the
     * property "isEmbeddable" set to "yes" or "true", this function add the flash
     * app used for clipboard and embed operations.
     *
     * @param {object} metaData - The metadata for the video, usually retrieved via
     *                              CVP's onContentMetadata event.
     */
    show: function show(metaData) {
        'use strict';

        var /* successCallback, */
            successCallbackName = 'successCallback' + this.getVideoContainerId(),
            embedSnippet,
            imagePath,
            embedContainer = this.getEmbedContainer();

        /* never used: successCallback = */
        window[successCallbackName] = jQuery.proxy(this.onSuccessfulCopy, this);
        if (typeof metaData === 'object' && typeof metaData.isEmbeddable !== 'undefined' &&
            metaData.isEmbeddable !== 'false' && metaData.isEmbeddable !== 'no' &&
            embedContainer && typeof CVP !== 'undefined' &&
            typeof CNN.VideoConfig.embedLinks !== 'undefined' &&
            typeof CNN.VideoConfig.embedLinks.clipboardOperationsSWF !== 'undefined') {

            embedSnippet = CNN.VideoConfig.embedLinks.embedLinkPattern;
            embedSnippet = embedSnippet.replace('{host}', window.location.hostname).replace('{videoId}', metaData.id);
            imagePath = CNN.VideoConfig.embedLinks.buttonImage.replace('{cdnassetpath}', CNN.Host.assetPath);
            if (!Array.isArray(embedContainer) || embedContainer.length <= 0) {
                this.generateEmbedContainer();
            }
            CVP.swfobject.embedSWF(CNN.VideoConfig.embedLinks.clipboardOperationsSWF,
                this.movieDivId, '80px', '21px', '10.0.0',
                CNN.VideoConfig.embedLinks.clipboardOperationsSWF + '?nocache=' + ((new Date()).valueOf()), {
                    callback: 'window["' + successCallbackName + '"]',
                    buttonImage: imagePath,
                    copyValue: embedSnippet,
                    xOffset: CNN.VideoConfig.embedLinks.imageButtonXOffset,
                    yOffset: CNN.VideoConfig.embedLinks.imageButtonYOffset
                }, {
                    allowScriptAccess: 'always',
                    quality: 'high',
                    wmode: 'transparent'
                }, {});
        } else {
            this.hide();
        }
    },

    /**
     * Hide and removes the DOM element for the embed link.
     */
    hide: function hide() {
        'use strict';

        if (this.getEmbedContainer()) {
            this.getEmbedContainer().remove();
        }
    }
};


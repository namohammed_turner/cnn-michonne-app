/* global CNN */

CNN.INJECTOR.executeFeature('videoLoader', {params:'?version=latest&client=expansion'}).then(function () {
    'use strict';

    /**
     * Javascript to create new document fragmetns for Watch CNN.
     *
     * @name CNN.VideoDocFragment
     * @namespace
     * @memberOf CNN
     */
    CNN.VideoDocFragment = CNN.VideoDocFragment || {};

    /**
     * Functionality unique to Authenticated Player Toggles.
     *
     * Javascript Module Pattern
     * @see http://www.adequatelygood.com/JavaScript-Module-Pattern-In-Depth.html
     *
     * @name CNN.VideoDocFragment.authPlayerToggle
     * @namespace
     * @memberOf  CNN
     *
     * @requires CNN.VideoAjax.NowPlaying
     */
    CNN.VideoDocFragment.authPlayerToggle = CNN.VideoDocFragment.authPlayerToggle || (function authPlayerToggle() {

        var pub = CNN.VideoDocFragment.BaseClass();

        /* Wait until the document is ready */
        jQuery(function authPlayerToggleDocReady() {
            pub.setEventName(CNN.VideoAjax.NowPlaying.getEventName());
        });

        /**
         * Fetch the data from the now-playing-schedule.json file.
         *
         * @param   {object} card  - The individual CMS card to work with.
         *
         * @memberOf CNN.VideoDocFragment.authPlayerToggle
         */
        pub.fetchData = function (card) {
            var cardStr = '';

            card.data.raw = CNN.VideoAjax.NowPlaying.filterNetwork(card.network);

            if (typeof card.data.raw === 'undefined' || card.data.raw === null) {
                throw ('No data was returned');
            }

            cardStr = JSON.stringify(card.data.raw);

            /* Only update the DOM if the data has changed */
            if (cardStr !== card.data.raw && card.data.raw !== null) {
                card.data.string = cardStr;

                card.docFragment = CNN.VideoDocFragment.fragments.SwitcherCard();
                card.docFragment.buildDocFragment(card);

                pub.updateDom(card);
            }
        };

        return pub;
    }());
});

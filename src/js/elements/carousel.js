/* global CNN, Modernizr */

CNN.DemandLoadConfig = CNN.DemandLoadConfig || {};

CNN.Gallery = CNN.Gallery || {};

CNN.Carousel = CNN.Carousel || {
    carouselSelector: '.js-owl-carousel',

    /* These are a subset of the events that OWL 2 lets us use */
    events: {
        initialize:             'initialize.owl.carousel',
        initialized:            'initialized.owl.carousel',
        loadedImage:            'loaded.owl.lazy',
        navigationInitialized:  'initialized.owl.navigation',
        destroy:                'destroy.owl.carousel',
        dragged:                'dragged.owl.carousel',
        change:                 'change.owl.carousel',
        changed:                'changed.owl.carousel',
        resize:                 'resize.owl.carousel',
        resized:                'resized.owl.carousel',
        refresh:                'refresh.owl.carousel',
        refreshed:              'refreshed.owl.carousel',
        to:                     'to.owl.carousel',
        next:                   'next.owl.carousel',
        prev:                   'prev.owl.carousel',
        drag:                   'drag.owl.carousel'
    },

    /**
     * Function to trigger refresh event on all OWL carousels.
     */
    refreshCarousels: function refresh() {
        'use strict';

        jQuery(CNN.Carousel.carouselSelector).trigger(CNN.Carousel.events.refresh);
    },

    /**
     * Constructor function to create a new instance of a carousel or gallery
     *
     * @param {object} elem - DOM Element used for the carousel or gallery
     * @returns {object} - returns newOwl object after initializing it
     */
    owl: function newOwl(elem) {
        'use strict';

        var self = this,
            galleryArrowHeight = 30;

        /* Set some object-root-level vars */
        this.$carousel = jQuery(elem);
        this.$filmstrip = this.$carousel.next('.js-owl-filmstrip');
        this.isPaginatedCarousel = this.$carousel.hasClass('carousel-small-paginated');
        this.isGallery = this.$filmstrip.length > 0 || this.isPaginatedCarousel;
        this.isAutoHeight = (this.isGallery || this.$carousel.is('[data-auto-height="true"]'));
        this.galleryHeightRatio = null;
        this.isFullCarousel = this.$carousel.is('.carousel--full');
        this.isLargeStripCarousel = this.$carousel.is('.carousel-large-strip');
        this.hasSingleItem = this.$carousel.data('slide-count') === 1;

        jQuery.extend(this.$filmstrip, this);
        jQuery.extend(this.$carousel, this);

        /**
         * To adjust the height of the navigation arrows so they are centered on
         * the card image
         *
         * @param {object} [_e] - OWL Event passed as event handler callback parameter
         */
        this.carouselAdjustNavigation = function (_e) {
            var ratio = self.$carousel.data('cut-format') === '4:3' ? (3 / 4) : (self.$carousel.data('cut-format') === '9:16' ? (16 / 9) : (9 / 16)),
                height = (self.$carousel.find('.owl-item').find('.el__resize, .media').width() * ratio),
                galleryCarouselTop = (height / 2) - galleryArrowHeight,
                $owlArrows = self.$carousel.find('.owl-prev, .owl-next');
            if (self.$carousel.attr('data-is-gallery') === 'true') {
                /* To set click only on arrows not outside of it */
                $owlArrows.css('top', galleryCarouselTop + 'px');
            } else {
                $owlArrows.css('height', height + 'px');
            }
            self.$carousel.siblings('.ad-slide').find('.ad-slide__prev, .ad-slide__next').css('top', galleryCarouselTop + 'px');
        };

        this.makeLazy = function ($carousel) {
            $carousel.find('img').each(function () {
                var $img = jQuery(this),
                    eqState = ($img.attr('data-eq-state') || '').replace(/^(\S+\s+)*/, ''),
                    srcAttr;

                if (eqState) {
                    srcAttr = eqState === 'xsmall' ? 'data-src-small' : 'data-src-' + eqState;
                } else {
                    srcAttr = 'src';
                }
                $img.attr('data-src', $img.attr(srcAttr)).addClass('owl-lazy');
            });
        };

        /**
        * Enable next and previous slides on the Large Carousel.
        */
        this.enablePrevNextItems = function () {
            var activeItem = self.$carousel.find('.owl-item.active'),
                nextItem = activeItem.next('.owl-item').find('img'),
                prevItem = activeItem.prev('.owl-item').find('img');

            if(self.isLargeStripCarousel) {
                CNN.ResponsiveImages.process(CNN.DemandLoading.addElement(nextItem));
                CNN.ResponsiveImages.process(CNN.DemandLoading.addElement(prevItem));
            }
        };

        /**
         * Sets the number of items visible per page and the number of items to
         * `slideBy` if the carousel is 1+ items
         *
         * @param {object} e - OWL Event passed as event handler callback parameter
         * @returns {number} - The number of visible items per page
        */
        this.setNumberOfItems = function (e) {
            var owl = e.relatedTarget,
                numItems;

            if (owl._widths[0] === 0) {
                return;
            } else {
                numItems = owl.settings.items > 1 ? owl.$element.width() / owl._widths[0] : owl.settings.items;
            }

            if (numItems > 1) {
                owl.settings.items = Math.ceil(numItems);
                owl.settings.slideBy = Math.floor(numItems);
                owl.$element.trigger(CNN.Carousel.events.refresh);
            }

            return numItems;
        };

        /**
         * Sets the opacity of filmstrip thumbnails visible.
         * @param {object} [_e] - Event object
         */
        this.resetThumbnails = function (_e) {
            self.$filmstrip.find('.owl-item').find('img').css('opacity', 1);
            self.$carousel.find('.owl-item').find('img').css('opacity', 1);
        };

        /**
         * Sets aspect ratio for galleries
         */
        this.setAspectRatio = function () {
            if (self.$carousel.data('gallery-ratio-set')) {
                return;
            }

            /**
             * Return the ratio of the height to width
             * read aspect ratio, parse, then divide
             * default format = 16:9
             * the regex exctracts '16:9' from 'custom16:9'
             *
             * @param {jQuery} $el - jQuery object representing carousel
             *
             * @return {number} ratio
             */
            function getHeightPercent($el) {
                var ratio;

                if (!$el) { return 1; }

                ratio = /\d+:\d+/.exec($el.data('cutFormat')) || [];

                ratio = (ratio[0] || '16:9').split(':');
                return (+ratio[1] / +ratio[0] * 100) || 1;
            }

            self.$carousel.data('gallery-ratio-set', true);

            self.galleryHeightRatio = getHeightPercent(self.$carousel);

            /* update gallery to respect aspect ratio */
            self.$carousel.find('.js-gallery-aspect-ratio-wrapper').css('padding-bottom', self.galleryHeightRatio + '%');

            /* update slides */
            self.$carousel.find('[data-cut-format]').each(function () {
                var $carousel = jQuery(this),
                    imgHeightRatio = getHeightPercent(this.$carousel);

                /* Math to adjust the padding so that aspect ratio is respected */
                if (imgHeightRatio < this.galleryHeightRatio) {
                    $carousel.css('padding', ((this.galleryHeightRatio - imgHeightRatio) / 2) + '% 0');
                } else if (imgHeightRatio > this.galleryHeightRatio) {
                    $carousel.css('padding', '0 ' + ((1 - this.galleryHeightRatio / imgHeightRatio) / 2 * 100) + '%');
                }
            });
        };

        /**
         * Based on eq-state, determines the amount of stage padding for the 1-up carousels.
         * @returns {number} - padding value
         */
        this.stagePadding = function () {
            var paddingValue,
                eqState;

            if (self.isFullCarousel && !self.isGallery) {
                eqState = (jQuery('body').attr('data-eq-state') || '').replace(/^(\S+\s+)*/, '');

                switch (eqState) {
                case 'xsmall':
                    paddingValue = 50;
                    break;
                case 'medium':
                    paddingValue = 100;
                    break;
                case 'large':
                case 'full16x9':
                    paddingValue = 150;
                    break;
                }
            } else {
                paddingValue = 0;
            }

            return paddingValue;
        };

        /**
         * Event Handler for each main carousel resize
         * Calculates stage padding for smaller devices and refreshes
         *
         * @param  {Object} e  - OWL Event passed as event handler callback parameter
         */
        this.onResize = function (e) {
            e.relatedTarget.options.stagePadding = self.stagePadding();
            self.$carousel.trigger(CNN.Carousel.events.refresh);
        };

        /**
         * Event Handler for a change in the main carousel (top in gallery,
         * THE carousel in strip carousels)
         *
         * @param  {Object} e  - OWL Event passed as event handler callback parameter
         */
        this.onMainCarouselChange = function (e) {
            if (e.property.name === 'position' && self.$filmstrip.data()) {
                /* Defer execution so any related functions can finish executing before this one */
                setTimeout(function () {
                    /* Find the index of the currently active item within the set of non-cloned items */
                    var index = e.property.value - e.relatedTarget.clones().length / 2,
                        maxIndex = self.$filmstrip.data('owl.carousel')._items.length - 1,
                        $filmstripItems = self.$filmstrip.find('.owl-item');

                    index = index === -1 ? maxIndex : (index > maxIndex ? 0 : index);

                    $filmstripItems.removeClass('synced');

                    /* If the active slide is part of the gallery */
                    if (self.isGallerySlide()) {
                        $filmstripItems.eq(index).addClass('synced');
                        self.$filmstrip.trigger(CNN.Carousel.events.to, [index]);
                    }
                }, 0);
            }
        };

        /**
        * Reprocess images that are lazily loaded to ensure the proper cut is
        * delivered.
        *
        * @param  {Object} e  - OWL Event passed as event handler callback parameter
        */
        this.onLazyImageLoaded = function (e) {
            CNN.ResponsiveImages.process(CNN.DemandLoading.addElement(e.element));
        };

        /**
         * Reprocess active image when a carousel is refreshed to ensure the
         * proper cut is delivered.
         *
         * @param  {Object} e  - OWL Event passed as event handler callback parameter
         */
        this.onRefreshed = function (e) {
            CNN.ResponsiveImages.queryEqjs(e.target);
        };

        /**
         * Event Handler for after the main carousel is initialized.
         *
         * @param {object} e - OWL Event passed as event handler callback parameter
         */
        this.onInitialized = function (e) {
            self.setNumberOfItems(e);
            self.$carousel.on(CNN.Carousel.events.changed, self.onMainCarouselChange);
            self.$carousel.on(CNN.Carousel.events.resized, self.carouselAdjustNavigation);
            self.$carousel.on(CNN.Carousel.events.resized, self.setNumberOfItems);
            self.$carousel.on(CNN.Carousel.events.resized, self.resetThumbnails);
            self.$carousel.on(CNN.Carousel.events.resized, self.onResize);
            self.$carousel.on(CNN.Carousel.events.refreshed, self.onRefreshed);
            self.$carousel.on(CNN.Carousel.events.refreshed, self.carouselAdjustNavigation);
            self.$carousel.on(CNN.Carousel.events.loadedImage, function (evt) {
                self.onLazyImageLoaded(evt);
                self.onRefreshed(evt);
                self.resetThumbnails(evt);
                self.enablePrevNextItems();
            });
        };

        /**
         * Event Handler to add analytics listeners
         *
         * @param {object} _e - Event object
         */
        this.addAnalyticsListeners = function (_e) {
            self.$carousel.find('.owl-nav > div').on('click', function owlNavItemClicked(_evt) {
                /* trigger analytics call */
                self.$carousel.trigger('carousel-action-event');
            });

            self.$carousel.find('.owl-item').on('click', function owlItemClicked(_evt) {
                /* trigger analytics call */
                self.$carousel.trigger('carousel-item-clicked');
            });
        };

        /**
         * Determines if the currently active carousel slide is part of the gallery
         * collection being displayed or if its an additional slide used for other
         * purposes, such as advertisement (e.g. Outbrain).
         *
         * @return {Boolean} - If active gallery slide is part of gallery or not
         */
        this.isGallerySlide = function () {
            return !!self.$carousel.find('.owl-item.active > div[data-slidename]').length;
        };

        this.makeLazy(this.$carousel);

        /* Set the aspect ratio of the carousel */
        this.setAspectRatio();

        /* Set the main carousel event listeners */
        this.$carousel.on(CNN.Carousel.events.initialized, this.onInitialized);
        this.$carousel.on(CNN.Carousel.events.navigationInitialized, this.carouselAdjustNavigation);
        this.$carousel.on(CNN.Carousel.events.navigationInitialized, this.addAnalyticsListeners);

        /* Initialize the Main Carousel */
        this.$carousel.owlCarousel({
            addClassActive: true,
            nav: true, /* Show next and prev buttons */
            navText: ['', ''],
            dots: this.isPaginatedCarousel,
            lazyLoad: true,
            transitionStyle: this.isGallery ? 'fadeIn' : false,
            slideBy: !this.isFullCarousel ? 'page' : 1,
            slideSpeed: 300,
            responsive: this.$carousel.hasClass('carousel--fixed') ? false : {},
            responsiveBaseWidth: this.$carousel[0],
            autoWidth: this.isFullCarousel ? false : true,
            autoHeight: this.isAutoHeight,
            items: this.isFullCarousel ? 1 : 3,
            stagePadding: this.stagePadding(),
            margin: this.isFullCarousel && !this.isGallery ? 20 : 0,
            loop: this.isPaginatedCarousel && this.hasSingleItem ? false : true,
            mouseDrag: !this.hasSingleItem,
            touchDrag: !this.hasSingleItem
        });

        /**
         * A paginated carousel is a form of gallery but without a film strip
         * so we don't require this portion of code to be executed.
         * If there's a gallery, make a filmstrip and all of it's functions
         */
        if (this.isGallery && !this.isPaginatedCarousel) {
            /**
             * Event Handler for rebinding events to the main carousel
             * This will be useful when the navigation can go two-ways
             *
             * @param {object} e - OWL Event passed as event handler callback parameter
             */
            this.reBindBigCarousel = function (e) {
                /* only catch the 'position' event */
                if (e.property.name === 'position') {

                    /* remove temporary event binding when everything's changed */
                    self.$carousel.off(CNN.Carousel.events.changed, self.reBindBigCarousel);

                    /* add the normal listener */
                    self.$carousel.on(CNN.Carousel.events.changed, self.onMainCarouselChange);
                }
            };

            /**
             * Overriding the maximum method available in carousel plugin to
             * enable lastItem properly in filmstrip.
             *
             * @param {object} e - OWL Event passed as event handler callback parameter
             */
            this.enableLastItem = function (e) {
                var filmstrip = e.relatedTarget,
                    numItems = filmstrip.settings.items,
                    stageWidth = Math.abs(filmstrip._coordinates.slice(-1)[0]),
                    stripWidth = filmstrip._width,
                    itemWidth = filmstrip._widths[0],
                    lastItem = Math.ceil((stageWidth - stripWidth) / itemWidth);

                if ((stageWidth > stripWidth) && itemWidth > 0) {
                    self.resetThumbnails(e);

                    /* Updating filmstrip items count which are completely visible */

                    filmstrip.settings.items = numItems > 1 ? Math.floor(stripWidth / itemWidth) : numItems;

                    /* Adjusted co-ordinate value to display last item in filmstrip properly */
                    filmstrip._coordinates[lastItem - 1] = (stripWidth % itemWidth) - (itemWidth * lastItem);

                    filmstrip.maximum = function (relative) {
                        var settings = this.settings,
                            stageWidth = this._width,
                            maximum = this._coordinates.length,
                            scrolledWidth = this._current > 0 ? this._coordinates[this._current - 1] : 0,
                            balance = Math.abs(filmstrip._coordinates.slice(-1)[0]) - (stageWidth - scrolledWidth);

                        if (settings.loop) {
                            maximum = this._clones.length / 2 + this._items.length - 1;
                        } else if (settings.autoWidth || settings.merge) {
                            maximum = Math.ceil((balance > stageWidth ? stageWidth - scrolledWidth : balance - scrolledWidth) / this._widths[0]);
                        } else if (settings.center) {
                            maximum = this._items.length - 1;
                        } else {
                            maximum = this._items.length - settings.items;
                        }

                        if (relative) {
                            maximum -= this._clones.length / 2;
                        }

                        return Math.max(maximum, 0);
                    };
                }
            };

            /**
             * Event Handler for the 'change' event, not currently used as the
             * filmstrip is subservient to the main gallery carousel
             *
             * @param {object} e - OWL Event passed as event handler callback parameter
             */
            this.onFilmstripChange = function (e) {
                var index;

                if (e.property.name === 'items') {
                    index = self.$filmstrip.find('.synced').index() < e.item.index ? e.item.index : self.$filmstrip.find('.synced').index();
                    self.$filmstrip.find('.owl-item').removeClass('synced');
                    self.$filmstrip.find('.owl-item').eq(index).addClass('synced');
                    self.$carousel.trigger(CNN.Carousel.events.to, [index]);
                }
            };

            /**
             * Handler for when a filmstrip item is clicked
             *
             * @param {object} e - jQuery Event passed on item click
             */
            this.onFilmstripItemClicked = function (e) {
                var index = jQuery(e.currentTarget).index(),
                    $owlItem = self.$filmstrip.find('.owl-item');

                if (!$owlItem.eq(index).hasClass('synced')) {
                    $owlItem.removeClass('synced');
                    $owlItem.eq(index).addClass('synced');

                    self.$carousel.off(CNN.Carousel.events.changed, self.onMainCarouselChange);
                    self.$carousel.on(CNN.Carousel.events.changed, self.reBindBigCarousel);
                    self.$carousel.on(CNN.Carousel.events.changed, self.resetThumbnails);
                    self.$carousel.trigger(CNN.Carousel.events.to, [index]);
                }
            };

            /**
             * After filmstrip is initialized, set the number of items and add
             * some event listeners
             *
             * @param {object} e - OWL Event passed as event handler callback parameter
             */
            this.onFilmstripInit = function (e) {
                self.setNumberOfItems(e);
                self.enableLastItem(e);

                self.$filmstrip.find('.owl-item').eq(0).addClass('synced');
                self.$filmstrip.on('click', '.owl-item', self.onFilmstripItemClicked);
                self.$filmstrip.on(CNN.Carousel.events.changed, self.onFilmstripChange);
                self.$filmstrip.on(CNN.Carousel.events.resized, self.setNumberOfItems);
                self.$filmstrip.on(CNN.Carousel.events.resized, self.enableLastItem);
                self.$filmstrip.on(CNN.Carousel.events.refreshed, self.enableLastItem);
            };

            this.makeLazy(this.$filmstrip);

            /* Set the filmstrip initialized listener */
            this.$filmstrip.on(CNN.Carousel.events.initialized, this.onFilmstripInit);

            /* Initialize the Filmstrip */
            this.$filmstrip.owlCarousel({
                items: 10,
                autoWidth: true,
                lazyLoad: true,
                loop: false,
                nav: true,
                dots: false,
                navText: ['', ''],
                responsiveRefreshRate: 100
            });
        }

        return this;
    },

    /**
     * This will intialize/recreate all carousels on the page
     *
     * @param  {Object} context  - DOM Context used for finding carousels
     */
    restore: function restore(context) {
        'use strict';

        var $carousel,
            videoCollectionList = [],
            videoCollectionClasses = '';

        /* Collection Class On Video Section Front */
        videoCollectionList.push('.cn-collection-player.el__videoexperience__collection');
        /* Collection Class On Video Leaf */
        videoCollectionList.push('.el__leafmedia--video-leaf-player.el__videoexperience__collection');
        /* Collection Class On Video Article */
        videoCollectionList.push('.el__leafmedia--featured-video-collection');
        videoCollectionClasses = videoCollectionList.join(', ');

        if (CNN.Features.enableVideoExperienceUnification) {
            jQuery(CNN.Carousel.carouselSelector, context).each(function () {
                function processCollection(carouselObj) {
                    Modernizr.on('phone', function checkModernizrPhone(result) {
                        /* This logic checks for a phone and that this is not an article. */
                        if (result && !(CNN.contentModel && CNN.contentModel.pageType && CNN.contentModel.pageType === 'article')) {
                            (function renderCarouselObject(carousel) {
                                CNN.Carousel.FeatureVideoContentStarted.then(
                                    function loadVidex() {
                                        CNN.INJECTOR.executeFeature('videx').done(
                                            function renderSeeMore() {
                                                jQuery('.loading-video-carousel').hide();
                                                CNN.Videx.SeeMore.init(carousel);
                                                jQuery(document).triggerCarouselLoaded();
                                            }
                                        );
                                    }
                                );
                            })(carouselObj);
                        } else {
                            (function renderCarouselObject(carousel) {
                                CNN.Carousel.FeatureVideoContentStarted.then(
                                    function () {
                                        var rtnCarousel = new CNN.Carousel.owl(carousel);
                                        jQuery('.loading-video-carousel').hide();
                                        jQuery(document).triggerCarouselLoaded();
                                        return rtnCarousel;
                                    }
                                );
                            })(carouselObj);
                        }
                    });

                }

                $carousel = jQuery(this);

                if ($carousel.parents(videoCollectionClasses).length > 0) {
                    processCollection($carousel);
                } else {
                    /* non-video gallery */
                    (function renderCarouselObject(carouselObj) {
                        var carousel = new CNN.Carousel.owl(carouselObj);
                        jQuery(document).triggerCarouselLoaded();
                        return carousel;
                    })($carousel);
                }
            });
        } else {
            jQuery(CNN.Carousel.carouselSelector, context).each(function () {
                jQuery(document).triggerCarouselLoaded();
                return new CNN.Carousel.owl(this);
            });
        }
    },

    manualImageLoad: function manualImageLoad(carousel) {
        'use strict';
        carousel.find('img').each(function () {
            var $img = jQuery(this),
                eqState = ($img.attr('data-eq-state') || '').replace(/^(\S+\s+)*/, ''),
                srcAttr;

            if (eqState) {
                srcAttr = eqState === 'xsmall' ? 'data-src-small' : 'data-src-' + eqState;
            } else {
                srcAttr = 'src';
            }
            $img.attr('data-src', $img.attr(srcAttr)).addClass('owl-lazy');
            CNN.ResponsiveImages.process(CNN.DemandLoading.addElement($img));
        });
        CNN.ResponsiveImages.queryEqjs(carousel);
    },

    init: function init(document) {
        'use strict';
        /* Init server loaded galleries */
        CNN.Carousel.restore(document);

        /* Refresh carousels on window resize (may not trigger an OWL resize/resized event) */
        jQuery(window)
            .throttleEvent('resize', function resizeCarousel() {
                jQuery(CNN.Carousel.carouselSelector).trigger(CNN.Carousel.events.refresh);
            }, 100);

        /* Refresh carousels on orientation */
        jQuery(window).on('orientationchange', function refreshCarouselOrient() {
            jQuery(CNN.Carousel.carouselSelector).trigger(CNN.Carousel.events.refresh);
        });
    }
};


CNN.ads = CNN.ads || {};
CNN.ads.events = CNN.ads.events || (function () {
    'use strict';

    var clickCounter = 0;

    /**
     * Generic (somewhat) ad refresh hook
     *
     * @requires window.AdFuel.refreshAd
     *
     * @param {object} e - Event object to latch onto.
     * @param {object} options - Ad refresh options
     * @param {string} [options.adId] - Ad div ID to refresh (e.g. 'ad_rect_atf_01')
     * @param {string} options.pageType - pageType to refresh the Ad on
     * @param {string} options.galleryParent - the jQuery selector for the gallery parent
     * @param {string} options.toolParent - the jQuery selector for the tool card parent
     * @param {string} options.tool - the jQuery selector for the tool card
     */
    function refreshAd(e, options) {
        var adId,
            enableGalleryAdRefresh = CNN.AdsConfig.enableGalleryAdRefresh;

        if (typeof e !== 'undefined' &&
            typeof window.AdFuel !== 'undefined' &&
            jQuery(e.target).parents(options.galleryParent).length &&
            CNN.contentModel.pageType === options.pageType &&
            CNN.Features.enableEpicAds === true) {

            clickCounter++;

            adId = options.adId || jQuery(options.toolParent).find(options.tool + ':first-child div').attr('id');

            if (clickCounter % 2 === 0 && enableGalleryAdRefresh === true) {
                window.AdFuel.refresh([adId], {
                    interval: 0,
                    pageload: false,
                    maintainCorrelator: false
                });
            }
        }
    }

    /**
     * Refresh rail ad based on gallery / carousel leaf interaction.
     *
     * @param {object} e - Event object created from CNN.Gallery.analytics.init
     */
    function refreshGalleryRailAd(e) {
        e = e || null;
        refreshAd(e, {
            adId: 'ad_rect_atf_01',
            pageType: 'article',
            galleryParent: '.zn-large-media',
            toolParent: '.pg-rail',
            tool: '.ad-desktop'
        });
    }

    return {
        refreshGalleryRailAd: refreshGalleryRailAd
    };
}());


CNN.Carousel.FeatureVideoContentStarted = jQuery.Deferred();

if (!(CNN.VideoConfig && CNN.VideoConfig.collection && CNN.VideoConfig.collection.delayVideoCarousel)) {
    /* Feature flag of video carousel. */
    CNN.Carousel.FeatureVideoContentStarted.resolve({});
}

jQuery(document).onVideoContentStarted(function onVideoContentStartedHandler() {
    'use strict';

    CNN.Carousel.FeatureVideoContentStarted.resolve({});
});

/* PAL inspects the model and sets CNN.autoPlayVideoExist value / video-demand also updates this value */
CNN.INJECTOR.executeFeature('videodemanddust').done(
    function checkForAutoPlayVideos() {
        'use strict';

        if (!CNN.autoPlayVideoExist) {
            /* There are no auto play videos. */
            CNN.Carousel.FeatureVideoContentStarted.resolve({});
        }
    }
);

jQuery(document).onZonesAndDomReady(function onZonesAndDomReadyHandler() {
    'use strict';

    CNN.Carousel.init(document);
});

/* global CNN, module, AdFuel */

/**
 * loadAds module.
 * @module loadAds
 * @see module:src/js/elements/ads/loadAds
 */

/*
Loads ads based on the right rail configuration.  The right rail configuration
originates in the michone trinity configuration file.
*/

var rightRailConfig = CNN.rightRailConfig,
    isLongArticle = false,
    initialized = jQuery.Deferred(),
    rightRailAdList = [];

/**
 * Sets up up configuration data and caches DOM elements.
 *
 * @private
 */

function init() {
    'use strict';
    var columnCount = 0,
        columns = [],
        parentElement = {},
        newColumn = {},
        i = 0,
        obWidgetElements = {},
        domRail = {};

    rightRailConfig = CNN.rightRailConfig;

    if (!rightRailConfig || !rightRailConfig.shortArticle || !rightRailConfig.longArticle) {
        return;
    }

    if (CNN.ArticleHeight >= rightRailConfig.longArticle.articleLength) {
        isLongArticle = true;
        rightRailAdList = rightRailConfig.longArticle.epic;
    } else {
        rightRailAdList = rightRailConfig.shortArticle.epic;
    }
    domRail = document.getElementsByClassName('pg-rail')[0];

    /* Cache DOM Elements For Ads So They Don't Need To Be Retrieved Any More */
    for (i = 0; i < rightRailAdList.length; i++) {
        /* cache ad's DOM zone */
        rightRailAdList[i].zone = domRail.children[(rightRailAdList[i].zonePosition)];
        /* cache ad's DOM column */
        columns = jQuery(rightRailAdList[i].zone).find('.zn__containers .column');
        columnCount = columns.length;
        if (columnCount === 1) {
            /* there was only one column so there is no place to put an ad */
            parentElement = columns[0].parentElement;
            newColumn = document.createElement('div');
            newColumn.className = 'column zn__column--idx-1';

            /* Create A Column For The Ad To Be Injected Into */
            parentElement.appendChild(newColumn);
            rightRailAdList[i].adColumn = newColumn;
        } else {
            /* The second column is the correct column for ads */
            rightRailAdList[i].adColumn = columns[1];
        }
        rightRailAdList[i].obColumn = columns[0];
        rightRailAdList[i].obColumn.listElement = jQuery(columns[0]).find('ul');
        obWidgetElements = jQuery(columns[0]).find('js-m-outbrain');

        /* check for outbrain in column 1 and cache the answer*/
        if (obWidgetElements.length > 0) {
            rightRailAdList[i].outbrainExists = true;
        } else {
            rightRailAdList[i].outbrainExists = false;
        }

        /*
        this is set to true when this system injects an ad...
        if it's false it might have been prepended elsewhere
        */
        rightRailAdList[i].adLoadedVerified = false;
    }
}

/**
 * Exposes init to anything that imports this module.  Prevents init from
 * executing until the article length has been resolved.
 *
 * @private
 */

function initializeData() {
    'use strict';
    CNN.ArticleLengthResolved.then(function initialInitialization() {
        init();
        initialized.resolve();
    });
}

/**
 * Injects ads into columns and puts the ad id into the HTML.
 * @param {string} ad - the configuration for the ad
 * @param {string} adTemplate - the HTML for the ad
 * @private
 */

function loadSingleAd(ad, adTemplate) {
    'use strict';
    if (ad) {
        jQuery(ad.adColumn).prepend(adTemplate.replace(/\{epicAdId\}/g, ad.adId));
    }
}

/**
 * When the page loads any Destop rail ads need to have some HTML prepended
 * to the ad.  If the HTML already exists it should note be added.  This
 * function expects a template to be passed in.  It replaces all [epicAdId]
 * with the appropriate epicAdId from trinity.
 *
 * @param {string} adTemplate - the HTML for the ad
 * @private
 */

function loadAd(adTemplate) {
    'use strict';

    var ad = {},
        i = 0;

    initialized.then(function renderAd() {
        for (i = 0; i < rightRailAdList.length; i++) {
            ad = rightRailAdList[i];
            if (ad.adColumn && !ad.adLoadedVerified) {
                /* make sure it hasn't been prepended elsewhere */
                if (jQuery(ad.adColumn).hasClass('ad--desktop')) {
                    ad.adLoadedVerified = true;
                } else {
                    loadSingleAd(ad, adTemplate);
                    ad.adLoadedVerified = true;
                }
            }
        }
    });
}

/**
 * Load the outbrain widget into the second zone.  Also executes OBR.extern.researchWidget().
 *
 * @param {string} outbrainTemplate - the HTML for the ad
 * @private
 */

function loadOutbrain(outbrainTemplate) {
    'use strict';
    initialized.then(function renderOutbrain() {
        var obHtml = '',
            i = 0,
            editionizedUrl = '';

        /* this jQuery has no layout validation side effects - no need for fastdom */
        editionizedUrl = jQuery('meta[property="vr:canonical"]').attr('content') || '';
        for (i = 0; i < rightRailAdList.length; i++) {
            if (!rightRailAdList[i].outbrainExists) {
                if (rightRailAdList[i].obColumn.listElement.length && rightRailConfig.longArticle.outbrain && rightRailConfig.longArticle.outbrain.widgetId) {
                    obHtml = outbrainTemplate.replace(/\{widgetid\}/g, rightRailConfig.longArticle.outbrain.widgetId);
                    obHtml = obHtml.replace(/\{editionizedUrl\}/g, editionizedUrl);
                    jQuery(rightRailAdList[i].obColumn.listElement).append(obHtml);
                }
            }
        }

        if (window.OBR && window.OBR.extern) {
            try {
                window.OBR.extern.researchWidget();
            } catch (err) {
                console.log('Failed to load Outbrain widget ID');
            }
        }
    });
}

/**
 * Sets up an event handler that handles the edge case where an article grows
 * from short to large.  It ads a new ad.
 *
 * @param {string} adTemplate - the HTML for the ad
 * @private
 */

function setupHeightChangeHandler(adTemplate) {
    'use strict';
    jQuery(document).onArticleLengthChange(
        function articleLengthChangeHandler() {
            var ad = {};
            /*
                Ignore this event unless:
                1) The current article length is shortArticle
                2) The new article length is long
            */
            if (!isLongArticle && rightRailConfig.longArticle.articleLength && (CNN.ArticleHeight >= rightRailConfig.longArticle.articleLength)) {
                ad = rightRailConfig.longArticle.epic[1];
                /* cache to the long article data */
                if (ad) {
                    init();
                    AdFuel.queueRegistry(CNN.contentModel.registryURL);
                    loadSingleAd(ad, adTemplate);
                    AdFuel.clearSlot([ad.adId]);
                    AdFuel.dispatchQueue({slots: [ad.adId]});
                }
                isLongArticle = true;
            }
        }
    );
}


module.exports.initializeData = initializeData;
module.exports.loadAd = loadAd;
module.exports.loadOutbrain = loadOutbrain;
module.exports.setupHeightChangeHandler = setupHeightChangeHandler;

/* global CNN, fastdom, module */

/**
 * lockZones module.
 * @module lockZones
 * @see module:src/js/elements/ads/lockZones
 */

/*
Sets the state of the first and second zones to one of three states:
1) clear - no special styling.
2) locked to the bottom - the bottom of the ad is connected to the bottom of the
zone.
3) locked to nav - top of the ad is connected to the bottom of the nav.
*/

var initialized = jQuery.Deferred(),
    pinningEnabled = false,
    rightRailConfig = CNN.rightRailConfig || {},
    pinningArticleLengthThreshold = rightRailConfig.pinningThreshold || 1000,
    longArticleConfig = rightRailConfig.longArticle || {},
    configPinningLock = longArticleConfig.enableAdLock,
    domRail,
    zones = [],
    firstZone,
    secondZone,
    firstAd,
    secondAd,
    nav = document.getElementById('nav');

/*
note: If you reference the actual ad for the height stuff gets weird because
ads and OB loads at difference times.
*/

/*
    Register For Article Change Events
    note: This code is auto executed when this module is loaded.
*/

/**
 * Measures and Analyzes the right rail.  Then sets the styles of the ads.
 *
 * @private
 */
function executeLocking() {
    'use strict';
    var bottomOfNav,
        topOfZone01,
        bottomOfZone01,
        bottomOfZone02,
        ad01AtBottom,
        topOfAd01,
        topOfZone02,
        topOfAd02,
        ad02AtBottom,
        mutateList = [];

    /**
     * All mutations should be executed at one time (during the next animation
     * frame) this function queues the mutation.
     *
     * @param {object} zone - reference to the zone element
     * @param {object} ad - reference to the ad element
     * @param {string} mod - clear, bottom, nav
     * @private
     */

    function stageMutation(zone, ad, mod) {
        mutateList.push({zone:zone, ad:ad, mod:mod});
    }


    /**
     * Executes the mutation.
     *
     * @param {object} zone - reference to the zone element
     * @param {object} ad - reference to the ad element
     * @param {string} mod - clear, bottom, nav
     * @private
     */

    function mutateLayout(zone, ad, mod) {

        /**
         * Removes all styles from the ad and the zone.
         *
         * @param {object} zone - reference to the zone element
         * @param {object} ad - reference to the ad element
         * @private
         */
        function clearAdStyles(zone, ad) {
            /* don't do anything if the ad has already been cleared */
            if (ad.locked !== 'none') {
                jQuery(zone).attr('style', '');
                jQuery(ad).attr('style', '');
                ad.locked = 'none';
            }
        }

        /**
         * Locks the bottom of the ad to the bottom of the zone
         *
         * @param {object} zone - reference to the zone element
         * @param {object} ad - reference to the ad element
         * @private
         */
        function lockAdToBottom(zone, ad) {
            if (ad.locked !== 'bottom') {
                jQuery(ad).attr('style', '');
                jQuery(zone.element).css('position', 'relative');
                jQuery(ad).css({position: 'absolute', bottom: '0px', width: '300px'});
                ad.locked = 'bottom';
            }
        }

        /**
         * Locks the top of the ad (actually outbrain) to the bottom of the nav
         *
         * @param {object} zone - reference to the zone element
         * @param {object} ad - reference to the ad element
         * @private
         */

        function lockAdToNav(zone, ad) {
            if (ad.locked !== 'nav') {
                jQuery(ad).attr('style', '');
                /* this means that the top of the nav is above the top of the first ad */
                jQuery(ad).css({position: 'fixed', top: bottomOfNav, width: '300px'});
                ad.locked = 'nav';
            }
        }

        if (mod === 'clear') {
            clearAdStyles(zone, ad);
        } else if (mod === 'bottom') {
            lockAdToBottom(zone, ad);
        } else if (mod === 'nav') {
            lockAdToNav(zone, ad);
        }
    }

    /**
     * Determines what the zone 1 and zone 2 ads layout state should be.
     *
     * @private
     */

    function analyzeLayoutData() {

        if (pinningEnabled === false) {
            /* pinning is disabled. */
            stageMutation(firstZone, firstAd, 'clear');
            stageMutation(secondZone, secondAd, 'clear');

        /*
            The navigation is above the first zone.
            navIsAboveZone01 = (bottomOfNav <= topOfZone01)
        */
        } else if (bottomOfNav <= topOfZone01) {
            /* browser is above zone 1. */
            stageMutation(firstZone, firstAd, 'clear');
            stageMutation(secondZone, secondAd, 'clear');

        /*
            The navigation bar is below the second zone.

            navIsBelowZone02 = (bottomOfNav >= bottomOfZone02)
        */
        } else if (bottomOfNav >= bottomOfZone02) {
            /* brower is below zone 2 */
            stageMutation(firstZone, firstAd, 'bottom');
            stageMutation(secondZone, secondAd, 'bottom');

            /*
                The navigation bar is within the the first zone.
                navIsInZone01 = (bottomOfNav > topOfZone01 && bottomOfNav < bottomOfZone01),
            */
        } else if (bottomOfNav > topOfZone01 && bottomOfNav < bottomOfZone01) {
            /* the browser is in zone01 */
            stageMutation(secondZone, secondAd, 'clear');
            /*
                The navigation bar is above the top of the first ad.
                navAboveAd01 = (bottomOfNav < topOfAd01)
            */
            if (ad01AtBottom && (bottomOfNav < topOfAd01)) {
                stageMutation(firstZone, firstAd, 'nav');
            } else if (ad01AtBottom) {
                stageMutation(firstZone, firstAd, 'bottom');
            } else {
                stageMutation(firstZone, firstAd, 'nav');
            }
            /*
                The navigation bar is within the second zone.
                navIsInZone02 = (bottomOfNav > topOfZone02 && bottomOfNav < bottomOfZone02),
            */
        } else if (bottomOfNav > topOfZone02 && bottomOfNav < bottomOfZone02) {
            /* the browser is in zone02 */
            stageMutation(firstZone, firstAd, 'bottom');

            /*
            The navigation bar is above the top of the second ad.
            (bottomOfNav < topOfAd02);
            */
            if (ad02AtBottom && (bottomOfNav < topOfAd02)) {
                stageMutation(secondZone, secondAd, 'nav');
            } else if (ad02AtBottom) {
                stageMutation(secondZone, secondAd, 'bottom');
            } else {
                stageMutation(secondZone, secondAd, 'nav');
            }
        }
    }

    /**
     * Itterates over each queued mutation and executes them.
     *
     * @private
     */

    function mutateLayouts() {
        var i;
        for (i = 0; i < mutateList.length; i++) {
            mutateLayout(mutateList[i].zone, mutateList[i].ad, mutateList[i].mod);
        }
    }

    /**
     * Measure and cache all necessary dimesions.
     *
     * @private
     */
    function measureLayouts() {
        bottomOfNav = nav.getBoundingClientRect().bottom;
        topOfZone01 = firstZone.element.getBoundingClientRect().top;
        bottomOfZone01 = firstZone.element.getBoundingClientRect().bottom;
        bottomOfZone02 = secondZone.element.getBoundingClientRect().bottom;
        ad01AtBottom = (firstAd.getBoundingClientRect().bottom >= bottomOfZone01);
        topOfAd01 = firstAd.getBoundingClientRect().top;
        topOfZone02 = secondZone.element.getBoundingClientRect().top;
        topOfAd02 = secondAd.getBoundingClientRect().top;
        ad02AtBottom = (secondAd.getBoundingClientRect().bottom >= bottomOfZone02);
    }

    /*
        Do all measures at one time, then analyze the data, then mutate.
        One set of reads and one set of writes.
    */
    fastdom.measure(
        function gatherLayoutInformation() {
            measureLayouts();
            fastdom.mutate(
                function () {
                    analyzeLayoutData();
                    mutateLayouts();
                }
            );
        }
    )
}

/**
 * Sets up the cached elements and data to avoid re-queries and unneeded recalculations.
 *
 * @private
 */

function init() {
    'use strict';
    var zoneList = [],
        container = {},
        containers = [],
        i = 0;

    pinningEnabled = (pinningArticleLengthThreshold < CNN.ArticleHeight) && configPinningLock;

    if (!domRail) { /* cache rail */
        domRail = {};
        domRail.element = document.getElementsByClassName('pg-rail')[0];
        domRail.top = domRail.element.getBoundingClientRect().top;
    }

    if (zones.length === 0) { /* cache zones this shouldn't change */
        zoneList = jQuery(domRail.element).find('.zn');
        container = {};
        for (i = 0; i < zoneList.length; i++) {
            /*
            container - the position for the container changes from fixed to
            absolute which causes the dimesions to change which causes a
            blinking along the boundaries.
            */
            container = jQuery(zoneList[i]).find('.l-container')[0];
            /*
            containers - the size for the containers doesn't change when the
            position of the parent changes.  So containers is needed as a reference
            to start/stop locking.
            */
            containers = jQuery(container).find('.zn__containers')[0];
            /*
            possible lock states:
            none - unchanged
            bottom - the bottom of the ad is locked to the bottom of its zone
            nav - to top (outbrain section) of the ad is locked to the bottom of the nav
            */
            zones.push({element:zoneList[i], container: container, containers: containers, locked: 'none'});
        }
        /* firstAd and secondAd are references to zone[0].container and zone[1].container */
        firstZone = zones[0];
        secondZone = zones[1];
        firstAd = zones[0].container;
        firstAd.locked = 'none';
        secondAd = zones[1].container;
        secondAd.locked = 'none';
    }

    for (i = 0; i < zones.length; i++) {
        zones[i].height = jQuery(zones[i].element).height();
    }
}

/**
 * Executes the initialization and resolves the initialized promise.
 *
 * @private
 */
function initializeData() {
    'use strict';

    /* Assign the custom article change handler */
    jQuery(document).onArticleLengthChange(
        function articleLengthChangeHandler() {
            if (initialized.state() === 'resolved') {
                init();
                executeLayoutChanges();
            }
        }
    );


    CNN.ArticleLengthResolved.then(function initialInitialization() {

        /* Assign the scroll handler */
        window.onscroll = function scrollHandler() {
            /*
                When this event was assigned as follows:

                window.onscroll = executeLocking;

                There is a lot ad blinking when the menu moves from first to the second
                zone.
            */
            executeLocking();
        };

        init();
        initialized.resolve();
    });
}

/**
 * Changes the height of the first and second zone based on the currently cached
 * article height.
 *
 * @private
 */

function executeLayoutChanges() {
    'use strict';
    initialized.then(
        function layoutChanges() {
            var eachHeight = 0,
                alignmentOffset = 19;

            if (pinningEnabled === true) {
                fastdom.measure(
                    function measureZones() {
                        eachHeight = Math.floor(CNN.ArticleHeight / 2 - zones[2].height / 2 - alignmentOffset);
                        jQuery(zones[0].element).height(eachHeight);
                        jQuery(zones[1].element).height(eachHeight);
                        fastdom.mutate(
                            function mutateForLock() {
                                executeLocking();
                            }
                        );
                    }
                );
            }
        }
    );
}

/**
 * Handle the case in which something changes in the right rail.  Usually one of
 * the ads changes heights.  Sometimes that change can cause the ads to overlap.
 *
 * Special attention must be given to the third zone (zones[2] because the height
 * of the third zone helps determine the height of the first and second zone.
 * This is esepcially tricky because the zone starts at 20 pixels tall and then
 * becomes 400 - 500 pixels tall.  Then it becomes 900 pixels tall.  But not always.
 *
 * @private
 */
function setupRightRailObserver() {
    'use strict';

    /*
        If anything in the DOM of the right rail changes:
        attribute: if a class, style property, datatype...
        childlist: a node is added or deleted
        characterData: text
        subtree: all of the above for the right rail's child nodes.

        Note: If something outside (usually a parent class) changes the dimesions
        of something within the right rail that won't be caught by this observer.
    */

    function mutationItemHandler() {
        /* check for a changes in the third zone */
        fastdom.measure(
            function measureThirdZone() {
                var thirdZoneHeight = jQuery(zones[2].element).height();
                if (zones[2].height !== thirdZoneHeight) {
                    zones[2].height = thirdZoneHeight;
                    executeLayoutChanges();
                } else {
                    executeLocking();
                }
            }
        );
    }

    initialized.then(
        function rightRailObserver() {
            var observer = {},
                config = { attributes: true, childList: true, characterData: true, subtree: true };

            /* create an observer instance */
            observer = new MutationObserver(function rightRailMutationHandler(mutations) {
                mutations.forEach(mutationItemHandler);
            });

            observer.observe(domRail.element, config);
        }
    );
}

module.exports.initializeData = initializeData;
module.exports.executeLayoutChanges = executeLayoutChanges;
module.exports.setupRightRailObserver = setupRightRailObserver;

/* global CNN, fastdom, jQuery */

/*
1) Determines the article size.
2) Updates the CNN.ArticleHeight.
3) Triggers triggerArticleLengthChange function call (defined in page-events.js)
when the articles size changes.
*/

(function initArticleLengthWatch(targetNode, jQuery, CNN) {
    'use strict';
    var currentHeight = 0,
        tallRailImageHeight = 0, /* content width pages must account for the height of the top image */
        isQueued = false,
        lastDomCheckTime = 0,
        lastHeight = 0,
        target = jQuery(targetNode),
        element = {},
        domThrottle = 1000,
        resizeThrottle = 250,
        config = { attributes: true, childList: true, characterData: true, subtree: true };

    CNN.ArticleHeight = 0;
    CNN.ArticleLengthResolved = jQuery.Deferred();

    if (target.length === 0) {
        /*  Don't set up article watching because the element does not exist. */
        CNN.ArticleLengthResolved.reject();
        return;
    } else {
        element = target[0];
    }

    /**
     * Sets the height of article and triggers the ArticleLengthChanged event.
     *
     * @private
     */

    function resetHeight() {
        lastDomCheckTime = Date.now();
        fastdom.measure(
            function measureContentHeight() {
                currentHeight = jQuery(element).height();
                CNN.ArticleHeight = currentHeight + tallRailImageHeight;
                isQueued = false; /* The last DOM change is handled */
                if (currentHeight !== lastHeight) {
                    lastHeight = currentHeight;
                    jQuery(document).triggerArticleLengthChange();
                    if (CNN.ArticleLengthResolved.state() !== 'resolved') {
                        CNN.ArticleLengthResolved.resolve();
                    }
                }
            }
        );
    }

    /**
     * Sets registers a resize reset request.
     *
     * @param {integer} throttle - the minimum amount of time that must have
     * passed since the last time a resize was executed before this event should
     * be processed.
     *
     * @private
     */

    function executeResize(throttle) {
        if (isQueued === false) {
            /* This is not a resize check in the pipeline */
            if ((Date.now() - lastDomCheckTime) > throttle) {
                /*
                In this case it's been a long time since the last RESIZE update so just
                fire it.
                */
                resetHeight();
            } else {
                /*
                In this case the height was checked less than a second ago and the next
                last is NOT queuede up so check it again in a second.
                */
                isQueued = true;
                setTimeout(resetHeight, throttle);
            }
        }
    }

    /**
     * registers the resize and orientationchange events.
     *
     * @private
     */

    (function initPageChangeWatch() {
        window.addEventListener('resize', function resizeHandler() {
            executeResize(resizeThrottle);
        }, false);

        window.addEventListener('orientationchange', function orientationChangeHandler() {
            executeResize(resizeThrottle);
        }, false);
    })();

    /**
     * sets up the DOM watch.
     *
     * DOM changes happen in bursts.  For example when the user expands an
     * image there are around 300 DOM changes over a one second period.  Code
     * that responds to article length changes shouldn't try to respond to
     * all 300 changes.  In addition most DOM changes don't change the article
     * length.  Code that response to article length change shouldn't be
     * informed of a DOM change unless the DOM change changes the length of
     * the article.
     *
     * 1) DOM changes should never be handled more than one time per second.
     * 2) The last DOM change should always be handled.
     * 3) Before triggering an article change event make sure the length actually
     * changed.
     *
     * @private
     */

    (function initDomWatch() {
        var observer = new MutationObserver(function mutationHandler() {
            executeResize(domThrottle);
        });

        /**
        * pass in the target node, as well as the observer options
        */
        observer.observe(element, config);
    })();

    /**
    * Check For Tall Rail Changes - During the next animation frame
    * @private
    */
    (function checkForTallRail() {
        var pageHead = jQuery('.pg-rail-tall__head');
        if (pageHead.length !== 0) {
            fastdom.measure(function measurePageHeadHeight() {
                tallRailImageHeight = jQuery(pageHead[0]).height() + 4;
            });
        }
    })();
})('.zn-body-text', jQuery, CNN);

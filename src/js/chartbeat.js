/* global CNN */

CNN.Chartbeat = CNN.Chartbeat || {};
CNN.Features = CNN.Features || {};

/**
 * Charbeat Enhancements
 * The sections value has been loaded with extra infromation
 * about leaf pages.  Since this is dependent on information
 * from the CNN.omniture object which is done by window.load,
 * the timing had to be adjusted.
 *
 * @function
 */
jQuery(window).load(function initChartBeat() {
    'use strict';
    var e;

    if (CNN.Features.enableChartbeat === true && CNN.Chartbeat.uid && CNN.Chartbeat.src) {
        /* CONFIGURATION START */
        window._sf_async_config = window._sf_async_config || {};
        if (CNN.Features.enableChartbeatMAB !== true) {
            /* Normally set in the Chartbeat MAB setup, but if that is disabled we need to do it here. */
            window._sf_async_config.domain = CNN.Host.main;
            window._sf_async_config.uid = CNN.Chartbeat.uid;
            window._sf_async_config.useCanonical = true;
        }
        window._sf_async_config.sections = (CNN.contentModel.analytics.chartbeat && CNN.contentModel.analytics.chartbeat.sections) || '';
        window._sf_async_config.authors = CNN.contentModel.analytics.author || '';
        /* CONFIGURATION END */

        window._sf_endpt = (new Date()).getTime();
        e = document.createElement('script');
        e.setAttribute('language', 'javascript');
        e.setAttribute('type', 'text/javascript');
        e.setAttribute('src', CNN.Chartbeat.src);
        document.body.appendChild(e);
    }
});

/* global CNN, FAVE, Modernizr, fastdom */

CNN.VideoConfig = CNN.VideoConfig || {};
CNN.VideoConfig.api = CNN.VideoConfig.api || {};

/**
 * Video Source utilities. This Utility class is used to find/update
 * a source span that may be under a video element and display the source
 * of the video, if applicable.
 */
CNN.VideoSourceUtils = {
    /**
     * Get the DOM element that contains the html that displays the
     * source for the video.
     *
     * @param {string} videoContainerId - The id of the DOM element
     *                 that contains the video player. This will be the
     *                 used to retrieve the Source span
     *
     * @returns {object} The Source span DOM element
     */
    getSourceElement: function (videoContainerId) {
        'use strict';

        var metadataElement,
            parentElement;

        /*
         * Retrieve the parent element of the video container
         * as that will be the the parent of the source span
         */
        parentElement = jQuery('#' + videoContainerId).parent();
        metadataElement = jQuery(parentElement).find('.metadata');

        return metadataElement.find('.metadata__source-name');
    },

    /**
     * Clear out the text of the source .
     * @param {string} videoContainerId - The id of the DOM element
     *                 that contains the video player. This will be the
     *                 used to retrieve the Source span
     */
    clearSource: function (videoContainerId) {
        'use strict';

        var sourceElement = this.getSourceElement(videoContainerId);

        if (sourceElement) {
            /*
             * Fill in with a non breaking space vs empty string, for
             * styling purposes
             */
            sourceElement.html('&nbsp;');
        }
    },

    /**
     * Update the source span with the data retrieved from the video.
     *
     * @param {string} videoContainerId - The id of the DOM element
     *                 that contains the video player. This will be the
     *                 used to retrieve the Source span
     *
     * @param {object} metadata - The json video metadata retrieved from a
     *                 currently playing PIP video.
     */
    updateSource: function (videoContainerId, metadata) {
        'use strict';

        var sourceElement = this.getSourceElement(videoContainerId),
            prependText = 'Source: ',
            withUrlText = jQuery('<a></a>');

        metadata = (typeof metadata === 'string') ? JSON.parse(metadata) : metadata;
        if (sourceElement) {
            sourceElement.empty();
            if (metadata) {
                /*
                 * If the source url is available, create a link
                 * If no source url is available, just display the source text
                 * */
                if (metadata.sourceUrl) {
                    withUrlText.attr('href', metadata.sourceUrl);
                    withUrlText.text(metadata.source);
                    sourceElement.html(prependText).append(withUrlText);
                } else {
                    sourceElement.html(prependText + metadata.source);
                }
            }
        }
    }
};

(function () {
    'use strict';

    var addedVideos = [];

    /**
     * Inititializing VideoPlayer to an empty object.
     */
    CNN.VideoPlayer = {};

    /**
     * Queue's up a video.
     *
     * @param {object}   options                     - Required: Options to pass to the Video API.
     * @param {object}   options.video               - Required: Video ID.
     * @param {string}   options.context             - Required: Specifies which UI to use.
     * @param {string}   options.network             - Required: 'cnn|cnn-test'
     * @param {string}   options.profile             - Required: Needs clarity. For now, use 'expansion'
     * @param {string}   options.markupId            - Required: DOM Element ID that the video will be injected into.
     * @param {string}  [options.frameWidth]         - Required if profile="iframeTest": Width of the video iframe.
     * @param {string}  [options.frameHeight]        - Required if profile="iframeTest": Height of the video iframe.
     * @param {string}  [options.adsection]          - Optional ad section name.
     * @param {string}  [options.width='100%']       - Width of video element.
     * @param {string}  [options.height='100%']      - Height of the video element.
     * @param {boolean} [options.autostart='false']  - Height of the video element.
     *
     * @param {object} [callbackObj='{}'] - callbacks object
     * @see http://docs.turner.com/display/MPDTCV/MPDT+-+JavaScript+API#MPDT-JavaScriptAPI-Contentcallbacks
     *
     * @public
     */
    CNN.VideoPlayer.addVideo = function (options, callbackObj) {
        if (typeof options === 'undefined') {
            console.error('required options were not passed');

            return;
        }

        if (typeof options.video === 'undefined' || typeof options.context === 'undefined' ||
            typeof options.network === 'undefined' || typeof options.profile === 'undefined' ||
            typeof options.markupId === 'undefined') {

            console.error('Not all required options were passed');
            return;
        }

        if (options.context === 'iframeTest' &&
            (typeof options.frameWidth === 'undefined' || typeof options.frameHeight === 'undefined')) {

            console.error('frameWidth and frameHeight must be passed for iframe videos');
            return;
        }

        if (typeof callbackObj === 'undefined') {
            callbackObj = {};
        }

        addedVideos.push({
            videoConfig: options,
            eventsCallback: callbackObj,
            overridesConfig: {} /* Temporarily Unused */
        });
        try {
            /* Add the event listeners and callback for getting amazon key/value pairs*/
            CNN.AmazonVideoAds.Manager.getInstance().addAmazonCallback(callbackObj);
        } catch (error) {
            /*
             * Do Nothing if an error occurs, just allow it to continue playing a regular ad
             * if an error occurs.
             */
        }
    };
    /**
     * Send video to the API
     *
     * @requires window.cnnVideoMangaer.renderMultipleContainers
     * @fires window.cnnVideoMangaer.renderMultipleContainers()
     *
     * @public
     */
    CNN.VideoPlayer.initVideos = function () {
        window.cnnVideoManager.renderMultipleContainers(addedVideos);
        /* Reset added videos */
        addedVideos = [];
    };

    /**
     * Queue multiple videos.
     *
     * @param {array}  array - An array of properly formated objects to send to add to the queue
     * @fires window.CNN.VideoPlayer.addVideo()
     * @fires window.CNN.VideoPlayer.initVideos()
     *
     * @public
     */
    CNN.VideoPlayer.addVideos = function (array) {
        var i = 0;

        if (typeof array === 'undefined') {
            console.error('An array was not passed');
            return;
        }

        for (i = array.length - 1; i >= 0; i--) {
            window.CNN.VideoPlayer.addVideo(array[i]);
        }

        window.CNN.VideoPlayer.initVideos();
    };

    /**
    * Public function that returns boolean of whether users is using a mobile client
    *
    * @returns {boolean} - does device support orientation feature (a way to check for mobile clients without useragent mappings)
    *
    * @public
    */
    CNN.VideoPlayer.isMobileClient = function () {
        return (typeof window.orientation !== 'undefined');
    };

    /**
     * Public function that returns boolean of whether user has flash
     *
     * @returns {boolean} hasFlash - whether Flash is installed
     *
     * @public
     */
    CNN.VideoPlayer.isFlashInstalled = function () {
        var hasFlash = false;

        if ('ActiveXObject' in window) {
            try {
                hasFlash = (typeof new window.ActiveXObject('ShockwaveFlash.ShockwaveFlash') !== 'undefined');
            } catch (b) {
                hasFlash = false;
            }
        } else {
            hasFlash = (typeof navigator.mimeTypes['application/x-shockwave-flash'] !== 'undefined');
        }
        return hasFlash;
    };

    /**
    * Handler to disable or enable video ads based on whether the player is in focus or not
    *
    * @param {string} containerId - the player container
    * @param {string} visible - boolean string for the player focus state
    *
    * @public
    */
    CNN.VideoPlayer.handleAdOnCVPVisibilityChange = function (containerId, visible) {
        if (CNN.VideoPlayer.getLibraryName(containerId) === 'videoLoader') {
            if (visible) {
                window.cnnVideoManager.getPlayerByContainer(containerId).videoInstance.cvp.setAdKeyValue('inFocus', 'true');
            } else {
                window.cnnVideoManager.getPlayerByContainer(containerId).videoInstance.cvp.setAdKeyValue('inFocus', 'false');
            }
        }
    };

    /**
    * Render a video player using the Video Loader library and add the Amazon Ad callback
    *
    * @param {object} configObj - video configs object
    * @param {object} callbackObj - video callbacks object
    *
    * @public
    */
    CNN.VideoPlayer.render = function renderVideoPlayer(configObj, callbackObj) {
        window.cnnVideoManager.renderMultipleContainers({videoConfig:configObj, eventsCallback:callbackObj});
        try {
            /* Add the event listeners and callback for getting amazon key/value pairs*/
            CNN.AmazonVideoAds.Manager.getInstance().addAmazonCallback(callbackObj);
        } catch (error) {
            /*
             * Do Nothing if an error occurs, just allow it to continue playing a regular ad
             * if an error occurs.
             */
        }
    };

    /**
     * Unmutes the player if it had been muted by CNN.VideoPlayer.mutePlayer
     *
     * @param {string} containerId - Id of the player
     *
     * @public
     */
    CNN.VideoPlayer.reverseAutoMute = function reverseAutoMute(containerId) {
        var playerInstance;

        if (CNN.VideoPlayer.getLibraryName(containerId) === 'fave') {
            playerInstance = FAVE.player.getInstance(containerId);
        } else {
            playerInstance = window.cnnVideoManager.getPlayerByContainer(containerId).videoInstance.cvp;
        }

        /*
         * Unmutes the player if it had been muted by CNN.VideoPlayer.mutePlayer
         */
        if (playerInstance.isMuted()) {
            playerInstance.unmute();
        }
    };

    /*
     * Capture the "time to first video frame", from start of the page load to when CVP initializes,
     * and pass the value to Aspen.
     * Only for autostart videos.
     * Per the CVP Team:
     * writeSessionId(string) is called immediately after the Aspen hello response.
     * Because all the timing info we are piecing together on the Aspen side starts with the hello,
     * writeSessionId is the best place to stop the page timing to avoid data gaps and overlap.
     * Note, writeSessionId is not a CVP callback like the others, but is instead called on the global space.
     * It also only exists on Flash currently.
     * - as soon as possible, start a page timer
     * - when writeSessionId is called, stop the timer
     * - in onCVPReady callback, call reportAnalytics to send the data
     */
    /* Start "time to first video frame" code */
    if (typeof CNN.VideoPlayer.pageLoadStartTime === 'undefined') {
        CNN.VideoPlayer.pageLoadStartTime = new Date();
    }

    window.writeSessionId = function writeSessionId(_string) {
        CNN.VideoPlayer.setLoadTime();
    };

    CNN.VideoPlayer.setLoadTime = function setLoadTime() {
        if (CNN.autoPlayVideoExist === true && typeof CNN.VideoPlayer.loadTime === 'undefined') {
            CNN.VideoPlayer.writeSessionIdCallTime = new Date();
            CNN.VideoPlayer.loadTime = CNN.VideoPlayer.writeSessionIdCallTime - CNN.VideoPlayer.pageLoadStartTime;
        }
    };

    /**
     * Reports the load time to analytics
     * Called by CVP's onPlayerReady callback
     * @param {string} containerId - container ID for player
     */
    CNN.VideoPlayer.reportLoadTime = function reportLoadTime(containerId) {
        var cvp;

        if (CNN.VideoPlayer.getLibraryName(containerId) === 'videoLoader' &&
            CNN.autoPlayVideoExist === true &&
            CNN.Utils.exists(CNN.VideoPlayer.loadTime)) {
            cvp = containerId && window.cnnVideoManager.getPlayerByContainer(containerId).videoInstance.cvp || null;
            if (CNN.Utils.existsObject(cvp)) {
                cvp.reportAnalytics('videoPageTiming', {
                    pageLoad: CNN.VideoPlayer.loadTime
                });
            }
        }
    };
    /* End "time to first video frame" code */

    CNN.VideoPlayer.showSpinner = function showSpinner(containerId) {
        if (CNN.VideoPlayer.getLibraryName(containerId) === 'videoLoader' &&
            Modernizr && !Modernizr.phone && !Modernizr.mobile && !Modernizr.tablet) {
            jQuery(document.getElementById(('spinner_' + containerId).replace('#', ''))).show();
        }
    };

    CNN.VideoPlayer.hideSpinner = function hideSpinner(containerId) {
        if (Modernizr && !Modernizr.phone && !Modernizr.mobile && !Modernizr.tablet) {
            jQuery(document.getElementById(('spinner_' + containerId).replace('#', ''))).hide();
        }
    };

    CNN.VideoPlayer.hideThumbnail = function hideThumbnail(containerId) {
        if (Modernizr && !Modernizr.phone && !Modernizr.mobile && !Modernizr.tablet) {
            jQuery(document.getElementById(containerId + '--thumbnail')).hide();
        }
    };

    CNN.VideoPlayer.setPlayerProperties = function setPlayerProperties(containerId, autoStartFlag, isLivePlayer, mutePlayer) {
        CNN.VideoPlayer.playerProperties = CNN.VideoPlayer.playerProperties || {};
        CNN.VideoPlayer.playerProperties[containerId] = CNN.VideoPlayer.playerProperties[containerId] || {
            autoStart: autoStartFlag,
            isLive: isLivePlayer,
            mute: mutePlayer
        };
    };

    CNN.VideoPlayer.setManualUnmute = function setManualUnmute(containerId) {
        var playerPropertyObj;

        CNN.VideoPlayer.playerProperties = CNN.VideoPlayer.playerProperties || {};
        playerPropertyObj = CNN.VideoPlayer.playerProperties[containerId] || {};
        playerPropertyObj.contentPlayed = true;
    };

    CNN.VideoPlayer.mutePlayer = function mutePlayer(containerId) {
        var playerInstance,
            playerPropertyObj,
            unmuteCTA,
            unmuteIdSelector = 'unmute_' + containerId;

        if (CNN.VideoPlayer.getLibraryName(containerId) === 'fave') {
            playerInstance = FAVE.player.getInstance(containerId);
        } else {
            playerInstance = window.cnnVideoManager.getPlayerByContainer(containerId).videoInstance.cvp;
        }
        playerPropertyObj = CNN.VideoPlayer.playerProperties[containerId];
        if (playerPropertyObj.mute) {
            if (playerPropertyObj.autoStart && playerPropertyObj.isLive) {
                playerInstance.mute();
                unmuteCTA = jQuery(document.getElementById(unmuteIdSelector));
                if (unmuteCTA.length > 0) {
                    unmuteCTA.removeClass('video__unmute--inactive').addClass('video__unmute--active');
                    unmuteCTA.on('click', jQuery.proxy(this.handleUnmutePlayer, this, containerId));
                }
            }
        } else {
            playerInstance.unmute();
        }
    };

    /**
     * Inject the FAVE library
     *
     * @param {object} configObj - configuration for the video player
     * @param {object} callbackObj - callbacks for the video player
     */
    CNN.VideoPlayer.injectFave = function injectFave(configObj, callbackObj) {
        CNN.INJECTOR.executeFeature('fave').then(function () {
            FAVE.player({
                configs: configObj,
                callbacks: callbackObj
            });
            CNN.VideoPlayer.setLibraryName('fave', configObj.markupId);
        });
    };

    /**
     * Inject the Video Loader library
     *
     * @param {object} configObj - configuration for the video player
     * @param {object} callbackObj - callbacks for the video player
     */
    CNN.VideoPlayer.injectLoader = function injectLoader(configObj, callbackObj) {
        CNN.INJECTOR.executeFeature('videoLoader', {params:'?version=latest&client=expansion'}).then(function () {
            if (CNN.VideoPlayer.apiInitialized) {
                CNN.VideoPlayer.render(configObj, callbackObj);
            } else {
                CNN.VideoPlayer.addVideo(configObj, callbackObj);
            }
            CNN.VideoPlayer.setLibraryName('videoLoader', configObj.markupId);
        });
    };

    /**
     * Check Fave Settings to determine whether 'fave' will be used
     * for a given live video id.
     *
     * @param {string} videoId - Video Id to be checked for fave enabled or not
     * @returns {boolean} isFaveLive - boolean showing whether FAVE will be used for this video id
     */
    CNN.VideoPlayer.isFaveLiveEnabled = function isFaveLiveEnabled(videoId) {
        var isFaveLive = false,
            faveLiveSettings = FAVE.settings.live,
            faveLiveStreams;

        if (FAVE.settings.enabled && faveLiveSettings.enabled) {
            faveLiveStreams = faveLiveSettings.enabledLiveStreams;
            if (Array.isArray(faveLiveStreams) &&
                faveLiveStreams.length > 0 &&
                faveLiveStreams.indexOf(videoId) !== -1) {
                isFaveLive = true;
            }
        }
        return isFaveLive;
    };

    /**
     * Set the library name for a player instance.
     * The library name can be 'fave' for the FAVE (Future Aligned Video Experience) Library
     * or 'videoLoader' for the CNN Video Loader library.
     *
     * @param {string} libraryName - name of the library used to instantiate the video player
     * @param {string} containerId - ID of the video player container
     */
    CNN.VideoPlayer.setLibraryName = function setLibraryName(libraryName, containerId) {
        CNN.VideoPlayer.playerProperties = CNN.VideoPlayer.playerProperties || {};
        CNN.VideoPlayer.playerProperties[containerId] = CNN.VideoPlayer.playerProperties[containerId] || {};
        CNN.VideoPlayer.playerProperties[containerId].libraryName = libraryName;
    };

    /**
     * Get the library name of an instantiated player.
     * The returned library name can be 'fave' for the FAVE (Future Aligned Video Experience) Library
     * or 'videoLoader' for the CNN Video Loader library.
     *
     * @param {string} containerId - ID of the video player container
     * @returns {string} - library name
     */
    CNN.VideoPlayer.getLibraryName = function getLibraryName(containerId) {
        return CNN.VideoPlayer.playerProperties &&
            CNN.VideoPlayer.playerProperties[containerId] &&
            CNN.VideoPlayer.playerProperties[containerId].libraryName;
    };

    /**
     * Get a video library, and use its API to create a video player instance
     *
     * @param {object} configObj - configuration for the video player
     * @param {object} callbackObj - callbacks for the video player
     * @param {boolean} isLive - whether this should be a live unauthenticated video or not
     */
    CNN.VideoPlayer.getLibrary = function getLibrary(configObj, callbackObj, isLive) {
        var videoId = configObj.video,
            isFaveLive;

        isLive = isLive || false;

        if (isLive) {
            isFaveLive = CNN.VideoPlayer.isFaveLiveEnabled(videoId);
            if (isFaveLive) {
                CNN.geoCheckStream(configObj.markupId, CNN.VideoPlayer.injectFave, configObj, callbackObj);
            } else {
                CNN.VideoPlayer.injectLoader(configObj, callbackObj);
            }
        } else {
            if (FAVE.settings.enabled) {
                /*
                 * Virtual Reality 360 video is considered a premium feature, so we need to:
                 * 1) Set the maximum bitrate to Infinity, so there is no bitrate cap
                 * 2) Set the player's initial rendition to high for best quality.
                 */
                if (configObj.isVr === true) {
                    configObj.player = configObj.player || {};
                    configObj.player.maxBitrate = 'Infinity';
                    configObj.theoplayer = configObj.theoplayer || {};
                    configObj.theoplayer.initialRendition = 'high';
                }
                CNN.VideoPlayer.injectFave(configObj, callbackObj);
            } else {
                CNN.VideoPlayer.injectLoader(configObj, callbackObj);
            }
        }
    };

    CNN.VideoPlayer.handleInitialExpandableVideoState = function handleInitialExpandableVideoState(containerId) {
        if (CNN.VideoPlayer.getLibraryName(containerId) === 'fave') {
            jQuery(document.getElementById(containerId)).parents('.js__video--standard, .js__video--expandable').each(function () {
                var $this = jQuery(this),
                    $thumbnail,
                    $videoContainer;
                fastdom.measure(function () {
                    $thumbnail = $this.find('.media__video--thumbnail');
                    $videoContainer = $this.find('.media__video--responsive');
                    fastdom.mutate(function () {
                        $videoContainer.hide();
                        $thumbnail.show();
                        $thumbnail.css('position', 'relative');
                        $this.find('.js-video-demand').show();
                    });
                });
            });
        }
    };

    CNN.VideoPlayer.setFirstVideoInCollection = function setFirstVideoInCollection(collection, containerId) {
        CNN.VideoPlayer.firstVideoInCollection = CNN.VideoPlayer.firstVideoInCollection || {};
        if (Array.isArray(collection) &&
            collection.length > 0 &&
            CNN.Utils.existsString(collection[0].videoId)) {
            CNN.VideoPlayer.firstVideoInCollection[containerId] = collection[0].videoId;
        }
    };

    /**
     * Checks to see if a currently playing video is the first video
     * in the current collection. Used to set Freewheel key/value pair
     *
     * @param {object} containerId - ID of the video player container
     * @param {object} videoId - current video ID
     */
    CNN.VideoPlayer.isFirstVideoInCollection = function (containerId, videoId) {
        var playerInstance;

        if (CNN.Utils.existsObject(CNN.VideoPlayer.firstVideoInCollection) &&
            CNN.Utils.existsString(CNN.VideoPlayer.firstVideoInCollection[containerId])) {
            if (CNN.VideoPlayer.getLibraryName(containerId) === 'fave') {
                playerInstance = FAVE.player.getInstance(containerId);
            } else {
                playerInstance = window.cnnVideoManager.getPlayerByContainer(containerId).videoInstance.cvp;
            }
            if (CNN.VideoPlayer.firstVideoInCollection[containerId] === videoId) {
                playerInstance.setAdKeyValue('isFirst', 'true');
            } else {
                playerInstance.setAdKeyValue('isFirst', 'false');
            }
        }
    };
}());

window.CNNVideoAPILoadComplete = function CNNVideoAPILoadComplete() {
    'use strict';

    var hasCompanion = CNN.companion && CNN.companion.initCompanionLayout;

    if (hasCompanion && typeof CNN.companion.initCompanionLayout === 'function') {
        CNN.companion.initCompanionLayout();
    }

    CNN.VideoPlayer.apiInitialized = true;
    CNN.VideoPlayer.initVideos();
};

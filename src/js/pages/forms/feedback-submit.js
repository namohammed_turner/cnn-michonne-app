/* global CNN */

(function (ns, $j) {
    'use strict';

    var ctx = {},
        validate,
        counter = $j('.feedback-form-comments-counter');

    function Context(state) {
        this.label = state.label;
        this.form = document.getElementById('js-' + this.label + '-form');
        this.mailTo = state.mailTo || ns.feedbackForms.context.mailTo || 'cnn-support-communications';
        this.ctx = state.ctx;

        this.state = {
            form:   this.form,
            label:  this.label,
            mailTo: this.mailTo,
            target: this.ctx.target
        };
    }

    Context.prototype.init = function init() {
        this.ctx.preventDefault();
        $j('#js-' + this.label + '-send')
            .prop('disabled', true)
            .off('click');
        ns.feedbackForms.init(this.state);
    };

    /* on success will submit the student news form */
    $j('#js-cnn10-send').on('click', function (e) {
        ctx.label = 'cnn10';
        ctx.mailTo = 'cnn-student-news';
        ctx.ctx = e;

        validate = new Context(ctx);
        validate.init();
    });

    /* on success will submit the ac360 form */
    $j('#js-ac360-send').on('click', function (e) {
        ctx.label = 'ac360';
        ctx.ctx = e;

        validate = new Context(ctx);
        validate.init();
    });

    /* on success will submit the feedback form */
    $j('#js-feedback-send').on('click', function (e) {
        ctx.label = 'feedback';
        ctx.ctx = e;

        validate = new Context(ctx);
        validate.init();
        counter.text(counter.data('maxLength') + ' characters remaining');
    });

    /* on success will submit the go feedback form */
    $j('#js-go-send').on('click', function (e) {
        ctx.label = 'go';
        ctx.mailTo = 'cnn-go-feedback';
        ctx.ctx = e;

        validate = new Context(ctx);
        validate.init();
    });

    /* on success will submit the politicsapp feedback form */
    $j('#js-politicsapp-send').on('click', function (e) {
        ctx.label = 'politicsapp';
        ctx.mailTo = 'cnn-politicsapp';
        ctx.ctx = e;

        validate = new Context(ctx);
        validate.init();
    });

    /* limit the number of characters allowed in the 'Additional Comments' textarea */
    $j('#feedback-form-comments').on('input', function (_e) {
        var charactersRemaining = counter.data('maxLength') - $j('#feedback-form-comments').val().length;

        if (charactersRemaining < 0) {
            $j('#feedback-form-comments').val($j('#feedback-form-comments').val().substr(0, counter.data('maxLength')));
        } else {
            if (charactersRemaining < 21) {
                counter.addClass('feedback-form-comments-limit');
            } else {
                if (counter.hasClass('feedback-form-comments-limit')) {
                    counter.removeClass('feedback-form-comments-limit');
                }
            }
            counter.text(charactersRemaining + (charactersRemaining === 1 ? ' character remaining' : ' characters remaining'));
        }
    });

}(CNN, jQuery));

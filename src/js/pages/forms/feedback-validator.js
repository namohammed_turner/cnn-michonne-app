/* global CNN, fastdom */

/**
 * @module user-feedback-validator
 *
 * validates and submits user feedback forms
 * (e.g. /feedback/studentnews and /feedback/ac360)
 *
 */
(function formValidator($j, doc, ns, win, fastdom) {
    'use strict';

    if (!ns.hasOwnProperty('feedbackForms')) {
        ns.feedbackForms = (function feedbackFormValidation() {

            /**
             * @var {object} setfeedback
             *
             * An object representing the message labels that are shown to the
             * user based on their action (e.g., if the user submitted anything other than
             * an email address). If other messages need to be shown to the user then add
             * them to this object.
             *
             * @var {String} setfeedback.submissionFailed
             * User feedback for an error at the service level
             *
             * @var {String} setfeedback.success
             * The text the end user sees on successful form submit
             *
             * @private
             */
            var
                setFeedback = {
                    submissionFailed: 'There was a problem in submitting your feedback, please try again later.',
                    submitted: 'Thank you for your feedback.  It has been submitted successfully.'
                };

            /**
             * on success will retrieve all required fields and check if they're empty
             *
             * @function
             * @param {Object} config
             * an object containing the necessary data to validate and process each form
             *
             * @private
             */
            function structure(config) {
                var i;

                config = config || {};
                if (config.form && config.form.nodeType && $j.type(config.label) === 'string') {
                    config.fields = getFields(config.form.id);
                    config.requiredFields = (function () {
                        var requiredFields = [];

                        for (i = 0; i < config.fields.length; i++) {
                            if (config.fields[i].required &&
                                config.fields[i].title &&
                                config.fields[i].type !== 'hidden' &&
                                $j(config.fields[i]).is(':visible')) {

                                requiredFields.push({
                                    node: config.fields[i],
                                    msg: config.fields[i].title
                                });
                            }
                        }

                        return requiredFields;
                    }());

                    if (config.requiredFields.length > 0) {
                        isValueEmpty(config.requiredFields, config);

                        $j(config.target).on('click', function (e) {
                            e.preventDefault();
                            resetUI(config.form.id);

                            fastdom.mutate(function disableWhileProcessing() {
                                $j(config.target).prop('disabled', true);
                            });
                            isValueEmpty(config.requiredFields, config);
                        });
                    }

                } else {
                    console.log('Config type validation failed.');
                }
            }

            /**
             * Reads the value of required fields
             *
             * @function
             * @param {Array} nodes - required field names
             * @returns {Array} - string values of fields
             *
             * @private
             */
            function getRequiredFieldValues(nodes) {
                return [].map.call(nodes, function (field) {
                    return $j.trim(field.node.value);
                });
            }

            /**
             * getFields
             *
             * @function
             * @private
             * @param {HTMLElement} form - The form element
             * @returns {NodeList} - All the form input nodes
             */
            function getFields(form) {
                return doc.querySelectorAll('#' + form + ' input, #' + form + ' textarea, #' + form + ' select');
            }

            /**
             * sets the message with the node in error
             * on success will add a class to the form in error & append the msg
             *
             * @function
             * @private
             * @param {Array} requiredFields - required fields for the form
             * @param {Object} config - config object
             *
             * @var {Number} i
             * @var {Array} hasErrors
             * @var {Array} nodeValues
             */
            function isValueEmpty(requiredFields, config) {
                var i,
                    hasErrors = [],
                    nodeValues = getRequiredFieldValues(requiredFields);

                for (i = 0; i < nodeValues.length; i++) {
                    if (nodeValues[i] === '') {
                        hasErrors.push({
                            msg: requiredFields[i].msg,
                            node: requiredFields[i].node
                        });
                    }
                }

                processForm(config);

                if (hasErrors.length > 0) {

                    fastdom.mutate(function enableSubmitButton() {
                        $j(config.target).prop('disabled', false);
                    });
                    highlightAndShowErrors(hasErrors);
                }
            }

            /**
             * submits the form to the mailcar service
             * on success will show a success msg to the user and reset the form
             *
             * @function
             * @private
             * @param {object} config - form config object
             *
             * @var {Number} i
             * @var {Array} hasErrors
             * @var {Array} nodeValues
             */
            function processForm(config) {
                var
                    ajaxOpts = {
                        cache: false,
                        dataType: 'json',
                        contentType: 'application/json',
                        type: 'POST',
                        url: ns.contentModel.feedback.apiEndpoint + config.label,
                        data: null
                    },
                    data,
                    mailTo;

                processFormFields(config).then(function () {
                    data = this.submitted;
                    mailTo = (ns.Utils.exists(config.mailTo)) ? {mailTo: config.mailTo} : {mailTo: config.email};
                    ajaxOpts.data = $j.extend(true, {}, data, {location: 'unknown'}, mailTo);
                    ajaxOpts.data = JSON.stringify(ajaxOpts.data);

                    $j.ajax(ajaxOpts).done(function (data) {
                        if (data.MessageId && data.ResponseMetadata.RequestId) {
                            setUserFeedback(1);
                            config.form.reset();
                            fastdom.mutate(function enableSubmitButton() {
                                $j(config.target).prop('disabled', false);
                            });
                        }
                    }).fail(function () {
                        setUserFeedback(0);
                    });
                });
            }

            /**
             * constructs an object of input names and values to send to the service
             * on success will resolve the promise with the form's data
             *
             * @function
             * @private
             * @param {object} config - form config object
             * @returns {Promise} - promise with form data
             *
             * @var {Object} data
             * @var {Object} dfr
             * @var {Array} hasErrors
             */
            function processFormFields(config) {
                var data        = {},
                    dfr         = $j.Deferred(),
                    hasErrors   = [],
                    element,
                    i;

                for (i = 0; i < config.fields.length; i++) {
                    element = config.fields[i];
                    if ($j(element).is(':visible')) {
                        if (element.type === 'email') {
                            if (!(validateEmail(element))) {
                                hasErrors.push({
                                    msg: element.title,
                                    node: element
                                });
                            } else {
                                data[element.name] = element.value;
                            }
                        } else if (element.type === 'radio') {
                            if ($j('input[name=' + element.name + ']:checked').length > 0) {
                                data[element.name] = $j('input[name=' + element.name + ']:checked').val();
                            } else {
                                hasErrors.push({
                                    msg: element.title,
                                    node: element,
                                    type: element.type
                                });
                            }
                        } else {
                            data[element.name] = escapeInput(element);
                        }
                    }
                }

                /* since we're dropping support for IE8 */
                if (!hasErrors.length && Object.keys(data).length > 0) {
                    dfr.resolveWith({submitted: data});
                } else if (hasErrors.length > 0) {
                    dfr.rejectWith({hasErrors: hasErrors});
                } else {
                    dfr.reject();
                }

                return dfr.promise().fail(function processFormFieldFail() {
                    if (this.hasErrors) {
                        fastdom.mutate(function enableSubmitButton() {
                            $j(config.target).prop('disabled', false);
                        });
                        highlightAndShowErrors(this.hasErrors);
                    } else {
                        console.log('Unable to process form fields');
                    }
                });
            }

            /**
             * For security purpose, escape the form field data.
             *
             * @function
             * @private
             * @param {domObject} input - form input string data
             * @returns {string} escaped version for input
             */
            function escapeInput(input) {
                return $j('<div></div>').text($j.trim(input.value)).html();
            }

            /**
             * on successful validation will return true
             *
             * @function
             * @private
             * @param {HTMLElement} input - form input element
             * @return {boolean} e-mail validity
             *
             * @var {regex} check
             */
            function validateEmail(input) {
                var check = /^([\w\.\'\+\-]+)\@([\w\-]{1,63}\.)+[\w\-]{2,63}$/;

                return (check.test(input.value)) ? true : false;
            }

            /**
             * highlights fields and writes user feedback on fields
             * with validation errors
             *
             * @function
             * @private
             * @param {Array} arr - an array of objects containing the node and its' error message
             *
             * @var {Number} i
             */
            function highlightAndShowErrors(arr) {
                var i;

                for (i = 0; i < arr.length; i++) {
                    if (ns.Utils.exists(arr[i].type) && (arr[i].type === 'radio')) {
                        $j(arr[i].node).addClass('error-invalidInput').closest('.feedback-go-rating')
                            .find('.feedback-form-error')
                            .text(arr[i].msg);
                    } else {
                        $j(arr[i].node).addClass('error-invalidInput')
                            .next('.feedback-form-error')
                            .text(arr[i].msg);
                    }
                }
            }

            /**
             * resetUI
             * @function
             * @private
             * @var {NodeList} errors
             * @var {Number} i
             */
            function resetUI() {
                var errors = doc.querySelectorAll('.error-invalidInput'),
                    i;

                if (errors.length > 0) {
                    for (i = 0; i < errors.length; i++) {
                        if (errors[i].type === 'radio') {
                            $j(errors[i]).removeClass('error-invalidInput').closest('.feedback-go-rating')
                                .find('.feedback-form-error')
                                .text('');
                        } else {
                            $j(errors[i]).removeClass('error-invalidInput')
                                .next('.feedback-form-error')
                                .text('');
                        }
                    }
                }
            }

            /**
             * provides user feedback
             *
             * @function
             * @private
             * @param {boolean} isSuccess - truthy value
             *
             * @var {String} msg
             */
            function setUserFeedback(isSuccess) {
                var msg = (isSuccess) ? setFeedback.submitted : setFeedback.submissionFailed;
                $j('#js-feedback-message').text(msg);
            }

            return {
                init: function process(config) {
                    if (ns.Utils.existsObject(config)) {
                        structure(config);
                    }
                },
                context: {email: 'cnn-support-communications'}
            };
        }());
    }
}(jQuery, document, CNN, window, fastdom));


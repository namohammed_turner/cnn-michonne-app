/* global CNN */

(function ($j, ns) {
    'use strict';

    /**
     * merges legacy static forms using the validate.additionalFields property
     *
     * @var {Number} i
     * interator
     *
     * @var {String} params
     * query string parameters
     *
     */

    var i,
        params,
        validate = ns.feedbackForms.context;

    if (location.search.length > 0) {
        params = location.search.toLowerCase().split(/[\?\&]+/);

        for (i = 0; i < params.length; i++) {
            switch (params[i]) {
            case 's=generalcomments&hdln=4':
                /* legacy form: /feedback/show/?s=generalcomments&hdln=4 */
                validate.additionalFields = undefined;
                break;

            case 's=ipad':
                /* legacy form: /feedback/show/?s=ipad */
                validate.mailTo = 'cnn-ipad';
                validate.additionalFields = undefined;
                break;

            case '1':
                /* legacy form: /feedback/forms/form11b.html?1 */
                validate.additionalFields = 'storyIdea';
                break;

            case '108':
                /* legacy form: /feedback/forms/form5.html?108 */
                validate.additionalFields = 'commentTone';
                break;

            case '106':
                /* legacy form: /feedback/forms/form5.html?106 */
                validate.additionalFields = 'commentTone';
                break;

            case 'newstips':
                /* legacy form: /feedback/tips/newstips.html */
                validate.additionalFields = 'newstip';
                break;

            case 'soundoff':
                /* legacy form: /feedback/forms/form.sound.off.html */
                validate.additionalFields = 'commentTone';
                break;

            default:
                validate.additionalFields = undefined;
            }
        }
    }

    if (!validate.hasOwnProperty('mailTo')) {
        validate.mailTo = 'cnn-support-communications';
    }

    if (validate.hasOwnProperty('additionalFields') && validate.additionalFields !== undefined) {
        $j('#js-feedback-additionalFields .feedback-' + validate.additionalFields)
            .show()
            .children('input, textarea, select')
            .show();
    }

}(jQuery, CNN));


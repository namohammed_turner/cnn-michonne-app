/* global CNN, fastdom */

/*  ------------
    CNN Special Article

    @description:   Custom interactions for the special article template- this will be
                    included only when the special article dust file is built.
    -------------
*/

jQuery(document).ready(function () {
    'use strict';

    var $shareRail = jQuery('.js-share-rail-top'),
        $shareRailContents = jQuery('.js-gigya-sharebar'),
        $bodyText = jQuery('.zn-body-text'),
        $bodyFooter = jQuery('.zn-body__footer'),
        $articleBody = jQuery('.pg-special-article__body'),
        headerHeight,
        originalPosition,
        articleBottom,
        bodyWidth = jQuery(window).width(),
        isMobile = jQuery('html').is('.ios, .android, .iemobile'),
        position;

    /* toggle caption open/close on page-top image */
    jQuery('.pg-special-article__head .media__caption').on('click', function () {
        jQuery(this).toggleClass('media__caption--open');
    });

    /* Calculates position using box model, offsetting for scroll */
    position = function (elem) {
        var box = elem[0].getBoundingClientRect();

        return {
            top: box.top + CNN.CurrentSize.getClientScrollTop(),
            bottom: box.bottom + CNN.CurrentSize.getClientScrollTop()
        };
    };


    /* update share bar position on resize */
    if (!isMobile) {
        jQuery(window).on('resize', function () {
            bodyWidth = jQuery(window).width();

            if (bodyWidth > 1040) {
                $shareRail.css({
                    position: '',
                    top: ''
                });
            }

            calculateSharePosition();
        });
    }

    /* calculate fixed share bar position */
    function calculateSharePosition() {
        fastdom.measure(function () {
            /* if not super wide, remove the fixed position */
            if (bodyWidth < 1040) {
                $shareRail.css({
                    position: '',
                    top: ''
                });
                return;
            }

            /* calculate some things */
            headerHeight = jQuery('#nav__plain-header').length !== 0 ? jQuery('#nav__plain-header').height() : jQuery('#nav-header').height();
            originalPosition = position($bodyText).top;
            articleBottom = position($bodyFooter).top - headerHeight - 100 + $bodyFooter.height();

            /* if we're past the bottom of the article (into the footer), place the share at the bottom */
            if (CNN.CurrentSize.getClientScrollTop() + $shareRailContents.height() > articleBottom) {
                $shareRail.css({
                    position: 'absolute',
                    top: position($bodyFooter).top - position($articleBody).top + $bodyFooter.height() - $shareRailContents.height()
                });
            /* if we're past the original position of the share, fix it to the left */
            } else if (CNN.CurrentSize.getClientScrollTop() > originalPosition - headerHeight - 100) {
                $shareRail.css({
                    position: 'fixed',
                    top: headerHeight + 50 + 'px'
                });
            /* otherwise, remove all positioning */
            } else {
                $shareRail.css({
                    position: '',
                    top: ''
                });
            }
        });
    }

    /* set scroll listener */
    if (!isMobile) {
        jQuery(document).ready(function () {
            jQuery(window).on('scroll', calculateSharePosition);
        });
    }
});

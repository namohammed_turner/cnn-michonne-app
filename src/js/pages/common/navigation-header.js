/* global fastdom, CNN */

/*
 * This file handles all of the navigation interaction.
 *
 * On scroll, nav gets a class of .skinny to slim it down. All transitions in .skinny state are handled in _header.scss, along
 * with Meganav timing, and all visual transitions.
 */
CNN.navigation = {

    willShowMeganav: false,
    $header: jQuery('.nav-header'),

    init: function () {
        'use strict';

        this.$body = jQuery('body');
        this.isMobileDevice = jQuery('html').is('.iemobile, .android, .ios');
        this.$showPoster = jQuery('.nav .specials .show-poster');
        this.$search = jQuery('.nav .search input');
        this.isSuperSkinnyNav = function () { return jQuery('.nav').is('.super-skinny'); };
        this.isEnableSkinny = function () { return jQuery('.nav').is('.js-skinny'); };
        this.$thirdPartyNav = function () { return jQuery('.cnn-js-navigation .nav'); };
        this.$staticNav =  function () { return jQuery('.nav.super-skinny'); };
        this.$skinnyNav = function () {
            var $tpn = this.$thirdPartyNav();
            return ($tpn.length > 0) ? $tpn : this.$staticNav();
        };

        /* set the listener for above, unless this is the super-skinny nav embedded in a third party page */
        if (!this.isSuperSkinnyNav() && this.isEnableSkinny()) {
            this.navAutoSkinny(true);
            this.subnavAutoHide(false);
        }

        /* workaround for weird webkit transition error if input is focused when we switch responsive navs */
        window.onresize = function () {
            if (!window.CNN.navigation.isMobileDevice) {
                window.CNN.navigation.$search.blur();
            }
        };

        /* EVENT LISTENERS */
        this.$body.on('click', '.menu-collapse',  this.hamburgerClick);
        this.$body.on('click', '.nav-menu',  this.hamburgerClick);
        this.$body.on('click', '.js-search-toggle', this.searchClick);
        this.$body.on('click', '.bucket > a', this.bucketClick);

        if (!this.isMobileDevice) {
            this.$body.on('mouseenter', '.search__button',  this.showSearch);
            this.$body.on('mouseout', '.search__field',  this.hideSearch);
        }

        this.$body.on('blur', '.search__input-field',  this.hideSearch);
        this.$body.on('focus', '.search__input-field',  this.disableSearchMouseOut);

        this.$body.on('mouseenter', '.bucket > a', this.bucketHover);
        this.$body.on('mouseenter', '.section', this.sectionEnter);
        this.$body.throttleEvent('mouseenter', this.sectionHover, 100, '.section > a');
        this.$body.on('mouseout', '.nav', this.navMouseOut);
        this.$body.on('mouseout', '.mega-nav, .section', this.meganavMouseOut);

        this.$showPoster.on('mouseover', this.showPosterMouseOver);
        this.$showPoster.on('mouseout', this.showPosterMouseOut);

        this.toggleWatchLiveButton();
    },

    /**
     * Display watch live link if user country supports it or if
     * no country can be detected hide button on international
     * and show it on domestic.
     */
    toggleWatchLiveButton: function () {
        'use strict';

        var userCountry = CNN.Utils.getCookie('countryCode');

        if (CNN.contentModel && CNN.WatchLiveCountries && ((!userCountry && CNN.contentModel.edition === 'domestic') || CNN.WatchLiveCountries.indexOf(userCountry) !== -1)) {
            jQuery('.js-nav__live-tv').show();
            if (jQuery('body').hasClass('pg-error-page')) {
                jQuery('body').addClass('show_live-tv');
            }
        }
    },

    /**
     * Add or remove an event listener that will hide the subnav on scroll.
     *
     * @param {Boolean} [isEnabled=true]  - enable/disable subnav toggle.
     */
    subnavAutoHide: function (isEnabled) {
        'use strict';

        if (typeof isEnabled === 'boolean' && isEnabled) {
            jQuery(window).on('scroll.cnnSubNavigation', jQuery.proxy(window.CNN.navigation.toggleHeaderClass, this, 'skinny--without-subnav', true));
        } else {
            jQuery(window).off('scroll.cnnSubNavigation');
            this.toggleHeaderClass('skinny--without-subnav', false);
        }
    },

    /**
     * Add or remove an event listener that will use a skinnier nav when
     * scrolling.
     *
     * @param {Boolean} [isEnabled=true]  - enable/disable skinnier nav toggle.
     */
    navAutoSkinny: function (isEnabled) {
        'use strict';

        if (typeof isEnabled === 'boolean' && isEnabled) {
            jQuery(window).on('scroll.cnnNavigation', jQuery.proxy(window.CNN.navigation.toggleHeaderClass, this, 'skinny', true));
        } else {
            jQuery(window).off('scroll.cnnNavigation');
            this.toggleHeaderClass('skinny', false);
        }
    },

    /**
     * Add or remove classes to the header depending on scroll position.
     *
     * @param {string}  className        - The class name to be added/removed.
     * @param {Boolean} [addClass=true]  - Boolen to force add a class.
     */
    toggleHeaderClass: function (className, addClass) {
        'use strict';
        var self = this;
        addClass = (typeof addClass === 'boolean') ? addClass : true;

        fastdom.mutate(function () {
            if (self.checkScrollPosition() && addClass === true) {
                self.$header.addClass(className);
            } else {
                self.$header.removeClass(className);
            }
        }, self);
    },

    checkScrollPosition: function () {
        'use strict';
        var distanceY = window.pageYOffset || document.documentElement.scrollTop,
            shrinkOn = 60;
        return distanceY > shrinkOn;
    },

    hamburgerClick: function (_e) {
        'use strict';

        window.CNN.navigation.$search.blur();

        fastdom.mutate(function () {
            if (window.CNN.navigation.isSuperSkinnyNav()) {
                window.CNN.navigation.$skinnyNav().toggleClass('nav-open').removeClass('search-open');
            } else {
                window.CNN.navigation.$body.toggleClass('nav-open').removeClass('search-open');
                window.CNN.navigation.headerTracking.adjustNav();
            }
        });

    },

    toggleAria: function ($elm) {
        'use strict';
        var isAriaExpanded = $elm.attr('aria-expanded') === 'true';

        fastdom.mutate(function () {
            $elm.attr('aria-expanded', !isAriaExpanded);
        });
    },

    /**
     * Break down complex conditionals to determine body size
     * @return {Boolean} - true if is tablet size
     */
    isTabletSize: function () {
        'use strict';

        var $body = window.CNN.navigation.$body;

        return ($body.is('[data-eq-state$="xsmall"]') || $body.is('[data-eq-state$="small"]') || $body.is('[data-eq-state$="medium"]'));
    },

    /**
     * Break down complex conditionals to determine if mobile
     * @return {Boolean} - true if is mobile size
     */
    isMobile: function () {
        'use strict';

        var $html = jQuery('html');

        return ($html.is('.iemobile') || $html.is('.android') || $html.is('.ios'));
    },

    /**
     * Adds 'search-open' class when user hovers over search icon- should have
     * no visual impact at full-width
     *
     * @param {object} _e - event object
     *
     * @TODO this search event may used by politics and style pages and is
     * adding unnecessary classes to pages with the new search. Look into how to
     * safely remove.
     */
    showSearch: function (_e) {
        'use strict';

        if (!window.CNN.navigation.isMobile()) {
            fastdom.mutate(function () {
                window.CNN.navigation.$body.addClass('search-open');
            });

            if (window.CNN.navigation.isTabletSize()) {
                jQuery('.search__input-field').focus();
            }
        }
    },

    /**
     * Disables mouse-out hiding the search field if the user has focused on the
     * input text
     *
     * @param {object} _e - event object
     */
    disableSearchMouseOut: function (_e) {
        'use strict';
        window.CNN.navigation.$body.off('mouseout', '.search__field',  window.CNN.navigation.hideSearch);
    },

    /**
     * Removes 'search-open' class when user hovers over search icon- should
     * have no visual impact at full-width.
     * Clears, then adds event listener for search field mouseout event (reset)
     * @param {object} e - event object
     */
    hideSearch: function (e) {
        'use strict';

        /**
         * check that the element firing the mouseout event is not a child of
         * search__field (mouse is still in!)
         */
        fastdom.measure(function () {
            if (e.type === 'mouseout' && ((jQuery(e.toElement).parents('.search__field').length > 0) ||
               (jQuery(e.relatedTarget).parents('.search__field').length > 0))) {
                return;
            }

            if ((e.type === 'mouseout' || e.type === 'focusout') &&
                (window.CNN.navigation.isTabletSize() || window.CNN.navigation.isMobile())) {
                return;
            }

            fastdom.mutate(function () {
                window.CNN.navigation.$body.removeClass('search-open');
            });
        });

        window.CNN.navigation.toggleAria(jQuery('.search-toggle'));

        window.CNN.navigation.$body.off('mouseout', '.search__field',  window.CNN.navigation.hideSearch);
        window.CNN.navigation.$body.on('mouseout', '.search__field',  window.CNN.navigation.hideSearch);
    },

    /**
     * Toggles 'search-open' for touch device search icon taps.
     *
     * @param {object} _e - event object
     */
    searchClick: function (_e) {
        'use strict';

        fastdom.mutate(function () {
            if (window.CNN.navigation.isSuperSkinnyNav()) {
                window.CNN.navigation.$skinnyNav().toggleClass('search-open').removeClass('nav-open');
            } else {
                window.CNN.navigation.$body.toggleClass('search-open');

                if (!(window.CNN.navigation.$body.is('.pg-vertical--entertainment') || window.CNN.navigation.$body.is('.pg-vertical--vr'))) {
                    window.CNN.navigation.$body.removeClass('nav-open');
                }
            }
        });

        if (window.CNN.navigation.$body.is('.search-open')) {

            /* ios will ignore 'fixed' element and chage it to position:absolute- here we scroll to the top and absolutely position the nav */
            if (jQuery('html').is('.ios')) {

                jQuery('.nav .search input').on('focus', function searchInputFocus() {
                    window.CNN.navigation.$body.scrollTop(0);
                    fastdom.mutate(function () {
                        window.CNN.navigation.$body.addClass('fixsearch');
                    });
                    /* on blur remove the class */
                    jQuery('.nav .search input').on('blur', function searchInputBlur() {
                        jQuery(this).off('blur');
                        fastdom.mutate(function () {
                            window.CNN.navigation.$body.removeClass('fixsearch');
                        });
                    });
                });

                /* after we scroll, focus on the input */
                setTimeout(function () {
                    jQuery('.nav .search input').focus();
                }, 10);
            } else {
                jQuery('.nav .search input').focus();
            }
        }
    },

    /**
     * Adds toggles bucket open class when clicking a bucket- this <a> will directly link the user to the bucket section,
     * but this provides a visual cue that your click actually did something before the page loads.
     *
     * @param {object} _e - event object
     */
    bucketClick: function (_e) {
        'use strict';

        var self = this;

        fastdom.mutate(function () {
            jQuery('.bucket.open').removeClass('open');
            jQuery('.bucket.selected').removeClass('selected');
            jQuery(self).parents('.bucket').addClass('open selected');
        }, self);
    },

    /**
     * Opens sub-nav for a top-level navigation bucket on hover
     *
     * @param {object} _e - event object
     */
    bucketHover: function (_e) {
        'use strict';
        var self = this,
            currentBucket;

        fastdom.measure(function () {
            currentBucket = jQuery(self).parents('.bucket');
            if (window.CNN.navigation.isMobileDevice) { return; }

            fastdom.mutate(function () {
                jQuery('.bucket').removeClass('open');

                /* load the more special images after hover the "more" section on mega-nav*/
                if (currentBucket.hasClass('more')) {
                    currentBucket.find('.show-poster').each(function () {
                        this.src = jQuery(this).data('img-bw');
                    });
                }
                currentBucket.addClass('open');
            }, self);
        }, self);
    },

    sectionEnter: function (_e) {
        'use strict';

        window.CNN.navigation.willShowMeganav = true;
    },

    /**
     * Checks if section has a meganav element, if so, OPEN
     *
     * @param {object} _e - event object
     */
    sectionHover: function (_e) {
        'use strict';

        var self = this;

        fastdom.measure(function () {
            /* check if we've got an android, ios, or windows phone/tablet and return to disable flyout menus */
            var $sectionLink = jQuery(self),
                $section = $sectionLink.parents('.section'),
                $megaNav = jQuery('.js-mega-nav'),
                $meganavItem = jQuery('.nav .container > .mega-nav .m-navigation__mega-nav-item').eq($section.index()),
                $prevItem = $meganavItem.prev(),
                $nextItem = $meganavItem.next(),
                doesNotHaveMegaNav = !$section.is('.js-has-meganav');

            if (!window.CNN.navigation.willShowMeganav) { return; }

            if (window.CNN.navigation.isMobileDevice) { return; }

            fastdom.mutate(function () {
                jQuery('.section').removeClass('open');

                if (doesNotHaveMegaNav) {
                    $megaNav.removeClass('open');
                    return;
                }

                $section.addClass('open');

                /* show current meganav item */
                jQuery('.nav .container > .mega-nav .m-navigation__mega-nav-item').removeClass('selected previous next');

                $prevItem.addClass('previous');
                $meganavItem.addClass('selected');
                $nextItem.addClass('next');

                /* Now open the meganav */
                $megaNav.addClass('open');
            });
        }, self);
    },

    /**
     * Navigation mouseout handler, removes open bucket sub-navs and meganav
     *
     * @param {object} e - event object
     */
    navMouseOut: function (e) {
        'use strict';

        /* checks if element that fired mouseout was a child outside of the .nav */
        if ((jQuery(e.toElement).parents('.nav').length === 0) && (jQuery(e.relatedTarget).parents('.nav').length === 0)) {
            fastdom.measure(function () {
                var $selected = jQuery('.bucket.selected'),
                    isNotMoreNav = !$selected.is('.more');

                fastdom.mutate(function () {
                    jQuery('.bucket, .section').removeClass('open');

                    window.CNN.navigation.willShowMeganav = false;

                    /* don't add .open to the .more bucket, will persist an open state */
                    if (isNotMoreNav) {
                        $selected.addClass('open');
                    }
                });
            });
        }
    },

    /**
     * Meganav mouse out handler- makes sure mouseout event target was for the entire meganav, then closes it.
     *
     * @param {object} e - event object
     */
    meganavMouseOut: function (e) {
        'use strict';

        fastdom.measure(function () {
            /* checks if element that fired mouseout was a child outside of the .nav */
            if ((jQuery(e.toElement).parents('.section, .js-mega-nav').length === 0) &&
                (jQuery(e.relatedTarget).parents('.section, .js-mega-nav').length === 0)) {

                fastdom.mutate(function () {
                    jQuery('.mega-nav, .section').removeClass('open');
                });

                window.CNN.navigation.willShowMeganav = false;
            }
        });
    },

    /**
     * 'More' menu mouse over handler
     *
     * @param {object} _e - event object
     */
    showPosterMouseOver: function (_e) {
        'use strict';

        var self = this;

        fastdom.measure(function () {
            self.src = jQuery(self).data('img');
        }, self);
    },

    /**
     * 'More' menu mouse out handler
     *
     * @param {object} _e - event object
     */
    showPosterMouseOut: function (_e) {
        'use strict';
        var self = this;
        fastdom.measure(function () {
            self.src = jQuery(self).data('img-bw');
        }, self);
    }
};

/**
 * Header tracking module.
 *
 * This will re-adust the body padding-top if there is either:
 * - breaking news banner
 *
 * Javascript Module Pattern
 * @see http://www.adequatelygood.com/JavaScript-Module-Pattern-In-Depth.html
 *
 * @name CNN.navigation.headerTracking
 * @namespace
 * @memberOf CNN
 */
window.CNN.navigation.headerTracking = window.CNN.navigation.headerTracking || (function ($, window, document) {
    'use strict';

    var numTracked = 0,
        eventStarted = false;

    /*
     * Increase number of items being tracked
     */
    function increment() {
        numTracked++;
    }

    /*
     * Descrease number of items being tracked
     */
    function decrement() {
        if (numTracked > 0) {
            numTracked--;
        }
    }

    /*
     * Tracking is on
     */
    function isOn() {
        return numTracked > 0;
    }

    /**
     * Set body padding to fixed position nav height with breaking news visible
     * @param {object} e - event object
     */
    function adjustBodyPadding(e) {
        var $body = $('body'),
            /* stores top padding value, used for comparison so that we will always use the bigger value, except on resize */
            currentPaddingTop = parseFloat($body.css('padding-top')),
            /* used to set the new top padding value */
            paddingTop,
            /* calculated height */
            calculatedHeight;

        /* if an event exists, and is of type resize- set the padding reference to 0 so that the page takes the computed value */
        if (e) {
            currentPaddingTop = e.type === 'resize' ? 0 : currentPaddingTop;
        }

        /* is tracking on */
        if (isOn()) {
            calculatedHeight = calculateBodyPadding();

            /* passed simple boolean, now let's check the navigation to see if we're in a special case */
            paddingTop = calculatedHeight > currentPaddingTop ? calculatedHeight : currentPaddingTop;

            /* set body padding */
            $body.css('padding-top', paddingTop);
        }
    }

    /**
     * Adjusts navigation height based on the breaking news-banner and nav bar height
     */
    function adjustNavPanel() {
        var $isPlainNav = $('#nav__plain-header'),
            $navExpanded = $('#nav-expanded'),
            windowHeight = $(window).height(),
            topHeight = calculateBodyPadding(),
            scrollableHeight = windowHeight - topHeight,
            requiredHeight = $navExpanded.css('height', 'auto').height() + topHeight,
            isScrollable = (requiredHeight > windowHeight) ? true : false;

        if ($isPlainNav && ($isPlainNav.length !== 0)) {
            if (isScrollable) {
                $navExpanded.css('height', scrollableHeight);
            } else {
                $navExpanded.css('height', 'auto');
            }
        }
    }

    /*
     * Calculate the body padding
     */
    function calculateBodyPadding() {
        var /* BN element */
            $breakingNews = $(document.getElementById('breaking-news')),
            /* stores nav height appropriate to navigation type (logo used for mobile, full nav header used for desktop) */
            navHeight = calculateNavHeight(),
            /* stores breaking news height, for use with mobile */
            breakingNewsHeight = $breakingNews.is(':hidden') ? 0 : $breakingNews.height();

        return breakingNewsHeight + navHeight;
    }

    function calculateNavHeight() {
        var isDomesticNav = $(document.getElementById('nav__plain-header'));

        if (isDomesticNav && isDomesticNav.length !== 0) {
            return jQuery(document.getElementById('nav')).height();
        } else {
            return $('.js-navigation').outerHeight(true);
        }
    }

    /*
     * Starts tracking of the header height and sets
     */
    function startTracking() {
        /* turn on flag */
        increment();

        /* track resize to adjust */
        if (!eventStarted) {
            $(window).throttleEvent('resize', adjustBodyPadding, 100);
            $(window).throttleEvent('scroll', adjustBodyPadding, 60);
            eventStarted = true;
        }
        /* call initial header adjustment */
        adjustBodyPadding();
    }

    /*
     * Stops tracking of the header height and remove javascript padding
     */
    function endTracking() {
        var $body = $('body');

        decrement();

        fastdom.mutate(function () {
            if (!isOn()) {
                /* remove inline padding-top style from body */
                $('body').attr('style', function (i, style) {
                    return style.replace(/padding-top[^;]+;?/g, '');
                });
            } else {
                $body.css('padding-top', calculateBodyPadding());
            }
        });
    }

    return {
        adjustNav: adjustNavPanel,
        start: startTracking,
        stop: endTracking
    };
})(jQuery, window, document);


(function ($, doc) {
    'use strict';

    if (typeof $.fn.onZonesAndDomReady === 'function') {
        $(doc).onZonesAndDomReady(function () {
            window.CNN.navigation.init();
        });
    } else {
        $(window).load(function () {
            window.CNN.navigation.init();
        });
    }
    $(window).on('resize', function () {
        window.CNN.navigation.headerTracking.adjustNav();
    });
}(jQuery, document));

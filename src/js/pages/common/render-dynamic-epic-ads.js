/* global CNN */

/**
 * Finds all ads with the data attribute of data-ad-branding. These are
 * all considered dynamic/entitlement ads. Ads that are not permanent
 * slots on the page like the standard ads slot. They also do not follow
 * the page level branding value that is set. A unique id attribute is
 * generated for each dynamic ad found. And then an entry is created in
 * the property tracking object window.CNN or window.CNNI (dependent on
 * the edition value) The entry is created in the property called
 * slotTargets which is just an object with a property called spec with
 * the branding value associated with the dyanmic tag.
 *
 * Example:
 * This is what the object would look like for the first dynamic ad tag
 * on a domestic page associated with the branding value ac360.
 *
 * CNN = {
 *     slotTargets: {
 *         ad_ns_dynamic_1: {
 *             spec = 'ac360'
 *         }
 *     }
 * };
 *
 *
 * @param {object} ns - The namespace to use.
 *
 * @param {object} $j - The jQuery library.
 *
 * @param {object} doc - The document object of the page.
 *
 * @param {object} win - The window object of the page.
 *
 */
(function renderDynamicEpicAds(ns, $j, doc, win) {
    'use strict';

    var contentModel = ns.contentModel || {},
        findAndRender;

    if (!ns.Features || ns.Features.enableEpicAds !== true) {
        ns.contentModel = ns.contentModel || {};
        ns.contentModel.entitlementSingletons = [];
        return;
    }

    ns.dynamicEpicAdCount = 0;

    findAndRender = function findAndRender(context) {
        var edition = contentModel.edition || '',
            entitlementIds = contentModel.entitlementSingletons || [],
            emptyEntitlement = {
                id: '',
                scriptName: ''
            },
            maxEntitlements = entitlementIds.length,
            globalEpicTracking,
            jQueryContext = '[data-ad-branding]';

        if (typeof context === 'string') {
            jQueryContext = context + ' ' + jQueryContext;
        }

        if (edition !== '') {
            edition = (edition === 'international') ? 'CNNI' : 'CNN';
            win[edition] = win[edition] || {};
            globalEpicTracking = win[edition];
            globalEpicTracking.slotTargets = globalEpicTracking.slotTargets || {};
            $j(jQueryContext).each(function (k, v) {
                var $v = $j(v),
                    spec = $v.attr('data-ad-branding'),
                    info = (ns.dynamicEpicAdCount < maxEntitlements) ? entitlementIds[ns.dynamicEpicAdCount] : emptyEntitlement,
                    id = info.id,
                    scriptName = info.scriptName;

                ns.dynamicEpicAdCount++;

                if (id !== '' && scriptName !== '') {
                    $v.attr('id', id);
                    globalEpicTracking.slotTargets[id] = globalEpicTracking.slotTargets[id] || {};
                    globalEpicTracking.slotTargets[id].spec = globalEpicTracking.slotTargets[id].spec || spec;
                    $j.getScript(scriptName);
                }
            });
        }
    };

    if (contentModel !== 'undefined' && contentModel !== null && contentModel.lazyLoad) {
        $j(doc).onZonesAndDomReady(function findAndRenderEvent(_event) {
            findAndRender();
        });

        if (contentModel.enableIntelligentLoad) {
            jQuery(document).on('onIZSecondaryLoad', function intelligentZoneForDynamicAds(event, zoneId) {
                findAndRender('#' + zoneId);
            });
        }
    } else {
        $j(doc).ready(function findAndRenderEvent(_event) {
            findAndRender();
        });
    }

}(CNN, jQuery, document, window));


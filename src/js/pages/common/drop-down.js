
(function ($, document) {
    'use strict';

    var $documentBody = $(document.body),
        $dropDowns = $('.js-el-drop-down'),
        $dropDownsH2 = $dropDowns.siblings('.zn-header'),
        $dropDownLists = $dropDowns.siblings('.js-el-drop-down__list__container').children('.js-el-drop-down__list');

    /**
     * Opens up the associated drop down list element.
     *
     * @param {object} event - jQuery event
     * @private
     */
    function _openDropDown(event) {
        var $targetDropDownList = $(event.currentTarget).siblings('.js-el-drop-down__list__container').children('.js-el-drop-down__list');

        $dropDowns.addClass('el-drop-down__arrow-up');
        if ($targetDropDownList.hasClass('el-drop-down__list--open')) {
            _closeDropDown();
        } else {
            $targetDropDownList.addClass('el-drop-down__list--open');
        }
    }

    /**
     * Closes up all drop down list elements on page.
     *
     * @private
     */
    function _closeDropDown() {
        $dropDowns.removeClass('el-drop-down__arrow-up');
        $dropDownLists.removeClass('el-drop-down__list--open');
    }

    /**
     * Setup required event handlers.
     *
     * @private
     */
    function _bindEvents() {
        $dropDownsH2.addClass('zn-header-cursor');
        $dropDowns.add($dropDownsH2).on('click', function dropDownClickHandler(event) {
            /* Stop event from bubbling up and triggering the handler we just attached */
            event.stopPropagation();

            /* Add a one time handler to close drop downs on any other interactions */
            $documentBody.one('click', _closeDropDown);

            _openDropDown(event);
        });

    }

    _bindEvents();
}(jQuery, document));


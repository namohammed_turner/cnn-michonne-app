
/**
 * This loads all the resources needed to put a skinny navigation
 * element on a page of a third party site that falls outside of the
 * cnn.com domain.
 *
 * Resources are loaded asynchronously and as late as possible to
 * minimize the impact this code has on external sites using it.
 *
 * The overall strategy that is employed to make this happen is to use
 * the postMessage API in conjunction with dynamically created iframes
 * for each navigational piece that is added to the page. Here are the
 * general steps taken in this file:
 *
 * 1. Load css resources needed by navigation.
 * 2. Find all .cnn-js-navigation elements and find out which template to
 *    use based on the data-template attribute.
 * 3. Create an iframe with the call out to OCS followed by a # with
 *    the iframe's ID. The iframe allows making the request in a cross
 *    domain fashion.
 * 4. The iframe uses postMessage to notify the calling page (the third
 *    party's site) that it has been loaded and has the rendered content
 *    ready for display.
 * 5. The rendered navigation is added to the .cnn-js-navigation element
 *    that contains the iframe that did the rendering.
 *
 */
var CNN = window.CNN || {};

/**
 * This is an anonyomous function that is executed right away to
 * create the NavigationBoot library.
 *
 * @param {object} ns - The namespace object used by this library.
 * @param {object} win - Represnts the window object of the page.
 * @param {object} doc - Represents the document object of the page.
 */
(function (ns, win, doc) {
    'use strict';

    var $j;

    ns.Host = ns.Host || {
        assetPath: '/asset',
        chrome: '//i.cdn.turner.com/cnn',
        cssPath: '/css',
        domain: '//www.cnn.com'
    };

    if (!ns.hasOwnProperty('NavigationBoot')) {
        ns.NavigationBoot = {
            /**
            * Used to render styling for skinny nav by making calls out to the CDN for the style sheets
            * as well as to set up the listener for the postMessage from the iframe that is created
            * on the page from the ready function that is processing all of the navigation divs that
            * are added to the page.
            */
            getNav: function () {
                $j = win.jQuery;

                ns.AsyncLoader.getStyle(ns.Host.domain + ns.Host.cssPath + '/pages/static-header-skinny.css');

                $j(win).on('message', ns.NavigationBoot.ocsReady);

                if (typeof $j.fn.onZonesAndDomReady === 'function') {
                    $j(doc).onZonesAndDomReady(function () {
                        $j('.cnn-js-navigation').each(ns.NavigationBoot.processNavigationRequest);
                    });
                } else {
                    $j(doc).ready(function () {
                        $j('.cnn-js-navigation').each(ns.NavigationBoot.processNavigationRequest);
                    });
                }
            },
            /**
             * This processes a new message from any page sending a
             * message to the external site using the postMessage API.
             * If the request is coming from our origin then we pull
             * the message out of the "output" wrapper div and
             * processes any additional script that needs to be brought
             * in using the data-script attribute. The data-id
             * attribute tells the function which iframe this content
             * came from. If the ID is not found then nothing is added
             * to the page.
             *
             * @param {object} e -- Post Message Event wrapped in a
             * jQuery event object.
             */
            ocsReady: function (e) {
                var data,
                    /* CCND-298 */
                    /* origin, */
                    mPos,
                    postMessageEvent = e.originalEvent,
                    postMessageEventDomain = postMessageEvent.origin.replace(/^(http|https):/, ''),
                    template,
                    templateId,
                    additionalJS;

                if (ns.Host.domain.length === 0) {
                    /* No request host, so pre-rendered (probably error page) */
                    mPos = postMessageEvent.origin.indexOf(ns.Host.main);
                    if (mPos < 7 || mPos > 8) {
                        return;
                    }
                } else if (postMessageEventDomain !== ns.Host.domain) {
                    return;
                }

                data = postMessageEvent.data;
                template = $j(data);

                if (template.attr('id') === 'output') {
                    templateId = template.attr('data-id');
                    additionalJS = template.attr('data-script');

                    $j(doc.getElementById(templateId)).closest('.cnn-js-navigation').append(template.html());
                    if (additionalJS) {
                        ns.AsyncLoader.getScript(additionalJS);
                    }
                }
            },
            /**
             * Used to process a specific navigation widget that is on
             * a page. For each widget it creates an iframe that makes
             * an OCS call for the template. If the template in the
             * data-template attribute is not found then no iframe is
             * created. Two other data- attributes are also processed
             * and reserved for future use. One is edition to make a
             * call out to domestic or international the second is
             * data-association which will allow the widget to say
             * what CNN URL it is tied. For example if the external
             * page is partnering with CNN to bring in extra health
             * information then its data-association might be /health.
             *
             * @param {integer} i -- The index of the current widget
             * being processed in array of widgets.
             * @param {object} v -- The DOM object of the widget being
             * processed.
             */
            processNavigationRequest: function (i, v) {
                var $v = $j(v),
                    /* association,
                    edition, */
                    template,
                    templatesMap = {
                        header: ns.Host.domain + '/data/ocs/section/index.html:*/views/pages/common/static-header-skinny.html'
                    },
                    containerId = 'cnn-template-iframe-' + i;

                /* never used...
                edition = $v.attr('data-edition') || 'domestic';
                association = $v.attr('data-association') || '';
                */
                template = $v.attr('data-template') || '';

                if (templatesMap.hasOwnProperty(template)) {
                    $v.append('<iframe id="' + containerId + '" src="' + templatesMap[template] + '#' + containerId + '" style="display:none;"></iframe>');
                }
            },
            /**
             * Kicks off the execution of the widget finding and
             * processing; pull in jQuery if needed and pulls in needed
             * stylesheets.
             */
            run: function () {
                if (!win.jQuery) {
                    ns.AsyncLoader.getScript(ns.Host.assetPath + '/js/vendor/jquery.min.js', ns.NavigationBoot.getNav);
                } else {
                    ns.NavigationBoot.getNav();
                }
            }
        };

        ns.NavigationBoot.run();
    }
}(CNN, window, document));

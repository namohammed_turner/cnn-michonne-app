
var CNN = window.CNN || {};

/**
 * This is an anonyomous function that is executed right away to create
 * the AsyncLoader library.
 *
 * @param {object} ns - The namespace object used by this library.
 * @param {object} doc - Represents the document object of the page.
 */
(function (ns, doc) {
    'use strict';

    ns = ns || {};

    if (!ns.hasOwnProperty('AsyncLoader')) {
        ns.AsyncLoader = {
            /**
             * Loads scripts asynchronously.
             *
             * Usage:
             *
             *     getScript('script.js', function() {
             *         console.log('script.js has been loaded!');
             *     });
             *
             * @param {string} url - The script to load.
             * @param {function} success - *optional* The function to
             * be notified once the script has been downloaded and parsed.
             */
            getScript: function (url, success) {
                var script,
                    head,
                    done;

                script = doc.createElement('script');
                script.src = url;
                head = doc.getElementsByTagName('head')[0];

                if (success) {
                    done = false;
                    script.onload = script.onreadystatechange = function () {
                        if (!done && (!this.readyState || this.readyState === 'loaded' || this.readyState === 'complete')) {
                            done = true;
                            success();
                            script.onload = script.onreadystatechange = null;
                            head.removeChild(script);
                        }
                    };
                }
                head.appendChild(script);
            },
            /**
             * Loads a stylesheet asynchronously
             *
             * Usage:
             *
             *     // Loads a style sheet.
             *     getStyle('stylesheet-to-load.css');
             *
             * @param {string} url - The stylesheet to load.
             * @param {object} _options - Flags that change how the
             * style is placed on the page.
             */
            getStyle: function (url, _options) {
                var link = doc.createElement('link'),
                    addLink = true;

                link.type = 'text/css';
                link.rel = 'stylesheet';
                link.href = url;

                if (addLink) {
                    doc.getElementsByTagName('head')[0].appendChild(link);
                }
            }
        };
    }
}(CNN, document));

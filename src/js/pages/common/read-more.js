/* global CNN, fastdom, OBR */

(function ($, win, doc) {
    'use strict';

    /**
     * Hide the Read More section
     */
    function hideReadMore() {
        var $readAllElements = $('.zn-body__read-all');

        if ($readAllElements.length > 0) {
            fastdom.mutate(function () {
                $('body').addClass('pg-show-read-all');
                /* Process responsive images in read all content */
                CNN.ResponsiveImages.processChildrenImages($readAllElements[0]);
            });
        }
    }

    /**
     * For those Outbrain sections that are only loaded if a certain condition is met.
     */
    function loadConditionalOutbrain() {
        var $conditionalNodes = $('.conditionalload');

        if ($conditionalNodes.length > 0 && typeof OBR !== 'undefined' && typeof OBR.extern !== 'undefined') {

            fastdom.mutate(function () {
                $.each($conditionalNodes, function (idx, node) {
                    $(node).removeClass('conditionalload').addClass('OUTBRAIN');

                    try {
                        OBR.extern.researchWidget();
                    } catch (err) {
                        console.log('Failed to load Outbrain widget ID', node.attr('data-widget-id'));
                    }
                });

                $('.zn-body__read-more-outbrain').css('display', 'block');
            });
        }
    }

    /**
     * For those Outbrain sections that are to be removed if a certain condition is met.
     */
    function removeConditionalOutbrain() {
        var $conditionalNodes = $('.conditionalremove');

        if ($conditionalNodes.length > 0) {
            $conditionalNodes.hide();
            $conditionalNodes.removeClass('conditionalremove');
        }
    }

    /**
     * read more button on article leaf that hides long content on mobile view
    */
    function readMoreOnReady() {
        /* check referrer */
        var urlPattern = /(.+:\/\/)?([^\/]+)(\/.*)*/i,
            docReferrer = doc.referrer || '',
            urlArray = urlPattern.exec(docReferrer),
            $readMoreButton = $('#js-body-read-more');
        if ($.isArray(urlArray) && urlArray[2] && (urlArray[2].indexOf('.cnn.com') >= 0)) {
            hideReadMore();
        } else {
            fastdom.measure(function () {
                if ($readMoreButton.length > 0) {
                    /* attach click event to read more button */
                    $('#js-body-read-more').click(function readMoreClick() {
                        if (typeof win.trackMetrics === 'function') {
                            try {
                                win.trackMetrics({
                                    type: 'readmore-click',
                                    data: {
                                        interaction: 'read more'
                                    }
                                });
                            } catch (e) {}
                        }

                        hideReadMore();
                        if (CNN.Features && CNN.Features.enableOutbrain) {
                            removeConditionalOutbrain();
                            loadConditionalOutbrain();
                        }
                    });
                } else {
                    if (CNN.Features && CNN.Features.enableOutbrain) {
                        removeConditionalOutbrain();
                    }
                }
            });
        }
    }

    if (typeof $.fn.onZonesAndDomReady === 'function') {
        $(doc).onZonesAndDomReady(function () {
            readMoreOnReady();
        });
    } else {
        $(doc).ready(function () {
            readMoreOnReady();
        });
    }
}(jQuery, window, document));

/* global CNN */

/*
 * This file handles all of the plain Navigation interaction.
 */

CNN.PlainNavigation = {
    breakpoint: 1120,
    pickerBreakpoint: 768,

    init: function () {
        'use strict';

        this.$editionItems = jQuery('.js-edition-item');
        this.$flyoutFooterEditionItems = jQuery('.js-nav-flyout-footer-edition-list-item');
        this.$hamburgerMenu = jQuery(document.getElementById('menu'));
        this.$articleTitle = jQuery(document.getElementById('js-nav-section-article-title'));
        this.$sectionExpandIcon = jQuery('.js-nav-section-name');
        this.$searchButton = jQuery(document.getElementById('search-button'));
        this.$searchSubmitButton = jQuery(document.getElementById('submit-button'));
        this.$editionPicker = jQuery('.edition-picker__current-edition');

        this.detectSubNav();

        if (jQuery('body').hasClass('pg-leaf') === true) {
            jQuery(window).on('scroll', function () {
                jQuery('body').toggleClass('page-scrolled', (jQuery(window).scrollTop() > 100));
            });

            if (CNN.contentModel && CNN.contentModel.title) {
                this.$articleTitle.html(CNN.contentModel.title);
            }
        }

        this.$editionItems.on('click', this.changeEdition);
        this.$flyoutFooterEditionItems.on('click', this.changeEdition);
        this.$hamburgerMenu.on('click', this.hideEdition);
        this.$searchSubmitButton.on('click', this.searchSubmit);
        this.$sectionExpandIcon.on('click', this.sectionRootExpand);
        this.$searchButton.on('click', function searchDetect(event) {
            if (window.innerWidth < CNN.PlainNavigation.breakpoint) {
                CNN.PlainNavigation.searchSubmit(event);
            } else {
                CNN.PlainNavigation.searchExpand(event);
            }
        });
        this.$editionPicker.on('click', this.toggleEdition);
    },

    /**
    * Hide the editon list when open the menu expanded view (hamburger click).
    */
    hideEdition: function () {
        'use strict';

        jQuery('.edition-picker__current-edition').removeClass('nav-section--expanded');
        jQuery('.js-nav-editions').removeClass('nav-section--expanded');
    },

    /**
    * Expand the sub menus when clicking the '+' icon on section header.
    *
    * @param {Object} event - event object
    */
    sectionRootExpand: function (event) {
        'use strict';

        if (window.innerWidth >= CNN.PlainNavigation.breakpoint) {
            return;
        }

        event.preventDefault();

        jQuery(this).parent().toggleClass('nav-section--expanded');
        if (jQuery(this).parent().hasClass('nav-section--expanded') === true) {
            jQuery(document.getElementById('nav-section-submenu')).addClass('nav-section--expanded');
        } else {
            jQuery(document.getElementById('nav-section-submenu')).removeClass('nav-section--expanded');
        }
    },

    /**
    * Trigger the search in the Nav Header.
    *
    * @param {Object} event - event object
    */
    searchSubmit: function (event) {
        'use strict';

        event.preventDefault();

        jQuery(document.getElementById('search-form')).submit();
    },

    /**
    * Expand the search text field when clicking the search button.
    *
    * @param {Object} event - event object
    */
    searchExpand: function (event) {
        'use strict';

        event.preventDefault();

        jQuery('body').toggleClass('search-expanded');
        jQuery(document.getElementById('search-input-field')).focus();
    },

    /**
    * Open the edition picker on Nav header.
    *
    * @param {Object} event - event object
    */
    toggleEdition: function (event) {
        'use strict';

        if (event) {
            event.preventDefault();
        }

        if ((jQuery('body').hasClass('nav-open') === true) && (window.innerWidth >= CNN.PlainNavigation.pickerBreakpoint)) {
            return;
        }

        jQuery('.edition-picker__current-edition').toggleClass('nav-section--expanded');
        jQuery('.js-nav-editions').toggleClass('nav-section--expanded');
    },

    /**
    * Detect the section name and change header to show the section Nav view.
    */
    detectSubNav: function () {
        'use strict';

        var sectionName = CNN.contentModel ? CNN.contentModel.sectionName : '',
            domesticSection = jQuery('body').hasClass('pg-' + sectionName),
            editionSection = jQuery('body').hasClass('pg-intl_' + sectionName);
        if ((domesticSection === true || editionSection === true) && (!(sectionName === 'homepage' || sectionName === 'intl_homepage'))) {
            jQuery(document.getElementById('nav')).addClass(sectionName);
        }
    },

    /**
    * Redirects the page to the selected edition.
    * This function will also add tracking query
    * parameters to the redirect URL.
    *
    * @param {object} event - jQuery event
    * @param {object} targetChoice - jQuery DOM element for choice
    * @public
    */
    changeEdition: function (event, targetChoice) {
        'use strict';

        var targetItem = event ? jQuery(event.currentTarget) : targetChoice,
            editionpicker = targetItem.parent(),
            dataElement = editionpicker.parent(),
            trackingRef = dataElement.data('location') + '_' + dataElement.data('analytics'),
            currentEdition = CNN.Edition,
            targetEdition = targetItem.data('value'),
            editionUrl = targetEdition + '?hpt=' + trackingRef,
            targetItemType = targetItem.data('type');

        if (CNN.contentModel && CNN.contentModel.edition) {
            currentEdition = (CNN.contentModel.edition === 'international') ? 'edition' : 'www';
        }

        if (currentEdition.indexOf(targetItemType) === -1) {
            window.location = editionUrl;
        }
    }
};

(function ($, doc) {
    'use strict';

    if (CNN.contentModel && CNN.contentModel.layout === 'error') {
        $(window).load(function () {
            CNN.PlainNavigation.init();
            CNN.EditionPickerPrefs.init();
            CNN.EditionPrefs.indicateCurrentEdition();
        });
    } else {
        if (typeof $.fn.onZonesAndDomReady === 'function') {
            $(doc).onZonesAndDomReady(function () {
                CNN.PlainNavigation.init();
            });
        } else {
            $(window).load(function () {
                CNN.PlainNavigation.init();
            });
        }
    }
}(jQuery, document));

/* global CNN */

/*  ------------
    Share Rail

    @description:   Create a windowdow onScroll event to manage the Share Rail on
                    leaf / article pages.
    -------------
*/

jQuery(function shareRailOnReady() {
    'use strict';

    /* Get only the first share rail module. */
    var $shareRail = jQuery('.js-share-rail').first(),
        $mainShareBar,
        $lastShareBar;

    /* Only proceed if a share rail module exists on the page. */
    if ($shareRail.length > 0) {

        /* Find the first and last sharebar on the page. */
        $mainShareBar = jQuery('.js-share-rail-top').first();

        /* For no-rail layout pages */
        if (jQuery('.pg-no-rail').length > 0) {
            $lastShareBar = jQuery('.l-footer').last();
        } else {
            $lastShareBar = jQuery('[itemprop="articleBody"] .js-share-rail-top').last();
        }

        /* If no sharebar exists, then null both of the previous vars. */
        if ($mainShareBar.length === 0) {
            $mainShareBar = null;
        }

        if (!$mainShareBar || $mainShareBar === $lastShareBar) {
            $lastShareBar = null;
        }

        /* Only proceed if there is at lease one sharebar on the page. */
        if ($mainShareBar) {
            /* Create scroll event.*/
            /* $.proxy() to scope the function to 'this' without hacks. */
            jQuery(window).scroll(jQuery.proxy(function (_e) {
                /* Calculate in and out positions based on their offset() positions. */
                var shareRailIn = $mainShareBar.first().offset().top + $mainShareBar.height(),
                    $shareRailOutFirst = $lastShareBar ? $lastShareBar.first() : null,
                    shareRailOutOffset = $shareRailOutFirst ? $shareRailOutFirst.offset() : null,
                    shareRailOut = shareRailOutOffset ? shareRailOutOffset.top : null;

                /* Check if the page's scroll has reached either of these thresholds. */
                if (CNN.CurrentSize.getClientScrollTop() < shareRailIn ||
                    (shareRailOut && CNN.CurrentSize.getClientScrollTop() + CNN.CurrentSize.getClientHeight()) > shareRailOut) {

                    /* If so, check to see if the 'show' class exists, and remove it
                       to hide the share rail. */
                    if ($shareRail.hasClass('show')) {
                        $shareRail.removeClass('show');
                    }

                } else {

                    /* If not, check to see if the 'show' class exists, and add it
                       to show the share rail. */
                    if (!$shareRail.hasClass('show')) {
                        $shareRail.addClass('show');
                    }

                }
            }, this));
        }
    }

});

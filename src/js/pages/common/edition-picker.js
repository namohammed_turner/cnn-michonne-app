/* global CNN */

CNN.EditionPicker = CNN.EditionPicker || (function ($, window) {
    'use strict';

    var $body = $('body'),
        $editionPicker = $('.edition-picker');

    /**
     * Stop event from bubbling up and triggering
     * the body click close picker.
     *
     * @param {object} event - jQuery event
     * @private
     */
    function _stopPropagation(event) {
        event.stopPropagation();
    }

    /**
     * Setup required event handlers.
     * CNN.EditionPrefs.showPreferences is defined in edition-preferences.js
     * @private
     */
    function _bindEvents() {
        $editionPicker
            .on('click', _stopPropagation)
            .on('click', '.list .item--edition', changeEdition)
            .on('click', '.current', togglePicker)
            .on('click', '.item--select', CNN.EditionPrefs.showPreferences);
    }

    /**
     * Redirects the page to the selected edition.
     * This function will also add tracking query
     * parameters to the redirect URL.
     *
     * @param {object} event - jQuery event
     * @param {object} $targetChoice - jQuery DOM element for choice
     * @public
     */
    function changeEdition(event, $targetChoice) {
        var $targetItem = event ? $(event.currentTarget) : $targetChoice,
            $targetInput = $targetItem.children('input'),
            $targetPickerList = $targetItem.closest('.list'),
            trackingRef = $targetPickerList.data('location') + '_' + $targetPickerList.data('analytics'),
            currentEdition = CNN.Edition,
            targetEdition = $targetItem.data('value'),
            editionUrl = targetEdition + '?hpt=' + trackingRef;

        if (CNN.contentModel && CNN.contentModel.edition) {
            currentEdition = (CNN.contentModel.edition === 'international') ? 'edition' : 'www';
        }

        /* Only redirect if we are not on this edition already */
        if (currentEdition.indexOf($targetInput.val()) === -1) {
            window.location = editionUrl;
        }
    }

    /**
     * Toggle the opening of the edition picker
     *
     * @public
     * @param {object} event - event object
     */
    function togglePicker(event) {
        var $targetPicker = event ? $(event.currentTarget).closest('.edition-picker') : $editionPicker;

        if ($targetPicker.hasClass('open')) {
            $targetPicker.removeClass('open');
            $body.off('click', togglePicker);
        } else {
            $editionPicker.removeClass('open');
            $targetPicker.addClass('open');
            $body.one('click', togglePicker);
        }
    }

    $(document).onZonesAndDomReady(_bindEvents);

    return {
        changeEdition: changeEdition,
        togglePicker: togglePicker
    };

}(jQuery, window));


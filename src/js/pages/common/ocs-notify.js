
/**
 * This is an anonyomous function that is executed right away to
 * send a message to the parent window with the content that was
 * loaded in the iframe window.
 *
 * @param {object} win - Represnts the window object of the page.
 * @param {object} doc - Represents the document object of the page.
 */
(function (win, doc) {
    'use strict';

    var notifyParent;

    if (win !== win.top && win !== win.parent) {
        notifyParent = function () {
            var container = win.location.hash,
                content = doc.getElementById('content'),
                additionalJS;

            if (container !== '') {
                container = container.substr(1);
                if (content.getAttribute('data-script')) {
                    additionalJS = ' data-script="' + content.getAttribute('data-script') + '"';
                }

                win.parent.postMessage('<div id="output" data-id="' + container + '"' + additionalJS + '>' + content.innerHTML + '</div>', '*');
            }
        };

        if (doc.addEventListener) {
            doc.addEventListener('DOMContentLoaded', notifyParent, false);
        } else {
            doc.attachEvent('onreadystatechange', notifyParent);
        }
    }
} (window, document));


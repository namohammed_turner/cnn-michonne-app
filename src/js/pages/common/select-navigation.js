/* global CNN */

/**
 * Reuseable function for using dropdown menus as navigation. Inappropriate, but
 * a good crutch.
 *
 * @param   {String}  selector  - The selector class or ID string.
 * @param   {Boolean} isHash    - Is the target location an anchor on the page?
 *
 * @name CNN.VideoAjax.LiveStream
 * @namespace
 * @memberOf CNN
 *
 * @example
 * CNN.selectNavigation('.js-el-faceted__select', true);
 */
CNN.selectNavigation = function selectNavigation(selector, isHash) {
    'use strict';

    /* Error check */
    if (typeof selector === 'undefined') {
        throw 'selector is not defined';
    }

    /* Set default isHash */
    if (isHash !== true) {
        isHash = false;
    }

    /* Meat and Potatoes. Run this function when the selector changes. */
    jQuery(selector).on('change', function onSelectNavigationChange() {
        var val = jQuery(this).find(':selected').val();

        if (isHash === false) {
            window.location = val;
        } else {
            window.location.hash = '#' + val;
        }
    });
};

/* Wait until document.ready() before calling. */
jQuery(function () {
    'use strict';

    /* Faceted search tool-cards */
    CNN.selectNavigation('.js-el-faceted__select', true);

    /* Navigation Subsections */
    CNN.selectNavigation('.js-pg-header__subsections--select select');
});


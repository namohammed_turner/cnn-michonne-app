/* global CNN, Bloodhound, store, dust, moment, trackMetrics, Cipher */

CNN.WeatherConfig = CNN.WeatherConfig || {};
CNN.MetaRefreshRate = CNN.MetaRefreshRate || 1800;

(function ($, CNN, store, dust, moment) {
    'use strict';

    function weatherService() {
        var temperatureType = null,
            settingsTempType = null,
            fahrenheit = 'fahrenheit',
            celsius = 'celsius',
            defaultUsLocCode = 'USNY9472',
            defaultIntlLocCode = 'UKXX0085',
            localStorageEnabled = store.enabled ? true : false,
            isEnabled = CNN.Features.enableWeather,
            isEnabledInFooter = CNN.WeatherConfig.enableInFooter,
            isEnabledInHeader = CNN.WeatherConfig.enableInHeader,
            forecastQueryParams = function (customLocation) {
                var params = '/json/';
                /* params = /mode/zipCode/celsius/locCode */
                if (customLocation === null) {
                    switch (CNN.contentModel.edition) {
                    /* JSHint made us indent this way. */
                    case 'domestic':
                        params += '10001/false/' + defaultUsLocCode;
                        temperatureType = fahrenheit;
                        break;
                    case 'international':
                        params += '336736767676/true/' + defaultIntlLocCode;
                        temperatureType = celsius;
                        break;
                    default:
                        params += '10007/false/' + defaultUsLocCode;
                        temperatureType = fahrenheit;
                        break;
                    }
                } else {
                    params += customLocation.zip + '/' +
                        (customLocation.international ? 'true' : 'false') + '/' +
                        customLocation.locCode;
                    if (temperatureType === null) {
                        temperatureType = customLocation.international ? celsius : fahrenheit;
                    }
                }
                if (localStorageKeyGet('_weather-temp-type')) {
                    temperatureType = localStorageKeyGet('_weather-temp-type');
                    if (temperatureType === null) {
                        temperatureType = customLocation.international ? celsius : fahrenheit;
                    }
                }
                toggleTempType(temperatureType);
                return params;
            },
            setTemperatureType = function () {
                if (localStorageEnabled && (settingsTempType !== null)) {
                    localStorageKeySet('_weather-temp-type', settingsTempType);
                    settingsTempType = null;
                }
            },
            /* Get a key from local storage */
            localStorageKeyGet = function (key) {
                return store.get(key);
            },
            /* Set a key/value pair in local storage */
            localStorageKeySet = function (key, value) {
                store.set(key, value);
            },
            replaceHTMLEntities = function (str) {
                return String(str).replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;');
            },
            /* Return an array of local map paths */
            localMapsRegion = function (weatherData) {
                var weatherLocation = weatherData.location,
                    weatherLocationRegion = weatherData.location.region,
                    maps = [],
                    state = weatherLocation.stateOrCountry.toLowerCase();
                /* Based on the region returned from the forecast lookup, return the right set of maps */
                switch (weatherLocationRegion) {
                /* JSHint made us indent this way */
                case 'af': /* africa */
                    maps = [
                        CNN.Host.chrome + '/.element/img/3.0/weather/maps/satisafrnf.gif',
                        CNN.Host.chrome + '/.element/img/3.0/weather/maps/africa_highs.jpg',
                        CNN.Host.chrome + '/.element/img/3.0/weather/maps/africa_forecast.jpg'
                    ];
                    break;
                case 'as': /* asia */
                    maps = [
                        CNN.Host.chrome + '/.element/img/3.0/weather/maps/satusasiaf.gif',
                        CNN.Host.chrome + '/.element/img/3.0/weather/maps/asia_highs.jpg',
                        CNN.Host.chrome + '/.element/img/3.0/weather/maps/asia_forecast.jpg'
                    ];
                    break;
                case 'au': /* austrailia */
                    maps = [
                        CNN.Host.chrome + '/.element/img/3.0/weather/maps/satusaustf.gif',
                        CNN.Host.chrome + '/.element/img/3.0/weather/maps/australia_highs.jpg',
                        CNN.Host.chrome + '/.element/img/3.0/weather/maps/australia_forecast.jpg'
                    ];
                    break;
                case 'ca': /* central america */
                    maps = [
                        CNN.Host.chrome + '/.element/img/3.0/weather/maps/satuscamf.gif',
                        CNN.Host.chrome + '/.element/img/3.0/weather/maps/camerica_highs.jpg',
                        CNN.Host.chrome + '/.element/img/3.0/weather/maps/camerica_forecast.jpg'
                    ];
                    break;
                case 'eu': /* europe */
                    maps = [
                        CNN.Host.chrome + '/.element/img/3.0/weather/maps/satusneurf.gif',
                        CNN.Host.chrome + '/.element/img/3.0/weather/maps/europe_highs.jpg',
                        CNN.Host.chrome + '/.element/img/3.0/weather/maps/europe_forecast.jpg'
                    ];
                    break;
                case 'me': /* middle east */
                    maps = [
                        CNN.Host.chrome + '/.element/img/3.0/weather/maps/satusmidef.gif',
                        CNN.Host.chrome + '/.element/img/3.0/weather/maps/mideast_highs.jpg',
                        CNN.Host.chrome + '/.element/img/3.0/weather/maps/mideast_forecast.jpg'
                    ];
                    break;
                case 'sa': /* south america */
                    maps = [
                        CNN.Host.chrome + '/.element/img/3.0/weather/maps/satuscsamf.gif',
                        CNN.Host.chrome + '/.element/img/3.0/weather/maps/samerica_highs.jpg',
                        CNN.Host.chrome + '/.element/img/3.0/weather/maps/samerica_forecast.jpg'
                    ];
                    break;
                case 'na':
                    maps = [
                        CNN.Host.chrome + '/.element/img/3.0/weather/maps/satcanada_.gif',
                        CNN.Host.chrome + '/.element/img/3.0/weather/maps/namerica_highs.jpg',
                        CNN.Host.chrome + '/.element/img/3.0/weather/maps/namerica_forecast.jpg'
                    ];
                    break;
                default:
                    if (weatherLocation.international) {
                        /* if none of the above international region cases are triggered,
                            and yet international is true, we don't have an image */
                        maps = [
                        ];
                    } else {
                        /* Fixes issues where we don't have a graphic for a state. */
                        switch (state) {
                        case 'ma' :
                        case 'ri' :
                            state = 'ct';
                            break;
                        case 'vt' :
                            state = 'nh';
                            break;
                        case 'dc' :
                        case 'de' :
                        case 'md' :
                        case 'wv' :
                            state = 'va';
                            break;
                        case 'nc' :
                            state = 'sc';
                            break;
                        case 'me':
                            state = 'nh';
                            break;
                        case 'nj':
                            state = 'pa';
                            break;
                        }
                        maps = [
                            CNN.Host.chrome + '/.element/img/3.0/weather/maps/nxrd_' + state + '_.gif',
                            CNN.Host.chrome + '/.element/img/3.0/weather/maps/sat' + state + '_.gif',
                            CNN.Host.chrome + '/.element/img/3.0/weather/maps/namerica_highs.jpg',
                            CNN.Host.chrome + '/.element/img/3.0/weather/maps/namerica_forecast.jpg'
                        ];
                    }
                }

                return maps;
            },
            convertToCelsius = function (temp) {
                /* If temp is NaN return it, else convert to Celsius from Fahrenheit */
                return isNaN(Number(temp)) ? temp : Math.round(((temp - 32) * 5) / 9);
            },
            convertToFahrenheit = function (temp) {
                /* If temp is NaN return it, else convert to Fahrenheit from Celsius */
                return isNaN(Number(temp)) ? temp : Math.round(((temp * 9) / 5) + 32);
            },
            convertTempHandler = function (scale, items, originalTempType, scaleStrings) {
                var original, converted;
                $.each(items, function (index, item) {
                    if ($(item).attr(scale)) {
                        $(item)
                            .html($(item).attr(scale))
                            .attr('data-temptype', scale);
                        $(scaleStrings[index]).html(scale === celsius ? 'C' : 'F');
                    } else {
                        original = $(item).html();
                        converted = scale === celsius ? convertToCelsius(original) : convertToFahrenheit(original);
                        $(item)
                            .html(converted)
                            .attr(originalTempType, original)
                            .attr('data-temptype', scale)
                            .attr(scale, converted);
                        $(scaleStrings[index]).html(scale === celsius ? 'C' : 'F');
                    }
                });
            },
            /* Renders the title for the current and impending weather zone */
            renderTitles = function (weatherData) {
                return $.Deferred(function (dfr) {
                    var $headerText = $('.zn-local-forecast .zn-header__text'),
                        $headerStripes = $('.zn-local-forecast .zn-header__text .zn-header__stripes').clone(),
                        weatherLocation = weatherData.location;
                    $headerText.html(weatherLocation.city + ', ' + weatherLocation.stateOrCountry).append($headerStripes);
                    dfr.resolve(weatherData);
                }).promise();
            },
            /* Render the Current conditions weather card */
            renderCurrentConditions = function (weatherData) {
                return (!isEnabled) ? undefined : $.Deferred(function (dfr) {
                    /* Use currentConditions to render the primary Forecast card */
                    var $currentConditions = $('article.cd--tool__weather-current'),
                        currentWeather = weatherData.currentConditions,
                        currentConditionsObject = {
                            weatherBackgroundClass: currentWeather.icon.substring(0, 2),
                            temperatureCurrent: currentWeather.temperature,
                            temperatureHigh: weatherData.forecast.days[0].high,
                            temperatureLow: weatherData.forecast.days[0].low,
                            forecastDescription: currentWeather.description,
                            feelsLikeTemperature: currentWeather.feelsLikeTemperature,
                            humidity: currentWeather.humidity_S,
                            wind: currentWeather.wind,
                            sunriseTime: moment(currentWeather.sunriseTime).format('h:mm a'),
                            sunsetTime: moment(currentWeather.sunsetTime).format('h:mm a'),
                            barometer: currentWeather.barometricPressure,
                            timestamp: moment(currentWeather.lastUpdated.time).format('h:mm A'),
                            temperatureType: weatherData.location.international ? celsius : fahrenheit
                        };
                    dust.render('views/cards/tool/weather-current', currentConditionsObject, function (err, out) {
                        if (err) { return; }

                        $currentConditions.empty().append(out);
                    });

                    dfr.resolve(weatherData);
                });
            },
            /* Render the Impending Weather cards */
            renderImpendingForecast = function (weatherData) {
                return (!isEnabled) ? undefined : $.Deferred(function (dfr) {
                    var $impendingForecastCards = $('article.cd--tool__weather-impending'),
                        weatherForecast = weatherData.forecast.days,
                        weatherForecastToday = weatherForecast[10],
                        weatherForecastTomorrow = weatherForecast[1],
                        impendingWeather = [];

                    impendingWeather.push({
                        timeRange: 'tonight',
                        weatherBackgroundClass: weatherForecastToday.icon.substring(0, 2),
                        temperatureLow: weatherForecastToday.low,
                        description: weatherForecastToday.description,
                        temperatureType: weatherData.location.international ? celsius : fahrenheit
                    });
                    impendingWeather.push({
                        timeRange: 'tomorrow',
                        weatherBackgroundClass: weatherForecastTomorrow.icon.substring(0, 2),
                        temperatureHigh: weatherForecastTomorrow.high,
                        temperatureLow: weatherForecastTomorrow.low,
                        description: weatherForecastTomorrow.description,
                        temperatureType: weatherData.location.international ? celsius : fahrenheit
                    });
                    $.each($impendingForecastCards, function (index, item) {
                        dust.render('views/cards/tool/weather-impending', impendingWeather[index], function (err, out) {
                            if (err) { return; }

                            $(item).empty().append(out);
                        });
                    });
                    dfr.resolve(weatherData);
                });
            },
            /* Render the eight extended forecast cards */
            renderExtendedForecast = function (weatherData) {
                return (!isEnabled) ? undefined : $.Deferred(function (dfr) {
                    var $extendedForecastCards = $('article.cd--tool__weather-conditions-forecast'),
                        forecastDaysUnsliced = weatherData.forecast.days,
                        forecastDays = forecastDaysUnsliced.slice(1, 9),
                        extendedForecasts = [];

                    $.each(forecastDays, function (index, item) {
                        extendedForecasts.push({
                            temperatureHigh: item.high,
                            temperatureLow: item.low,
                            weatherBackgroundClass: item.icon.substring(0, 2),
                            description: item.description,
                            date: moment(item.dayDate.time).format('dddd, MMMM Do'),
                            temperatureType: weatherData.location.international ? celsius : fahrenheit
                        });
                    });

                    $.each($extendedForecastCards, function (index, item) {
                        dust.render('views/cards/tool/weather-conditions-forecast', extendedForecasts[index], function (err, out) {
                            if (err) { return; }

                            $(item).empty().append(out);
                        });
                    });
                    dfr.resolve(weatherData);
                });
            },
            /* Render Local Maps */
            renderLocalMaps = function (weatherData) {
                return $.Deferred(function (dfr) {
                    /* Get our maps array */
                    var imagesArray = localMapsRegion(weatherData),
                        $localMaps = $('.zn-local-maps .cd__content'),
                        mapClasses = ['radar', 'satellite', 'temperature', 'forecast'],
                        mapTitles = ['Current Radar', 'Satellite', 'Temperature', 'Forecast'],
                        mapToModify;

                    /* If we're showing an international city, no radar, hide this card */
                    if (imagesArray.length === 3) {
                        mapToModify = $localMaps.eq(1);

                        mapToModify.closest('.cn__column').hide();
                        /* keep the map from being added to the lightbox */
                        mapToModify.find('[data-lightbox]').removeAttr('data-lightbox');
                        mapClasses.shift();
                    } else if (imagesArray.length === 0) {
                        /* if we have no maps, hide all cards */
                        $localMaps.closest('.cn__column').hide();
                    } else {
                        $localMaps.closest('.cn__column').show();
                    }

                    /* Apply the images */
                    $.each($localMaps.filter(':visible'), function (index, item) {
                        $(item).html($('<div />', {
                            'class': 'weather__localmap weather__localmap--' + mapClasses[index],
                            html: '<a href="' + imagesArray[index] + '" data-lightbox="weatherMaps" data-title="' +
                            mapTitles[index] + '"><div class="js-weather_map_img_wrapper weather_map_img_wrapper"><img src="' +
                            imagesArray[index] + '"></div></a>'
                        }));
                    });

                    $('.js-weather_map_img_wrapper').click(function () {
                        $('#lightbox').animate({top:'10%'}, 750);
                    });

                    dfr.resolve(weatherData);
                });
            },
            prepRenderArray = function (storedLocationWeather) {
                return {
                    locationAttr: storedLocationWeather.location.zip + ',' +
                        storedLocationWeather.location.international + ',' +
                        storedLocationWeather.location.locCode,
                    locCode: storedLocationWeather.location.locCode,
                    city: storedLocationWeather.location.city,
                    state: storedLocationWeather.location.stateOrCountry,
                    iconNumber: storedLocationWeather.currentConditions.icon,
                    temperature: storedLocationWeather.currentConditions.temperature_S,
                    temperatureType: storedLocationWeather.location.international ? celsius : fahrenheit
                };
            },
            /* Render the header and footer weather items */
            renderWeather = function (weatherData) {

                return (!isEnabled) ? undefined : $.Deferred(function (dfr) {

                    var $weatherHeader = $(document.getElementById('weather-header')),
                        $weatherFooter = $('.l-footer__tools--weather'),
                        weatherCurrentConditions = weatherData.currentConditions,
                        weatherLocation = weatherData.location,
                        headerFooterRenderObject = {
                            backgroundClass: weatherCurrentConditions.icon,
                            weatherIcon: CNN.Host.chrome + '/.e/img/3.0/weather/weatherIcon_' + weatherCurrentConditions.icon + '.png',
                            weatherClass: 'weather-' + weatherCurrentConditions.icon,
                            description: weatherCurrentConditions.description,
                            locationCity: weatherLocation.city,
                            locationState: weatherLocation.stateOrCountry,
                            temperature: weatherCurrentConditions.temperature_S,
                            temperatureType: weatherData.location.international ? celsius : fahrenheit
                        };

                    /* Render Weather Header */
                    if (isEnabledInHeader) {
                        dust.render('views/cards/tool/weather-header', headerFooterRenderObject, function (err, out) {
                            if (err) { return; }

                            $weatherHeader.replaceWith(out);

                            if (typeof CNN.Analytics !== 'undefined' &&
                                typeof CNN.Analytics.utils !== 'undefined' &&
                                typeof CNN.Analytics.utils.addTrackingTags === 'function') {

                                CNN.Analytics.utils.addTrackingTags('.nav .weather');
                            }
                        });
                    }

                    /* Render Weather Footer */
                    if (isEnabledInFooter) {
                        dust.render('views/cards/tool/weather-footer', headerFooterRenderObject, function (err, out) {
                            if (err) { return; }

                            $weatherFooter.empty().append(out);

                            if (typeof CNN.Analytics !== 'undefined' &&
                                typeof CNN.Analytics.utils !== 'undefined' &&
                                typeof CNN.Analytics.utils.addTrackingTags === 'function') {

                                CNN.Analytics.utils.addTrackingTags('.weather__footer-content');
                            }
                        });
                    }

                    dfr.resolve(weatherData);
                });
            },
            /* Returns true if the weather page is being rendered */
            isWeatherPage = function () {
                var weatherPage = false;
                if (CNN.contentModel.sectionName === 'weather') {
                    weatherPage = true;
                }
                return weatherPage;
            },
            /* Render the General Weather ToolCard, usable on all pages */
            renderWeatherGeneral = function (weatherData) {
                return (!isEnabled || !isWeatherPage()) ? undefined : $.Deferred(function (dfr) {
                    var $weatherGeneralCard = $('.js-weather-general-default'),
                        currentWeather = weatherData.currentConditions,
                        weatherLocation = weatherData.location,
                        renderObject = {
                            location: weatherLocation.city + ', ' + weatherLocation.stateOrCountry,
                            weatherIcon: CNN.Host.chrome + '/.e/img/3.0/weather/weatherIcon_' + currentWeather.icon + '.png',
                            weatherBackgroundClass: currentWeather.icon.substring(0, 2),
                            temperatureCurrent: currentWeather.temperature,
                            temperatureHigh: weatherData.forecast.days[0].high,
                            temperatureLow: weatherData.forecast.days[0].low,
                            forecastDescription: currentWeather.description,
                            temperatureType: weatherData.location.international ? celsius : fahrenheit,
                            timestamp: moment(currentWeather.lastUpdated.time).format('h:mm a, MMMM Do')
                        };


                    $('.js-weather-general-settings').hide();

                    dust.render('views/cards/tool/weather-general-html', renderObject, function (err, out) {
                        if (err) {
                            $('.js-weather-general-default').show();
                            $('.el-weather__settings-link').children('.weather__local-query-visibility-toggle').show();
                            return;
                        }

                        $weatherGeneralCard.html(out);

                        $('.js-forecast-graphic').addClass('js-weather-background-' + currentWeather.icon.substring(0, 2));
                        $('.js-weather-general-icon-image').html(
                            '<i class="icon icon--weather-' + currentWeather.icon + '"></i>'
                        );
                        $('.js-weather__bar-timestamp').html(moment(currentWeather.lastUpdated.time).format('h:mm a, MMMM Do'));
                        $('.js-weather-general-default').show();
                        $('.el-weather__settings-link').children('.weather__local-query-visibility-toggle').show();

                    });

                    $('.weather__general .js-temperature').on('click', function weatherClick(e) {
                        /* if we click anywhere in the settings form, which is a child of temperature */
                        if ($(e.target).parents('form').length === 0) {
                            window.location = '/weather';
                        }
                    });

                    dfr.resolve(weatherData);
                });
            },
            /* initialize typeahead on general weather tool card location search in settings */
            initTypeAhead = function () {
                var $settingsForm = $('.js-weather__general-query-form'),
                    $locationInput = $settingsForm.find('.js-weather__local-query__search'),
                    /* Bloodhound docs located at https://github.com/twitter/typeahead.js/tree/master/doc */
                    lookupEngine = new Bloodhound({
                        limit: 15,
                        queryTokenizer: Bloodhound.tokenizers.whitespace,
                        datumTokenizer: function (datum) {
                            return Bloodhound.tokenizers.whitespace(datum.data.city);
                        },
                        remote: {
                            url: CNN.WeatherConfig.serviceCitySearchHost + CNN.WeatherConfig.citySearch + '/json/%QUERY/true',
                            filter: function (response) {
                                var datums = [];

                                if (typeof response !== 'undefined') {
                                    $.each(response, function (index, cities) {
                                        datums.push({
                                            name: cities.city + ', ' + cities.stateOrCountry,
                                            data: cities
                                        });
                                    });
                                }

                                return datums;
                            },
                            ajax: {
                                dataType: 'json',
                                complete: function (response) {
                                    var isValidResponse = !(response.responseJSON && response.responseJSON.length);

                                    $locationInput.toggleClass('error', isValidResponse);
                                    $settingsForm[isValidResponse ? 'attr' : 'removeAttr']('disabled', 'disabled');
                                }
                            },
                            replace: function (url, _query) {
                                var input = $locationInput.val().replace(/\s+/g, '+');

                                return url.replace('%QUERY', input);
                            }
                        }
                    });

                lookupEngine.initialize();

                $locationInput.typeahead({
                    hint: false
                }, {
                    name: 'user-location',
                    displayKey: 'name',
                    source: lookupEngine.ttAdapter(),
                    templates: {
                        suggestion: function (context) {
                            return context.data.city + ', ' + context.data.stateOrCountry;
                        }
                    }
                }).on('typeahead:selected', function () {
                    $settingsForm.trigger('submit');
                    $locationInput.typeahead('val', '');
                });

                /* fix inline block for typeahead */
                $settingsForm.find('.twitter-typeahead').css({display: 'block'});
                /* fix z-index issue with subsequent columns appear above dropdown element */
                $settingsForm.closest('.column').addClass('column--top');
            },
            /* toggles the temperature between celsius and fahrenheit */
            toggleTempType = function (tempType) {
                var $items,
                    $filteredItems,
                    originalTempType = tempType,
                    scale = tempType,
                    $scaleStrings;

                $('.js-el-weather__local-query__temp-toggle-button.active').removeClass('active');
                $('.js-el-weather__local-query__temp-toggle-button--' + tempType).addClass('active');

                $items = $('.js-temp');
                $filteredItems = $items.filter('[data-temptype="' + (tempType === celsius ? fahrenheit : celsius) + '"]');
                $scaleStrings = $filteredItems.next('.js--temperatureScale');
                settingsTempType = tempType;
                convertTempHandler(scale, $filteredItems, originalTempType, $scaleStrings);
            },
            /* Updates stored locations in local-storage */
            updateStoredLocations = function (storedLocations, weatherLocation) {
                return $.Deferred(function (dfr) {
                    var i,
                        previousLocCodes = [];

                    if (typeof storedLocations !== 'undefined') {
                        /* If the locCode for the location we are processing is already part of
                         * the local storage object, return right away.
                        */
                        for (i = 0; i < storedLocations.locations.length; i++) {
                            previousLocCodes.push(storedLocations.locations[i].locCode);
                        }

                        if ($.inArray(weatherLocation.locCode, previousLocCodes) < 0) {
                            if (storedLocations.locations.length === Number(CNN.WeatherConfig.storedLocationsAmount)) {
                                /* If the stored location limit has been reached, take
                                 * oldest and discard.
                                 */
                                storedLocations.locations = storedLocations.locations.slice(0, storedLocations.locations.length - 1);
                            }

                            /* Now add the new location and save data.
                             */
                            storedLocations.locations.unshift({
                                zip: weatherLocation.zip,
                                international: weatherLocation.international,
                                locCode: weatherLocation.locCode
                            });
                            localStorageKeySet('_weather-storedlocations', storedLocations);
                        }
                    }
                    dfr.resolve(storedLocations, weatherLocation);
                });
            },
            /* Process the stored locations and return the object for rendering */
            processStoredLocations = function (storedLocations, weatherLocation) {
                return $.Deferred(function (dfr) {
                    var storedLocationsRequests = [],
                        storedLocationsResponse,
                        storedLocationWeather,
                        storedLocationsRenderArray = [];

                    /* Remove all stored locations from the DOM */
                    $('.weather__conditions--location.js-weather__conditions--recent--locations').remove();

                    /* When an undetermined amount of AJAX requests have all completed in their
                     * original order, forecasts.done fires and adds everything back to the DOM
                     */
                    if ($('.cd--tool__weather-stored-locations').length > 0) {
                        $.each(storedLocations.locations, function (index, item) {
                            storedLocationsRequests.push(forecastRequestHelper(forecastQueryParams(item)));
                        });
                        storedLocationsResponse = $.when.apply(null, storedLocationsRequests);
                        storedLocationsResponse.done(function () {
                            if (typeof arguments[0][0].location !== 'undefined') {
                                storedLocationWeather = arguments[0][0];
                                storedLocationsRenderArray.push(prepRenderArray(storedLocationWeather));
                            } else {
                                $.each(arguments, function (index, item) {
                                    storedLocationWeather = item[0][0];
                                    storedLocationsRenderArray.push(prepRenderArray(storedLocationWeather));
                                });
                            }
                            dfr.resolve(storedLocations, storedLocationsRenderArray, weatherLocation);
                        });
                    } else {
                        dfr.resolve(storedLocations, storedLocationsRenderArray, weatherLocation);
                    }
                });
            },
            /* Render the saved locations to the dom */
            renderStoredLocations = function (storedLocations, storedLocationsRenderArray, weatherLocation) {
                return (!isEnabled) ? undefined : $.Deferred(function (dfr) {
                    var weatherStoredLocations = $('.weather__stored-locations');
                    $.each(storedLocationsRenderArray, function (index) {
                        /* arguments that could be passed in: index, item */
                        dust.render('views/cards/tool/weather-stored-locations-single', storedLocationsRenderArray[index], function (err, out) {
                            if (err) {
                                console.log('Dust.render error:', err);
                            } else {
                                $(out).on('click', function () {
                                    /* arguments that could be passed in: event */
                                    if (localStorageEnabled) {
                                        localStorageKeySet('_weather-customlocation',  storedLocations.locations[index]);
                                    }

                                    getWeather(forecastQueryParams({
                                        zip: storedLocations.locations[index].zip,
                                        international: storedLocations.locations[index].international,
                                        locCode: storedLocations.locations[index].locCode
                                    }));
                                }).prependTo(weatherStoredLocations);
                            }
                        });
                    });
                    dfr.resolve(storedLocations, weatherLocation);
                });
            },
            /* Set default weather location */
            setDefaultWeatherLocation = function (locCode) {
                $('.stored-location--default').removeClass('stored-location--default');
                $('.weather__conditions--location[data-loccode=' + locCode + ']')
                    .addClass('stored-location--default');
            },
            /* Render make default button */
            renderMakeDefaultButton = function (weatherData) {
                return (!isEnabled) ? undefined : $.Deferred(function insertMakeDefaultButton(dfr) {
                    var $header = $('.zn-local-forecast .zn-header__text .zn-header__stripes'),
                        recentLocation = weatherData.location,
                        recentLocationData = {
                            zip: recentLocation.zip,
                            international: recentLocation.international,
                            locCode: recentLocation.locCode,
                            locationAttr: recentLocation.zip + ',' +
                                recentLocation.international + ',' +
                                recentLocation.locCode
                        };

                    dust.render('views/cards/tool/weather-make-default-button', recentLocationData, function renderLocation(err, out) {
                        if (err) {
                            console.log('Dust.render: weather-make-default-button - error:', err);
                        } else {
                            $(out).on('click', function saveCustomWeatherLocation() {

                                if (localStorageEnabled) {
                                    localStorageKeySet('_weather-customlocation',  weatherData.location);
                                }
                                setDefaultWeatherLocation(weatherData.location.locCode);
                            }).insertBefore($header);
                        }
                    });
                    dfr.resolve(weatherData);
                });
            },
            /* Accepts a queryParams object and returns an ajax request for weather forecasts */
            forecastRequestHelper = function (queryParams) {
                return $.ajax({
                    url: CNN.WeatherConfig.serviceForecastHost + CNN.WeatherConfig.getForecast + queryParams,
                    type: 'GET',
                    dataType: 'json'
                });
            },
            /* Accepts a queryParams object and returns an ajax request for lite data weather forecasts */
            forecastLiteRequestHelper = function (queryParams) {
                var locCode,
                    celcius,
                    query,
                    queryFields,
                    queryObject,
                    cipherQuery,
                    url;

                function getLocationObject() {
                    var locationObject = {};
                    if (localStorageEnabled) {
                        locationObject = localStorageKeyGet('_weather-customlocation');
                        if (!locationObject) {
                            locationObject = {};
                        }
                    }
                    if (!locationObject.locCode) {
                        locationObject = {locCode: defaultUsLocCode};
                        if (CNN.contentModel.edition === 'international') {
                            locationObject = {locCode: defaultIntlLocCode};
                        }
                    }
                    if (!locationObject.temperatureType) {
                        locationObject.temperatureType = 'false';
                        if (CNN.contentModel.edition === 'international') {
                            locationObject.temperatureType = 'true';
                        }
                    }
                    return locationObject || {};
                }

                function getLocationCode() {
                    return getLocationObject().locCode;
                }

                function getTemperatureCode() {
                    return getLocationObject().temperatureType;
                }

                locCode = getLocationCode();
                celcius = getTemperatureCode();

                queryParams = [{name: 'locCode', value: locCode}, {name: 'celcius', value: celcius}];
                queryFields = [
                    {name: 'currentConditions', values: [{name: 'temperature'}, {name: 'description'}, {name: 'icon'}]},
                    {name: 'location', values: [{name: 'city'}, {name: 'stateOrCountry'}]}
                ];

                queryObject = {name: 'wsiForecast', params: queryParams, fields: queryFields};
                cipherQuery = '{' + Cipher.createGraphQLQuery(queryObject) + '}';

                query = encodeURIComponent(cipherQuery);
                url = CNN.WeatherConfig.serviceForecastHost + '/graphql?query=' + query;

                return $.ajax({
                    url: url,
                    type: 'GET',
                    dataType: 'json'
                });
            },
            /* Query the weather service */
            getWeather = function (jsonParams) {
                var forecastResponse,
                    storedLocations,
                    weather,
                    weatherLocation;

                if (isWeatherPage()) {
                    forecastResponse = forecastRequestHelper(jsonParams);
                } else {
                    forecastResponse = forecastLiteRequestHelper(jsonParams);
                }

                forecastResponse
                    .fail(function () {
                        /* arguments that could be passed in: data, status, error */
                        /* If the request fails, and local storage is enabled, render cached weather */
                        if (localStorageEnabled) {
                            renderWeather(localStorageKeyGet('_weather-' + CNN.Edition));
                        }
                    })
                    .done(function (data) {
                        var $weatherFooter,
                            headerFooterRenderObject,
                            userTempType;

                        /* arguments that could be pased in: data, status */
                        if (isWeatherPage()) {
                            weather = data[0];
                            weatherLocation = data[0].location;
                        }

                        /* Set local storage copy of the weather and render everything */
                        if (localStorageEnabled) {
                            localStorageKeySet('_weather-' + CNN.Edition, weather);
                        }

                        if (!isWeatherPage()) {
                            $weatherFooter = $('.l-footer__tools--weather');
                            headerFooterRenderObject = {
                                backgroundClass: data.data.wsiForecast.currentConditions.icon,
                                weatherIcon: CNN.Host.chrome + '/.e/img/3.0/weather/weatherIcon_' + data.data.wsiForecast.currentConditions.icon + '.png',
                                weatherClass: 'weather-' + data.data.wsiForecast.currentConditions.icon,
                                description: data.data.wsiForecast.currentConditions.description,
                                locationCity: data.data.wsiForecast.location.city,
                                locationState: data.data.wsiForecast.location.stateOrCountry,
                                temperature: data.data.wsiForecast.currentConditions.temperature,
                                temperatureType: (CNN.contentModel.edition === 'domestic' ? fahrenheit : celsius)
                            },
                            userTempType;

                            if (localStorageEnabled) {
                                userTempType = localStorageKeyGet('_weather-temp-type');
                                if (typeof userTempType !== 'undefined' && headerFooterRenderObject.temperatureType !== userTempType) {
                                    headerFooterRenderObject.temperatureType = userTempType;
                                    if (userTempType === celsius) {
                                        headerFooterRenderObject.temperature = convertToCelsius(headerFooterRenderObject.temperature);
                                    } else {
                                        headerFooterRenderObject.temperature = convertToFahrenheit(headerFooterRenderObject.temperature);
                                    }
                                }
                            }

                            dust.render('views/cards/tool/weather-footer', headerFooterRenderObject, function (err, out) {
                                if (err) { return; }

                                $weatherFooter.empty().append(out);
                            });

                        } else {
                            renderTitles(weather)
                                .then(renderMakeDefaultButton)
                                .then(renderCurrentConditions)
                                .then(renderImpendingForecast)
                                .then(renderExtendedForecast)
                                .then(renderLocalMaps)
                                .then(renderWeather)
                                .then(renderWeatherGeneral)
                                .then(function () {
                                    if (localStorageEnabled) {
                                        storedLocations = localStorageKeyGet('_weather-storedlocations');
                                        if (typeof storedLocations !== 'undefined') {
                                            updateStoredLocations(storedLocations, weatherLocation)
                                                .then(processStoredLocations)
                                                .then(renderStoredLocations)
                                                .then(function () {
                                                    /* arguments that could be passed in: result */
                                                    var defaultLocation = localStorageKeyGet('_weather-customlocation'),
                                                        scale;
                                                    if (typeof defaultLocation !== 'undefined') {
                                                        scale = defaultLocation.international ? celsius : fahrenheit;
                                                        $('.js-el-weather__local-query__temp-toggle-button.active')
                                                            .removeClass('active');
                                                        $('.js-el-weather__local-query__temp-toggle-button.' + scale)
                                                            .addClass('active');

                                                        setDefaultWeatherLocation(defaultLocation.locCode);
                                                    }
                                                    if (temperatureType) {
                                                        toggleTempType(temperatureType);
                                                    }
                                                });
                                        } else {
                                            storedLocations = {
                                                locations: []
                                            };
                                            /* storedLocations, storedLocationsRenderArray, weatherLocation */
                                            renderStoredLocations(storedLocations, [prepRenderArray(weather)], weatherLocation)
                                                .then(updateStoredLocations);
                                        }
                                    } else {
                                        /* If we cannot render recent locations then hide card */
                                        $('.weather__stored-locations').closest('.cn').hide();
                                    }
                                });

                        }


                    });
            },
            /* Get a list of possible matches for a custom location query */
            getCustomLocation = function (citySearchQuery, queryType) {
                var $citySearchInput = $('#weather__' + queryType + '-query-form input[name="weather-local-search"]'),
                    $formSubmit = $('#weather__' + queryType + '-query-form input[name="set-button"]'),
                    queryParams = ('/json/' + citySearchQuery + '/true').replace(/\s+/g, '+');  /* queryParams = /mode/search_term/filter */

                $citySearchInput.val('').on('focus', function () {
                    var errorClass = $(this).hasClass('error');
                    if (typeof errorClass !== typeof undefined && errorClass !== false) {
                        $(this).removeClass('error');
                        $formSubmit.removeAttr('disabled');
                        $formSubmit.removeClass('half-opacity');
                        $(this).val('');
                    }
                });

                $.getJSON(CNN.WeatherConfig.serviceCitySearchHost + CNN.WeatherConfig.citySearch + queryParams)
                    .done(function (data) {
                        /* arguments that could be passed in: data, status */
                        if (data.length === 0) {
                            /* Show the error message if the lookup finished, but no results */
                            $citySearchInput
                                .val(citySearchQuery)
                                .addClass('error')
                                .blur();
                            $formSubmit.attr('disabled', 'disabled');
                            $formSubmit.addClass('half-opacity');
                        } else if (data.length === 1) {
                            /* Only one location, do everything beind the scenes */
                            if (queryType === 'general') {
                                localStorageKeySet('_weather-customlocation', data[0]);
                            }
                            if (typeof CNN.Features !== 'object' || CNN.Features.enableOmniture !== false) {
                                trackWeather();
                            }
                            getWeather(forecastQueryParams(data[0]));
                        }
                    });
            },

            /* makes call to track user initiated change of weather location */
            trackWeather = function () {
                try {
                    trackMetrics({
                        type: 'weather-page',
                        data: {}
                    });
                } catch (e) {}
            },

            kickOffWeather = function () {
                var localWeather,
                    userTempType;

                /* Check if we've already saved a custom location, if we have use it */
                if (localStorageEnabled) {
                    localWeather = localStorageKeyGet('_weather-customlocation');
                    userTempType = localStorageKeyGet('_weather-temp-type');
                    /* see if user has saved temperature type preference */
                    if (typeof userTempType !== 'undefined') {
                        temperatureType = userTempType;
                    }
                    if (typeof localWeather !== 'undefined') {
                        getWeather(forecastQueryParams(localWeather));
                    } else {
                        /* local-storage is enabled, but we don't have a saved location */
                        getWeather(forecastQueryParams(null));
                    }
                } else {
                    /* No local-storage, just get default weather */
                    getWeather(forecastQueryParams(null));
                }
            },
            /* processes the weather location query form */
            handleQueryForm = function (event, queryType) {
                var citySearchQuery;

                if (event.preventDefault) {
                    event.preventDefault();
                } else {
                    event.returnValue = false;
                }

                citySearchQuery = $('#weather__' + queryType + '-query-form input:first').val();
                if (citySearchQuery.length <= 2) {
                    return;
                }
                getCustomLocation(replaceHTMLEntities(citySearchQuery), queryType);
            };

        /* Initial query for weather, and start the timer */
        $(document).onZonesAndDomReady(function weatherDOMReady() {
            kickOffWeather();

            /* Only init typeahead if we will be using it */
            if ($('.js-weather__general-query-form').length > 0 && typeof Bloodhound !== 'undefined') {
                initTypeAhead();
            }

            /* Repeat the above process every 30 minutes */
            setInterval(kickOffWeather, CNN.MetaRefreshRate * 1000);
        });

        /* Everything below this point is handlers for events occurring in the DOM */

        /* Intercept the local weather page form submit */
        $(document).on('submit', '#weather__local-query-form', function (event) {
            handleQueryForm(event, 'local');
        });

        /* Intercept the general "small" weather card form submit */
        $(document).on('submit', '#weather__general-query-form', function (event) {
            /* if user has switched temp type and hits the "Set" button on the general card then save */
            setTemperatureType();

            if ($('#weather__general-query-form input:first').val()) {
                handleQueryForm(event, 'general');
            } else {
                $('.js-weather-general-default').toggle();
                $('.js-weather-general-settings').toggle();
                $('.el-weather__settings-link').children('.weather__local-query-visibility-toggle').toggle();
                return false;
            }

        });

        /* Handle the conversion to Fahrenheit & Celsius */
        $(document).on('click', '.js-el-weather__local-query__temp-toggle-button', function () {
            /* arguments that could be passed in: event */
            toggleTempType($(this).data().temp);
            setTemperatureType();
        });

        /* Handle the the settings toggle
         * for the local query box on the general weather tool card.
        */
        $(document).on('click', '.weather__local-query-visibility-toggle', function () {
            /* you tried out the temp type change in settings but didn't hit the "set" button; shame on you for clicking "close" */
            if (settingsTempType !== null) {
                toggleTempType(temperatureType);
            }

            /* perform a toggle of settings / temperature display */
            $('.js-weather-general-default').toggle();
            $('.js-weather-general-settings').toggle();
            $('.el-weather__settings-link').children('.weather__local-query-visibility-toggle').toggle();

        });
    }

    $(document).onZonesAndDomReady(function invokeWeather() {
        setTimeout(weatherService, 50);
    });

}(jQuery, CNN, store, dust, moment));

/* global CNN, Modernizr, fastdom */

/* Generic User Message with your standard dismiss feature.
 * When the user clicks the upper right X a cookie is set
 * so that the user won't see the same user message again.
 * All text and cookie values are setup in the config file.
 *
 * A single user message has the following properties:
 * {
 *     "id": "tos",
 *     "headerText": "Our Terms of Service and Privacy Policy Have Changed",
 *     "messageText": "By using this site, you agree to the Privacy Policy.",
 *     "dismissCookie": {
 *         "name": "CNNtosAgreed",
 *         "value": true,
 *         "expiration": "Tue, 01 Jan 2115 00:00:00 UTC",
 *         "expirationPeriod": "129600000"
 *         "oldCookieName": "tosAgreed",
 *         "oldCookieExpiration": "Tue, 01 Sep 2015 00:00:00 UTC"
 *     },
 *     "height": {
 *         "small": 120,
 *         "medium": 100,
 *         "large": 110
 *     }
 * }
 *
 * You can have one or more user messages setup in the array
 *
 * OPTIONAL:
 * 1) You can optionally set a 'skip' flag for the dismissCookie.
 * If set, the cookie is not checked when determining whether to display the message on load.
 * Instead an event is fired to indicate the message is initialized. This allows
 * a 3rd party to listen for the init event and use their own cookie logic to
 * display the message on load
 *
 * EX: Let TRUSTe do a geo check to see if a user is from EU and has not yet seen the message.
 * If yes, their event listener can then call CNN.userMessage.showMessage() explicitly
 *
 * Example JSON:
 * "dismissCookie": {
 *     "name": "cookieName",
 *     "value": true,
 *     "expirationDate": "",
 *     "expirationPeriod": "",
 *     "skip": true
 * }
 *
 * 2) You can optionally set a 'defer' flag for the dismissCookie.
 * If set, the message will not be displayed on load and will have to be triggered manually at a later time.
 * Once initialized, an event is fired to indicate the message is ready. This allows us to
 * perform some intermediate actions before finally calling CNN.userMessage.showDeferredMessage()
 *
 * Once the message is the displayed, our usual cookie logic can then be used.
 * Note the use of showDeferredMessage() instead of the regular showMessage()
 *
 * EX: Wait for TRUSTe to finish checking if a user is from EU and hasn't seen the message.
 * If yes, display the message. If no, do the deferred cookie check and display the message if needed
 *
 * Example JSON:
 * "dismissCookie": {
 *     "name": "CNNtosAgreed",
 *     "value": true,
 *     "expiration": "Tue, 01 Jan 2115 00:00:00 UTC",
 *     "expirationPeriod": "129600000"
 *     "oldCookieName": "tosAgreed",
 *     "oldCookieExpiration": "Tue, 01 Sep 2015 00:00:00 UTC",
 *     "defer": true
 * }
 *
 * Note: The 'skip' flag takes precedence over 'defer'
 */
CNN.Features = CNN.Features || {};
CNN.UserMessageConfig = CNN.UserMessageConfig || {};

CNN.userMessage = CNN.userMessage || (function ($, window, document) {
    'use strict';

    var $userMsg,
        messageQueue = [],
        currMessageIndex = 0,
        utils = CNN.Utils,
        exists = utils.exists,
        existsObject = utils.existsObject,
        enabled = false,
        messageId = '',
        messageEventIdSuffix = '_userMessageReady',
        cookieName = '',
        cookieValue = '',
        cookieExpiration = '',
        headerText = '',
        messageText = '',
        messageHeight = {},
        /* skip means we will remember dismissing the message another way */
        skipCookieCheck = false,
        /* defer means we will check the cookie at another time */
        deferCookieCheck = false,
        /*  Used when a cookie name is being retired. */
        oldCookieName = '',
        oldCookieExpiration = '';

    /**
     * Check whether TOS tracking should be enabled
     *
     * @private
     * @return {boolean} true if TOS tracking is enabled
     */
    function _checkTosTrackingEnabled() {
        /* Add this once we know how we will be handling it. */
        return false;
    }

    /**
     * Tracking if user move to another site/page without closing TOS
     * @private
     */
    function _trackTermsNotClosed() {
        if (_checkTosTrackingEnabled() === true) {
            /* Track Terms of Service */
            /* Fill this out once we have something to put here...
            window.addEventListener('beforeunload', function (_event) {
                if (exists(cookieName) && !exists(utils.getCookie(cookieName))) {
                    AspenCNNTOS.send('userClickedTOS', {
                        userClickedTOS: false
                    });
                }
            });
            */
        }
    }

    /**
     * Close the current user message and display the next in the queue, if any
     */
    function closeMessage() {
        if (!skipCookieCheck) {
            /* store the cookie */
            if (cookieName && cookieValue && cookieExpiration) {
                utils.setCNNCookie(cookieName, cookieValue, cookieExpiration);
            }
        }

        fastdom.mutate(function () {
            $userMsg.animate({height: 0}, 1000, function close$userMsgComplete() {
                $userMsg.hide();

                if (_checkTosTrackingEnabled() === true /* && AspenCNNTOS.initialized !== undefined */) {
                    /* Flush this out once we have something to do here...
                    AspenCNNTOS.send('userClickedTOS', {
                        userClickedTOS: true
                    });
                    */
                }

                /* Trigger displaying the next message */
                _showNextMessage();
            });
        });
    }

    /**
     * Expires the previous cookie if one is provided
     *
     * @private
     */
    function _expireOldCookie() {
        /* set a new expiration and value for the old cookie */
        if (oldCookieName && cookieValue && oldCookieExpiration) {
            utils.setCNNCookie(oldCookieName, cookieValue, oldCookieExpiration);
        }
    }

    /**
     * Display the current user message
     * @returns {Promise} - Promise when the message has been displayed.
     */
    function showMessage() {
        var dfd = $.Deferred();

        fastdom.measure(function () {
            var hasBreakingNews = $(document.body).hasClass('breaking-news--showing'),
                windowSize = ($('body[data-eq-state]').attr('data-eq-state') || 'small').replace(/^(\S+\s+)*/, '');

            /* Only show the message if there is no breaking new banner */
            if (!hasBreakingNews) {
                $userMsg = $('.user-msg');
                fastdom.mutate(function () {
                    /* In some cases a message's position in the messageQueue is used as its id */
                    if (isNaN(messageId)) {
                        $(document.getElementById('js-user-msg--body-text')).attr('id', messageId);
                    }

                    $userMsg.height(0).addClass('user-msg-flexbox');
                    $userMsg.animate({height: messageHeight[windowSize]}, 1000, function userMessageShowAnon() {
                        /* Add tracking for the TOS popup */
                        if (_checkTosTrackingEnabled() === true) {
                            _trackTermsNotClosed();
                        }
                        dfd.resolve();
                    });
                });

                /* init one time close listener on close message. */
                $('.js-user-msg--close, .js-user-msg--agree').one('click', function (_evt) {
                    closeMessage();
                });
            }
        });
        return dfd.promise();
    }

    /**
     * Load the next message in the queue, if any, and show it
     *
     * @private
     */
    function _loadAndShowNextMessage() {
        /* Show user message if there are more messages in the queue */
        if (currMessageIndex < messageQueue.length) {
            _showSingleMessage(messageQueue[currMessageIndex]);
        }
    }

    /**
     * Increment the message index and show the next message
     * @private
     */
    function _showNextMessage() {
        currMessageIndex++;
        _loadAndShowNextMessage();
    }

    /**
     * Check message targeting
     *
     * @private
     * @param {object} msgConfig - Message config options object
     */
    function checkMessageTargeting(msgConfig) {
        var f,
            i,
            t,
            tgt = msgConfig.targeting;

        try {
            if ((Array.isArray(tgt.pageTypes) && tgt.pageTypes.length > 0 && tgt.pageTypes.indexOf(CNN.contentModel.pageType) < 0) ||
                (Array.isArray(tgt.sectionNames) && tgt.sectionNames.length > 0 && tgt.sectionNames.indexOf(CNN.contentModel.sectionName) < 0) ||
                (Array.isArray(tgt.geoTargets) && tgt.geoTargets.length > 0 && CNN.GeoCheck.inTarget(tgt.geoTargets) === false)) {

                _showNextMessage();
                return;
            }

            if (typeof tgt.browserTests === 'object' && tgt.browserTests !== null) {
                if (Array.isArray(tgt.browserTests.useragents) && tgt.browserTests.useragents.length > 0) {
                    f = false;
                    for (i = 0; i < tgt.browserTests.useragents.length; i++) {
                        t = tgt.browserTests.useragents[i];
                        if (t.length > 0) {
                            if (t.charAt(0) === '-') {
                                if (window.navigator.userAgent.search(new RegExp(t.slice(1))) >= 0) {
                                    /* excluded useragent pattern matched */
                                    _showNextMessage();
                                    return;
                                }
                            } else if (window.navigator.userAgent.search(new RegExp(t)) >= 0) {
                                f = true;  /* included useragent pattern matched */
                            }
                        }
                    }
                    if (f === false) {
                        _showNextMessage();
                        return;
                    }
                }
                if (typeof tgt.browserTests.modernizrTests === 'object' && tgt.browserTests.modernizrTests !== null) {
                    for (t in tgt.browserTests.modernizrTests) {
                        if (tgt.browserTests.modernizrTests.hasOwnProperty(t) && t !== 'flash' &&
                            Modernizr[t].valueOf() !== tgt.browserTests.modernizrTests[t]) {

                            _showNextMessage();
                            return;
                        }
                    }
                    if (tgt.browserTests.modernizrTests.hasOwnProperty('flash')) {
                        /* The flash check is asynchronous, so deal with that. */
                        Modernizr.on('flash', function () {
                            if ((Modernizr.flash.valueOf() && !Modernizr.flash.blocked) !== tgt.browserTests.modernizrTests[t]) {
                                _showNextMessage();
                            } else {
                                _showSingleMessageComplete(msgConfig);
                            }
                        });
                        return;
                    }
                }
            }
            _showSingleMessageComplete(msgConfig);
        } catch (err) {
            _showNextMessage();
        }
    }

    /**
     * Checks if the cookie is already set and displays the user message.
     * If the cookie is NOT set, display the message
     * If the cookie is set, skip and go to the next message in the queue
     *
     * @private
     */
    function _checkCookieAndShowMessage() {
        /* check if the cookie is already set */
        if (exists(cookieName) && exists(utils.getCookie(cookieName))) {
            /* user already had the cookie stored, jump to next message */
            _showNextMessage();
            return;
        }

        /* Display the current message */
        showMessage();
    }

    /**
     * Perform the cookie check and show the message
     *
     * @public
     */
    function showDeferredMessage() {
        _checkCookieAndShowMessage();
    }

    /**
     * Displays a user message with the given message config
     *
     * @private
     * @param  {object} msgConfig the configuration for the user message
     */
    function _showSingleMessage(msgConfig) {
        if (typeof msgConfig.targeting === 'object' && msgConfig.targeting !== null) {
            /* Check the optional targeting of the message, if any */
            checkMessageTargeting(msgConfig);
        } else {
            _showSingleMessageComplete(msgConfig);
        }
    }

    function _showSingleMessageComplete(msgConfig) {
        var cookie;

        /* Store the message details */
        messageId = exists(msgConfig.id) ? msgConfig.id : currMessageIndex;
        headerText = exists(msgConfig.headerText) ? msgConfig.headerText : '';
        messageText = exists(msgConfig.messageText) ? msgConfig.messageText : '';

        /* add message content */
        if (headerText.length === 0) {
            $('.user-msg').addClass('headerless');
        }
        $('.js-user-msg--header-text').html(headerText);
        $('.js-user-msg--body-text').html(messageText);

        /* Get user message height */
        messageHeight = msgConfig.height;
        messageHeight.xsmall = messageHeight.small;
        messageHeight.full16x9 = messageHeight.large;

        /* fill in cookie details */
        if (existsObject(msgConfig.dismissCookie)) {
            cookie = msgConfig.dismissCookie;

            /* user message cookie */
            cookieName = exists(cookie.name) ? cookie.name : '';
            cookieValue = exists(cookie.name) ? cookie.value : '';
            cookieExpiration = _getCookieExpiration(cookie);
            skipCookieCheck = exists(cookie.skip) && cookie.skip;
            deferCookieCheck = exists(cookie.defer) && cookie.defer;

            /* old cookie info */
            oldCookieName = exists(cookie.oldCookieName) ? cookie.oldCookieName : '';
            oldCookieExpiration = exists(cookie.oldCookieExpiration) ? cookie.oldCookieExpiration : '';
        }

        /* Expire the previous cookie */
        _expireOldCookie();

        /* Do we need to check the cookie? */
        if (!skipCookieCheck && !deferCookieCheck) {
            _checkCookieAndShowMessage();
        } else {
            /* If we are skipping the cookie check, trigger an event to allow
             * something else to display the message. */
            $(document).trigger(messageId + messageEventIdSuffix);
        }
    }

    /**
     * Removes a user message with the given id from the messageQueue
     *
     * @private
     * @param  {string} messageId the id of the MessageConfig
     */
    function _removeMessage(messageId) {
        var messageQueueIndex,
            message;

        for (messageQueueIndex = 0; messageQueueIndex < messageQueue.length; messageQueueIndex++) {
            message = messageQueue[messageQueueIndex];
            /* look for matching id, or use index in messageQueue if no id is defined */
            if ((exists(message.id) ? message.id : messageQueueIndex) === messageId) {
                messageQueue.splice(messageQueueIndex, 1);
            }
        }
    }

    /**
    * Gets the proper expiration date to be set on the cookie from cookie's
    * configuration. Uses whichever is sooner, between the finite expiration
    * date (cookieConfig.expiration) and cookieConfig.expirationPeriod, which
    * represents a span of time from now.
    *
    * @private
    * @param {object} cookieConfig the 'dismissCookie' property of the messageConfig
    * @return {string} the UTC date string that should be set on the cookie
    */
    function _getCookieExpiration(cookieConfig) {
        var now = new Date(),
            expiration,
            expirationDate,
            expirationPeriod;

        if (exists(cookieConfig.expiration)) {
            expirationDate = new Date(cookieConfig.expiration);
        }

        if (exists(cookieConfig.expirationPeriod)) {
            expirationPeriod = new Date(cookieConfig.expirationPeriod + now.getTime());
        }

        if (expirationDate && expirationPeriod) {
            expiration = expirationDate > expirationPeriod ? expirationPeriod : expirationDate;
        } else {
            expiration = expirationDate || expirationPeriod;
        }
        return expiration ? expiration.toUTCString() : '';
    }

    /**
     * Load configuration and show message (if enabled)
     */
    function init() {
        var cfg = CNN.UserMessageConfig; /* the original config */

        messageQueue = Array.isArray(cfg) ? cfg : (typeof cfg === 'object' && cfg !== null ? [cfg] : []); /* save queue */
        enabled = CNN.Features.enableUserMessage === true;

        /* If user messages are enabled, show the next message in the queue */
        if (enabled) {

            /* Only show ie not supported message if browser is ie8, ie9 and ie10
            *  Also removing the messages when the Modernizr.ieUnsupported is undefined
            *  to avoid showing it form other browsers.
            */
            if (!(typeof Modernizr.ieunsupported === 'boolean' && Modernizr.ieunsupported)) {
                _removeMessage('ieUnsupported');
            }

            /* Show video not supported message if browser is ie11
            *  Also removing the messages when the Modernizr.ieUnsupported is undefined
            *  to avoid showing it form other browsers.
            */
            if (Modernizr.ie11unsupported !== true) {
                _removeMessage('ie11VideoUnsupported');
            }

            _loadAndShowNextMessage();
        }
    }

    return {
        showMessage: showMessage,
        showDeferredMessage: showDeferredMessage,
        closeMessage: closeMessage,
        init: init
    };
})(jQuery, window, document);

/* Initialize user messages */
(function ($, document, ns) {
    'use strict';

    /* Fixes execution on static pages */
    if (typeof $.fn.onZonesAndDomReady === 'function') {
        $(document).onZonesAndDomReady(ns.userMessage.init);
    } else {
        $(document).ready(ns.userMessage.init);
    }
})(jQuery, document, CNN);

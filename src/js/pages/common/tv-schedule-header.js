/* global eqjs, CNN */

(function ($, document) {
    'use strict';

    var $documentBody = $(document.body),
        $dropDowns = $('.js-tv-schedule-drop-down').add('.js-tv-schedule-day-drop-down'),
        $dropDownLists = $dropDowns.siblings().children('.js-el-drop-down__list__container').children('.js-el-drop-down__list'),
        $scheduleItems = $('.js-tv_schedule_item');

    /**
     * Opens up the associated drop down list element.
     *
     * @param {object} event - jQuery event
     * @private
     */
    function _openDropDown(event) {
        var $targetDropDownList = $(event.currentTarget).siblings().children('.js-el-drop-down__list__container').children('.js-el-drop-down__list');

        $dropDowns = $(event.currentTarget);
        $dropDownLists = $dropDowns.siblings().children('.js-el-drop-down__list__container').children('.js-el-drop-down__list');
        $dropDowns.addClass('el-drop-down__arrow-up');
        if ($targetDropDownList.hasClass('el-drop-down__list--open')) {
            _closeDropDown(event);
        } else {
            $targetDropDownList.addClass('el-drop-down__list--open');
        }
    }

    /**
     * Closes currently open drop down list element on page.
     *
     * @private
     * @param {object} event - jQuery event
     */
    function _closeDropDown(event) {
        $dropDowns = $(event.currentTarget);
        $dropDownLists = $dropDowns.siblings().children('.js-el-drop-down__list__container').children('.js-el-drop-down__list');
        $dropDowns.removeClass('el-drop-down__arrow-up');
        $dropDownLists.removeClass('el-drop-down__list--open');
    }

    /**
     * Closes up all drop down list elements on page.
     *
     * @private
     */
    function _closeAllDropDown() {
        $dropDowns = $('.js-tv-schedule-drop-down').add('.js-tv-schedule-day-drop-down');
        $dropDownLists = $dropDowns.siblings().children('.js-el-drop-down__list__container').children('.js-el-drop-down__list');
        $dropDowns.removeClass('el-drop-down__arrow-up');
        $dropDownLists.removeClass('el-drop-down__list--open');
    }

    /**
     * Setup required event handlers.
     *
     * @private
     */
    function _bindEvents() {
        var context = jQuery('.pg-wrapper');

        if (CNN.Utils.exists(eqjs) &&
            CNN.Utils.exists(CNN.ResponsiveImages) &&
            CNN.Utils.exists(context[0])) {
            eqjs.query(context[0].querySelectorAll('[data-eq-pts]'), CNN.ResponsiveImages.resize);
        }

        $dropDowns.on('click', function dropDownClickHandler(event) {
            /* Stop event from bubbling up and triggering the handler we just attached */
            event.stopPropagation();

            /* Add a one time handler to close drop downs on any other interactions */
            $documentBody.one('click', _closeAllDropDown);

            _openDropDown(event);
        });

        /* Click handler when the user clicks to review a schedule for another day */
        $('.js-tv-schedule-header-day__container .js-el-drop-down__list-item').on('click', function dayClickHandler(event) {
            var $targetDayItem,
                selectedDay;

            event.stopPropagation();
            $targetDayItem = $(event.currentTarget);
            selectedDay = $targetDayItem.data('value');
            $scheduleItems.hide();
            $('.js-tv_schedule_' + selectedDay).show();
            $('.js-tv-schedule-header-day-initial-day__label').text($targetDayItem.text());
            _closeAllDropDown();

            if (CNN.Utils.exists(eqjs) &&
                CNN.Utils.exists(CNN.ResponsiveImages) &&
                CNN.Utils.exists(context[0])) {
                eqjs.query(context[0].querySelectorAll('[data-eq-pts]'), CNN.ResponsiveImages.resize);
            }
        });
        /* Click handler when the user clicks to shows for Morning, Afternoon, Evening, or Overnight */
        $('.js-tv-schedule-header-day-period__list .js-day-period__list-item').on('click', function dayPeriodClickHandler(event) {
            var $targetDayPeriodItem,
                selectedDayPeriod,
                $znWithPeriodLabel;

            event.stopPropagation();
            $targetDayPeriodItem = $(event.currentTarget);
            selectedDayPeriod = $targetDayPeriodItem.data('period');

            $znWithPeriodLabel = $('.js-tv_schedule_item:visible').filter(function filterZoneWithSelectedDayPeriod() {
                return $(this).data('zone-label') ===  selectedDayPeriod;
            });

            $('html, body').animate({
                scrollTop: $($znWithPeriodLabel).offset().top - 90
            }, 1000);
        });

        /* Hide all show except those for the current day */
        $('.js-tv_schedule_item:not(.js-tv_schedule_day_0)').hide();
    }

    $(document).onZonesAndDomReady(function tvScheduleDOMReady() {
        _bindEvents();
    });

}(jQuery, document));


/* global CNN, jQuery:false */

CNN.staticNav = CNN.staticNav || {};

(function setStaticChromeMessageContent(ns, win, doc, $j) {
    'use strict';

    /**
     *
     * @var {Boolean} appActive
     * if attempting to run in a mobile app, exit
     *
     * @var {String} sect
     * used to set the highlighted section of the nav
     *
     * @var {String} sectUriFragment
     * on success creates a valid URI fragment
     *
     * @var {Array} type
     * list of templates to load
     *
     * @var {Boolean} vertical
     * vertical overrides
     *
     * @var {Object} getPath
     * @member {Object} ocs
     * an object representing the chrome template paths
     * additional template paths should be added here
     */
    var appActive = ns.staticNav.appDetected || false,
        sect = ns.staticNav.ctx.section || '',
        sectUriFragment = sect ? sect + '/' : '',
        type = ns.staticNav.dataType || [],
        vertical = ns.staticNav.ctx.isVertical || false,

        getPath = {
            ocs: {
                header: '/data/ocs/section/' + sectUriFragment + 'index.html:*/views/pages/common/static-header.html',
                footer: '/data/ocs/section/' + sectUriFragment + 'index.html:*/views/pages/common/static-footer.html',
                skinny: '/data/ocs/section/index.html:*/views/pages/common/static-header-skinny.html',
                sponsored: '/data/ocs/section/index.html:*/views/pages/common/static-header-sponsor.html'
            }
        },

        getNav = {
            init: function init() {
                /**
                 *
                 * @returns {null | Function} On success will initialize this script.
                 */
                var self = this;

                self.env = ns.staticNav.hostDomain || '//www.cnn.com';
                self.nav = getPath.ocs.header;
                self.footer = getPath.ocs.footer;
                self.skinny = getPath.ocs.skinny;
                self.sponsored = getPath.ocs.sponsored;

                return (appActive) ? null : self.getOCS();
            },

            getOCS: function queryOCS() {
                /* handle vertical css activation */
                if (vertical) {
                    $j('body').addClass('pg pg-vertical pg-leaf pg-static pg-' + vertical + ' pg-vertical--' + vertical);
                }

                /* create a postmessage for each wrapper */
                $j('.cnn-js-chrome-wrapper').each(getNav.getMarkupFrame);
            },

            getMarkupFrame: function iframeTemplate(i, b) {
                /**
                 * On success will create an iframe for communication with the
                 * parent page
                 *
                 * @param {int} i
                 * index of the jQuery object passed in
                 *
                 * @param {obj} b
                 * DOM node
                 *
                 * @var {obj} $b
                 * jQuery wrapper for DOM node
                 *
                 * @var {str} frameId
                 * unique iframe id
                 */

                var $b = $j(b),
                    frameId = 'cnn-nav-chrome-' + i;

                if (type.length && getNav.hasOwnProperty(type[i])) {
                    $b.append('<iframe id="' + frameId + '" src="' + getNav.env + getNav[type[i]] + '#' + frameId + '" style="display:none;"></iframe>');
                }
            }
        };

    getNav.init();
}(CNN, window, document, jQuery));

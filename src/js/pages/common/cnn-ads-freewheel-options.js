/* global CNN */

/**
 * This file sets the global variables that the Freemarker ads library
 * requires to inject ads into the page.
 *
 * THIS CODE IS PURPOSEFULLY NOT INCLUDED IN AN EVENT HANDLER.
 * IT MUST BE RUN AT BROWSER PARSE-TIME FOR AD SUPPORT.
 */
(function setGlobalFreeWheelParams(ns, win) {
    'use strict';

    var windowWidth = (ns.CurrentSize && typeof ns.CurrentSize.getClientWidth === 'function') ? ns.CurrentSize.getClientWidth() : -1;

    /*
     * Do these variables need to be global? I know for sure that
     * fw_params needs to be global. They were implicitly global
     * variables in the dust version which is why they are here. I am
     * happy to make them local variables if it does not effect
     * anything.
     */
    win.desktopSSID = ns.AdsConfig.desktopSSID;
    win.mobileSSID = ns.AdsConfig.mobileSSID;
    win.randNum = Math.floor(Math.random() * 90000) + 10000; /* random 5 digit number required by Freewheel */
    win.ssid = (windowWidth < 600) ? win.mobileSSID : win.desktopSSID; /* get the right SSID */

    /* Setting up freewheel parameters */
    win.fw_params = {
        scan_delay: 0,
        server: 'bea4.v.fwmrm.net',
        network_id: ns.AdsConfig.fwNetworkId,
        profile: ns.AdsConfig.fwProfile,
        site_section_id: win.ssid,
        other_global_params: 'csid=' + win.ssid + '&pvrn=' + win.randNum,
        key_values: 'cnn_platform=offdeck'
    };
}(CNN, window));


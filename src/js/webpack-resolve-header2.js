/* global CNN */

/*
 * Informs the injector that the second header has completed loadeding asynchronously.
 *
 * This library should be the last library in the second header.  This calls a
 * header specific function in the injector so any functionality that is
 * dependent on any libraries in the header can execute.
 *
 *
 */
(function () {
    'use strict';

    var events;

    try {
        /*
        * Set default libraries to page
        */
        CNN.INJECTOR.createDeferredForFeature('header1');
        CNN.INJECTOR.createDeferredForFeature('header2');
        CNN.INJECTOR.createDeferredForFeature('footer');
        CNN.INJECTOR.createDeferredForFeature('videodemanddust');

        /**
        * Array of event listeners
        */
        events = ['onZonesAndDomReady', 'onZoneRendered'];

        /**
        * Add array of event listeners to page
        * @param {array} events - array of event listeners
        */
        CNN.INJECTOR.registerEvents(events);

        /**
        * Resolve deferrs to activate injector
        */
        CNN.INJECTOR.scriptComplete('header1');
        CNN.INJECTOR.scriptComplete('header2');
    } catch (e) {
        console.log(e);
    }
})();


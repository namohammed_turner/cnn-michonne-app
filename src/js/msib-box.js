/* global CNN:true */

/* Basic reference implementation of the "user profile box" that lets the user
 * log in and log out.
 */

/* Exported functions. */
CNN = window.CNN || {};
CNN.mycnn = CNN.mycnn || {};

(function ($, CNN) {
    'use strict';

    var currentUser;

    /**
     * Update the UI to reflect the current login state.
     * @param {CnnUser} user - The composite user.
     */
    function updatePanels(user) {
        var loginBoxPanel = $('.msib-box-login-panel'),
            infoBoxPanel = $('.msib-box-info-panel'),
            avatarNotifyCountPanel = $('.msib-avatar--notifycount'),
            userFirstName = user.displayName.split(' ')[0],
            notifyCount;

        $.each(infoBoxPanel, function (index, item) {
            if (user.loggedIn) {
                $(item).show();
                $(item).find('.msib-info-username').text('Hi ' + userFirstName + '!');
            } else {
                $(item).hide();
            }
        });

        $.each(loginBoxPanel, function (index, item) {
            if (user.loggedIn) {
                $(item).hide();

            } else {
                $(item).addClass('show-msib-panel');
            }
        });

        $.each(avatarNotifyCountPanel, function (index, item) {
            if (user.loggedIn) {
                CNN.notifier.getNotification(true, function (data) {
                    if (data !== null && typeof data.data !== 'undefined' && data.data.length > 0) {
                        notifyCount = data.data.length;
                        if (notifyCount === 50 && data.meta.cursor.hasNext === true) {
                            notifyCount = '50+';
                        }
                        $(item).show().find('.msib-avatar--notifycountvalue').text(notifyCount);
                    } else {
                        $(item).hide();
                    }
                });
            } else {
                $(item).hide();
            }
        });
    }

    /**
     * Log out from MSIB.
     * @param {CnnUser} user - The composite user, passed from msib-init.
     */
    function msibLogout(user) {
        user.logout();
        delete CNN.msib.user;
    }

    /**
     * Perform post-MSIB-initialization steps.
     * @param {CnnUser} user - The composite user, passed from msib-init.
     */
    function postInit(user) {
        currentUser = user;

        /* Activate the login link.
         * This is done in the init handler so that we know that the
         * local config has been applied. */
        $(document.getElementById('msib-login-link')).prop('href', CNN.msib.getLoginUrl());

        updatePanels(user);

        /* Wire up the links. */
        $(document.getElementById('msib-logout')).click(function () {
            msibLogout(user);
            return false;
        });
    }

    /*
     * External point to trigger updatePanels to update the count.
     *
     * @param {function} callback - Called when complete.
     */
    function update(callback) {
        if (typeof currentUser !== 'undefined' && currentUser !== null) {
            updatePanels(currentUser);
        }
        callback();
    }

    $(document).onZonesAndDomReady(function msibDOMReady() {
        if (CNN.Utils.existsObject(CNN.msib) && typeof CNN.msib.onInitialized === 'function') {
            try {
                CNN.msib.onInitialized(postInit);
            } catch (err) {
                console.log('MSIB initialization error: ', err);
            }
        }
    });

    CNN.mycnn.updateCount = update;
}(jQuery, CNN));


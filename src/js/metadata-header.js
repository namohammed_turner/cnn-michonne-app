
(function ($) {
    'use strict';

    /* Toggle show/hide the description in the Metadata Header */
    $('.metadata-header__description-toggle').click(function () {
        var link = $(this);

        $('.metadata-header__description').toggle('slide', function () {
            if ($(this).is(':visible')) {
                link.text('Hide Description');
                link.removeClass('show-desc').addClass('hide-desc');
            } else {
                link.text('Show Description');
                link.removeClass('hide-desc').addClass('show-desc');
            }
        });
    });
}(jQuery));


/* global CNN, moment */

(function ($) {
    'use strict';

    /**
     * Entry point for this tab
     *
     * @public
     */
    function show() {
        if (CNN.notifier && CNN.notifier.reload) {
            $.when(_getNotifications()).always(_renderStream);
        }
    }

    /**
     * Render the user stream and inject it onto the page
     *
     * @param {object} output - Notification message items and suggested topics to be rendered
     * @private
     */
    function _renderStream(output) {
        window.dust.render('views/cards/tool/mycnn-stream', output, function (err, out) {
            if (err) {
                if (window.console && window.console.error) {
                    console.error('displayNotification:error: ', err);
                }
            } else {
                CNN.mycnn.updateBody(out);
                _registerHandlers();
            }
        });
    }

    /**
     * Fetch user notifications
     *
     * @return {object} The promise object
     * @private
     */
    function _getNotifications() {
        return $.Deferred(function (dfd) {
            CNN.notifier.getNotification(false, function (notifications) {
                if (notifications && notifications.data) {
                    /* We mark all notifications as read on page load. */
                    CNN.notifier.markAllAsRead(notifications, function (success) {
                        if (success) {
                            CNN.mycnn.updateCount(function updateCount() {
                                CNN.follow.getSubscriptions(function getSubscribedTopics(success, topics) {
                                    return dfd.resolve(_processNotifications(notifications.data, topics));
                                });
                            });
                        } else {
                            dfd.reject();
                        }
                    });
                } else {
                    dfd.reject();
                }
            });
        }).promise();
    }

    /**
     * Split notifications into followed topics and messages
     *
     * @param {object[]} notifications - Raw unprocessed notification stream from API request
     * @param {object} subscribedTopics - Followed topics from API request
     * @return {object} - Processed data object containing items to be rendered
     * @private
     */
    function _processNotifications(notifications, subscribedTopics) {
        var anchorName,
            articles = [],
            i,
            id,
            moreCNNAnchors = [],
            processedNotifications = [],
            profileName,
            profileUrl,
            suggestedTopic,
            suggestedTopicId,
            suggestedTopics = CNN.suggestedTopics,
            suggestedTopicsLength = 0,
            subscribedTopicIds = [],
            subscribedTopicId,
            subscribedTopicLength;

        if (CNN.Utils.exists(CNN.suggestedTopics) && CNN.suggestedTopics instanceof Array) {
            suggestedTopicsLength = CNN.suggestedTopics.length;
        }

        if (CNN.Utils.exists(subscribedTopics)) {
            for (id in subscribedTopics) {
                if (subscribedTopics.hasOwnProperty(id)) {
                    subscribedTopicIds.push(subscribedTopics[id]);
                }
            }
        }

        /*
         * Iterate through subscribed profiles and collect maximum of 5 stories under each profile
         */
        subscribedTopicLength = subscribedTopicIds.length;
        for (i = 0; i < subscribedTopicLength; i++) {

            subscribedTopicId = subscribedTopicIds[i];
            articles = getArticlesForSubscribedTopic(notifications, subscribedTopicId);
            if (articles.length && typeof articles[0].anchorName !== 'undefined') {
                anchorName = articles[0].anchorName;
            }
            if (articles.length > 0) {
                profileName = anchorName.toLowerCase().replace(/ /g, '-') + '-profile';
                profileUrl = '/data/ocs/profile/profiles/' + profileName + '/index.html:*.json';
                $.ajax({
                    url: profileUrl,
                    async: false,
                    cache: true,
                    dataType: 'json',
                    method: 'GET',
                    complete: function (response) {
                        var jsonResponse,
                            imageUrl;

                        if (response !== null && typeof response !== 'undefined') {
                            jsonResponse = response.responseJSON || {};
                            imageUrl = jsonResponse.background.image.cuts.xsmall.uri || '';
                        }
                        if (typeof imageUrl === 'undefined' || imageUrl.length === 0) {
                            /* use default image if anchor image is not available */
                            imageUrl = '//i.cdn.turner.com/cnn/.e/img/3.0/shows/default-show-image-large-169.jpg';
                        }
                        processedNotifications.push({
                            articles: articles,
                            anchor: [{
                                name: anchorName,
                                id: subscribedTopicId,
                                imageUrl: imageUrl
                            }]
                        });
                    }
                });
            }
        }

        /*
         * If suggested profile does not already exist in subscribed profiles,
         * display the suggested profile under View More Profiles section
         */
        for (i = 0; i < suggestedTopicsLength; i++) {
            suggestedTopic = suggestedTopics[i];
            if (CNN.Utils.exists(suggestedTopic)) {
                suggestedTopicId = suggestedTopic.id;
            }

            if (CNN.Utils.exists(suggestedTopicId)) {

                if (subscribedTopicIds.indexOf(suggestedTopicId) < 0) {
                    moreCNNAnchors.push({
                        anchor: [{
                            name: suggestedTopic.name,
                            id: suggestedTopicId,
                            imageUrl: suggestedTopic.imageUrl
                        }]
                    });
                }
            }
        }

        return {
            notifications: processedNotifications,
            suggestedTopics: moreCNNAnchors
        };
    }

    /**
     * Get articles for given topic id.
     *
     * @param {object[]} notifications - Raw unprocessed notification stream from API request
     * @param {string} suggestedTopicId - Topic Id
     * @return {object[]} - Articles containing items to be rendered
     * @private
     */
    function getArticlesForSubscribedTopic(notifications, suggestedTopicId) {

        var articles = [],
            articleId,
            i,
            j,
            MAX_ARTICLE_COUNT = 5,
            notification,
            notificationCount = 0,
            tagCount,
            targetObject,
            topic,
            topicId,
            anchorName;

        if (CNN.Utils.exists(notifications) && notifications instanceof Array) {
            notificationCount = notifications.length;
        }

        for (i = 0; i < notificationCount; i++) {
            notification = notifications[i] || {};
            targetObject = notification.target;
            if (notification.verb) {
                switch (notification.verb) {
                case 'create':
                    targetObject = notification.object;
                    if (targetObject.tags.length > 0) {
                        anchorName = targetObject.tags[0].displayName;
                    }
                    break;
                case 'add':
                    targetObject = notification.target;
                    anchorName = notification.object.displayName;
                    break;
                }
            }
            tagCount = targetObject.tags.length;

            articleId = targetObject.articleId;

            for (j = 0; j < tagCount; j++) {
                topic = targetObject.tags[j];
                topicId = topic.id.match(/topic=[\w\-\_]+/g);
                if (CNN.Utils.exists(topicId)) {
                    topicId = topicId[0].replace('topic=', '');
                }

                if (suggestedTopicId === topicId && !isArticleExists(articles, articleId)) {
                    articles.push({
                        title: targetObject.title,
                        url: targetObject.url,
                        published: moment(notification.published).format('MM/DD/YYYY'),
                        articleId: articleId,
                        anchorName: anchorName
                    });
                }
            }
            if (articles.length === MAX_ARTICLE_COUNT) {
                return articles;
            }
        }

        return articles;
    }

    /**
     * Check whether the given article is exits or not in articles.
     *
     * @param {object[]} articles - Articles
     * @param {string} articleId - Article Id
     * @returns {boolean} - true if article exists
     * @private
     */
    function isArticleExists(articles, articleId) {
        var i,
            len = articles.length;

        for (i = 0; i < len; i++) {
            if (articleId === articles[i].articleId) {
                return true;
            }
        }
        return false;
    }

    /**
     * Set up call back handlers
     *
     * @private
     */
    function _registerHandlers() {
        /* Deligate follow/unfollow button status update to cnn-social.
            Update Follow buttons state using cnn-social's livefyre-follow */
        CNN.follow.updateFollowButton(CNN.mycnn.user);
        /* After updating status for the current button, update for all other buttons
            by refreshing page */
        $('.js-jfy-button').on('click', show);
    }

    /* Expose publically */
    CNN.mycnn.stream = {
        show: show
    };
}(jQuery));


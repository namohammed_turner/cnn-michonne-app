/* global CNN */

(function ($, window, document) {
    'use strict';

    var $tickerList = $('.js-m-sports-ticker__list'),
        $ticker = $tickerList.parent(),
        ocsQueryUrl = '/data/ocs/section/sport/index.html:*.json',
        hypatiaQueryUrl = '/section:{section}/type:article/rows:1?callback=?';

    /**
     * Gets items to populate the ticker.
     *
     * @returns {object} jQuery promise object
     * @private
     */
    function _getTickerItems() {
        return $.Deferred(function (dfd) {
            $.when(_getCuratedItems()).then(dfd.resolve, function () {
                // Fallback to pulling latest items from each section
                $.when(_getLatestItems()).done(dfd.resolve);
            });
        }).promise();
    }

    /**
     * Makes a request to the OCS end point
     * to pull the manually curated documents.
     *
     * @returns {object} jQuery promise object
     * @private
     */
    function _getCuratedItems() {
        return $.Deferred(function (dfd) {
            $.get(ocsQueryUrl).then(function (json) {
                var pageZones = json.pageContents[0],
                    tickerZone = pageZones[pageZones.length - 1],
                    items = [];

                // Sports Ticker zone is defined by; last zone, hidden, has content
                if (tickerZone.theme === 'hidden' && tickerZone.zoneContents.length) {
                    $.each(tickerZone.zoneContents, function (index, container) {
                        var card = container.containerContents[0].cardContents;

                        items.push({
                            sectionName: card.cardSectionName,
                            sectionUrl: card.sectionUri,
                            title: card.headlinePlainText,
                            url: card.url
                        });
                    });

                    dfd.resolve.apply(null, items);
                } else {
                    dfd.reject();
                }
            }, dfd.reject);
        }).promise();
    }

    /**
     * Makes a request to the Hypatia API end point
     * for each of our desired sections and pulls the
     * latest document from each of these.
     *
     * @returns {object} jQuery promise object
     * @private
     */
    function _getLatestItems() {
        return $.Deferred(function (dfd) {
            var requests = [];

            $.each(CNN.SportsTicker.sections, function (sectionKey, sectionName) {
                var requestPromise = $.Deferred(function (dfd) {
                    $.ajax({
                        cache: true,
                        url: CNN.SportsTicker.apiBaseUrl + hypatiaQueryUrl.replace('{section}', sectionKey),
                        dataType: 'jsonp',
                        jsonpCallback: sectionKey + 'Ticker',
                        success: function (json) {
                            var data = json.docs[0];
                            dfd.resolve({
                                sectionName: sectionName,
                                title: data.title,
                                url: data.url
                            });
                        }
                    });
                }).promise();

                // Add request to templated query url with desired section added in
                requests.push(requestPromise);
            });

            $.when.apply(null, requests).done(dfd.resolve);
        }).promise();
    }

    /**
     * Inject the ticker items into the ticker.
     *
     * @private
     */
    function _renderTickerItems() {
        var listItems = $.map(arguments, function (item) {
            var sectionUrl = item.sectionUrl || ('/sport/' + item.sectionName.toLowerCase().replace(' ', '-')),
                trackingQuery = '?cid=sportsticker';

            return '<li class="m-sports-ticker__list-item">' +
                '<a class="m-sports-ticker__section-link" href="' + sectionUrl + trackingQuery + '">' + item.sectionName + '</a>' +
                '<a class="m-sports-ticker__article-link" href="' + item.url + trackingQuery + '">' + item.title + '</a>' +
            '</li>';
        });

        $tickerList.html(listItems);
        $ticker.addClass('m-sports-ticker--loaded');

        if (listItems.length > 1) {
            _animateTickerItems();
        }
    }

    /**
     * Animates the up scrolling of ticker items.
     *
     * @private
     */
    function _animateTickerItems() {
        var animateOffset = $ticker.height(),
            animateDelay = 5000,
            animationTimeout,
            animateStep;

        animateStep = function () {
            $tickerList.animate({
                marginTop: '-' + animateOffset
            }, 250, function () {
                $tickerList.css('marginTop', 0)
                    .children().first().appendTo($tickerList);

                animationTimeout = setTimeout(animateStep, animateDelay);
            });
        };

        $ticker.on('mouseleave', function () {
            animationTimeout = setTimeout(animateStep, animateDelay * 0.20);
        }).on('mouseenter', function () {
            animationTimeout = clearTimeout(animationTimeout);
            $tickerList.stop();
        });

        animationTimeout = setTimeout(animateStep, animateDelay);
    }

    /**
     * Kicks off the initialisation of this
     * Sports Ticker widget.
     *
     * @private
     */
    function _init() {
        $.when(_getTickerItems()).done(_renderTickerItems);
    }

    // Setup module variables and init hook
    $(document).onZonesAndDomReady(function () {
        // Hook to disable ticker if needed
        if ($.isPlainObject(CNN.SportsTicker)) {
            _init();
        }
    });

}(jQuery, window, document));

'use strict';

/**
 * New Relic agent configuration.
 *
 * See lib/config.defaults.js in the agent distribution for a more complete
 * description of configuration variables and their potential values.
 *
 * @param {string} env - environment name
 * @returns {string} level descriptor
 */
function logLevel(env) {
    var level = 'trace';

    switch (env) {
    case 'train':
    case 'prod':
        level = 'fatal';
        break;

    case 'dev':
    case 'enable':
    case 'politics':
    case 'ref':
    case 'stage':
    case 'sweet':
    case 'terra':
    case 'video':
        level = 'trace';
        break;

    default:
        level = 'trace';
        break;
    }

    return level;
}

exports.config = {
    /**
     * Array of application names.
     */
    app_name: ['Michonne ' + process.env.ENVIRONMENT],
    /**
     * Your New Relic license key.
     */
    license_key: '99479c2f49dfd416800d26c990cd5f8bf90cf06c',
    logging: {
        /**
         * Level at which to log. 'trace' is most useful to New Relic when diagnosing
         * issues with the agent, 'info' and higher will impose the least overhead on
         * production applications.
         */
        level: logLevel(process.env.ENVIRONMENT)
    }
};

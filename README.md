# CNN Michonne 
This is a [Node][node] web application for serving CNN.com.

[ ![Codeship Status for vgtf/cnn-michonne-app](https://www.codeship.com/projects/20e3cca0-0f82-0132-1c34-2204b975dd06/status?branch=master)](https://www.codeship.com/projects/32648)

## Table of Contents
- tl;dr
- System Requirements
- How to...
    - Install Node via nvm
    - Install needed npm packages
    - Install Redis
    - Install cnn-michonne-app
    - Generate Documentation
    - Build for different environments
    - Run Selenium functional tests on localhost
    - Run Selenium functional tests on SauceLabs
- Test it is working
- File structure
- npm scripts
- Other useful information
    - Useful Selenium websites
- Deployment

---

## tl;dr
**In terminal window 1, start up redis-server**
```shell
$ redis-server
```

**In terminal window 2, start up cnn-michonne-app**
```shell
$ cd ~/dev
$ git clone git@bitbucket.org:vgtf/cnn-michonne-app.git
$ cd cnn-michonne-app
$ nvm use
$ npm install
$ source set-env/local.conf
$ npm run build
$ npm start
```

**In your favorite browser**
Go to [http://localhost.next.cnn.com:8080](http://localhost.next.cnn.com:8080)


## System Requirements
This app is normally developed on OS X and runs on Unix systems.  In addition
to having a current version of OS X, Xcode, and the developer command line
tools and all that comes along with that installed, you will also need to
have the following:

- [Node][node] 6.10.x - It is recommended that you use [nvm][nvm]
- Some global node packages. See below for details.

**NOTE**: We *highly* recommend you use nvm.  By using nvm you will not need to
use `sudo` to install npm packages, the instructions here will work a lot
better, and your life will generally be less stressful working with all these
Node things.

**MAC OSX NOTE**:  On a Mac running Yosemite (OSX 10.10.*) or newer, you may run
into problems with too low of a setting for maxfiles causing problems during
npm install.  This will generally manifest itself with several errors during
`npm install` with the message "Cannot read property 'dependencies' of
undefined".  To address this problem, check your current system maxfiles
setting with:
```shell
$ ulimit -n
```
If the value returned is less then 1024, you should increase it.  To do so, do
the following:
```shell
$ sudo launchctl limit maxfiles 2048 unlimited
```
You will likely need to restart your terminal windows (close ALL windows and
completely exit the terminal program, then restart it) before it will pick up the
changes.  If you want to change this permanently, with "sudo" add the following
file to your Mac with path/name `/Library/LaunchDaemons/limit.maxfiles.plist`:
```xml
<?xml version="1.0" encoding="UTF-8"?>  
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN"  
        "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">  
  <dict>
    <key>Label</key>
    <string>limit.maxfiles</string>
    <key>ProgramArguments</key>
    <array>
      <string>launchctl</string>
      <string>limit</string>
      <string>maxfiles</string>
      <string>2048</string>
      <string>unlimited</string>
    </array>
    <key>RunAtLoad</key>
    <true/>
    <key>ServiceIPC</key>
    <false/>
  </dict>
</plist>
```
Then make sure the file is owned by user "root", group "wheel" with 644
permissions by doing:
```shell
$ sudo chown root:wheel /Library/LaunchDaemons/limit.maxfiles.plist
$ sudo chmod 644 /Library/LaunchDaemons/limit.maxfiles.plist
```
Then reboot your Mac and when it restarts everything will by honky-dory.


## How to ...
### Install Node via nvm
If you do not have nvm installed, you can do so with the following.  It is
recommended that you check the [nvm github repo][nvm] to verify the current
version.
```shell
$ curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.32.1/install.sh | bash
```

Once nvm is installed you can install a current version of node with the
following.  Check the [node releases](https://nodejs.org/en/download/releases/)
page to determine the most current 0.10.x version.
```shell
$ nvm install 6.10.2
$ nvm alias default 6.10.2
```


### Install needed npm packages
If you installed node via nvm you will not need to `sudo` these.
```shell
$ nvm use
$ npm install --global grunt-cli
$ npm install --global eslint
```


### Install cnn-michonne-app
```shell
$ cd ~/dev
$ git clone git@bitbucket.org:vgtf/cnn-michonne-app.git
$ cd cnn-michonne-app
$ nvm use
$ npm install
$ source set-env/local.conf
$ npm run build
```


### Generate Documentation
```shell
$ npm run generate-docs
$ open docs/*/*/index.html
```


### Build for different environments
The environment you are running for is based on the environment conf file that
you source before running `npm start`.

For example, to run a domestic instance in the "local" environment (which
connects to a PAL instance running locally on your system, no matter what
environment it might be using):
```shell
$ source set-env/local.conf
$ npm start
```
To run in the "ref" environment and connect to that PAL environment:
```shell
$ source set-env/ref.conf
$ npm start
```
To run an international (or edition) instance in the "enable" environment:
```shell
$ source set-env/international-enable.conf
$ npm start
```
To run a "local" domestic preview server:
```shell
$ source set-env/prev-local.conf
$ npm start
```


### Run the Selenium functional tests on localhost
This will require a few terminal windows.

**Terminal Window 1 - Start Michonne**
```shell
$ cd path/to/cnn-michonne-app
$ nvm use
$ npm install
$ source set-env/dev.conf
$ npm run build
$ npm run start
```

**Terminal Window 2 - Start Selenium**
```shell
$ cd path/to/cnn-michonne-app
$ npm run stat-selenium
```

**Terminal Window 3 - Run the tests**
```shell
$ cd path/to/cnn-michonne-app
$ npm run test-selenium --silent
```


### Run the Selenium functional tests on SauceLabs
This will require a few terminal windows. You must also be on the VPN.

First, install [SauceConnect][sauceconnect].

**For OS X**
```shell
$ curl -O https://saucelabs.com/downloads/sc-4.4.1-osx.zip
$ unzip sc-4.4.1-osx.zip
```

**For Linux**
```shell
$ curl -O https://saucelabs.com/downloads/sc-4.4.1-linux.tar.gz
$ tar xvzf sc-4.4.1-linux.tar.gz
```


Next, start it up, and run the tests.

**Terminal Window 1 - Start Michonne**
```shell
$ cd path/to/cnn-michonne-app
$ nvm use
$ npm install
$ source set-env/dev.conf
$ npm run build
$ npm run start
```

**Terminal Window 2 - Start SauceConnect**
```shell
$ cd path/to/cnn-michonne-app
$ source saucelabs.conf
$ ulimit -n 8000
$ path/to/bin/sc
```

**Terminal Window 3 - Run the tests**
```shell
$ cd path/to/cnn-michonne-app
$ nvm use
$ source saucelabs.conf
$ npm run test-selenium --silent
```


## npm scripts
You can see all of the npm scripts by doing `$ npm run` in your terminal.  The
most important ones that you will want to use are probably:

- `npm install` - installs the cnn-michonne-app
- `npm run build` - builds Michonne for local testing
- `npm test` - runs the unit tests



## Other useful Information
### Useful Selenium Websites
**IMPORTANT** - The official Selenium WebDriver for JavaScript, commonly
referred to as _WebDriverJS_ and listed in the npm repository as
[selenium-webdriver][npm-selenium-webdriver] is *NOT* the same as the
_WebDriverJS_ that is listed in the npm repository as
[webdriverjs][npm-webdriverjs].  You will find lots of documentation referring
to `WebDriverJS` that is not referring to the official selenium-webdriver that
we are using.  Here are some useful links that are for the official
selenium-webdriver that we are using.

- [Selenium WebDriver API for JavaScript](http://selenium.googlecode.com/git/docs/api/javascript/index.html)
- [Selenium WebDriverJS User's Guide](https://code.google.com/p/selenium/wiki/WebDriverJs)
- [selenium-webdriver npm repository](https://www.npmjs.org/package/selenium-webdriver)
- [StackOverflow answer with good links](http://stackoverflow.com/questions/5644868/webdriver-selenium-2-api-documentation)
- [StackOverflow answer with good example](http://stackoverflow.com/questions/20773772/mocha-may-be-deferring-chai-expectations)


## Deployment
Details on the process used to deploy to any upstream environment (DEV, REF,
STAGE, TRAIN, ESAND, PROD) can be found in these two documents.

- [MSS Expansion Release Process](https://gist.github.com/DrEnter/f2c0db44d934eb5b4d0c)
- [CNN.com Production Deployment Information](https://gist.github.com/jamsyoung/422646b0a0e9feb857e2)


[node]: http://nodejs.org/
[npm-selenium-webdriver]: https://www.npmjs.org/package/selenium-webdriver
[npm-webdriverjs]: https://www.npmjs.org/package/webdriverjs
[nvm]: https://github.com/creationix/nvm
[sauceconnect]: https://docs.saucelabs.com/reference/sauce-connect/

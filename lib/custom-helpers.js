/* global config */

'use strict';

(function () {
    var uniqueId = 0;

    /**
    * This helper function succeeds if the provided URL is relative (has no host component)
    * @param {string} url - Url to be examined
    * @returns {boolean} true if has a host and false if it doesn't have a host
    */

    function isRelativeUrl(url) {
        return (typeof url === 'string' && url.search(/^(http\:|https\:)?\/\//) === -1);
    }

    function install() {
        var dust = require('dustjs-helpers');

        /**
         * This helper function can be used in dust files as
         * {@contains value="{key}" item1="value1" item2="value2"}
         *  - or -
         * {@contains value="{key}" items=array_reference}
         * {/contains}
         *
         * @memberof helpers
         * @param {object} chunk - Current template render output chunk
         * @param {object} context - Current context stack
         * @param {object} bodies - Body sections
         * @param {object} params - Helper params passed-in
         * @returns {object} Resulting output chunk after processing
         */
        dust.helpers.contains = function (chunk, context, bodies, params) {
            var value = context.resolve(params.value),
                item = '',
                items = context.resolve(params.items),
                found = false,
                p;

            if (typeof value !== 'undefined') {
                if (items && Array.isArray(items)) {
                    found = (items.indexOf(value) !== -1);
                } else {
                    for (p in params) {
                        if (p === 'value') { continue; }
                        item = context.resolve(params[p]);
                        found = (item === value) ? true : false;
                        if (found === true) {
                            break;
                        }
                    }
                }
            }

            if (found === true) {
                return bodies.block(chunk, context.push(context.stack.index));
            } else if (bodies.else) {
                return bodies.else(chunk, context.push(context.stack.index));
            }
            return chunk;
        };


        /**
         * This helper function can be used in dust files as
         * {@inArray value="{key}" items=array_reference}
         * {/inArray}
         *
         * @memberof helpers
         * @param {object} chunk - Current template render output chunk
         * @param {object} context - Current context stack
         * @param {object} bodies - Body sections
         * @param {object} params - Helper params passed-in
         * @returns {object} Resulting output chunk after processing
         */
        dust.helpers.inArray = function (chunk, context, bodies, params) {
            var value = context.resolve(params.value),
                items = context.resolve(params.items),
                found = false;

            if (typeof value !== 'undefined' && Array.isArray(items) && items.length !== 0) {
                found = (items.indexOf(value) !== -1);
            }

            if (found === true) {
                return bodies.block(chunk, context);
            } else if (bodies.else) {
                return bodies.else(chunk, context);
            }
            return chunk;
        };


        /**
         * This helper function can validate the given layout availablity.
         * If the given layout is available in the list, helper will return the same layout,
         * else will return the default layout based on page type.
         * {@layoutLoader pageType="{pageType}" currentLayout="{layout}" }
         * {/layoutLoader}
         *
         * @memberof helpers
         * @param {object} chunk - Current template render output chunk
         * @param {object} context - Current context stack
         * @param {object} bodies - Body sections
         * @param {object} params - Helper params passed-in
         * @returns {object} Resulting output chunk after processing
         */
        dust.helpers.layoutLoader = function (chunk, context, bodies, params) {
            var defaultLayouts,
                defaultLayout = '',
                layout = context.resolve(params.currentLayout),
                layouts,
                found = false,
                p,
                pageType = context.resolve(params.pageType),
                q;

            if (config && config.runtime && config.runtime.layouts && config.runtime.pages) {
                layouts = config.runtime.layouts;
                defaultLayouts = config.runtime.pages;
            } else {
                return chunk.render(bodies.block, context.push({pageLayout: layout}));
            }
            for (p in layouts) {
                if (layout === layouts[p]) {
                    found = true;
                    break;
                }
            }

            if (found) {
                return chunk.render(bodies.block, context.push({pageLayout: layout}));
            } else {
                /* Load default layout based on runtime config */
                for (q in defaultLayouts) {
                    if (pageType === defaultLayouts[q].pageType) {
                        defaultLayout = defaultLayouts[q].layout;
                        break;
                    }
                }
                return chunk.render(bodies.block, context.push({pageLayout: defaultLayout ? defaultLayout : 'no-rail'}));
            }
        };


        /**
         * This helper function is used to validate the given URL is Partner URL or non partner URLs.
         * Non partner URLs are configured in runtme configuration, so based on this validation
         * "data-vr-track" attribute will be added for Partner URL <a> tags.
         *
         * @memberof helpers
         * @param {object} chunk - Current template render output chunk
         * @param {object} context - Current context stack
         * @param {object} bodies - Body sections
         * @param {object} params - Helper params passed-in
         * @returns {object} Resulting output chunk after processing
         */
        dust.helpers.externalUrl = function (chunk, context, bodies, params) {
            var url = context.resolve(params.url),
                urlHost;

            urlHost = (typeof url === 'string') ?
                url.replace(/^(?:http\:|https\:)?\/\/([\w\-\.]+)(?:\:\d+)?.*$/, '$1') : '';

            if (urlHost !== url && global.config.runtime.cnnHosts.indexOf(urlHost) < 0) {
                return bodies.block(chunk, context.push(context.stack.index));
            }
            return chunk;
        };


        /**
         * This helper function succeeds if the provided URL is relative (has no host component)
         *
         * @memberof helpers
         * @param {object} chunk - Current template render output chunk
         * @param {object} context - Current context stack
         * @param {object} bodies - Body sections
         * @param {object} params - Helper params passed-in
         * @returns {object} Resulting output chunk after processing
         */
        dust.helpers.relativeUrl = function (chunk, context, bodies, params) {
            var url = context.resolve(params.url);

            if (isRelativeUrl(url)) {
                return bodies.block(chunk, context.push(context.stack.index));
            } else if (bodies.else) {
                return bodies.else(chunk, context.push(context.stack.index));
            }
            return chunk;
        };


        /**
         * This helper function succeeds if the provided vertical, section, and pageType
         * are match for the timer object.  Note that this populates an object (pageTimer,
         * adTimers, or eventTimers) with the matching timer(s) and pushes into the top of
         * the current context.
         *
         * @memberof helpers
         * @param {object} chunk - Current template render output chunk
         * @param {object} context - Current context stack
         * @param {object} bodies - Body sections
         * @param {object} params - Helper params passed-in
         * @returns {object} Resulting output chunk after processing
         */
        dust.helpers.getTimers = function (chunk, context, bodies, params) {
            var key,
                match = 0,
                matchCurrent,
                matches = {},
                name = '',
                pType = context.get('pageType'),
                sect = context.get('sectionName'),
                timer,
                timers = context.resolve(params.timers),
                ttype = context.resolve(params.timerType) || 'page',
                vert = context.get('vertical');

            /* Scan the runtime config timer values passed-in via "timers" */
            for (key in timers) {
                if (timers.hasOwnProperty(key) && key !== 'default') {  /* Ignore "default" for now */
                    timer = timers[key];
                    /* If the timer has a vertical, pageType, or sectionName set, see if they match
                       the current page's (in vert, pType, and sect) */
                    matchCurrent = 0;
                    if (typeof timer.vertical === 'string' && timer.vertical.length > 0) {
                        if (timer.vertical !== vert) {
                            continue;
                        }
                        matchCurrent = 1;
                    }
                    if (typeof timer.pageType === 'string' && timer.pageType.length > 0) {
                        if (timer.pageType !== pType) {
                            continue;
                        }
                        matchCurrent++;
                    }
                    if (typeof timer.sectionName === 'string' && timer.sectionName.length > 0) {
                        if (timer.sectionName !== sect) {
                            continue;
                        }
                        matchCurrent++;
                    }
                    matchCurrent++;  /* If nothing was set, we are still a match */
                    if (ttype === 'page') {
                        /* Set the page timer to the best match */
                        if (matchCurrent > match) {
                            match = matchCurrent;
                            name = key;
                        }
                    } else {
                        /* Include all matches in the ad/event timer objects */
                        matches[key] = timers[key];
                        match++;
                    }
                }
            }
            if (ttype === 'page') {
                /* Return a single page timer object */
                if (match === 0 && typeof timers.default === 'object' &&
                    typeof timers.default.interval === 'number' && timers.default.interval > 0) {

                    name = 'default';
                }
                if (name.length > 0) {
                    return chunk.render(bodies.block, context.push({pageTimer: timers[name]}));
                } else if (bodies.else) {
                    return bodies.else(chunk, context.push(context.stack.index));
                }
            } else {
                /* Return the ad or event timer objects for this page */
                if (typeof timers.default === 'object' && timers.default !== null &&
                    typeof timers.default.interval === 'number' && timers.default.interval > 0) {
                    matches.default = timers.default;
                    match++;
                }
                if (ttype === 'ad') {
                    return chunk.render(bodies.block, context.push({adTimers: matches}));
                } else {
                    return chunk.render(bodies.block, context.push({eventTimers: matches}));
                }
            }
            return chunk;
        };


        /**
         * This helper function succeeds if the provided vertical, section, and pageType
         * are match for the provided parameters.
         *
         * Either a match object or three array parameters (verticals, sections, pageTypes) may
         * be passed in.  If a matchObject is used, it would contain up to three properties
         * of the same names as the other three parameters.  Each paramter is an array of
         * strings to check against that value.
         *
         * @memberof helpers
         * @param {object} chunk - Current template render output chunk
         * @param {object} context - Current context stack
         * @param {object} bodies - Body sections
         * @param {object} params - Helper params passed-in
         * @returns {object} Resulting output chunk after processing
         */
        dust.helpers.matchPage = function (chunk, context, bodies, params) {
            var curPType = context.get('pageType') || '',
                curSect = context.get('sectionName') || '',
                curVert = context.get('vertical') || 'main',
                matchIt,
                matchObj = context.resolve(params.match) || null,
                pTypes = (matchObj && matchObj.pageTypes) || context.resolve(params.pageTypes) || null,
                sects = (matchObj && matchObj.sections) || context.resolve(params.sections) || null,
                verts = (matchObj && matchObj.verticals) || context.resolve(params.verticals) || null;

            /* Function to quickly match item in include/exclude list */
            matchIt = function (item, list) {
                var i,
                    exclItem = '-' + item;

                for (i = 0; i < list.length; i++) {
                    if (list[i] === '*' || list[i] === item) {
                        return true;
                    } else if (list[i] === exclItem) {
                        return false;
                    }
                }
                return false;
            };

            /* Normalize parameters (split strings on whitespace) before each test */
            if (typeof verts === 'string' && verts.length !== 0) {
                verts = verts.split(/\s+/);
            }

            /* If we are matching verticals and we don't match, or we match exclude list, return false now */
            if (Array.isArray(verts) && verts.length > 0 && matchIt(curVert, verts) === false) {
                if (bodies.else) {
                    return bodies.else(chunk, context.push(context.stack.index));
                }
                return chunk;
            }

            /* If we are matching pageTypes and we don't match, or we match exclude list, return false now */
            if (typeof pTypes === 'string' && pTypes.length !== 0) {
                pTypes = pTypes.split(/\s+/);
            }
            if (Array.isArray(pTypes) && pTypes.length > 0 && matchIt(curPType, pTypes) === false) {
                if (bodies.else) {
                    return bodies.else(chunk, context.push(context.stack.index));
                }
                return chunk;
            }

            /* If we are matching sections and we don't match, or we match exclude list, return false now */
            if (typeof sects === 'string' && sects.length !== 0) {
                sects = sects.split(/\s+/);
            }
            if (Array.isArray(sects) && sects.length > 0 && matchIt(curSect, sects) === false) {
                if (bodies.else) {
                    return bodies.else(chunk, context.push(context.stack.index));
                }
                return chunk;
            }

            /* We match, so return true */
            return bodies.block(chunk, context.push(context.stack.index));
        };


        /**
         * This helper function appends the host to the URL when var-use-absolute-urls
         * is set to true.  The url can be passed in or be text within the tag
         * (bodies.block) note that the body url takes priority over the param url
         *
         * @memberof helpers
         * @param {object} chunk - Current template render output chunk
         * @param {object} context - Current context stack
         * @param {object} bodies - Body sections
         * @param {object} params - Helper params passed-in (var-use-absolute-urls,
         * url, and host)
         * @returns {object} Resulting output chunk after processing
         */
        dust.helpers.navUrlRenderer = function (chunk, context, bodies, params) {
            var url = '/';

            if (params.url) {
                url = params.url;
            }
            if (params.absolute === 'true' && typeof params.host === 'string' && isRelativeUrl(url) === true) {
                url = params.host + url;
            }
            return chunk.write(url);
        };


        /**
         * This helper function makes sure the passed-in URL has a protocol and
         * outputs it.
         *
         * @memberof helpers
         * @param {object} chunk - Current template render output chunk
         * @param {object} context - Current context stack
         * @param {object} bodies - Body sections
         * @param {object} params - Helper params passed-in (url and proto)
         * @returns {object} Resulting output chunk after processing
         */
        dust.helpers.protocolize = function (chunk, context, bodies, params) {
            var force = (params.force === 'true') ? true : false,
                host = params.host || '',
                proto = params.proto || 'http',
                url = params.url || '/';

            if (url.charAt(0) === '/' && url.charAt(1) === '/') {
                url = proto + ':' + url;
            } else if (url.charAt(0) === '/' && url.charAt(1) !== '/' && host.length !== 0) {
                if (host.charAt(0) !== '/') {
                    url = proto + '://' + host + url;
                } else {
                    url = proto + ':' + host + url;
                }
            } else if (force) {
                url = url.replace(/^(http|https)(:\/\/.+)$/, proto + '$2');
            }
            return chunk.write(url);
        };


        /**
         * This helper function converts a string to lowercase
         *
         * @memberof helpers
         * @param {object} chunk - Current template render output chunk
         * @param {object} context - Current context stack
         * @param {object} bodies - Body sections
         * @param {object} _params - Helper params passed-in (var-use-absolute-urls,
         * @returns {string} lowercase
        */
        dust.helpers.toLowerCase = function (chunk, context, bodies, _params) {
            return chunk.capture(bodies.block, context, function (string, chunk) {
                chunk.end(string.toLowerCase());
            });
        };

        /**
         * This helper function evaluates whether the FAVE (Future Aligned Video Experience) Library
         * should be enabled or not.
         * The helper returns true if:
         * The feature is enabled AND
         * exclude is a member of enabledPageTypes, but the pageType and sectionName provided don't match the page.
         * Else if:
         * exclude is not a member of enabledPageTypes AND
         * The Trinity config enabledPageTypes, passed in as a param, is an empty object
         * OR
         * enabledPageTypes has the given pageType as a property AND that property is an array AND
         * the array is empty OR has an index containing the sectionName
         *
         * @memberof helpers
         * @param {object} chunk - Current template render output chunk
         * @param {object} context - Current context stack
         * @param {object} bodies - Body sections
         * @param {object} params - Helper params passed-in
         * @returns {boolean} Resulting output chunk after processing
         */
        dust.helpers.enableFave = function (chunk, context, bodies, params) {
            var enabledPageTypes = context.resolve(params.enabledPageTypes) || {},
                featureEnabled = context.resolve(params.featureEnabled) || false,
                pageType = context.get('pageType'),
                sectionName = context.get('sectionName'),
                pageTypeAndSectionNameCheck = function (root) {
                    return (root.hasOwnProperty(pageType) && Array.isArray(root[pageType]) &&
                    (root[pageType].length === 0 || root[pageType].indexOf(sectionName) !== -1));
                };

            if (featureEnabled === true) {
                if (enabledPageTypes.hasOwnProperty('exclude')) {
                    if (pageTypeAndSectionNameCheck(enabledPageTypes.exclude)) {
                        if (bodies.else) {
                            return bodies.else(chunk, context.push(context.stack.index));
                        }
                    } else {
                        return bodies.block(chunk, context.push(context.stack.index));
                    }
                } else if (Object.keys(enabledPageTypes).length === 0 || pageTypeAndSectionNameCheck(enabledPageTypes)) {
                    return bodies.block(chunk, context.push(context.stack.index));
                }
            } else if (bodies.else) {
                return bodies.else(chunk, context.push(context.stack.index));
            }
        };

        /**
         * This helper function injects the _uniqueId key into the current context set to a unique
         * ID value for the enclosed block.  This will be a hexadecimal string, prefixed by any
         * string passed-in with the optional "base" parameter.
         *
         * {@withUniqueId} code that uses {._uniqueId} ... {/withUniqueId}
         * {@withUniqueId base="xyz"} code that uses {._uniqueId} ... {/withUniqueId}
         *
         * @memberof helpers
         * @param {object} chunk - Current template render output chunk
         * @param {object} context - Current context stack
         * @param {object} bodies - Body sections
         * @param {object} params - Helper params passed-in
         * @returns {object} Resulting output chunk after processing
         */
        dust.helpers.withUniqueId = function (chunk, context, bodies, params) {
            var topContext = context.pop() || {},
                newId = (params.base && typeof params.base === 'string' && params.base.length > 0) ? params.base : '';

            newId += uniqueId.toString(16);
            topContext._uniqueId = newId;
            uniqueId++;
            return chunk.render(bodies.block, context.push(topContext));
        };

        return dust;
    }

    install();
    module.exports = install;

}());

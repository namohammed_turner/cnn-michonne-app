'use strict';

/**
 * Useful stuff.
 *
 * @module useful
 */

const
    ipaddr = require('ipaddr.js'),
    logLevelName = process.env.LOG_LEVEL || 'silly',
    logLevels = {
        silly: 0b00000001,
        debug: 0b00000011,
        verbose: 0b00000111,
        info: 0b00001111,
        warn: 0b00011111,
        error: 0b00111111,
        fatal: 0b01111111,
        important: 0b11111111
    },
    logLevel = logLevels[logLevelName],
    Url = require('url');

var
    lastTS = 0,
    tsIndex = 0;


/**
 * Check specified logLevel against current level
 *
 * @function
 * @public
 *
 * @param {string} level - log level to check
 * @returns {boolean} - true if level will log, false if not
 */
function willLog(level) {
    return (logLevels[level] & logLevel) !== 0;
}


/**
 * Get difference between hrtime value and current hrtime, in milliseconds
 *
 * @function
 * @public
 *
 * @param {array} start - start time returned from hrtime to use
 * @returns {number} - time difference in milliseconds (float)
 */
function getHrDiff(start) {
    let diff = process.hrtime(start);

    return (diff[0] * 1000) + (diff[1] / 1000000);
}


/**
 * Convert unsigned integer to string of desired length and radix
 *
 * @function
 * @public
 *
 * @param {Uint32Array} val - Value to convert (0 = low bits, 1 = high bits)
 * @param {number} [radix] - Radix value (2, 8, 10, 16), default is 10
 * @param {number} [len] - Length to 0-pad the result to
 * @returns {string} - Resulting string
 */
function ulongToString(val, radix, len) {
    let res = val[0].toString(radix);

    if (val[1] !== 0) {
        if (res.length < 8) {
            res = '0'.repeat(8 - res.length) + res;
        }
        res = val[1].toString(radix) + res;
    }
    if (len && res.length < len) {
        res = '0'.repeat(len - res.length) + res;
    }
    return res;
}


/**
 * Get request serial value
 *
 * @function
 * @public
 *
 * @param {object} req - Request object to generate serial for
 * @returns {string} - Generated serial value
 */
function getRequestSerial(req) {
    let
        addr = ipaddr.parse(req.socket.localAddress).toByteArray().slice(-4).reduce(function (p, c) {
            return (p << 8) | c;
        }, 0),
        ts = process.hrtime()[0];

    if (ts === lastTS) {
        tsIndex++;
    } else {
        tsIndex = 0;
        lastTS = ts;
    }
    ts = ((ts & 0xffff) << 16) | tsIndex;

    return ulongToString(new Uint32Array([ts >>> 0, addr >>> 0]));
}


/**
 * Function to recursively descend an object specification using a dot-notation string and return the value, if any.
 *
 * @function
 * @public
 *
 * @param {string} spec - Object specification
 * @param {object} parent - Parent object
 * @returns {string} Resulting value if string else empty string
 */
function parseObjectName(spec, parent) {
    var ppos,
        prop;

    if (spec.length > 0 && typeof parent === 'object' && parent !== null) {
        ppos = spec.indexOf('.');
        if (ppos === -1) {
            return (typeof parent[spec] === 'string') ? parent[spec] : '';
        }
        prop = spec.substring(0, ppos);
        return parseObjectName(spec.substring(ppos + 1), parent[prop]);
    }
    return '';
}


/**
 * Is URL valid?
 *
 * @function
 * @public
 *
 * @param {string} url - URL to test for validity
 * @param {boolean} [acceptSlashes] - Are URLs that start with "//:" valid?
 * @returns {boolean} - true is valid, false if not
 */
function isValidUrl(url, acceptSlashes) {
    if (typeof url === 'string' && url.length !== 0) {
        let urlObj = Url.parse(url, false, acceptSlashes);

        return (urlObj && urlObj.hostname) ? true : false;
    }
    return false;
}


/**
 * Add protocol to URL, if necessary
 *
 * @function
 * @public
 *
 * @param {string} url - Possibly protocol-less URL
 * @param {string} proto - protocol to use (http or https)
 * @returns {string} - URL with protocol
 */
function protocolize(url, proto) {
    if (url.charAt(0) === '/' && url.charAt(1) === '/') {
        return proto + ':' + url;
    } else if (proto.length === 5 && url.charAt(0) === 'h' && url.charAt(1) === 't' &&
        url.charAt(2) === 't' && url.charAt(3) === 'p' && url.charAt(4) === ':') {

        return 'https' + url.slice(4);
    }
    return url;
}


module.exports = {
    willLog: willLog,
    getHrDiff: getHrDiff,
    ulongToString: ulongToString,
    getRequestSerial: getRequestSerial,
    parseObjectName: parseObjectName,
    isValidUrl: isValidUrl,
    protocolize: protocolize
};


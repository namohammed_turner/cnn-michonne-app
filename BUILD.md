# Build / Deployment Details
Builds are done on Codeship when commits are made to any branch.
Production deployments are done when builds are completed on the master branch.
Test deployments are done when builds are completed on the develop branch.


## Links
- Codeship - https://codeship.com/projects/32648
- Codeship Setup/Test - https://codeship.com/projects/32648/configure_tests
- Codeship Deployment - https://codeship.com/projects/32648/deployment_branches/36176
- Argo Deployments
  - http://deployit.services.dmtio.net/#deployments/cnn-michonne-domestic
  - http://deployit.services.dmtio.net/#deployments/cnn-michonne-domestic-preview
  - http://deployit.services.dmtio.net/#deployments/cnn-michonne-international
  - http://deployit.services.dmtio.net/#deployments/cnn-michonne-international-preview
- Healthchecks
  - REF DOMESTIC: http://ref.next.cnn.com/_healthcheck
  - REF INTL: http://edition.ref.next.cnn.com/_healthcheck
  - STAGE DOMESTIC: http://stage.next.cnn.com/_healthcheck
  - STAGE INTL: http://edition.stage.next.cnn.com/_healthcheck
  - PROD DOMESTIC: http://www.cnn.com/_healthcheck
  - PROD INTL: http://edition.cnn.com/_healthcheck


## Codeship scripts
These scripts are copies of what is on Codeship.


#### Setup
```shell
## A copy of this script is in the repository in BUILD.md.
## If you make changes directly on Codeship, make sure to
## check in the changes to the repository.  It is actually
## best to update the one in source control and copy/paste
## the script into the Codeship page.

## Maintained by james.young@turner.com


#### SETUP ####

## Clean npm cache
npm cache clean
rm -rf ~/clone/node_modules

## Setup Ruby
rvm use 2.1.5

## Install most current npm
npm install --global npm
npm --version

## Install Grunt
npm install --global grunt-cli

## Configure Node Envrionment
npm config set geoip-lite:update false

## Install Package
npm install

## Build Package (Production)
export BUILD_ENV=$(build-scripts/process_branch.sh)
grunt build-ci
```


#### Test Pipeline 1
```shell
#### Pipeline 1 ####

## Run Tests
source set-env/${BUILD_ENV}.conf
grunt test

## Generate Documentation
npm run generate-docs

## Copy documentation out for uploading to documentation server later
cp -r docs ~/
```


#### Deploy Step 1
```shell
## A copy of this script is in the repository in BUILD.md.
## If you make changes directly on Codeship, make sure to
## check in the changes to the repository.  It is actually
## best to update the one in source control and copy/paste
## the script into the Codeship page.

## Maintained by james.young@turner.com


## Variables
NOW=$(date +"%Y%m%d%H%M%S")
ARTIFACT_FILENAME=artifact.tgz
ARTIFACT_PACKAGE_NAME=michonne-app
NODE_ENGINE_VERSION=$(jq -r '.engines.node' package.json)
PACKAGE_NAME=$(jq -r '.name' package.json)
PACKAGE_VERSION=$(jq -r '.version' package.json)
QA_TAG=$(build-scripts/process_tag.sh)
SEMVER=${PACKAGE_VERSION}-${CI_BUILD_NUMBER}${QA_TAG}
TARBALL_FILENAME=${PACKAGE_NAME}-${SEMVER}.${NOW}.tar.gz

## used in step 2
export S3_ARTIFACT_BUCKET_NAME=cnn/${ARTIFACT_PACKAGE_NAME}

## used in step 3
export ARTIFACT_NAME=cnn-michonne

## Update version in package.json
jq ".version = \"${SEMVER}\"" package.json > modified && mv modified package.json

## Clean out files not needed in the artifact
grunt cleanUp:prepareForArtifact

# Change directory to home
cd ~

## Get Node and place binaries
curl -O "http://nodejs.org/dist/v${NODE_ENGINE_VERSION}/node-v${NODE_ENGINE_VERSION}-linux-x64.tar.gz"
tar xzf "node-v${NODE_ENGINE_VERSION}-linux-x64.tar.gz"
mkdir clone/bin
cp "node-v${NODE_ENGINE_VERSION}-linux-x64/bin/node" clone/bin/node
cp "node-v${NODE_ENGINE_VERSION}-linux-x64/bin/npm" clone/bin/npm

## Create the tarball
tar czf "${ARTIFACT_FILENAME}" -C "src/bitbucket.org/vgtf/${PACKAGE_NAME}" .

## Create the artifact directory, this is required due to the
## way the Codeship S3 deployment mechanism works.
mkdir artifact

## Move the artifact to the artifact directory, this is
## required due to the way the Codeship S3 deployment
## mechanism works.
mv "${ARTIFACT_FILENAME}" "artifact/${TARBALL_FILENAME}"

## Get the checksum - used in step 3
export CHECKSUM=$(openssl dgst -sha256 "artifact/${TARBALL_FILENAME}" | awk '{print $2}')
```


#### Deploy Step 2
This is encrypted.  Speak with James Young for the key to decrypt it.

```
U2FsdGVkX182RxMI3leVjPvX2F10Ez2pZb/uDDeftgNjFrrCVW66dwP9eU6BbEfT
/flIi1CoCJNSmSnrwfl6kCnxBIN8KewlQGIbktPgKmzv9b1zi2iKUy6SurNc0C6q
G3mAJ1ik+fq4npl0DSwYnbuZr5WJOxluFNAunWvbRgtvtrLT4XdWTMVGXskmrogZ
UtGV14+xyRpHSWMUWXPsosIwe0ahj0umvnmRBse2CjWWV2lhSr0Pz9zFJPn8K8o2
cbq7jhHGOMqELuH1pgeUcIvr/qUPZfJnVNvtL8OiN/Mh4yojyWMyZd6J5l4gQk4C
1xxYSEdtk4UpGLb+csheelarVttmzXE5+ppKp73+FpTLqDNITpe12Sn2MrLKhqHk
onQ8MYHCe7064xKAzfl+CD182Ot79l/a0VRes9gdecDQDOIEFpEkhntFxOLBIBlo
JgeY/SOtKmUevqS/JPp787iK8QafGlp+W+f2WfH4uzqTInMjHNr9gan2hLalKf/b
sl7gWyeTUW2062fTQubEfIu3S5TghM+tdROOHgiZm73BS5SVxAHX8Fa9N1727ZK3
EhT+W2KMGAUSUTKrujZLwXbOhefTBuMYodwtU+fNVZ2YW6+WuQaEAso3mSs0XJig
lEQOkRe03oXAApcmWCmGH6f/qyMDxv/XfHWjOwpO9qF+2j4nqqUF2NH4HXoYQvG9
GtnDT8JWkZHpVKSehgsZAba2L5pw5ANpBTJGESoKQvPCBC5WIHF0CjdxcagtR4m9
v+JWnL/mZRYG5sVbWR8F4x8fJGfNZev1fXC0VnrDvFKlP7az13V+OMc0wt5aDgZV
```


#### Deploy Step 3
```shell
## A copy of this script is in the repository in BUILD.md.
## If you make changes directly on Codeship, make sure to
## check in the changes to the repository.  It is actually
## best to update the one in source control and copy/paste
## the script into the Codeship page.

## Maintained by james.young@turner.com


## POST to baton
curl -H "Content-Type: application/json" -sv -X POST http://baton.outturner.io/deploy/request --data "{\"buildName\": \"${CI_NAME}\", \"branchName\": \"${CI_BRANCH}\", \"artifactPackageName\": \"${ARTIFACT_PACKAGE_NAME}\", \"artifactName\": \"${ARTIFACT_NAME}\", \"packageVersion\": \"${SEMVER}\", \"packageTarballName\": \"${TARBALL_FILENAME}\", \"packageChecksum\": \"${CHECKSUM}\"}"

## -sv = --silent --verbose
## -X  = --request POST
## -H  = --header
## -d  = --data
```


#### Deploy Step 4
This is encrypted.  Speak with James Young for the key to decrypt it.

```
U2FsdGVkX18EUa2ei1okpcXilwLv4duS8qiiKa7nAZbAq5gDLoCWPMc5Nz6s4dgK
ApM+vGt+dddAU2AepcTEI+lXnMXiQS8EwSMhL9FV/0mmXlDTlt7EQKaRM+qS9PPr
lfgk/+zaM/1S1iS89YnhdxeFZHmDHWheFS/xqz5doGWAHKIOULnjQ0z7rbegEIq6
+lp4Cd5MrAvm8JmJg6j/die3NPj7INBrKiWIWnfUpSAhuSGHXKVyN8wOjP4QHiJ5
c/scgQMrc3FZ6G5JGZZA6Pcv1acY8AsG05vxFw///7r/aLnqoUe+Gz0HjuCsnR1E
HpywTr3tHYcLq4vfGOyYslKsoqIyVDRtzhU41GPSFd9k6R7aepMYhnnD9KsKCi4F
eQUhtE1FojmXcxVjX7bGVli93VyZIunfAN0Cp9p7oeIxH7ZlsZ6F0MZVMIC8FL6G
OdFjEf7HlcgeqRMwW7Ldynv4Y8R2pJjxvBdJzbo5GAlLL0K4wJxvjV3WIzkc7xGN
/iUIKpHhBmYbkFkar2Z+cR2n5fB3FIl8B7StCN5cZvY9mn9gw/x3PGG3Z1tgfc4R
YRON6WFLvLdIc+BNJvDBk/X//gTk6eDFAYJnNuifuJH9IEIrWM7mqSzHNPjvh2Op
TIAPKUlw54xfJRhvy9qUFhOkMxQJ1BnmzuJb8BDhs2QKwaq9R9NjeCKqkDHPFbUX
jYnA54RzZ0pbS2kf1Eug3iQb5K0fqFB2f8fPOwT62iTJ9uDkF19jeEmU0x+FaLTi
npGJsrnG7zr64Xff+NLR+3HVTOj+s9famTfk4tbPMOAn64Pl0NloCeuOYyJ9cH24
jRCUIi8+G5cue+0hsryegVs1N0C0MXzLQ5YwqzIUgzc=
```


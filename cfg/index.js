'use strict';

var config = {
    // --- default settings (aka DOMESTIC PROD)
    'default': {
        blogs: {
            connecttheworld: {
                modelType: 'show',
                parent: '/shows/connect-the-world',
                section: 'world'
            },
            crossfire: {
                section: 'tv',
                modelType: 'show',
                parent: '/shows/crossfire'
            },
            geekout: {
                section: 'living'
            },
            inamerica: {
                section: 'us'
            },
            johnkingusa: {
                section: 'us'
            },
            religion: {
                section: 'tv',
                modelType: 'list',
                parent: '/living/belief-blog'
            },
            schoolsofthought: {
                section: 'us'
            },
            startingpoint: {
                section: 'tv'
            },
            strombo: {
                section: 'us'
            },
            whatsnext: {
                section: 'tech'
            },
            whitehouse: {
                section: 'politics'
            },
            yourbottomline: {
                section: 'tv'
            }
        },
        companionMinBreakpoints: 'small:0, large:768',
        config: 'default',
        cdn: {
            assetPath: '/.a',
            assetHost: '//www.i.cdn.cnn.com',
            baseZip: '//z.cdn.turner.com',
            baseChrome: '//i.cdn.cnn.com',
            chrome: '//i.cdn.cnn.com/cnn',
            content: '//i2.cdn.cnn.com/cnnnext/dam/assets',
            proxyContent: '',
            zip: '//z.cdn.turner.com/cnn'
        },
        dataPaths: {
            config: '/cfg/v1/domestic/prod/michonne.json',  // LSD_CONFIG_PATH
            homepage: '/v1/prod/domestic/homepage.json',  // LSD_HOMEPATH_PATH
            overrides: '/cfg/v1/domestic/prod/michonne-overrides.json',
            routes: '/cfg/v1/domestic/prod/routing.json',  // LSD_ROUTES_PATH
            sectionconfig: '/cfg/v1/domestic/prod/sectionconfig.json', // LSD_SECTIONCONFIG_PATH
            zones: '/cfg/v1/domestic/prod/zones.json' // LSD_ZONES_PATH
        },
        dataTimeout: 5000,
        dataTtl: 60000,  // LSD_DATA_TTL
        edition: 'domestic',
        editionCookie: 'PreferredEdition', // changing the name to reset cookie (SelectedEdition was used in Red site)
        errorTemplates: {
            'default': 'views/pages/errors/default',
            403: 'views/pages/errors/403',
            404: 'views/pages/errors/404'
        },
        flags: {
            enableHttp2: false,
            localTLS: false,
            usingCDN: true
        },
        host: {
            port: '',
            sslPort: '',
            data: '//data.cnn.com',  // LSD_HOST_URL
            nowPlayingDataHost: '//data.cnn.com',
            www: '//www.cnn.com',
            us: '//us.cnn.com',
            intl: '//edition.cnn.com',
            domain: '//www.cnn.com',
            domainWww: '//www.cnn.com',
            domainIntl: '//edition.cnn.com',
            domainPreview: '//www.preview.cnn.com'
        },
        hostnames: {
            altHost: 'us.cnn.com',
            assetHost: 'www.i.cdn.cnn.com',
            mainHost: 'www.cnn.com',
            previewHost: 'www.preview.cnn.com',
            sponsorHost: 'sponsorcontent.cnn.com',
            theSourceHost: 'thesource.prod.cnn.io',
            travelHost: 'darwin.cnn-travel-vertical.ui.cnn.io'
        },
        isTest: false,
        localPalContentProxy: {
            dev: 'http://cnn-dev-cms3-dist-dam1.cnn.vgtf.net:8080/dam/assets',
            enable: 'http://cnn-enbl-cms3-clump.cop.vgtf.net/dam/assets',
            esand: 'http://cnn-esand-cms3-dist-dam1.56m.vgtf.net:8080/dam/assets',
            local: 'http://cnn-dev-cms3-dist-dam1.cnn.vgtf.net:8080/dam/assets',
            politics: 'http://cnn-pol-cms3-clump.cop.vgtf.net/dam/assets',
            prod: '',
            ref: 'http://cnn-ref-cms3-clump.56m.vgtf.net/dam/assets',
            stage: '',
            sweet: 'http://cnn-swt-cms3-clump.cop.vgtf.net/dam/assets',
            terra: 'http://cnn-ter-cms3-clump.cop.vgtf.net/dam/assets',
            train: 'http://cnn-train-cms3-clump.56m.vgtf.net/dam/assets',
            video: 'http://cnn-vid-cms3-clump.cop.vgtf.net/dam/assets'
        },
        livefyre: {
            logoUrl: 'http://zor.livefyre.com/wjs/v1.0/images/icons/poweredbylivefyre.png'
        },
        palTimeout: 12000,  // 12 seconds
        previewDataTtl: 1,
        searchApp: {
            searchUrl: '/search/',
            queryUrl: 'http://searchapp.cnn.com/search/query.jsp'
        },
        maxConnections: 1000,
        maxSockets: 2000,
        s3: {
            sponsorContent: '//s3.amazonaws.com/cnn-sponsored-content'
        },
        truste: {
            noticeUrl: '//consent.truste.com/notice?domain=cnn.com&c=m-truste&text=true',
            modalUrl: '//preferences.truste.com/webservices/js?domain=turner.com&type=turner&js=responsive'
        },
        trustedAddrs: {
            internal: ['10.0.0.0/8', '157.166.0.0/16', '168.161.97.0/24', '172.16.0.0/12'],
            partners: ['10.0.0.0/8', '157.166.0.0/16', '168.161.97.0/24', '172.16.0.0/12', '4.71.33.160/27', '50.31.185.32/27', '64.74.232.32/27', '64.202.112.0/27', '66.225.223.0/27', '74.201.198.64/27', '74.217.148.96/27'],
            proxies: ['10.0.0.0/8', '172.16.0.0/12']
        }
    }, // end of default

    // Start of DOMESTIC configurations
    domestic: {

        // --- DOMESTIC LOCAL unique settings
        local: {
            config: 'domestic-local',
            cdn: {
                assetPath: '/.asset',
                assetHost: '',
                content: '/dam/assets',
                proxyContent: 'http://cnn-dev-cms3-dist-dam1.cnn.vgtf.net:8080/dam/assets'
            },
            dataPaths: {
                config: '/cfg/v1/domestic/local/michonne.json',
                homepage: '/v1/dev/domestic/homepage.json',
                overrides: '/cfg/v1/domestic/local/michonne-overrides.json',
                routes: '/cfg/v1/domestic/local/routing.json',
                sectionconfig: '/cfg/v1/domestic/local/sectionconfig.json',
                zones: '/cfg/v1/domestic/local/zones.json'
            },
            flags: {
                localTLS: true,
                usingCDN: false
            },
            host: {
                port: ':8080',
                sslPort: ':8443',
                www: '//localhost.next.cnn.com',
                us: '//localhost.next.cnn.com',
                intl: '//localhost.next.cnn.com',
                domain: '//localhost.next.cnn.com',
                domainWww: '//localhost.next.cnn.com',
                domainIntl: '//edition.dev.next.cnn.com',
                domainPreview: '//localhost.next.cnn.com'
            },
            hostnames: {
                altHost: '',
                assetHost: '',
                mainHost: 'localhost.next.cnn.com',
                previewHost: 'localhost.next.cnn.com',
                sponsorHost: 'sponsorcontent.localhost.next.cnn.com',
                theSourceHost: 'cnn-the-source.dev.services.ec2.dmtio.net',
                travelHost: 'darwin-dev.cnn-travel-vertical.ui.cnn.io'
            },
            isTest: true,
            sourceMaps: {
                outSourceMap: true,
                sourceRoot: ''
            },
            searchApp: {
                queryUrl: 'http://searchapp.dev.cnn.com/search/query.jsp'
            }
        },

        // --- DOMESTIC DEV unique settings
        dev: {
            config: 'domestic-dev',
            cdn: {
                assetPath: '/.asset',
                assetHost: '',
                content: '/dam/assets',
                proxyContent: 'http://cnn-dev-cms3-dist-dam1.cnn.vgtf.net:8080/dam/assets'
            },
            dataPaths: {
                config: '/cfg/v1/domestic/dev/michonne.json',
                homepage: '/v1/dev/domestic/homepage.json',
                overrides: '/cfg/v1/domestic/dev/michonne-overrides.json',
                routes: '/cfg/v1/domestic/dev/routing.json',
                sectionconfig: '/cfg/v1/domestic/dev/sectionconfig.json',
                zones: '/cfg/v1/domestic/dev/zones.json'
            },
            flags: {
                localTLS: false,
                usingCDN: false
            },
            host: {
                www: '//dev.next.cnn.com',
                us: '//dev.next.cnn.com',
                intl: '//edition.dev.next.cnn.com',
                domain: '//dev.next.cnn.com',
                domainWww: '//dev.next.cnn.com',
                domainIntl: '//edition.dev.next.cnn.com',
                domainPreview: '//preview.dev.next.cnn.com'
            },
            hostnames: {
                altHost: '',
                assetHost: '',
                mainHost: 'dev.next.cnn.com',
                previewHost: 'preview.dev.next.cnn.com',
                sponsorHost: 'sponsorcontent.dev.next.cnn.com',
                theSourceHost: 'cnn-the-source.dev.services.ec2.dmtio.net',
                travelHost: 'darwin-dev.cnn-travel-vertical.ui.cnn.io'
            },
            sourceMaps: {
                outSourceMap: true,
                sourceRoot: ''
            },
            searchApp: {
                queryUrl: 'http://searchapp.dev.cnn.com/search/query.jsp'
            }
        },

        // --- DOMESTIC REF unique settings
        ref: {
            config: 'domestic-ref',
            cdn: {
                assetPath: '/.asset',
                assetHost: '',
                content: '/dam/assets',
                proxyContent: 'http://cnn-ref-cms3-clump.56m.vgtf.net/dam/assets'
            },
            dataPaths: {
                config: '/cfg/v1/domestic/ref/michonne.json',
                homepage: '/v1/ref/domestic/homepage.json',
                overrides: '/cfg/v1/domestic/ref/michonne-overrides.json',
                routes: '/cfg/v1/domestic/ref/routing.json',
                sectionconfig: '/cfg/v1/domestic/ref/sectionconfig.json',
                zones: '/cfg/v1/domestic/ref/zones.json'
            },
            flags: {
                localTLS: false,
                usingCDN: false
            },
            host: {
                www: '//ref.next.cnn.com',
                us: '//ref.next.cnn.com',
                intl: '//edition.ref.next.cnn.com',
                domain: '//ref.next.cnn.com',
                domainWww: '//ref.next.cnn.com',
                domainIntl: '//edition.ref.next.cnn.com',
                domainPreview: '//preview.ref.next.cnn.com'
            },
            hostnames: {
                altHost: '',
                assetHost: '',
                mainHost: 'ref.next.cnn.com',
                previewHost: 'preview.ref.next.cnn.com',
                sponsorHost: 'sponsorcontent.ref.next.cnn.com',
                theSourceHost: 'cnn-the-source.ref.services.ec2.dmtio.net',
                travelHost: 'darwin-ref.cnn-travel-vertical.ui.cnn.io'
            },
            sourceMaps: {
                outSourceMap: true,
                sourceRoot: ''
            },
            searchApp: {
                queryUrl: 'http://searchapp.ref.cnn.com/search/query.jsp'
            }
        },

        // --- DOMESTIC VIDEO QA unique settings
        video: {
            config: 'domestic-video',
            cdn: {
                assetPath: '/.asset',
                assetHost: '',
                content: '/dam/assets',
                proxyContent: 'http://cnn-vid-cms3-clump.cop.vgtf.net/dam/assets'
            },
            dataPaths: {
                config: '/cfg/v1/domestic/video/michonne.json',
                homepage: '/v1/video/domestic/homepage.json',
                overrides: '/cfg/v1/domestic/video/michonne-overrides.json',
                routes: '/cfg/v1/domestic/video/routing.json',
                sectionconfig: '/cfg/v1/domestic/video/sectionconfig.json',
                zones: '/cfg/v1/domestic/video/zones.json'
            },
            flags: {
                localTLS: false,
                usingCDN: false
            },
            host: {
                www: '//video.next.cnn.com',
                us: '//video.next.cnn.com',
                intl: '//edition.video.next.cnn.com',
                domain: '//video.next.cnn.com',
                domainWww: '//video.next.cnn.com',
                domainIntl: '//edition.video.next.cnn.com',
                domainPreview: '//preview.video.next.cnn.com'
            },
            hostnames: {
                altHost: '',
                assetHost: '',
                mainHost: 'video.next.cnn.com',
                previewHost: 'preview.video.next.cnn.com',
                sponsorHost: 'sponsorcontent.video.next.cnn.com',
                theSourceHost: 'cnn-the-source.dev.services.ec2.dmtio.net',
                travelHost: 'darwin-dev.cnn-travel-vertical.ui.cnn.io'
            },
            sourceMaps: {
                outSourceMap: true,
                sourceRoot: ''
            },
            searchApp: {
                queryUrl: 'http://searchapp.ref.cnn.com/search/query.jsp'
            }
        },

        // --- DOMESTIC ENABLE QA unique settings
        enable: {
            config: 'domestic-enable',
            cdn: {
                assetPath: '/.asset',
                assetHost: '',
                content: '/dam/assets',
                proxyContent: 'http://cnn-enbl-cms3-clump.cop.vgtf.net/dam/assets'
            },
            dataPaths: {
                config: '/cfg/v1/domestic/enable/michonne.json',
                homepage: '/v1/enable/domestic/homepage.json',
                overrides: '/cfg/v1/domestic/enable/michonne-overrides.json',
                routes: '/cfg/v1/domestic/enable/routing.json',
                sectionconfig: '/cfg/v1/domestic/enable/sectionconfig.json',
                zones: '/cfg/v1/domestic/enable/zones.json'
            },
            flags: {
                localTLS: false,
                usingCDN: false
            },
            host: {
                www: '//enable.next.cnn.com',
                us: '//enable.next.cnn.com',
                intl: '//edition.enable.next.cnn.com',
                domain: '//enable.next.cnn.com',
                domainWww: '//enable.next.cnn.com',
                domainIntl: '//edition.enable.next.cnn.com',
                domainPreview: '//preview.enable.next.cnn.com'
            },
            hostnames: {
                altHost: '',
                assetHost: '',
                mainHost: 'enable.next.cnn.com',
                previewHost: 'preview.enable.next.cnn.com',
                sponsorHost: 'sponsorcontent.enable.next.cnn.com',
                theSourceHost: 'cnn-the-source.dev.services.ec2.dmtio.net',
                travelHost: 'darwin-dev.cnn-travel-vertical.ui.cnn.io'
            },
            sourceMaps: {
                outSourceMap: true,
                sourceRoot: ''
            },
            searchApp: {
                queryUrl: 'http://searchapp.ref.cnn.com/search/query.jsp'
            }
        },

        // --- DOMESTIC POLITICS QA unique settings
        politics: {
            config: 'domestic-politics',
            cdn: {
                assetPath: '/.asset',
                assetHost: '',
                content: '/dam/assets',
                proxyContent: 'http://cnn-pol-cms3-clump.cop.vgtf.net/dam/assets'
            },
            dataPaths: {
                config: '/cfg/v1/domestic/politics/michonne.json',
                homepage: '/v1/politics/domestic/homepage.json',
                overrides: '/cfg/v1/domestic/politics/michonne-overrides.json',
                routes: '/cfg/v1/domestic/politics/routing.json',
                sectionconfig: '/cfg/v1/domestic/politics/sectionconfig.json',
                zones: '/cfg/v1/domestic/politics/zones.json'
            },
            flags: {
                localTLS: false,
                usingCDN: false
            },
            host: {
                www: '//politics.next.cnn.com',
                us: '//politics.next.cnn.com',
                intl: '//edition.politics.next.cnn.com',
                domain: '//politics.next.cnn.com',
                domainWww: '//politics.next.cnn.com',
                domainIntl: '//edition.politics.next.cnn.com',
                domainPreview: '//preview.politics.next.cnn.com'
            },
            hostnames: {
                altHost: '',
                assetHost: '',
                mainHost: 'politics.next.cnn.com',
                previewHost: 'preview.politics.next.cnn.com',
                sponsorHost: 'sponsorcontent.politics.next.cnn.com',
                theSourceHost: 'cnn-the-source.dev.services.ec2.dmtio.net',
                travelHost: 'darwin-dev.cnn-travel-vertical.ui.cnn.io'
            },
            sourceMaps: {
                outSourceMap: true,
                sourceRoot: ''
            },
            searchApp: {
                queryUrl: 'http://searchapp.ref.cnn.com/search/query.jsp'
            }
        },

        // --- DOMESTIC SWEET QA unique settings
        sweet: {
            config: 'domestic-sweet',
            cdn: {
                assetPath: '/.asset',
                assetHost: '',
                content: '/dam/assets',
                proxyContent: 'http://cnn-swt-cms3-clump.cop.vgtf.net/dam/assets'
            },
            dataPaths: {
                config: '/cfg/v1/domestic/sweet/michonne.json',
                homepage: '/v1/sweet/domestic/homepage.json',
                overrides: '/cfg/v1/domestic/sweet/michonne-overrides.json',
                routes: '/cfg/v1/domestic/sweet/routing.json',
                sectionconfig: '/cfg/v1/domestic/sweet/sectionconfig.json',
                zones: '/cfg/v1/domestic/sweet/zones.json'
            },
            flags: {
                localTLS: false,
                usingCDN: false
            },
            host: {
                www: '//sweet.next.cnn.com',
                us: '//sweet.next.cnn.com',
                intl: '//edition.sweet.next.cnn.com',
                domain: '//sweet.next.cnn.com',
                domainWww: '//sweet.next.cnn.com',
                domainIntl: '//edition.sweet.next.cnn.com',
                domainPreview: '//preview.sweet.next.cnn.com'
            },
            hostnames: {
                altHost: '',
                assetHost: '',
                mainHost: 'sweet.next.cnn.com',
                previewHost: 'preview.sweet.next.cnn.com',
                sponsorHost: 'sponsorcontent.sweet.next.cnn.com',
                theSourceHost: 'cnn-the-source.dev.services.ec2.dmtio.net',
                travelHost: 'darwin-dev.cnn-travel-vertical.ui.cnn.io'
            },
            sourceMaps: {
                outSourceMap: true,
                sourceRoot: ''
            },
            searchApp: {
                queryUrl: 'http://searchapp.ref.cnn.com/search/query.jsp'
            }
        },

        // --- DOMESTIC TERRA unique settings
        terra: {
            config: 'domestic-terra',
            cdn: {
                assetPath: '/.asset',
                assetHost: '',
                content: '/dam/assets',
                proxyContent: 'http://cnn-ter-cms3-clump.cop.vgtf.net/dam/assets'
            },
            dataPaths: {
                config: '/cfg/v1/domestic/terra/michonne.json',
                homepage: '/v1/terra/domestic/homepage.json',
                overrides: '/cfg/v1/domestic/terra/michonne-overrides.json',
                routes: '/cfg/v1/domestic/terra/routing.json',
                sectionconfig: '/cfg/v1/domestic/terra/sectionconfig.json',
                zones: '/cfg/v1/domestic/terra/zones.json'
            },
            flags: {
                localTLS: false,
                usingCDN: false
            },
            host: {
                www: '//terra.next.cnn.com',
                us: '//terra.next.cnn.com',
                intl: '//edition.terra.next.cnn.com',
                domain: '//terra.next.cnn.com',
                domainWww: '//terra.next.cnn.com',
                domainIntl: '//edition.terra.next.cnn.com',
                domainPreview: '//preview.terra.next.cnn.com'
            },
            hostnames: {
                altHost: '',
                assetHost: '',
                mainHost: 'terra.next.cnn.com',
                previewHost: 'preview.terra.next.cnn.com',
                sponsorHost: 'sponsorcontent.terra.next.cnn.com',
                theSourceHost: 'cnn-the-source.dev.services.ec2.dmtio.net',
                travelHost: 'darwin-dev.cnn-travel-vertical.ui.cnn.io'
            },
            sourceMaps: {
                outSourceMap: true,
                sourceRoot: ''
            },
            searchApp: {
                queryUrl: 'http://searchapp.ref.cnn.com/search/query.jsp'
            }
        },

        // --- DOMESTIC TRAIN unique settings
        train: {
            config: 'domestic-train',
            cdn: {
                assetPath: '/.asset',
                assetHost: '',
                content: '/dam/assets',
                proxyContent: 'http://cnn-train-cms3-clump.56m.vgtf.net/dam/assets'
            },
            dataPaths: {
                config: '/cfg/v1/domestic/train/michonne.json',
                homepage: '/v1/train/domestic/homepage.json',
                overrides: '/cfg/v1/domestic/train/michonne-overrides.json',
                routes: '/cfg/v1/domestic/train/routing.json',
                sectionconfig: '/cfg/v1/domestic/train/sectionconfig.json',
                zones: '/cfg/v1/domestic/train/zones.json'
            },
            flags: {
                localTLS: false,
                usingCDN: false
            },
            host: {
                www: '//train.next.cnn.com',
                us: '//train.next.cnn.com',
                intl: '//edition.train.next.cnn.com',
                domain: '//train.next.cnn.com',
                domainWww: '//train.next.cnn.com',
                domainIntl: '//edition.train.next.cnn.com',
                domainPreview: '//preview.train.next.cnn.com'
            },
            hostnames: {
                altHost: '',
                assetHost: '',
                mainHost: 'train.next.cnn.com',
                previewHost: 'preview.train.next.cnn.com',
                sponsorHost: 'sponsorcontent.train.next.cnn.com',
                theSourceHost: 'cnn-the-source.ref.services.ec2.dmtio.net',
                travelHost: 'darwin-dev.cnn-travel-vertical.ui.cnn.io'
            },
            sourceMaps: {
                outSourceMap: true,
                sourceRoot: ''
            },
            searchApp: {
                queryUrl: 'http://searchapp.ref.cnn.com/search/query.jsp'
            }
        },

        // --- DOMESTIC ESAND unique settings
        esand: {
            config: 'domestic-esand',
            cdn: {
                assetPath: '/.asset',
                assetHost: '',
                content: '/dam/assets',
                proxyContent: 'http://cnn-esand-cms3-dist-dam1.56m.vgtf.net:8080/dam/assets'
            },
            dataPaths: {
                config: '/cfg/v1/domestic/esand/michonne.json',
                homepage: '/v1/esand/domestic/homepage.json',
                overrides: '/cfg/v1/domestic/esand/michonne-overrides.json',
                routes: '/cfg/v1/domestic/esand/routing.json',
                sectionconfig: '/cfg/v1/domestic/esand/sectionconfig.json',
                zones: '/cfg/v1/domestic/esand/zones.json'
            },
            flags: {
                localTLS: false,
                usingCDN: false
            },
            host: {
                www: '//etrain.next.cnn.com',
                us: '//etrain.next.cnn.com',
                intl: '//edition.etrain.next.cnn.com',
                domain: '//etrain.next.cnn.com',
                domainWww: '//etrain.next.cnn.com',
                domainIntl: '//edition.etrain.next.cnn.com',
                domainPreview: '//preview.etrain.next.cnn.com'
            },
            hostnames: {
                altHost: '',
                assetHost: '',
                mainHost: 'etrain.next.cnn.com',
                previewHost: 'preview.etrain.next.cnn.com',
                sponsorHost: 'sponsorcontent.etrain.next.cnn.com',
                theSourceHost: 'cnn-the-source.ref.services.ec2.dmtio.net',
                travelHost: 'darwin-dev.cnn-travel-vertical.ui.cnn.io'
            },
            searchApp: {
                queryUrl: 'http://searchapp.ref.cnn.com/search/query.jsp'
            }
        },

        // --- DOMESTIC STAGE unique settings
        stage: {
            config: 'domestic-stage',
            cdn: {
                assetHost: '//stage.www.i.cdn.cnn.com'
            },
            dataPaths: {
                config: '/cfg/v1/domestic/stage/michonne.json',
                homepage: '/v1/stage/domestic/homepage.json',
                overrides: '/cfg/v1/domestic/stage/michonne-overrides.json',
                routes: '/cfg/v1/domestic/stage/routing.json',
                sectionconfig: '/cfg/v1/domestic/stage/sectionconfig.json',
                zones: '/cfg/v1/domestic/stage/zones.json'
            },
            flags: {
                localTLS: false,
                usingCDN: true
            },
            host: {
                www: '//stage.next.cnn.com',
                us: '//stage.next.cnn.com',
                intl: '//edition.stage.next.cnn.com',
                domain: '//stage.next.cnn.com',
                domainWww: '//stage.next.cnn.com',
                domainIntl: '//edition.stage.next.cnn.com',
                domainPreview: '//preview.stage.next.cnn.com'
            },
            hostnames: {
                altHost: '',
                assetHost: 'stage.www.i.cdn.cnn.com',
                mainHost: 'stage.next.cnn.com',
                previewHost: '',
                sponsorHost: 'sponsorcontent.stage.next.cnn.com',
                theSourceHost: 'cnn-the-source.stage.services.ec2.dmtio.net'
            }
        }
    },

    // INTERNATIONAL configs
    international: {

        // --- INTERNATIONAL LOCAL unique settings
        local: {
            config: 'international-local',
            cdn: {
                assetPath: '/.asset',
                assetHost: '',
                content: '/dam/assets',
                proxyContent: 'http://cnn-dev-cms3-dist-dam1.cnn.vgtf.net:8080/dam/assets'
            },
            dataPaths: {
                config: '/cfg/v1/international/local/michonne.json',
                homepage: '/v1/dev/international/homepage.json',
                overrides: '/cfg/v1/international/local/michonne-overrides.json',
                routes: '/cfg/v1/international/local/routing.json',
                sectionconfig: '/cfg/v1/international/local/sectionconfig.json',
                zones: '/cfg/v1/international/local/zones.json'
            },
            edition: 'international',
            flags: {
                localTLS: true,
                usingCDN: false
            },
            host: {
                port: ':8080',
                sslPort: ':8443',
                www: '//localhost.next.cnn.com',
                us: '//localhost.next.cnn.com',
                intl: '//localhost.next.cnn.com',
                domain: '//localhost.next.cnn.com',
                domainWww: '//dev.next.cnn.com',
                domainIntl: '//localhost.next.cnn.com',
                domainPreview: '//localhost.next.cnn.com'
            },
            hostnames: {
                altHost: '',
                assetHost: '',
                mainHost: 'localhost.next.cnn.com',
                previewHost: 'localhost.next.cnn.com',
                sponsorHost: 'sponsorcontent.localhost.next.cnn.com',
                theSourceHost: 'cnn-the-source.dev.services.ec2.dmtio.net',
                travelHost: 'darwin-dev.cnn-travel-vertical.ui.cnn.io'
            },
            isTest: true,
            sourceMaps: {
                outSourceMap: true,
                sourceRoot: ''
            },
            searchApp: {
                queryUrl: 'http://searchapp.dev.cnn.com/search/query.jsp'
            }
        },

        // --- INTERNATIONAL DEV unique settings
        dev: {
            config: 'international-dev',
            cdn: {
                assetPath: '/.asset',
                assetHost: '',
                content: '/dam/assets',
                proxyContent: 'http://cnn-dev-cms3-dist-dam1.cnn.vgtf.net:8080/dam/assets'
            },
            dataPaths: {
                config: '/cfg/v1/international/dev/michonne.json',
                homepage: '/v1/dev/international/homepage.json',
                overrides: '/cfg/v1/international/dev/michonne-overrides.json',
                routes: '/cfg/v1/international/dev/routing.json',
                sectionconfig: '/cfg/v1/international/dev/sectionconfig.json',
                zones: '/cfg/v1/international/dev/zones.json'
            },
            edition: 'international',
            flags: {
                localTLS: false,
                usingCDN: false
            },
            host: {
                www: '//dev.next.cnn.com',
                us: '//dev.next.cnn.com',
                intl: '//edition.dev.next.cnn.com',
                domain: '//edition.dev.next.cnn.com',
                domainWww: '//dev.next.cnn.com',
                domainIntl: '//edition.dev.next.cnn.com',
                domainPreview: '//preview.edition.dev.next.cnn.com'
            },
            hostnames: {
                altHost: '',
                assetHost: '',
                mainHost: 'edition.dev.next.cnn.com',
                previewHost: 'preview.edition.dev.next.cnn.com',
                sponsorHost: 'sponsorcontent.edition.dev.next.cnn.com',
                theSourceHost: 'cnn-the-source.dev.services.ec2.dmtio.net',
                travelHost: 'darwin-dev.cnn-travel-vertical.ui.cnn.io'
            },
            sourceMaps: {
                outSourceMap: true,
                sourceRoot: ''
            },
            searchApp: {
                queryUrl: 'http://searchapp.dev.cnn.com/search/query.jsp'
            }
        },

        // --- INTERNATIONAL REF unique settings
        ref: {
            config: 'international-ref',
            cdn: {
                assetPath: '/.asset',
                assetHost: '',
                content: '/dam/assets',
                proxyContent: 'http://cnn-ref-cms3-clump.56m.vgtf.net/dam/assets'
            },
            dataPaths: {
                config: '/cfg/v1/international/ref/michonne.json',
                homepage: '/v1/ref/international/homepage.json',
                overrides: '/cfg/v1/international/ref/michonne-overrides.json',
                routes: '/cfg/v1/international/ref/routing.json',
                sectionconfig: '/cfg/v1/international/ref/sectionconfig.json',
                zones: '/cfg/v1/international/ref/zones.json'
            },
            edition: 'international',
            flags: {
                localTLS: false,
                usingCDN: false
            },
            host: {
                www: '//ref.next.cnn.com',
                us: '//ref.next.cnn.com',
                intl: '//edition.ref.next.cnn.com',
                domain: '//edition.ref.next.cnn.com',
                domainWww: '//ref.next.cnn.com',
                domainIntl: '//edition.ref.next.cnn.com',
                domainPreview: '//preview.edition.ref.next.cnn.com'
            },
            hostnames: {
                altHost: '',
                assetHost: '',
                mainHost: 'edition.ref.next.cnn.com',
                previewHost: 'preview.edition.ref.next.cnn.com',
                sponsorHost: 'sponsorcontent.edition.ref.next.cnn.com',
                theSourceHost: 'cnn-the-source.ref.services.ec2.dmtio.net',
                travelHost: 'darwin-ref.cnn-travel-vertical.ui.cnn.io'
            },
            sourceMaps: {
                outSourceMap: true,
                sourceRoot: ''
            },
            searchApp: {
                queryUrl: 'http://searchapp.ref.cnn.com/search/query.jsp'
            }
        },

        // --- INTERNATIONAL VIDEO QA unique settings
        video: {
            config: 'international-video',
            cdn: {
                assetPath: '/.asset',
                assetHost: '',
                content: '/dam/assets',
                proxyContent: 'http://cnn-vid-cms3-clump.cop.vgtf.net/dam/assets'
            },
            dataPaths: {
                config: '/cfg/v1/international/video/michonne.json',
                homepage: '/v1/video/international/homepage.json',
                overrides: '/cfg/v1/international/video/michonne-overrides.json',
                routes: '/cfg/v1/international/video/routing.json',
                sectionconfig: '/cfg/v1/international/video/sectionconfig.json',
                zones: '/cfg/v1/international/video/zones.json'
            },
            edition: 'international',
            flags: {
                localTLS: false,
                usingCDN: false
            },
            host: {
                www: '//video.next.cnn.com',
                us: '//video.next.cnn.com',
                intl: '//edition.video.next.cnn.com',
                domain: '//edition.video.next.cnn.com',
                domainWww: '//video.next.cnn.com',
                domainIntl: '//edition.video.next.cnn.com',
                domainPreview: '//preview.edition.video.next.cnn.com'
            },
            hostnames: {
                altHost: '',
                assetHost: '',
                mainHost: 'edition.video.next.cnn.com',
                previewHost: 'preview.edition.video.next.cnn.com',
                sponsorHost: 'sponsorcontent.edition.video.next.cnn.com',
                theSourceHost: 'cnn-the-source.dev.services.ec2.dmtio.net',
                travelHost: 'darwin-dev.cnn-travel-vertical.ui.cnn.io'
            },
            sourceMaps: {
                outSourceMap: true,
                sourceRoot: ''
            },
            searchApp: {
                queryUrl: 'http://searchapp.ref.cnn.com/search/query.jsp'
            }
        },

        // --- INTERNATIONAL ENABLE QA unique settings
        enable: {
            config: 'international-enable',
            cdn: {
                assetPath: '/.asset',
                assetHost: '',
                content: '/dam/assets',
                proxyContent: 'http://cnn-enbl-cms3-clump.cop.vgtf.net/dam/assets'
            },
            dataPaths: {
                config: '/cfg/v1/international/enable/michonne.json',
                homepage: '/v1/enable/international/homepage.json',
                overrides: '/cfg/v1/international/enable/michonne-overrides.json',
                routes: '/cfg/v1/international/enable/routing.json',
                sectionconfig: '/cfg/v1/international/enable/sectionconfig.json',
                zones: '/cfg/v1/international/enable/zones.json'
            },
            edition: 'international',
            flags: {
                localTLS: false,
                usingCDN: false
            },
            host: {
                www: '//enable.next.cnn.com',
                us: '//enable.next.cnn.com',
                intl: '//edition.enable.next.cnn.com',
                domain: '//edition.enable.next.cnn.com',
                domainWww: '//enable.next.cnn.com',
                domainIntl: '//edition.enable.next.cnn.com',
                domainPreview: '//preview.edition.enable.next.cnn.com'
            },
            hostnames: {
                altHost: '',
                assetHost: '',
                mainHost: 'edition.enable.next.cnn.com',
                previewHost: 'preview.edition.enable.next.cnn.com',
                sponsorHost: 'sponsorcontent.edition.enable.next.cnn.com',
                theSourceHost: 'cnn-the-source.dev.services.ec2.dmtio.net',
                travelHost: 'darwin-dev.cnn-travel-vertical.ui.cnn.io'
            },
            sourceMaps: {
                outSourceMap: true,
                sourceRoot: ''
            },
            searchApp: {
                queryUrl: 'http://searchapp.ref.cnn.com/search/query.jsp'
            }
        },

        // --- INTERNATIONAL POLITICS QA unique settings
        politics: {
            config: 'international-politics',
            cdn: {
                assetPath: '/.asset',
                assetHost: '',
                content: '/dam/assets',
                proxyContent: 'http://cnn-pol-cms3-clump.56m.vgtf.net/dam/assets'
            },
            dataPaths: {
                config: '/cfg/v1/international/politics/michonne.json',
                homepage: '/v1/politics/international/homepage.json',
                overrides: '/cfg/v1/international/politics/michonne-overrides.json',
                routes: '/cfg/v1/international/politics/routing.json',
                sectionconfig: '/cfg/v1/international/politics/sectionconfig.json',
                zones: '/cfg/v1/international/politics/zones.json'
            },
            edition: 'international',
            flags: {
                localTLS: false,
                usingCDN: false
            },
            host: {
                www: '//politics.next.cnn.com',
                us: '//politics.next.cnn.com',
                intl: '//edition.politics.next.cnn.com',
                domain: '//edition.politics.next.cnn.com',
                domainWww: '//politics.next.cnn.com',
                domainIntl: '//edition.politics.next.cnn.com',
                domainPreview: '//preview.edition.politics.next.cnn.com'
            },
            hostnames: {
                altHost: '',
                assetHost: '',
                mainHost: 'edition.politics.next.cnn.com',
                previewHost: 'preview.edition.politics.next.cnn.com',
                sponsorHost: 'sponsorcontent.edition.politics.next.cnn.com',
                theSourceHost: 'cnn-the-source.dev.services.ec2.dmtio.net',
                travelHost: 'darwin-dev.cnn-travel-vertical.ui.cnn.io'
            },
            sourceMaps: {
                outSourceMap: true,
                sourceRoot: ''
            },
            searchApp: {
                queryUrl: 'http://searchapp.ref.cnn.com/search/query.jsp'
            }
        },

        // --- INTERNATIONAL SWEET QA unique settings
        sweet: {
            config: 'international-sweet',
            cdn: {
                assetPath: '/.asset',
                assetHost: '',
                content: '/dam/assets',
                proxyContent: 'http://cnn-swt-cms3-clump.cop.vgtf.net/dam/assets'
            },
            dataPaths: {
                config: '/cfg/v1/international/sweet/michonne.json',
                homepage: '/v1/sweet/international/homepage.json',
                overrides: '/cfg/v1/international/sweet/michonne-overrides.json',
                routes: '/cfg/v1/international/sweet/routing.json',
                sectionconfig: '/cfg/v1/international/sweet/sectionconfig.json',
                zones: '/cfg/v1/international/sweet/zones.json'
            },
            edition: 'international',
            flags: {
                localTLS: false,
                usingCDN: false
            },
            host: {
                www: '//sweet.next.cnn.com',
                us: '//sweet.next.cnn.com',
                intl: '//edition.sweet.next.cnn.com',
                domain: '//edition.sweet.next.cnn.com',
                domainWww: '//sweet.next.cnn.com',
                domainIntl: '//edition.sweet.next.cnn.com',
                domainPreview: '//preview.edition.sweet.next.cnn.com'
            },
            hostnames: {
                altHost: '',
                assetHost: '',
                mainHost: 'edition.sweet.next.cnn.com',
                previewHost: 'preview.edition.sweet.next.cnn.com',
                sponsorHost: 'sponsorcontent.edition.sweet.next.cnn.com',
                theSourceHost: 'cnn-the-source.dev.services.ec2.dmtio.net',
                travelHost: 'darwin-dev.cnn-travel-vertical.ui.cnn.io'
            },
            sourceMaps: {
                outSourceMap: true,
                sourceRoot: ''
            },
            searchApp: {
                queryUrl: 'http://searchapp.ref.cnn.com/search/query.jsp'
            }
        },

        // --- INTERNATIONAL TERRA QA unique settings
        terra: {
            config: 'international-terra',
            cdn: {
                assetPath: '/.asset',
                assetHost: '',
                content: '/dam/assets',
                proxyContent: 'http://cnn-ter-cms3-clump.cop.vgtf.net/dam/assets'
            },
            dataPaths: {
                config: '/cfg/v1/international/terra/michonne.json',
                homepage: '/v1/terra/international/homepage.json',
                overrides: '/cfg/v1/international/terra/michonne-overrides.json',
                routes: '/cfg/v1/international/terra/routing.json',
                sectionconfig: '/cfg/v1/international/terra/sectionconfig.json',
                zones: '/cfg/v1/international/terra/zones.json'
            },
            edition: 'international',
            flags: {
                localTLS: false,
                usingCDN: false
            },
            host: {
                www: '//terra.next.cnn.com',
                us: '//terra.next.cnn.com',
                intl: '//edition.terra.next.cnn.com',
                domain: '//edition.terra.next.cnn.com',
                domainWww: '//terra.next.cnn.com',
                domainIntl: '//edition.terra.next.cnn.com',
                domainPreview: '//preview.edition.terra.next.cnn.com'
            },
            hostnames: {
                altHost: '',
                assetHost: '',
                mainHost: 'edition.terra.next.cnn.com',
                previewHost: 'preview.edition.terra.next.cnn.com',
                sponsorHost: 'sponsorcontent.edition.terra.next.cnn.com',
                theSourceHost: 'cnn-the-source.dev.services.ec2.dmtio.net',
                travelHost: 'darwin-dev.cnn-travel-vertical.ui.cnn.io'
            },
            sourceMaps: {
                outSourceMap: true,
                sourceRoot: ''
            },
            searchApp: {
                queryUrl: 'http://searchapp.ref.cnn.com/search/query.jsp'
            }
        },

        // --- INTERNATIONAL TRAIN unique settings
        train: {
            config: 'international-train',
            cdn: {
                assetPath: '/.asset',
                assetHost: '',
                content: '/dam/assets',
                proxyContent: 'http://cnn-train-cms3-clump.56m.vgtf.net/dam/assets'
            },
            dataPaths: {
                config: '/cfg/v1/international/train/michonne.json',
                homepage: '/v1/train/international/homepage.json',
                overrides: '/cfg/v1/international/train/michonne-overrides.json',
                routes: '/cfg/v1/international/train/routing.json',
                sectionconfig: '/cfg/v1/international/train/sectionconfig.json',
                zones: '/cfg/v1/international/train/zones.json'
            },
            edition: 'international',
            flags: {
                localTLS: false,
                usingCDN: false
            },
            host: {
                www: '//train.next.cnn.com',
                us: '//train.next.cnn.com',
                intl: '//edition.train.next.cnn.com',
                domain: '//edition.train.next.cnn.com',
                domainWww: '//train.next.cnn.com',
                domainIntl: '//edition.train.next.cnn.com',
                domainPreview: '//preview.edition.train.next.cnn.com'
            },
            hostnames: {
                altHost: '',
                assetHost: '',
                mainHost: 'edition.train.next.cnn.com',
                previewHost: 'preview.edition.train.next.cnn.com',
                sponsorHost: 'sponsorcontent.edition.train.next.cnn.com',
                theSourceHost: 'cnn-the-source.ref.services.ec2.dmtio.net',
                travelHost: 'darwin-dev.cnn-travel-vertical.ui.cnn.io'
            },
            sourceMaps: {
                outSourceMap: true,
                sourceRoot: ''
            },
            searchApp: {
                queryUrl: 'http://searchapp.ref.cnn.com/search/query.jsp'
            }
        },

        // --- INTERNATIONAL ESAND unique settings
        esand: {
            config: 'international-esand',
            cdn: {
                assetPath: '/.asset',
                assetHost: '',
                content: '/dam/assets',
                proxyContent: 'http://cnn-esand-cms3-dist-dam1.56m.vgtf.net:8080/dam/assets'
            },
            dataPaths: {
                config: '/cfg/v1/international/esand/michonne.json',
                homepage: '/v1/esand/international/homepage.json',
                overrides: '/cfg/v1/international/esand/michonne-overrides.json',
                routes: '/cfg/v1/international/esand/routing.json',
                sectionconfig: '/cfg/v1/international/esand/sectionconfig.json',
                zones: '/cfg/v1/international/esand/zones.json'
            },
            edition: 'international',
            flags: {
                localTLS: false,
                usingCDN: false
            },
            host: {
                www: '//etrain.next.cnn.com',
                us: '//etrain.next.cnn.com',
                intl: '//edition.etrain.next.cnn.com',
                domain: '//edition.etrain.next.cnn.com',
                domainWww: '//etrain.next.cnn.com',
                domainIntl: '//edition.etrain.next.cnn.com',
                domainPreview: '//preview.edition.etrain.next.cnn.com'
            },
            hostnames: {
                altHost: '',
                assetHost: '',
                mainHost: 'edition.etrain.next.cnn.com',
                previewHost: 'preview.edition.etrain.next.cnn.com',
                sponsorHost: 'sponsorcontent.edition.etrain.next.cnn.com',
                theSourceHost: 'cnn-the-source.ref.services.ec2.dmtio.net',
                travelHost: 'darwin-dev.cnn-travel-vertical.ui.cnn.io'
            },
            searchApp: {
                queryUrl: 'http://searchapp.ref.cnn.com/search/query.jsp'
            }
        },

        // --- INTERNATIONAL STAGE unique settings
        stage: {
            config: 'international-stage',
            cdn: {
                assetHost: '//stage.edition.i.cdn.cnn.com'
            },
            dataPaths: {
                config: '/cfg/v1/international/stage/michonne.json',
                homepage: '/v1/stage/domestic/homepage.json',
                overrides: '/cfg/v1/international/stage/michonne-overrides.json',
                routes: '/cfg/v1/international/stage/routing.json',
                sectionconfig: '/cfg/v1/international/stage/sectionconfig.json',
                zones: '/cfg/v1/international/stage/zones.json'
            },
            edition: 'international',
            flags: {
                localTLS: false,
                usingCDN: true
            },
            host: {
                www: '//stage.next.cnn.com',
                us: '//stage.next.cnn.com',
                intl: '//edition.stage.next.cnn.com',
                domain: '//edition.stage.next.cnn.com',
                domainWww: '//stage.next.cnn.com',
                domainIntl: '//edition.stage.next.cnn.com',
                domainPreview: '//preview.edition.stage.next.cnn.com'
            },
            hostnames: {
                altHost: '',
                assetHost: 'stage.edition.i.cdn.cnn.com',
                mainHost: 'edition.stage.next.cnn.com',
                previewHost: '',
                sponsorHost: 'sponsorcontent.edition.stage.next.cnn.com',
                theSourceHost: 'cnn-the-source.stage.services.ec2.dmtio.net'
            }
        },

        // --- INTERNATIONAL PROD unique settings
        prod: {
            config: 'international-prod',
            cdn: {
                assetHost: '//edition.i.cdn.cnn.com'
            },
            dataPaths: {
                config: '/cfg/v1/international/prod/michonne.json',
                homepage: '/v1/prod/international/homepage.json',
                overrides: '/cfg/v1/international/prod/michonne-overrides.json',
                routes: '/cfg/v1/international/prod/routing.json',
                sectionconfig: '/cfg/v1/international/prod/sectionconfig.json',
                zones: '/cfg/v1/international/prod/zones.json'
            },
            edition: 'international',
            host: {
                domain: '//edition.cnn.com',
                domainPreview: '//edition.preview.cnn.com'
            },
            hostnames: {
                altHost: '',
                assetHost: 'edition.i.cdn.cnn.com',
                mainHost: 'edition.cnn.com',
                previewHost: 'edition.preview.cnn.com',
                sponsorHost: 'sponsorcontent.edition.cnn.com'
            }
        }
    }
};

/**
 * Function to do a recursive merge of a config object against the default config, but with no array concating.
 * @private
 * @param {object} obj - Original object to merge into
 * @param {object} source - Source object to merge from
 * @return {object} - Merged version of original object
 */
function mergeConfig(obj, source) {
    var prop;

    for (prop in source) {
        if (typeof obj[prop] === 'undefined') {
            obj[prop] = source[prop];
        } else if (typeof obj[prop] === 'object' && !Array.isArray(obj[prop]) && !Array.isArray(source[prop])) {
            obj[prop] = mergeConfig(obj[prop], source[prop]);
        }
    }
    return obj;
}

/**
 * Gets a configuration for a specific edition and environment
 *
 * @param {string} edition
 * The edition of the environment.  Typically `domestic` or `international`.
 *
 * @param {string} environment
 * The environment.  Typically `local`, `dev`, `ref`, `stage`, `prod`, etc.
 *
 * @returns {object}
 * The configuration.
 */
function getConfiguration(edition, environment) {
    var conf;

    if (typeof config[edition] === 'undefined' ||
        config[edition] === null ||
        typeof config[edition][environment] === 'undefined' ||
        config[edition][environment] === null) {
        conf = config.default;
    } else {
        conf = mergeConfig(config[edition][environment], config.default);
    }

    return conf;
}

/**
 * Gets a configuration for a specific grunt task.
 *
 * @param {string} task
 * The grunt task to get a configuration for.  If it matches `getConfiguration`
 * will be called.  If it does not match, we throw it on the ground.
 *
 * @returns {object}
 * The configuration.
 */
function getConfigurationForGruntTask(task) {
    var validReplaceTask,
        validCombineTask;

    if (task) {
        validReplaceTask = task.match(/^replace:.*:(.*):(.*)$/);
        validCombineTask = task.match(/^combine:(.*):(.*)$/);

        if (task === 'watchSass' || task === 'watchJs') {
            return getConfiguration(process.env.EDITION, process.env.ENVIRONMENT);
        }

        if (validReplaceTask) {
            return getConfiguration(validReplaceTask[1], validReplaceTask[2]);
        }

        if (validCombineTask) {
            return getConfiguration(validCombineTask[1], validCombineTask[2]);
        }
    }

    return;
}


/**
 * @module Config
 * @desc Manages all the configuration per environment and edition
 */
module.exports = {
    getConfiguration: getConfiguration,
    getConfigurationForGruntTask: getConfigurationForGruntTask
};

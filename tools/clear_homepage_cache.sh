#!/bin/bash
#
# Clear the Fastly cache for the homepage
#

# Purge command
PURGE='curl -s -X PURGE'

# The ZONE URI format
ZONEURI='/data/ocs/section/index.html:%EDITION%homepage%HPNUM%-zone-%ZONENUM%/views/zones/common/zone-manager.%IZL%'
ZONEINJURI='/data/ocs/section/_homepage_zone_injection/index.html:homepage-injection-zone-%ZONENUM%/views/zones/common/zone-manager.%IZL%'

SITE='us'
ENV='prod'

while [ $# -gt 0 ]; do
    case "$1" in
        prod*) ENV='prod' ;;
        stag*) ENV='stage' ;;
        us*) SITE='us' ;;
        int*) SITE='intl' ;;
        www*) SITE='us' ;;
        edit*) SITE='intl' ;;
        *) echo "Unknown option \"$1\"" > /dev/stderr ; exit 1 ;;
    esac
    shift 1
done

if [ $SITE = 'us' ]; then
    ZONEURI=${ZONEURI/\%EDITION\%/}
    if [ $ENV = 'stage' ]; then
        HOSTS='stage.next.cnn.com'
        echo "Flushing domestic stage homepage caches..."
    else
        HOSTS='www.cnn.com us.cnn.com'
        echo "Flushing domestic production homepage caches..."
    fi
else
    ZONEURI=${ZONEURI/\%EDITION\%/intl_}
    if [ $ENV = 'stage' ]; then
        HOSTS='edition.stage.next.cnn.com'
        echo "Flushing international stage homepage caches..."
    else
        HOSTS='edition.cnn.com'
        echo "Flushing international production homepage caches..."
    fi
fi

#
# Function to purge a URL
purgeUrl () {
    $PURGE $1 > /dev/null
    return $?
}

for h in ${HOSTS}; do
    purgeUrl "http://${h}/"
    purgeUrl "http://${h}/index.html"
    # For IZL on/off type
    for i in html izl; do
        HPIZLURL=${ZONEURI/\%IZL\%/$i}
        HPINJURL=${ZONEINJURI/\%IZL\%/$i}
        # For each homepage block, 1-4
        for (( b=1; b < 5; b++ )) ; do
            HPURL=${HPIZLURL/\%HPNUM\%/$b}
            # For each possible zone in each block, 1-10
            for (( z=1; z < 11; z++ )) ; do
                #echo "http://${h}${HPURL/\%ZONENUM\%/$z}"
                #echo "http://${h}${HPINJURL/\%ZONENUM\%/$z}"
                purgeUrl "http://${h}${HPURL/\%ZONENUM\%/$z}"
                purgeUrl "http://${h}${HPINJURL/\%ZONENUM\%/$z}"
            done
        done
    done
done

echo "Homepage caches cleared."

exit 0


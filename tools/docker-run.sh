#!/bin/bash

TOPDIR=$(dirname $0)/..
if [ "${TOPDIR}" != "." -a -n "${TOPDIR}" ]; then
    cd $TOPDIR
fi

CONTAINER=$(jq -r .name package.json):v1
if [ $# -gt 1 ]; then
    CONTAINER="$1"
fi

if [ -z "${ENVIRONMENT}" ]; then
    echo "No ENVIRONMENT set, aborting!"
    exit 1
fi

if [ "${ENVIRONMENT}" = "local" ]; then
    PAL_URL="http://172.16.123.1:8888"
    if [ -z "${PAL_TIMEOUT}" ]; then
        PAL_TIMEOUT=20000
    fi
    echo "I see you are trying to run using a local PAL instance..."
    echo "If Michonne fails to connect to PAL, you should run the following"
    echo "and try restarting:"
    echo "    sudo ifconfig lo0 alias 172.16.123.1"
    echo ""
fi

echo "Starting up..."

if [ -z "${SSL_PORT}" ]; then
    # Run without HTTPS
    docker run -i -t -p ${PORT}:${PORT} --no-healthcheck \
        -e ENVIRONMENT=${ENVIRONMENT} -e EDITION=${EDITION} \
        -e PORT=${PORT} \
        -e PAL_URL="${PAL_URL}" \
        -e PAL_TIMEOUT=${PAL_TIMEOUT} \
        -e MICHONNE_BUILD_COMPRESS=${MICHONNE_BUILD_COMPRESS} \
        -e MICHONNE_BUILD_MANGLE=${MICHONNE_BUILD_MANGLE} \
        -e MICHONNE_BUILD_BEAUTIFY=${MICHONNE_BUILD_BEAUTIFY} \
        -e LOG_LEVEL=${LOG_LEVEL} \
        -e PRODUCT=${PRODUCT} \
        -e LEGACY_CONTENT_SERVICE_URL="${LEGACY_CONTENT_SERVICE_URL}" \
        -e STATIC_FILE_URL="${STATIC_FILE_URL}" \
        ${CONTAINER}
else
    # Run with HTTPS
    docker run -i -t -p ${PORT}:${PORT} -p ${SSL_PORT}:${SSL_PORT} --no-healthcheck \
        -e ENVIRONMENT=${ENVIRONMENT} -e EDITION=${EDITION} \
        -e PORT=${PORT} -e SSL_PORT=${SSL_PORT} \
        -e PAL_URL="${PAL_URL}" \
        -e PAL_TIMEOUT=${PAL_TIMEOUT} \
        -e MICHONNE_BUILD_COMPRESS=${MICHONNE_BUILD_COMPRESS} \
        -e MICHONNE_BUILD_MANGLE=${MICHONNE_BUILD_MANGLE} \
        -e MICHONNE_BUILD_BEAUTIFY=${MICHONNE_BUILD_BEAUTIFY} \
        -e LOG_LEVEL=${LOG_LEVEL} \
        -e PRODUCT=${PRODUCT} \
        -e LEGACY_CONTENT_SERVICE_URL="${LEGACY_CONTENT_SERVICE_URL}" \
        -e STATIC_FILE_URL="${STATIC_FILE_URL}" \
        ${CONTAINER}
fi

